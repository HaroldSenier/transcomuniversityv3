using System;
using System.Data;
using System.Collections.Generic;
using System.Web.Security;
using System.Web;

namespace DotNetSCORM.PortalAPI
{
    public class PortalUsers
    {
        private Guid m_UserID;
        private string m_fullname;
        private string m_firstname;
        private string m_LastName;
        private string m_address;
        private string m_UserName;
        private string m_Email;
        private string m_Phone;

        public Guid UserID
        {
            get { return m_UserID; }
            set { m_UserID = value; }
        }

        public string FullName
        {
            get { return m_fullname; }
            set { m_fullname = value; }
        }

        public string FirstName
        {
            get
            {
                return m_firstname;
            }
        }

        public string LastName
        {
            get
            {
                return m_LastName;
            }
        }

        public string Address
        {
            get
            {
                return m_address;
            }
        }

        public string UserName
        {
            get
            {
                return m_UserName;
            }
        }

        public string Email
        {
            get
            {
                return m_Email;
            }
        }

        public string Phone
        {
            get
            {
                return m_Phone;
            }
        }

        public static MembershipUser GetCurrentUserInfo()
        {
            MembershipUser member = Membership.GetUser();
            return member;
        }

        public static string GetCurrentUserID()
        {
            //MembershipUser member = Membership.GetUser();
            //var retVal = member.UserName.Split('|');

            //return retVal[0];
            ////return Membership.GetUser().UserName;

            var transcomEmail = HttpContext.Current.User.Identity.Name.Split('|')[0];

            return transcomEmail;
        }

        public static string GetCurrentUserName()
        {
            //MembershipUser member = Membership.GetUser();
            //var retVal = member.UserName.Split('|');

            //return retVal[0];
            ////return member.UserName;

            var transcomEmail = HttpContext.Current.User.Identity.Name.Split('|')[0];

            return transcomEmail;
        }
    }  
}