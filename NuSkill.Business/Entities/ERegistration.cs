using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using NuSkill.Data;
using TheLibrary.DBImportTool;

namespace NuSkill.Business.Entities
{
    [Serializable]
    internal class ERegistration : IRegistration
    {
        #region properties
        private int _registrationID;

        public int RegistrationID
        {
            get { return _registrationID; }
            set { _registrationID = value; }
        }

        private string _lastName;

        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        private string _firstName;

        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        private string _phoneNumber;

        public string PhoneNumber
        {
            get { return _phoneNumber; }
            set { _phoneNumber = value; }
        }

        private string _location;

        public string Location
        {
            get { return _location; }
            set { _location = value; }
        }

        private string _autoUserID;

        public string AutoUserID
        {
            get { return _autoUserID; }
            set { _autoUserID = value; }
        }

        private string _autoPassword;

        public string AutoPassword
        {
            get { return _autoPassword; }
            set { _autoPassword = value; }
        }

        private int _cimNumber;

        public int CimNumber
        {
            get { return _cimNumber; }
            set { _cimNumber = value; }
        }

        private DateTime _dateCreated;

        public DateTime DateCreated
        {
            get { return _dateCreated; }
            set { _dateCreated = value; }
        }

        #endregion

        #region Constructors
        public ERegistration()
        {
        }

        public ERegistration(string lastName, string firstName, string phoneNumber, string location, string autoUserID, string autoPassword, int cimnumber)
        {
            this._lastName = lastName;
            this._firstName = firstName;
            this._phoneNumber = phoneNumber;
            this._location = location;
            this._autoUserID = autoUserID;
            this._autoPassword = autoPassword;
            this._cimNumber = cimnumber;
        }

        public ERegistration(string autoUserID)
        {
            this._lastName = string.Empty;
            this._firstName = string.Empty;
            this._phoneNumber = string.Empty;
            this._location = string.Empty;
            this._autoUserID = autoUserID;
            this._autoPassword = autoUserID;
            this._cimNumber = 0;
        }
        #endregion

        #region methods
        public void Insert()
        {
            RegistrationDAL dal = new RegistrationDAL();
            dal.Insert(this._lastName, this._firstName, this._phoneNumber, this._location, this._autoUserID, this._autoPassword, this._cimNumber);
        }

        public bool CheckExists(string firstName, string lastName, string phoneNumber, string location)
        {
            RegistrationDAL dal = new RegistrationDAL();
            IRegistration[] regs = Conversion.SetProperties<IRegistration>(dal.CheckExists(firstName, lastName, phoneNumber, location));
            return regs.Length < 1 ? false : true;
        }

        public bool ValidateLogin(string autoUserID, string autoPassword)
        {
            RegistrationDAL dal = new RegistrationDAL();
            return dal.ValidateLogin(autoUserID, autoPassword);
        }

        public IRegistration[] Search(string searchParam)
        {
            RegistrationDAL dal = new RegistrationDAL();
            return Conversion.SetProperties<IRegistration>(dal.Search(searchParam, -1));
        }

        public IRegistration Select(string userID)
        {
            RegistrationDAL dal = new RegistrationDAL();
            IRegistration[] users = Conversion.SetProperties<IRegistration>(dal.Select(userID));
            return users == null || users.Length == 0 ? null : users[0];
        }

        public IRegistration[] SelectAll()
        {
            RegistrationDAL dal = new RegistrationDAL();
            return Conversion.SetProperties<IRegistration>(dal.SelectAll());
        }


        public int LoginToCim(string username, string password)
        {
            RegistrationDAL dal = new RegistrationDAL();
            if (dal.LoginToCim(username, password).Tables.Count < 1)
                return 0;
            return 1;
        }

        public int GetCim(string username)
        {
            RegistrationDAL dal = new RegistrationDAL();
            return dal.GetCim(username);
        }

        public void InsertByBatch(int batchID, string prefix, int minSeed, int maxSeed, string suffix)
        {
            RegistrationDAL dal = new RegistrationDAL();
            dal.InsertByBatch(batchID, prefix, minSeed, maxSeed, suffix);
        }

        public IRegistration[] GetExistingByBatch(string prefix, int minseed, int maxseed, string suffix)
        {
            RegistrationDAL dal = new RegistrationDAL();
            return Conversion.SetProperties<IRegistration>(dal.GetExistingByBatch(prefix, minseed, maxseed, suffix));
        }

        public IRegistration[] SelectByBatch(int batchID)
        {
            RegistrationDAL dal = new RegistrationDAL();
            return Conversion.SetProperties<IRegistration>(dal.SelectByBatch(batchID));
        }

        public IRegistration[] SearchByBatch(string username, int batchID)
        {
            RegistrationDAL dal = new RegistrationDAL();
            return Conversion.SetProperties<IRegistration>(dal.Search(username, batchID));
        }

        public int SearchEmployee(int cimNumber)
        {
            RegistrationDAL dal = new RegistrationDAL();
            return dal.SearchEmployee(cimNumber);
        }
        #endregion
    }
}
