using System;
using System.Collections.Generic;
using System.Text;
using TheLibrary.DBImportTool;
using NuSkill.Data;

namespace NuSkill.Business
{
    [Serializable]
    internal class ERightsLevel : IRightsLevel
    {
        private int _rightsLevelID;

        public int RightsLevelID
        {
            get { return _rightsLevelID; }
            set { _rightsLevelID = value; }
        }

        private string _rightsLevel;

        public string RightsLevelName
        {
            get { return _rightsLevel; }
            set { _rightsLevel = value; }
        }

        private bool _hideFromList;

        public bool HideFromList
        {
            get { return _hideFromList; }
            set { _hideFromList = value; }
        }

        public IRightsLevel[] SelectAll()
        {
            RightsLevelDal dal = new RightsLevelDal();
            return Conversion.SetProperties<IRightsLevel>(dal.SelectAll());
        }
    }
}
