using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using TheLibrary.DBImportTool;
using NuSkill.Data;

namespace NuSkill.Business.Entities
{
    [Serializable]
    public class ETestCategoryHarmonyQualificationMatrix : ITestCategoryHarmonyQualificationMatrix
    {
        private int _testCategoryHarmonyQualificationMatrixID;

        public int TestCategoryHarmonyQualificationID
        {
            get { return _testCategoryHarmonyQualificationMatrixID; }
            set { _testCategoryHarmonyQualificationMatrixID = value; }
        }

        private int _testCategoryID;

        public int TestCategoryID
        {
            get { return _testCategoryID; }
            set { _testCategoryID = value; }
        }

        private ITestCategory _testCategory;

        public ITestCategory TestCategory
        {
            get { return _testCategory; }
            set { _testCategory = value; }
        }

        private int _harmonyQualificationID;

        public int HarmonyQualificationID
        {
            get { return _harmonyQualificationID; }
            set { _harmonyQualificationID = value; }
        }

        private bool _hideFromList;

        public bool HideFromList
        {
            get { return _hideFromList; }
            set { _hideFromList = value; }
        }

        public void Insert()
        {
            TestCategoryHarmonyQualificationDal dal = new TestCategoryHarmonyQualificationDal();
            dal.Insert(this._testCategoryID, this._harmonyQualificationID);
        }

        public void Delete(int testCategoryHarmonyQualificationMatrixID)
        {
            TestCategoryHarmonyQualificationDal dal = new TestCategoryHarmonyQualificationDal();
            dal.Delete(testCategoryHarmonyQualificationMatrixID);
        }

        public DataSet SelectAll()
        {
            TestCategoryHarmonyQualificationDal dal = new TestCategoryHarmonyQualificationDal();
            return dal.SelectAll();
        }

    }
}
