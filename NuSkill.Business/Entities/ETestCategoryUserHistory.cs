using System;
using System.Collections.Generic;
using System.Text;
using NuSkill.Data;
using TheLibrary.DBImportTool;

namespace NuSkill.Business
{
    [Serializable]
    internal class ETestCategoryUserHistory : ITestCategoryUserHistory
    {
        #region properties
        private int _testCategoryID;

        public int TestCategoryID
        {
            get { return _testCategoryID; }
            set { _testCategoryID = value; }
        }

        private ITestCategory _testCategory;

        public ITestCategory TestCategory
        {
            get { return _testCategory; }
            set { _testCategory = value; }
        }

        private string _testName;

        public string TestName
        {
            get { return _testName; }
            set { _testName = value; }
        }

        private int _topScore;

        public int TopScore
        {
            get { return _topScore; }
            set { _topScore = value; }
        }

        private int _passingScore;

        public int PassingScore
        {
            get { return _passingScore; }
            set { _passingScore = value; }
        }

        private bool _passed;

        public bool Passed
        {
            get { return _passed; }
            set { _passed = value; }
        }

        private int _triesMade;

        public int TriesMade
        {
            get { return _triesMade; }
            set { _triesMade = value; }
        }

        private int _maxTries;

        public int MaxTries
        {
            get { return _maxTries; }
            set { _maxTries = value; }
        }
        #endregion

        #region methods
        public ITestCategoryUserHistory[] SelectAllUserHistory(string userID, bool includeElapsed, bool recentOnly, bool isCampaign, int campaignAccountID, int subcategory)
        {
            TestCategoryDAL dal = new TestCategoryDAL();
            return Conversion.SetProperties<ITestCategoryUserHistory>(dal.SelectAllUserHistory(userID, includeElapsed, recentOnly, isCampaign, campaignAccountID, subcategory));
        }

        public ITestCategoryUserHistory[] SelectAllTestGroup(string userID, bool includeElapsed, bool recentOnly, int testGroupID)
        {
            TestCategoryDAL dal = new TestCategoryDAL();
            return Conversion.SetProperties<ITestCategoryUserHistory>(dal.SelectAllTestGroup(userID, includeElapsed, recentOnly, testGroupID));
        }
        #endregion
    }
}
