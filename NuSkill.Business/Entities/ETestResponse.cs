using System;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using NuSkill.Data;
using TheLibrary.DBImportTool;

namespace NuSkill.Business.Entities
{
    [Serializable]
    internal class ETestResponse : ITestResponse
    {
        #region properties
        private int _testResponseID;

        public int TestResponseID
        {
            get { return _testResponseID; }
            set { _testResponseID = value; }
        }

        private string _userID;

        public string UserID
        {
            get { return _userID; }
            set { _userID = value; }
        }

        private int _testTakenID;

        public int TestTakenID
        {
            get { return _testTakenID; }
            set { _testTakenID = value; }
        }

        private int _questionnaireID;

        public int QuestionnaireID
        {
            get { return _questionnaireID; }
            set { _questionnaireID = value; }
        }

        private IQuestionnaire _questionnaire;

        public IQuestionnaire Questionnaire
        {
            get { return _questionnaire; }
            set { _questionnaire = value; }
        }

        private string _response1;

        public string Response1
        {
            get { return _response1; }
            set { _response1 = value; }
        }

        private string _response2;

        public string Response2
        {
            get { return _response2; }
            set { _response2 = value; }
        }

        private string _response3;

        public string Response3
        {
            get { return _response3; }
            set { _response3 = value; }
        }

        private string _response4;

        public string Response4
        {
            get { return _response4; }
            set { _response4 = value; }
        }

        private string _response5;

        public string Response5
        {
            get { return _response5; }
            set { _response5 = value; }
        }

        private string _response6;

        public string Response6
        {
            get { return _response6; }
            set { _response6 = value; }
        }

        private string _response7;

        public string Response7
        {
            get { return _response7; }
            set { _response7 = value; }
        }

        private string _response8;

        public string Response8
        {
            get { return _response8; }
            set { _response8 = value; }
        }

        private string _response9;

        public string Response9
        {
            get { return _response9; }
            set { _response9 = value; }
        }

        private string _response10;

        public string Response10
        {
            get { return _response10; }
            set { _response10 = value; }
        }

        private string _essayResponse;

        public string EssayResponse
        {
            get { return _essayResponse; }
            set { _essayResponse = value; }
        }

        private int _essayScore;

        public int EssayScore
        {
            get { return _essayScore; }
            set { _essayScore = value; }
        }

        #endregion

        #region Constructors
        public ETestResponse()
        {
        }

        public ETestResponse(string userID, int testTakenID, int questionnaireID, string response1, string response2, string response3,
            string response4, string response5, string response6, string response7, string response8, string response9, string response10)
        {
            this._userID = userID;
            this._testTakenID = testTakenID;
            this._questionnaireID = questionnaireID;
            this._response1 = response1;
            this._response2 = response2;
            this._response3 = response3;
            this._response4 = response4;
            this._response5 = response5;
            this._response6 = response6;
            this._response7 = response7;
            this._response8 = response8;
            this._response9 = response9;
            this._response10 = response10;
        }

        public ETestResponse(ISaveTestResponse response)
        {
            this._questionnaireID = response.Questionnaire.QuestionnaireID;
            this._response1 = response.Response1;
            this._response2 = response.Response2;
            this._response3 = response.Response3;
            this._response4 = response.Response4;
            this._response5 = response.Response5;
            this._response6 = response.Response6;
            this._response7 = response.Response7;
            this._response8 = response.Response8;
            this._response9 = response.Response9;
            this._response10 = response.Response10;
            this._testResponseID = response.SaveTestResponseID;
            this._userID = response.UserID;
            this._essayResponse = response.EssayResponse;
        }

        #endregion

        #region methods
        public int Insert()
        {
            TestResponseDAL dal = new TestResponseDAL();
            return dal.Insert(this._userID, this._testTakenID, this._questionnaireID, this._response1, this._response2, this._response3,
                this._response4, this._response5, this._response6, this._response7, this._response8, this._response9, this._response10, this._essayResponse);
        }

        public void GradeEssayQuestion(int essayScore)
        {
            TestResponseDAL dal = new TestResponseDAL();
            dal.GradeEssayQuestion(essayScore, this._testResponseID);
        }

        public ITestResponse[] SelectByUser(string userID)
        {
            TestResponseDAL dal = new TestResponseDAL();
            return Conversion.SetProperties<ITestResponse>(dal.SelectByUser(userID));
        }

        public ITestResponse Select(int testResponseID)
        {
            TestResponseDAL dal = new TestResponseDAL();
            ITestResponse[] responses = Conversion.SetProperties<ITestResponse>(dal.Select(testResponseID));
            return responses.Length < 1 || responses == null ? null : responses[0];
        }

        public ITestResponse Select(int testTakenID, int questionnaireID, string userID)
        {
            TestResponseDAL dal = new TestResponseDAL();
            ITestResponse[] responses = Conversion.SetProperties<ITestResponse>(dal.Select(testTakenID, questionnaireID, userID));
            return responses.Length < 1 || responses == null ? null : responses[0];
        }

        public ITestResponse[] SelectAll()
        {
            TestResponseDAL dal = new TestResponseDAL();
            return Conversion.SetProperties<ITestResponse>(dal.SelectAll());
        }

        public int CountTestResponse(int questionnaireID)
        {
            TestResponseDAL dal = new TestResponseDAL();
            return dal.CountPerQuestion(questionnaireID);
        }

        public int GetCorrectAnswer(int questionnaireID)
        {
            TestResponseDAL dal = new TestResponseDAL();
            return dal.GetCorrectAnswer(questionnaireID);
        }

        public ITestResponse[] SelectByQuestion(int questionnaireID)
        {

            TestResponseDAL dal = new TestResponseDAL();
            return Conversion.SetProperties<ITestResponse>(dal.SelectByQuestion(questionnaireID));
        }

        public ITestResponse[] SelectByTestTaken(int testTakenID)
        {
            TestResponseDAL dal = new TestResponseDAL();
            return Conversion.SetProperties<ITestResponse>(dal.SelectByTestTaken(testTakenID));
        }

        public DataSet GetMultipleEssays(int testTakenID)
        {
            TestResponseDAL dal = new TestResponseDAL();
            return dal.GetMultipleEssays(testTakenID);
        }
        #endregion
    }
}
