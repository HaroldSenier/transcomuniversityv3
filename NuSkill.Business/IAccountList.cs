using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace NuSkill.Business
{
    public interface IAccountList
    {
        int AccountID { get; set; }
        string Account { get; set; }
        IAccountList[] SelectAll(int showAll);
        IAccountList[] SelectAllVader2(int showAll);
        IAccountList[] SelectAccountsVader2(int showAll);
        IAccountList[] SelectGenericCampaignsVader2(int showAll);
        IAccountList[] SelectAccounts(int showAll);
        IAccountList[] SelectGenericCampaigns(int showAll);
    }
}
