using System;
using System.Collections.Generic;
using System.Text;

namespace NuSkill.Business
{
    public interface IAppRights
    {
        int AppRightsID { get; set; }
        string Title { get; set; }
        string Description { get; set; }

        IAppRights[] SelectAll();
    }
}
