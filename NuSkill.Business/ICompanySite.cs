using System;
using System.Collections.Generic;
using System.Text;

namespace NuSkill.Business
{
    public interface ICompanySiteForTests
    {
        string CompanySite { get; set; }
        string CompanySiteShort { get; set; }

        ICompanySiteForTests[] GetCompanySites(int cimnumber);
    }
}
