using System;
using System.Collections.Generic;
using System.Text;

namespace NuSkill.Business
{
    public interface IEssayMarker
    {
        int EssayMarkerID { get; set; }
        string UserID { get; set; }
        int TestResponseID { get; set; }
        int Score { get; set; }
        DateTime DateMarked { get; set; }
        string CheckerComments { get; set; }
        int Insert();
        IEssayMarker[] SelectByTestTaken(int testTakenID);
        IEssayMarker SelectByTestResponse(int testresponseID);
    }
}
