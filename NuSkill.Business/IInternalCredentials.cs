using System;
using System.Collections.Generic;
using System.Text;

namespace NuSkill.Business
{
    public interface IInternalCredentials
    {
        int Result { get; set; }
        int CimNumber { get; set; }
        int RoleID { get; set; }
        int AccountID { get; set; }
        string Account { get; set; }

        IInternalCredentials LoginUser(int cimNumber, string password);
    }
}
