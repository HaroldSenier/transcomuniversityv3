using System;
using System.Collections.Generic;
using System.Text;

namespace NuSkill.Business
{
    public interface INonCimCampaign
    {
        int CampaignID { get; set; }
        string Campaign { get; set; }
        bool Hide { get; set; }
        DateTime StartDate { get; set; }
        int ParentCampaignID { get; set; }
        DateTime EndDate { get; set; }
        int DisplayParentCampaignID { get; set; }

        INonCimCampaign[] SelectAll();
        INonCimCampaign[] SelectParents();
        INonCimCampaign[] SelectFromParent(int campaignID, bool includeNone);
        INonCimCampaign Select(int campaignID);
        INonCimCampaign[] SelectExcept(int campaignID);
        int Insert();
        void Delete();
    }
}
