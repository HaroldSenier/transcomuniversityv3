using System;
using System.Collections.Generic;
using System.Text;

namespace NuSkill.Business
{
    public interface IRecommendedQuestion
    {
        IQuestionnaire Questionnaire { get; set; }
        int TestCategoryID { get; set; }
        DateTime DateCreated { get; set; }
        string CreatedBy { get; set; }
        string CreatorComments { get; set; }
        int RecommendedQuestionStatusID { get; set; }
        DateTime DateApproved { get; set; }
        string ApprovedBy { get; set; }
        string ApproverComments { get; set; }
        bool HideFromList { get; set; }


        void Insert();
        IRecommendedQuestion SelectByQuestionnaireID(int questionnaireID);
        void Update();
    }
}
