using System;
using System.Collections.Generic;
using System.Text;

namespace NuSkill.Business
{
    public interface IRightsLevel
    {
        int RightsLevelID { get; set; }
        string RightsLevelName { get; set; }
        bool HideFromList { get; set; }
        IRightsLevel[] SelectAll();
    }
}
