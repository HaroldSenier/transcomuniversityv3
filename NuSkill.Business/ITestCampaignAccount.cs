using System;
using System.Collections.Generic;
using System.Text;

namespace NuSkill.Business
{
    public interface ITestCampaignAccount
    {
        int TestCategoryID { get; set; }
        int CampaignID { get; set; }
        int AccountID { get; set; }
        bool IsCampaign { get; set; }

        ITestCampaignAccount[] SelectAll();
        ITestCampaignAccount Select(int testCategoryID, int campaignAccountID, bool isCampaign);
        ITestCampaignAccount SelectByTestCategory(int testCategoryID);
        ITestCampaignAccount[] SelectByCampaign(int campaignID);
        ITestCampaignAccount[] SelectByAccount(int accountID);
        void Insert();
        void Update();
    }
}
