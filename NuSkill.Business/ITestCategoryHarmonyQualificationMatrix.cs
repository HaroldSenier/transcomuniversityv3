using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace NuSkill.Business
{
    public interface ITestCategoryHarmonyQualificationMatrix
    {
        int TestCategoryHarmonyQualificationID { get; set; }
        ITestCategory TestCategory { get; set; }
        int HarmonyQualificationID { get; set; }
        bool HideFromList { get; set; }

        void Insert();
        void Delete(int testCategoryHarmonyQualificationMatrixID);
        DataSet SelectAll();
    }
}
