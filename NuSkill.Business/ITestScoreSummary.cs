using System;
using System.Collections.Generic;
using System.Text;

namespace NuSkill.Business
{
    public interface ITestScoreSummary
    {
        ITestTaken TestTaken { get; set; }
        ITestCategory TestCategory { get; set; }
        int CimNumber { get; set; }
        string TestName { get; set; }
        double Score { get; set; }
        bool Passed { get; set; }
        int AccountID { get; set; }
        DateTime DateLastSave { get; set; }

        void Insert();
    }
}
