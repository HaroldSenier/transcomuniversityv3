using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using TheLibrary.DBImportTool;
using NuSkill.Data;

namespace NuSkill.Business
{
    [Serializable]
    public class NonCimCampaign
    {
        private int _campaignID;

        public int CampaignID
        {
            get { return _campaignID; }
            set { _campaignID = value; }
        }

        private string _campaign;

        public string Campaign
        {
            get { return _campaign; }
            set { _campaign = value; }
        }

        private bool _hide;

        public bool Hide
        {
            get { return _hide; }
            set { _hide = value; }
        }

        private DateTime _startDate;

        public DateTime StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }

        private int _parentCampaignID;

        public int ParentCampaignID
        {
            get { return _parentCampaignID; }
            set { _parentCampaignID = value; }
        }

        private DateTime _endDate;

        public DateTime EndDate
        {
            get { return _endDate; }
            set { _endDate = value; }
        }

        private int _displayParentCampaignID;

        public int DisplayParentCampaignID
        {
            get { return _displayParentCampaignID; }
            set { _displayParentCampaignID = value; }
        }

        public NonCimCampaign()
        { }

        public static NonCimCampaign[] SelectAll()
        {
            NonCimCampaignDal dal = new NonCimCampaignDal();
            return Conversion.SetProperties<NonCimCampaign>(dal.SelectAll());
        }

        public static NonCimCampaign[] SelectParents()
        {
            NonCimCampaignDal dal = new NonCimCampaignDal();
            return Conversion.SetProperties<NonCimCampaign>(dal.SelectParents());
        }

        public static NonCimCampaign[] SelectFromParent(int campaignID, bool includeNone)
        {
            NonCimCampaignDal dal = new NonCimCampaignDal();
            return Conversion.SetProperties<NonCimCampaign>(dal.selectFromParent(campaignID, includeNone));
        }

        public static NonCimCampaign Select(int campaignID)
        {
            NonCimCampaignDal dal = new NonCimCampaignDal();
            NonCimCampaign[] campaigns = Conversion.SetProperties<NonCimCampaign>(dal.Select(campaignID));
            return campaigns == null || campaigns.Length == 0 ? null : campaigns[0];
        }

        public static NonCimCampaign[] SelectExcept(int campaignID)
        {
            NonCimCampaignDal dal = new NonCimCampaignDal();
            return Conversion.SetProperties<NonCimCampaign>(dal.SelectExcept(campaignID));
        }

        public int Insert()
        {
            NonCimCampaignDal dal = new NonCimCampaignDal();
            return dal.Insert(this._campaign, this._startDate, this._endDate, this._parentCampaignID);
        }

        public void Delete()
        {
            NonCimCampaignDal dal = new NonCimCampaignDal();
            dal.Delete(this._campaignID, this._hide);
        }

        public void Update()
        {
            NonCimCampaignDal dal = new NonCimCampaignDal();
            dal.Update(this._campaignID, this._campaign, this._hide, this._startDate, this._endDate, this._parentCampaignID);
        }

        public static NonCimCampaign[] SelectCatSubFromParent(int campaignID, int cim)
        {
            NonCimCampaignDal dal = new NonCimCampaignDal();
            return Conversion.SetProperties<NonCimCampaign>(dal.selectCatSubFromParent(campaignID, cim));
        }
    }
}
