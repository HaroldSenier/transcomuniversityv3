using System;
using NuSkill.Data;
using TheLibrary.DBImportTool;

namespace NuSkill.Business
{
    [Serializable]
    public class Registration_Applicant
    {
        #region properties
        private int _applicantId;

        public int ApplicantId
        {
            get { return _applicantId; }
            set { _applicantId = value; }
        }

        private string _firstName;

        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        private string _lastName;

        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        private string _atsId;

        public string ATSID
        {
            get { return _atsId; }
            set { _atsId = value; }
        }

        private string _site;

        public string Site
        {
            get { return _site; }
            set { _site = value; }
        }

        private DateTime _createDate;

        public DateTime CreateDate
        {
            get { return _createDate; }
            set { _createDate = value; }
        }

        #endregion

        #region Constructors

        public Registration_Applicant()
        {
        }

        public Registration_Applicant(string atsid, string fname, string lname, string site)
        {
            _atsId = atsid;
            _firstName = fname;
            _lastName = lname;
            _site = site;
        }

        #endregion

        #region methods
        public void Insert_Applicant()
        {
            RegistrationDAL dal = new RegistrationDAL();
            dal.Insert_Applicant(_atsId, _firstName, _lastName, _site);
        }

        public static bool CheckExists_Applicant(string atsid, string fname, string lname, string site)
        {
            RegistrationDAL dal = new RegistrationDAL();
            Registration[] regs = Conversion.SetProperties<Registration>(dal.CheckExists_Applicant(atsid, fname, lname, site));
            return regs.Length < 1 ? false : true;
        }

        public static bool ValidateLogin_Applicant(string autoUserID, string autoPassword)
        {
            RegistrationDAL dal = new RegistrationDAL();
            return dal.ValidateLogin(autoUserID, autoPassword);
        }

        //public static Registration[] Search(string searchParam)
        //{
        //    RegistrationDAL dal = new RegistrationDAL();
        //    return Conversion.SetProperties<Registration>(dal.Search(searchParam, -1));
        //}

        //public static Registration Select(string userID)
        //{
        //    RegistrationDAL dal = new RegistrationDAL();
        //    Registration[] users = Conversion.SetProperties<Registration>(dal.Select(userID));
        //    return users == null || users.Length == 0 ? null : users[0];
        //}

        //public static Registration[] SelectAll()
        //{
        //    RegistrationDAL dal = new RegistrationDAL();
        //    return Conversion.SetProperties<Registration>(dal.SelectAll());
        //}


        //public static int LoginToCim(string username, string password)
        //{
        //    RegistrationDAL dal = new RegistrationDAL();
        //    if (dal.LoginToCim(username, password).Tables.Count < 1)
        //        return 0;
        //    return 1;
        //}

        //public static int GetCim(string username)
        //{
        //    RegistrationDAL dal = new RegistrationDAL();
        //    return dal.GetCim(username);
        //}

        //public static void InsertByBatch(int batchID, string prefix, int minSeed, int maxSeed, string suffix)
        //{
        //    RegistrationDAL dal = new RegistrationDAL();
        //    dal.InsertByBatch(batchID, prefix, minSeed, maxSeed, suffix);
        //}

        //public static Registration[] GetExistingByBatch(string prefix, int minseed, int maxseed, string suffix)
        //{
        //    RegistrationDAL dal = new RegistrationDAL();
        //    return Conversion.SetProperties<Registration>(dal.GetExistingByBatch(prefix, minseed, maxseed, suffix));
        //}

        //public static Registration[] SelectByBatch(int batchID)
        //{
        //    RegistrationDAL dal = new RegistrationDAL();
        //    return Conversion.SetProperties<Registration>(dal.SelectByBatch(batchID));
        //}

        //public static Registration[] SearchByBatch(string username, int batchID)
        //{
        //    RegistrationDAL dal = new RegistrationDAL();
        //    return Conversion.SetProperties<Registration>(dal.Search(username, batchID));
        //}

        //public static int SearchEmployee(int cimNumber)
        //{
        //    RegistrationDAL dal = new RegistrationDAL();
        //    return dal.SearchEmployee(cimNumber);
        //}
        #endregion
    }
}
