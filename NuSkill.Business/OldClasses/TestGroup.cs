using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using NuSkill.Data;
using TheLibrary.DBImportTool;

namespace NuSkill.Business
{
    [Serializable]
    public class TestGroup
    {
        #region properties
        private int _testGroupID;

        public int TestGroupID
        {
            get { return _testGroupID; }
            set { _testGroupID = value; }
        }

        private string _testGroupName;

        public string TestGroupName
        {
            get { return _testGroupName; }
            set { _testGroupName = value; }
        }

        private string _createdBy;

        public string CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }

        private bool _hideFromList;

        public bool HideFromList
        {
            get { return _hideFromList; }
            set { _hideFromList = value; }
        }
        #endregion

        #region constructors
        public TestGroup()
        { }

        public TestGroup(string testGroupName, string createdBy)
        {
            this._testGroupName = testGroupName;
            this._createdBy = createdBy;
        }
        #endregion

        #region methods
        public static TestGroup Select(int testGroupID)
        {
            TestGroupDAL dal = new TestGroupDAL();
            TestGroup[] groups = Conversion.SetProperties<TestGroup>(dal.Select(testGroupID));
            return groups == null || groups.Length == 0 ? null : groups[0];
        }

        public static TestGroup[] SelectAll()
        {
            TestGroupDAL dal = new TestGroupDAL();
            return Conversion.SetProperties<TestGroup>(dal.SelectAll());
        }

        public void Delete()
        {
            TestGroupDAL dal = new TestGroupDAL();
            dal.Delete(this._testGroupID);
        }

        public void Insert()
        {
            TestGroupDAL dal = new TestGroupDAL();
            dal.Insert(this._testGroupName, this._createdBy);
        }
        #endregion
    }
}
