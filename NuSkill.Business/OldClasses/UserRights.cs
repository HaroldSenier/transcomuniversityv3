using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using NuSkill.Data;
using TheLibrary.DBImportTool;

namespace NuSkill.Business
{
    [Serializable]
    public class UserRights
    {
        #region Properties
        private int _userRightsID;

        public int UserRightsID
        {
            get { return _userRightsID; }
            set { _userRightsID = value; }
        }

        private string _userID;

        public string UserID
        {
            get { return _userID; }
            set { _userID = value; }
        }

        private string _rights;

        public string Rights
        {
            get { return _rights; }
            set { _rights = value; }
        }

        private int _rightsLevel;

        public int RightsLevel
        {
            get { return _rightsLevel; }
            set { _rightsLevel = value; }
        }


        private bool _hideFromList;

        public bool HideFromList
        {
            get { return _hideFromList; }
            set { _hideFromList = value; }
        }

        #endregion

        #region Constructors
        public UserRights()
        { }

        public UserRights(string username, string rights)
        {
            this._rights = rights;
            this._userID = username;
            this._rightsLevel = 1;
        }

        public UserRights(string username, string rights, int rightsLevel)
        {
            this._rights = rights;
            this._userID = username;
            this._rightsLevel = rightsLevel;
        }
        #endregion

        #region Methods
        public static UserRights Select(string username, int rightsLevel)
        {
            UserRightsDal dal = new UserRightsDal();
            UserRights[] appRights = Conversion.SetProperties<UserRights>(dal.Select(username, rightsLevel));
            return appRights == null || appRights.Length == 0 ? null : appRights[0];
        }

        public static UserRights[] SelectAll()
        {
            UserRightsDal dal = new UserRightsDal();
            return Conversion.SetProperties<UserRights>(dal.SelectAll());
        }

        public static UserRights[] SelectAllByUser(string username)
        {
            UserRightsDal dal = new UserRightsDal();
            return Conversion.SetProperties<UserRights>(dal.SelectAllByUser(username));
        }

        public void Insert()
        {
            UserRightsDal dal = new UserRightsDal();
            dal.Insert(this._userID, this._rights, this._rightsLevel);
        }

        public void CopyRights(int copyFrom)
        {
            UserRightsDal dal = new UserRightsDal();
            dal.CopyRights(copyFrom, this._userRightsID);
        }

        public void DeleteRights()
        {
            UserRightsDal dal = new UserRightsDal();
            dal.DeleteRights(this._userRightsID);
        }

        public void EditRights()
        {
            UserRightsDal dal = new UserRightsDal();
            dal.EditRights(this._userRightsID, this._rights);
        }

        public static DataSet GetRoles()
        {
            UserRightsDal dal = new UserRightsDal();
            return dal.GetRoles();
        }
        #endregion
    }
}
