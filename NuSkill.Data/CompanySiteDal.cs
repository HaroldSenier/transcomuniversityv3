using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using TheLibrary.DBImportTool;

namespace NuSkill.Data
{
    public class CompanySiteForTestsDal : TheLibrary.DBImportTool.Base
    {
        public CompanySiteForTestsDal()
        {
            //this.ConnectionString = Properties.Settings.Default.LocalConnectionString;
        }


        public CompanySiteForTestsDal(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        public DataSet GetCompanySites(int cimnumber)
        {
            return Connection.ExecuteSPQuery("pr_WeeklyTesting_Lkp_CompanySiteByCIM",
                Helper.CreateParam("@CIMNumber", SqlDbType.Int, cimnumber));
        }
    }
}
