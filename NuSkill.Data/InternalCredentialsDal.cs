using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using TheLibrary.DBImportTool;

namespace NuSkill.Data
{
    public class InternalCredentialsDal : Base
    {
        public InternalCredentialsDal()
        {
            //this.ConnectionString = Properties.Settings.Default.LocalConnectionString;
        }

        public InternalCredentialsDal(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        public DataSet LoginUser(int cimNumber, string password)
        {
            return Connection.ExecuteSPQuery("pr_InternalCredentials_Lkp_LoginUser",
                Helper.CreateParam("@CimNumber", SqlDbType.Int, cimNumber),
                Helper.CreateParam("@PassPhrase", SqlDbType.VarChar, password));
        }
    }
}
