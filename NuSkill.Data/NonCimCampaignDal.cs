using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using TheLibrary.DBImportTool;

namespace NuSkill.Data
{
    public class NonCimCampaignDal : Base
    {
        public NonCimCampaignDal()
        { }

        public NonCimCampaignDal(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        public DataSet SelectAll()
        {
            return Connection.ExecuteSPQuery("pr_Campaign_SelectAll");
        }

        public DataSet SelectParents()
        {
            return Connection.ExecuteSPQuery("pr_Campaign_SelectParents");
        }

        public DataSet selectFromParent(int campaignID, bool includeNone)
        {
            return Connection.ExecuteSPQuery("pr_Campaign_SelectFromParent",
                Helper.CreateParam("@CampaignID", SqlDbType.Int, campaignID),
                Helper.CreateParam("@IncludeNone", SqlDbType.Bit, includeNone));
        }

        public DataSet Select(int campaignID)
        {
            return Connection.ExecuteSPQuery("pr_Campaign_Select",
                Helper.CreateParam("@CampaignID", SqlDbType.Int, campaignID));
        }

        public DataSet SelectExcept(int campaignID)
        {
            return Connection.ExecuteSPQuery("pr_Campaign_SelectExcept",
                Helper.CreateParam("@CampaignID", SqlDbType.Int, campaignID));
        }

        public int Insert(string campaign, DateTime startDate, DateTime endDate, int parentCampaignID)
        {
            return Convert.ToInt32(Connection.ExecuteScalar("pr_Campaign_Insert",
                Helper.CreateParam("@Campaign", SqlDbType.VarChar, campaign),
                Helper.CreateParam("@StartDate", SqlDbType.DateTime, startDate),
                Helper.CreateParam("@EndDate", SqlDbType.DateTime, endDate),
                Helper.CreateParam("@ParentCampaignID", SqlDbType.Int, parentCampaignID)));
        }

        public void Delete(int campaignID, bool delete)
        {
            Connection.ExecuteSP("pr_Campaign_Delete",
                Helper.CreateParam("@CampaignID", SqlDbType.Int, campaignID),
                Helper.CreateParam("@Delete", SqlDbType.Bit, delete));
        }

        public void Update(int campaignID, string campaign, bool delete, DateTime startDate, DateTime endDate, int parentCampaignID)
        {
            Connection.ExecuteSP("pr_Campaign_Update",
                Helper.CreateParam("@CampaignID", SqlDbType.Int, campaignID),
                Helper.CreateParam("@Campaign", SqlDbType.VarChar, campaign),
                Helper.CreateParam("@Delete", SqlDbType.Bit, delete),
                Helper.CreateParam("@StartDate", SqlDbType.DateTime, startDate),
                Helper.CreateParam("@EndDate", SqlDbType.DateTime, endDate),
                Helper.CreateParam("@ParentCampaignID", SqlDbType.Int, parentCampaignID));
        }

        public DataSet selectCatSubFromParent(int campaignID, int cim)
        {
            return Connection.ExecuteSPQuery("pr_CatSub_SelectFromParent",
                Helper.CreateParam("@CampaignID", SqlDbType.Int, campaignID),
                Helper.CreateParam("@CIM", SqlDbType.Int, cim));
        }
    }
}
