using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using TheLibrary.DBImportTool;

namespace NuSkill.Data
{
    public class SaveTestResponseDAL : TheLibrary.DBImportTool.Base
    {
        public SaveTestResponseDAL()
        {
            //this.ConnectionString = Properties.Settings.Default.LocalConnectionString;
        }

        public SaveTestResponseDAL(string connString)
        {
            this.ConnectionString = connString;
        }

        public void Insert(string userID, int testTakenID, int questionnaireID, string response1, string response2, string response3,
            string response4, string response5, string response6, string response7, string response8, string response9, string response10, string essayResponse)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pUserID", SqlDbType.VarChar, userID));
            paramList.Add(Helper.CreateParam("@pTestTakenID", SqlDbType.Int, testTakenID));
            paramList.Add(Helper.CreateParam("@pQuestionnaireID", SqlDbType.Int, questionnaireID));
            paramList.Add(Helper.CreateParam("@pResponse1", SqlDbType.VarChar, response1));
            paramList.Add(Helper.CreateParam("@pResponse2", SqlDbType.VarChar, response2));
            paramList.Add(Helper.CreateParam("@pResponse3", SqlDbType.VarChar, response3));
            paramList.Add(Helper.CreateParam("@pResponse4", SqlDbType.VarChar, response4));
            paramList.Add(Helper.CreateParam("@pResponse5", SqlDbType.VarChar, response5));
            paramList.Add(Helper.CreateParam("@pResponse6", SqlDbType.VarChar, response6));
            paramList.Add(Helper.CreateParam("@pResponse7", SqlDbType.VarChar, response7));
            paramList.Add(Helper.CreateParam("@pResponse8", SqlDbType.VarChar, response8));
            paramList.Add(Helper.CreateParam("@pResponse9", SqlDbType.VarChar, response9));
            paramList.Add(Helper.CreateParam("@pResponse10", SqlDbType.VarChar, response10));
            paramList.Add(Helper.CreateParam("@pEssayResponse", SqlDbType.VarChar, essayResponse));
            Connection.ExecuteSP("pr_saveTestResponse_sav_insert", paramList.ToArray());
        }

        public void UpdateAnswers(int saveTestResponseID, string response1, string response2, string response3, string response4, string response5, string response6,
            string response7, string response8, string response9, string response10, string essayResponse)
        {
            Connection.ExecuteSP("pr_savetestResponse_sav_updateanswers",
                Helper.CreateParam("@SaveTestResponseID", SqlDbType.Int, saveTestResponseID),
                Helper.CreateParam("@Response1", SqlDbType.VarChar, response1),
                Helper.CreateParam("@Response2", SqlDbType.VarChar, response2),
                Helper.CreateParam("@Response3", SqlDbType.VarChar, response3),
                Helper.CreateParam("@Response4", SqlDbType.VarChar, response4),
                Helper.CreateParam("@Response5", SqlDbType.VarChar, response5),
                Helper.CreateParam("@Response6", SqlDbType.VarChar, response6),
                Helper.CreateParam("@Response7", SqlDbType.VarChar, response7),
                Helper.CreateParam("@Response8", SqlDbType.VarChar, response8),
                Helper.CreateParam("@Response9", SqlDbType.VarChar, response9),
                Helper.CreateParam("@Response10", SqlDbType.VarChar, response10),
                Helper.CreateParam("@EssayResponse", SqlDbType.VarChar, essayResponse));
        }


        public DataSet Retrieve(string userID, int testTakenID, int questionnaireID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pUserID", SqlDbType.VarChar, userID));
            paramList.Add(Helper.CreateParam("@pTestTakenID", SqlDbType.Int, testTakenID));
            paramList.Add(Helper.CreateParam("@pQuestionnaireID", SqlDbType.Int, questionnaireID));
            return Connection.ExecuteSPQuery("pr_savetestresponse_lkp_getsavedtest", paramList.ToArray());
        }

        public DataSet CheckPendingEssays(string userID, int testTakenID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pUserID", SqlDbType.VarChar, userID));
            paramList.Add(Helper.CreateParam("@pTestTakenID", SqlDbType.Int, testTakenID));
            return Connection.ExecuteSPQuery("pr_savetestresponse_lkp_checkpendingessays", paramList.ToArray());
        }

        public void Delete(int saveTestResponseID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pSaveTestResponseID", SqlDbType.Int, saveTestResponseID));
            Connection.ExecuteSP("pr_savetestresponse_sav_delete", paramList.ToArray());
        }

        public DataSet GetEssays()
        {
            return Connection.ExecuteSPQuery("pr_savetestresponse_lkp_getessays");
        }

        public DataSet GetByID(int saveTestResponseID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pSaveTestResponseID", SqlDbType.Int, saveTestResponseID));
            return Connection.ExecuteSPQuery("pr_savetestresponse_lkp_getbyid", paramList.ToArray());
        }

        public DataSet GetByTestTaken(int testTakenID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pTestTakenID", SqlDbType.Int, testTakenID));
            return Connection.ExecuteSPQuery("pr_savetestresponse_lkp_getbytesttaken", paramList.ToArray());
        }

        public DataSet GetMultipleEssays(int testTakenID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@pTestTakenID", SqlDbType.Int, testTakenID));
            return Connection.ExecuteSPQuery("pr_savetestresponse_lkp_getmultipleessays", paramList.ToArray());
        }

        public DataSet GetPendingExamAnswerStatus(int testTakenID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@TestTakenID", SqlDbType.Int, testTakenID));
            return Connection.ExecuteSPQuery("pr_savetestresponse_lkp_getpendingexamanswerstatus", paramList.ToArray());
        }
    }
}
