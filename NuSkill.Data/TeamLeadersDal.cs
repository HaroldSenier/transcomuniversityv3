using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using TheLibrary.DBImportTool;

namespace NuSkill.Data
{
    public class TeamLeadersDal : Base
    {
        public TeamLeadersDal()
        {
        }

        public TeamLeadersDal(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        public DataSet GetTeamLeaders()
        {
            return Connection.ExecuteSPQuery("pr_TimeTable_Lkp_TeamLeaders");
        }
    }
}
