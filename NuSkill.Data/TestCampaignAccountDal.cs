using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using TheLibrary.DBImportTool;

namespace NuSkill.Data
{
    public class TestCampaignAccountDal : TheLibrary.DBImportTool.Base
    {
        public TestCampaignAccountDal()
        {
            //this.ConnectionString = Properties.Settings.Default.LocalConnectionString;
        }

        public TestCampaignAccountDal(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        public DataSet SelectAll()
        {
            return Connection.ExecuteSPQuery("pr_testcampaignaccount_lkp_selectall");
        }

        public DataSet Select(int testCategoryID, int campaignAccountID, bool isCampaign)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@TestCategoryID", SqlDbType.Int, testCategoryID));
            paramList.Add(Helper.CreateParam("@CampaignAccountID", SqlDbType.Int, campaignAccountID));
            paramList.Add(Helper.CreateParam("@IsCampaign", SqlDbType.Bit, isCampaign));
            return Connection.ExecuteSPQuery("pr_testcampaignaccount_lkp_getsingle", paramList.ToArray());
        }

        public DataSet SelectByTestCategory(int testCategoryID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@TestCategoryID", SqlDbType.Int, testCategoryID));
            return Connection.ExecuteSPQuery("pr_testcampaignaccount_lkp_selectbytestcategory", paramList.ToArray());
        }

        public DataSet SelectByCampaign(int campaignID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@CampaignID", SqlDbType.Int, campaignID));
            return Connection.ExecuteSPQuery("pr_testcampaignaccount_lkp_selectbycampaign", paramList.ToArray());
        }

        public DataSet SelectByAccount(int accountID)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@AccountID", SqlDbType.Int, accountID));
            return Connection.ExecuteSPQuery("pr_testcampaignaccount_lkp_selectbyaccount", paramList.ToArray());
        }

        public void Insert(int testCategoryID, int campaignAccountID, bool isCampaign)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@TestCategoryID", SqlDbType.Int, testCategoryID));
            paramList.Add(Helper.CreateParam("@CampaignAccountID", SqlDbType.Int, campaignAccountID));
            paramList.Add(Helper.CreateParam("@IsCampaign", SqlDbType.Bit, isCampaign));
            Connection.ExecuteSP("pr_testcampaignaccount_sav_insert", paramList.ToArray());
        }

        public void Update(int testCategoryID, int campaignAccountID, bool isCampaign)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@TestCategoryID", SqlDbType.Int, testCategoryID));
            paramList.Add(Helper.CreateParam("@CampaignAccountID", SqlDbType.Int, campaignAccountID));
            paramList.Add(Helper.CreateParam("@IsCampaign", SqlDbType.Bit, isCampaign));
            Connection.ExecuteSP("pr_testcampaignaccount_sav_update", paramList.ToArray());
        }
    }
}
