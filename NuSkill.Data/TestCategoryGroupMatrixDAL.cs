using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using TheLibrary.DBImportTool;

namespace NuSkill.Data
{
    public class TestCategoryGroupMatrixDAL : TheLibrary.DBImportTool.Base
    {
        public TestCategoryGroupMatrixDAL()
        {
            //this.ConnectionString = Properties.Settings.Default.LocalConnectionString;
        }

        public TestCategoryGroupMatrixDAL(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        public DataSet SelectAll()
        {
            return Connection.ExecuteSPQuery("pr_testingCategoryGroupMatrix_lkp_SelectAll");
        }

        public DataSet SelectByCategory(int testCategoryID)
        {
            return Connection.ExecuteSPQuery("pr_testingCategoryGroupMatrix_lkp_selectByCategory",
                Helper.CreateParam("@TestCategoryID", SqlDbType.Int, testCategoryID));
        }

        public DataSet SelectByGroup(int testGroupID)
        {
            return Connection.ExecuteSPQuery("pr_testingCategoryGroupMatrix_lkp_selectByGroup",
                Helper.CreateParam("@TestGroupID", SqlDbType.Int, testGroupID));
        }

        public void DeleteSingle(int testCategoryID, int testGroupID)
        {
            Connection.ExecuteSP("pr_testingCategoryGroupMatrix_lkp_deletesingle",
                Helper.CreateParam("@TestCategoryID", SqlDbType.Int, testGroupID),
                Helper.CreateParam("@TestGroupID", SqlDbType.Int, testGroupID));
        }

        public void DeleteByCategory(int testCategoryID)
        {
            Connection.ExecuteSP("pr_testCategoryGroupMatrix_sav_deleteByCategory",
                Helper.CreateParam("@TestCategoryID", SqlDbType.Int, testCategoryID));
        }

        public void DeleteByGroup(int testGroupID)
        {
            Connection.ExecuteSP("pr_testCategoryGroupMatrix_sav_deleteByGroup",
                Helper.CreateParam("@TestGroupID", SqlDbType.Int, testGroupID));
        }

        public void Insert(int testCategoryID, int testGroupID)
        {
            Connection.ExecuteSP("pr_testingCategoryGroupMatrix_sav_Insert",
                Helper.CreateParam("@TestCategoryID", SqlDbType.Int, testCategoryID),
                Helper.CreateParam("@TestGroupID", SqlDbType.Int, testGroupID));
        }

        public DataSet SelectAllGroupByCategory(int testCategoryID)
        {
            return Connection.ExecuteSPQuery("pr_testingCategoryGroupMatrix_lkp_selectallgroupbycategory",
                 Helper.CreateParam("@TestCategoryID", SqlDbType.Int, testCategoryID));
        }


    }
}
