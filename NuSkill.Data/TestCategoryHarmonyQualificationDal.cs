using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using TheLibrary.DBImportTool;

namespace NuSkill.Data
{
    public class TestCategoryHarmonyQualificationDal : Base
    {
        public TestCategoryHarmonyQualificationDal()
        { }

        public TestCategoryHarmonyQualificationDal(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        public void Insert(int testCategoryID, int harmonyQualificationID)
        {
            Connection.ExecuteSP("pr_testcategoryharmonyqualificationmatrix_sav_insert",
                Helper.CreateParam("@TestCategoryID", SqlDbType.Int, testCategoryID),
                Helper.CreateParam("@HarmonyQualificationID", SqlDbType.Int, harmonyQualificationID));
        }

        public void Delete(int testCategoryHarmonyQualificationMatrixID)
        {
            Connection.ExecuteSP("pr_testcategoryharmonyqualificationmatrix_sav_delete",
                Helper.CreateParam("@TestCategoryHarmonyQualificationID", SqlDbType.Int, testCategoryHarmonyQualificationMatrixID));
        }

        public DataSet SelectAll()
        {
            return Connection.ExecuteSPQuery("pr_testcategoryharmonyqualificationmatrix_lkp_selectall");
        }

    }
}
