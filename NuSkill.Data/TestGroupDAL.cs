using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using TheLibrary.DBImportTool;

namespace NuSkill.Data
{
    public class TestGroupDAL : TheLibrary.DBImportTool.Base
    {
        public TestGroupDAL()
        {
            //this.ConnectionString = Properties.Settings.Default.LocalConnectionString;
        }

        public TestGroupDAL(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        public DataSet Select(int testGroupID)
        {
            return Connection.ExecuteSPQuery("pr_testGroup_lkp_Select",
                Helper.CreateParam("@TestGroupID", SqlDbType.Int, testGroupID));
        }

        public DataSet SelectAll()
        {
            return Connection.ExecuteSPQuery("pr_testGroup_lkp_SelectAll");
        }

        public void Delete(int testGroupID)
        {
            Connection.ExecuteSP("pr_testGroup_sav_Delete",
                Helper.CreateParam("@TestGroupID", SqlDbType.Int, testGroupID));
        }

        public int Insert(string testGroupName, string createdBy)
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(Helper.CreateParam("@TestGroupName", SqlDbType.VarChar, testGroupName));
            paramList.Add(Helper.CreateParam("@CreatedBy", SqlDbType.VarChar, createdBy));
            return Convert.ToInt32(Connection.ExecuteScalar("pr_testGroup_sav_Insert", paramList.ToArray()));
        }
    }
}
