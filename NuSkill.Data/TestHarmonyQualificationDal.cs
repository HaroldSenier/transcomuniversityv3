using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using TheLibrary.DBImportTool;

namespace NuSkill.Data
{
    public class TestHarmonyQualificationDal : Base
    {
        public TestHarmonyQualificationDal()
        { }

        public TestHarmonyQualificationDal(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        public DataSet SelectAll()
        {
            return Connection.ExecuteSPQuery("pr_TestQualification_SelectAll");
        }

        public DataSet SelectByTestCategory(int testCategoryID)
        {
            return Connection.ExecuteSPQuery("pr_TestQualification_Select",
                Helper.CreateParam("@TestCategoryID", SqlDbType.Int, testCategoryID));
        }

        public void Insert(int testCategoryID, int harmonyQualificationID)
        {
            Connection.ExecuteSP("pr_TestQualification_Insert",
                Helper.CreateParam("@TestCategoryID", SqlDbType.Int, testCategoryID),
                Helper.CreateParam("@HarmonyQualificationID", SqlDbType.Int, harmonyQualificationID));
        }

        public void InsertToVader2(int qualificationID, DateTime dateAchieved, DateTime expiryDate, int instructorCimID, string score, int employeeID, int employeeLoggedIn, int passed, string comments, string licenseNumber)
        {
            Connection.ExecuteSP("pr_Personnel_Sav_EmployeeQualification",
                Helper.CreateParam("@QualificationID", SqlDbType.Int, qualificationID),
                Helper.CreateParam("@DateAchieved", SqlDbType.DateTime, dateAchieved),
                Helper.CreateParam("@ExpiryDate", SqlDbType.Date, expiryDate),
                Helper.CreateParam("@InstructorCIMID", SqlDbType.Int, instructorCimID),
                Helper.CreateParam("@Score", SqlDbType.VarBinary, score),
                Helper.CreateParam("@EmployeeID", SqlDbType.Int, employeeID),
                Helper.CreateParam("@EmployeeLoggedIn", SqlDbType.Int, employeeLoggedIn),
                Helper.CreateParam("@Passed", SqlDbType.Int, passed),
                Helper.CreateParam("@Comments", SqlDbType.VarChar, comments),
                Helper.CreateParam("@LicenseNumber", SqlDbType.VarChar, licenseNumber));
        }
    }
}
