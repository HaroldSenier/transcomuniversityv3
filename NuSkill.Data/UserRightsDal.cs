using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using TheLibrary.DBImportTool;

namespace NuSkill.Data
{
    public class UserRightsDal : TheLibrary.DBImportTool.Base
    {
        public UserRightsDal()
        {
            //this.ConnectionString = NuSkill.Data.Properties.Settings.Default.LocalConnectionString;
        }

        public UserRightsDal(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        public void Insert(string username, string rights, int rightsLevel)
        {
            Connection.ExecuteSP("pr_userrights_sav_insert",
                Helper.CreateParam("@pUsername", SqlDbType.VarChar, username),
                Helper.CreateParam("@pRights", SqlDbType.VarChar, rights),
                Helper.CreateParam("@pRightsLevel", SqlDbType.Int, rightsLevel));
        }

        public DataSet Select(string username, int rightsLevel)
        {
            return Connection.ExecuteSPQuery("pr_userrights_lkp_select",
                Helper.CreateParam("@pUsername", SqlDbType.VarChar, username),
                Helper.CreateParam("@pRightsLevel", SqlDbType.Int, rightsLevel));
        }

        public DataSet SelectAllByUser(string username)
        {
            return Connection.ExecuteSPQuery("pr_userrights_lkp_selectallbyuser",
                Helper.CreateParam("@pUsername", SqlDbType.VarChar, username));
        }

        public DataSet SelectAll()
        {
            return Connection.ExecuteSPQuery("pr_userrights_lkp_selectall");
        }

        public void CopyRights(int copyfrom, int copyTo)
        {
            Connection.ExecuteSP("pr_userrights_sav_copyrights",
                Helper.CreateParam("@pCopyFrom", SqlDbType.Int, copyfrom),
                Helper.CreateParam("@pCopyTo", SqlDbType.Int, copyTo));
        }

        public void DeleteRights(int userrightsID)
        {
            Connection.ExecuteSP("pr_userrights_sav_delete",
                Helper.CreateParam("@pUserRightsID", SqlDbType.Int, userrightsID));
        }

        public void EditRights(int userrightsID, string rights)
        {
            Connection.ExecuteSP("pr_userrrights_sav_update",
                Helper.CreateParam("@pUserRightsID", SqlDbType.Int, userrightsID),
                Helper.CreateParam("@pRights", SqlDbType.VarChar, rights));
        }

        public DataSet GetRoles()
        {
            this.ConnectionString = Properties.Settings.Default.Vader4CIMEnterpriseConnectionString;
            return Connection.ExecuteTextQuery("SELECT RoleID, CAST(A.RoleID as varchar) + ' - ' +  A.Role + ' - ' + B.CompanyGroup AS 'Role' FROM [dbo].[tbl_Personnel_Cor_Role] A INNER JOIN [dbo].[tbl_Personnel_Lkp_CompanyGroup] B ON A.CompanyGroupID = B.CompanyGroupID");
        }
    }
}
