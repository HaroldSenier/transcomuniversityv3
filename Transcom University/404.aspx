﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TranscomUniversityV3.Master" AutoEventWireup="true" CodeFile="404.aspx.cs" Inherits="_404" %>

<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolderMain" Runat="Server">
<div class="container text-center">
    <h1><i class="fa fa-warning" style="font-size:50px;color:#ff5046"></i><asp:Label runat="server" Text="<%$ Resources: LocalizedResource, 404PageNotFound %>" /></h1>
    <span><asp:Label runat="server" Text="<%$ Resources: LocalizedResource, WecouldnotfindthepageyouwerelookingforMeanwhileyoumayreturntoour %>" /> <asp:LinkButton runat="server" PostBackUrl="Home.aspx" Text="<%$ Resources: LocalizedResource, HOMEPAGE %>" style="color:#ff5046"></asp:LinkButton></span>
</div>
</asp:Content>

