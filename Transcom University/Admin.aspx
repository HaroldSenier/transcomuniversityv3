﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TranscomUniversityV3.Master" AutoEventWireup="true"
    CodeFile="Admin.aspx.cs" Inherits="Admin" %>

<%@ Register Src="UserControl/Pages/CoursePreviewUserCtrl.ascx" TagName="CoursePreviewUserCtrl"
    TagPrefix="uc1" %>
<%@ Register Src="UserControl/Pages/AdminCoursesForumUserCtrl.ascx" TagName="CourseForumUserCtrl"
    TagPrefix="ucf" %>
<%@ Register Src="UserControl/Pages/AdminForumTopics.ascx" TagName="ForumTopicsUserCtrl"
    TagPrefix="ucft" %>
<%@ Register Src="UserControl/Pages/AdminForumAddNewTopic.ascx" TagName="ForumAddTopic"
    TagPrefix="ucat" %>
<%@ Register Src="UserControl/Pages/ForumThreadUserCtrl.ascx" TagName="ForumThread"
    TagPrefix="uctd" %>
<%@ Register Src="UserControl/Pages/MandatoryCourseSettings.ascx" TagName="pageEditorMandatoryCourse"
    TagPrefix="ucMandatory" %>
<%@ Register Src="UserControl/Pages/TrendingCourseSettings.ascx" TagName="pageEditorTrendingCourse"
    TagPrefix="ucTrending" %>
<%@ Register Src="UserControl/Pages/RecommendedCourseSettings.ascx" TagName="pageEditorRecommendedCourse"
    TagPrefix="ucRecommended" %>
<%@ Register Src="UserControl/Pages/MustTakeCourseSettings.ascx" TagName="pageEditorMustTakeCourse"
    TagPrefix="ucMustTake" %>
<%@ Register Src="UserControl/Pages/RecentlyAddedCourseSettings.ascx" TagName="pageEditorRecentlyAddedCourse"
    TagPrefix="ucRecentlyAdded" %>
<%@ Register Src="UserControl/Pages/HomepageEditor.ascx" TagName="pageEditor" TagPrefix="ucpe" %>
<%@ Register Src="UserControl/Pages/ImageSliderSettings.ascx" TagName="imageSliderSettings"
    TagPrefix="ucis" %>
<%@ Register Src="UserControl/Pages/FeaturedCoursesUserCtrl.ascx" TagName="featuredCoursesSettings"
    TagPrefix="ucfc" %>
<%@ Register Src="UserControl/Pages/NewsUserCtrl.ascx" TagName="newsSettings" TagPrefix="ucns" %>
<%@ Register Src="UserControl/Pages/TestimonialSettingUserCtrl.ascx" TagName="testimonialSettings"
    TagPrefix="ucts" %>
<%@ Register Src="UserControl/Pages/ClassManagement.ascx" TagName="classManagement"
    TagPrefix="uccm" %>
<%@ Register Src="UserControl/Pages/CourseManagement.ascx" TagName="courseManagement"
    TagPrefix="ucmg" %>
<asp:Content ID="Content1" ContentPlaceHolderID="contentPlaceHolderLeftPanel" runat="server">
    <rad:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Transparency="25"
        IsSticky="true" CssClass="Loading" />
    <rad:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel2" runat="server" MinDisplayTime="1000"
        CssClass="Loading2" Transparency="25" />
    <rad:RadAjaxLoadingPanel ID="noLoadingPanel" runat="server" CssClass="Loading-Empty"
        Transparency="25" />
     <rad:RadAjaxLoadingPanel ID="fullPageLoading" runat="server" Transparency="25" IsSticky="true"
        CssClass="Loading" />
    <rad:RadWindowManager ID="RadWindowManager1" RenderMode="Lightweight" EnableShadow="true"
        Skin="Bootstrap" Modal="true" VisibleOnPageLoad="false" Behaviors="Close, Move"
        DestroyOnClose="true" RestrictionZoneID="RestrictionZone" Opacity="99" runat="server"
        Width="800" Height="400px">
        <Windows>
            <rad:RadWindow ID="rwAddCategories" runat="server" RenderMode="Lightweight" Modal="true"
                Skin="Bootstrap" Behaviors="Move, Close" CssClass="tc-radwindow-1 height-inherit"
                Height="250px" Width="450px" VisibleStatusbar="false" Title="Add Category">
                <ContentTemplate>
                    <br />
                    <asp:Panel ID="pnlAddCategory" runat="server">
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td>
                                        Category Name :
                                    </td>
                                    <td>
                                        <rad:RadTextBox ID="txtCategoryName" runat="server" RenderMode="Lightweight" EmptyMessage="- Category Name -"
                                            MaxLength="50">
                                        </rad:RadTextBox>
                                        <asp:RequiredFieldValidator ID="rfvCategoryName" runat="server" ErrorMessage="*"
                                            ControlToValidate="txtCategoryName" SetFocusOnError="True" Display="Dynamic"
                                            ValidationGroup="addCategory" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Description :
                                    </td>
                                    <td>
                                        <rad:RadTextBox ID="txtCategoryDesc" runat="server" RenderMode="Lightweight" EmptyMessage="- Description -"
                                            TextMode="MultiLine">
                                        </rad:RadTextBox>
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot class="footer-bottom">
                                <tr>
                                    <td style="padding-top: 10px;">
                                        <asp:LinkButton ID="btnAddCategory" runat="server" OnClick="btnAddCategory_Click"
                                            ToolTip="Add Category" Text="Add" CssClass="btn tc-btn-md btn-teal btn-flat pull-right"
                                            ValidationGroup="addCategory">
                                        </asp:LinkButton>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </asp:Panel>
                    <br />
                </ContentTemplate>
            </rad:RadWindow>
        </Windows>
        <Windows>
            <rad:RadWindow ID="rwDeleteCategories" runat="server" RenderMode="Lightweight" Modal="true"
                Skin="Bootstrap" Behaviors="Move, Close" CssClass="tc-radwindow-1 height-inherit"
                Height="450px" Width="850px" VisibleStatusbar="false" Title="Delete Category">
                <ContentTemplate>
                    <asp:Panel ID="pnlDelCategory" runat="server">
                        <rad:RadGrid ID="gvDeleteCategory" runat="server" RenderMode="Lightweight" OnNeedDataSource="gvDeleteCategory_NeedDataSource"
                            OnItemCommand="gvDeleteCategory_ItemCommand" AutoGenerateColumns="false" Height="370px"
                            PageSize="5" CssClass="GridLess">
                            <PagerStyle Mode="NextPrev" AlwaysVisible="true" />
                            <ClientSettings EnableAlternatingItems="true" AllowColumnsReorder="true" EnableRowHoverStyle="true">
                                <Selecting AllowRowSelect="true"></Selecting>
                                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                <Resizing AllowColumnResize="true" ResizeGridOnColumnResize="true" AllowResizeToFit="true" />
                            </ClientSettings>
                            <ItemStyle Wrap="false"></ItemStyle>
                            <MasterTableView DataKeyNames="CategoryID" AllowSorting="true" AllowPaging="true"
                                CommandItemDisplay="None" HeaderStyle-ForeColor="Teal">
                                <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                <Columns>
                                    <rad:GridButtonColumn ConfirmDialogType="RadWindow" ConfirmTitle="Delete Record"
                                        ButtonType="ImageButton" CommandName="Delete" Text="Delete Record" UniqueName="DeleteColumn"
                                        ConfirmText="Are you sure you want to delete this record?" ImageUrl="~/Media/Images/deletesmall.png"
                                        ConfirmDialogHeight="180" ConfirmDialogWidth="330" HeaderStyle-Width="30">
                                    </rad:GridButtonColumn>
                                    <rad:GridBoundColumn UniqueName="Category" HeaderText="CATEGORY" DataField="Category"
                                        ReadOnly="true">
                                    </rad:GridBoundColumn>
                                    <rad:GridBoundColumn UniqueName="Description" HeaderText="DESCRIPTION" DataField="Description"
                                        ReadOnly="true">
                                    </rad:GridBoundColumn>
                                    <rad:GridBoundColumn UniqueName="Subcategories" HeaderText="SUBCATEGORIES" DataField="Subcategories"
                                        ReadOnly="true">
                                    </rad:GridBoundColumn>
                                    <rad:GridBoundColumn UniqueName="Courses" HeaderText="COURSES" DataField="Courses"
                                        ReadOnly="true">
                                    </rad:GridBoundColumn>
                                    <rad:GridBoundColumn UniqueName="CreatedBy" HeaderText="UPDATED BY" DataField="CreatedBy"
                                        ReadOnly="true">
                                    </rad:GridBoundColumn>
                                    <rad:GridBoundColumn UniqueName="DateTimeCreated" HeaderText="DATE UPDATED" DataField="DateTimeCreated"
                                        ReadOnly="true">
                                    </rad:GridBoundColumn>
                                </Columns>
                            </MasterTableView>
                        </rad:RadGrid>
                    </asp:Panel>
                </ContentTemplate>
            </rad:RadWindow>
        </Windows>
    </rad:RadWindowManager>
    <rad:RadCodeBlock ID="rcbAdmin1" runat="server">
        <script type="text/javascript">
            function keyPress(sender, args) {
                var text = sender.get_value() + args.get_keyCharacter();
                if (!text.match('^[0-9]+$'))
                    args.set_cancel(true);
            }

            function RequestStart(sender, args) {
                // the following Javascript code takes care of expanding the RadAjaxLoadingPanel
                // to the full height of the page, if it is more than the browser window viewport

                var loadingPanel = $get("<%= RadAjaxLoadingPanel1.ClientID %>");
                var pageHeight = document.documentElement.scrollHeight;
                var viewportHeight = document.documentElement.clientHeight;

                if (pageHeight > viewportHeight) {
                    loadingPanel.style.height = pageHeight + "px";
                }

                var pageWidth = document.documentElement.scrollWidth;
                var viewportWidth = document.documentElement.clientWidth;

                if (pageWidth > viewportWidth) {
                    loadingPanel.style.width = pageWidth + "px";
                }

                // the following Javascript code takes care of centering the RadAjaxLoadingPanel
                // background image, taking into consideration the scroll offset of the page content

                if ($telerik.isSafari) {
                    var scrollTopOffset = document.body.scrollTop;
                    var scrollLeftOffset = document.body.scrollLeft;
                }
                else {
                    var scrollTopOffset = document.documentElement.scrollTop;
                    var scrollLeftOffset = document.documentElement.scrollLeft;
                }

                var loadingImageWidth = 55;
                var loadingImageHeight = 55;

                loadingPanel.style.backgroundPosition = (parseInt(scrollLeftOffset) + parseInt(viewportWidth / 2) - parseInt(loadingImageWidth / 2)) + "px " + (parseInt(scrollTopOffset) + parseInt(viewportHeight / 2) - parseInt(loadingImageHeight / 2)) + "px";

                //hide course preview
            }

            function pageLoad() {
                $telerik.$('.ruFileInput').attr('accept', 'image/*');
                highlightSelectedSubmenu();


            }

      

            function highlightSelectedSubmenu() {

                var tab = '<%= Request.QueryString["Tab"] %>';
                var selectedTab = "#tab" + tab;
                console.log(selectedTab);
                $(selectedTab).addClass("active");
                if (!$(selectedTab).hasClass("submenu-title")) {
                    $(selectedTab).parent().parent().find("a.submenu-title").addClass("active");
                }


            }

            function tabClick(el) {

                var caret = $(el).find("i.js-collapse-event-2");

                if ($(caret).hasClass("fa-sort-up")) {
                    $(caret).removeClass("fa-sort-up").addClass("fa-sort-down");
                } else {
                    $(caret).removeClass("fa-sort-down").addClass("fa-sort-up");
                }

            }

            function showMenu(e) {
                var contextMenu = $find("<%= rcmCourseCategory.ClientID %>");

                if ((!e.relatedTarget) || (!$telerik.isDescendantOrSelf(contextMenu.get_element(), e.relatedTarget))) {
                    contextMenu.show(e);
                }

                $telerik.cancelRawEvent(e);
            }

            function addCategories() {
                var radwindow = $find('<%=rwAddCategories.ClientID %>');

                radwindow.show();
            }

            function deleteCategories() {
                var radwindow = $find('<%=rwDeleteCategories.ClientID %>');

                radwindow.show();
            }
       


            function OnClientEntryAdding1(sender, args) {
                if (sender.get_entries().get_count() > 0) {
                    sender.get_entries().clear();
                }
            }



            function showPanel(panelId) {
                var panel = $("#" + panelId + "");
                panel.show();
            }

            function hidePanel(panelId) {
                var panel = $("#" + panelId + "");
                panel.hide();
            }

            function toggleErrorModal(modalId) {
                $("#" + modalId + "").modal("toggle");
            }

            function OnClientBlurHandler(sender, eventArgs) {
                var textInTheCombo = sender.get_text();
                var item = sender.findItemByText(textInTheCombo);
                //if there is no item with that text
                if (!item) {
                    sender.set_text("");
                    setTimeout(function () {
                        var inputElement = sender.get_inputDomElement();
                        inputElement.focus();
                        var empty = sender.get_emptyMessage();
                        sender.set_text(empty);
                        //inputElement.style.backgroundColor = "red";
                    }, 20);
                }
            }
            function showPageLoading() {
                console.log("page loading");
                var panel = $find("<%= fullPageLoading.ClientID %>");
                panel.show("ulBreadcrumb");
            }

            function hidePageLoading() {
                console.log("hide page loading");
                var panel = $find("<%= fullPageLoading.ClientID %>");
                panel.hide("ulBreadcrumb");
            }

            $(document).ready(function () {
                $(window).scroll(function () {
                    sidebarFull();
                });
            })
        
        </script>
    </rad:RadCodeBlock>
    
    <rad:RadAjaxManager ID="RadAjaxManager1" runat="server" DefaultLoadingPanelID="RadAjaxLoadingPanel1">
        <ClientEvents OnRequestStart="RequestStart" />
        <AjaxSettings>
            <rad:AjaxSetting AjaxControlID="btnCourseMngt">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" UpdatePanelRenderMode="Inline" />
                </UpdatedControls>
            </rad:AjaxSetting>         
            <rad:AjaxSetting AjaxControlID="gridMandatoryCourses">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="gridMandatoryCourses" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="gridTrendingCourses">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="gridTrendingCourses" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="gridRecentlyAddedCourses">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="gridRecentlyAddedCourses" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnSwitchCatListView">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                    <rad:AjaxUpdatedControl ControlID="ulBreadcrumb" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnSwitchCatGridView">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                    <rad:AjaxUpdatedControl ControlID="ulBreadcrumb" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="rdpCourseCategory">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="gvCourseCategory">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="gvCourseCategory" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="rcmCourseCategory">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnAddCategory">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlAddCategory" LoadingPanelID="RadAjaxLoadingPanel2" />
                    <rad:AjaxUpdatedControl ControlID="lvCourseCategory" />
                    <rad:AjaxUpdatedControl ControlID="gvCourseCategory" />
                    <rad:AjaxUpdatedControl ControlID="ltCourseCategory" UpdatePanelRenderMode="Inline" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnCatDelete">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="gvDeleteCategory">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlDelCategory" LoadingPanelID="RadAjaxLoadingPanel2" />
                    <rad:AjaxUpdatedControl ControlID="lvCourseCategory" />
                    <rad:AjaxUpdatedControl ControlID="gvCourseCategory" />
                    <rad:AjaxUpdatedControl ControlID="ltCourseCategory" UpdatePanelRenderMode="Inline" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="gridCourseForum">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="gridCourseForum" LoadingPanelID="RadAjaxLoadingPanel2" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="gvClassNewHire">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="gvClassNewHire" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="gvClassCrossTraining">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="gvClassCrossTraining" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="gvClassRefresherTraining">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="gvClassRefresherTraining" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="gvClassUpTraining">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="gvClassUpTraining" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="gvClassProfessionalDevelopment">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="gvClassProfessionalDevelopment" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnViewAllNH">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnViewAllCT">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnTopicNew">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </rad:AjaxSetting>            
            <rad:AjaxSetting AjaxControlID="btnViewAllRT">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnViewAllUT">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnViewAllPD">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnViewLessNH">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnViewLessCT">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnViewLessRT">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnViewLessUT">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnViewLessPD">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="ddlTrainingType">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="ddlStatus">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <%--  <rad:AjaxSetting AjaxControlID="CourseImage">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="CourseImage" />
                    <rad:AjaxUpdatedControl ControlID="DropzonePreview" LoadingPanelID="RadAjaxLoadingPanel2" />
                </UpdatedControls>
            </rad:AjaxSetting>--%>
            <rad:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="CourseImage" LoadingPanelID="noLoadingPanel" />
                    <rad:AjaxUpdatedControl ControlID="courseImagePreview" LoadingPanelID="localLoadingPanel" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnRemovePreviewImg">
                <UpdatedControls>
                    <%--<rad:AjaxUpdatedControl ControlID="CourseImage" LoadingPanelID="noLoadingPanel" />--%>
                    <rad:AjaxUpdatedControl ControlID="courseImagePreview" LoadingPanelID="localLoadingPanel" />
                </UpdatedControls>
            </rad:AjaxSetting>
           <rad:AjaxSetting AjaxControlID="btnAddTopic">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="RadMultiPage1"  LoadingPanelID="fullPageLoading" />
            </UpdatedControls>    
        </rad:AjaxSetting>
        </AjaxSettings>
    </rad:RadAjaxManager>
    <div class="sidebar-relative">
        <div class="side-menu-container">
            <div class="overlay">
            </div>
            <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
                <ul class="nav sidebar-nav">
                    <li class="sidebar-brand" style="z-index: 1000; color: #fff;">
                        <a href="#" class="close"><i class="fa fa-times" aria-hidden="true"></i></a> Admin Task Menu
                    </li>
                    <rad:RadTabStrip ID="RadTabStrip1" runat="server" RenderMode="Lightweight" SelectedIndex="0" Skin="Black"><%--OnClientTabSelected="onClientTabSelected"--%>
                        <Tabs>
                            <rad:RadTab runat="server" Text="Course Management" Value="1" NavigateUrl="Admin.aspx?Tab=CourseMgmt" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                            <rad:RadTab runat="server" Text="Create a Course" Value="2" NavigateUrl="CreateCourse.aspx?" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                            <rad:RadTab runat="server" Text="Course Categories" Value="3" NavigateUrl="Admin.aspx?Tab=CourseCat" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                            <rad:RadTab runat="server" Text="Class Management" Value="4" NavigateUrl="ClassManagement.aspx?" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                            <rad:RadTab runat="server" Text="Create a Class" Value="5" NavigateUrl="ClassBuilder.aspx?" Width="285px" SelectedCssClass="rtsSelected" >
                            </rad:RadTab>
                            <rad:RadTab runat="server" Text="Page Editor" Value="6"  Width="285px" SelectedCssClass="rtsSelected" CssClass="toggle-submenu"  PostBack="false">
                                 <TabTemplate>
                                  <ul class="nav custom-submenu panel-title" id="menuPageEditor">
		                            <li>
                                        <a class="tab-title" >Page Editor
                                         <i class="fa fa-sort-down pull-right hover-pointer js-collapse-event-2" data-toggle="collapse" data-target="#pageEditorMenu" aria-expanded="false" onclick="tabClick(this);"></i>
                                        </a>
			                            <ul class="nav collapse submenu" id="pageEditorMenu" role="menu">
				                            <li id="tabCourseLogins">
                                                <a id="tabHomepageEditor" href="Admin.aspx?Tab=HomepageEditor" class="submenu-title">Homepage Layout
                                                <i class="fa fa-sort-down pull-right hover-pointer js-collapse-event-2"  data-toggle="collapse" data-target="#homepageMenu" aria-expanded="false" onclick="tabClick(this);"></i>
                                                </a>
                                                 
                                                <ul class="nav collapse submenu" id="homepageMenu" role="menu" >
				                                    <li id="tabImageSlider"><a href="Admin.aspx?Tab=ImageSlider">Image Slider</a></li>
                                                    <li id="tabFeaturedCourses"><a href="Admin.aspx?Tab=FeaturedCourses">Featured Courses</a></li>
                                                    <li id="tabLatestNews"><a href="Admin.aspx?Tab=LatestNews">Latest News</a></li>
                                                    <li id="tabTestimonials"><a href="Admin.aspx?Tab=Testimonials">Testimonials</a></li>
			                                    </ul>
                                            </li>
				                             <li id="Li5">
                                                <a class="submenu-title" data-toggle="collapse" data-target="#courseCatalogMenu" aria-expanded="false" onclick="tabClick(this);">Course Catalog
                                                <i class="fa fa-sort-down pull-right hover-pointer js-collapse-event-2"></i>
                                                </a>
                                                <ul class="nav collapse submenu" id="courseCatalogMenu" role="menu" >
				                                    <li id="tabMandatoryCourses"><a href="Admin.aspx?Tab=MandatoryCourses">Mandatory Courses</a></li>
                                                    <li id="tabTrendingCourses"><a href="Admin.aspx?Tab=TrendingCourses">Trending Courses</a></li>
                                                    <li id="tabRecommendedCourses"><a href="Admin.aspx?Tab=RecommendedCourses">Recommended Courses</a></li>
                                                    <li id="tabMustTakeCourses"><a href="Admin.aspx?Tab=MustTakeCourses">Must Take Courses</a></li>
                                                    <li id="tabRecentCourses"><a href="Admin.aspx?Tab=RecentCourses">Recently Added Courses</a></li>
			                                    </ul>
                                            </li>
			                            </ul>
		                            </li>
	                            </ul>
                                </TabTemplate>
                            </rad:RadTab>
                            <rad:RadTab runat="server" Text="User Management" Value="7" NavigateUrl="Admin.aspx?Tab=UserMgmt" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                            <rad:RadTab runat="server" Text="Forums" Value="8" NavigateUrl="Admin.aspx?Tab=AdminForum" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                        </Tabs>
                    </rad:RadTabStrip>
                </ul>
            </nav>
            <div class="hamburger-container text-center" style="font-size: 20px; color: #fff;
                margin-top: 10px">
                <a href="#" class="open-nav is-closed animated fadeInLeft"><i class="fa fa-bars"
                    aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolderMain" runat="server">
    <div class="col-sm-11 col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12 thirty-px-padding">
                        <div class="col-md-12 no-paddings">
                            <ul id="ulBreadcrumb" runat="server" class="breadcrumb">
                                <li><a>
                                    <asp:Label ID="lblTab1" runat="server" Text="Admin Dashboard" /></a></li>
                                <li><a>
                                    <asp:Label ID="lblTab2" runat="server" Text="" /></a></li>
                                <li id="item3" runat="server" visible="false"><a>
                                    <asp:Label ID="lblTab3" runat="server" Text="" ClientIDMode="Static" /></a></li>
                                <li id="item4" runat="server" visible="false"><a>
                                    <asp:Label ID="lblTab4" runat="server" Text="" /></a></li>
                            </ul>
                        </div>
                        <rad:RadMultiPage ID="RadMultiPage1" runat="server" RenderMode="Lightweight" SelectedIndex="0"
                            CssClass="outerMultiPage" RenderSelectedPageOnly="true">
                            <rad:RadPageView ID="rdpCourseManagement" runat="server">
                                <ucmg:courseManagement ID="CourseManagement1" runat="server" />
                            </rad:RadPageView>
                            <rad:RadPageView ID="rdpCreateCourse" runat="server">
                             
                            </rad:RadPageView>
                            <rad:RadPageView ID="rdpCourseCategories" runat="server">
                                <br />
                                <br />
                                <br />
                                <div class="row">
                                    <div class="container" style="text-align:right;">
                                        <asp:LinkButton ID="btnSwitchCatListView" runat="server" OnClick="btnSwitchCatListView_Click"
                                            Font-Underline="false" ForeColor="Black">
                                            <i id="iCatListView" runat="server" class="fa fa-th-list" style="margin-right: 15px"
                                                aria-hidden="true" title="List View"></i>
                                        </asp:LinkButton>
                                        <asp:LinkButton ID="btnSwitchCatGridView" runat="server" OnClick="btnSwitchCatGridView_Click"
                                            Font-Underline="false" ForeColor="Black" Visible="false">
                                            <i id="iCatGridView" runat="server" class="fa fa-th" style="margin-right: 15px" aria-hidden="true"
                                                title="Grid View"></i>
                                        </asp:LinkButton>
                                        <asp:LinkButton ID="btnCatSettings" runat="server" Font-Underline="false" ForeColor="Black"
                                            OnClientClick="showMenu(event);">
                                            <i id="iCatSettings" runat="server" class="fa fa-ellipsis-v" style="margin-right: 15px"
                                                aria-hidden="true" title="Settings"></i>
                                        </asp:LinkButton>
                                        <asp:LinkButton ID="btnCatDelete" runat="server" Font-Underline="false" ForeColor="Black"
                                            OnClick="btnCatDelete_Click">
                                            <i id="iCatDelete" runat="server" class="fa fa-trash-o" aria-hidden="true" title="Delete">
                                            </i>
                                        </asp:LinkButton>
                                    </div>
                                    <div class="container">
                                        <asp:Label runat="server" ID="lblNumberOfCourseCat" ForeColor="Teal" Font-Bold="true"
                                            Font-Size="Small">There are
                                            <asp:Literal ID="ltCourseCategory" runat="server"></asp:Literal>
                                            Course Categories </asp:Label>
                                    </div>
                                </div>
                                <asp:Panel ID="pnlCatListView" runat="server">
                                    <div class="col-md-14" style="margin-left: 1%; margin-right: 1%">
                                        <div class="col-md-12">
                                            <asp:Panel ID="pnlCatLv" runat="server">
                                                <div id="divCatLv" class="demo-container col-md-14">
                                                    <rad:RadListView ID="lvCourseCategory" runat="server" ItemPlaceholderID="ListViewContainer1"
                                                        AllowPaging="true" OnNeedDataSource="lvCourseCategory_NeedDataSource">
                                                        <ItemTemplate>
                                                            <asp:HyperLink ID="hlLvCourseCategory" runat="server" NavigateUrl='<%# string.Format("Subcategories.aspx?CategoryID={0}", HttpUtility.UrlEncode(Encrypt(Eval("CategoryID").ToString()))) %>'
                                                                ForeColor="Black">
                                                                <div id="divCatFolder" class="rlvCourseCat" title='<%#Eval("Category")%>'>
                                                                    <div class="photo-container-coursecat">
                                                                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Media/Images/CourseCat.png" Width="170px"
                                                                            Height="130px" />
                                                                    </div>
                                                                    <div class="category font-bold" align="center">
                                                                        <%#Eval("Category")%>
                                                                    </div>
                                                                </div>
                                                            </asp:HyperLink>
                                                        </ItemTemplate>
                                                        <LayoutTemplate>
                                                            <asp:PlaceHolder ID="ListViewContainer1" runat="server" />
                                                            <rad:RadDataPager ID="rdpCourseCategory" runat="server" RenderMode="Lightweight" 
                                                                PagedControlID="lvCourseCategory" PageSize="5" BackColor="White" BorderStyle="None"
                                                                CssClass="RadDataPagerx" >
                                                                <Fields>
                                                                    <rad:RadDataPagerButtonField FieldType="Prev" PrevButtonText="Prev"></rad:RadDataPagerButtonField>
                                                                    <%--<rad:RadDataPagerButtonField FieldType="Numeric" PageButtonCount="10" />--%>
                                                                    <rad:RadDataPagerButtonField FieldType="Next" NextButtonText="Next"></rad:RadDataPagerButtonField>
                                                                </Fields>
                                                            </rad:RadDataPager>
                                                        </LayoutTemplate>
                                                    </rad:RadListView>
                                                    <br />
                                                    <br />
                                                    <br />
                                                    <br />
                                                    <br />
                                                    <br />
                                                    <br />
                                                    <br />
                                                    <br />
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="pnlCatGridView" runat="server" Visible="false">
                                    <div class="col-md-14" style="margin-left: 1%; margin-right: 1%">
                                        <div class="col-md-12">
                                            <asp:Panel ID="pnlCatGv" runat="server">
                                                <div id="divCatGv" class="demo-container col-md-14">
                                                    <rad:RadGrid ID="gvCourseCategory" runat="server" RenderMode="Lightweight" OnNeedDataSource="gvCourseCategory_NeedDataSource"
                                                        AutoGenerateColumns="false" CssClass="GridLess" Height="370px" PageSize="5">
                                                        <PagerStyle Mode="NextPrev" AlwaysVisible="true"  />
                                                        <ClientSettings EnableAlternatingItems="true" AllowColumnsReorder="true">
                                                            <Selecting AllowRowSelect="true"></Selecting>
                                                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                            <Resizing AllowColumnResize="true" ResizeGridOnColumnResize="true" AllowResizeToFit="true" />
                                                        </ClientSettings>
                                                        <ItemStyle Wrap="false"></ItemStyle>
                                                        <MasterTableView DataKeyNames="CategoryID" AllowSorting="true" AllowPaging="true"
                                                             HeaderStyle-ForeColor="Teal" >
                                                            <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                            <Columns>
                                                                <rad:GridTemplateColumn SortExpression="Category"  UniqueName="Category" HeaderText="CATEGORY" DataField="Category">
                                                                    <ItemTemplate>
                                                                        <asp:HyperLink ID="hlGvCourseCategory" runat="server" NavigateUrl='<%# string.Format("Subcategories.aspx?CategoryID={0}", HttpUtility.UrlEncode(Encrypt(Eval("CategoryID").ToString()))) %>'>
                                                                            <%#Eval("Category")%>
                                                                        </asp:HyperLink>
                                                                    </ItemTemplate>
                                                                </rad:GridTemplateColumn>
                                                                <rad:GridBoundColumn UniqueName="Description" HeaderText="DESCRIPTION" DataField="Description"
                                                                    ReadOnly="true">
                                                                </rad:GridBoundColumn>
                                                                <rad:GridBoundColumn UniqueName="Subcategories" HeaderText="SUBCATEGORIES" DataField="Subcategories"
                                                                    ReadOnly="true">
                                                                </rad:GridBoundColumn>
                                                                <rad:GridBoundColumn UniqueName="Courses" HeaderText="COURSES" DataField="Courses"
                                                                    ReadOnly="true">
                                                                </rad:GridBoundColumn>
                                                                <rad:GridBoundColumn UniqueName="CreatedBy" HeaderText="UPDATED BY" DataField="CreatedBy"
                                                                    ReadOnly="true">
                                                                </rad:GridBoundColumn>
                                                                <rad:GridBoundColumn UniqueName="DateTimeCreated" HeaderText="DATE UPDATED" DataField="DateTimeCreated"
                                                                    ReadOnly="true">
                                                                </rad:GridBoundColumn>
                                                            </Columns>
                                                        </MasterTableView>
                                                    </rad:RadGrid>
                                                    <br />
                                                    <br />
                                                    <br />
                                                    <br />
                                                    <br />
                                                    <br />
                                                    <br />
                                                    <br />
                                                    <br />
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <rad:RadContextMenu ID="rcmCourseCategory" runat="server" RenderMode="Lightweight"
                                    OnItemClick="rcmCourseCategory_Click" EnableShadows="true" CssClass="RadContextMenu1"
                                    EnableScreenBoundaryDetection="false">
                                    <Items>
                                        <rad:RadMenuItem Text="Add Category" Value="1" />
                                    </Items>
                                </rad:RadContextMenu>
                            </rad:RadPageView>
                            <rad:RadPageView ID="rdpClassManagement" runat="server">
                                <uccm:classManagement ID="classManagement1" runat="server" />
                                <%--                                <div class="display-inline-flex" style="margin-top: 1%; margin-bottom: 0; margin-left: 31.5%;">
                                    <div class="no-paddings" style="margin-right: 5px;">
                                        <p>
                                            Training Type:
                                        </p>
                                    </div>
                                    <div class="no-paddings" style="margin-right: 25px;">
                                        <asp:DropDownList ID="ddlTrainingType" runat="server" CssClass="form-control rounded-corner"
                                            ClientIDMode="Static" Width="140px" DataSourceID="sdsTrainingType" DataTextField="TrainingType"
                                            DataValueField="TrainingTypeID" OnSelectedIndexChanged="ddlTrainingType_SelectedIndexChanged"
                                            AutoPostBack="true">
                                        </asp:DropDownList>
                                        <asp:SqlDataSource ID="sdsTrainingType" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection%>"
                                            SelectCommand="SELECT 0 AS TrainingTypeID, '- Select -' AS TrainingType UNION ALL SELECT TrainingTypeID, TrainingType FROM tbl_TranscomUniversity_Lkp_TrainingType WHERE HideFromList = 0">
                                        </asp:SqlDataSource>
                                    </div>
                                    <div class="no-paddings" style="margin-right: 5px;">
                                        <p>
                                            Status:
                                        </p>
                                    </div>
                                    <div class="no-paddings" style="margin-right: 25px;">
                                        <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control rounded-corner"
                                            ClientIDMode="Static" Width="140px" DataSourceID="sdsStatus" DataTextField="Status"
                                            DataValueField="StatusID" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged"
                                            AutoPostBack="true">
                                        </asp:DropDownList>
                                        <asp:SqlDataSource ID="sdsStatus" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection%>"
                                            SelectCommand="SELECT 0 AS StatusID, '- Select -' AS [Status] UNION ALL SELECT 1 AS StatusID, 'Not Yet Started' AS [Status] UNION ALL SELECT 2 AS StatusID, 'In-Progress' AS [Status]">
                                        </asp:SqlDataSource>
                                    </div>
                                    <div class="no-paddings" style="margin-right: 5px;">
                                        <p>
                                            Add data fields:
                                        </p>
                                    </div>
                                    <div class="no-paddings" style="margin-right: 25px;">
                                        <asp:DropDownList ID="ddlAddDataFields" runat="server" CssClass="form-control rounded-corner"
                                            ClientIDMode="Static" Width="140px" DataSourceID="sdsAddDataFields" DataTextField="DataFields"
                                            DataValueField="DataFieldsID">
                                        </asp:DropDownList>
                                        <asp:SqlDataSource ID="sdsAddDataFields" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection%>"
                                            SelectCommand="SELECT 0 AS DataFieldsID, '- Select -' AS DataFields"></asp:SqlDataSource>
                                    </div>
                                </div>
                                <div class="col-md-11" style="margin-left: 4.1%; margin-right: 4.1%;">
                                    <div class="row gray-border-no-pads">
                                        <asp:Panel ID="pnlNewHire" runat="server">
                                            <div class="widgets black-header">
                                                <div class="col-md-6">
                                                    New Hire</div>
                                                <div class="col-md-6">
                                                    <div class="row-fluid mcViews">
                                                        <div class="pull-right">
                                                            <asp:LinkButton ID="btnViewAllNH" runat="server" OnClick="btnViewAllNH_Click" Text="View All"
                                                                ForeColor="Gold"></asp:LinkButton>
                                                            <asp:LinkButton ID="btnViewLessNH" runat="server" OnClick="btnViewLessNH_Click" ForeColor="Gold"
                                                                Font-Underline="false" Visible="false">
                                                                <i class="fa fa-long-arrow-left"></i>
                                                            </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <rad:RadGrid ID="gvClassNewHire" runat="server" RenderMode="Lightweight" OnNeedDataSource="gvClassNewHire_NeedDataSource"
                                                AutoGenerateColumns="false" CssClass="GridLess" Height="370px" PageSize="5">
                                                <PagerStyle Mode="NextPrev" AlwaysVisible="true" />
                                                <ClientSettings EnableAlternatingItems="true" AllowColumnsReorder="true">
                                                    <Selecting AllowRowSelect="true"></Selecting>
                                                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                    <Resizing AllowColumnResize="true" ResizeGridOnColumnResize="true" AllowResizeToFit="true" />
                                                </ClientSettings>
                                                <ItemStyle Wrap="false"></ItemStyle>
                                                <MasterTableView DataKeyNames="ClassID" AllowSorting="true" AllowPaging="true" CommandItemDisplay="None"
                                                    HeaderStyle-ForeColor="Teal">
                                                    <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                    <Columns>
                                                        <rad:GridTemplateColumn UniqueName="ClassName" HeaderText="TITLE" DataField="ClassName">
                                                            <ItemTemplate>
                                                                <asp:HyperLink ID="hlClassNewHire" runat="server" NavigateUrl="#">
                                                                    <%#Eval("ClassName")%>
                                                                </asp:HyperLink>
                                                            </ItemTemplate>
                                                        </rad:GridTemplateColumn>
                                                        <rad:GridBoundColumn UniqueName="ClassDescription" HeaderText="DESCRIPTION" DataField="ClassDescription"
                                                            ReadOnly="true">
                                                        </rad:GridBoundColumn>
                                                        <rad:GridBoundColumn UniqueName="DateTimeStart" HeaderText="DATE/TIME START" DataField="DateTimeStart"
                                                            ReadOnly="true">
                                                        </rad:GridBoundColumn>
                                                        <rad:GridBoundColumn UniqueName="DateTimeEnd" HeaderText="DATE/TIME END" DataField="DateTimeEnd"
                                                            ReadOnly="true">
                                                        </rad:GridBoundColumn>
                                                        <rad:GridBoundColumn UniqueName="Trainer" HeaderText="TRAINER" DataField="Trainer"
                                                            ReadOnly="true">
                                                        </rad:GridBoundColumn>
                                                        <rad:GridBoundColumn UniqueName="Venue" HeaderText="VENUE" DataField="Venue" ReadOnly="true">
                                                        </rad:GridBoundColumn>
                                                        <rad:GridBoundColumn UniqueName="Status" HeaderText="STATUS" DataField="Status" ReadOnly="true">
                                                        </rad:GridBoundColumn>
                                                    </Columns>
                                                </MasterTableView>
                                            </rad:RadGrid>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlCrossTraining" runat="server">
                                            <div class="widgets black-header">
                                                <div class="col-md-6">
                                                    Cross Training</div>
                                                <div class="col-md-6">
                                                    <div class="row-fluid mcViews">
                                                        <div class="pull-right">
                                                            <asp:LinkButton ID="btnViewAllCT" runat="server" OnClick="btnViewAllCT_Click" Text="View All"
                                                                ForeColor="Gold"></asp:LinkButton>
                                                            <asp:LinkButton ID="btnViewLessCT" runat="server" OnClick="btnViewLessCT_Click" ForeColor="Gold"
                                                                Font-Underline="false" aria-hidden="true" Visible="false">
                                                                <i class="fa fa-long-arrow-left"></i>
                                                            </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <rad:RadGrid ID="gvClassCrossTraining" runat="server" RenderMode="Lightweight" OnNeedDataSource="gvClassCrossTraining_NeedDataSource"
                                                AutoGenerateColumns="false" CssClass="GridLess" Height="370px" PageSize="5">
                                                <PagerStyle Mode="NextPrev" AlwaysVisible="true" />
                                                <ClientSettings EnableAlternatingItems="true" AllowColumnsReorder="true">
                                                    <Selecting AllowRowSelect="true"></Selecting>
                                                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                    <Resizing AllowColumnResize="true" ResizeGridOnColumnResize="true" AllowResizeToFit="true" />
                                                </ClientSettings>
                                                <ItemStyle Wrap="false"></ItemStyle>
                                                <MasterTableView DataKeyNames="ClassID" AllowSorting="true" AllowPaging="true" CommandItemDisplay="None"
                                                    HeaderStyle-ForeColor="Teal">
                                                    <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                    <Columns>
                                                        <rad:GridTemplateColumn UniqueName="ClassName" HeaderText="TITLE" DataField="ClassName">
                                                            <ItemTemplate>
                                                                <asp:HyperLink ID="hlClassNewHire" runat="server" NavigateUrl="#">
                                                                    <%#Eval("ClassName")%>
                                                                </asp:HyperLink>
                                                            </ItemTemplate>
                                                        </rad:GridTemplateColumn>
                                                        <rad:GridBoundColumn UniqueName="ClassDescription" HeaderText="DESCRIPTION" DataField="ClassDescription"
                                                            ReadOnly="true">
                                                        </rad:GridBoundColumn>
                                                        <rad:GridBoundColumn UniqueName="DateTimeStart" HeaderText="DATE/TIME START" DataField="DateTimeStart"
                                                            ReadOnly="true">
                                                        </rad:GridBoundColumn>
                                                        <rad:GridBoundColumn UniqueName="DateTimeEnd" HeaderText="DATE/TIME END" DataField="DateTimeEnd"
                                                            ReadOnly="true">
                                                        </rad:GridBoundColumn>
                                                        <rad:GridBoundColumn UniqueName="Trainer" HeaderText="TRAINER" DataField="Trainer"
                                                            ReadOnly="true">
                                                        </rad:GridBoundColumn>
                                                        <rad:GridBoundColumn UniqueName="Venue" HeaderText="VENUE" DataField="Venue" ReadOnly="true">
                                                        </rad:GridBoundColumn>
                                                        <rad:GridBoundColumn UniqueName="Status" HeaderText="STATUS" DataField="Status" ReadOnly="true">
                                                        </rad:GridBoundColumn>
                                                    </Columns>
                                                </MasterTableView>
                                            </rad:RadGrid>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlRefresherTraining" runat="server">
                                            <div class="widgets black-header">
                                                <div class="col-md-6">
                                                    Refresher Training</div>
                                                <div class="col-md-6">
                                                    <div class="row-fluid mcViews">
                                                        <div class="pull-right">
                                                            <asp:LinkButton ID="btnViewAllRT" runat="server" OnClick="btnViewAllRT_Click" Text="View All"
                                                                ForeColor="Gold"></asp:LinkButton>
                                                            <asp:LinkButton ID="btnViewLessRT" runat="server" OnClick="btnViewLessRT_Click" ForeColor="Gold"
                                                                Font-Underline="false" aria-hidden="true" Visible="false">
                                                                <i class="fa fa-long-arrow-left"></i>
                                                            </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <rad:RadGrid ID="gvClassRefresherTraining" runat="server" RenderMode="Lightweight"
                                                OnNeedDataSource="gvClassRefresherTraining_NeedDataSource" AutoGenerateColumns="false"
                                                CssClass="GridLess" Height="370px" PageSize="5">
                                                <PagerStyle Mode="NextPrev" AlwaysVisible="true" />
                                                <ClientSettings EnableAlternatingItems="true" AllowColumnsReorder="true">
                                                    <Selecting AllowRowSelect="true"></Selecting>
                                                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                    <Resizing AllowColumnResize="true" ResizeGridOnColumnResize="true" AllowResizeToFit="true" />
                                                </ClientSettings>
                                                <ItemStyle Wrap="false"></ItemStyle>
                                                <MasterTableView DataKeyNames="ClassID" AllowSorting="true" AllowPaging="true" CommandItemDisplay="None"
                                                    HeaderStyle-ForeColor="Teal">
                                                    <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                    <Columns>
                                                        <rad:GridTemplateColumn UniqueName="ClassName" HeaderText="TITLE" DataField="ClassName">
                                                            <ItemTemplate>
                                                                <asp:HyperLink ID="hlClassNewHire" runat="server" NavigateUrl="#">
                                                                    <%#Eval("ClassName")%>
                                                                </asp:HyperLink>
                                                            </ItemTemplate>
                                                        </rad:GridTemplateColumn>
                                                        <rad:GridBoundColumn UniqueName="ClassDescription" HeaderText="DESCRIPTION" DataField="ClassDescription"
                                                            ReadOnly="true">
                                                        </rad:GridBoundColumn>
                                                        <rad:GridBoundColumn UniqueName="DateTimeStart" HeaderText="DATE/TIME START" DataField="DateTimeStart"
                                                            ReadOnly="true">
                                                        </rad:GridBoundColumn>
                                                        <rad:GridBoundColumn UniqueName="DateTimeEnd" HeaderText="DATE/TIME END" DataField="DateTimeEnd"
                                                            ReadOnly="true">
                                                        </rad:GridBoundColumn>
                                                        <rad:GridBoundColumn UniqueName="Trainer" HeaderText="TRAINER" DataField="Trainer"
                                                            ReadOnly="true">
                                                        </rad:GridBoundColumn>
                                                        <rad:GridBoundColumn UniqueName="Venue" HeaderText="VENUE" DataField="Venue" ReadOnly="true">
                                                        </rad:GridBoundColumn>
                                                        <rad:GridBoundColumn UniqueName="Status" HeaderText="STATUS" DataField="Status" ReadOnly="true">
                                                        </rad:GridBoundColumn>
                                                    </Columns>
                                                </MasterTableView>
                                            </rad:RadGrid>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlUpTraining" runat="server">
                                            <div class="widgets black-header">
                                                <div class="col-md-6">
                                                    Up Training</div>
                                                <div class="col-md-6">
                                                    <div class="row-fluid mcViews">
                                                        <div class="pull-right">
                                                            <asp:LinkButton ID="btnViewAllUT" runat="server" OnClick="btnViewAllUT_Click" Text="View All"
                                                                ForeColor="Gold"></asp:LinkButton>
                                                            <asp:LinkButton ID="btnViewLessUT" runat="server" OnClick="btnViewLessUT_Click" ForeColor="Gold"
                                                                Font-Underline="false" aria-hidden="true" Visible="false">
                                                                <i class="fa fa-long-arrow-left"></i>
                                                            </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <rad:RadGrid ID="gvClassUpTraining" runat="server" RenderMode="Lightweight" OnNeedDataSource="gvClassUpTraining_NeedDataSource"
                                                AutoGenerateColumns="false" CssClass="GridLess" Height="370px" PageSize="5">
                                                <PagerStyle Mode="NextPrev" AlwaysVisible="true" />
                                                <ClientSettings EnableAlternatingItems="true" AllowColumnsReorder="true">
                                                    <Selecting AllowRowSelect="true"></Selecting>
                                                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                    <Resizing AllowColumnResize="true" ResizeGridOnColumnResize="true" AllowResizeToFit="true" />
                                                </ClientSettings>
                                                <ItemStyle Wrap="false"></ItemStyle>
                                                <MasterTableView DataKeyNames="ClassID" AllowSorting="true" AllowPaging="true" CommandItemDisplay="None"
                                                    HeaderStyle-ForeColor="Teal">
                                                    <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                    <Columns>
                                                        <rad:GridTemplateColumn UniqueName="ClassName" HeaderText="TITLE" DataField="ClassName">
                                                            <ItemTemplate>
                                                                <asp:HyperLink ID="hlClassNewHire" runat="server" NavigateUrl="#">
                                                                    <%#Eval("ClassName")%>
                                                                </asp:HyperLink>
                                                            </ItemTemplate>
                                                        </rad:GridTemplateColumn>
                                                        <rad:GridBoundColumn UniqueName="ClassDescription" HeaderText="DESCRIPTION" DataField="ClassDescription"
                                                            ReadOnly="true">
                                                        </rad:GridBoundColumn>
                                                        <rad:GridBoundColumn UniqueName="DateTimeStart" HeaderText="DATE/TIME START" DataField="DateTimeStart"
                                                            ReadOnly="true">
                                                        </rad:GridBoundColumn>
                                                        <rad:GridBoundColumn UniqueName="DateTimeEnd" HeaderText="DATE/TIME END" DataField="DateTimeEnd"
                                                            ReadOnly="true">
                                                        </rad:GridBoundColumn>
                                                        <rad:GridBoundColumn UniqueName="Trainer" HeaderText="TRAINER" DataField="Trainer"
                                                            ReadOnly="true">
                                                        </rad:GridBoundColumn>
                                                        <rad:GridBoundColumn UniqueName="Venue" HeaderText="VENUE" DataField="Venue" ReadOnly="true">
                                                        </rad:GridBoundColumn>
                                                        <rad:GridBoundColumn UniqueName="Status" HeaderText="STATUS" DataField="Status" ReadOnly="true">
                                                        </rad:GridBoundColumn>
                                                    </Columns>
                                                </MasterTableView>
                                            </rad:RadGrid>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlProfessionalDevelopment" runat="server">
                                            <div class="widgets black-header">
                                                <div class="col-md-6">
                                                    Professional Development</div>
                                                <div class="col-md-6">
                                                    <div class="row-fluid mcViews">
                                                        <div class="pull-right">
                                                            <asp:LinkButton ID="btnViewAllPD" runat="server" OnClick="btnViewAllPD_Click" Text="View All"
                                                                ForeColor="Gold"></asp:LinkButton>
                                                            <asp:LinkButton ID="btnViewLessPD" runat="server" OnClick="btnViewLessPD_Click" ForeColor="Gold"
                                                                Font-Underline="false" aria-hidden="true" Visible="false">
                                                                <i class="fa fa-long-arrow-left"></i>
                                                            </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <rad:RadGrid ID="gvClassProfessionalDevelopment" runat="server" RenderMode="Lightweight"
                                                OnNeedDataSource="gvClassProfessionalDevelopment_NeedDataSource" AutoGenerateColumns="false"
                                                CssClass="GridLess" Height="370px" PageSize="5">
                                                <PagerStyle Mode="NextPrev" AlwaysVisible="true" />
                                                <ClientSettings EnableAlternatingItems="true" AllowColumnsReorder="true">
                                                    <Selecting AllowRowSelect="true"></Selecting>
                                                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                    <Resizing AllowColumnResize="true" ResizeGridOnColumnResize="true" AllowResizeToFit="true" />
                                                </ClientSettings>
                                                <ItemStyle Wrap="false"></ItemStyle>
                                                <MasterTableView DataKeyNames="ClassID" AllowSorting="true" AllowPaging="true" CommandItemDisplay="None"
                                                    HeaderStyle-ForeColor="Teal">
                                                    <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                    <Columns>
                                                        <rad:GridTemplateColumn UniqueName="ClassName" HeaderText="TITLE" DataField="ClassName">
                                                            <ItemTemplate>
                                                                <asp:HyperLink ID="hlClassNewHire" runat="server" NavigateUrl="#">
                                                                    <%#Eval("ClassName")%>
                                                                </asp:HyperLink>
                                                            </ItemTemplate>
                                                        </rad:GridTemplateColumn>
                                                        <rad:GridBoundColumn UniqueName="ClassDescription" HeaderText="DESCRIPTION" DataField="ClassDescription"
                                                            ReadOnly="true">
                                                        </rad:GridBoundColumn>
                                                        <rad:GridBoundColumn UniqueName="DateTimeStart" HeaderText="DATE/TIME START" DataField="DateTimeStart"
                                                            ReadOnly="true">
                                                        </rad:GridBoundColumn>
                                                        <rad:GridBoundColumn UniqueName="DateTimeEnd" HeaderText="DATE/TIME END" DataField="DateTimeEnd"
                                                            ReadOnly="true">
                                                        </rad:GridBoundColumn>
                                                        <rad:GridBoundColumn UniqueName="Trainer" HeaderText="TRAINER" DataField="Trainer"
                                                            ReadOnly="true">
                                                        </rad:GridBoundColumn>
                                                        <rad:GridBoundColumn UniqueName="Venue" HeaderText="VENUE" DataField="Venue" ReadOnly="true">
                                                        </rad:GridBoundColumn>
                                                        <rad:GridBoundColumn UniqueName="Status" HeaderText="STATUS" DataField="Status" ReadOnly="true">
                                                        </rad:GridBoundColumn>
                                                    </Columns>
                                                </MasterTableView>
                                            </rad:RadGrid>
                                        </asp:Panel>
                                    </div>
                                </div>--%>
                            </rad:RadPageView>
                            <rad:RadPageView ID="rdpBuiltinClasses" runat="server">
                                rdpBuiltinClasses
                            </rad:RadPageView>
                            <rad:RadPageView ID="rdpClassBuilder" runat="server">
                            </rad:RadPageView>
                            <rad:RadPageView ID="rdpHomepageEditor" runat="server">
                                <div class="container" style="display: block; width: 90vw;">
                                    <ucpe:pageEditor ID="PageEditor1" runat="server" />
                                </div>
                            </rad:RadPageView>
                            <rad:RadPageView ID="rdpImageSlider" runat="server">
                                <ucis:imageSliderSettings ID="ImageSlider1" runat="server" />
                            </rad:RadPageView>
                            <rad:RadPageView ID="rdpFeaturedCourses" runat="server">
                                <ucfc:featuredCoursesSettings runat="server" />
                            </rad:RadPageView>
                            <rad:RadPageView ID="rdpLatesNews" runat="server">
                                <ucns:newsSettings runat="server" />
                            </rad:RadPageView>
                            <rad:RadPageView ID="rdpTestimonials" runat="server">
                                <ucts:testimonialSettings runat="server" />
                            </rad:RadPageView>
                            <rad:RadPageView ID="rdpMandatoryCourses" runat="server">
                                <ucMandatory:pageEditorMandatoryCourse ID="PageEditorMandatoryCourse" runat="server" />
                            </rad:RadPageView>
                            <rad:RadPageView ID="rdpTrendingCourses" runat="server">
                                <ucTrending:pageEditorTrendingCourse ID="PageEditorTrendingCourse" runat="server" />
                            </rad:RadPageView>
                            <rad:RadPageView ID="rdpRecommendedCourses" runat="server">
                                <ucRecommended:pageEditorRecommendedCourse ID="PageEditorRecommendedCourse" runat="server" />
                            </rad:RadPageView>
                            <rad:RadPageView ID="rdpMustTakeCourses" runat="server">
                                <ucMustTake:pageEditorMustTakeCourse ID="PageEditorMustTakeCourse" runat="server" />
                            </rad:RadPageView>
                            <rad:RadPageView ID="rdpRecentCourses" runat="server">
                                <ucRecentlyAdded:pageEditorRecentlyAddedCourse ID="PageEditorRecentlyAddedCourse"
                                    runat="server" />
                            </rad:RadPageView>
                            <rad:RadPageView ID="rdpUserManagement" runat="server">
                                rdpUserManagement
                            </rad:RadPageView>
                            <rad:RadPageView ID="rdpForums" runat="server">
                                <asp:Panel runat="server" ID="pnlForums">
                                <br />
                                <br />
                                <br />
                                <br />
                                <ucf:CourseForumUserCtrl ID="coursesForum" runat="server" Visible="false" />
                                <ucft:ForumTopicsUserCtrl ID="forumTopics" runat="server" Visible="false" />
                                <ucat:ForumAddTopic ID="forumAddTopic" runat="server" Visible="false" />
                                <uctd:ForumThread ID="forumThreadView" runat="server" Visible="false" />
                                </asp:Panel>
                            </rad:RadPageView>
                            <rad:RadPageView ID="rdpOverView" runat="server">
                                <%--<ucao:adminOverview ID="AdminOverview" runat="server" />--%>
                                <asp:Panel ID="pnlAdminDashboard" runat="server">
                                    <div class="col-md-12">
                                        <div class="col-md-12 no-paddings" style="margin-top: 2%; margin-bottom: 1%;">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <div class="big-menu-dashboard pointer" title="Overview" onclick="document.getElementById('<%= btnOverview.ClientID %>').click()">
                                                        <div class="icon-holder text-center">
                                                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                            <div class="col-xs-6 icon-glyph text-center">
                                                                <span class="fa" aria-hidden="true">
                                                                    <image src="Media/Images/Overview.png" style="width: 58px; length: 58px;"></image>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <p>
                                                            Overview
                                                        </p>
                                                    </div>
                                                    <asp:HyperLink ID="btnOverview" runat="server" NavigateUrl="Admin.aspx#"></asp:HyperLink>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="big-menu-dashboard pointer" title="All Classes" onclick="document.getElementById('<%= btnAllClasses.ClientID %>').click()">
                                                        <div class="icon-holder">
                                                            <div class="col-xs-6 counter text-center">
                                                                <asp:Literal ID="ltAllClasses" runat="server"></asp:Literal>
                                                            </div>
                                                            <div class="col-xs-6 icon-glyph text-center">
                                                                <span class="fa" aria-hidden="true">
                                                                    <image src="Media/Images/allClasses.png" style="width: 58px; length: 58px;"></image>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <p>
                                                            All Classes
                                                        </p>
                                                    </div>
                                                    <asp:HyperLink ID="btnAllClasses" runat="server" NavigateUrl="Admin.aspx#"></asp:HyperLink>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="big-menu-dashboard pointer" title="All Courses" onclick="document.getElementById('<%= btnAllCourses.ClientID %>').click()">
                                                        <div class="icon-holder">
                                                            <div class="col-xs-6 counter text-center">
                                                                <asp:Literal ID="ltAllCourses" runat="server"></asp:Literal>
                                                            </div>
                                                            <div class="col-xs-6 icon-glyph text-center">
                                                                <span class="fa fa-graduation-cap" aria-hidden="true"></span>
                                                            </div>
                                                        </div>
                                                        <p>
                                                            All Courses
                                                        </p>
                                                    </div>
                                                    <asp:HyperLink ID="btnAllCourses" runat="server" NavigateUrl="Admin.aspx?Tab=CourseMgmt"></asp:HyperLink>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="big-menu-dashboard pointer" title="All Users" onclick="document.getElementById('<%= btnAllUsers.ClientID %>').click()">
                                                        <div class="icon-holder">
                                                            <div class="col-xs-6 counter text-center">
                                                                <asp:Literal ID="ltAllUsers" runat="server"></asp:Literal>
                                                            </div>
                                                            <div class="col-xs-6 icon-glyph text-center">
                                                                <span class="fa fa-users" aria-hidden="true"></span>
                                                            </div>
                                                        </div>
                                                        <p>
                                                            All Users
                                                        </p>
                                                    </div>
                                                    <asp:HyperLink ID="btnAllUsers" runat="server" NavigateUrl="Admin.aspx?Tab=UserMgmt"></asp:HyperLink>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="big-menu-dashboard pointer" title="All Enrollments" onclick="document.getElementById('<%= btnAllEnrollments.ClientID %>').click()">
                                                        <div class="icon-holder">
                                                            <div class="col-xs-4 counter text-center">
                                                                <asp:Literal ID="ltAllEnrollments" runat="server"></asp:Literal>
                                                            </div>
                                                            <div class="col-xs-6 icon-glyph text-center">
                                                                <span class="fa" aria-hidden="true">
                                                                    <image src="Media/Images/allEnrollment.png" style="width: 58px; length: 58px;"></image>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <p>
                                                            All Enrollments
                                                        </p>
                                                    </div>
                                                    <asp:HyperLink ID="btnAllEnrollments" runat="server" NavigateUrl="Admin.aspx#"></asp:HyperLink>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="big-menu-dashboard pointer" title="Site Traffic" onclick="document.getElementById('<%= btnSiteTraffic.ClientID %>').click()">
                                                        <div class="icon-holder">
                                                            <div class="col-xs-4 counter text-center">
                                                                <asp:Literal ID="ltSiteTraffic" runat="server"></asp:Literal>
                                                            </div>
                                                            <div class="col-xs-6 icon-glyph text-center">
                                                                <span class="fa" aria-hidden="true" style="font-size: small;">Active Visitors On Site</span>
                                                            </div>
                                                        </div>
                                                        <p style="text-align: center;">
                                                            Site Traffic
                                                        </p>
                                                    </div>
                                                    <asp:HyperLink ID="btnSiteTraffic" runat="server" NavigateUrl="Admin.aspx#"></asp:HyperLink>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-10 widgets dark-gray-header" style="width: 100%;">
                                            <div class="col-md-12 no-paddings text-transform-uppercase">
                                                <rad:RadCodeBlock ID="RadCodeBlock1" runat="server">
                                                    <asp:Label ID="lblBreadcrumbs" runat="server" Text="Admin Overview"></asp:Label>
                                                </rad:RadCodeBlock>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 gray-border-no-pads ten-px-padding bg-color-light-gray">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-8 ">
                                                            <p class="color-0D9E9E font-bold">
                                                                <asp:Label ID="lblOverViewHeader" runat="server">SHOWING OVERVIEW IN THE LAST
                                                                    <rad:RadTextBox runat="server" ID="lblLastDays" Width="30px" MaxLength="2" oncopy="return false"
                                                                        onpaste="return false" oncut="return false">
                                                                        <ClientEvents OnKeyPress="keyPress" />
                                                                    </rad:RadTextBox>
                                                                    DAYS </asp:Label></p>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <asp:Label ID="lblOverviewStartDate" runat="server">Start Date</asp:Label>
                                                            <rad:RadDatePicker ID="rdpOverviewStart" runat="server" RenderMode="Lightweight"
                                                                DateInput-ValidationGroup="addCourse" Width="120px" ToolTip="Set Start Date"
                                                                CssClass="left-calendar-icon form-control">
                                                            </rad:RadDatePicker>
                                                            <asp:Label ID="lblOverviewEndDate" runat="server">End Date</asp:Label>
                                                            <rad:RadDatePicker ID="rdpOverviewEnd" runat="server" RenderMode="Lightweight" DateInput-ValidationGroup="addCourse"
                                                                Width="120px" ToolTip="Set Start Date" CssClass="left-calendar-icon form-control">
                                                            </rad:RadDatePicker>
                                                        </div>
                                                    </div>
                                                    <asp:Panel ID="pnlPie" runat="server" Visible="false">
                                                        <div class="row" style="margin-top: 2%;">
                                                            <div class="col-md-3">
                                                                <div class="container">
                                                                    <div class="col-md-8 text-center">
                                                                        <rad:RadHtmlChart runat="server" ID="DonutChart" Width="250" Height="250" Transitions="true">
                                                                            <Appearance>
                                                                                <FillStyle BackgroundColor="transparent"></FillStyle>
                                                                            </Appearance>
                                                                            <PlotArea>
                                                                                <Series>
                                                                                    <rad:DonutSeries HoleSize="50">
                                                                                        <LabelsAppearance Visible="false">
                                                                                        </LabelsAppearance>
                                                                                        <TooltipsAppearance DataFormatString="{0}%" BackgroundColor="transparent">
                                                                                        </TooltipsAppearance>
                                                                                        <SeriesItems>
                                                                                            <rad:PieSeriesItem BackgroundColor="#00adcc" Y="100"></rad:PieSeriesItem>
                                                                                            <rad:PieSeriesItem BackgroundColor="#cccccc" Y="40"></rad:PieSeriesItem>
                                                                                            <rad:PieSeriesItem BackgroundColor="#999999" Y="60"></rad:PieSeriesItem>
                                                                                        </SeriesItems>
                                                                                    </rad:DonutSeries>
                                                                                </Series>
                                                                            </PlotArea>
                                                                        </rad:RadHtmlChart>
                                                                    </div>
                                                                    <div class="col-md-12 text-center">
                                                                        <p class="bold_text color-created" style="font-size: xx-large;">
                                                                            <asp:Literal runat="server" ID="ltOverViewCreatedClasses" Text="0"></asp:Literal></p>
                                                                    </div>
                                                                    <div class="col-md-12 text-center color-created">
                                                                        <p>
                                                                            CREATED CLASSES</p>
                                                                    </div>
                                                                    <div class="col-md-12 text-center">
                                                                        <p class="bold_text color-completed" style="font-size: xx-large;">
                                                                            <asp:Literal runat="server" ID="ltOverviewCompletedClasses" Text="0"></asp:Literal></p>
                                                                    </div>
                                                                    <div class="col-md-12 text-center color-completed">
                                                                        <p>
                                                                            COMPLETED CLASSES</p>
                                                                    </div>
                                                                    <div class="col-md-12 text-center">
                                                                        <p class="bold_text color-in-progress" style="font-size: xx-large;">
                                                                            <asp:Literal runat="server" ID="ltOverviewClassesInProgress" Text="0"></asp:Literal></p>
                                                                    </div>
                                                                    <div class="col-md-12 text-center color-in-progress">
                                                                        <p>
                                                                            CLASSES IN-PROGRESS</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="container">
                                                                    <div class="col-md-8 text-center">
                                                                        <rad:RadHtmlChart runat="server" ID="rcCourses" Width="250" Height="250" Transitions="true">
                                                                            <Appearance>
                                                                                <FillStyle BackgroundColor="transparent"></FillStyle>
                                                                            </Appearance>
                                                                            <PlotArea>
                                                                                <Series>
                                                                                    <rad:DonutSeries HoleSize="50">
                                                                                        <LabelsAppearance Visible="false">
                                                                                        </LabelsAppearance>
                                                                                        <TooltipsAppearance DataFormatString="{0}%" BackgroundColor="transparent">
                                                                                        </TooltipsAppearance>
                                                                                        <SeriesItems>
                                                                                            <rad:PieSeriesItem BackgroundColor="#00adcc" Y="100"></rad:PieSeriesItem>
                                                                                            <rad:PieSeriesItem BackgroundColor="#cccccc" Y="40"></rad:PieSeriesItem>
                                                                                            <rad:PieSeriesItem BackgroundColor="#999999" Y="60"></rad:PieSeriesItem>
                                                                                        </SeriesItems>
                                                                                    </rad:DonutSeries>
                                                                                </Series>
                                                                            </PlotArea>
                                                                        </rad:RadHtmlChart>
                                                                    </div>
                                                                    <div class="col-md-12 text-center">
                                                                        <p class="bold_text color-created" style="font-size: xx-large;">
                                                                            <asp:Literal runat="server" ID="ltOverviewCreatedCourses" Text="0"></asp:Literal>
                                                                        </p>
                                                                    </div>
                                                                    <div class="col-md-12 text-center color-created">
                                                                        <p>
                                                                            CREATED COURSES</p>
                                                                    </div>
                                                                    <div class="col-md-12 text-center">
                                                                        <p class="bold_text color-completed" style="font-size: xx-large;">
                                                                            <asp:Literal runat="server" ID="ltOverviewCompleteCourses" Text="0"></asp:Literal></p>
                                                                    </div>
                                                                    <div class="col-md-12 text-center color-completed">
                                                                        <p>
                                                                            COMPLETED COURSES</p>
                                                                    </div>
                                                                    <div class="col-md-12 text-center">
                                                                        <p class="bold_text color-in-progress" style="font-size: xx-large;">
                                                                            <asp:Literal runat="server" ID="ltOverviewInProgressCourses" Text="0"></asp:Literal></p>
                                                                    </div>
                                                                    <div class="col-md-12 text-center color-in-progress">
                                                                        <p>
                                                                            IN-PROGRESS COURSES</p>
                                                                    </div>
                                                                    <div class="col-md-12 text-center">
                                                                        <p class="bold_text color-not-started" style="font-size: xx-large;">
                                                                            <asp:Literal runat="server" ID="ltOverviewNotStartedCourses" Text="0"></asp:Literal></p>
                                                                    </div>
                                                                    <div class="col-md-12 text-center color-not-started">
                                                                        <p>
                                                                            NOT STARTED COURSES</p>
                                                                    </div>
                                                                    <div class="col-md-12 text-center">
                                                                        <p class="bold_text color-overdue" style="font-size: xx-large;">
                                                                            <asp:Literal runat="server" ID="ltOverviewOverDueCourses" Text="0"></asp:Literal></p>
                                                                    </div>
                                                                    <div class="col-md-12 text-center color-overdue">
                                                                        <p>
                                                                            OVERDUE COURSES</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="container">
                                                                    <div class="col-md-8 text-center">
                                                                        <rad:RadHtmlChart runat="server" ID="RadHtmlChart2" Width="250" Height="250" Transitions="true">
                                                                            <Appearance>
                                                                                <FillStyle BackgroundColor="transparent"></FillStyle>
                                                                            </Appearance>
                                                                            <PlotArea>
                                                                                <Series>
                                                                                    <rad:DonutSeries HoleSize="50">
                                                                                        <LabelsAppearance Visible="false">
                                                                                        </LabelsAppearance>
                                                                                        <TooltipsAppearance DataFormatString="{0}%" BackgroundColor="Transparent">
                                                                                        </TooltipsAppearance>
                                                                                        <SeriesItems>
                                                                                            <rad:PieSeriesItem BackgroundColor="#00adcc" Y="100"></rad:PieSeriesItem>
                                                                                            <rad:PieSeriesItem BackgroundColor="#cccccc" Y="40"></rad:PieSeriesItem>
                                                                                            <rad:PieSeriesItem BackgroundColor="#999999" Y="60"></rad:PieSeriesItem>
                                                                                        </SeriesItems>
                                                                                    </rad:DonutSeries>
                                                                                </Series>
                                                                            </PlotArea>
                                                                        </rad:RadHtmlChart>
                                                                    </div>
                                                                    <div class="col-md-12 text-center">
                                                                        <p class="bold_text color-created" style="font-size: xx-large;">
                                                                            <asp:Literal runat="server" ID="ltOverviewUsers" Text="0"></asp:Literal>
                                                                        </p>
                                                                    </div>
                                                                    <div class="col-md-12 text-center color-created">
                                                                        <p>
                                                                            USERS</p>
                                                                    </div>
                                                                    <div class="col-md-12 text-center">
                                                                        <p class="bold_text color-created" style="font-size: xx-large;">
                                                                            <asp:Literal runat="server" ID="ltOverviewTotalUsersLogin" Text="0"></asp:Literal></p>
                                                                    </div>
                                                                    <div class="col-md-12 text-center color-completed">
                                                                        <p>
                                                                            TOTAL USERS LOGIN</p>
                                                                    </div>
                                                                    <div class="col-md-12 text-center">
                                                                        <p class="bold_text color-completed" style="font-size: xx-large;">
                                                                            <asp:Literal runat="server" ID="ltOverviewMostActiveUsers" Text="0"></asp:Literal></p>
                                                                    </div>
                                                                    <div class="col-md-12 text-center color-in-progress">
                                                                        <p>
                                                                            MOST ACTIVE USERS</p>
                                                                    </div>
                                                                    <div class="col-md-12 text-center">
                                                                        <p class="bold_text color-in-progress" style="font-size: xx-large;">
                                                                            <asp:Literal runat="server" ID="ltOverviewNeverLoggedIn" Text="0"></asp:Literal></p>
                                                                    </div>
                                                                    <div class="col-md-12 text-center color-completed">
                                                                        <p>
                                                                            NEVER LOGGED IN</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="container">
                                                                    <div class="col-md-8 text-center">
                                                                        <rad:RadHtmlChart runat="server" ID="RadHtmlChart3" Width="250" Height="250" Transitions="true">
                                                                            <Appearance>
                                                                                <FillStyle BackgroundColor="transparent"></FillStyle>
                                                                            </Appearance>
                                                                            <PlotArea>
                                                                                <Series>
                                                                                    <rad:DonutSeries HoleSize="50">
                                                                                        <LabelsAppearance Visible="false">
                                                                                        </LabelsAppearance>
                                                                                        <TooltipsAppearance DataFormatString="{0}%" BackgroundColor="Transparent">
                                                                                        </TooltipsAppearance>
                                                                                        <SeriesItems>
                                                                                            <rad:PieSeriesItem BackgroundColor="#00adcc" Y="100"></rad:PieSeriesItem>
                                                                                            <rad:PieSeriesItem BackgroundColor="#cccccc" Y="40"></rad:PieSeriesItem>
                                                                                            <rad:PieSeriesItem BackgroundColor="#999999" Y="60"></rad:PieSeriesItem>
                                                                                        </SeriesItems>
                                                                                    </rad:DonutSeries>
                                                                                </Series>
                                                                            </PlotArea>
                                                                        </rad:RadHtmlChart>
                                                                    </div>
                                                                    <div class="col-md-12 text-center">
                                                                        <p class="bold_text color-created" style="font-size: xx-large;">
                                                                            <asp:Literal runat="server" ID="ltOverviewEnrollments" Text="0"></asp:Literal></p>
                                                                    </div>
                                                                    <div class="col-md-12 text-center color-created">
                                                                        <p>
                                                                            ENROLLMENTS</p>
                                                                    </div>
                                                                    <div class="col-md-12 text-center">
                                                                        <p class="bold_text color-completed" style="font-size: xx-large;">
                                                                            <asp:Literal runat="server" ID="ltOverviewApproved" Text="0"></asp:Literal></p>
                                                                    </div>
                                                                    <div class="col-md-12 text-center color-completed">
                                                                        <p>
                                                                            APPROVED</p>
                                                                    </div>
                                                                    <div class="col-md-12 text-center">
                                                                        <p class="bold_text color-in-progress" style="font-size: xx-large;">
                                                                            <asp:Literal runat="server" ID="ltOverviewPending" Text="0"></asp:Literal></p>
                                                                    </div>
                                                                    <div class="col-md-12 text-center color-in-progress">
                                                                        <p>
                                                                            PENDING</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </asp:Panel>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </rad:RadPageView>
                        </rad:RadMultiPage>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        <p class="display-none js-redirectMessage">
            &nbsp;Course Added Successfuly<br />
            &nbsp;Redirecting to Course Builder...
        </p>
    </div>
</asp:Content>
