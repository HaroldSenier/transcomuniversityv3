﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using Telerik.Web.UI;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Text;
using Telerik.Charting;
using System.Security.Cryptography;
using TranscomUniversityV3Model;

public class NewHireClassData
{
    public int count { get; set; }
    public IEnumerable<dynamic> data { get; set; }
}

public partial class Admin : BasePage
{
    public class CourseDetails
    {
        public string EncryptedCourseID { get; set; }

        public int? CourseID { get; set; }

        public string CourseImage { get; set; }

        public string CourseTitle { get; set; }

        public string CourseType { get; set; }

        public int? CourseDuration { get; set; }

        public string CourseCategory { get; set; }

        public string CourseSubcategory { get; set; }

        public string CourseDescription { get; set; }

        public string DateLastModified { get; set; }

        public string CoursePath { get; set; }
    }

    public class userDetails
    {
        public string userID { get; set; }
        public string userEmail { get; set; }
    }

    //private int newCourseID;

    //private DateTime serverDate;

    //private static string global_courseImage;

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Form.Attributes.Add("enctype", "multipart/form-data");

        //newCourseID = lastInsertedCourseID() + "" == null ? 0 : lastInsertedCourseID() + 1;
        if (!Page.IsPostBack)
        {
            //if (!System.Web.Security.Roles.IsUserInRole(HttpContext.Current.User.Identity.Name.Split('|')[0], "Admin"))
            //    Response.Redirect("~/Unauthorized.aspx");

            //set Session
            Session["SessionRoles"] = "Admin";

            //clearCourseInput();

            //set breadcrumb text
            lblTab2.Text = "Overview";
            //if (btnSwitchToListView.Visible)
            //    lblTab3.Text = "Grid View";
            //else
            //    lblTab3.Text = "List View";

            ////populate tenure years
            //for (int i = 0; i <= 20; i++)
            //{
            //    ListItem item = new ListItem();
            //    item.Value = i.ToString();
            //    item.Text = i.ToString();
            //    ddlTenureYears.Items.Add(item);
            //    ddlRoleYears.Items.Add(item);
            //}

            ////populate duration dropdown
            //for (int i = 0; i <= 60; i++)
            //{
            //    ListItem item = new ListItem();
            //    item.Value = i.ToString();
            //    item.Text = i.ToString();
            //    ddlDuration.Items.Add(item);

            //}

            ////Load Course Type

            //rcbCourseType.DataTextField = "CourseType";
            //rcbCourseType.DataValueField = "CourseTypeID";
            //rcbCourseType.DataSource = DataHelper.GetCourseType();
            //rcbCourseType.DataBind();
            ////Load Categories
            //rcbCategory.DataTextField = "Category";
            //rcbCategory.DataValueField = "CategoryID";
            //rcbCategory.DataSource = DataHelper.GetAllCategories();
            //rcbCategory.DataBind();

            //rcbReqCourse.DataTextField = "Category";
            //rcbReqCourse.DataValueField = "CategoryID";
            //rcbReqCourse.AppendDataBoundItems = true;
            //rcbReqCourse.DataSource = DataHelper.GetAllCategories();
            //rcbReqCourse.Items.Insert(0, new RadComboBoxItem("- Select -", "-1"));
            //rcbReqCourse.DataBind();

            ////rcbAuthor.Filter = RadComboBoxFilter.StartsWith;
            //rcbDepartment.Filter = RadComboBoxFilter.StartsWith;
            //rcbReqProgram.Filter = RadComboBoxFilter.StartsWith;
            //serverDate = DataHelper.serverDate();
            //txtDateCreated.Text = serverDate.ToShortDateString();
            //ddtReqSubcategory.DefaultMessage = "";

            ////if image directory exist delete all files in it
            //global_courseImage = null;
            //deleteImageInServer(newCourseID);
        }
        else
        {
            //int len = ddtReqSubcategory.SelectedValue.Length;

            //if (len > 1)
            //    ddtReqSubcategory.SelectedValue = ddtReqSubcategory.SelectedValue.Substring(len - 1, len - 2);
            //else if (len == 1)
            //    ddtReqSubcategory.SelectedValue = ddtReqSubcategory.SelectedValue;

            //int len2 = ddtSubcategory.SelectedValue.Length;

            //if (len2 > 1)
            //    ddtSubcategory.SelectedValue = ddtSubcategory.SelectedValue.Substring(len2 - 1, len2 - 2);
            //else if (len2 == 1)
            //    ddtSubcategory.SelectedValue = ddtSubcategory.SelectedValue;

        }

        //txtCourseID.Text = newCourseID.ToString();

        RadTab courseManagement = RadTabStrip1.Tabs.FindTabByValue("1");
        //RadTab createCourse = RadTabStrip1.Tabs.FindTabByValue("2");
        RadTab courseCategories = RadTabStrip1.Tabs.FindTabByValue("3");
        RadTab classManagement = RadTabStrip1.Tabs.FindTabByValue("4");
        RadTab createClass = RadTabStrip1.Tabs.FindTabByValue("5");
        RadTab pageEditor = RadTabStrip1.Tabs.FindTabByValue("6");
        RadTab userManagement = RadTabStrip1.Tabs.FindTabByValue("7");
        RadTab forums = RadTabStrip1.Tabs.FindTabByValue("8");

        //rcbAdminMain.Visible = false;

        //CreateCourse
        //if (Request.QueryString["Tab"] != null && Request.QueryString["Tab"].ToString() == "CreateCourse")
        //{
        //    RadMultiPage1.SelectedIndex = 1;
        //    lblTab2.Text = "Create a Course";
        //    createCourse.Selected = true;
        //}

        //CourseCat
        if (Request.QueryString["Tab"] != null && Request.QueryString["Tab"].ToString() == "CourseCat")
        {
            RadMultiPage1.SelectedIndex = 2;
            courseCategories.Selected = true;
            lvCourseCategory.Rebind();
            GetCourseCatCount();

            lblTab2.Text = "Course Categories";
            lblTab3.Visible = true;
            lblTab3.Text = "Grid View";
        }

        //ClassMgmt
        else if (Request.QueryString["Tab"] != null && Request.QueryString["Tab"].ToString() == "ClassMgmt")
        {
            RadMultiPage1.SelectedIndex = 3;
            classManagement.Selected = true;
            lblTab2.Text = "Class Management";
        }
        //Create Class
        else if (Request.QueryString["Tab"] != null && Request.QueryString["Tab"].ToString() == "BuiltinClass")
        {
            RadMultiPage1.SelectedIndex = 4;
            createClass.Selected = true;
            lblTab2.Text = "Create a Class";
            item3.Visible = true;
            lblTab3.Text = "Built-in Classes";
        }
        else if (Request.QueryString["Tab"] != null && Request.QueryString["Tab"].ToString() == "ClassBuilder")
        {
            RadMultiPage1.SelectedIndex = 5;
            createClass.Selected = true;

            lblTab2.Text = "Create a Class";
            item3.Visible = true;
            lblTab3.Text = "Class Builder";
        }

        //Page Editor - Homepage Layout
        else if (Request.QueryString["Tab"] != null && Request.QueryString["Tab"].ToString() == "HomepageEditor")
        {
            RadMultiPage1.SelectedIndex = 6;
            pageEditor.Selected = true;

            lblTab2.Text = "Page Editor";
            item3.Visible = true;
            lblTab3.Text = "Homepage Layout";
            item4.Visible = false;
        }
        else if (Request.QueryString["Tab"] != null && Request.QueryString["Tab"].ToString() == "ImageSlider")
        {
            RadMultiPage1.SelectedIndex = 7;
            pageEditor.Selected = true;

            lblTab2.Text = "Page Editor";
            item3.Visible = true;
            lblTab3.Text = "Homepage Layout";
            item4.Visible = true;
            lblTab4.Text = "Image Slider";
        }
        else if (Request.QueryString["Tab"] != null && Request.QueryString["Tab"].ToString() == "FeaturedCourses")
        {
            RadMultiPage1.SelectedIndex = 8;
            pageEditor.Selected = true;

            lblTab2.Text = "Page Editor";
            item3.Visible = true;
            lblTab3.Text = "Homepage Layout";
            item4.Visible = true;
            lblTab4.Text = "Featured Courses";
        }
        else if (Request.QueryString["Tab"] != null && Request.QueryString["Tab"].ToString() == "LatestNews")
        {
            RadMultiPage1.SelectedIndex = 9;
            pageEditor.Selected = true;

            lblTab2.Text = "Page Editor";
            item3.Visible = true;
            lblTab3.Text = "Homepage Layout";
            item4.Visible = true;
            lblTab4.Text = "Latest News";
        }
        else if (Request.QueryString["Tab"] != null && Request.QueryString["Tab"].ToString() == "Testimonials")
        {
            RadMultiPage1.SelectedIndex = 10;
            pageEditor.Selected = true;

            lblTab2.Text = "Page Editor";
            item3.Visible = true;
            lblTab3.Text = "Homepage Layout";
            item4.Visible = true;
            lblTab4.Text = "Testimonials";
        }

        //Page Editor - Course Catalog
        else if (Request.QueryString["Tab"] != null && Request.QueryString["Tab"].ToString() == "MandatoryCourses")
        {
            RadMultiPage1.SelectedIndex = 11;
            pageEditor.Selected = true;

            lblTab2.Text = "Page Editor";
            item3.Visible = true;
            lblTab3.Text = "Course Catalog";
            item4.Visible = true;
            lblTab4.Text = "Mandatory Courses";
        }
        else if (Request.QueryString["Tab"] != null && Request.QueryString["Tab"].ToString() == "TrendingCourses")
        {
            RadMultiPage1.SelectedIndex = 12;
            pageEditor.Selected = true;

            lblTab2.Text = "Page Editor";
            item3.Visible = true;
            lblTab3.Text = "Course Catalog";
            item4.Visible = true;
            lblTab4.Text = "Trending Courses";
        }
        else if (Request.QueryString["Tab"] != null && Request.QueryString["Tab"].ToString() == "RecommendedCourses")
        {
            RadMultiPage1.SelectedIndex = 13;
            pageEditor.Selected = true;

            lblTab2.Text = "Page Editor";
            item3.Visible = true;
            lblTab3.Text = "Course Catalog";
            item4.Visible = true;
            lblTab4.Text = "Recommended Courses";

        }
        else if (Request.QueryString["Tab"] != null && Request.QueryString["Tab"].ToString() == "MustTakeCourses")
        {
            RadMultiPage1.SelectedIndex = 14;
            pageEditor.Selected = true;

            lblTab2.Text = "Page Editor";
            item3.Visible = true;
            lblTab3.Text = "Course Catalog";
            item4.Visible = true;
            lblTab4.Text = "Must Take Courses";
        }
        else if (Request.QueryString["Tab"] != null && Request.QueryString["Tab"].ToString() == "RecentCourses")
        {
            RadMultiPage1.SelectedIndex = 15;
            pageEditor.Selected = true;

            lblTab2.Text = "Page Editor";
            item3.Visible = true;
            lblTab3.Text = "Course Catalog";
            item4.Visible = true;
            lblTab4.Text = "Recently Added Courses";
        }

        //UserMgmt
        else if (Request.QueryString["Tab"] != null && Request.QueryString["Tab"].ToString() == "UserMgmt")
        {
            RadMultiPage1.SelectedIndex = 16;
            userManagement.Selected = true;

            lblTab2.Text = "User Management";
        }

        //Forums
        else if (Request.QueryString["Tab"] != null && Request.QueryString["Tab"].ToString() == "AdminForum")
        {
            RadMultiPage1.SelectedIndex = 17;
            lblTab2.Text = "Forums";
            forums.Selected = true;

            //if (Request.QueryString["forumaction"] != null && Request.QueryString["forumaction"].ToString() == "AddNewTopic")
            //{
            //    var courseTitle = DataHelper.getCourseTitleByID(Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"].ToString())));
            //    item3.Visible = true;
            //    lblTab3.Text = courseTitle;
            //    item4.Visible = true;
            //    //lblTab4.Text = DataHelper.getThreadTitleByID(Convert.ToInt32(Utils.Decrypt(Request.QueryString["ThreadID"].ToString())));
            //    lblTab4.Text = "Add New Topic";
            //    coursesForum.Visible = false;
            //    forumTopics.Visible = false;
            //    forumAddTopic.Visible = true;
            //    forumThreadView.Visible = false;
            //}
            if (Request.QueryString["CourseID"] != null && Request.QueryString["ThreadID"] == null)
            {
                var courseTitle = DataHelper.getCourseTitleByID(Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"].ToString())));
                item3.Visible = true;
                lblTab3.Text = courseTitle;
                coursesForum.Visible = false;
                forumTopics.Visible = true;
                //forumAddTopic.Visible = false;
                forumThreadView.Visible = false;
            }
            else if (Request.QueryString["ThreadID"] != null)
            {                
                var courseTitle = DataHelper.getCourseTitleByID(Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"].ToString())));
                var threadTitle = DataHelper.getThreadTitleByID(Convert.ToInt32(Utils.Decrypt(Request.QueryString["ThreadID"].ToString())));
                item3.Visible = true;                
                lblTab3.Text = courseTitle;
                item4.Visible = true;
                lblTab4.Text = threadTitle;
                coursesForum.Visible = false;
                forumTopics.Visible = false;
                //forumAddTopic.Visible = false;
                forumThreadView.Visible = true;
            }
            else
            {
                item3.Visible = false;
                item4.Visible = false;

                coursesForum.Visible = true;
                forumTopics.Visible = false;
                //forumAddTopic.Visible = false;
                forumThreadView.Visible = false;
            }
        }
        //CourseMgmt
        else if (Request.QueryString["Tab"] != null && Request.QueryString["Tab"].ToString() == "CourseMgmt")
        {
            //rcbAdminMain.Visible = true;
            courseManagement.Selected = true;
            RadMultiPage1.SelectedIndex = 0;
            lblTab2.Text = "Course Management";
            item3.Visible = true;
            lblTab3.Text = "Grid View";
        }

        else
        {
            RadMultiPage1.SelectedIndex = 18;
            lblTab2.Text = "Overview";
            item3.Visible = false;
        }
        Session["FromPage"] = "Admin";
        loadHeadersCount();
    }

    private void loadHeadersCount()
    {
        ltAllClasses.Text = "0";
        ltAllCourses.Text = "0";
        ltAllEnrollments.Text = "0";
        ltAllUsers.Text = Convert.ToString(getCreatedUsersCount());
        ltSiteTraffic.Text = "0";
        ltAllCourses.Text = Convert.ToString(getCreatedCourseCount());
        ltOverviewCreatedCourses.Text = Convert.ToString(getCreatedCourseCount());
        ltOverviewUsers.Text = Convert.ToString(getCreatedUsersCount());
    }

    #region AdminOverview
    private static IEnumerable<CourseDetails> _createdCourse;
    private static IEnumerable<userDetails> _userDetails;

    [System.Web.Services.WebMethod()]
    public static int getCreatedCourseCount()
    {
        return LoadCreatedCourse().Count();

    }
    public static IEnumerable<CourseDetails> LoadCreatedCourse()
    {
        TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();
        _createdCourse = db.vw_TranscomUniversity_AllCourses
                           .AsEnumerable()
                           .Select(c => new CourseDetails
                           {
                               EncryptedCourseID = Utils.Encrypt(c.CourseID),
                               CourseID = c.CourseID,
                               CourseImage = c.CourseImage,
                               CourseTitle = c.CourseTitle
                           })
                           .OrderBy(c => c.CourseTitle)
                           .ToList();
        return _createdCourse;
    }

    [System.Web.Services.WebMethod()]
    public static int getCreatedUsersCount()
    {
        return loadUsers().Count();

    }

    public static IEnumerable<userDetails> loadUsers()
    {
        TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();
        _userDetails = db.vw_TranscomUniversity_UsersInRoles
                         .AsEnumerable()
                         .Select(c => new userDetails
                         {
                             userID = Convert.ToString(c.UserId),
                             userEmail = c.UserName
                         })
                         .OrderBy(c => c.userEmail)
                         .ToList();

        return _userDetails;
    }


    private void fillChart()
    {

    }

    #endregion

    #region CourseManagement

    protected string Encrypt(string clearText)
    {
        string EncryptionKey = "TRNSCMV32017111";
        byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);

        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(clearBytes, 0, clearBytes.Length);
                    cs.Close();
                }
                clearText = Convert.ToBase64String(ms.ToArray());
            }
        }
        return clearText;
    }

    #region Load CourseLists

    private static IEnumerable<CourseDetails> _mandatoryCourse;
    private static IEnumerable<CourseDetails> _trendingCourse;
    private static IEnumerable<CourseDetails> _recentCourse;

    //--Start Mandatory Course Page Methods
    [System.Web.Services.WebMethod()]
    public static IEnumerable<CourseDetails> GetManadatoryCourseData(int startRowIndex, int maximumRows, string sortExpression)
    {
        return RadGrid.GetBindingData(LoadMandatoryCourse().AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data;
    }

    [System.Web.Services.WebMethod()]
    public static int GetMandatoryCount()
    {
        return LoadMandatoryCourse().Count();

    }

    [System.Web.Services.WebMethod()]
    public static IEnumerable<CourseDetails> LoadMandatoryCourse()
    {
        TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();

        _mandatoryCourse = db.vw_TranscomUniversity_MandatoryCourses
                            .AsEnumerable()
                            .Select(c => new CourseDetails
                            {
                                EncryptedCourseID = Utils.Encrypt(c.CourseID),
                                CourseID = c.CourseID,
                                CourseImage = c.CourseImage,
                                CourseTitle = c.CourseTitle,
                                CourseType = c.CourseType,
                                CourseDuration = c.CourseDuration,
                                CourseCategory = c.CourseCategory,
                                CourseSubcategory = c.CourseSubcategory,
                                CourseDescription = c.CourseDescription,
                                DateLastModified = string.Format("{0: yyyy/MM/dd hh:mm:ss tt}", c.DateLastModified),
                                CoursePath = c.CoursePath
                            })
                            .OrderBy(c => c.CourseTitle)
                            .ToList();
        //string json = new JavaScriptSerializer().Serialize(mandatoryCourse);
        return _mandatoryCourse;

    }

    //--Start Trending Course Page Methods
    [System.Web.Services.WebMethod()]
    public static IEnumerable<CourseDetails> GetTrendingCourseData(int startRowIndex, int maximumRows, string sortExpression)
    {
        return RadGrid.GetBindingData(LoadTrendingCourse().AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data;
    }

    [System.Web.Services.WebMethod()]
    public static int GetTrendingCount()
    {
        return LoadTrendingCourse().Count();

    }

    [System.Web.Services.WebMethod()]
    public static IEnumerable<CourseDetails> LoadTrendingCourse()
    {
        TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();

        _trendingCourse = db.vw_TranscomUniversity_TrendingCourses
                            .AsEnumerable()
                            .Select(c => new CourseDetails
                            {
                                EncryptedCourseID = Utils.Encrypt(c.CourseID),
                                CourseID = c.CourseID,
                                CourseImage = c.CourseImage,
                                CourseTitle = c.CourseTitle,
                                CourseType = c.CourseType,
                                CourseDuration = c.CourseDuration,
                                CourseCategory = c.CourseCategory,
                                CourseSubcategory = c.CourseSubcategory,
                                CourseDescription = c.CourseDescription,
                                DateLastModified = string.Format("{0: yyyy/MM/dd hh:mm:ss tt}", c.DateLastModified),
                                CoursePath = c.CoursePath
                            })
                            .OrderBy(c => c.CourseTitle)
                            .ToList();
        //string json = new JavaScriptSerializer().Serialize(mandatoryCourse);
        return _trendingCourse;

    }

    //--Start Recently Added Course Page Methods
    [System.Web.Services.WebMethod()]
    public static IEnumerable<CourseDetails> GetRecentCourseData(int startRowIndex, int maximumRows, string sortExpression)
    {
        return RadGrid.GetBindingData(LoadRecentCourse().AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data;
    }

    [System.Web.Services.WebMethod()]
    public static int GetRecentCount()
    {
        return LoadRecentCourse().Count();

    }

    [System.Web.Services.WebMethod()]
    public static IEnumerable<CourseDetails> LoadRecentCourse()
    {
        TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();

        _recentCourse = db.pr_TranscomUniversity_RecentlyAddedCourse()
                                .AsEnumerable()
                                .Select(c => new CourseDetails
                                {
                                    EncryptedCourseID = Utils.Encrypt(c.CourseID),
                                    CourseID = c.CourseID,
                                    CourseImage = c.CourseImage,
                                    CourseTitle = c.CourseTitle,
                                    CourseType = c.CourseType,
                                    CourseDuration = c.CourseDuration,
                                    CourseCategory = c.CourseCategory,
                                    CourseSubcategory = c.CourseSubcategory,
                                    CourseDescription = c.CourseDescription,
                                    DateLastModified = string.Format("{0: yyyy/MM/dd hh:mm:ss tt}", c.DateLastModified),
                                    CoursePath = c.CoursePath
                                })
                                .ToList();
        //string json = new JavaScriptSerializer().Serialize(mandatoryCourse);
        return _recentCourse;

    }

    #endregion

    #endregion

    //#region CreateCourse

    //protected void btnSaveCreateCourse_Click(object sender, EventArgs e)
    //{
    //    //int len2 = ddtSubcategory.SelectedValue.Length;
    //    //if (len2 > 1)
    //    //{
    //    //    ddtSubcategory.SelectedValue = ddtSubcategory.SelectedValue.Substring(len2 - 1, len2 - 2);
    //    //}
    //    //else if (len2 == 1)
    //    //{
    //    //    ddtSubcategory.SelectedValue = ddtSubcategory.SelectedValue;
    //    //}
    //    if (dpStartDateTime.SelectedDate != null)
    //    {
    //        if (Page.IsValid)
    //        {
    //            DateTime defaultMax = new DateTime(9999, 12, 31, 11, 59, 59);
    //            int dateValid = DateTime.Compare(Convert.ToDateTime(dpStartDateTime.SelectedDate), dpEndDateTime.SelectedDate == null ? defaultMax : Convert.ToDateTime(dpEndDateTime.SelectedDate));

    //            if (dateValid < 0)
    //            {
    //                int courseId = newCourseID;
    //                var courseImage = global_courseImage == null ? "No_image.jpg" : global_courseImage;

    //                if (courseImage != "error")
    //                {
    //                    int courseTypeID = Convert.ToInt32(rcbCourseType.SelectedValue);
    //                    string courseTitle = txtCourseTitle.Text.Trim();
    //                    int categoryID = Convert.ToInt32(rcbCategory.SelectedValue == "" ? "0" : rcbCategory.SelectedValue);
    //                    //int subCategoryID = Convert.ToInt32(ddtSubcategory.SelectedValue == "" ? "0" : ddtSubcategory.SelectedValue);
    //                    //****
    //                    string[] subcats = ddtSubcategory.SelectedValue.Split(',');
    //                    int subCategoryID = Convert.ToInt32(subcats[subcats.Length - 1].Trim());
    //                    //**
    //                    string description = txtDescription.Text.Trim();
    //                    DateTime dateCreated = Convert.ToDateTime(txtDateCreated.Text);
    //                    DateTime startDate = Convert.ToDateTime(dpStartDateTime.SelectedDate);
    //                    DateTime endDate = dpEndDateTime.SelectedDate.ToString() == "" ? defaultMax : Convert.ToDateTime(dpEndDateTime.SelectedDate);
    //                    int duration = Convert.ToInt32(ddlDuration.SelectedValue == "" ? "0" : ddlDuration.SelectedValue);
    //                    string AuthorCim = txtAuthor.Text.Trim();
    //                    string department = rcbDepartment.SelectedValue == "" ? "0" : rcbDepartment.SelectedValue;
    //                    int reqProgramID = Convert.ToInt32(rcbReqProgram.SelectedValue == "" ? "0" : rcbReqProgram.SelectedValue);
    //                    int reqCourseID;
    //                    string level = "";

    //                    var levelList = rcbLevel.CheckedItems;
    //                    level = string.Join(",", levelList.Select(l => l.Value));

    //                    if (rcbReqCourse.SelectedValue == "-1" || rcbReqCourse.SelectedValue == "")
    //                    {
    //                        reqCourseID = 0;
    //                    }
    //                    else
    //                    {
    //                        reqCourseID = Convert.ToInt32(rcbReqCourse.SelectedValue);
    //                    }

    //                    int yearsInTenure = Convert.ToInt32(ddlTenureYears.SelectedValue);
    //                    int yearsInRole = Convert.ToInt32(ddlRoleYears.SelectedValue);

    //                    int publishedBy = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
    //                    string accessMode = ddlAccessType.SelectedValue;

    //                    DataHelper.InsertCourse(courseTitle, categoryID, subCategoryID, description, dateCreated, startDate, endDate, publishedBy, duration, AuthorCim, department, reqProgramID, yearsInTenure, yearsInRole, accessMode, courseImage, courseTypeID, level);

    //                    IList<RadListBoxItem> collection = lbAssignedCourses.Items;
    //                    if (collection.Count > 0)
    //                    {
    //                        List<int> selectedReqCourse = new List<int>();
    //                        string str = "";
    //                        foreach (RadListBoxItem item in collection)
    //                        {
    //                            selectedReqCourse.Add(Convert.ToInt32(item.Value));
    //                            str += item + ",";
    //                        }
    //                        DataHelper.insertRequiredCourse(courseId, selectedReqCourse);
    //                    }
    //                    clearCourseInput();
    //                    //string script = "function f(){openSuccessModal(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
    //                    //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, true);
    //                    ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", Utils.callClientScript("redirectToCourseBuilder", Utils.Encrypt(courseId)), true);
    //                    //Response.Redirect("CourseBuilder.aspx?CourseID=" + );
    //                }
    //            }
    //            else
    //            {
    //                RadWindowManager1.RadAlert("End Date should", 330, 180, "Error Start Date", "");
    //                //string script = "function f(){openErrorDateModal(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
    //                //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, true);
    //            }
    //        }
    //    }
    //    else
    //    {

    //        RadWindowManager1.RadAlert(" Start Date is not defined!", 330, 180, "Error Start Date", "");
    //        //string script = "function f(){noStartDateError(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
    //        //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, true);
    //    }
    //}

    //void clearCourseInput()
    //{
    //    txtCourseTitle.Text = "";
    //    newCourseID = lastInsertedCourseID() + 1;
    //    txtCourseID.Text = newCourseID.ToString();
    //    rcbCategory.ClearSelection();
    //    ddtSubcategory.Entries.Clear();
    //    txtDescription.Text = "";
    //    txtDateCreated.Text = serverDate.ToShortDateString();
    //    dpStartDateTime.SelectedDate = null;
    //    dpEndDateTime.SelectedDate = null;
    //    ddlDuration.SelectedIndex = 0;
    //    //rcbAuthor.ClearSelection();
    //    txtAuthor.Text = "";
    //    rcbDepartment.ClearSelection();
    //    rcbReqProgram.ClearSelection();
    //    rcbReqProgram.Text = "";
    //    rcbReqCourse.ClearSelection();
    //    rcbReqCourse.SelectedIndex = 0;
    //    ddtReqSubcategory.Entries.Clear();
    //    lbAvailableCourses.Items.Clear();
    //    lbAssignedCourses.Items.Clear();
    //    ddlRoleYears.SelectedIndex = 0;
    //    ddlTenureYears.SelectedIndex = 0;
    //    ddlAccessType.SelectedIndex = 0;
    //    CourseImage.UploadedFiles.Clear();
    //    rcbCourseType.ClearSelection();
    //    rcbLevel.ClearCheckedItems();
    //    rcbLevel.ClearSelection();
    //}

    //protected string imageCourse(int courseID, UploadedFile f)
    //{

    //    string error = null;

    //    if (string.IsNullOrWhiteSpace(error))
    //    {
    //        string uploadFolder;
    //        var userCim = DataHelper.GetCurrentUserCIM();

    //        if (HttpContext.Current.Request.Url.Host == "localhost")
    //            uploadFolder = Request.PhysicalApplicationPath + "Media\\Uploads\\CourseImg\\" + courseID + "\\";
    //        else
    //            uploadFolder = HttpRuntime.AppDomainAppPath + "Media\\Uploads\\CourseImg\\" + courseID + "\\";

    //        string directoryPath = Server.MapPath(string.Format("~/{0}/", "Media/Uploads/CourseImg/" + courseID + "\\"));

    //        if (!Directory.Exists(directoryPath))
    //            Directory.CreateDirectory(directoryPath);

    //        if (f.ContentType == "image/jpeg" || f.ContentType == "image/png" || f.ContentType == "image/jpg")
    //        {
    //            if (f.ContentLength < 1024000000)
    //            {
    //                Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
    //                string extension = Path.GetExtension(f.FileName);
    //                f.SaveAs(uploadFolder + f.FileName, true);
    //                //f.SaveAs(uploadFolder + unixTimestamp.ToString() + extension, true);
    //                //global_courseImage = unixTimestamp.ToString() + extension;
    //                global_courseImage = f.FileName;
    //            }
    //            else
    //            {
    //                global_courseImage = "error";
    //                RadWindowManager1.RadAlert("  File size is too large, please select another one!", 330, 180, "Image too Large", "");
    //            }
    //        }
    //        else
    //        {
    //            global_courseImage = "error";
    //            RadWindowManager1.RadAlert("Error file format please select another one!", 330, 180, "Invalid File Format", "");
    //        }
    //    }

    //    return global_courseImage;
    //}

    //protected void rcbCategory_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    ddtSubcategory.Enabled = true;
    //    ddtSubcategory.Entries.Clear();
    //    int selectCategoryValue = Convert.ToInt32(rcbCategory.SelectedValue);
    //    LoadSubcategories(selectCategoryValue);

    //    if (selectCategoryValue == 17)
    //        pnlLevelContainer.Visible = true;
    //    else
    //        pnlLevelContainer.Visible = false;
    //}

    //protected void LoadSubcategories(int categoryID)
    //{
    //    ddtSubcategory.DataTextField = "Subcategory";
    //    ddtSubcategory.DataValueField = "SubcategoryID";
    //    ddtSubcategory.DataSource = DataHelper.GetSubcategory(categoryID);
    //    ddtSubcategory.DefaultMessage = "Select Subcategory";
    //    try
    //    {

    //        ddtSubcategory.DataBind();
    //    }
    //    catch (ArgumentOutOfRangeException aore)
    //    {
    //        RadWindowManager1.RadAlert("Something went wrong. Please reload the page and try again.", 330, 180, "Error", "");


    //    }
    //}

    //private int lastInsertedCourseID()
    //{
    //    string constr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
    //    string getQuery = "SELECT MAX(CourseID) AS lastCourseID FROM [dbo].[tbl_TranscomUniversity_Cor_Course]";
    //    int lastCourseID;

    //    using (SqlConnection con = new SqlConnection(constr))
    //    {
    //        using (SqlCommand cmd = new SqlCommand(getQuery))
    //        {
    //            try
    //            {
    //                cmd.Connection = con;
    //                con.Open();
    //                lastCourseID = Convert.ToInt32(cmd.ExecuteScalar());
    //                con.Close();
    //            }
    //            catch (Exception)
    //            {
    //                lastCourseID = 0;
    //            }
    //        }
    //    }

    //    return lastCourseID;
    //}

    //private static bool IsImage(HttpPostedFile file)
    //{
    //    return ((file != null) && System.Text.RegularExpressions.Regex.IsMatch(file.ContentType, "image/\\S+") && (file.ContentLength > 0));
    //}

    //protected void LoadReqSubcategories(int categoryID)
    //{
    //    ddtReqSubcategory.DataSource = DataHelper.GetSubcategory(categoryID);
    //    ddtReqSubcategory.DefaultMessage = "-Select Subcategory-";
    //    ddtReqSubcategory.DefaultValue = "-1";
    //    try
    //    {
    //        ddtReqSubcategory.DataBind();
    //    }
    //    catch (ArgumentOutOfRangeException aore)
    //    {

    //    }

    //}

    //protected void rcbReqCourse_IndexChanged(object sender, EventArgs e)
    //{

    //    if (rcbReqCourse.SelectedValue == "-1")
    //    {
    //        //string script = "function f(){hideRequiredCoursePanel(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
    //        //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, true);
    //        lbAvailableCourses.Items.Clear();
    //        lbAssignedCourses.Items.Clear();
    //        ddtReqSubcategory.Entries.Clear();
    //    }
    //    else
    //    {
    //        //string script = "function f(){showPanel('panelSubReqCategory'); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
    //        //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, true);
    //        ddtReqSubcategory.Entries.Clear();
    //        int selectCategoryValue = Convert.ToInt32(rcbReqCourse.SelectedValue);
    //        LoadReqSubcategories(selectCategoryValue);
    //    }

    //}

    //protected void btnSelectCourse_Click(object sender, EventArgs e)
    //{
    //    //load data on rad window
    //    //show rad window
    //    int categoryid = 0;
    //    int subcategoryid = 0;
    //    try
    //    {
    //        categoryid = Convert.ToInt32(rcbReqCourse.SelectedValue);
    //        int len = ddtReqSubcategory.SelectedValue.Length;
    //        if (len > 1)
    //            subcategoryid = Convert.ToInt32(ddtReqSubcategory.SelectedValue.Substring(len - 1, len - 2));
    //        else if (len == 1)
    //            subcategoryid = Convert.ToInt32(ddtReqSubcategory.SelectedValue);


    //    }
    //    catch (Exception error)
    //    {
    //        categoryid = 0;
    //        subcategoryid = 0;
    //    }
    //    finally
    //    {
    //        lbAvailableCourses.DataValueField = "CourseID";
    //        lbAvailableCourses.DataTextField = "Title";
    //        lbAvailableCourses.DataSource = DataHelper.GetFilterCourses(categoryid, subcategoryid);
    //        lbAvailableCourses.DataBind();

    //    }
    //    IList<RadListBoxItem> assignedList = lbAssignedCourses.Items;
    //    IList<RadListBoxItem> availableList = lbAvailableCourses.Items;
    //    foreach (RadListBoxItem a in assignedList)
    //    {
    //        foreach (RadListBoxItem b in availableList)
    //        {
    //            if (a.Value == b.Value)
    //            {
    //                b.Checked = true;
    //            }


    //        }
    //    }
    //    string script = "function f(){openCourseWindow(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
    //    ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", script, true);

    //}

    //protected void btnAddCourses_Click(object sender, EventArgs e)
    //{
    //    IList<RadListBoxItem> collection = lbAvailableCourses.CheckedItems;

    //    if (collection.Count > 0)
    //    {
    //        foreach (RadListBoxItem item in collection)
    //        {
    //            lbAvailableCourses.Transfer(item, lbAvailableCourses, lbAssignedCourses);
    //        }
    //        string script = "function f(){showPanel('panelAssignedCourse'); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
    //        ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", script, true);
    //    }
    //    else
    //    {
    //        string script = "function f(){hidePanel('panelAssignedCourse'); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
    //        ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", script, true);
    //    }
    //    ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox", "ClosePrerequisiteForm()", true);
    //}

    //protected void DateValidate(object source, ServerValidateEventArgs args)
    //{
    //    args.IsValid = (args.Value == "");
    //}

    ////protected void lbtnCancelCreateCourse_Click(object sender, EventArgs e)
    ////{
    ////    Response.Redirect("Admin.aspx");
    ////}

    //protected void upload_FileUploaded(object sender, FileUploadedEventArgs e)
    //{

    //    if (e.IsValid)
    //    {
    //        imageCourse(newCourseID, e.File);
    //        courseImagePreview.ImageUrl = "~/Media/Uploads/CourseImg/" + newCourseID + "/" + global_courseImage;
    //    }
    //    else
    //    {
    //        CourseImage.UploadedFiles.Clear();
    //        RadWindowManager1.RadAlert("Invalid File. Allowed Files (jpeg,jpg,gif,png)", null, null, "Error Message", "");

    //    }

    //}

    //protected void btnRemovePreviewImg_Click(object sender, EventArgs e)
    //{

    //    //courseImagePreview.DataValue = null;
    //    global_courseImage = "No_image.jpg";
    //    courseImagePreview.ImageUrl = "";
    //    deleteImageInServer(newCourseID);
    //    ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", Utils.callClientScript("hideAllLocalLoading"), true);

    //}

    //private void deleteImageInServer(int courseID)
    //{
    //    try
    //    {
    //        var folderPath = Server.MapPath("Media/Uploads/CourseImg/" + courseID + "\\");
    //        System.IO.DirectoryInfo folderInfo = new DirectoryInfo(folderPath);

    //        foreach (FileInfo file in folderInfo.GetFiles())
    //        {
    //            file.Delete();
    //        }
    //    }
    //    catch { }

    //}

    ////protected void validateStartEndDate(object sender, ServerValidateEventArgs  e) 
    ////{
    ////      var startDate = dpStartDateTime.SelectedDate;
    ////      var endDate = dpEndDateTime.SelectedDate;
    ////        endDate = endDate == null ? new DateTime(9999, 12, 30) : endDate;
    ////        e.IsValid = (endDate > startDate);
    ////}

    //#endregion

    #region CourseCategories

    public void GetCourseCatCount()
    {
        var db = new TranscomUniversityV3ModelContainer();

        var countCourseCategory = (from c in db.vw_TranscomUniversity_Category
                                   select c).Count().ToString();

        ltCourseCategory.Text = countCourseCategory;
    }

    protected void lvCourseCategory_NeedDataSource(object source, RadListViewNeedDataSourceEventArgs e)
    {
        if (e.RebindReason == Telerik.Web.UI.RadListViewRebindReason.ExplicitRebind || e.RebindReason == Telerik.Web.UI.RadListViewRebindReason.PostBackEvent)
        {
            var courseCategory = DataHelper.GetCourseCategory();

            if (courseCategory != null)
            {
                lvCourseCategory.DataSource = courseCategory;
            }
        }
    }

    protected void gvCourseCategory_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        var courseCategory = DataHelper.GetCourseCategory();

        if (courseCategory != null)
            gvCourseCategory.DataSource = courseCategory;
    }

    protected void btnSwitchCatListView_Click(object sender, EventArgs e)
    {
        btnSwitchCatGridView.Visible = true;
        pnlCatGridView.Visible = true;

        gvCourseCategory.Rebind();

        btnSwitchCatListView.Visible = false;
        pnlCatListView.Visible = false;

        lblTab3.Text = "List View";
    }

    protected void btnSwitchCatGridView_Click(object sender, EventArgs e)
    {
        btnSwitchCatListView.Visible = true;
        pnlCatListView.Visible = true;

        lvCourseCategory.Rebind();

        btnSwitchCatGridView.Visible = false;
        pnlCatGridView.Visible = false;

        lblTab3.Text = "Grid View";
    }

    protected void rcmCourseCategory_Click(object sender, RadMenuEventArgs e)
    {
        var itemVal = e.Item.Value;

        if (itemVal == "1")
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "addCategories", DataHelper.callClientScript("addCategories"), true);
    }

    protected void btnAddCategory_Click(object sender, EventArgs e)
    {
        try
        {

            var createdBy = DataHelper.GetCurrentUserCIM();
            var db = new TranscomUniversityV3ModelContainer();

            //Logger variables Start
            string _userIP = DataHelper.GetIPAddress();
            string _userID = DataHelper.GetCurrentUserCIM();
            double _duration = 0;

            DateTime startTime;
            DateTime endTime;

            var dts = db.CreateQuery<DateTime>("CurrentDateTime() ");
            startTime = dts.AsEnumerable().First();
            //Logger variables End

            pr_TranscomUniversity_InsertCategory_Result retVal = db.pr_TranscomUniversity_InsertCategory(txtCategoryName.Text.Trim(), txtCategoryDesc.Text, Convert.ToInt32(createdBy)).SingleOrDefault();


            //logger start transaction
            string _action = "Category " + txtCategoryName.Text.Trim() + " was created by " + _userID;

            var dte = db.CreateQuery<DateTime>("CurrentDateTime()");

            endTime = dte.AsEnumerable().First();
            _duration = (endTime - startTime).TotalMilliseconds;

            int l_actionID = DataHelper.getLastLogID() + 1;
            DataHelper.logCategory(Convert.ToInt32(retVal.CategoryID), l_actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);
            //logger end transaction

            if (retVal.CategoryID.ToString() != "0")
            {
                RadWindowManager1.RadAlert("Category successfully added.", 330, 180, "Success", "");

                lvCourseCategory.Rebind();
                gvCourseCategory.Rebind();
                GetCourseCatCount();
            }
            else
                RadWindowManager1.RadAlert("Category name already exists. Please try another name.", 330, 180, "Error", "");

            txtCategoryName.Text = "";
            txtCategoryDesc.Text = "";
        }
        catch
        {
            RadWindowManager1.RadAlert("Error adding category. Please contact your System Administrator.", 330, 180, "Error", "");
        }
    }

    protected void btnCatDelete_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(Page, typeof(Page), "deleteCategories", DataHelper.callClientScript("deleteCategories"), true);
    }

    protected void gvDeleteCategory_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        var courseCategory = DataHelper.GetCourseCategory();

        if (courseCategory != null)
            gvDeleteCategory.DataSource = courseCategory;
    }

    protected void gvDeleteCategory_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == RadGrid.DeleteCommandName)
        {
            var deletedItem = e.Item as GridEditableItem;

            try
            {
                var catId = deletedItem.GetDataKeyValue("CategoryID");
                var db = new TranscomUniversityV3ModelContainer();

                //Logger variables Start
                string _userIP = DataHelper.GetIPAddress();
                string _userID = DataHelper.GetCurrentUserCIM();
                double _duration = 0;

                DateTime startTime;
                DateTime endTime;

                var dts = db.CreateQuery<DateTime>("CurrentDateTime() ");
                startTime = dts.AsEnumerable().First();
                //Logger variables End

                db.pr_TranscomUniversity_DeleteCategory(Convert.ToInt32(catId));

                //logger start transaction
                string _action = "Category " + txtCategoryName.Text.Trim() + " was deleted by " + _userID;

                var dte = db.CreateQuery<DateTime>("CurrentDateTime()");

                endTime = dte.AsEnumerable().First();
                _duration = (endTime - startTime).TotalMilliseconds;

                int l_actionID = DataHelper.getLastLogID() + 1;
                DataHelper.logCategory(Convert.ToInt32(catId), l_actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);
                //logger end transaction

                gvDeleteCategory.Rebind();
                lvCourseCategory.Rebind();
                gvCourseCategory.Rebind();
                GetCourseCatCount();
            }
            catch
            {
                RadWindowManager1.RadAlert("Error deleting category. Please contact your System Administrator.", 330, 180, "Error", "");
            }
        }
    }

    #endregion

    #region ClassManagement

    ////DataSource
    //protected void gvClassNewHire_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    //{
    //    var classNewHire = DataHelper.GetClassNewHire();

    //    if (classNewHire != null)
    //        gvClassNewHire.DataSource = classNewHire;
    //}

    //protected void gvClassCrossTraining_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    //{
    //    var classCrossTraining = DataHelper.GetClassCrossTraining();

    //    if (classCrossTraining != null)
    //        gvClassCrossTraining.DataSource = classCrossTraining;
    //}

    //protected void gvClassRefresherTraining_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    //{
    //    var classRefresherTraining = DataHelper.GetClassRefresherTraining();

    //    if (classRefresherTraining != null)
    //        gvClassRefresherTraining.DataSource = classRefresherTraining;
    //}

    //protected void gvClassUpTraining_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    //{
    //    var classUpTraining = DataHelper.GetClassUpTraining();

    //    if (classUpTraining != null)
    //        gvClassUpTraining.DataSource = classUpTraining;
    //}

    //protected void gvClassProfessionalDevelopment_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    //{
    //    var classProfessionalDevelopment = DataHelper.GetClassProfessionalDevelopment();

    //    if (classProfessionalDevelopment != null)
    //        gvClassProfessionalDevelopment.DataSource = classProfessionalDevelopment;
    //}

    ////ViewAll
    //protected void btnViewAllNH_Click(object sender, EventArgs e)
    //{
    //    gvClassNewHire.Height = Unit.Pixel(770);
    //    gvClassNewHire.PageSize = 20;
    //    gvClassNewHire.Rebind();

    //    btnViewAllNH.Visible = false;
    //    btnViewLessNH.Visible = true;

    //    pnlCrossTraining.Visible = false;
    //    pnlRefresherTraining.Visible = false;
    //    pnlUpTraining.Visible = false;
    //    pnlProfessionalDevelopment.Visible = false;
    //}

    //protected void btnViewAllCT_Click(object sender, EventArgs e)
    //{
    //    gvClassCrossTraining.Height = Unit.Pixel(770);
    //    gvClassCrossTraining.PageSize = 20;
    //    gvClassCrossTraining.Rebind();

    //    btnViewAllCT.Visible = false;
    //    btnViewLessCT.Visible = true;

    //    pnlNewHire.Visible = false;
    //    pnlRefresherTraining.Visible = false;
    //    pnlUpTraining.Visible = false;
    //    pnlProfessionalDevelopment.Visible = false;
    //}

    //protected void btnViewAllRT_Click(object sender, EventArgs e)
    //{
    //    gvClassRefresherTraining.Height = Unit.Pixel(770);
    //    gvClassRefresherTraining.PageSize = 20;
    //    gvClassRefresherTraining.Rebind();

    //    btnViewAllRT.Visible = false;
    //    btnViewLessRT.Visible = true;

    //    pnlNewHire.Visible = false;
    //    pnlCrossTraining.Visible = false;
    //    pnlUpTraining.Visible = false;
    //    pnlProfessionalDevelopment.Visible = false;
    //}

    //protected void btnViewAllUT_Click(object sender, EventArgs e)
    //{
    //    gvClassUpTraining.Height = Unit.Pixel(770);
    //    gvClassUpTraining.PageSize = 20;
    //    gvClassUpTraining.Rebind();

    //    btnViewAllUT.Visible = false;
    //    btnViewLessUT.Visible = true;

    //    pnlNewHire.Visible = false;
    //    pnlCrossTraining.Visible = false;
    //    pnlRefresherTraining.Visible = false;
    //    pnlProfessionalDevelopment.Visible = false;
    //}

    //protected void btnViewAllPD_Click(object sender, EventArgs e)
    //{
    //    gvClassProfessionalDevelopment.Height = Unit.Pixel(770);
    //    gvClassProfessionalDevelopment.PageSize = 20;
    //    gvClassProfessionalDevelopment.Rebind();

    //    btnViewAllPD.Visible = false;
    //    btnViewLessPD.Visible = true;

    //    pnlNewHire.Visible = false;
    //    pnlCrossTraining.Visible = false;
    //    pnlRefresherTraining.Visible = false;
    //    pnlUpTraining.Visible = false;
    //}

    ////ViewLess
    //protected void btnViewLessNH_Click(object sender, EventArgs e)
    //{
    //    gvClassNewHire.Height = Unit.Pixel(370);
    //    gvClassNewHire.PageSize = 5;
    //    gvClassNewHire.Rebind();

    //    btnViewAllNH.Visible = true;
    //    btnViewLessNH.Visible = false;

    //    pnlNewHire.Visible = true;
    //    pnlCrossTraining.Visible = true;
    //    pnlRefresherTraining.Visible = true;
    //    pnlUpTraining.Visible = true;
    //    pnlProfessionalDevelopment.Visible = true;
    //}

    //protected void btnViewLessCT_Click(object sender, EventArgs e)
    //{
    //    gvClassCrossTraining.Height = Unit.Pixel(370);
    //    gvClassCrossTraining.PageSize = 5;
    //    gvClassCrossTraining.Rebind();

    //    btnViewAllCT.Visible = true;
    //    btnViewLessCT.Visible = false;

    //    pnlNewHire.Visible = true;
    //    pnlCrossTraining.Visible = true;
    //    pnlRefresherTraining.Visible = true;
    //    pnlUpTraining.Visible = true;
    //    pnlProfessionalDevelopment.Visible = true;
    //}

    //protected void btnViewLessRT_Click(object sender, EventArgs e)
    //{
    //    gvClassRefresherTraining.Height = Unit.Pixel(370);
    //    gvClassRefresherTraining.PageSize = 5;
    //    gvClassRefresherTraining.Rebind();

    //    btnViewAllRT.Visible = true;
    //    btnViewLessRT.Visible = false;

    //    pnlNewHire.Visible = true;
    //    pnlCrossTraining.Visible = true;
    //    pnlRefresherTraining.Visible = true;
    //    pnlUpTraining.Visible = true;
    //    pnlProfessionalDevelopment.Visible = true;
    //}

    //protected void btnViewLessUT_Click(object sender, EventArgs e)
    //{
    //    gvClassUpTraining.Height = Unit.Pixel(370);
    //    gvClassUpTraining.PageSize = 5;
    //    gvClassUpTraining.Rebind();

    //    btnViewAllUT.Visible = true;
    //    btnViewLessUT.Visible = false;

    //    pnlNewHire.Visible = true;
    //    pnlCrossTraining.Visible = true;
    //    pnlRefresherTraining.Visible = true;
    //    pnlUpTraining.Visible = true;
    //    pnlProfessionalDevelopment.Visible = true;
    //}

    //protected void btnViewLessPD_Click(object sender, EventArgs e)
    //{
    //    gvClassProfessionalDevelopment.Height = Unit.Pixel(370);
    //    gvClassProfessionalDevelopment.PageSize = 5;
    //    gvClassProfessionalDevelopment.Rebind();

    //    btnViewAllPD.Visible = true;
    //    btnViewLessPD.Visible = false;

    //    pnlNewHire.Visible = true;
    //    pnlCrossTraining.Visible = true;
    //    pnlRefresherTraining.Visible = true;
    //    pnlUpTraining.Visible = true;
    //    pnlProfessionalDevelopment.Visible = true;
    //}

    ////Filter
    //protected void ddlTrainingType_SelectedIndexChanged(Object sender, EventArgs e)
    //{
    //    if (ddlTrainingType.SelectedValue == "0")
    //    {
    //        pnlNewHire.Visible = true;
    //        pnlCrossTraining.Visible = true;
    //        pnlRefresherTraining.Visible = true;
    //        pnlUpTraining.Visible = true;
    //        pnlProfessionalDevelopment.Visible = true;
    //    }

    //    if (ddlTrainingType.SelectedValue == "1")
    //    {
    //        pnlNewHire.Visible = true;
    //        pnlCrossTraining.Visible = false;
    //        pnlRefresherTraining.Visible = false;
    //        pnlUpTraining.Visible = false;
    //        pnlProfessionalDevelopment.Visible = false;
    //    }

    //    if (ddlTrainingType.SelectedValue == "2")
    //    {
    //        pnlNewHire.Visible = false;
    //        pnlCrossTraining.Visible = true;
    //        pnlRefresherTraining.Visible = false;
    //        pnlUpTraining.Visible = false;
    //        pnlProfessionalDevelopment.Visible = false;
    //    }

    //    if (ddlTrainingType.SelectedValue == "3")
    //    {
    //        pnlNewHire.Visible = false;
    //        pnlCrossTraining.Visible = false;
    //        pnlRefresherTraining.Visible = true;
    //        pnlUpTraining.Visible = false;
    //        pnlProfessionalDevelopment.Visible = false;
    //    }

    //    if (ddlTrainingType.SelectedValue == "4")
    //    {
    //        pnlNewHire.Visible = false;
    //        pnlCrossTraining.Visible = false;
    //        pnlRefresherTraining.Visible = false;
    //        pnlUpTraining.Visible = true;
    //        pnlProfessionalDevelopment.Visible = false;
    //    }

    //    if (ddlTrainingType.SelectedValue == "5")
    //    {
    //        pnlNewHire.Visible = false;
    //        pnlCrossTraining.Visible = false;
    //        pnlRefresherTraining.Visible = false;
    //        pnlUpTraining.Visible = false;
    //        pnlProfessionalDevelopment.Visible = true;
    //    }
    //}

    //protected void ddlStatus_SelectedIndexChanged(Object sender, EventArgs e)
    //{
    //    if (ddlStatus.SelectedValue == "0")
    //    {
    //        var newHire = DataHelper.GetClassNewHire();
    //        var crossTraining = DataHelper.GetClassCrossTraining();
    //        var refresherTraining = DataHelper.GetClassRefresherTraining();
    //        var upTraining = DataHelper.GetClassUpTraining();
    //        var profDevelopment = DataHelper.GetClassProfessionalDevelopment();

    //        if (newHire != null)
    //        {
    //            gvClassNewHire.DataSource = newHire;
    //            gvClassNewHire.Rebind();
    //        }

    //        if (crossTraining != null)
    //        {
    //            gvClassCrossTraining.DataSource = crossTraining;
    //            gvClassCrossTraining.Rebind();
    //        }

    //        if (refresherTraining != null)
    //        {
    //            gvClassRefresherTraining.DataSource = refresherTraining;
    //            gvClassRefresherTraining.Rebind();
    //        }

    //        if (upTraining != null)
    //        {
    //            gvClassUpTraining.DataSource = upTraining;
    //            gvClassUpTraining.Rebind();
    //        }

    //        if (profDevelopment != null)
    //        {
    //            gvClassProfessionalDevelopment.DataSource = profDevelopment;
    //            gvClassProfessionalDevelopment.Rebind();
    //        }
    //    }

    //    if (ddlStatus.SelectedValue == "1")
    //    {
    //        var newHire = DataHelper.GetClassNHNotYetStarted();
    //        var crossTraining = DataHelper.GetClassCTNotYetStarted();
    //        var refresherTraining = DataHelper.GetClassRTNotYetStarted();
    //        var upTraining = DataHelper.GetClassUTNotYetStarted();
    //        var profDevelopment = DataHelper.GetClassPDNotYetStarted();

    //        if (newHire != null)
    //        {
    //            gvClassNewHire.DataSource = newHire;
    //            gvClassNewHire.Rebind();
    //        }

    //        if (crossTraining != null)
    //        {
    //            gvClassCrossTraining.DataSource = crossTraining;
    //            gvClassCrossTraining.Rebind();
    //        }

    //        if (refresherTraining != null)
    //        {
    //            gvClassRefresherTraining.DataSource = refresherTraining;
    //            gvClassRefresherTraining.Rebind();
    //        }

    //        if (upTraining != null)
    //        {
    //            gvClassUpTraining.DataSource = upTraining;
    //            gvClassUpTraining.Rebind();
    //        }

    //        if (profDevelopment != null)
    //        {
    //            gvClassProfessionalDevelopment.DataSource = profDevelopment;
    //            gvClassProfessionalDevelopment.Rebind();
    //        }
    //    }

    //    if (ddlStatus.SelectedValue == "2")
    //    {
    //        var newHire = DataHelper.GetClassNHInProgress();
    //        var crossTraining = DataHelper.GetClassCTInProgress();
    //        var refresherTraining = DataHelper.GetClassRTInProgress();
    //        var upTraining = DataHelper.GetClassUTInProgress();
    //        var profDevelopment = DataHelper.GetClassPDInProgress();

    //        if (newHire != null)
    //        {
    //            gvClassNewHire.DataSource = newHire;
    //            gvClassNewHire.Rebind();
    //        }

    //        if (crossTraining != null)
    //        {
    //            gvClassCrossTraining.DataSource = crossTraining;
    //            gvClassCrossTraining.Rebind();
    //        }

    //        if (refresherTraining != null)
    //        {
    //            gvClassRefresherTraining.DataSource = refresherTraining;
    //            gvClassRefresherTraining.Rebind();
    //        }

    //        if (upTraining != null)
    //        {
    //            gvClassUpTraining.DataSource = upTraining;
    //            gvClassUpTraining.Rebind();
    //        }

    //        if (profDevelopment != null)
    //        {
    //            gvClassProfessionalDevelopment.DataSource = profDevelopment;
    //            gvClassProfessionalDevelopment.Rebind();
    //        }
    //    }
    //}

    #endregion

    #region CourseThread
    [System.Web.Services.WebMethod()]
    public static postResult GetThreadPosts(int startRowIndex, int maximumRows, string sortExpression, string _threadID, string _courseID)
    {

        var db = new TranscomUniversityV3ModelContainer();
        int threadID = Convert.ToInt32(Utils.Decrypt(_threadID));
        int courseID = Convert.ToInt32(Utils.Decrypt(_threadID));
        int currentUser = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
        var threadPosts = db.pr_TranscomUniversity_ThreadPost(threadID, currentUser)
            .AsEnumerable()
            .Select(p => new
            {
                p.ThreadId,
                p.PostId,
                p.AuthorCIM,
                p.AuthorName,
                p.PostComment,
                p.RoleTitle,
                p.UserImageUrl,
                dtDatePosted = p.DatePosted,
                ButtonClassHelpful = p.Helpful == 0 ? "btn-teal" : "btn-lime",
                QuotePostClass = p.QuotePostId > 0 ? "" : "display-none",
                DatePosted = string.Format("{0: MMMM dd, yyyy. hh:mm tt}", p.DatePosted),
                p.OriginalPostAuthor,
                p.OriginalPostComment

            })
            .ToArray();

        var postResult = new postResult();
        postResult.data = RadGrid.GetBindingData(threadPosts.AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data;
        postResult.count = threadPosts.Count();

        return postResult;
    }

    [System.Web.Services.WebMethod()]
    public static int addNewPost(string comment, string _courseID, string _threadID)
    {
        try
        {

            int threadID = Convert.ToInt32(Utils.Decrypt(_threadID));
            int authorCIM = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
            var db = new TranscomUniversityV3ModelContainer();

            pr_TranscomUniversity_InsertPost_Result res = db.pr_TranscomUniversity_InsertPost(threadID, 0, authorCIM, comment).SingleOrDefault();


            if (res.Response == 1)
            {
                return 1;
                //rwmThread.RadAlert("Reply Successful", 330, 180, "Successful Post", "");
                //txtPostComment.Text = "";
            }
            else
                return 2;
            //rwmThread.RadAlert("Error Posting Reply. You're don't have permission to post in this thread.", 330, 180, "Error Post", "");
        }
        catch (Exception e)
        {
            return 3;
        }


    }


    [System.Web.Services.WebMethod()]
    public static int btnHelpfulRes(int postId)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            int postid = Convert.ToInt32(postId);
            int usercim = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
            pr_TranscomUniversity_InsertHelpfulPost_Result res = db.pr_TranscomUniversity_InsertHelpfulPost(postid, usercim).SingleOrDefault();
            return res.Response;
        }
    }

    [System.Web.Services.WebMethod()]
    public static int btnSubmitReport(int postId, string txtReportMessage)
    {
        int usercim = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
        string reportmessage = txtReportMessage;
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            pr_TranscomUniversity_InsertReportPost_Result res = db.pr_TranscomUniversity_InsertReportPost(postId, usercim, reportmessage).SingleOrDefault();
            return res.Response;
        }

    }

    [System.Web.Services.WebMethod()]
    public static int btnSubmitReply(int postId, string _threadId, string txtReportMessage)
    {

        int usercim = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
        int threadId = Convert.ToInt32(Utils.Decrypt(_threadId));
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            pr_TranscomUniversity_InsertPost_Result res = db.pr_TranscomUniversity_InsertPost(threadId, postId, usercim, txtReportMessage).SingleOrDefault();

            return res.Response;
        }

    }

    public class postResult
    {
        public IEnumerable<dynamic> data { get; set; }
        public int count { get; set; }
    }
    #endregion


}

