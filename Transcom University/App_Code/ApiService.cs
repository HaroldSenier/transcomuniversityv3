﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using Newtonsoft.Json;
using System.Web.Script.Services;
using Telerik.Web.UI;
using TranscomUniversityV3Model;
using System.Web.Script.Serialization;
using System.Collections;
using Org.BouncyCastle.Asn1.Ocsp;
using System.IO;
using System.ServiceModel.Activation;

/// <summary>
/// Summary description for ApiService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.ComponentModel.ToolboxItem(false)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
[System.Web.Script.Services.ScriptService]
[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
public class ApiService : System.Web.Services.WebService
{

    [WebMethod]
    public string HelloWorld()
    {
        return "Hello World";
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SaveCourseSetting(string CourseID, string BitVal, string ActPage)
    {
        bool tick;
        if (Convert.ToInt32(BitVal) == 1)
            tick = true;
        else
            tick = false;

        DataSet ds = DataHelper.GetCourseSetting(Convert.ToInt32(Utils.Decrypt(CourseID)));

        DataHelper.UpdateCourseSetting(
                Convert.ToInt32(Utils.Decrypt(CourseID))
                , Convert.ToBoolean(ds.Tables[0].Rows[0]["AllowLearnersToEnroll"])
                , Convert.ToBoolean(ds.Tables[0].Rows[0]["IsOpen"])
                , Convert.ToBoolean(ds.Tables[0].Rows[0]["NotifyTrainers"])
                , Convert.ToBoolean(ds.Tables[0].Rows[0]["NotifySupervisor"])
                , Convert.ToBoolean(ds.Tables[0].Rows[0]["NotifyManagers"])
                , Convert.ToBoolean(ds.Tables[0].Rows[0]["NotifyAdmin"])
                , Convert.ToBoolean(ds.Tables[0].Rows[0]["ApplyUnenrollments"])
                , tick
                , Convert.ToBoolean(ds.Tables[0].Rows[0]["SimilarCourseWidget"])
                , Convert.ToBoolean(ds.Tables[0].Rows[0]["CourseViewWidget"])
                , Convert.ToBoolean(ds.Tables[0].Rows[0]["WithApproval"]));
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SaveCourseSetting2(string CourseID, string BitVal, string ActPage)
    {
        bool tick;
        if (Convert.ToInt32(BitVal) == 1)
            tick = true;
        else
            tick = false;

        DataSet ds = DataHelper.GetCourseSetting(Convert.ToInt32(Utils.Decrypt(CourseID)));

        if (ActPage == "WithApproval")
        {
            DataHelper.UpdateCourseSetting(
               Convert.ToInt32(Utils.Decrypt(CourseID))
               , ActPage == "AllowLearners" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["AllowLearnersToEnroll"])
               , ActPage == "IsOpen" ? tick : false
               , ActPage == "NotifyTrainers" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["NotifyTrainers"])
               , ActPage == "NotifySupervisors" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["NotifySupervisor"])
               , ActPage == "NotifyManagers" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["NotifyManagers"])
               , ActPage == "NotifyAdmins" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["NotifyAdmin"])
               , ActPage == "Unenrollments" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["ApplyUnenrollments"])
               , ActPage == "Forum" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["ForumWidget"])
               , ActPage == "SimilarCourse" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["SimilarCourseWidget"])
               , ActPage == "CourseView" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["CourseViewWidget"])
               , ActPage == "WithApproval" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["WithApproval"]));

        }else if(ActPage == "IsOpen")
        {
         DataHelper.UpdateCourseSetting(
                Convert.ToInt32(Utils.Decrypt(CourseID))
                , ActPage == "AllowLearners" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["AllowLearnersToEnroll"])
                , ActPage == "IsOpen" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["IsOpen"])
                , ActPage == "NotifyTrainers" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["NotifyTrainers"])
                , ActPage == "NotifySupervisors" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["NotifySupervisor"])
                , ActPage == "NotifyManagers" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["NotifyManagers"])
                , ActPage == "NotifyAdmins" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["NotifyAdmin"])
                , ActPage == "Unenrollments" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["ApplyUnenrollments"])
                , ActPage == "Forum" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["ForumWidget"])
                , ActPage == "SimilarCourse" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["SimilarCourseWidget"])
                , ActPage == "CourseView" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["CourseViewWidget"])
                , ActPage == "WithApproval" ? tick : false);
        }else
        {
            DataHelper.UpdateCourseSetting(
                   Convert.ToInt32(Utils.Decrypt(CourseID))
                   , ActPage == "AllowLearners" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["AllowLearnersToEnroll"])
                   , ActPage == "IsOpen" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["IsOpen"])
                   , ActPage == "NotifyTrainers" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["NotifyTrainers"])
                   , ActPage == "NotifySupervisors" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["NotifySupervisor"])
                   , ActPage == "NotifyManagers" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["NotifyManagers"])
                   , ActPage == "NotifyAdmins" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["NotifyAdmin"])
                   , ActPage == "Unenrollments" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["ApplyUnenrollments"])
                   , ActPage == "Forum" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["ForumWidget"])
                   , ActPage == "SimilarCourse" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["SimilarCourseWidget"])
                   , ActPage == "CourseView" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["CourseViewWidget"])
                   , ActPage == "WithApproval" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["WithApproval"]));
        }
       


        if (ActPage == "Forum")
        {
            DataSet dsx = DataHelper.GetForumSetting(Convert.ToInt32(ds.Tables[0].Rows[0]["CourseSettingID"]));

            if (dsx.Tables[0].Rows.Count == 0)
            {
                DataHelper.InsertForumSetting(
                    Convert.ToInt32(ds.Tables[0].Rows[0]["CourseSettingID"])
                    , false, false, false, 0, 0, false, false);
            }
        }

        if (ActPage == "SimilarCourse")
        {
            DataSet dsx = DataHelper.GetSimilarCourseSetting(Convert.ToInt32(ds.Tables[0].Rows[0]["CourseSettingID"]));

            if (dsx.Tables[0].Rows.Count == 0)
            {
                DataHelper.InsertSimilarCourseSetting(
                    Convert.ToInt32(ds.Tables[0].Rows[0]["CourseSettingID"])
                    , false, false, false, false, false, false, false, false, false);
            }
        }

        if (ActPage == "CourseView")
        {
            DataSet dsx = DataHelper.GetCourseViewSetting(Convert.ToInt32(ds.Tables[0].Rows[0]["CourseSettingID"]));

            if (dsx.Tables[0].Rows.Count == 0)
            {
                DataHelper.InsertCourseViewSetting(
                    Convert.ToInt32(ds.Tables[0].Rows[0]["CourseSettingID"])
                    , false, false, false);
            }
        }
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SaveForumSetting(string CourseID, string BitVal, string ActPage)
    {
        bool tick;
        if (Convert.ToInt32(BitVal) == 1)
            tick = true;
        else
            tick = false;

        DataSet ds = DataHelper.GetCourseSetting(Convert.ToInt32(Utils.Decrypt(CourseID)));

        DataSet dsx = DataHelper.GetForumSetting(Convert.ToInt32(ds.Tables[0].Rows[0]["CourseSettingID"]));

        if (dsx.Tables[0].Rows.Count == 0)
        {
            if (ActPage == "EnrolledOnly") {
                DataHelper.InsertForumSetting(
                   Convert.ToInt32(ds.Tables[0].Rows[0]["CourseSettingID"])
                       , false 
                       , ActPage == "EnrolledOnly" ? tick : Convert.ToBoolean(dsx.Tables[0].Rows[0]["EnrolledUsersOnly"])
                       , ActPage == "AllowUpload" ? tick :  Convert.ToBoolean(dsx.Tables[0].Rows[0]["AllowFileUpload"])
                       , Convert.ToInt32(dsx.Tables[0].Rows[0]["MaxUploadSize"])
                       , Convert.ToInt32(dsx.Tables[0].Rows[0]["MaxChars"])
                       , ActPage == "CanJoinMultiForums" ? tick : Convert.ToBoolean(dsx.Tables[0].Rows[0]["CanJoinMultipleForums"])
                       , ActPage == "LimitToClass" ? tick : Convert.ToBoolean(dsx.Tables[0].Rows[0]["LimitToClass"]));
            }
            else if (ActPage == "UsersCanParticipate")
            {
                DataHelper.InsertForumSetting(
                 Convert.ToInt32(ds.Tables[0].Rows[0]["CourseSettingID"])
                     , ActPage == "UsersCanParticipate" ? tick : Convert.ToBoolean(dsx.Tables[0].Rows[0]["UsersCanParticipate"])
                     , false 
                     , ActPage == "AllowUpload" ? tick : Convert.ToBoolean(dsx.Tables[0].Rows[0]["AllowFileUpload"])
                     , Convert.ToInt32(dsx.Tables[0].Rows[0]["MaxUploadSize"])
                     , Convert.ToInt32(dsx.Tables[0].Rows[0]["MaxChars"])
                     , ActPage == "CanJoinMultiForums" ? tick : Convert.ToBoolean(dsx.Tables[0].Rows[0]["CanJoinMultipleForums"])
                     , ActPage == "LimitToClass" ? tick : Convert.ToBoolean(dsx.Tables[0].Rows[0]["LimitToClass"]));
            
            }else{
                DataHelper.InsertForumSetting(
                  Convert.ToInt32(ds.Tables[0].Rows[0]["CourseSettingID"])
                      , ActPage == "UsersCanParticipate" ? tick : Convert.ToBoolean(dsx.Tables[0].Rows[0]["UsersCanParticipate"])
                      , ActPage == "EnrolledOnly" ? tick : Convert.ToBoolean(dsx.Tables[0].Rows[0]["EnrolledUsersOnly"])
                      , ActPage == "AllowUpload" ? tick : Convert.ToBoolean(dsx.Tables[0].Rows[0]["AllowFileUpload"])
                      , Convert.ToInt32(dsx.Tables[0].Rows[0]["MaxUploadSize"])
                      , Convert.ToInt32(dsx.Tables[0].Rows[0]["MaxChars"])
                      , ActPage == "CanJoinMultiForums" ? tick : Convert.ToBoolean(dsx.Tables[0].Rows[0]["CanJoinMultipleForums"])
                      , ActPage == "LimitToClass" ? tick : Convert.ToBoolean(dsx.Tables[0].Rows[0]["LimitToClass"]));
            }
          
        }
        else
        {


            DataHelper.UpdateForumSetting(
                    Convert.ToInt32(ds.Tables[0].Rows[0]["CourseSettingID"])
                    , ActPage == "UsersCanParticipate" ? tick : Convert.ToBoolean(dsx.Tables[0].Rows[0]["UsersCanParticipate"])
                    , ActPage == "EnrolledOnly" ? tick : Convert.ToBoolean(dsx.Tables[0].Rows[0]["EnrolledUsersOnly"])
                    , ActPage == "AllowUpload" ? tick : Convert.ToBoolean(dsx.Tables[0].Rows[0]["AllowFileUpload"])
                    , Convert.ToInt32(dsx.Tables[0].Rows[0]["MaxUploadSize"])
                    , Convert.ToInt32(dsx.Tables[0].Rows[0]["MaxChars"])
                    , ActPage == "CanJoinMultiForums" ? tick : Convert.ToBoolean(dsx.Tables[0].Rows[0]["CanJoinMultipleForums"])
                    , ActPage == "LimitToClass" ? tick : Convert.ToBoolean(dsx.Tables[0].Rows[0]["LimitToClass"]));
        }
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SaveForumSetting2(string CourseID, string BitVal, string ActPage)
    {

        int RealValue = Convert.ToInt32(BitVal);

        DataSet ds = DataHelper.GetCourseSetting(Convert.ToInt32(Utils.Decrypt(CourseID)));

        DataSet dsx = DataHelper.GetForumSetting(Convert.ToInt32(ds.Tables[0].Rows[0]["CourseSettingID"]));

        if (dsx.Tables[0].Rows.Count == 0)
        {
            DataHelper.InsertForumSetting(
                Convert.ToInt32(ds.Tables[0].Rows[0]["CourseSettingID"])
                , false, false, false, 0, 0, false, false);
        }
        else
        {


            DataHelper.UpdateForumSetting(
                    Convert.ToInt32(ds.Tables[0].Rows[0]["CourseSettingID"])
                    , Convert.ToBoolean(dsx.Tables[0].Rows[0]["UsersCanParticipate"])
                    , Convert.ToBoolean(dsx.Tables[0].Rows[0]["EnrolledUsersOnly"])
                    , Convert.ToBoolean(dsx.Tables[0].Rows[0]["AllowFileUpload"])
                    , ActPage == "UploadSize" ? RealValue : Convert.ToInt32(dsx.Tables[0].Rows[0]["MaxUploadSize"])
                    , ActPage == "CharSize" ? RealValue : Convert.ToInt32(dsx.Tables[0].Rows[0]["MaxChars"])
                    , Convert.ToBoolean(dsx.Tables[0].Rows[0]["CanJoinMultipleForums"])
                    , Convert.ToBoolean(dsx.Tables[0].Rows[0]["LimitToClass"]));
        }
    }


    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SaveSimilarCourseSetting(string CourseID, string BitVal, string ActPage)
    {
        bool tick;
        if (Convert.ToInt32(BitVal) == 1)
            tick = true;
        else
            tick = false;

        DataSet ds = DataHelper.GetCourseSetting(Convert.ToInt32(Utils.Decrypt(CourseID)));

        DataSet dsx = DataHelper.GetSimilarCourseSetting(Convert.ToInt32(ds.Tables[0].Rows[0]["CourseSettingID"]));

        DataHelper.UpdateSimilarCourseSetting(
                Convert.ToInt32(ds.Tables[0].Rows[0]["CourseSettingID"])
                , ActPage == "SameCategory" ? tick : Convert.ToBoolean(dsx.Tables[0].Rows[0]["SameCategory"])
                , ActPage == "SameSubCategory" ? tick : Convert.ToBoolean(dsx.Tables[0].Rows[0]["SameSubCategory"])
                , ActPage == "SameAuthor" ? tick : Convert.ToBoolean(dsx.Tables[0].Rows[0]["SameAuthor"])
                , ActPage == "SameDept" ? tick : Convert.ToBoolean(dsx.Tables[0].Rows[0]["SameDepartment"])
                , ActPage == "BundleCourse" ? tick : Convert.ToBoolean(dsx.Tables[0].Rows[0]["BundledCourse"])
                , ActPage == "SameProgram" ? tick : Convert.ToBoolean(dsx.Tables[0].Rows[0]["SameProgram"])
                , ActPage == "SameCatalogue" ? tick : Convert.ToBoolean(dsx.Tables[0].Rows[0]["SameCatalogueCategory"])
                , ActPage == "SameClassType" ? tick : Convert.ToBoolean(dsx.Tables[0].Rows[0]["SameCourseType"])
                , ActPage == "SaveAverageRating" ? tick : Convert.ToBoolean(dsx.Tables[0].Rows[0]["SameRating"]));
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SaveCourseViewSetting(string CourseID, string BitVal, string ActPage)
    {
        bool tick;
        if (Convert.ToInt32(BitVal) == 1)
            tick = true;
        else
            tick = false;

        DataSet ds = DataHelper.GetCourseSetting(Convert.ToInt32(Utils.Decrypt(CourseID)));

        DataSet dsx = DataHelper.GetCourseViewSetting(Convert.ToInt32(ds.Tables[0].Rows[0]["CourseSettingID"]));

        if (dsx.Tables[0].Rows.Count == 0)
        {
            DataHelper.InsertCourseViewSetting(
                   Convert.ToInt32(ds.Tables[0].Rows[0]["CourseSettingID"])
                   , ActPage == "ClickTitle" ? tick : false
                   , ActPage == "CountUnique" ? tick : false
                   , ActPage == "CountVisit" ? tick : false);
        }
        else
        {

            DataHelper.UpdateCourseViewSetting(
                    Convert.ToInt32(ds.Tables[0].Rows[0]["CourseSettingID"])
                    , ActPage == "ClickTitle" ? tick : Convert.ToBoolean(dsx.Tables[0].Rows[0]["ClickTitleOrImage"])
                    , ActPage == "CountUnique" ? tick : Convert.ToBoolean(dsx.Tables[0].Rows[0]["CountOfUniqueIDVisits"])
                    , ActPage == "CountVisit" ? tick : Convert.ToBoolean(dsx.Tables[0].Rows[0]["CountOfIDVisits"]));
        }
    }


    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SaveDripContent(string CourseID, string BitVal, string ActPage)
    {

        int RealValue = Convert.ToInt32(BitVal);

        DataSet ds = DataHelper.GetCourseSetting(Convert.ToInt32(Utils.Decrypt(CourseID)));

        DataSet dsx = DataHelper.GetForumSetting(Convert.ToInt32(ds.Tables[0].Rows[0]["CourseSettingID"]));

        if (dsx.Tables[0].Rows.Count == 0)
        {
            DataHelper.InsertForumSetting(
                Convert.ToInt32(ds.Tables[0].Rows[0]["CourseSettingID"])
                , false, false, false, 0, 0, false, false);
        }
    }


    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SaveCourseSettingNotif(string CourseID, string BitVal, string ActPage)
    {
        bool tick;
        if (Convert.ToInt32(BitVal) == 1)
            tick = true;
        else
            tick = false;

        DataSet ds = DataHelper.GetCourseSettingNotif(Convert.ToInt32(Utils.Decrypt(CourseID)));

        if (ds.Tables[0].Rows.Count == 0)
        {
            DataHelper.InsertNotif(
                Convert.ToInt32(Utils.Decrypt(CourseID))
                , ActPage == "EnrolRequest" ? tick : (tick == true ? true : false)
                , ActPage == "Trainers" ? tick : (tick == true ? true : false)
                , ActPage == "Supervisors" ? tick : (tick == true ? true : false)
                , ActPage == "Managers" ? tick : (tick == true ? true : false)
                , ActPage == "CourseAvailability" ? tick : (tick == true ? true : false)
                , ActPage == "ClassAssignments" ? tick : (tick == true ? true : false)
                , ActPage == "OverdueCourses" ? tick : (tick == true ? true : false)
                , ActPage == "Forums" ? tick : (tick == true ? true : false)
                , ActPage == "EnrolRequest1" ? tick : (tick == true ? true : false)
                , ActPage == "Trainers1" ? tick : (tick == true ? true : false)
                , ActPage == "Supervisors1" ? tick : (tick == true ? true : false)
                , ActPage == "Managers1" ? tick : (tick == true ? true : false)
                , ActPage == "CourseAvailability1" ? tick : (tick == true ? true : false)
                , ActPage == "ClassAssignments1" ? tick : (tick == true ? true : false)
                , ActPage == "OverdueCourses1" ? tick : (tick == true ? true : false)
                , ActPage == "Forums1" ? tick : (tick == true ? true : false));
        }
        else
        {
            DataHelper.UpdateNotif(
                Convert.ToInt32(Utils.Decrypt(CourseID))
                , ActPage == "EnrolRequest" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["AlertEnrollmentReq"])
                , ActPage == "Trainers" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["AlertTrainers"])
                , ActPage == "Supervisors" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["AlertSupervisors"])
                , ActPage == "Managers" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["AlertManagers"])
                , ActPage == "CourseAvailability" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["AlertCourseAvailability"])
                , ActPage == "ClassAssignments" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["AlertClassAssignments"])
                , ActPage == "OverdueCourses" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["AlertOverdueCourses"])
                , ActPage == "Forums" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["AlertForums"])
                , ActPage == "EnrolRequest1" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["EmailEnrollmentReq"])
                , ActPage == "Trainers1" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["EmailTrainers"])
                , ActPage == "Supervisors1" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["EmailSupervisors"])
                , ActPage == "Managers1" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["EmailManagers"])
                , ActPage == "CourseAvailability1" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["EmailCourseAvailability"])
                , ActPage == "ClassAssignments1" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["EmailClassAssignments"])
                , ActPage == "OverdueCourses1" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["EmailOverdueCourses"])
                , ActPage == "Forums1" ? tick : Convert.ToBoolean(ds.Tables[0].Rows[0]["EmailForums"]));
        }
    }

    /*Page Editor*/
    /*Mandatory Course*/
    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SaveMandatoryCourseSetting(string IsGridView, string DisplayCount, string IsManual, string IsVisible)
    {
        bool _isGridView = Convert.ToBoolean(IsGridView);
        int _displayCount = Convert.ToInt32(DisplayCount);
        bool _isManual = Convert.ToBoolean(IsManual);
        bool _isVisible = Convert.ToBoolean(IsVisible);

        DataHelper.updateMandatorySettings(_isGridView, _displayCount, _isManual, _isVisible);
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public dynamic GetMandatoryCourseList(int startRowIndex, int maximumRows, string sortExpression)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var courses = db.pr_TranscomUniversity_MandatoryCourse()
            .AsEnumerable()
            .Select(m => new
            {
                m.CourseID,
                EncryptedCourseID = Utils.Encrypt(m.CourseID),
                m.CourseTitle,
                m.CourseDescription,
                m.CourseImage,
                m.OrderNumber,
                m.CourseType,
                m.CourseDuration,
                m.CourseCategory,
                m.CourseSubcategory,
                DateLastModified = string.Format("{0: yyyy/MM/dd hh:mm:ss tt}", m.DateLastModified),
                m.CoursePath
            })
            .ToArray();

            var CustomData = new CustomData();
            CustomData.data = RadGrid.GetBindingData(courses.AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data;
            CustomData.count = courses.Count();

            return CustomData;
        }

    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public dynamic GetMandatoryCourseLibraryList(int startRowIndex, int maximumRows, string sortExpression)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var courses = db.pr_TranscomUniversity_MandatoryCourseLibrary()
            .AsEnumerable()
            .Select(c => new
            {
                c.CourseID,
                EncryptedCourseID = Utils.Encrypt(c.CourseID),
                c.CourseTitle,
                c.CourseDescription,
                c.CourseImage,
            })
            .ToArray();

            var CustomData = new CustomData();
            CustomData.data = RadGrid.GetBindingData(courses.AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data;
            CustomData.count = courses.Count();

            return CustomData;
        }
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultResponse SaveSelectedMandatoryCourses(string selectedCourses)
    {
        ResultResponse res = new ResultResponse();

        try
        {
            dynamic data = JsonConvert.DeserializeObject(selectedCourses);

            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                for (int i = 0; i < data.courses.Count; i++)
                {
                    int courseID = Convert.ToInt32(Utils.Decrypt(data.courses[i].Value));
                    db.pr_TranscomUniversity_InsertMandatoryCourse(courseID);
                }
                res.isSuccess = true;
                res.message = "";

                return res;
            }

        }
        catch (Exception e)
        {
            res.message = e.Message;
            res.isSuccess = false;
            return res;
        }
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultResponse DeleteMandatoryCourse(string selectedCourses)
    {
        ResultResponse res = new ResultResponse();

        try
        {
            dynamic data = JsonConvert.DeserializeObject(selectedCourses);

            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                for (int i = 0; i < data.courses.Count; i++)
                {
                    int courseID = Convert.ToInt32(Utils.Decrypt(data.courses[i].Value));
                    db.pr_TranscomUniversity_DeleteMandatoryCourseInCatalog(courseID);
                }
                res.isSuccess = true;
                res.message = "";

                return res;
            }

        }
        catch (Exception e)
        {
            res.message = e.Message;
            res.isSuccess = false;
            return res;
        }
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultResponse UpdateMandatoryCourseOrder(string jsonData)
    {
        ResultResponse res = new ResultResponse();

        try
        {
            dynamic data = JsonConvert.DeserializeObject(jsonData);

            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                for (int i = 0; i < data.courses.Count; i++)
                {
                    int courseID = Convert.ToInt32(Utils.Decrypt(data.courses[i].courseid.Value));
                    int orderNumber = Convert.ToInt32(data.courses[i].index.Value);

                    db.pr_TranscomUniversity_UpdateMandatoryCourseOrder(courseID, orderNumber);
                }
                res.isSuccess = true;
                res.message = "";

                return res;
            }

        }
        catch (Exception e)
        {
            res.message = e.Message;
            res.isSuccess = false;
            return res;
        }
    }

    /*Trending Course*/

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SaveTrendingCourseSetting(string IsGridView, string DisplayCount, string ManualSelection, string EnableMostView, string MostViewCount, string EnableMostRating, string MostRatingCount, string EnableRatingWithin, string RatingWithin, string Completed, string EnableMostEnrollee, string MostEnrolleeCount, string EnableAddedLast, string LastAddInMillisecond, string LastAddUnit, string EnableLastModified, string LastModifiedInMillisecond, string LastModifiedUnit, string Visible)
    {
        bool _IsGridView = Convert.ToBoolean(IsGridView);
        int _DisplayCount = Convert.ToInt32(DisplayCount);
        bool _ManualSelection = Convert.ToBoolean(ManualSelection);
        bool _EnableMostView = Convert.ToBoolean(EnableMostView);
        int _MostViewCount = Convert.ToInt32(MostViewCount);
        bool _EnableMostRating = Convert.ToBoolean(EnableMostRating);
        int _MostRatingCount = Convert.ToInt32(MostRatingCount);
        bool _EnableRatingWithin = Convert.ToBoolean(EnableRatingWithin);
        int _RatingWithin = Convert.ToInt32(RatingWithin);
        bool _Completed = Convert.ToBoolean(Completed);
        bool _EnableMostEnrollee = Convert.ToBoolean(EnableMostEnrollee);
        int _MostEnrolleeCount = Convert.ToInt32(MostEnrolleeCount);
        bool _EnableAddedLast = Convert.ToBoolean(EnableAddedLast);
        int _LastAddInMillisecond = Convert.ToInt32(LastAddInMillisecond);
        int _LastAddUnit = Convert.ToInt32(LastAddUnit);
        bool _EnableLastModified = Convert.ToBoolean(EnableLastModified);
        int _LastModifiedInMillisecond = Convert.ToInt32(LastModifiedInMillisecond);
        int _LastModifiedUnit = Convert.ToInt32(LastModifiedUnit);
        bool _Visible = Convert.ToBoolean(Visible);

        DataHelper.updateTrendingSettings(_IsGridView, _DisplayCount, _ManualSelection, _EnableMostView, _MostViewCount, _EnableMostRating, _MostRatingCount, _EnableRatingWithin, _RatingWithin, _Completed, _EnableMostEnrollee, _MostEnrolleeCount, _EnableAddedLast, _LastAddInMillisecond, _LastAddUnit, _EnableLastModified, _LastModifiedInMillisecond, _LastModifiedUnit, _Visible);
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public dynamic GetTrendingCourseList(int startRowIndex, int maximumRows, string sortExpression)
    {
        //try
        //{
            
        //}
        //catch (Exception)
        //{
        //    var CustomData = new CustomData();
        //    CustomData.data = null;
        //    CustomData.count = 0;

        //    return CustomData;
        //    throw;
        //}

            var CustomData = new CustomData();
            try
            {
                using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
                {


                    var courses = db.pr_TranscomUniversity_TrendingCourse()
                    .AsEnumerable()
                    .Select(m => new
                    {
                        m.CourseID,
                        EncryptedCourseID = Utils.Encrypt(m.CourseID),
                        m.CourseTitle,
                        m.CourseDescription,
                        m.CourseImage,
                        m.OrderNumber,
                        m.CourseType,
                        m.CourseDuration,
                        m.CourseCategory,
                        m.CourseSubcategory,
                        DateLastModified = string.Format("{0: yyyy/MM/dd hh:mm:ss tt}", m.DateLastModified),
                        m.CoursePath
                    })
                    .ToArray();

                    CustomData.data = RadGrid.GetBindingData(courses.AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data;
                    CustomData.count = courses.Count();

                }
            }
            catch (Exception)
            {
                throw;
            }

            return CustomData;


    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultResponse SaveSelectedTrendingCourses(string selectedCourses)
    {
        ResultResponse res = new ResultResponse();

        try
        {
            dynamic data = JsonConvert.DeserializeObject(selectedCourses);

            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                for (int i = 0; i < data.courses.Count; i++)
                {
                    int courseID = Convert.ToInt32(Utils.Decrypt(data.courses[i].Value));
                    db.pr_TranscomUniversity_InsertTrendingCourse(courseID);
                }
                res.isSuccess = true;
                res.message = "";

                return res;
            }

        }
        catch (Exception e)
        {
            res.message = e.Message;
            res.isSuccess = false;
            return res;
        }
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public dynamic GetTrendingCourseLibraryList(int startRowIndex, int maximumRows, string sortExpression)
    {
        try
        {
            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                var courses = db.pr_TranscomUniversity_TrendingCourseLibrary()
                .AsEnumerable()
                .Select(c => new
                {
                    c.CourseID,
                    EncryptedCourseID = Utils.Encrypt(c.CourseID),
                    c.CourseTitle,
                    c.CourseImage,
                })
                .ToArray();

                var CustomData = new CustomData();
                CustomData.data = RadGrid.GetBindingData(courses.AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data;
                CustomData.count = courses.Count();

                return CustomData;
            }

        }
        catch (Exception)
        {
            var CustomData = new CustomData();
            CustomData.data = null;
            CustomData.count = 0;

            return CustomData;
            throw;
        }
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultResponse UpdateTrendingCourseOrder(string jsonData)
    {
        ResultResponse res = new ResultResponse();

        try
        {
            dynamic data = JsonConvert.DeserializeObject(jsonData);

            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                for (int i = 0; i < data.courses.Count; i++)
                {
                    int courseID = Convert.ToInt32(Utils.Decrypt(data.courses[i].courseid.Value));
                    int orderNumber = Convert.ToInt32(data.courses[i].index.Value);

                    db.pr_TranscomUniversity_UpdateTrendingCourseOrder(courseID, orderNumber);
                }
                res.isSuccess = true;
                res.message = "";

                return res;
            }

        }
        catch (Exception e)
        {
            res.message = e.Message;
            res.isSuccess = false;
            return res;
        }
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultResponse DeleteTrendingCourse(string selectedCourses)
    {
        ResultResponse res = new ResultResponse();

        try
        {
            dynamic data = JsonConvert.DeserializeObject(selectedCourses);

            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                for (int i = 0; i < data.courses.Count; i++)
                {
                    int courseID = Convert.ToInt32(Utils.Decrypt(data.courses[i].Value));
                    db.pr_TranscomUniversity_DeleteTrendingCourseInCatalog(courseID);
                }

                res.isSuccess = true;
                res.message = "";

                return res;
            }

        }
        catch (Exception e)
        {
            res.message = e.Message;
            res.isSuccess = false;
            return res;
        }
    }

    //Recommended Course

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public dynamic GetRecommendedCourseList(int startRowIndex, int maximumRows, string sortExpression, string user)
    {
        try
        {
            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {

                int currentUser = DataHelper.GetCimByEmail(user);
                var courses = db.pr_TranscomUniversity_RecommendedCourse(currentUser)
                       .AsEnumerable()
                       .Select(m => new
                       {
                           m.CourseID,
                           EncryptedCourseID = Utils.Encrypt(m.CourseID),
                           m.CourseTitle,
                           m.CourseDescription,
                           m.CourseImage,
                           m.OrderNumber
                       })
                       .ToArray();
                var CustomData = new CustomData();
                CustomData.data = RadGrid.GetBindingData(courses.AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data;
                CustomData.count = courses.Count();

                return CustomData;
            }
        }
        catch (Exception)
        {
            var CustomData = new CustomData();
            CustomData.data = null;
            CustomData.count = 0;

            return CustomData;
            throw;
        }

    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultResponse UpdateRecommendedCourseOrder(string jsonData)
    {
        ResultResponse res = new ResultResponse();

        try
        {
            dynamic data = JsonConvert.DeserializeObject(jsonData);

            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                for (int i = 0; i < data.courses.Count; i++)
                {
                    int courseID = Convert.ToInt32(Utils.Decrypt(data.courses[i].courseid.Value));
                    int orderNumber = Convert.ToInt32(data.courses[i].index.Value);

                    db.pr_TranscomUniversity_UpdateRecommendedCourseOrder(courseID, orderNumber);
                }
                res.isSuccess = true;
                res.message = "";

                return res;
            }

        }
        catch (Exception e)
        {
            res.message = e.Message;
            res.isSuccess = false;
            return res;
        }
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SaveRecommendedCourseSetting(string IsAssigned, string IsSimilar, string IsRequiredTenure, string IsRequiredRole, string IsBasedOnLearner, string IsViewedEnable, string ViewCount, string IsGridView, string DisplayCount, string IsManual, string IsNotifyAlert, string IsNotifyEmail, string IsVisible)
    {

        bool _isAssigned = Convert.ToBoolean(IsAssigned);
        bool _isSimilar = Convert.ToBoolean(IsSimilar);
        bool _isRequiredTenure = Convert.ToBoolean(IsRequiredTenure);
        bool _isRequiredRole = Convert.ToBoolean(IsRequiredRole);
        bool _isBasedOnLearner = Convert.ToBoolean(IsBasedOnLearner);
        bool _isViewedEnable = Convert.ToBoolean(IsViewedEnable);
        int _viewCount = Convert.ToInt32(ViewCount);
        bool _isGridView = Convert.ToBoolean(IsGridView);
        int _displayCount = Convert.ToInt32(DisplayCount);
        bool _isManual = Convert.ToBoolean(IsManual);
        bool _notifyAlert = Convert.ToBoolean(IsNotifyAlert);
        bool _notifyEmail = Convert.ToBoolean(IsNotifyEmail);
        bool _isVisible = Convert.ToBoolean(IsVisible);

        DataHelper.updateRecommendedCourseSettings(_isAssigned, _isSimilar, _isRequiredTenure, _isRequiredRole, _isBasedOnLearner, _isViewedEnable, _viewCount, _isGridView, _displayCount, _isManual, _notifyAlert, _notifyEmail, _isVisible);
    }


    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultResponse DeleteRecommendedCourse(string selectedCourses)
    {
        ResultResponse res = new ResultResponse();

        try
        {
            dynamic data = JsonConvert.DeserializeObject(selectedCourses);

            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                for (int i = 0; i < data.courses.Count; i++)
                {
                    int courseID = Convert.ToInt32(Utils.Decrypt(data.courses[i].Value));
                    db.pr_TranscomUniversity_DeleteRecommendedCourseInCatalog(courseID);
                }

                res.isSuccess = true;
                res.message = "";

                return res;
            }

        }
        catch (Exception e)
        {
            res.message = e.Message;
            res.isSuccess = false;
            return res;
        }
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public dynamic GetRecommendedCourseLibraryList(int startRowIndex, int maximumRows, string sortExpression)
    {
        try
        {
            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                var courses = db.pr_TranscomUniversity_RecommendedCourseLibrary()
                .AsEnumerable()
                .Select(c => new
                {
                    c.CourseID,
                    EncryptedCourseID = Utils.Encrypt(c.CourseID),
                    c.CourseTitle,
                    c.CourseImage,
                })
                .ToArray();

                var CustomData = new CustomData();
                CustomData.data = RadGrid.GetBindingData(courses.AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data;
                CustomData.count = courses.Count();

                return CustomData;
            }
        }
        catch (Exception)
        {
            var CustomData = new CustomData();
            CustomData.data = null;
            CustomData.count = 0;

            return CustomData;
            throw;
        }
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultResponse SaveSelectedRecommendedCourses(string selectedCourses)
    {
        ResultResponse res = new ResultResponse();

        try
        {
            dynamic data = JsonConvert.DeserializeObject(selectedCourses);

            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                for (int i = 0; i < data.courses.Count; i++)
                {
                    int courseID = Convert.ToInt32(Utils.Decrypt(data.courses[i].Value));
                    db.pr_TranscomUniversity_InsertRecommendedCourse(courseID);
                }
                res.isSuccess = true;
                res.message = "";

                return res;
            }

        }
        catch (Exception e)
        {
            res.message = e.Message;
            res.isSuccess = false;
            return res;
        }
    }

    //Must Take Courses

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SaveMustTakeCourseSetting(string IsOverdue, string IsIncomplete, string IsGridView, string DisplayCount, string IsManual, string IsNotifyAlert, string IsNotifyEmail, string IsVisible)
    {

        bool _isOverdue = Convert.ToBoolean(IsOverdue);
        bool _isIncomplete = Convert.ToBoolean(IsIncomplete);
        bool _isGridView = Convert.ToBoolean(IsGridView);
        int _displayCount = Convert.ToInt32(DisplayCount);
        bool _isManual = Convert.ToBoolean(IsManual);
        bool _notifyAlert = Convert.ToBoolean(IsNotifyAlert);
        bool _notifyEmail = Convert.ToBoolean(IsNotifyEmail);
        bool _isVisible = Convert.ToBoolean(IsVisible);

        DataHelper.updateMustTakeCourseSettings(_isOverdue, _isIncomplete, _isGridView, _displayCount, _isManual, _notifyAlert, _notifyEmail, _isVisible);
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public dynamic GetMustTakeCourseList(int startRowIndex, int maximumRows, string sortExpression, string user)
    {
        try
        {
            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                int currentUser = DataHelper.GetCimByEmail(user);
                var courses = db.pr_TranscomUniversity_MustTakeCourse(currentUser)
                .AsEnumerable()
                .Select(m => new
                {
                    m.CourseID,
                    EncryptedCourseID = Utils.Encrypt(m.CourseID),
                    m.CourseTitle,
                    m.CourseDescription,
                    m.CourseImage,
                    m.OrderNumber
                })
                .ToArray();

                var CustomData = new CustomData();
                CustomData.data = RadGrid.GetBindingData(courses.AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data;
                CustomData.count = courses.Count();

                return CustomData;
            }
        }
        catch (Exception)
        {
            var CustomData = new CustomData();
            CustomData.data = null;
            CustomData.count = 0;

            return CustomData;
            throw;
        }

    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public dynamic GetMustTakeCourseLibraryList(int startRowIndex, int maximumRows, string sortExpression)
    {
        try
        {
            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                var courses = db.pr_TranscomUniversity_MustTakeCourseLibrary()
                .AsEnumerable()
                .Select(c => new
                {
                    c.CourseID,
                    EncryptedCourseID = Utils.Encrypt(c.CourseID),
                    c.CourseTitle,
                    c.CourseImage,
                })
                .ToArray();

                var CustomData = new CustomData();
                CustomData.data = RadGrid.GetBindingData(courses.AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data;
                CustomData.count = courses.Count();

                return CustomData;
            }
        }
        catch (Exception)
        {
            var CustomData = new CustomData();
            CustomData.data = null;
            CustomData.count = 0;

            return CustomData;
            throw;
        }
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultResponse DeleteMustTakeCourse(string selectedCourses)
    {
        ResultResponse res = new ResultResponse();

        try
        {
            dynamic data = JsonConvert.DeserializeObject(selectedCourses);

            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                for (int i = 0; i < data.courses.Count; i++)
                {
                    int courseID = Convert.ToInt32(Utils.Decrypt(data.courses[i].Value));
                    db.pr_TranscomUniversity_DeleteMustTakeCourseInCatalog(courseID);
                }

                res.isSuccess = true;
                res.message = "";

                return res;
            }

        }
        catch (Exception e)
        {
            res.message = e.Message;
            res.isSuccess = false;
            return res;
        }
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultResponse UpdateMustTakeCourseOrder(string jsonData)
    {
        ResultResponse res = new ResultResponse();

        try
        {
            dynamic data = JsonConvert.DeserializeObject(jsonData);

            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                for (int i = 0; i < data.courses.Count; i++)
                {
                    int courseID = Convert.ToInt32(Utils.Decrypt(data.courses[i].courseid.Value));
                    int orderNumber = Convert.ToInt32(data.courses[i].index.Value);

                    db.pr_TranscomUniversity_UpdateMustTakeCourseOrder(courseID, orderNumber);
                }
                res.isSuccess = true;
                res.message = "";

                return res;
            }

        }
        catch (Exception e)
        {
            res.message = e.Message;
            res.isSuccess = false;
            return res;
        }
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultResponse SaveSelectedMustTakeCourses(string selectedCourses)
    {
        ResultResponse res = new ResultResponse();

        try
        {
            dynamic data = JsonConvert.DeserializeObject(selectedCourses);

            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                for (int i = 0; i < data.courses.Count; i++)
                {
                    int courseID = Convert.ToInt32(Utils.Decrypt(data.courses[i].Value));
                    db.pr_TranscomUniversity_InsertMustTakeCourse(courseID);
                }
                res.isSuccess = true;
                res.message = "";

                return res;
            }

        }
        catch (Exception e)
        {
            res.message = e.Message;
            res.isSuccess = false;
            return res;
        }
    }

    //Recently Added

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SaveRecentlyAddedCourseSetting(string IsCreatedWithin, string CreatedWithin, string CreatedWithinUnit, string IsUpdatedWithin, string UpdatedWithin, string UpdatedWithinUnit, string IsGridView, string DisplayCount, string IsManual, string IsNotifyAlert, string IsNotifyEmail, string IsVisible)
    {

        bool _isEnableCreatedWithin = Convert.ToBoolean(IsCreatedWithin);
        int _createdWithin = Convert.ToInt32(CreatedWithin);
        int _createdWithinUnit = Convert.ToInt32(CreatedWithinUnit);
        bool _isEnableUpdatedWithin = Convert.ToBoolean(IsUpdatedWithin);
        int _updatedWithin = Convert.ToInt32(UpdatedWithin);
        int _updatedWithinUnit = Convert.ToInt32(UpdatedWithinUnit);
        bool _isGridView = Convert.ToBoolean(IsGridView);
        int _displayCount = Convert.ToInt32(DisplayCount);
        bool _isManual = Convert.ToBoolean(IsManual);
        bool _notifyAlert = Convert.ToBoolean(IsNotifyAlert);
        bool _notifyEmail = Convert.ToBoolean(IsNotifyEmail);
        bool _isVisible = Convert.ToBoolean(IsVisible);

        DataHelper.updateRecentlyAddedCourseSettings(_isEnableCreatedWithin, _createdWithin, _createdWithinUnit, _isEnableUpdatedWithin, _updatedWithin, _updatedWithinUnit, _isGridView, _displayCount, _isManual, _notifyAlert, _notifyEmail, _isVisible);
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public dynamic GetRecentlyAddedList(int startRowIndex, int maximumRows, string sortExpression)
    {
        //try
        //{
            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                var courses = db.pr_TranscomUniversity_RecentlyAddedCourse()
                .AsEnumerable()
                .Select(m => new
                {
                    m.CourseID,
                    EncryptedCourseID = Utils.Encrypt(m.CourseID),
                    m.CourseTitle,
                    m.CourseDescription,
                    m.CourseImage,
                    m.OrderNumber,
                    m.CourseType,
                    m.CourseDuration,
                    m.CourseCategory,
                    m.CourseSubcategory,
                    DateLastModified = string.Format("{0: yyyy/MM/dd hh:mm:ss tt}", m.DateLastModified),
                    m.CoursePath
                })
                .ToArray();

                var CustomData = new CustomData();
                CustomData.data = RadGrid.GetBindingData(courses.AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data;
                CustomData.count = courses.Count();

                return CustomData;
            }
        //}
        //catch (Exception)
        //{
        //    var CustomData = new CustomData();
        //    CustomData.data = null;
        //    CustomData.count = 0;

        //    return CustomData;
        //    throw;
        //}

    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public dynamic GetRecentlyAddedLibraryList(int startRowIndex, int maximumRows, string sortExpression)
    {
        try
        {
            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                var courses = db.pr_TranscomUniversity_RecentlyAddedCourseLibrary()
                .AsEnumerable()
                .Select(c => new
                {
                    c.CourseID,
                    EncryptedCourseID = Utils.Encrypt(c.CourseID),
                    c.CourseTitle,
                    c.CourseImage,
                })
                .ToArray();

                var CustomData = new CustomData();
                CustomData.data = RadGrid.GetBindingData(courses.AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data;
                CustomData.count = courses.Count();

                return CustomData;
            }
        }
        catch (Exception)
        {
            var CustomData = new CustomData();
            CustomData.data = null;
            CustomData.count = 0;

            return CustomData;
            throw;
        }
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultResponse DeleteRecentlyAdded(string selectedCourses)
    {
        ResultResponse res = new ResultResponse();

        try
        {
            dynamic data = JsonConvert.DeserializeObject(selectedCourses);

            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                for (int i = 0; i < data.courses.Count; i++)
                {
                    int courseID = Convert.ToInt32(Utils.Decrypt(data.courses[i].Value));
                    db.pr_TranscomUniversity_DeleteRecentlyAddedCourseInCatalog(courseID);
                }

                res.isSuccess = true;
                res.message = "";

                return res;
            }

        }
        catch (Exception e)
        {
            res.message = e.Message;
            res.isSuccess = false;
            return res;
        }
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultResponse UpdateRecentlyAddedOrder(string jsonData)
    {
        ResultResponse res = new ResultResponse();

        try
        {
            dynamic data = JsonConvert.DeserializeObject(jsonData);

            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                for (int i = 0; i < data.courses.Count; i++)
                {
                    int courseID = Convert.ToInt32(Utils.Decrypt(data.courses[i].courseid.Value));
                    int orderNumber = Convert.ToInt32(data.courses[i].index.Value);

                    db.pr_TranscomUniversity_UpdateRecentlyAddedCourseOrder(courseID, orderNumber);
                }
                res.isSuccess = true;
                res.message = "";

                return res;
            }

        }
        catch (Exception e)
        {
            res.message = e.Message;
            res.isSuccess = false;
            return res;
        }
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultResponse SaveSelectedRecentlyAdded(string selectedCourses)
    {
        ResultResponse res = new ResultResponse();

        try
        {
            dynamic data = JsonConvert.DeserializeObject(selectedCourses);

            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                for (int i = 0; i < data.courses.Count; i++)
                {
                    int courseID = Convert.ToInt32(Utils.Decrypt(data.courses[i].Value));
                    db.pr_TranscomUniversity_InsertRecentlyAdded(courseID);
                }
                res.isSuccess = true;
                res.message = "";

                return res;
            }

        }
        catch (Exception e)
        {
            res.message = e.Message;
            res.isSuccess = false;
            return res;
        }
    }

    //FORUMS
    [WebMethod(EnableSession = true)]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public dynamic GetUploadedForumImages(string optional)
    {
        try
        {
            ArrayList UserUploadedImages = (ArrayList)Session["ForumUploadedImages"]; ;
            return UserUploadedImages;
        }
        catch (Exception e)
        {
            return e.Message;
        }
    }

    //SLIDER
    [WebMethod(EnableSession = true)]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public dynamic GetUploadedSliderImages(string optional)
    {
        try
        {
            ArrayList UserUploadedImages = (ArrayList)Session["SliderUploadedImages"];
            return UserUploadedImages;
        }
        catch (Exception e)
        {
            return e.Message;
        }
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public dynamic SaveUploadedSliderImages(string selectedSlides, string playlistName, string user)
    {
        ResultResponse res = new ResultResponse();

        try
        {

            if (playlistName != "" || playlistName != null)
            {

                dynamic uploadedSlides = JsonConvert.DeserializeObject(HttpUtility.UrlDecode(selectedSlides));

                using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
                {
                    //save playlist
                    int currentUser = DataHelper.GetCimByEmail(user);


                    db.pr_TranscomUniversity_AddPlaylist(playlistName, currentUser);
                    db.SaveChanges();


                    int playlistID = db.tbl_trancomuniversity_Playlist.ToList().LastOrDefault().ID;

                    for (int i = 0; i < uploadedSlides.Count; i++)
                    {
                        string filename = uploadedSlides[i].filename.Value;
                        string title = uploadedSlides[i].title.Value;
                        string description = uploadedSlides[i].desc.Value;

                        db.pr_TranscomUniversity_AddSlide(playlistID, filename, title, description, currentUser);

                        //check if each file source exist

                        string sourcePath = Server.MapPath(string.Format("~/{0}", "Media/Uploads/Slider/Temporary/" + currentUser + "/" + filename));
                        string destinationPath = Server.MapPath(string.Format("~/{0}", "Media/Uploads/Sliders/" + playlistID + "/" + filename));
                        string directory = Server.MapPath(string.Format("~/{0}/", "Media/Uploads/Sliders/" + playlistID));
                        if (!Directory.Exists(directory))
                            Directory.CreateDirectory(directory);

                        System.IO.File.Copy(sourcePath, destinationPath, true);

                    }

                    //delete source folder
                    //string directoryPath = Server.MapPath(string.Format("~/{0}/", "Media/Uploads/Slider/Temporary/" + currentUser + "/"));

                    //if (Directory.Exists(directoryPath))
                    //    Directory.Delete(directoryPath, true);

                    //cancel delete we always delete directory every page load anyway

                    res.isSuccess = true;
                    res.message = "";

                    return res;
                }
            }
            else
            {
                res.isSuccess = false;
                res.message = "Playlist Name is Empty";

                return res;
            }

        }
        catch (Exception e)
        {
            res.message = e.Message + HttpContext.Current.User.Identity.Name.Split('|')[0];
            res.isSuccess = false;
            return res;
        }
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public dynamic SaveSliderSetting(string maxImages, string playInterval, string isAutoplay, string isOnclick, string videoDelay, string isVideoOnClick, string loopDirection, string selectedPlaylistID, string user)
    {
        ResultResponse res = new ResultResponse();

        try
        {
            int _maxImage = Convert.ToInt32(maxImages);
            int _playInterval = Convert.ToInt32(playInterval);
            bool _isAutoplay = Convert.ToBoolean(isAutoplay);
            bool _isOnclick = Convert.ToBoolean(isOnclick);
            int _videoDelay = Convert.ToInt32(videoDelay);
            bool _isVideoOnClick = Convert.ToBoolean(isVideoOnClick);
            int _loopDirection = Convert.ToInt32(loopDirection);
            int _selectedPlaylistID = Convert.ToInt32(Utils.Decrypt(selectedPlaylistID));
            int currentUser = DataHelper.GetCimByEmail(user);
            if ((_maxImage > 0 && _maxImage < 20) || (_playInterval > 0 && _playInterval < 60) || (_videoDelay > 0 && _videoDelay < 60) || (_loopDirection > 0 && _loopDirection < 5))
            {
                using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
                {
                    //save playlist
                    db.pr_TranscomUniversity_SliderSettingInsertOrUpdate(_maxImage, _playInterval, _isAutoplay, _isOnclick, _videoDelay, _isVideoOnClick, _loopDirection, _selectedPlaylistID, currentUser);
                    db.SaveChanges();

                    res.isSuccess = true;
                    res.message = "";

                    return res;
                }
            }
            else
            {
                res.isSuccess = false;
                res.message = "One of the input is not valid please check and try again.";

                return res;

            }
        }
        catch (Exception e)
        {
            res.message = e.Message;
            res.isSuccess = false;
            return res;
        }
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public dynamic RemovePlaylist(string playlistID)
    {
        ResultResponse res = new ResultResponse();

        try
        {
            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                //save playlist
                int DecPlaylistID = 0;
                if (int.TryParse(Utils.Decrypt(playlistID), out DecPlaylistID))
                {
                    db.pr_transcomuniversity_DeletePlaylist(DecPlaylistID);
                    db.SaveChanges();

                }

                res.isSuccess = true;
                res.message = "";

                return res;

            }

        }
        catch (Exception e)
        {
            res.message = e.Message;
            res.isSuccess = false;
            return res;
        }
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public dynamic GetOtherPlaylist(string optional)
    {

        try
        {

            CustomData cd = new CustomData();
            TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();

            var playlistsList = db.tbl_trancomuniversity_Playlist
                               .Where(p => p.Inactive == false)
                               .AsEnumerable()
                               .Select(p => new
                               {
                                   ID = Utils.Encrypt(p.ID),
                                   p.PlaylistName,
                               })
                               .ToArray();
            cd.data = playlistsList;
            cd.count = playlistsList.Count();
            return cd;

        }
        catch (Exception e)
        {
            ResultResponse res = new ResultResponse();
            res.isSuccess = false;
            res.message = e.Message;
            return res;
        }
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public dynamic GetPlaylist(string playlistID)
    {
        int _playlistID = Convert.ToInt32(Utils.Decrypt(playlistID));
        try
        {
            CustomData cd = new CustomData();
            TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();

            var playlistsList = db.tbl_trancomuniversity_PlaylistSlides
                                    .AsEnumerable()
                                    .Where(p => p.PlaylistID == _playlistID)
                //.Select(p => new
                //{
                //    PlaylistID = Utils.Encrypt(p.PlaylistID),
                //    p.PlaylistTitle,
                //    p.PlaylistDesc,
                //    p.PlaylistImg
                //})
                                    .ToArray();
            cd.data = playlistsList;
            cd.count = playlistsList.Count();
            return cd;

        }
        catch (Exception e)
        {
            ResultResponse res = new ResultResponse();
            res.isSuccess = false;
            res.message = e.Message;
            return res;
        }
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public dynamic GetSliderSettings(string optional)
    {
        try
        {
            CustomData cd = new CustomData();

            var settings = DataHelper.getSliderSettings();

            cd.data = settings;
            cd.count = settings.Count();
            return cd;

        }
        catch (Exception e)
        {
            ResultResponse res = new ResultResponse();
            res.isSuccess = false;
            res.message = e.Message;
            return res;
        }
    }


    // FEATURED COURSE

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SaveFeaturedCourseSetting(string IsGridView, string DisplayCount, string ManualSelection, string EnableMostView, string MostViewCount, string EnableMostRating, string MostRatingCount, string EnableRatingWithin, string RatingWithin, string Completed, string EnableMostEnrollee, string MostEnrolleeCount, string EnableAddedLast, string LastAddInMillisecond, string LastAddUnit, string EnableLastModified, string LastModifiedInMillisecond, string LastModifiedUnit, string Visible)
    {
        bool _IsGridView = Convert.ToBoolean(IsGridView);
        int _DisplayCount = Convert.ToInt32(DisplayCount);
        bool _ManualSelection = Convert.ToBoolean(ManualSelection);
        bool _EnableMostView = Convert.ToBoolean(EnableMostView);
        int _MostViewCount = Convert.ToInt32(MostViewCount);
        bool _EnableMostRating = Convert.ToBoolean(EnableMostRating);
        int _MostRatingCount = Convert.ToInt32(MostRatingCount);
        bool _EnableRatingWithin = Convert.ToBoolean(EnableRatingWithin);
        int _RatingWithin = Convert.ToInt32(RatingWithin);
        bool _Completed = Convert.ToBoolean(Completed);
        bool _EnableMostEnrollee = Convert.ToBoolean(EnableMostEnrollee);
        int _MostEnrolleeCount = Convert.ToInt32(MostEnrolleeCount);
        bool _EnableAddedLast = Convert.ToBoolean(EnableAddedLast);
        int _LastAddInMillisecond = Convert.ToInt32(LastAddInMillisecond);
        int _LastAddUnit = Convert.ToInt32(LastAddUnit);
        bool _EnableLastModified = Convert.ToBoolean(EnableLastModified);
        int _LastModifiedInMillisecond = Convert.ToInt32(LastModifiedInMillisecond);
        int _LastModifiedUnit = Convert.ToInt32(LastModifiedUnit);
        bool _Visible = Convert.ToBoolean(Visible);

        DataHelper.updateFeaturedSettings(_IsGridView, _DisplayCount, _ManualSelection, _EnableMostView, _MostViewCount, _EnableMostRating, _MostRatingCount, _EnableRatingWithin, _RatingWithin, _Completed, _EnableMostEnrollee, _MostEnrolleeCount, _EnableAddedLast, _LastAddInMillisecond, _LastAddUnit, _EnableLastModified, _LastModifiedInMillisecond, _LastModifiedUnit, _Visible);
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public dynamic GetFeaturedCourseList(int startRowIndex, int maximumRows, string sortExpression)
    {
        try
        {
            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                var courses = db.pr_TranscomUniversity_FeaturedCourse()
                .AsEnumerable()
                .Select(m => new
                {
                    m.CourseID,
                    EncryptedCourseID = Utils.Encrypt(m.CourseID),
                    m.CourseTitle,
                    m.CourseDescription,
                    m.CourseImage,
                    m.OrderNumber,
                    m.CourseType,
                    m.CourseDuration,
                    m.CourseCategory,
                    m.CourseSubcategory,
                    DateLastModified = string.Format("{0: yyyy/MM/dd hh:mm:ss tt}", m.DateLastModified),
                    m.CoursePath
                })
                .ToArray();

                var CustomData = new CustomData();
                CustomData.data = RadGrid.GetBindingData(courses.AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data;
                CustomData.count = courses.Count();

                return CustomData;
            }
        }
        catch (Exception)
        {
            var CustomData = new CustomData();
            CustomData.data = null;
            CustomData.count = 0;

            return CustomData;
            throw;
        }

    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultResponse SaveSelectedFeaturedCourses(string selectedCourses)
    {
        ResultResponse res = new ResultResponse();

        try
        {
            dynamic data = JsonConvert.DeserializeObject(selectedCourses);

            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                for (int i = 0; i < data.courses.Count; i++)
                {
                    int courseID = Convert.ToInt32(Utils.Decrypt(data.courses[i].Value));
                    db.pr_TranscomUniversity_InsertFeaturedCourse(courseID);
                }
                res.isSuccess = true;
                res.message = "";

                return res;
            }

        }
        catch (Exception e)
        {
            res.message = e.Message;
            res.isSuccess = false;
            return res;
        }
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public dynamic GetFeaturedCourseLibraryList(int startRowIndex, int maximumRows, string sortExpression)
    {
        try
        {
            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                var courses = db.pr_TranscomUniversity_FeaturedCourseLibrary()
                .AsEnumerable()
                .Select(c => new
                {
                    c.CourseID,
                    EncryptedCourseID = Utils.Encrypt(c.CourseID),
                    c.CourseTitle,
                    c.CourseImage,
                })
                .ToArray();

                var CustomData = new CustomData();
                CustomData.data = RadGrid.GetBindingData(courses.AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data;
                CustomData.count = courses.Count();

                return CustomData;
            }
        }
        catch (Exception)
        {
            var CustomData = new CustomData();
            CustomData.data = null;
            CustomData.count = 0;

            return CustomData;
            throw;
        }
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultResponse UpdateFeaturedCourseOrder(string jsonData)
    {
        ResultResponse res = new ResultResponse();

        try
        {
            dynamic data = JsonConvert.DeserializeObject(jsonData);

            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                for (int i = 0; i < data.courses.Count; i++)
                {
                    int courseID = Convert.ToInt32(Utils.Decrypt(data.courses[i].courseid.Value));
                    int orderNumber = Convert.ToInt32(data.courses[i].index.Value);

                    db.pr_TranscomUniversity_UpdateFeaturedCourseOrder(courseID, orderNumber);
                }
                res.isSuccess = true;
                res.message = "";

                return res;
            }

        }
        catch (Exception e)
        {
            res.message = e.Message;
            res.isSuccess = false;
            return res;
        }
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultResponse DeleteFeaturedCourse(string selectedCourses)
    {
        ResultResponse res = new ResultResponse();

        try
        {
            dynamic data = JsonConvert.DeserializeObject(selectedCourses);

            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                for (int i = 0; i < data.courses.Count; i++)
                {
                    int courseID = Convert.ToInt32(Utils.Decrypt(data.courses[i].Value));
                    db.pr_TranscomUniversity_DeleteFeaturedCourseInCatalog(courseID);
                }

                res.isSuccess = true;
                res.message = "";

                return res;
            }

        }
        catch (Exception e)
        {
            res.message = e.Message;
            res.isSuccess = false;
            return res;
        }
    }

    //testimonial

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public dynamic SaveNewTestimonialPlaylist(string playlistName, string user)
    {
        ResultResponse res = new ResultResponse();

        try
        {
            if (playlistName != "" || playlistName != null)
            {
                int currentUser = DataHelper.GetCimByEmail(user);
                using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
                {
                    //check if playlistname exist
                    var exist = db.tbl_transcomuniversity_Lkp_Testimonial
                                    .Where(t => t.Title.ToLower() == playlistName.ToLower())
                                    .SingleOrDefault();
                    if (exist != null)
                    {
                        res.isSuccess = false;
                        res.message = "Playlist name already exists, Please try another one.";
                    }
                    else
                    {
                        //save playlist
                        db.pr_TranscomUniversity_AddTestimonialPlaylist(playlistName, currentUser);
                        db.SaveChanges();

                        res.isSuccess = true;
                        res.message = "";
                    }

                    return res;
                }
            }
            else
            {
                res.isSuccess = false;
                res.message = "Playlist Name is Empty";

                return res;
            }

        }
        catch (Exception e)
        {
            res.message = e.Message;
            res.isSuccess = false;
            return res;
        }
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public dynamic GetOtherPlaylistTestimonial(string optional)
    {

        try
        {
            CustomData cd = new CustomData();
            TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();

            var playlistsList = db.tbl_transcomuniversity_Lkp_Testimonial
                               .Where(p => p.HideFromList == false)
                               .AsEnumerable()
                               .Select(p => new
                               {
                                   ID = Utils.Encrypt(p.TestimonialID),
                                   PlaylistName = p.Title,
                               })
                               .ToArray();
            cd.data = playlistsList;
            cd.count = playlistsList.Count();
            return cd;

        }
        catch (Exception e)
        {
            ResultResponse res = new ResultResponse();
            res.isSuccess = false;
            res.message = e.Message;
            return res;
        }
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public dynamic SaveTestimonialSetting(string maxImages, string playInterval, string loopDirection, string selectedPlaylistID, string user)
    {
        ResultResponse res = new ResultResponse();

        try
        {
            int _maxImage = Convert.ToInt32(maxImages);
            int _playInterval = Convert.ToInt32(playInterval);
            int _loopDirection = Convert.ToInt32(loopDirection);
            int _selectedPlaylistID = Convert.ToInt32(Utils.Decrypt(selectedPlaylistID));
            int currentUser = DataHelper.GetCimByEmail(user);
            if ((_maxImage > 0 && _maxImage < 20) || (_playInterval > 0 && _playInterval < 60) || (_loopDirection > 0 && _loopDirection < 5))
            {
                using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
                {
                    //save playlist
                    db.pr_TranscomUniversity_InsertOrUpdateTestimonialSettings(_maxImage, _playInterval, _loopDirection, _selectedPlaylistID, currentUser);
                    db.SaveChanges();

                    res.isSuccess = true;
                    res.message = "";

                    return res;
                }
            }
            else
            {
                res.isSuccess = false;
                res.message = "One of the input is not valid please check and try again.";

                return res;

            }
        }
        catch (Exception e)
        {
            res.message = e.Message;
            res.isSuccess = false;
            return res;
        }
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public dynamic GetAllUserComments(string sortIndex)
    {
        int index = Convert.ToInt32(sortIndex);
        try
        {
            CustomData cd = new CustomData();
            TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();

            DateTime dt1 = DateTime.Now;
            if (index == 1)
            {
                dt1 = DateTime.Now.AddHours(-12);
            }
            else if (index == 2)
            {
                dt1 = DateTime.Now.AddHours(-24);
            }
            else if (index == 3)
            {
                dt1 = DateTime.Now.AddDays(-7);
            }
            else if (index == 4)
            {
                dt1 = DateTime.Now.AddMonths(-1);
            }
            else if (index == 5)
            {
                dt1 = DateTime.Now.AddMonths(-6);
            }
            else if (index == 6)
            {
                dt1 = DateTime.Now.AddYears(-1);
            }

            var comments = db.vw_TranscomUniversity_CourseComments
                               .AsEnumerable()
                               .Where(c => c.DateCreated > dt1)
                               .Select(c => new
                               {
                                   CommentID = Utils.Encrypt(c.ReviewID),
                                   c.Comment,
                                   DateCreated = c.ShortDateCreated,
                                   c.UserImageUrl,
                                   c.Rating,
                                   c.Name,
                                   c.Title,
                                   UID = c.ReviewID
                               })
                               .ToArray();
            cd.data = comments;
            cd.count = comments.Count();

            return cd;

        }
        catch (Exception e)
        {
            ResultResponse res = new ResultResponse();
            res.isSuccess = false;
            res.message = e.Message;
            return res;
        }
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public dynamic AddCommentToTestimonial(string playlistID, string commentID, string user)
    {
        ResultResponse res = new ResultResponse();

        try
        {
            int _commentID = Convert.ToInt32(Utils.Decrypt(commentID));
            int _playlistID = Convert.ToInt32(Utils.Decrypt(playlistID));
            int currentUser = DataHelper.GetCimByEmail(user);

            if ((_commentID > 0) && (_playlistID > 0))
            {
                using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
                {
                    //save playlist
                    pr_transcomuniversity_InsertCommentInTestimonial_Result server_res = db.pr_transcomuniversity_InsertCommentInTestimonial(_playlistID, _commentID, currentUser).SingleOrDefault();

                    if (server_res.Response == 1)
                    {
                        res.isSuccess = true;
                        res.message = "";
                    }
                    else
                    {
                        res.isSuccess = false;
                        res.message = "Comment Already Exist in the selected Playlist";
                    }

                    return res;
                }
            }
            else
            {
                res.isSuccess = false;
                res.message = "One of the input is not valid please check and try again.";

                return res;

            }
        }
        catch (Exception e)
        {
            res.message = e.Message;
            res.isSuccess = false;
            return res;
        }
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public dynamic GetCurrentTestimonialPlaylist(string optional)
    {
        try
        {
            CustomData cd = new CustomData();
            TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();

            var comments = db.vw_TranscomUniversity_CurrentTestimonials
                               .AsEnumerable()
                               .Select(c => new
                               {
                                   CommentID = Utils.Encrypt(c.ID),
                                   c.OriginalComment,
                                   DateCreated = c.ShortDateCreated,
                                   c.UserImageUrl,
                                   c.Rating,
                                   c.Name,
                                   c.Title,
                                   UID = c.ID
                               })
                               .ToArray();
            cd.data = comments;
            cd.count = comments.Count();

            return cd;

        }
        catch (Exception e)
        {
            ResultResponse res = new ResultResponse();
            res.isSuccess = false;
            res.message = e.Message;
            return res;
        }
    }

    //FORUM

    #region CourseThread
    [System.Web.Services.WebMethod()]
    public dynamic GetThreadPosts(string startRowIndex, string maximumRows, string sortExpression, string threadID)
    {
        try
        {
            var db = new TranscomUniversityV3ModelContainer();
            int _threadID = Convert.ToInt32(Utils.Decrypt(threadID));
            int currentUser = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
            var threadPosts = db.pr_TranscomUniversity_ThreadPost(_threadID, currentUser)
                .AsEnumerable()
                .Select(p => new
                {
                    p.ThreadId,
                    p.PostId,
                    p.AuthorCIM,
                    p.AuthorName,
                    p.PostComment,
                    p.RoleTitle,
                    p.UserImageUrl,
                    dtDatePosted = p.DatePosted,
                    ButtonClassHelpful = p.Helpful == 0 ? "btn-teal" : "btn-lime",
                    QuotePostClass = p.QuotePostId > 0 ? "" : "display-none",
                    DatePosted = string.Format("{0: MMMM dd, yyyy. hh:mm tt}", p.DatePosted),
                    p.OriginalPostAuthor,
                    p.OriginalPostComment

                })
                .ToArray();

            var CustomData = new CustomData();
            CustomData.data = RadGrid.GetBindingData(threadPosts.AsQueryable(), Convert.ToInt32(startRowIndex), Convert.ToInt32(maximumRows), sortExpression, String.Empty).Data;
            CustomData.count = threadPosts.Count();

            return CustomData;
        }
        catch (Exception)
        {
            var CustomData = new CustomData();
            CustomData.data = null;
            CustomData.count = 0;

            return CustomData;
            throw;
        }
    }

    [System.Web.Services.WebMethod()]
    public int addNewPost(string comment, string _courseID, string _threadID)
    {
        try
        {

            int threadID = Convert.ToInt32(Utils.Decrypt(_threadID));
            int authorCIM = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
            var db = new TranscomUniversityV3ModelContainer();

            pr_TranscomUniversity_InsertPost_Result res = db.pr_TranscomUniversity_InsertPost(threadID, 0, authorCIM, comment).SingleOrDefault();


            if (res.Response == 1)
            {
                return 1;
                //rwmThread.RadAlert("Reply Successful", 330, 180, "Successful Post", "");
                //txtPostComment.Text = "";
            }
            else
                return 2;
            //rwmThread.RadAlert("Error Posting Reply. You're don't have permission to post in this thread.", 330, 180, "Error Post", "");
        }
        catch (Exception e)
        {
            return 3;
        }


    }

    [System.Web.Services.WebMethod()]
    public int btnHelpfulRes(int postId)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            int postid = Convert.ToInt32(postId);
            int usercim = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
            pr_TranscomUniversity_InsertHelpfulPost_Result res = db.pr_TranscomUniversity_InsertHelpfulPost(postid, usercim).SingleOrDefault();
            return res.Response;
        }
    }

    [System.Web.Services.WebMethod()]
    public int btnSubmitReport(int postId, string txtReportMessage)
    {
        int usercim = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
        string reportmessage = txtReportMessage;
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            pr_TranscomUniversity_InsertReportPost_Result res = db.pr_TranscomUniversity_InsertReportPost(postId, usercim, reportmessage).SingleOrDefault();
            return res.Response;
        }

    }

    [System.Web.Services.WebMethod()]
    public int btnSubmitReply(int postId, string _threadId, string txtReportMessage)
    {

        int usercim = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
        int threadId = Convert.ToInt32(Utils.Decrypt(_threadId));
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            pr_TranscomUniversity_InsertPost_Result res = db.pr_TranscomUniversity_InsertPost(threadId, postId, usercim, txtReportMessage).SingleOrDefault();

            return res.Response;
        }

    }


    #endregion

    //Course Builder
    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public dynamic UpdateCourseTrainingType(string courseID, string trainingTypeID)
    {
        try
        {
            CustomData cd = new CustomData();
            TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();
            int myCourseID = Convert.ToInt32(Utils.Decrypt(courseID));
            int myTrainingTypeID = Convert.ToInt32(trainingTypeID);
            var comments = db.pr_transcomuniversity_updateCourseTrainingType(myCourseID, myTrainingTypeID);

            ResultResponse res = new ResultResponse();
            res.isSuccess = true;
            res.message = "";

            return res;

        }
        catch (Exception e)
        {
            ResultResponse res = new ResultResponse();
            res.isSuccess = false;
            res.message = e.Message;
            return res;
        }
    }


    //My Profile - Learning Plan
    //[WebMethod]
    //[System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    //public dynamic GetLearningPlanMandatoryCourse(int startRowIndex, int maximumRows, string sortExpression, string user)
    //{
    //    try
    //    {
    //        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
    //        {
    //            int cim = DataHelper.GetCimByEmail(user);
    //            var courses = db.pr_TranscomUniversity_Lkp_AssignedEnrolledCourses(cim, string.Empty, user)
    //                .AsEnumerable()
    //                .Select(m => new
    //                {
    //                    CourseID = m.CourseId,
    //                    EncryptedCourseID = Utils.Encrypt(m.CourseId),
    //                    CourseTitle = m.Title,
    //                    CourseCategory = m.Category,
    //                    CategoryID = m.CategoryID,
    //                    CourseSubcategory = m.Subcategory,
    //                    SubcategoryID = m.SubcategoryID,
    //                    DateAdded = string.Format("{0: MM/dd/yyyy}", m.CreateDate),
    //                    DueDate = string.Format("{0: MM/dd/yyyy}", m.DueDate),
    //                    m.CourseType,
    //                    Progress = progressVal(m.CourseTypeID, m.HasTest, m.CourseLogin.ToString(), m.Passed, m.CourseId.ToString()),
    //                    m.CourseImage,
    //                    m.Status,
    //                    m.StatusId,
    //                })
    //                .Where(m => m.CategoryID == 19)
    //                .ToArray();

    //            var CustomData = new CustomData();
    //            CustomData.data = RadGrid.GetBindingData(courses.AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data;
    //            CustomData.count = courses.Count();

    //            return CustomData;
    //        }
    //    }
    //    catch (Exception e)
    //    {

    //        ResultResponse res = new ResultResponse();
    //        res.isSuccess = false;
    //        res.message = e.Message;
    //        return res;

    //    }

    //}

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public dynamic GetLearningPlanProgramSpecific(int startRowIndex, int maximumRows, string sortExpression, string user)
    {
        try
        {
            TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();
            int cim = DataHelper.GetCimByEmail(user);
            var result = db.pr_TranscomUniversity_Lkp_AssignedEnrolledCourses(cim, "", user)
                               .AsQueryable()
                               .Select(m => new
                               {
                                   CourseID = m.CourseId,
                                   EncryptedCourseID = Utils.Encrypt(m.CourseId),
                                   CourseTitle = m.Title,
                                   CourseCategory = m.Category,
                                   CategoryID = m.CategoryID,
                                   CourseSubcategory = m.Subcategory,
                                   SubcategoryID = m.SubcategoryID,
                                   DateAdded = string.Format("{0: MM/dd/yyyy}", m.CreateDate),
                                   DueDate = string.Format("{0: MM/dd/yyyy}", m.DueDate),
                                   m.CourseType,
                                   Progress = progressVal(m.CourseTypeID, m.HasTest, m.CourseLogin.ToString(), m.Passed, m.CourseId.ToString()),
                                   m.CourseImage,
                                   m.Status,
                                   m.StatusId,
                               })
                               .Where(m => m.CategoryID == 5)
                               .ToArray();

            var CustomData = new CustomData();
            CustomData.data = RadGrid.GetBindingData(result.AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data;
            CustomData.count = result.Count();

            return CustomData;

        }
        catch (Exception e)
        {
            ResultResponse res = new ResultResponse();
            res.isSuccess = false;
            res.message = e.Message;
            return res;
        }
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public dynamic GetLearningPlanLeadershipRecommended(int startRowIndex, int maximumRows, string sortExpression, string user)
    {
        try
        {
            TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();
            int cim = DataHelper.GetCimByEmail(user);
            var result = db.pr_TranscomUniversity_Lkp_AssignedEnrolledCourses(cim, "", user)
                               .AsEnumerable()
                               .Select(m => new
                               {
                                   CourseID = m.CourseId,
                                   EncryptedCourseID = Utils.Encrypt(m.CourseId),
                                   CourseTitle = m.Title,
                                   CourseCategory = m.Category,
                                   CategoryID = m.CategoryID,
                                   CourseSubcategory = m.Subcategory,
                                   SubcategoryID = m.SubcategoryID,
                                   DateAdded = string.Format("{0: MM/dd/yyyy}", m.CreateDate),
                                   DueDate = string.Format("{0: MM/dd/yyyy}", m.DueDate),
                                   StartDate = string.Format("{0: MM/dd/yyyy}", m.StartDate),
                                   EndDate = string.Format("{0: MM/dd/yyyy}", m.EndDate),
                                   m.Duration,
                                   m.CourseType,
                                   Progress = progressVal(m.CourseTypeID, m.HasTest, m.CourseLogin.ToString(), m.Passed, m.CourseId.ToString()),
                                   m.CourseImage,
                                   m.Status,
                                   m.StatusId,
                                   Rating = Convert.ToDecimal(m.Rating),
                                   m.Description
                               })
                               .Where(m => m.CategoryID == 17)
                               .ToArray();


            var CustomData = new CustomData();
            CustomData.data = RadGrid.GetBindingData(result.AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data;
            CustomData.count = result.Count();
            return CustomData;

        }
        catch (Exception e)
        {
            ResultResponse res = new ResultResponse();
            res.isSuccess = false;
            res.message = e.StackTrace + "-" + e.InnerException + e;
            return res;
        }
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public dynamic GetLearningPlanSkillDevelopment(int startRowIndex, int maximumRows, string sortExpression, string user)
    {
        try
        {
            TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();
            int cim = DataHelper.GetCimByEmail(user);
            var result = db.pr_TranscomUniversity_Lkp_AssignedEnrolledCourses(cim, "", user)
                               .AsQueryable()
                               .Select(m => new
                               {
                                   CourseID = m.CourseId,
                                   EncryptedCourseID = Utils.Encrypt(m.CourseId),
                                   CourseTitle = m.Title,
                                   CourseCategory = m.Category,
                                   CategoryID = m.CategoryID,
                                   CourseSubcategory = m.Subcategory,
                                   SubcategoryID = m.SubcategoryID,
                                   DateAdded = string.Format("{0: MM/dd/yyyy}", m.CreateDate),
                                   DueDate = string.Format("{0: MM/dd/yyyy}", m.DueDate),
                                   StartDate = string.Format("{0: MM/dd/yyyy}", m.StartDate),
                                   EndDate = string.Format("{0: MM/dd/yyyy}", m.EndDate),
                                   m.Duration,
                                   m.CourseType,
                                   Progress = progressVal(m.CourseTypeID, m.HasTest, m.CourseLogin.ToString(), m.Passed, m.CourseId.ToString()),
                                   m.CourseImage,
                                   m.Status,
                                   m.StatusId,
                                   Rating = Convert.ToDecimal(m.Rating),
                                   m.Description

                               })
                               .Where(m => m.CategoryID == 18)
                               .ToArray();

            var CustomData = new CustomData();
            CustomData.data = RadGrid.GetBindingData(result.AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data;
            CustomData.count = result.Count();

            return CustomData;

        }
        catch (Exception e)
        {
            ResultResponse res = new ResultResponse();
            res.isSuccess = false;
            res.message = e.Message;
            return res;
        }
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public dynamic GetLearningPlanMandatoryCourse(int startRowIndex, int maximumRows, string sortExpression, string user, int subcatID)
    {
        try
        {
            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                int cim = DataHelper.GetCimByEmail(user);
                var courses = db.pr_TranscomUniversity_Lkp_AssignedEnrolledCourses(cim, string.Empty, user)
                    .AsEnumerable()
                    .Select(m => new
                    {
                        CourseID = m.CourseId,
                        EncryptedCourseID = Utils.Encrypt(m.CourseId),
                        CourseTitle = m.Title,
                        CourseCategory = m.Category,
                        CategoryID = m.CategoryID,
                        CourseSubcategory = m.Subcategory,
                        SubcategoryID = m.SubcategoryID,
                        DateAdded = string.Format("{0: MM/dd/yyyy}", m.CreateDate),
                        DueDate = string.Format("{0: MM/dd/yyyy}", m.DueDate),
                        StartDate = string.Format("{0: MM/dd/yyyy}", m.StartDate),
                        EndDate = string.Format("{0: MM/dd/yyyy}", m.EndDate),
                        m.Duration,
                        m.CourseType,
                        Progress = progressVal(m.CourseTypeID, m.HasTest, m.CourseLogin.ToString(), m.Passed, m.CourseId.ToString()),
                        m.CourseImage,
                        m.Status,
                        m.StatusId,
                        Rating = Convert.ToDecimal(m.Rating),
                        m.Description
                    })
                    .Where(m => m.CategoryID == 19 && m.SubcategoryID == subcatID)
                    .ToArray();

                var CustomData = new CustomData();
                CustomData.data = RadGrid.GetBindingData(courses.AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data;
                CustomData.count = courses.Count();

                return CustomData;
            }
        }
        catch (Exception e)
        {

            ResultResponse res = new ResultResponse();
            res.isSuccess = false;
            res.message = e.Message;
            return res;

        }

    }

    public static string ToReadableString(int getTime)
    {
        TimeSpan span = TimeSpan.FromMinutes(getTime);

        string formatted = string.Format("{0}{1}{2}{3}",
            span.Duration().Days > 0 ? string.Format("{0:0} Day{1} and ", span.Days, span.Days == 1 ? String.Empty : "s") : string.Empty,
            span.Duration().Hours > 0 ? string.Format("{0:0} Hour{1} and ", span.Hours, span.Hours == 1 ? String.Empty : "s") : string.Empty,
            span.Duration().Minutes > 0 ? string.Format("{0:0} Minute{1} and ", span.Minutes, span.Minutes == 1 ? String.Empty : "s") : string.Empty,
            span.Duration().Seconds > 0 ? string.Format("{0:0} Second{1}", span.Seconds, span.Seconds == 1 ? String.Empty : "s") : string.Empty);

        if (formatted.EndsWith("and ")) formatted = formatted.Substring(0, formatted.Length - 4);

        if (string.IsNullOrEmpty(formatted)) formatted = "0 seconds";

        return formatted;
    }

    public static double progressVal(int? courseTypeID, int? hasTest, string login, int? passed, string courseId)
    {
        double progressval = 0.00;

        if (courseTypeID == 1)
        {
            if (hasTest > 0) //COURSE WITH ASSESSMENT
            {
                if (login == "1")
                {
                    progressval = 20;

                    if (passed == 1)
                    {
                        progressval = progressval + 80;
                    }
                }
            }
            else //COURSE WO ASSESSMENT
            {
                if (login == "1")
                {
                    progressval = 100;
                }
            }
        }

        else if (courseTypeID == 2)
        {

            var email = HttpContext.Current.User.Identity.Name.Split('|')[0];
            progressval = Convert.ToDouble(ScormDataHelper.GetOverallProgressByCourseID(email, Convert.ToInt32(courseId)));
        }

        return progressval;
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultResponse LogResourceLaunch(int CIM, string scoID, int scoTypeID, int courseID)
    {
        ResultResponse res = new ResultResponse();

        try
        {
            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                db.pr_TranscomUniversity_LogResourceLaunch(Convert.ToInt32(Utils.Decrypt(scoID)), scoTypeID, CIM);
                db.pr_TranscomUniversity_CourseLogin(CIM, courseID);


                res.isSuccess = true;
                res.message = "";
                return res;
            }

        }
        catch (Exception e)
        {
            res.message = e.Message;
            res.isSuccess = false;
            return res;
        }
    }


    /*LEADERSHIP COURSES*/
    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public dynamic GetRecommendedLeadershipCourse(int startRowIndex, int maximumRows, string sortExpression)
    {
        var CustomData = new CustomData();
        try
        {
            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {

                var result = db.vw_TranscomUniversity_AllCourses
                                .AsEnumerable()
                                .Select(c => new CourseDetails
                                {
                                    EncryptedCourseID = Utils.Encrypt(c.CourseID),
                                    CourseID = c.CourseID,
                                    CourseImage = c.CourseImage,
                                    CourseTitle = c.CourseTitle,
                                    CourseType = c.CourseType,
                                    CourseDuration = c.CourseDuration,
                                    CourseCategory = c.CourseCategory,
                                    CourseSubcategory = c.CourseSubcategory,
                                    CourseDescription = c.CourseDescription,
                                    DateLastModified = string.Format("{0: yyyy/MM/dd hh:mm:ss tt}", c.DateLastModified),
                                    CoursePath = c.CoursePath
                                })
                                .Where(c => c.CourseCategory.Contains("Leadership"))
                                .ToArray();

                CustomData.data = RadGrid.GetBindingData(result.AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data;
                CustomData.count = result.Count();
            }
        }
        catch (Exception)
        {
            throw;
        }

        return CustomData;

    }

    #region GET MANDATORY COURSE NEW FOR UPDATED HOME PAGE || MDQUERUBIN
    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public dynamic GetCourseMandatoryCourseNew(int startRowIndex, int maximumRows, string sortExpression)
    {
        var CustomData = new CustomData();
        try
        {
            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {

                var result = db.vw_TranscomUniversity_MandatoryCourse2New
                                .AsEnumerable()
                                .Select(c => new
                                {
                                    EncryptedCourseID = Utils.Encrypt(c.CourseID),
                                    CourseID = c.CourseID,
                                    CourseImage = c.CourseImage,
                                    CourseTitle = c.CourseTitle,
                                    CourseType = c.CourseType,
                                    CourseDuration = c.CourseDuration == 0 ? "0" : ToReadableString(Convert.ToInt32(c.CourseDuration)),
                                    CourseCategory = c.CourseCategory,
                                    CourseSubcategory = c.CourseSubcategory,
                                    CourseDescription = c.CourseDescription == "" ? "No Description" : c.CourseDescription,
                                    DateLastModified = string.Format("{0: yyyy/MM/dd hh:mm:ss tt}", c.DateLastModified),
                                    CoursePath = c.CoursePath,
                                    c.CategoryID,
                                    CourseViews = c.CourseViews == 0 ? 0 : c.CourseViews

                                })
                                .ToArray();

                CustomData.data = RadGrid.GetBindingData(result.AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data;
                CustomData.count = result.Count();
            }
        }
        catch (Exception)
        {
            throw;
        }

        return CustomData;

    }
    #endregion
    #region GET MANDATORY COURSE NEW FOR UPDATED HOME PAGE || MDQUERUBIN
    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public dynamic GetAllCoursesNew(int startRowIndex, int maximumRows, string sortExpression)
    {
        var CustomData = new CustomData();
        try
        {
            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {

                var result = db.vw_TranscomUniversity_AllCoursesNew
                                .AsEnumerable()
                                .Select(c => new
                                {
                                    EncryptedCourseID = Utils.Encrypt(c.CourseID),
                                    CourseID = c.CourseID,
                                    CourseImage = c.CourseImage,
                                    CourseTitle = c.CourseTitle,
                                    CourseType = c.CourseType,
                                    CourseDuration = c.CourseDuration == 0 ? "0" : ToReadableString(Convert.ToInt32(c.CourseDuration)),
                                    CourseCategory = c.CourseCategory,
                                    CourseSubcategory = c.CourseSubcategory,
                                    CourseDescription = c.CourseDescription == "" ? "No Description" : c.CourseDescription,
                                    DateLastModified = string.Format("{0: yyyy/MM/dd hh:mm:ss tt}", c.DateLastModified),
                                    CoursePath = c.CoursePath,
                                    c.CategoryID,
                                    CourseViews = c.CourseViews == 0 ? 0 : c.CourseViews

                                })
                                .ToArray();

                CustomData.data = RadGrid.GetBindingData(result.AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data;
                CustomData.count = result.Count();
            }
        }
        catch (Exception)
        {
            throw;
        }

        return CustomData;

    }
    #endregion
    #region GET PROGRAMS SPECIFIC COURSE NEW FOR UPDATED HOME PAGE || MDQUERUBIN
    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public dynamic GetProgramSpecificCourseNew(int startRowIndex, int maximumRows, string sortExpression, int cim, int SubcategoryID)
    {
        var CustomData = new CustomData();
        try
        {
            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {

                var result = db.vw_TranscomUniversity_ProgramSpecificCourseNew
                                .AsEnumerable()
                                .Select(c => new
                                {
                                    EncryptedCourseID = Utils.Encrypt(c.CourseID),
                                    CourseID = c.CourseID,
                                    CourseImage = c.CourseImage,
                                    CourseTitle = c.CourseTitle,
                                    CourseType = c.CourseType,
                                    CourseDuration = c.CourseDuration == 0 ? "0" : ToReadableString(Convert.ToInt32(c.CourseDuration)),
                                    CourseCategory = c.CourseCategory,
                                    CourseSubcategory = c.CourseSubcategory,
                                    CourseDescription = c.CourseDescription == "" ? "No Description" : c.CourseDescription,
                                    DateLastModified = string.Format("{0: yyyy/MM/dd hh:mm:ss tt}", c.DateLastModified),
                                    CoursePath = c.CoursePath,
                                    c.CategoryID,
                                    CourseViews = c.CourseViews == 0 ? 0 : c.CourseViews,
                                    c.enrolleeCIM,
                                    c.SubcategoryID
                                })
                                .Where(c => c.enrolleeCIM == cim && c.SubcategoryID == SubcategoryID)
                                .ToArray();

                CustomData.data = RadGrid.GetBindingData(result.AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data;
                CustomData.count = result.Count();
            }
        }
        catch (Exception)
        {
            throw;
        }

        return CustomData;

    }
    #endregion
    #region GET REGIONAL COURSE NEW FOR UPDATED HOME PAGE || MDQUERUBIN
    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public dynamic GetCourseRegionalCourseNew(int startRowIndex, int maximumRows, string sortExpression)
    {
        var CustomData = new CustomData();
        try
        {
            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {

                var result = db.vw_TranscomUniversity_MandatoryCourse2New
                                .AsEnumerable()
                                .Select(c => new
                                {
                                    EncryptedCourseID = Utils.Encrypt(c.CourseID),
                                    CourseID = c.CourseID,
                                    CourseImage = c.CourseImage,
                                    CourseTitle = c.CourseTitle,
                                    CourseType = c.CourseType,
                                    CourseDuration = c.CourseDuration == 0 ? "0" : ToReadableString(Convert.ToInt32(c.CourseDuration)),
                                    CourseCategory = c.CourseCategory,
                                    CourseSubcategory = c.CourseSubcategory,
                                    CourseDescription = c.CourseDescription == "" ? "No Description" : c.CourseDescription,
                                    DateLastModified = string.Format("{0: yyyy/MM/dd hh:mm:ss tt}", c.DateLastModified),
                                    CoursePath = c.CoursePath,
                                    c.CategoryID,
                                    CourseViews = c.CourseViews == 0 ? 0 : c.CourseViews,
                                    c.SubcategoryID

                                })
                                .Where(c => c.SubcategoryID == 30)
                                .ToArray();

                CustomData.data = RadGrid.GetBindingData(result.AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data;
                CustomData.count = result.Count();
            }
        }
        catch (Exception)
        {
            throw;
        }

        return CustomData;

    }
    #endregion
    #region GET COUNTRY COURSE NEW FOR UPDATED HOME PAGE || MDQUERUBIN
    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public dynamic GetCourseCountryCourseNew(int startRowIndex, int maximumRows, string sortExpression)
    {
        var CustomData = new CustomData();
        try
        {
            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {

                var result = db.vw_TranscomUniversity_MandatoryCourse2New
                                .AsEnumerable()
                                .Select(c => new
                                {
                                    EncryptedCourseID = Utils.Encrypt(c.CourseID),
                                    CourseID = c.CourseID,
                                    CourseImage = c.CourseImage,
                                    CourseTitle = c.CourseTitle,
                                    CourseType = c.CourseType,
                                    CourseDuration = c.CourseDuration == 0 ? "0" : ToReadableString(Convert.ToInt32(c.CourseDuration)),
                                    CourseCategory = c.CourseCategory,
                                    CourseSubcategory = c.CourseSubcategory,
                                    CourseDescription = c.CourseDescription == "" ? "No Description" : c.CourseDescription,
                                    DateLastModified = string.Format("{0: yyyy/MM/dd hh:mm:ss tt}", c.DateLastModified),
                                    CoursePath = c.CoursePath,
                                    c.CategoryID,
                                    CourseViews = c.CourseViews == 0 ? 0 : c.CourseViews,
                                    c.SubcategoryID

                                })
                                .Where(c => c.SubcategoryID == 31)
                                .ToArray();

                CustomData.data = RadGrid.GetBindingData(result.AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data;
                CustomData.count = result.Count();
            }
        }
        catch (Exception)
        {
            throw;
        }

        return CustomData;

    }
    #endregion


    /*Get Course by Category*/
    /*LEADERSHIP COURSES*/
    [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public dynamic GetCourseByCategory(int startRowIndex, int maximumRows, string sortExpression, string catID)
    {
        var CustomData = new CustomData();
        int categoryID = Convert.ToInt32(Utils.Decrypt(catID));
        try
        {
            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                
                var result = db.vw_TranscomUniversity_AllCourses
                                .AsEnumerable()
                                .Select(c => new
                                {
                                    EncryptedCourseID = Utils.Encrypt(c.CourseID),
                                    CourseID = c.CourseID,
                                    CourseImage = c.CourseImage,
                                    CourseTitle = c.CourseTitle,
                                    CourseType = c.CourseType,
                                    CourseDuration = c.CourseDuration == 0 ? "0" : ToReadableString(Convert.ToInt32(c.CourseDuration)),
                                    CourseCategory = c.CourseCategory,
                                    CourseSubcategory = c.CourseSubcategory,
                                    CourseDescription = c.CourseDescription == "" ? "No Description" : c.CourseDescription,
                                    DateLastModified = string.Format("{0: yyyy/MM/dd hh:mm:ss tt}", c.DateLastModified),
                                    CoursePath = c.CoursePath,
                                    c.CategoryID

                                })
                                .Where(c => c.CategoryID == categoryID)
                                .ToArray();

                CustomData.data = RadGrid.GetBindingData(result.AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data;
                CustomData.count = result.Count();
            }
        }
        catch (Exception)
        {
            throw;
        }

        return CustomData;

    }

     [WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public dynamic GetSupervisorDetails(int userCIM)
    {
        var CustomData = new CustomData();
        try
        {
            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                return db.pr_TranscomUniversity_GetSupervisorDetails(userCIM).SingleOrDefault();
                
            }
        }
        catch (Exception)
        {
            return null;
            throw;
          
        }

    }

     [WebMethod]
     [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
     public dynamic EncryptOffset(string offset)
     {
         var encrypted = Utils.Encrypt(Convert.ToInt32(offset));
 
        return encrypted;
     }

    public class CustomData
    {
        public IEnumerable<dynamic> data { get; set; }
        public int count { get; set; }
    }

    public class ResultResponse
    {
        public string message { get; set; }
        public bool isSuccess { get; set; }
    }


    public class CourseDetails
    {
        public string EncryptedCourseID { get; set; }

        public int? CourseID { get; set; }

        public string CourseImage { get; set; }

        public string CourseTitle { get; set; }

        public string CourseType { get; set; }

        public int? CourseDuration { get; set; }

        public string CourseCategory { get; set; }

        public string CourseSubcategory { get; set; }

        public string CourseDescription { get; set; }

        public string DateLastModified { get; set; }

        public string CoursePath { get; set; }
    }
}
