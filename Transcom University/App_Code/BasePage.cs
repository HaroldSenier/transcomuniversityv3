﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading;
using System.Globalization;
using System.Web.Security;

/// <summary>
/// Summary description for BasePage
/// </summary>
///

public class BasePage : System.Web.UI.Page
{
    protected override void InitializeCulture()
    {
        string language= String.Empty;
        try
        {
            language = Session["CurrLang"].ToString();
        }
        catch
        { language = null; }

        CultureInfo ci = CultureInfo.InstalledUICulture;
        String CI_Info = ci.Name;

        if (language != null)
        {
            String selectedLanguage = language;
            UICulture = selectedLanguage;
            Culture = selectedLanguage;

            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(selectedLanguage);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(selectedLanguage);
        }
        else
        {
            String selectedLanguage = CI_Info;
            UICulture = selectedLanguage;
            Culture = selectedLanguage;

            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(selectedLanguage);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(selectedLanguage);
        }
        base.InitializeCulture();
    }
}