﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Web.Configuration;
using System.Security.Cryptography;
using System.IO;
using System.Text;
using TranscomUniversityV3Model;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Configuration;

/// <summary>
/// Summary description for DataHelper
/// </summary>
public class DataHelper
{
    readonly Database _db;
    static TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();

    public DataHelper(string database)
    {
        _db = DatabaseFactory.CreateDatabase(database);

    }

    public static dynamic GetUsers()
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var users = (from r in db.vw_TranscomUniversity_UsersInRoles
                         select r).ToList();

            if (users != null)
                return users;
        }
        return null;
    }

    public static dynamic GetUsersInfo()
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var transcomEmail = HttpContext.Current.User.Identity.Name.Split('|')[0];
            //var transcomEmail = "eunice.macuha@transcom.com";

            var usersInfo = (from ui in db.vw_TranscomUniversity_UsersInfo
                             where ui.TranscomEmail == transcomEmail
                             select ui).ToList();

            if (usersInfo != null)
                return usersInfo;
        }
        return null;
    }

    public static string GetGoogleID()
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var transcomEmail = HttpContext.Current.User.Identity.Name.Split('|')[0];
            //var transcomEmail = "eunice.macuha@transcom.com";

            var retVal = (from u in db.vw_TranscomUniversity_UsersInfo
                          where u.TranscomEmail == transcomEmail
                          select u.GoogleID).FirstOrDefault().ToString();

            if (retVal != null)
                return retVal;
        }
        return null;
    }

    public static dynamic GetCareerMap()
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var careerMap = (from cm in db.vw_TranscomUniversity_CareerMap
                             select cm).ToList();

            if (careerMap != null)
                return careerMap;
        }

        return null;
    }

    public static dynamic GetMandatoryCourses()
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var mandatoryCourses = (from r in db.vw_TranscomUniversity_MandatoryCourses
                                    select r).ToList();

            if (mandatoryCourses != null)
                return mandatoryCourses;
        }
        return null;
    }

    public static dynamic GetTrendingCourses()
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var trendingCourses = (from r in db.vw_TranscomUniversity_TrendingCourses
                                   select r).ToList();

            if (trendingCourses != null)
                return trendingCourses;
        }
        return null;
    }

    public static dynamic GetRecentlyAddedCourses()
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var recentlyAddedCourses = (from r in db.vw_TranscomUniversity_RecentlyAddedCourses
                                        select r).ToList();

            if (recentlyAddedCourses != null)
                return recentlyAddedCourses;
        }
        return null;
    }

    public static string GetCurrentUserCIM()
    {
        try
        {
            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                var transcomEmail = HttpContext.Current.User.Identity.Name.Split('|')[0];
                //var transcomEmail = "eunice.macuha@transcom.com";
                var retVal = (from u in db.vw_TranscomUniversity_UsersInfo
                              where u.TranscomEmail == transcomEmail
                              select u.CIMNumber).FirstOrDefault().ToString();


                if (retVal != null)
                    return retVal;
            }
        }
        catch (Exception)
        {
            return null;
            throw;
        }
        return null;
      
    }

    public static dynamic GetPendingEnrollee(int courseId)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var pendingEnrollee = (from p in db.vw_TranscomUniversity_PendingEnrollments
                                   where p.CourseID == courseId
                                   select p).OrderByDescending( a=> a.EnrollmentID)
                                   .ToList();

            if (pendingEnrollee != null)
                return pendingEnrollee;
        }
        return null;
    }

    public static dynamic GetApprovedEnrollee(int courseId)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var approvedEnrollee = (from a in db.vw_TranscomUniversity_ApprovedEnrollments
                                    where a.CourseID == courseId
                                    select a).OrderByDescending(a => a.EnrolleeID)
                                    .ToList();

            if (approvedEnrollee != null)
                return approvedEnrollee;
        }
        return null;
    }

    public static dynamic GetAllActiveCourses()
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var allActiveCourses = (from a in db.vw_TranscomUniversity_GetAllActiveCourses
                                    select a).ToList();

            if (allActiveCourses != null)
                return allActiveCourses;
        }
        return null;
    }

    public static dynamic GetAllTrainers(int courseId)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var trainers = (from t in db.vw_TranscomUniversity_CourseTrainers
                            where t.CourseID == courseId
                            select t).ToList();

            if (trainers != null)
                return trainers;
        }
        return null;
    }

    public static void AddPlaylist(int UserID, string PlaylistName)
    {
        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();

            SqlCommand cmd;

            string query = @"pr_TranscomUniversity_AddPlaylist";

            cmd = new SqlCommand(query, cn);
            cmd.Parameters.Add("@PlaylistName", PlaylistName);
            cmd.Parameters.Add("@CreatedBy", UserID);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.ExecuteNonQuery();
        }
    }

    public static void UpdatePlaylist(int ID, bool isActive)
    {
        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();

            SqlCommand cmd;

            string query = @"pr_TranscomUniversity_UpdatePlaylist";

            cmd = new SqlCommand(query, cn);
            cmd.Parameters.Add("@ID", ID);
            cmd.Parameters.Add("@IsActive", isActive);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.ExecuteNonQuery();
        }
    }

    public static DataSet GetAllPlaylists(bool inactive)
    {
        DataSet ds = new DataSet();

        int c = 0;

        if (inactive == true)
            c = 1;



        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();

            SqlCommand cmd;
            string query = @"SELECT * FROM tbl_trancomuniversity_Playlist WHERE Inactive =" + c;

            cmd = new SqlCommand(query, cn);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
        }
        return ds;
    }

    public static DataSet GetPlaylist(int PlaylistID)
    {
        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();

            SqlCommand cmd;
            string query = @"SELECT * FROM tbl_trancomuniversity_Playlist WHERE ID =" + PlaylistID;

            cmd = new SqlCommand(query, cn);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
        }
        return ds;
    }

    public static DataSet GetAllPlaylistsSlides()
    {
        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();

            SqlCommand cmd;
            string query = @"
                            SELECT 
	                            a.ID PlaylistID, a.PlaylistName
	                            , b.* 
                            FROM tbl_trancomuniversity_Playlist a
                            JOIN tbl_trancomuniversity_PlaylistSlides b
                            ON a.ID = b.PlaylistID
                            ";

            cmd = new SqlCommand(query, cn);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
        }
        return ds;
    }

    public static DataSet GetActivePlaylist()
    {
        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();

            SqlCommand cmd;
            string query = @"
                            SELECT 
	                            a.ID PlaylistID, a.PlaylistName
	                            , b.* 
                            FROM tbl_trancomuniversity_Playlist a
                            JOIN tbl_trancomuniversity_PlaylistSlides b
                            ON a.ID = b.PlaylistID WHERE Inactive = 0
                            ";

            cmd = new SqlCommand(query, cn);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
        }
        return ds;
    }

    public static DataSet GetSelectedPlaylist(int PlaylistID)
    {
        DataSet ds = new DataSet();



        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();

            SqlCommand cmd;
            string query = @"
                            SELECT 
	                            a.ID PlaylistID, a.PlaylistName
	                            , b.* 
                            FROM tbl_trancomuniversity_Playlist a
                            JOIN tbl_trancomuniversity_PlaylistSlides b
                            ON a.ID = b.PlaylistID WHERE a.ID = 
                            " + PlaylistID;

            cmd = new SqlCommand(query, cn);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
        }
        return ds;
    }

    public static DataSet GetSelectedSlide(int SlideID)
    {
        DataSet ds = new DataSet();



        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();

            SqlCommand cmd;
            string query = @"
                            SELECT TOP 1
	                            a.ID PlaylistID, a.PlaylistName
	                            , b.* 
                            FROM tbl_trancomuniversity_Playlist a
                            JOIN tbl_trancomuniversity_PlaylistSlides b
                            ON a.ID = b.PlaylistID WHERE b.ID = 
                            " + SlideID;

            cmd = new SqlCommand(query, cn);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
        }
        return ds;
    }

    public static void AddSlide(int PlaylistID, string FileName, string FileDesc, int UpdatedBy)
    {
        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();

            SqlCommand cmd;

            string query = @"pr_TranscomUniversity_AddSlide";

            cmd = new SqlCommand(query, cn);
            cmd.Parameters.Add("@PlaylistID", PlaylistID);

            cmd.Parameters.Add("@SlideImg", FileName);
            cmd.Parameters.Add("@SlideDesc", FileDesc);
            cmd.Parameters.Add("@UpdatedBy", UpdatedBy);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.ExecuteNonQuery();
        }
    }

    public static void UpdateSlide(int SlideID, string FileName, string FileDesc)
    {
        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();

            SqlCommand cmd;

            string query = @"pr_TranscomUniversity_UpdateSlide";

            cmd = new SqlCommand(query, cn);
            cmd.Parameters.Add("@SlideID", SlideID);

            cmd.Parameters.Add("@Description", FileDesc);
            cmd.Parameters.Add("@ImgFile", FileName);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.ExecuteNonQuery();
        }
    }

    public static void DeleteSlide(int SlideID)
    {
        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();

            SqlCommand cmd;

            string query = @"pr_TranscomUniversity_DeleteSlide";

            cmd = new SqlCommand(query, cn);
            cmd.Parameters.Add("@SlideID", SlideID);


            cmd.CommandType = CommandType.StoredProcedure;
            cmd.ExecuteNonQuery();
        }
    }

    public static DataSet GetNewsCategories()
    {
        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();

            SqlCommand cmd;
            string query = @"SELECT * FROM tbl_trancomuniversity_NewsCategories";

            cmd = new SqlCommand(query, cn);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
        }
        return ds;
    }

    public static DataSet GetAllNewsArticles()
    {
        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();

            SqlCommand cmd;
            string query = @"SELECT
                            a.*
                            ,
                            SUBSTRING(
	                            (SELECT ',' + tbl_trancomuniversity_NewsCategories.name
	                            FROM tbl_trancomuniversity_NewsCategories
	                            INNER JOIN tbl_trancomuniversity_NewsArticleCategories 
	                            ON tbl_trancomuniversity_NewsCategories.id = tbl_trancomuniversity_NewsArticleCategories.newsCategoryID
	                            AND tbl_trancomuniversity_NewsArticleCategories.newsArticleID = a.id
	                            FOR XML PATH('')),2,200000) AS category
                            FROM tbl_trancomuniversity_NewsArticles a
                            WHERE Inactive <> 1
                            ORDER BY a.PublishedOn DESC
                            ";

            cmd = new SqlCommand(query, cn);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
        }
        return ds;
    }

    public static DataSet GetAllArticles()
    {
        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();

            SqlCommand cmd;
            string query = @"SELECT
                            a.*
                            , b.name category
                            FROM tbl_trancomuniversity_NewsArticles a
                            JOIN tbl_trancomuniversity_NewsCategories b
                            ON b.id = a.CategoryID
                            --WHERE Inactive <> 1
                            ORDER BY a.PublishedOn DESC";

            cmd = new SqlCommand(query, cn);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
        }
        return ds;
    }

    public static DataSet GetAllFeaturedCourses()
    {
        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();

            SqlCommand cmd;
            string query = @"SELECT * FROM tbl_trancomuniversity_FeaturedCourse ORDER BY Created DESC";

            cmd = new SqlCommand(query, cn);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
        }
        return ds;
    }

    public static void InsertNews(string NewsTitle, string NewContent, int AuthorID, string FeaturedImage, int CategoryID)
    {
        //string headLine = NewContent.Substring(0, NewContent.IndexOf(Environment.NewLine));

        string headLine = new string(NewContent.TakeWhile(c => c != '\n').ToArray());

        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();

            SqlCommand cmd;

            string query = @"pr_TranscomUniversity_AddNews";

            cmd = new SqlCommand(query, cn);
            cmd.Parameters.Add("@NewsTitle", NewsTitle);
            cmd.Parameters.Add("@NewsShortDesc", headLine);
            cmd.Parameters.Add("@NewContent", NewContent);
            cmd.Parameters.Add("@AuthorID", AuthorID);
            cmd.Parameters.Add("@FeaturedImage", FeaturedImage);
            cmd.Parameters.Add("@CategoryID", CategoryID);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.ExecuteNonQuery();
        }
    }

    public static void UpdateNews(int NewsID, string NewsTitle, string NewContent, bool Inactive, string FeaturedImage, int CategoryID)
    {
        string headLine = new string(NewContent.TakeWhile(c => c != '\n').ToArray());

        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();

            SqlCommand cmd;

            string query = @"pr_TranscomUniversity_UpdateNews";

            cmd = new SqlCommand(query, cn);
            cmd.Parameters.Add("@NewsID", NewsID);
            cmd.Parameters.Add("@NewsTitle", NewsTitle);
            cmd.Parameters.Add("@NewsShortDesc", headLine);
            cmd.Parameters.Add("@NewContent", NewContent);
            cmd.Parameters.Add("@Inactive", Inactive);
            cmd.Parameters.Add("@FeaturedImage", FeaturedImage);
            cmd.Parameters.Add("@CategoryID", CategoryID);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.ExecuteNonQuery();
        }
    }

    public static DataSet GetAllUserIntCerts(string GoogleUserID, int? CourseID)
    {

        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();

            string extraQuery = "";
            if (CourseID != null)
            {
                //extraQuery = " AND courseCat.CategoryID = " + Convert.ToInt32(CourseID);
                extraQuery = " AND courseDetails.CategoryID = " + Convert.ToInt32(CourseID);
            }


            SqlCommand cmd;
//            string query = @"SELECT 
//	                            usrCert.ID
//	                            , courseDetails.ID CourseID 
//	                            , courseDetails.Title CertificateName
//	                            , usrCert.DateAcquired 
//                            FROM tbl_transcomuniversity_UserCertificate usrCert
//                            JOIN tbl_trancomuniversity_CourseDetails courseDetails 
//                            ON usrCert.CourseID = courseDetails.ID
//                            JOIN tbl_trancomuniversity_CourseDetailsCategory courseCat
//                            ON courseCat.CourseID = courseDetails.ID
//                            WHERE usrCert.GoogleUserID = '" + GoogleUserID + "'" + extraQuery + " AND CertType = 2  ORDER BY usrCert.DateAcquired DESC";

            string query = @"SELECT 
	                            usrCert.ID,
                                courseDetails.Title encryptedID
	                            , courseDetails.courseID CourseID 
	                            , courseDetails.Title CertificateName	                            
	                            , usrCert.DateAcquired 
	                            , UI.FirstName + ' ' + UI.LastName FullName
                            FROM tbl_transcomuniversity_UserCertificate usrCert
                            INNER JOIN tbl_TranscomUniversity_Cor_Course courseDetails 
                            ON usrCert.CourseID = courseDetails.courseID 
                            INNER JOIN tbl_TranscomUniversity_Cor_UsersInfo UI
                            ON usrCert.GoogleUserID = UI.GoogleID
                            WHERE usrCert.GoogleUserID = '" + GoogleUserID + "'" + extraQuery + " AND CertType = 0  ORDER BY usrCert.DateAcquired DESC";

            cmd = new SqlCommand(query, cn);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
        }
        return ds;
    }

    public static DataSet GetAllUserExtCerts(string GoogleUserID)
    {

        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();



            SqlCommand cmd;
            string query = @"SELECT *,GoogleUserID encryptedID FROM tbl_transcomuniversity_UserCertificate WHERE GoogleUserID = '" + GoogleUserID + "' AND CertType = 1 ORDER BY DateAcquired DESC";

            cmd = new SqlCommand(query, cn);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
        }
        return ds;
    }

    public static DataSet GetAllCourseCategory()
    {


        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();

            SqlCommand cmd;
            string query = @"SELECT CategoryID as 'ID', Description, Category as 'Name' FROM [tbl_TranscomUniversity_Lkp_Category] where hidefromlist = 0";

            cmd = new SqlCommand(query, cn);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
        }
        return ds;
    }

    public static DataSet GetCert(int CertID)
    {


        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();

            SqlCommand cmd;
            string query = @"SELECT * FROM tbl_transcomuniversity_UserCertificate WHERE ID = " + CertID;

            cmd = new SqlCommand(query, cn);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
        }
        return ds;
    }

    public static void InsertCert(string a, string b, string c, DateTime d, int? e, string f, bool g)
    {

        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();

            SqlCommand cmd;

            string query = @"pr_AddCertificate";

            cmd = new SqlCommand(query, cn);
            cmd.Parameters.Add("@GoogleUserID", a);
            cmd.Parameters.Add("@CertificateName", b);
            cmd.Parameters.Add("@Description", c);
            cmd.Parameters.Add("@DateAcquired", d);
            cmd.Parameters.Add("@CourseID", (e.HasValue ? (object)e.Value : (object)DBNull.Value));
            cmd.Parameters.Add("@ImgFile", f);
            cmd.Parameters.Add("@CertType", g);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.ExecuteNonQuery();
        }
    }

    public static DataSet GetAllCertBadges(int CertID)
    {
        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();
            SqlCommand cmd;
            string query = @"SELECT 
                                a.CertID, b.* 
                            FROM tbl_transcomuniversity_CertBadge a 
                            JOIN tbl_transcomuniversity_Lkp_Badge b
                            ON a.BadgeID = b.ID 
                            WHERE a.CertID = " + CertID;

            cmd = new SqlCommand(query, cn);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
        }
        return ds;
    }

    public static DataSet GetCourse(int CourseID)
    {
        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();
            SqlCommand cmd;
            string query = @"SELECT 
                              courseDetails.*
                            FROM tbl_TranscomUniversity_Cor_Course courseDetails
                             WHERE courseDetails.courseID =" + CourseID;

            cmd = new SqlCommand(query, cn);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
        }
        return ds;
    }

    public static DataSet GetAllCategories()
    {
        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();
            SqlCommand cmd;
            string query = @"SELECT '-1' as CategoryID,'- Select Category -' as  Category
                                UNION 
                                SELECT 
                                CategoryID, Category 
                                FROM tbl_TranscomUniversity_Lkp_Category 
                                WHERE [HideFromList] = 0 
                                Order by Category asc";
            cmd = new SqlCommand(query, cn);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
        }
        return ds;
    }

    public static DataSet GetAllCourses()
    {
        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();
            SqlCommand cmd;
            string query = @"SELECT 
                                CourseID, Title
                                FROM tbl_TranscomUniversity_Cor_Course
                                WHERE [HideFromList] = 0 
                                Order by Title asc";
            cmd = new SqlCommand(query, cn);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
        }
        return ds;
    }

    public static DataSet GetFilterCourses(int a, int b)
    {
        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();
            SqlCommand cmd;
            string query = @"SELECT 
                                CourseID, Title
                                FROM tbl_TranscomUniversity_Cor_Course
                                WHERE [HideFromList] = 0 and
                                [CategoryID] = " + a + " and [SubcategoryID] =" + b + " Order by Title asc";
            cmd = new SqlCommand(query, cn);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
        }
        return ds;
    }

    public static dynamic GetSubcategory(int categoryID)
    {

        using (var db = new TranscomUniversityV3ModelContainer())
        {
            var subcategory = db.tbl_TranscomUniversity_Lkp_Subcategory
                                 .Where(c => c.HideFromList == false && c.CategoryID == categoryID)
                                 .OrderBy(c => c.Subcategory)
                                 .ToList();
            return subcategory;
        }
        return null;
    }

    public static DataSet GetAllClient()
    {
        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();
            SqlCommand cmd;
            string query = @"select * from tbl_TranscomUniversity_Lkp_Client where HideFromList=0";
            cmd = new SqlCommand(query, cn);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
        }
        return ds;
    }

    public static void InsertCourse(string b, int c, int d, string e, DateTime f, DateTime g, DateTime h, int i, int j, string k, string l, int m, int o, int p, string q, string r, int s, string t)
    {
        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();
            SqlCommand cmd;
            string query = @"pr_TranscomUniversity_AddCourse";

            cmd = new SqlCommand(query, cn);
            //cmd.Parameters.Add("@CourseId", a);
            cmd.Parameters.AddWithValue("@CourseTitle", b);
            cmd.Parameters.AddWithValue("@CategoryID", c);
            cmd.Parameters.AddWithValue("@SubCategoryID", d);
            cmd.Parameters.AddWithValue("@Description", e);
            cmd.Parameters.AddWithValue("@DateCreated", f);
            cmd.Parameters.AddWithValue("@StartDate", g);
            cmd.Parameters.AddWithValue("@EndDate", h);
            cmd.Parameters.AddWithValue("@PublishedBy", i);
            cmd.Parameters.AddWithValue("@Duration", j);
            cmd.Parameters.AddWithValue("@AuthorName", k);
            cmd.Parameters.AddWithValue("@Department", l);
            cmd.Parameters.AddWithValue("@RequiredProgramID", m);
            cmd.Parameters.AddWithValue("@YearsInTenure", o);
            cmd.Parameters.AddWithValue("@YearsInRole", p);
            cmd.Parameters.AddWithValue("@AccessMode", q);
            cmd.Parameters.AddWithValue("@courseImage", r);
            cmd.Parameters.AddWithValue("@CourseType", s);
            cmd.Parameters.AddWithValue("@JobLevel", t);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.ExecuteNonQuery();
        }
    }

    public static void UpdateCourse(int courseID, string CourseTitle, int CategoryID, int SubCategoryID, string Description, DateTime DateCreated, DateTime StartDateTime, DateTime EndDateTime, int CIMNo, int Duration, string Author, string DepartmentOwner, int RequiredProg, int YearsInTenure, int YearsInRoles, string AccessMode, string CourseImage, int CourseType, string JobLevel)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var ds = db.pr_TranscomUniversity_UpdateCourse(courseID, CourseTitle, CategoryID, SubCategoryID, Description, DateCreated, StartDateTime, EndDateTime, CIMNo, Duration, Author, DepartmentOwner, RequiredProg, YearsInTenure, YearsInRoles, AccessMode, CourseImage, CourseType, JobLevel);

        }
    }
    
    public static bool isCIMValid(string usercim)
    {
        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();
            SqlCommand cmd;
            //string query = @"select top 1 * from SAP.[dbo].[tbl_SAP_Hld_Export] where End_Date='00000000' and CIM_Number=" + usercim;
            string query = @"select top 1 * from tbl_TranscomUniversity_Cor_UsersInfo where CIMNumber=" + usercim;
            cmd = new SqlCommand(query, cn);
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                return true;
            }
        }
        return false;
    }

    public static void insertRequiredCourse(int courseID, List<int> RequiredCourseID)
    {
        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();
            SqlCommand cmd;
            string values = "";
            int c = RequiredCourseID.Count;
            int i = 1;
            if (c > 0)
            {
                foreach (var item in RequiredCourseID)
                {
                    if (c == i)
                    {
                        values += " SELECT '" + courseID + "', '" + item + "'";
                    }
                    else
                    {
                        values += " SELECT '" + courseID + "', '" + item + "' UNION ALL ";
                    }

                    i++;
                }
                string query = @"insert into [tbl_TranscomUniversity_Rlt_RequiredCourse] ([CourseID], [RequiredCourse]) " + values;
                cmd = new SqlCommand(query, cn);
                cmd.CommandType = CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
            }
        }
    }

    public static DataSet GetCourseDetails(int CourseID)
    {
        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();
            SqlCommand cmd;
            string query = @"SELECT 
                            a.*,
                            isnull(a.JobLevel, '') JobLevels
                            , cat.Category CategoryName
                            , subcat.Subcategory
                            ,ISNULL(CL.Client, '0') RequiredProgramName
                            FROM tbl_TranscomUniversity_Cor_Course a
                            JOIN tbl_TranscomUniversity_Lkp_Category cat
                            ON a.CategoryID = cat.CategoryID
                            JOIN tbl_TranscomUniversity_Lkp_Subcategory subcat
                            ON a.SubCategoryID = subcat.SubCategoryID
                            LEFT JOIN tbl_TranscomUniversity_Lkp_Client CL
                            ON a.RequiredProgram = CL.ClientID
                            WHERE a.CourseID = " + CourseID;
            cmd = new SqlCommand(query, cn);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
        }
        return ds;
    }

    public static string callClientScript(string functionName)
    {
        return "function f(){" + functionName + "(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
    }

    public static string Decrypt(string cipherText)
    {
        string EncryptionKey = "TRNSCMV32017111";
        cipherText = cipherText.Replace(" ", "+");
        byte[] cipherBytes = Convert.FromBase64String(cipherText);

        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(cipherBytes, 0, cipherBytes.Length);
                    cs.Close();
                }
                cipherText = Encoding.Unicode.GetString(ms.ToArray());
            }
        }

        return cipherText;
    }

    public static DataSet GetCurriculumCourse(int a)
    {
        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();
            SqlCommand cmd;

            //string query = @"pr_TranscomUniversity_lkp_GetScormPackage";
            string query = @"pr_transcomuniversity_GetAllResources";
            cmd = new SqlCommand(query, cn);
            cmd.Parameters.Add("@CourseId", a);
            //cmd.Parameters.AddWithValue("@CIM", w);
            //cmd.Parameters.AddWithValue("@CurriculumID", CourseID);

            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            da.Fill(ds);
        }

        return ds;
    }

    public static DataSet GetCurriculumCourse(string w, int CourseID)
    {
        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();
            SqlCommand cmd;

            string query = @"pr_TranscomUniversity_lkp_GetScormPackage";

            cmd = new SqlCommand(query, cn);
            //cmd.Parameters.Add("@CourseId", a);
            cmd.Parameters.AddWithValue("@CIM", w);
            cmd.Parameters.AddWithValue("@CurriculumID", CourseID);

            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            da.Fill(ds);
        }

        return ds;
    }

    public static DataSet GetCurriculumPackages(int w, int CourseID, int Bundle)
    {
        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();
            SqlCommand cmd;

            string query = @"pr_Scorm_Cor_GetCurriculumPackages";

            cmd = new SqlCommand(query, cn);
            //cmd.Parameters.Add("@CourseId", a);
            cmd.Parameters.AddWithValue("@CurriculumID", w);
            cmd.Parameters.AddWithValue("@CurriculumCourseID", CourseID);
            cmd.Parameters.AddWithValue("@isBundle", 0);

            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            da.Fill(ds);
        }

        return ds;
    }

    public static DataSet GetDripContent(int ContentID, int courseID)
    {
        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();
            SqlCommand cmd;
            string query = @"SELECT * FROM tbl_TranscomUniversity_Rlt_DripContent WHERE ContentID = " + ContentID + "and CourseID =" + courseID;
            cmd = new SqlCommand(query, cn);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
        }
        return ds;
    }


    public static DataSet GetDripContents(int a)
    {
        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();
            SqlCommand cmd;

            string query = @"pr_TranscomUniversity_GetDripContent";

            cmd = new SqlCommand(query, cn);
            //cmd.Parameters.Add("@CourseId", a);
            cmd.Parameters.AddWithValue("@CourseID", a);

            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            da.Fill(ds);
        }

        return ds;
    }

    public static void InsertDripContent(int a, int b, DateTime c, DateTime d, bool e)
    {
        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();
            SqlCommand cmd;
            string query = @"pr_TranscomUniversity_DripContentInsert";

            cmd = new SqlCommand(query, cn);
            //cmd.Parameters.Add("@CourseId", a);
            cmd.Parameters.AddWithValue("@ContentID", a);
            cmd.Parameters.AddWithValue("@CourseID", b);
            cmd.Parameters.AddWithValue("@PresetStartDate", c);
            cmd.Parameters.AddWithValue("@PresetEndDate", d);
            cmd.Parameters.AddWithValue("@Inactive", e);



            cmd.CommandType = CommandType.StoredProcedure;
            cmd.ExecuteNonQuery();
        }
    }

    public static DataSet GetNotification(int CourseID)
    {
        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();
            SqlCommand cmd;
            string query = @"SELECT * FROM
                                tbl_TranscomUniversity_Rlt_CourseSetting WHERE CourseID = " + CourseID;

            cmd = new SqlCommand(query, cn);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
        }

        return ds;
    }

    public static DataSet GetCourseSetting(int CourseID)
    {
        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();
            SqlCommand cmd;
            string query = @"SELECT * FROM
                                tbl_TranscomUniversity_Rlt_CourseSetting WHERE CourseID = " + CourseID;

            cmd = new SqlCommand(query, cn);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
        }

        return ds;
    }

    public static void UpdateCourseSetting(int a, bool b, bool c, bool d, bool e, bool f, bool g, bool h, bool i, bool j, bool k, bool l)
    {
        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();
            SqlCommand cmd;
            string query = @"pr_TranscomUniversity_UpdateSettings";

            cmd = new SqlCommand(query, cn);
            //cmd.Parameters.Add("@CourseId", a);
            cmd.Parameters.AddWithValue("@CourseID", a);
            cmd.Parameters.AddWithValue("@AllowLearnersToEnroll", b);
            cmd.Parameters.AddWithValue("@IsOpen", c);
            cmd.Parameters.AddWithValue("@NotifyTrainers", d);
            cmd.Parameters.AddWithValue("@NotifySupervisor", e);
            cmd.Parameters.AddWithValue("@NotifyManagers", f);
            cmd.Parameters.AddWithValue("@NotifyAdmin", g);
            cmd.Parameters.AddWithValue("@ApplyUnenrollments", h);
            cmd.Parameters.AddWithValue("@ForumWidget", i);
            cmd.Parameters.AddWithValue("@SimilarCourseWidget", j);
            cmd.Parameters.AddWithValue("@CourseViewWidget", k);
            cmd.Parameters.AddWithValue("@WithApproval", l);


            cmd.CommandType = CommandType.StoredProcedure;
            cmd.ExecuteNonQuery();
        }
    }

    public static DataSet GetCourseSettingNotif(int CourseID)
    {
        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();
            SqlCommand cmd;
            string query = @"SELECT * FROM tbl_TranscomUniversity_Rlt_CourseSettingNotif WHERE CourseID = " + CourseID;

            cmd = new SqlCommand(query, cn);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
        }

        return ds;
    }

    public static void InsertNotif(int a, bool b, bool c, bool d, bool e, bool f, bool g, bool h, bool i, bool b1, bool c1, bool d1, bool e1, bool f1, bool g1, bool h1, bool i1)
    {
        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();
            SqlCommand cmd;
            string query = @"pr_TranscomUniversity_CourseSettingNotifInsert";

            cmd = new SqlCommand(query, cn);
            //cmd.Parameters.Add("@CourseId", a);
            cmd.Parameters.AddWithValue("@CourseID", a);
            cmd.Parameters.AddWithValue("@AlertEnrollmentReq", b);
            cmd.Parameters.AddWithValue("@AlertTrainers", c);
            cmd.Parameters.AddWithValue("@AlertSupervisors", d);
            cmd.Parameters.AddWithValue("@AlertManagers", e);
            cmd.Parameters.AddWithValue("@AlertCourseAvailability", f);
            cmd.Parameters.AddWithValue("@AlertClassAssignments", g);
            cmd.Parameters.AddWithValue("@AlertOverdueCourses", h);
            cmd.Parameters.AddWithValue("@AlertForums", i);
            cmd.Parameters.AddWithValue("@EmailEnrollmentReq", b1);
            cmd.Parameters.AddWithValue("@EmailTrainers", c1);
            cmd.Parameters.AddWithValue("@EmailSupervisors", d1);
            cmd.Parameters.AddWithValue("@EmailManagers", e1);
            cmd.Parameters.AddWithValue("@EmailCourseAvailability", f1);
            cmd.Parameters.AddWithValue("@EmailClassAssignments", g1);
            cmd.Parameters.AddWithValue("@EmailOverdueCourses", h1);
            cmd.Parameters.AddWithValue("@EmailForums", i1);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.ExecuteNonQuery();
        }
    }

    public static void UpdateNotif(int a, bool b, bool c, bool d, bool e, bool f, bool g, bool h, bool i, bool b1, bool c1, bool d1, bool e1, bool f1, bool g1, bool h1, bool i1)
    {
        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();
            SqlCommand cmd;
            string query = @"pr_TranscomUniversity_CourseSettingNotifUpdate";

            cmd = new SqlCommand(query, cn);
            //cmd.Parameters.Add("@CourseId", a);
            cmd.Parameters.AddWithValue("@CourseID", a);
            cmd.Parameters.AddWithValue("@AlertEnrollmentReq", b);
            cmd.Parameters.AddWithValue("@AlertTrainers", c);
            cmd.Parameters.AddWithValue("@AlertSupervisors", d);
            cmd.Parameters.AddWithValue("@AlertManagers", e);
            cmd.Parameters.AddWithValue("@AlertCourseAvailability", f);
            cmd.Parameters.AddWithValue("@AlertClassAssignments", g);
            cmd.Parameters.AddWithValue("@AlertOverdueCourses", h);
            cmd.Parameters.AddWithValue("@AlertForums", i);
            cmd.Parameters.AddWithValue("@EmailEnrollmentReq", b1);
            cmd.Parameters.AddWithValue("@EmailTrainers", c1);
            cmd.Parameters.AddWithValue("@EmailSupervisors", d1);
            cmd.Parameters.AddWithValue("@EmailManagers", e1);
            cmd.Parameters.AddWithValue("@EmailCourseAvailability", f1);
            cmd.Parameters.AddWithValue("@EmailClassAssignments", g1);
            cmd.Parameters.AddWithValue("@EmailOverdueCourses", h1);
            cmd.Parameters.AddWithValue("@EmailForums", i1);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.ExecuteNonQuery();
        }
    }

    public static DataSet GetSizeUpload()
    {
        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();
            SqlCommand cmd;
            string query = @"SELECT * FROM tbl_TranscomUniversity_Lkp_UploadSize";

            cmd = new SqlCommand(query, cn);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
        }

        return ds;
    }

    public static DataSet GetSizeChar()
    {
        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();
            SqlCommand cmd;
            string query = @"SELECT * FROM tbl_TranscomUniversity_Lkp_CharSize";

            cmd = new SqlCommand(query, cn);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
        }

        return ds;
    }

    public static DataSet GetForumSetting(int CourseSettingID)
    {
        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();
            SqlCommand cmd;
            string query = @"SELECT * FROM
                                tbl_TranscomUniversity_Rlt_CourseSettingForumWidget WHERE CourseSettingID = " + CourseSettingID;

            cmd = new SqlCommand(query, cn);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
        }

        return ds;
    }

    public static void InsertForumSetting(int a, bool b, bool c, bool d, int? e, int? f, bool g, bool h)
    {
        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();
            SqlCommand cmd;
            string query = @"pr_TranscomUniversity_ForumSettingsInsert";

            cmd = new SqlCommand(query, cn);
            //cmd.Parameters.Add("@CourseId", a);
            cmd.Parameters.AddWithValue("@CourseSettingID", a);
            cmd.Parameters.AddWithValue("@UsersCanParticipate", b);
            cmd.Parameters.AddWithValue("@EnrolledUsersOnly", c);
            cmd.Parameters.AddWithValue("@AllowFileUpload", d);
            cmd.Parameters.AddWithValue("@MaxUploadSize", e);
            cmd.Parameters.AddWithValue("@MaxChars", f);
            cmd.Parameters.AddWithValue("@CanJoinMultipleForums", g);
            cmd.Parameters.AddWithValue("@LimitToClass", h);


            cmd.CommandType = CommandType.StoredProcedure;
            cmd.ExecuteNonQuery();
        }
    }

    public static void UpdateForumSetting(int a, bool b, bool c, bool d, int e, int f, bool g, bool h)
    {
        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();
            SqlCommand cmd;
            string query = @"pr_TranscomUniversity_ForumSettingsUpdate";

            cmd = new SqlCommand(query, cn);
            //cmd.Parameters.Add("@CourseId", a);
            cmd.Parameters.AddWithValue("@CourseSettingID", a);
            cmd.Parameters.AddWithValue("@UsersCanParticipate", b);
            cmd.Parameters.AddWithValue("@EnrolledUsersOnly", c);
            cmd.Parameters.AddWithValue("@AllowFileUpload", d);
            cmd.Parameters.AddWithValue("@MaxUploadSize", e);
            cmd.Parameters.AddWithValue("@MaxChars", f);
            cmd.Parameters.AddWithValue("@CanJoinMultipleForums", g);
            cmd.Parameters.AddWithValue("@LimitToClass", h);


            cmd.CommandType = CommandType.StoredProcedure;
            cmd.ExecuteNonQuery();
        }
    }


    public static DataSet GetSimilarCourseSetting(int CourseSettingID)
    {
        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();
            SqlCommand cmd;
            string query = @"SELECT * FROM
                                tbl_TranscomUniversity_Rlt_CourseSettingSimilarCourseWidget WHERE CourseSettingID = " + CourseSettingID;

            cmd = new SqlCommand(query, cn);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
        }

        return ds;
    }

    public static void InsertSimilarCourseSetting(int a, bool b, bool c, bool d, bool e, bool f, bool g, bool h, bool i, bool j)
    {
        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();
            SqlCommand cmd;
            string query = @"pr_TranscomUniversity_SimilarCourseSettingInsert";

            cmd = new SqlCommand(query, cn);
            //cmd.Parameters.Add("@CourseId", a);
            cmd.Parameters.AddWithValue("@CourseSettingID", a);
            cmd.Parameters.AddWithValue("@SameCategory", b);
            cmd.Parameters.AddWithValue("@SameSubCategory", c);
            cmd.Parameters.AddWithValue("@SameAuthor", d);
            cmd.Parameters.AddWithValue("@SameDepartment", e);
            cmd.Parameters.AddWithValue("@BundledCourse", f);
            cmd.Parameters.AddWithValue("@SameProgram", g);
            cmd.Parameters.AddWithValue("@SameCatalogueCategory", h);
            cmd.Parameters.AddWithValue("@SameCourseType", i);
            cmd.Parameters.AddWithValue("@SameRating", j);


            cmd.CommandType = CommandType.StoredProcedure;
            cmd.ExecuteNonQuery();
        }
    }

    public static void UpdateSimilarCourseSetting(int a, bool b, bool c, bool d, bool e, bool f, bool g, bool h, bool i, bool j)
    {
        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();
            SqlCommand cmd;
            string query = @"pr_TranscomUniversity_SimilarCourseSettingUpdate";

            cmd = new SqlCommand(query, cn);
            //cmd.Parameters.Add("@CourseId", a);
            cmd.Parameters.AddWithValue("@CourseSettingID", a);
            cmd.Parameters.AddWithValue("@SameCategory", b);
            cmd.Parameters.AddWithValue("@SameSubCategory", c);
            cmd.Parameters.AddWithValue("@SameAuthor", d);
            cmd.Parameters.AddWithValue("@SameDepartment", e);
            cmd.Parameters.AddWithValue("@BundledCourse", f);
            cmd.Parameters.AddWithValue("@SameProgram", g);
            cmd.Parameters.AddWithValue("@SameCatalogueCategory", h);
            cmd.Parameters.AddWithValue("@SameCourseType", i);
            cmd.Parameters.AddWithValue("@SameRating", j);


            cmd.CommandType = CommandType.StoredProcedure;
            cmd.ExecuteNonQuery();
        }
    }

    public static DataSet GetCourseViewSetting(int CourseSettingID)
    {
        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();
            SqlCommand cmd;
            string query = @"SELECT * FROM
                                tbl_TranscomUniversity_Rlt_CourseSettingCourseViewWidget WHERE CourseSettingID = " + CourseSettingID;

            cmd = new SqlCommand(query, cn);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
        }

        return ds;
    }

    public static void InsertCourseViewSetting(int a, bool b, bool c, bool d)
    {
        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();
            SqlCommand cmd;
            string query = @"pr_TranscomUniversity_CourseViewSettingInsert";

            cmd = new SqlCommand(query, cn);
            //cmd.Parameters.Add("@CourseId", a);
            cmd.Parameters.AddWithValue("@CourseSettingID", a);
            cmd.Parameters.AddWithValue("@ClickTitleOrImage", b);
            cmd.Parameters.AddWithValue("@CountOfUniqueIDVisits", c);
            cmd.Parameters.AddWithValue("@CountOfIDVisits", d);


            cmd.CommandType = CommandType.StoredProcedure;
            cmd.ExecuteNonQuery();
        }
    }

    public static void UpdateCourseViewSetting(int a, bool b, bool c, bool d)
    {
        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();
            SqlCommand cmd;
            string query = @"pr_TranscomUniversity_CourseViewSettingUpdate";

            cmd = new SqlCommand(query, cn);
            //cmd.Parameters.Add("@CourseId", a);
            cmd.Parameters.AddWithValue("@CourseSettingID", a);
            cmd.Parameters.AddWithValue("@ClickTitleOrImage", b);
            cmd.Parameters.AddWithValue("@CountOfUniqueIDVisits", c);
            cmd.Parameters.AddWithValue("@CountOfIDVisits", d);


            cmd.CommandType = CommandType.StoredProcedure;
            cmd.ExecuteNonQuery();
        }
    }

    public static DataSet GetFolderAndBadges(System.Nullable<int> a, int CourseID)
    {
        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();
            SqlCommand cmd;

            string query = @"pr_TranscomUniversity_FoldersAndBadges";

            cmd = new SqlCommand(query, cn);
            cmd.Parameters.Add("@FileType", a);
            cmd.Parameters.Add("@CourseID", CourseID);

            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            da.Fill(ds);
        }

        return ds;
    }

    public static DataSet GetAllBadges(int fileType, int CourseID)
    {
        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();
            SqlCommand cmd;
            string query = @"SELECT * FROM
                                tbl_TranscomUniversity_Lkp_Badge
                                where FileType = " + fileType + @" and courseID = " + CourseID+
                                @" and HideFromList = 0";

            cmd = new SqlCommand(query, cn);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
        }

        return ds;
    }      

    public static dynamic getAllGroup()
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var allgroup = db.vw_TranscomUniversity_DistinctGroups.ToList();

            if (allgroup != null)
                return allgroup;
        }

        return null;

    }

    public static dynamic getCourseGroup(int courseid)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var groups = db.vw_TranscomUniversity_Groups.Where(g => g.CourseID == courseid).ToList();

            if (groups != null)
                return groups;
        }

        return null;
    }

    public static dynamic getGroupDetail(int _groupid)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var groupsDetails = db.tbl_TranscomUniversity_Rlt_CourseGroupName.Where(g => g.GroupID == _groupid)
                .Select(gd => new
                {
                    GroupID = gd.GroupID,
                    GroupName = gd.GroupName,
                    Account = gd.Account,
                    Site = gd.Site
                }).ToList();
            if (groupsDetails != null)
                return groupsDetails;
        }

        return null;
    }

    public static bool removeGroup(int GroupID, int courseID)
    {
        bool removed = false;
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            db.pr_TranscomUniversity_RemoveGroup(GroupID, courseID);
            removed = true;
        }
        return removed;
    }

    public static dynamic getGroupMembers(int groupID)
    {

        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var gm = db.vw_TranscomUniversity_GroupMembers.Where(g => g.GroupID == groupID).ToList();
            if (gm != null)
                return gm;
        }
        return null;
    }

    public static dynamic getCourseLogHistory(int courseid)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var groups = db.vw_TranscomUniversity_CourseLog.Where(g => g.CourseID == courseid).OrderByDescending(g => g.LogDate).ToArray()
                .Select(g => new
                {
                    LogDate = g.LogDate,
                    UserID = g.UserID,
                    UserRole = g.Role,
                    EmailAddress = g.EmailAddress,
                    UserIpAddress = g.Ip,
                    Action = g.Action,
                    Duration = (TimeSpan.FromMilliseconds((Double)g.Duration)).ToString(@"hh\:mm\:ss\.fff")
                });
            if (groups != null)
                return groups;
        }
        return null;
    }

    public static int getLastLogID()
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            int lastlogid = 0;
            try
            {
                lastlogid = db.tbl_TranscomUniversity_Rlt_CourseLog.Max(l => l.LogID);
                if (lastlogid >= 0)
                    return lastlogid;
            }
            catch
            {
                return lastlogid;
            }

        }
        return 0;
    }

    public static string GetIPAddress()
    {
        System.Web.HttpContext context = System.Web.HttpContext.Current;
        string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (!string.IsNullOrEmpty(ipAddress))
        {
            string[] addresses = ipAddress.Split(',');
            if (addresses.Length != 0)
            {
                return addresses[0];
            }
        }

        return context.Request.ServerVariables["REMOTE_ADDR"];
    }

    public static void logCourse(int CourseID, int ActionID, int Duration, string UserID, string ActionDescription, string IpAddress)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            db.pr_TranscomUniversity_CourseLog(CourseID, ActionID, Duration, UserID, ActionDescription, IpAddress);
            db.SaveChanges();
        }

    }

    public static string getGroupName(int id)
    {

        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var name = db.vw_TranscomUniversity_DistinctGroups.Where(g => g.GroupID == id).Select(g => g.GroupName).First();

            if (name != null)
                return name;
        }
        return null;
    }

    public static string getCourseName(int id)
    {

        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var name = db.tbl_TranscomUniversity_Cor_Course.Where(g => g.CourseID == id).Select(g => g.Title).First();

            if (name != null)
                return name;
        }
        return null;
    }

    public static string getUserName(int id)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            try
            {
                var name = db.pr_TranscomUniversity_EmployeeName(id);
                string fname = "";
                foreach (var n in name)
                {
                    fname = n.Name;
                }

                if (fname != "")
                    return fname;
            }
            catch
            {
                return "(No name )" + id.ToString();
            }

        }
        return "(No name )" + id.ToString();
    }

    public DataSet GetCurriculumPackages(int CurriculumID, int CurriculumCourseID)
    {
        var ds = new DataSet();
        DbCommand com = _db.GetStoredProcCommand("pr_Scorm_Cor_GetCurriculumPackages");
        _db.AddInParameter(com, "CurriculumID", DbType.Int32, CurriculumID);
        _db.AddInParameter(com, "CurriculumCourseID", DbType.Int32, CurriculumCourseID);
        try
        {
            _db.LoadDataSet(com, ds, "GetCurriculumPackages");
        }
        catch (Exception ex)
        {
            ProcessHandledException("GetCurriculumPackages", ex);
            ds = null;
        }
        finally
        {
            com.Connection.Close();
        }

        return ds;
    }

    private void ProcessHandledException(string sender, Exception ex)
    {
        if (!sender.Equals("ProcessHandledException"))
        {
            Database db = DatabaseFactory.CreateDatabase("DefaultConnection");
            DbCommand com = db.GetStoredProcCommand("dbo.pr_TranscomUniversity_Sav_Exception");

            db.AddInParameter(com, "Message", DbType.String, ex.Message);
            db.AddInParameter(com, "Source", DbType.String, ex.Source);
            db.AddInParameter(com, "Object", DbType.String, sender);

            try
            {
                int i = db.ExecuteNonQuery(com);
            }
            catch (Exception procEx)
            {
                ProcessHandledException("ProcessHandledException", procEx);
            }
            finally
            {
                com.Connection.Close();
            }
        }
    }

    public static dynamic GetCourseCategory()
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var courseCategory = (from c in db.vw_TranscomUniversity_Category
                                  select c).ToList();

            if (courseCategory != null)
                return courseCategory;
        }
        return null;
    }

    public static dynamic GetCourseSubCategory(int categoryId)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var courseSubCategory = (from s in db.vw_TranscomUniversity_Subcategory
                                     where s.CategoryID == categoryId
                                     select s).ToList();

            if (courseSubCategory != null)
                return courseSubCategory;
        }
        return null;
    }

    public static dynamic GetSubCatCourses(int subcategoryId)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var subCatCourses = (from s in db.vw_TranscomUniversity_SubCatCourses
                                 where s.SubcategoryID == subcategoryId
                                 select s).ToList();

            if (subCatCourses != null)
                return subCatCourses;
        }
        return null;
    }

    public static bool isEnrolled(int cim, int courseid)
    {

        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            try
            {
                var enrolleeCim = db.vw_TranscomUniversity_ApprovedEnrollments.Where(m => m.EnrolleeCIM == cim && m.CourseID == courseid).Select(m => m.EnrolleeCIM).SingleOrDefault();
                if (enrolleeCim != 0)
                    return true;
                else
                    return false;

            }
            catch
            {

            }
        }

        return false;

    }

    public static int EnrollmentStatus(int cim, int courseID)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            try
            {

                var pending = db.tbl_TranscomUniversity_Cor_CourseEnrollment
                    .Where(u => u.EnrolleeCIM == cim && u.CourseID == courseID && u.HideFromList == false)
                    .Select(u => u.EnrollmentStatus).SingleOrDefault();

                if (pending >= 1)
                    return pending;
                else
                {
                    var approved = db.vw_TranscomUniversity_ApprovedEnrollments
                        .Where(u => u.EnrolleeCIM == cim && u.CourseID == courseID)
                        .Select(u => u.EnrolleeCIM).SingleOrDefault();
                    if (approved > 0)
                        return 2;
                }

            }
            catch
            {

            }
        }

        return -1;

    }

    public static int getUserCourseRating(int cim, int courseID)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            try
            {
                var pending = db.vw_TranscomUniversity_PendingEnrollments
                    .Where(u => u.EnrolleeCIM == cim && u.CourseID == courseID)
                    .Select(u => u.EnrolleeCIM).SingleOrDefault();
                if (pending != 0)
                {
                    return 1;
                }
                else if (pending == 0)
                {
                    var approved = db.vw_TranscomUniversity_ApprovedEnrollments
                        .Where(u => u.EnrolleeCIM == cim && u.CourseID == courseID)
                        .Select(u => u.EnrolleeCIM).SingleOrDefault();
                    return 2;
                }

            }
            catch
            {

            }
        }

        return -1;
    }

    public static dynamic courseComments(int courseID)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            try
            {
                var comments = db.vw_TranscomUniversity_CourseComments
                    .Where(c => c.CourseID == courseID)
                    .ToList();
                return comments;

            }
            catch
            {

            }
        }

        return null;
    }

    public static dynamic courseReviewStats(int courseID)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            try
            {
                var comments = db.vw_TranscomUniversity_CourseReviewDetails
                    .Where(c => c.CourseID == courseID)
                    .SingleOrDefault();
                return comments;

            }
            catch
            {

            }
        }

        return null;
    }

    public static List<vw_TranscomUniversity_TakeLateCoursesFolders> getTakeLaterCourses(int cim)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {

            var folders = db.vw_TranscomUniversity_TakeLateCoursesFolders
                .Where(c => c.EnrolleeCIM == cim)
                 .OrderByDescending(c => c.FileType)
                 .ToList();

            if (folders != null)
                return folders;
        }

        return null;
    }

    public static dynamic courseForumTopics(int courseID)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {

            var topics = db.vw_TranscomUniversity_ForumTopics
                .Where(c => c.CourseID == courseID).OrderByDescending(c => c.DateCreated)
                .ToList();
            if (topics != null)
                return topics;


            return null;
        }
    }

    public static bool isThreadMember(int usercim, int courseid)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {

            var member = db.vw_TranscomUniversity_ThreadMembers
                .Where(c => c.CourseId == courseid && c.UserCIM == usercim)
                .SingleOrDefault();
            if (member != null)
                return true;

            return false;
        }
    }

    public static dynamic courseThreadComments(int threadID, int userCIM)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var posts = db.pr_TranscomUniversity_ThreadPost(threadID, userCIM);
            db.SaveChanges();
            if (posts != null)
                return posts;

            return null;
        }
    }

    public static DataTable threadPosts(int threadID, int userCIM)
    {
        DataTable posts = new DataTable();
        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {

            cn.Open();

            // Define the command 
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = cn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "pr_TranscomUniversity_ThreadPost";
                cmd.Parameters.AddWithValue("@ThreadId", threadID);
                cmd.Parameters.AddWithValue("@UserCIM", userCIM);

                // Define the data adapter and fill the dataset 
                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    da.Fill(posts);
                }
            }
        }

        return posts;
    }

    public static bool isCourseValid(int courseID, int timezoneOffset)
    {
        var db = new TranscomUniversityV3ModelContainer();


        DateTime localizedTimeNow = (DateTime)serverDate() - new TimeSpan(timezoneOffset / 60, timezoneOffset % 60, 0);
        var course = db.tbl_TranscomUniversity_Cor_Course
                      .Where(u => u.CourseID == courseID && u.HideFromList == false)
                      .SingleOrDefault();
        DateTime localizedStartDate = (DateTime)course.StartDate;
        //localizedStartDate.AddHours(-(-7 + (timezoneOffset / 60)));
        //localizedStartDate.AddMinutes(-((timezoneOffset % 60)));

        DateTime localizedEndDate = (DateTime)course.Enddate;
        //localizedEndDate.AddHours(-(-7 + (timezoneOffset / 60)));
        //localizedEndDate.AddMinutes(-((timezoneOffset % 60)));

        //DateTime localizedEndDate = (DateTime)course.Enddate - new TimeSpan(timezoneOffset / 60, timezoneOffset % 60, 0);
        //var valid = db.tbl_TranscomUniversity_Cor_Course
        //                .Where(u => u.CourseID == courseID && u.HideFromList == false && localizedTime <= u.Enddate && localizedTime >= u.StartDate)
        //                .Select(u => u.CourseID)
        //                .SingleOrDefault();
        if (localizedTimeNow >= localizedStartDate && localizedTimeNow <= localizedEndDate)
            return true;
        else
            return false;
    }

    public static DateTime serverDate()
    {
        string constr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        string getQuery = "SELECT GETUTCDATE();";
        DateTime serverDate;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand(getQuery))
            {
                cmd.Connection = con;
                con.Open();
                serverDate = (DateTime)cmd.ExecuteScalar();
                con.Close();
            }
        }

        return serverDate;
    }

    public static dynamic scoPackage(string email, int courseID, int currID)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var package = db.pr_TranscomUniversity_lkp_PackageDetails(email, currID, courseID).ToList();
            db.SaveChanges();
            if (package != null)
                return package;

            return null;
        }
    }

    //public static dynamic GetClassNewHire()
    //{
    //    using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
    //    {
    //        var classNewHire = (from c in db.vw_TranscomUniversity_ClassNewHire
    //                            select c).ToList();

    //        if (classNewHire != null)
    //            return classNewHire;
    //    }
    //    return null;
    //}

    public static dynamic GetClassCrossTraining()
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var classCrossTraining = (from c in db.vw_TranscomUniversity_ClassCrossTraining
                                      select c).ToList();

            if (classCrossTraining != null)
                return classCrossTraining;
        }
        return null;
    }

    public static dynamic GetClassRefresherTraining()
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var classRefresherTraining = (from c in db.vw_TranscomUniversity_ClassRefresherTraining
                                          select c).ToList();

            if (classRefresherTraining != null)
                return classRefresherTraining;
        }
        return null;
    }

    public static dynamic GetClassUpTraining()
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var classUpTraining = (from c in db.vw_TranscomUniversity_ClassUpTraining
                                   select c).ToList();

            if (classUpTraining != null)
                return classUpTraining;
        }
        return null;
    }

    public static dynamic GetClassProfessionalDevelopment()
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var classProfessionalDevelopment = (from c in db.vw_TranscomUniversity_ClassProfDevelopment
                                                select c).ToList();

            if (classProfessionalDevelopment != null)
                return classProfessionalDevelopment;
        }
        return null;
    }

    //public static dynamic GetClassNHNotYetStarted()
    //{
    //    using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
    //    {
    //        var classNewHire = (from c in db.vw_TranscomUniversity_ClassNewHire
    //                            where c.Status == "Not Yet Started"
    //                            select c).ToList();

    //        if (classNewHire != null)
    //            return classNewHire;
    //    }
    //    return null;
    //}

    public static dynamic GetClassCTNotYetStarted()
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var classCrossTraining = (from c in db.vw_TranscomUniversity_ClassCrossTraining
                                      where c.Status == "Not Yet Started"
                                      select c).ToList();

            if (classCrossTraining != null)
                return classCrossTraining;
        }
        return null;
    }

    public static dynamic GetClassRTNotYetStarted()
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var classRefresherTraining = (from c in db.vw_TranscomUniversity_ClassRefresherTraining
                                          where c.Status == "Not Yet Started"
                                          select c).ToList();

            if (classRefresherTraining != null)
                return classRefresherTraining;
        }
        return null;
    }

    public static dynamic GetClassUTNotYetStarted()
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var classUpTraining = (from c in db.vw_TranscomUniversity_ClassUpTraining
                                   where c.Status == "Not Yet Started"
                                   select c).ToList();

            if (classUpTraining != null)
                return classUpTraining;
        }
        return null;
    }

    public static dynamic GetClassPDNotYetStarted()
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var classProfessionalDevelopment = (from c in db.vw_TranscomUniversity_ClassProfDevelopment
                                                where c.Status == "Not Yet Started"
                                                select c).ToList();

            if (classProfessionalDevelopment != null)
                return classProfessionalDevelopment;
        }
        return null;
    }

    //public static dynamic GetClassNHInProgress()
    //{
    //    using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
    //    {
    //        var classNewHire = (from c in db.vw_TranscomUniversity_ClassNewHire
    //                            where c.Status == "In-Progress"
    //                            select c).ToList();

    //        if (classNewHire != null)
    //            return classNewHire;
    //    }
    //    return null;
    //}

    public static dynamic GetClassCTInProgress()
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var classCrossTraining = (from c in db.vw_TranscomUniversity_ClassCrossTraining
                                      where c.Status == "In-Progress"
                                      select c).ToList();

            if (classCrossTraining != null)
                return classCrossTraining;
        }
        return null;
    }

    public static dynamic GetClassRTInProgress()
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var classRefresherTraining = (from c in db.vw_TranscomUniversity_ClassRefresherTraining
                                          where c.Status == "In-Progress"
                                          select c).ToList();

            if (classRefresherTraining != null)
                return classRefresherTraining;
        }
        return null;
    }

    public static dynamic GetClassUTInProgress()
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var classUpTraining = (from c in db.vw_TranscomUniversity_ClassUpTraining
                                   where c.Status == "In-Progress"
                                   select c).ToList();

            if (classUpTraining != null)
                return classUpTraining;
        }
        return null;
    }

    public static dynamic GetClassPDInProgress()
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var classProfessionalDevelopment = (from c in db.vw_TranscomUniversity_ClassProfDevelopment
                                                where c.Status == "In-Progress"
                                                select c).ToList();

            if (classProfessionalDevelopment != null)
                return classProfessionalDevelopment;
        }
        return null;
    }

    public static void courseLogin(int courseID)
    {
        int enrolleeCIM = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
        var db = new TranscomUniversityV3ModelContainer();
        db.pr_TranscomUniversity_CourseLogin(enrolleeCIM, courseID);
    }

    public static string getCourseTitleByID(int courseID)
    {
        string courseTitle;
        var db = new TranscomUniversityV3ModelContainer();
        courseTitle = db.tbl_TranscomUniversity_Cor_Course.Where(c => c.CourseID == courseID).Select(c => c.Title).SingleOrDefault();


        return courseTitle;
    }

    public static string getThreadTitleByID(int threadID)
    {
        string threadTitle;
        var db = new TranscomUniversityV3ModelContainer();
        threadTitle = db.tbl_TranscomUniversity_Cor_Forum.Where(c => c.ThreadId == threadID).Select(c => c.ThreadTitle).SingleOrDefault();


        return threadTitle;
    }

    public static string getCurrentUserEmail()
    {
        return HttpContext.Current.User.Identity.Name.Split('|')[0];
    }

    public static bool isOverDue(int courseid, int timezoneOffset)
    {
        var db = new TranscomUniversityV3ModelContainer();
        bool isDue = false;
        var endDate = db.tbl_TranscomUniversity_Cor_Course
                    .Where(c => c.CourseID == courseid)
                    .Select(c => c.Enddate)
                    .SingleOrDefault();

        DateTime localizedDateNow = (DateTime)serverDate() - new TimeSpan(timezoneOffset / 60, timezoneOffset % 60, 0);
        //DateTime localizedEndDate = (DateTime)endDate.Value - new TimeSpan(timezoneOffset / 60, timezoneOffset % 60, 0);

        isDue = localizedDateNow <= endDate.Value;

        return isDue;
    }

    public static bool? isCourseOpen(int courseid)
    {
        var db = new TranscomUniversityV3ModelContainer();

        bool? isOpen = db.tbl_TranscomUniversity_Rlt_CourseSetting
                    .Where(c => c.CourseID == courseid)
                    .Select(c => c.IsOpen)
                    .FirstOrDefault();
        return isOpen;
    }

    public static bool? isEnrollmentAllowed(int courseid)
    {
        var db = new TranscomUniversityV3ModelContainer();

        bool? isAllowed = db.tbl_TranscomUniversity_Rlt_CourseSetting
                    .Where(c => c.CourseID == courseid)
                    .Select(c => c.AllowLearnersToEnroll)
                    .FirstOrDefault();

        return isAllowed;
    }

    public static bool hasCourseStarted(int courseid)
    {
        var db = new TranscomUniversityV3ModelContainer();
        bool hasStarted = false;
        var startDate = db.tbl_TranscomUniversity_Cor_Course
                     .Where(c => c.CourseID == courseid)
                     .Select(c => c.StartDate)
                     .SingleOrDefault();
        hasStarted = startDate < serverDate();

        return hasStarted;
    }

    public static dynamic GetCourseType()
    {
        var db = new TranscomUniversityV3ModelContainer();

        var courseTypes = db.tbl_Scorm_Lkp_CourseType
                            .Where(t => t.Inactive == 0)
                            .ToList();

        if (courseTypes != null)
            return courseTypes;

        return null;
    }

    public static string getCurrentUserImg()
    {
        var db = new TranscomUniversityV3ModelContainer();
        string email = getCurrentUserEmail();
        return db.Users.Where(u => u.UserName == email).Select(u => u.UserImageUrl).SingleOrDefault();
    }

    public static bool isLoggedIn()
    {
        try
        {
            return GetCurrentUserCIM() == "" ? false : true;
        }
        catch (Exception)
        {
            return false;
            throw;
        }
       
    }

    public static dynamic getMandatoryCourseSettings()
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var settings = db.tbl_TranscomUniversity_Rlt_MandatoryCourseSettings
                                .Where(c => c.CatalogSettingID == 1)
                                .SingleOrDefault();

            if (settings != null)
                return settings;

            return null;
        }
    }

    public static dynamic getTrendingCourseSettings()
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var settings = db.tbl_TranscomUniversity_Rlt_TrendingCourseSettings
                                .Where(c => c.CatalogSettingID == 1)
                                .SingleOrDefault();

            if (settings != null)
                return settings;

            return null;
        }
    }

    public static void updateMandatorySettings(bool defaultView, int displayCount, bool isManual, bool isVisible)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            db.pr_TranscomUniversity_UpdateMandatoryCourseSettings(defaultView, displayCount, isManual, isVisible);
        }
    }

    public static void updateTrendingSettings(bool IsGridView, int DisplayCount, bool ManualSelection, bool EnableMostView, int MostViewCount, bool EnableMostRating, int MostRatingCount, bool EnableRatingWithin, int RatingWithin, bool Completed, bool EnableMostEnrollee, int MostEnrolleeCount, bool EnableAddedLast, int LastAddInMillisecond, int LastAddUnit, bool EnableLastModified, int LastModifiedInMillisecond, int LastModifiedUnit, bool Visible)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            db.pr_TranscomUniversity_UpdateTrendingCourseSettings(IsGridView, DisplayCount, ManualSelection, EnableMostView, MostViewCount, EnableMostRating, MostRatingCount, EnableRatingWithin, RatingWithin, Completed, EnableMostEnrollee, MostEnrolleeCount, EnableAddedLast, LastAddInMillisecond, LastAddUnit, EnableLastModified, LastModifiedInMillisecond, LastModifiedUnit, Visible);
        }
    }

    public static dynamic getRecommendedCourseSettings()
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var settings = db.tbl_TranscomUniversity_Rlt_RecommendedCourseSettings
                                .Where(c => c.CatalogSettingID == 1)
                                .SingleOrDefault();

            if (settings != null)
                return settings;

            return null;
        }
    }

    public static void updateRecommendedCourseSettings(bool IsAssigned, bool IsSimilar, bool IsRequiredTenure, bool IsRequiredRole, bool IsBasedOnLearner, bool IsViewedEnable, int ViewCount, bool IsGridView, int DisplayCount, bool IsManual, bool IsNotifyAlert, bool IsNotifyEmail, bool IsVisible)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            db.pr_TranscomUniversity_UpdateRecommendedCourseSettings(IsAssigned, IsSimilar, IsRequiredTenure, IsRequiredRole, IsBasedOnLearner, IsViewedEnable, ViewCount, IsGridView, DisplayCount, IsManual, IsNotifyAlert, IsNotifyEmail, IsVisible);
        }
    }

    public static int selfEnrollment(int enrolleeCIM, int courseID)
    {
        try
        {
            int delegator = enrolleeCIM;
            TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();

            string _userIP = DataHelper.GetIPAddress();
            string _userID = DataHelper.GetCurrentUserCIM();
            double _duration = 0;

            DateTime startTime;
            DateTime endTime;

            var dts = db.CreateQuery<DateTime>("CurrentDateTime() ");
            startTime = dts.AsEnumerable().First();

            //if (isEnrollmentAllowed(courseID) == true && isCourseOpen(courseID) == true && (!isEnrolled(enrolleeCIM, courseID)))
            //if (isEnrollmentAllowed(courseID) == true && isCourseOpen(courseID) == true)
            if (isCourseOpen(courseID) == true)
            {

                db.pr_TranscomUniversity_InsertSelfEnrollUser(courseID, enrolleeCIM, 2, enrolleeCIM);

                string _action = DataHelper.getUserName(enrolleeCIM) + " Enrolled.";
                var dte = db.CreateQuery<DateTime>("CurrentDateTime() ");

                endTime = dte.AsEnumerable().First();
                _duration = (endTime - startTime).TotalMilliseconds;

                int l_actionID = DataHelper.getLastLogID() + 1;
                DataHelper.logCourse(courseID, l_actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);

                return 1;//success
            }
            else
            {
                return 2;//enrollment not allowed
            }


        }
        catch (Exception e)
        {
            return 3;//error
        }
    }

    public static dynamic getMustTakeCourseSettings()
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var settings = db.tbl_TranscomUniversity_Rlt_MustTakeCourseSettings
                                .Where(c => c.CatalogSettingID == 1)
                                .SingleOrDefault();

            if (settings != null)
                return settings;

            return null;
        }
    }

    public static void updateMustTakeCourseSettings(bool IsOverdue, bool IsIncomplete, bool IsGridView, int DisplayCount, bool IsManual, bool IsNotifyAlert, bool IsNotifyEmail, bool IsVisible)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            db.pr_TranscomUniversity_UpdateMustTakeCourseSettings(IsOverdue, IsIncomplete, IsGridView, DisplayCount, IsManual, IsNotifyAlert, IsNotifyEmail, IsVisible);
        }
    }

    public static dynamic getRecentlyAddedCourseSettings()
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var settings = db.tbl_TranscomUniversity_Rlt_RecentlyAddedCourseSettings
                                .Where(c => c.CatalogSettingID == 1)
                                .SingleOrDefault();

            if (settings != null)
                return settings;

            return null;
        }
    }

    public static void updateRecentlyAddedCourseSettings(bool EnableCreatedWithin, int CreatedWithin, int CreatedWithinUnit, bool EnableUpdatedWithin, int UpdatedWithin, int UpdatedWithinUnit, bool IsGridView, int DisplayCount, bool IsManual, bool IsNotifyAlert, bool IsNotifyEmail, bool IsVisible)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            db.pr_TranscomUniversity_UpdateRecentlyAddedCourseSettings(EnableCreatedWithin, CreatedWithin, CreatedWithinUnit, EnableUpdatedWithin, UpdatedWithin, UpdatedWithinUnit, IsGridView, DisplayCount, IsManual, IsNotifyAlert, IsNotifyEmail, IsVisible);
        }
    }

    public static bool? isMandatoryVisible()
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var settings = db.tbl_TranscomUniversity_Rlt_MandatoryCourseSettings
                                .Where(c => c.CatalogSettingID == 1)
                                .Select(c => c.Visible)
                                .SingleOrDefault();

            if (settings != null)
                return settings;

            return false;
        }
    }

    public static bool? isTrendingVisible()
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var settings = db.tbl_TranscomUniversity_Rlt_TrendingCourseSettings
                                .Where(c => c.CatalogSettingID == 1)
                                .Select(c => c.Visible)
                                .SingleOrDefault();

            if (settings != null)
                return settings;

            return false;
        }
    }

    public static bool? isRecentlyAddedVisible()
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var settings = db.tbl_TranscomUniversity_Rlt_RecentlyAddedCourseSettings
                                .Where(c => c.CatalogSettingID == 1)
                                .Select(c => c.Visible)
                                .SingleOrDefault();

            if (settings != null)
                return settings;

            return false;
        }
    }

    public static bool? isRecommendedAddedVisible()
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var settings = db.tbl_TranscomUniversity_Rlt_RecommendedCourseSettings
                                .Where(c => c.CatalogSettingID == 1)
                                .Select(c => c.Visible)
                                .SingleOrDefault();

            if (settings != null)
                return settings;

            return false;
        }
    }

    public static bool? isMustTakeVisible()
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var settings = db.tbl_TranscomUniversity_Rlt_MustTakeCourseSettings
                                .Where(c => c.CatalogSettingID == 1)
                                .Select(c => c.Visible)
                                .SingleOrDefault();

            if (settings != null)
                return settings;

            return false;
        }
    }

    public static bool? isCourseEnrollmentWithApproval(int courseid)
    {
        var db = new TranscomUniversityV3ModelContainer();

        bool? withApproval = db.tbl_TranscomUniversity_Rlt_CourseSetting
                    .Where(c => c.CourseID == courseid)
                    .Select(c => c.WithApproval)
                    .FirstOrDefault();

        return withApproval;
    }

    public static dynamic getPlayListDetail(int playlistID)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var settings = db.tbl_trancomuniversity_Playlist
                                .Where(c => c.ID == playlistID)
                                .SingleOrDefault();
            if (settings != null)
                return settings;

            return null;
        }
    }

    public static dynamic getCurrentPlaylist()
    {
        try
        {
            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {


                tbl_TranscomUniversity_Rlt_SliderSettings currentPlaylistSettings = db.tbl_TranscomUniversity_Rlt_SliderSettings
                    //.Select(p => new { p.SelectedPlaylistID, p.MaxItemCount })
                                                                                .FirstOrDefault();

                var currentPlaylist = db.tbl_trancomuniversity_PlaylistSlides
                            .Where(p => p.PlaylistID == currentPlaylistSettings.SelectedPlaylistID)
                            .Take(currentPlaylistSettings.MaxItemCount)
                            .ToList();

                return currentPlaylist;
            }
        }
        catch (Exception)
        {
            return null;
        }
    }

    public static dynamic getSliderSettings()
    {

        TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();

        var settings = db.tbl_TranscomUniversity_Rlt_SliderSettings.FirstOrDefault();

        return settings;

    }

    //featured

    public static dynamic GetFeaturedCourses()
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            try
            {
                var FeaturedCourses = db.pr_TranscomUniversity_FeaturedCourse()
                                   .AsEnumerable()
                                   .Select(m => new
                                   {
                                       m.CourseID,
                                       EncCourseID = Utils.Encrypt(m.CourseID),
                                       m.CourseTitle,
                                       m.CourseDescription,
                                       m.CourseImage,
                                       m.OrderNumber
                                   })
                                   .Take(3)
                                   .ToArray();

                if (FeaturedCourses != null)
                    return FeaturedCourses;
            }
            catch (Exception)
            {
                return null;
                throw;
            }
           
        }
        return null;
    }

    public static dynamic getFeaturedCourseSettings()
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var settings = db.tbl_TranscomUniversity_Rlt_FeaturedCourseSettings
                                .Where(c => c.CatalogSettingID == 1)
                                .SingleOrDefault();

            if (settings != null)
                return settings;

            return null;
        }
    }

    public static void updateFeaturedSettings(bool IsGridView, int DisplayCount, bool ManualSelection, bool EnableMostView, int MostViewCount, bool EnableMostRating, int MostRatingCount, bool EnableRatingWithin, int RatingWithin, bool Completed, bool EnableMostEnrollee, int MostEnrolleeCount, bool EnableAddedLast, int LastAddInMillisecond, int LastAddUnit, bool EnableLastModified, int LastModifiedInMillisecond, int LastModifiedUnit, bool Visible)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            db.pr_TranscomUniversity_UpdateFeaturedCourseSettings(IsGridView, DisplayCount, ManualSelection, EnableMostView, MostViewCount, EnableMostRating, MostRatingCount, EnableRatingWithin, RatingWithin, Completed, EnableMostEnrollee, MostEnrolleeCount, EnableAddedLast, LastAddInMillisecond, LastAddUnit, EnableLastModified, LastModifiedInMillisecond, LastModifiedUnit, Visible);
        }
    }

    public static bool? isFeaturedVisible()
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var settings = db.tbl_TranscomUniversity_Rlt_FeaturedCourseSettings
                                .Where(c => c.CatalogSettingID == 1)
                                .Select(c => c.Visible)
                                .SingleOrDefault();

            if (settings != null)
                return settings;

            return false;
        }
    }

    //testimonial

    public static dynamic getTestimonialSettings()
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var settings = db.tbl_transcomuniversity_Rlt_TestimonialSettings
                                .Where(c => c.ID == 1)
                                .SingleOrDefault();

            if (settings != null)
                return settings;

            return null;
        }
    }

    public static dynamic getTestimonialPlaylistDetail(int ID)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var settings = db.tbl_transcomuniversity_Lkp_Testimonial
                                .Where(c => c.TestimonialID == ID)
                                .SingleOrDefault();

            if (settings != null)
                return settings;

            return null;
        }
    }

    public static dynamic getAllComments()
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            try
            {
                var comments = db.vw_TranscomUniversity_CourseComments
                    .ToList();
                return comments;

            }
            catch
            {

            }
        }

        return null;
    }

    public static dynamic getRecommendedCourseList()
    {

        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var courses = db.pr_TranscomUniversity_RecommendedCourse(Convert.ToInt32(GetCurrentUserCIM()))
               .AsEnumerable()
               .Select(m => new
               {
                   m.CourseID,
                   EncryptedCourseID = Utils.Encrypt(m.CourseID),
                   m.CourseTitle,
                   m.CourseDescription,
                   m.CourseImage,
                   m.OrderNumber
               })
               .ToArray();

            return courses;
        }
    }

    public static int GetCimByEmail(string email)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var transcomEmail = email;

            var retVal = Convert.ToInt32((from u in db.vw_TranscomUniversity_UsersInfo
                                          where u.TranscomEmail == transcomEmail
                                          select u.CIMNumber).FirstOrDefault().ToString());


            return retVal;
        }

    }

    //training type

    public static dynamic getCourseTrainingType()
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var d = db.tbl_transcomuniversity_lkp_course_ilttype
                .Where(t => t.ID > 1)
                .ToList();

            return d;
        }
    }

    public static dynamic GetAllResources(int courseID)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var d = db.pr_transcomuniversity_GetAllResources(courseID)
                        .ToList();
            if (d.Count != null)
                return d;
            else
                return null;
        }
    }


    //Assessment Builder
    public static dynamic GetTestCategoryID(int courseID)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var d = db.tbl_testing_testCategory.Where(t => t.CourseID == courseID)
                        .Select(t => new
                        {
                            t.TestCategoryID
                        })
                        .SingleOrDefault();
            if (d == null)
                return 0;
            else
                return d.TestCategoryID;
        }
    }

    public static dynamic allExamsByCourse(int courseID)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var d = db.tbl_testing_testCategory
                        //.Where(t => t.CourseID == courseID)
                        .Where(t => t.CourseID == courseID && t.HideFromList == false)
                        .Select(t => new
                        {
                            t.TestCategoryID,
                            ExamName = t.TestName,
                            EndDate = t.EndDate,
                            TimeLimit = t.TimeLimit / 60,
                            TestLimit = t.TestLimit
                        })
                        .ToList();
            return d;
        }
    }

    public static dynamic GetTestCount(int courseID)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var d = db.tbl_testing_testCategory
                        .Where(t => t.CourseID == courseID)
                        .Count();
            if (d == null)
                return 0;
            else
                return d;
        }
    }

    //GradeBook

    public static dynamic GetGradeBookDetails(int courseID)
    {

        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();
            SqlCommand cmd;
            string query = @"pr_transcomuniversity_getGradeBook";
            cmd = new SqlCommand(query, cn);
            cmd.Parameters.Add("@CourseId", courseID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            da.Fill(ds);
        }
        return ds;
    }

    //Badges and Certificates
    public static bool insertBadgeFolder(int CourseID, string FolderName, int CimNo, int FileType)
    {
        try
        {
            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;

                string query = @"pr_TranscomUniversity_AddBadgeFolder";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@CourseID", CourseID);
                cmd.Parameters.Add("@folderName", FolderName);
                cmd.Parameters.Add("@createdBy", CimNo);
                cmd.Parameters.Add("@FileType", FileType);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
            return true;
        }
        catch
        {
            return false;
        }

    }

    public static bool updateBadgeFolder(string FolderName, int CimNo, int ID)
    {
        try
        {
            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;

                string query = @"pr_TranscomUniversity_UpdateBadgeFolder";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@ID", ID);
                cmd.Parameters.Add("@folderName", FolderName);
                cmd.Parameters.Add("@updatedBy", CimNo);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
            return true;
        }
        catch
        {
            return false;
        }
    }

    public static bool updateBadgesAndCertificate(string BadgeName, int CimNo, int ID)
    {
        try
        {
            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;

                string query = @"pr_TranscomUniversity_UpdateBadgesAndCertificates";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@ID", ID);
                cmd.Parameters.Add("@BadgeName", BadgeName);
                cmd.Parameters.Add("@updatedBy", CimNo);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
            return true;
        }
        catch
        {
            return false;
        }
    }

    public static bool deleteBadgeFolder(int CimNo, int ID)
    {
        try
        {
            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;

                string query = @"pr_TranscomUniversity_DeleteBadgesFolder";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@ID", ID);
                cmd.Parameters.Add("@CIM", CimNo);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
            return true;
        }
        catch
        {
            return false;
        }
    }

    public static bool deleteBadgesCertificate(int CimNo, int ID)
    {
        try
        {
            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;

                string query = @"pr_TranscomUniversity_DeleteBadgesCertificate";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@ID", ID);
                cmd.Parameters.Add("@CIM", CimNo);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
            return true;
        }
        catch
        {
            return false;
        }
    }

    public static DataSet GetBadge(int BadgeID, int FileType, int CourseID)
    {
        DataSet ds = new DataSet();

        using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        {
            cn.Open();
            SqlCommand cmd;

            string query = @"SELECT * FROM
                                tbl_TranscomUniversity_Lkp_Badge WHERE HIDEFROMLIST = 0 and ID = " + BadgeID +
                                @" and FileType = " + FileType + 
                                @" and CourseID =" + CourseID;

            cmd = new SqlCommand(query, cn);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
        }
        return ds;
    }

    public static bool insertAwardingConditions(int BadgeID, int CriteriaID, int CimNo, int FileType)
    {
        try
        {
            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;

                string query = @"pr_TranscomUniversity_InsertAwardingConditions";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@badgeID", BadgeID);
                cmd.Parameters.Add("@criteriaID", CriteriaID);
                cmd.Parameters.Add("@cimNo", CimNo);
                cmd.Parameters.Add("@fileType", FileType);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
            return true;
        }
        catch
        {
            return false;
        }

    }

    public static dynamic GetAssessmentResourceType(int testCategoryID)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var d = db.tbl_transcomuniversity_rlt_assessment
                        .Where(t => t.TestCategoryID == testCategoryID)
                        .Select(t => new
                        {
                            t.ResourceTypeID
                        }).SingleOrDefault();
            return d.ResourceTypeID;
        }
    }

    public static dynamic GetAssessmentTestType(int testCategoryID)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var d = db.tbl_transcomuniversity_rlt_assessment
                        .Where(t => t.TestCategoryID == testCategoryID)
                        .Select(t => new
                        {
                            t.TestTypeID
                        }).SingleOrDefault();
            return d.TestTypeID;
        }
    }

    public static dynamic GetAssessmentSection(int testCategoryID)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var d = db.tbl_transcomuniversity_rlt_assessment
                        .Where(t => t.TestCategoryID == testCategoryID)
                        .Select(t => new
                        {
                            t.SectionID
                        }).SingleOrDefault();
            return d.SectionID;
        }
    }

    public static dynamic GetCourseSections(int courseID, int resourceTypeID)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var d = db.tbl_transcomuniversity_rlt_CourseSections
                        .Where(s => s.CourseID == courseID && s.HideFromList == false && s.ResourceTypeID == resourceTypeID)
                        .Select(s => new
                        {
                            s.SectionID,
                            s.SectionName
                        })
                        .ToList();
            var ns = new { SectionID = 0, SectionName = "Not Section" };
            d.Add(ns);

            return d;
        }
    }

    //MyProfile Starc STR

    public static dynamic GetStarcStrength(int CIM)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var d = db.pr_TranscomUniversity_GetStarcAssessment(CIM, 1)
                        .ToList();
            return d;
        }
    }

    public static dynamic GetStarcOpportunity(int CIM)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var d = db.pr_TranscomUniversity_GetStarcAssessment(CIM, 0)
                        .ToList();
            return d;
        }
    }

    public static bool insertBadges(int CourseID, string BadgeName, string Description, string FileName, System.Nullable<int> FolderID, int CIM, int FileType)
    {
        try
        {
            using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                cn.Open();

                SqlCommand cmd;

                string query = @"pr_TranscomUniversity_addBadgesAndCertificates";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add("@CourseID", CourseID);
                cmd.Parameters.Add("@BadgeName", BadgeName);
                cmd.Parameters.Add("@Description", Description);
                cmd.Parameters.Add("@FileName", FileName);
                cmd.Parameters.Add("@FolderID", FolderID);
                cmd.Parameters.Add("@createdBy", CIM);
                cmd.Parameters.Add("@FileType", FileType);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
            return true;
        }
        catch
        {
            return false;
        }
    }

    //CLASS MANAGEMENT

    public static string GetClassNameById(int classID)
    {
        try
        {
            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                var d = db.tbl_TranscomUniversity_Cor_Classes
                            .Where(c => c.ClassID == classID)
                            .SingleOrDefault();
                return d.ClassName;
            }
        }
        catch (Exception)
        {
            return "";
            throw;
        }
    }

    public static dynamic GetClassDetails(int classID)
    {
        try
        {
            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                var d = db.vw_TranscomUniversity_ClassDetails
                            .Where(c => c.ClassID == classID)
                            .SingleOrDefault();
                return d;
            }
        }
        catch (Exception)
        {
            return null;
            throw;
        }
    }

    public static void logClass(int ClassID, int ActionID, int Duration, string UserID, string ActionDescription, string IpAddress)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            db.pr_TranscomUniversity_ClassLog(ClassID, ActionID, Duration, UserID, ActionDescription, IpAddress);
            db.SaveChanges();
        }

    }

    public static dynamic getClassLogHistory(int classid)
    {
        //using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        //{
        //    var groups = db.vw_TranscomUniversity_CourseLog.Where(g => g.CourseID == courseid).OrderByDescending(g => g.LogDate).ToArray()
        //        .Select(g => new
        //        {
        //            LogDate = g.LogDate,
        //            UserID = g.UserID,
        //            UserRole = g.Role,
        //            EmailAddress = g.EmailAddress,
        //            UserIpAddress = g.Ip,
        //            Action = g.Action,
        //            Duration = (TimeSpan.FromMilliseconds((Double)g.Duration)).ToString(@"hh\:mm\:ss\.fff")
        //        });
        //    if (groups != null)
        //        return groups;
        //}
        return null;
    }

    public static dynamic GetLeadershipCourses()
    {
        try
        {
            var db = new TranscomUniversityV3ModelContainer();
            int cim = Convert.ToInt32(GetCurrentUserCIM());
            var courses = db.pr_TranscomUniversity_getCompetencies(cim)
                       .AsEnumerable()
                       .Select(m => new
                       {
                           CourseID = m.CourseID,
                           EncryptedCourseID = Utils.Encrypt(m.CourseID),
                           CourseTitle = m.CourseTitle,
                           CourseDescription = m.CourseDescription,
                           CourseImage = m.CourseImage
                       })
                       .Take(3)
                       .ToList();

            return courses;
        }
        catch (Exception)
        {
            return null;
            throw;
        }
       

    }

    public static dynamic GetSkillDevelopmentCourses()
    {
        try
        {
            var db = new TranscomUniversityV3ModelContainer();
            var courses = db.vw_TranscomUniversity_AllCourses
                       .AsEnumerable()
                       .Select(m => new
                       {
                           CourseID = m.CourseID,
                           EncryptedCourseID = Utils.Encrypt(m.CourseID),
                           CourseTitle = m.CourseTitle,
                           CourseDescription = m.CourseDescription,
                           CourseImage = m.CourseImage,
                           m.CategoryID
                       })
                //.Where(c => c.CategoryID == 18)
                       .Take(3)
                       .ToArray();
            return courses;
        }
        catch (Exception)
        {
            return null;
            throw;
        }
      

    }

    public static dynamic GetMandatoryProgramSpecificCourses(int CIM, int subcategoryid)
    {
        try
        {
            var db = new TranscomUniversityV3ModelContainer();
            var courses = db.pr_GetMandatoryProgramSpecificCourses(CIM, subcategoryid)
                .AsEnumerable()
                       .Select(m => new
                       {
                           CourseID = m.CourseID,
                           EncryptedCourseID = Utils.Encrypt(m.CourseID),
                           CourseTitle = m.Title,
                           CourseDescription = m.Description,
                           CourseImage = m.CourseImage,
                           m.SubcategoryID
                       });
            return courses;
        }
        catch (Exception)
        {
            return null;
            throw;
        }
       

    }

    public static dynamic GetMandatoryTranscomWideCourses()
    {
        try
        {
            var db = new TranscomUniversityV3ModelContainer();
            var courses = db.vw_TranscomUniversity_MandatoryCourse2
                       .AsEnumerable()
                       .Select(m => new
                       {
                           CourseID = m.CourseID,
                           EncryptedCourseID = Utils.Encrypt(m.CourseID),
                           CourseTitle = m.Title,
                           CourseDescription = m.Description,
                           CourseImage = m.CourseImage,
                           m.SubcategoryID
                       })
                        .Where(c => c.SubcategoryID == 29)
                        .OrderByDescending(c => c.CourseID)
                       .Take(3)
                       .ToArray();
            return courses;
        }
        catch (Exception)
        {
            return null;
            throw;
        }
       

    }

    public static dynamic GetMandatoryRegionalCourses()
    {
        try
        {
            var db = new TranscomUniversityV3ModelContainer();
            var courses = db.vw_TranscomUniversity_MandatoryCourse2
                       .AsEnumerable()
                       .Select(m => new
                       {
                           CourseID = m.CourseID,
                           EncryptedCourseID = Utils.Encrypt(m.CourseID),
                           CourseTitle = m.Title,
                           CourseDescription = m.Description,
                           CourseImage = m.CourseImage,
                           m.SubcategoryID
                       })
                       .Where(c => c.SubcategoryID == 30)
                       .OrderByDescending(c => c.CourseID)
                      .Take(3)
                      .ToArray();
            return courses;
        }
        catch (Exception)
        {
            return null;
            throw;
        }
      

    }

    public static dynamic GetMandatoryCountryCourses()
    {
        try
        {
            var db = new TranscomUniversityV3ModelContainer();
            var courses = db.vw_TranscomUniversity_MandatoryCourse2
                      .AsEnumerable()
                      .Select(m => new
                      {
                          CourseID = m.CourseID,
                          EncryptedCourseID = Utils.Encrypt(m.CourseID),
                          CourseTitle = m.Title,
                          CourseDescription = m.Description,
                          CourseImage = m.CourseImage,
                          m.SubcategoryID
                      })
                       .Where(c => c.SubcategoryID == 31)
                       .OrderByDescending(c => c.CourseID)
                      .Take(3)
                      .ToArray();
            return courses;
        }
        catch (Exception)
        {
            return null;
            throw;
        }
      

    }

    public static dynamic GetSupervisorDetails(int CIM)
    {
        var db = new TranscomUniversityV3ModelContainer();
        return db.pr_TranscomUniversity_GetSupervisorDetails(CIM).SingleOrDefault();
    }

    public static int lastInsertedCourseID()
    {

        try
        {
            int lastCourseID = db.tbl_TranscomUniversity_Cor_Course.Max(c => c.CourseID);
            return lastCourseID == null ? 0 : lastCourseID;
        }
        catch (Exception)
        {
            return 0;
            throw;
        }
      
      
      
    }

    public static int? GetCourseType(int courseID)
    {
        var db = new TranscomUniversityV3ModelContainer();
        int? courseTypeID = db.tbl_TranscomUniversity_Cor_Course
                            .Where(c => c.CourseID == courseID)
                            .Select(c => c.CourseTypeID)
                            .SingleOrDefault();
        return courseTypeID;
    }

    public static string GetSubcategoryNameByCourseID(int courseID)
    {
        try
        {
            var db = new TranscomUniversityV3ModelContainer();
            return db.pr_TranscomUniversity_GetSubcategoryById(courseID).SingleOrDefault().Subcategory;
        }
        catch (Exception)
        {

            throw;
        }

    }

    public static dynamic GetCourseAllSections(int courseID)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var d = db.tbl_transcomuniversity_rlt_CourseSections
                        .Where(s => s.CourseID == courseID && s.HideFromList == false)
                        .Select(s => new
                        {
                            s.SectionID,
                            s.SectionName
                        })
                        .ToList();
            var ns = new { SectionID = 0, SectionName = "Not Section" };
            d.Add(ns);

            return d;
        }
    }

    public static dynamic GetRecentlyAddedCoursesHome()
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            
            try
            {
                var recentlyAddedCourses = db.pr_TranscomUniversity_RecentlyAddedCourse()
                                     .Take(3).ToList();
                if (recentlyAddedCourses != null)
                    return recentlyAddedCourses;
            }
            catch (Exception)
            {
                return null;
                throw;
            }
        }
        return null;
    }

    public static int GetExamType(int id)
    {
        try
        {
            int courseTypes = (int)db.tbl_transcomuniversity_rlt_assessment
                           .Where(t => t.TestCategoryID == id)
                           .Select(t => t.TestTypeID)
                           .Single();

            return courseTypes;
        }
        catch (Exception)
        {

            throw;
        }

    }

    public static int lastResumableTestID(int testID, string CIM)
    {
        try
        {
            int testTaken = db.tbl_testing_testTaken
                           .Where(t => t.TestCategoryID == testID && t.UserID == CIM && t.DateEndTaken == null)
                           .OrderByDescending(t => t.TestTakenID)
                           .Select(t =>
                               t.TestTakenID
                           )
                           .Take(1)
                           .SingleOrDefault();


            return testTaken;
        }
        catch (Exception)
        {
            return 0;
            throw;
        }

    }

    public static bool isInSAP(string email)
    {
        return
            db.vw_TranscomUniversity_UsersInfo.Where(u => u.TranscomEmail == email).Select(u => u.CIMNumber).SingleOrDefault() != null ? true : false;

    }

    public static string GetCourseImage(int courseID)
    {
        try
        {
            return db.tbl_TranscomUniversity_Cor_Course
                    .Where(c => c.CourseID == courseID)
                    .Select(c => c.CourseImage)
                    .SingleOrDefault();

        }
        catch (Exception)
        {

            throw;
        }
    }

    public static int GetTestQuestionCount(int testID)
    {
        return db.tbl_testing_questionnaire.Count(q => q.TestCategoryID == testID && q.HideFromList == false);
    }

    public static dynamic MyTranscript(int cim)
    {
        return db.pr_TranscomUniversity_MyTranscript(cim).ToList();
    }

    public static dynamic GetDepartments()
    {
        try
        {

            return db.tbl_TranscomUniversity_Lkp_Department.Where(d => d.HideFromList == false).ToList();
        }
        catch (Exception)
        {

            throw;
        } 
        
        
    }

    public static dynamic GetPrograms()
    {
        try
        {

            return db.tbl_TranscomUniversity_Lkp_Client.Where(d => d.HideFromList == false).ToList();
        }
        catch (Exception)
        {

            throw;
        }

       
    }

    public static dynamic GetRequiredCourses(int courseID)
    {
        return db.vw_Transcomuniversity_RequiredCoures
                .Where(c => c.CourseID == courseID && c.HideFromList == false)
                .ToList();
    }

    public static void HideAllRequiredCourse(int courseID)
    {
        try
        {
            db.pr_TranscomUniversity_HideRequiredCourse(courseID);
        }
        catch (Exception)
        {
            
            throw;
        }
    }

    public static void UpdateRequiredCourse(int courseID, int reqCourseID)
    {
        try
        {
            db.pr_TranscomUniversity_UpdateRequiredCourse(courseID, reqCourseID);
        }
        catch (Exception)
        {
            
            throw;
        }
       
    }

    public static void logCategory(int CategoryID, int ActionID, int Duration, string UserID, string ActionDescription, string IpAddress)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            db.pr_TranscomUniversity_CategoryLog(CategoryID, ActionID, Duration, UserID, ActionDescription, IpAddress);
            db.SaveChanges();
        }

    }

    public static int getLastCategoryLogID()
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            int lastlogid = 0;
            try
            {
                lastlogid = db.tbl_TranscomUniversity_Rlt_CategoryLog.Max(l => l.CategoryID);
                if (lastlogid >= 0)
                    return lastlogid;
            }
            catch
            {
                return lastlogid;
            }

        }
        return 0;
    }

    public static bool isTrainer(int courseID, int userCIM)
    {
        try
        {
            var trainer = db.tbl_TranscomUniversity_Cor_CourseTrainer
                .Where(t => t.CourseID == courseID && t.TrainerCIM == userCIM && t.HideFromList == false)
                .SingleOrDefault();

            if (trainer != null)
                return true;
        }
        catch (Exception)
        {
            return false;
            throw;
        }
        return false;
    }
    
 
    public static int getFileID ()
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())        {
            

            var retVal = Convert.ToInt32((from u in db.tbl_transcomuniversity_Lkp_Badge     
                                          orderby u.ID descending
                                          select u.ID).FirstOrDefault().ToString());

            return Convert.ToInt32(retVal + 1);
        }
   
    }

    public static bool isFolderName(string folderName, int courseid, int FileType, string Type)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            if (Type != "image")
            {
                var ds = db.tbl_TranscomUniversity_Lkp_FolderBadge
                                 .Select(a => new
                                 {
                                     a.FolderName,
                                     a.FileType,
                                     a.CourseID,
                                     a.HideFromList
                                 })
                                 .Where(a => a.FolderName == folderName && a.FileType == FileType && a.CourseID == courseid && a.HideFromList == 0)
                                 .ToList();


                if (ds.Count == 0)
                    return true;
                else
                    return false;
            }
            else {
                var ds = db.tbl_transcomuniversity_Lkp_Badge
                                    .Select(a => new
                                    {
                                        a.BadgeName,
                                        a.FileType,
                                        a.CourseID,
                                        a.HideFromList
                                    })
                                    .Where(a => a.BadgeName == folderName && a.FileType == FileType && a.CourseID == courseid && a.HideFromList == 0)
                                    .ToList();


                if (ds.Count == 0)
                    return true;
                else
                    return false;
            }
        }
    }

    //TEST
    public static pr_transcomuniversity_coursedetails_Result GetCourseDetails2(int CourseID)
    {
        return db.pr_transcomuniversity_coursedetails(CourseID).SingleOrDefault();
    }

    public static dynamic GetCourseRating2(int CourseID)
    {
        return db.vw_TranscomUniversity_CourseComments
                .Select(c => new
                {
                    c.CourseID,
                    c.Rating
                })
                .Where(c => c.CourseID == CourseID)
                .ToList();
    }
    

}


