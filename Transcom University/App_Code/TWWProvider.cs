﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;
using System.Collections.Specialized;
using TranscomUniversityV3Model;
using NuComm;

namespace TWW.SecurityProvider
{
    public class TWWMembershipProvider : MembershipProvider
    {

        #region privatevariables

        private bool _enablePasswordReset;
        private bool _enablePasswordRetrieval;
        private int _maxInvalidPasswordAttempts;
        private int _minRequiredNonalphanumericCharacters;
        private int _minRequiredPasswordLength;
        private int _passwordAttemptWindow;
        private MembershipPasswordFormat _passwordFormat;
        private string _passwordStrengthRegularExpression;
        private bool _requiresQuestionAndAnswer;
        private bool _requiresUniqueEmail;

        private string _applicationName;
        private string _remoteProviderName;

        #endregion

        #region properties

        /// <summary>
        /// required implementation
        /// </summary>
        public override string ApplicationName
        {
            get
            {
                return _applicationName;
            }
            set
            {
                _applicationName = value;
            }
        }

        /// <summary>
        /// required implementation
        /// </summary>
        public override bool EnablePasswordReset
        {
            get { return _enablePasswordReset; }
        }

        /// <summary>
        /// required implementation
        /// </summary>
        public override bool EnablePasswordRetrieval
        {
            get { return _enablePasswordRetrieval; }
        }

        /// <summary>
        /// required implementation
        /// </summary>
        public override int MaxInvalidPasswordAttempts
        {
            get { return _maxInvalidPasswordAttempts; }
        }

        /// <summary>
        /// required implementation
        /// </summary>
        public override int MinRequiredNonAlphanumericCharacters
        {
            get { return _minRequiredNonalphanumericCharacters; }
        }

        /// <summary>
        /// required implementation
        /// </summary>
        public override int MinRequiredPasswordLength
        {
            get { return _minRequiredPasswordLength; }
        }

        /// <summary>
        /// required implementation
        /// </summary>
        public override int PasswordAttemptWindow
        {
            get { return _passwordAttemptWindow; }
        }

        /// <summary>
        /// required implementation
        /// </summary>
        public override MembershipPasswordFormat PasswordFormat
        {
            get { return _passwordFormat; }
        }

        /// <summary>
        /// required implementation
        /// </summary>
        public override string PasswordStrengthRegularExpression
        {
            get { return _passwordStrengthRegularExpression; }
        }

        /// <summary>
        /// required implementation
        /// </summary>
        public override bool RequiresQuestionAndAnswer
        {
            get { return _requiresQuestionAndAnswer; }
        }

        /// <summary>
        /// required implementation
        /// </summary>
        public override bool RequiresUniqueEmail
        {
            get { return _requiresUniqueEmail; }
        }

        #endregion

        public TWWMembershipProvider()
        {
        }

        #region function overrides

        public override void Initialize(string name, NameValueCollection config)
        {
            _applicationName = config["applicationName"];
            if (string.IsNullOrEmpty(_applicationName))
            {
                _applicationName = ProviderUtility.GetDefaultAppName();
            }

            _enablePasswordRetrieval = ProviderUtility.GetBooleanValue(config, "enablePasswordRetrieval", false);
            _enablePasswordReset = ProviderUtility.GetBooleanValue(config, "enablePasswordReset", true);
            _requiresQuestionAndAnswer = ProviderUtility.GetBooleanValue(config, "requiresQuestionAndAnswer", true);
            _requiresUniqueEmail = ProviderUtility.GetBooleanValue(config, "requiresUniqueEmail", true);
            _maxInvalidPasswordAttempts = ProviderUtility.GetIntValue(config, "maxInvalidPasswordAttempts", 5, false, 0);
            _passwordAttemptWindow = ProviderUtility.GetIntValue(config, "passwordAttemptWindow", 10, false, 0);
            _minRequiredPasswordLength = ProviderUtility.GetIntValue(config, "minRequiredPasswordLength", 7, false, 0x80);
            _minRequiredNonalphanumericCharacters = ProviderUtility.GetIntValue(config, "minRequiredNonalphanumericCharacters", 1, true, 0x80);
            _passwordStrengthRegularExpression = config["passwordStrengthRegularExpression"];

            if (config["passwordFormat"] != null)
            {
                _passwordFormat = (MembershipPasswordFormat)Enum.Parse(typeof(MembershipPasswordFormat), config["passwordFormat"]);
            }
            else
            {
                _passwordFormat = MembershipPasswordFormat.Hashed;
            }

            _remoteProviderName = config["remoteProviderName"];

            base.Initialize(name, config);
        }

        /// <summary>
        /// required implementation
        /// </summary>
        /// <param name="username">a username</param>
        /// <param name="oldPassword">original password</param>
        /// <param name="newPassword">new password</param>
        /// <returns>true or false</returns>
        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            //return service.ChangePassword(_RemoteProviderName, _ApplicationName, username, oldPassword, newPassword);
            return false;
        }

        /// <summary>
        /// required implementation
        /// </summary>
        /// <param name="username">a username</param>
        /// <param name="password">the password</param>
        /// <param name="newPasswordQuestion">new question</param>
        /// <param name="newPasswordAnswer">new answer</param>
        /// <returns>true or false</returns>
        public override bool ChangePasswordQuestionAndAnswer(string username, string password,
          string newPasswordQuestion, string newPasswordAnswer)
        {
            //return service.ChangePasswordQuestionAndAnswer(_RemoteProviderName, _ApplicationName, username, password,
            //  newPasswordQuestion, newPasswordAnswer);
            return false;
        }

        /// <summary>
        /// required implementation
        /// </summary>
        /// <param name="username">required implementation</param>
        /// <param name="password">required implementation</param>
        /// <param name="email">required implementation</param>
        /// <param name="passwordQuestion">required implementation</param>
        /// <param name="passwordAnswer">required implementation</param>
        /// <param name="isApproved">required implementation</param>
        /// <param name="providerUserKey">required implementation</param>
        /// <param name="status">required implementation</param>
        /// <returns>a user object</returns>
        public override MembershipUser CreateUser(string username, string password, string email,
          string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey,
          out MembershipCreateStatus status)
        {
            //  passwordQuestion, passwordAnswer, isApproved, providerUserKey, out newStatus));
            status = (MembershipCreateStatus)Enum.Parse(typeof(MembershipCreateStatus), null);
            return null;
        }

        /// <summary>
        /// required implementation
        /// </summary>
        /// <param name="username">required implementation</param>
        /// <param name="deleteAllRelatedData">required implementation</param>
        /// <returns>required implementation</returns>
        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            return false;
        }

        /// <summary>
        /// required implementation
        /// </summary>
        /// <param name="emailToMatch">required implementation</param>
        /// <param name="pageIndex">required implementation</param>
        /// <param name="pageSize">required implementation</param>
        /// <param name="totalRecords">required implementation</param>
        /// <returns>required implementation</returns>
        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            totalRecords = 0;
            return null;
        }

        /// <summary>
        /// required implementation
        /// </summary>
        /// <param name="usernameToMatch">required implementation</param>
        /// <param name="pageIndex">required implementation</param>
        /// <param name="pageSize">required implementation</param>
        /// <param name="totalRecords">required implementation</param>
        /// <returns>required implementation</returns>
        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex,
          int pageSize, out int totalRecords)
        {
            totalRecords = 0;
            return null;
        }

        /// <summary>
        /// required implementation
        /// </summary>
        /// <param name="pageIndex">required implementation</param>
        /// <param name="pageSize">required implementation</param>
        /// <param name="totalRecords">required implementation</param>
        /// <returns>required implementation</returns>
        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize,
          out int totalRecords)
        {
            totalRecords = 0;
            return null;
        }

        /// <summary>
        /// required implementation
        /// </summary>
        /// <returns>required implementation</returns>
        public override int GetNumberOfUsersOnline()
        {
            return 0;
        }

        /// <summary>
        /// required implementation
        /// </summary>
        /// <param name="username">required implementation</param>
        /// <param name="answer">required implementation</param>
        /// <returns>required implementation</returns>
        public override string GetPassword(string username, string answer)
        {
            return null;
        }

        /// <summary>
        /// required implementation
        /// </summary>
        /// <param name="username">required implementation</param>
        /// <param name="userIsOnline">required implementation</param>
        /// <returns>required implementation</returns>
        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            int cimNumber;

            if (@username.Contains("@"))
            {
                cimNumber = 0;
                var user = new MembershipUser("TWWMembershipProvider",
                                              @username,
                                              @username,
                                              null,
                                              null,
                                              null,
                                              true,
                                              false,
                                              DateTime.MinValue,
                                              DateTime.MinValue,
                                              DateTime.MinValue,
                                              DateTime.MinValue,
                                              DateTime.MinValue);

                return user;
            }
            else
            {
                if (!Int32.TryParse(username, out cimNumber))
                    return null;

                var emp = new Employee();

                if (emp.LoadEmployeeByCIM(cimNumber, DateTime.Now))
                {
                    var user = new MembershipUser("TWWMembershipProvider",
                                                  emp.CimNumber,
                                                  emp.EmployeeID,
                                                  emp.FirstName,
                                                  emp.LastName,
                                                  null,
                                                  true,
                                                  false,
                                                  Convert.ToDateTime(emp.StartDate),
                                                  DateTime.MinValue,
                                                  DateTime.MinValue,
                                                  DateTime.MinValue,
                                                  DateTime.MinValue);

                    return user;
                }
                else
                    return null;
            }
        }

        /// <summary>
        /// required implementation
        /// </summary>
        /// <param name="providerUserKey">required implementation</param>
        /// <param name="userIsOnline">required implementation</param>
        /// <returns>required implementation</returns>
        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            var emp = new Employee();

            if (emp.LoadEmployee(int.Parse(providerUserKey.ToString()), DateTime.Now))
            {
                var user = new MembershipUser("TWWMembershipProvider",
                                              emp.FirstName + " " + emp.LastName,
                                              emp.EmployeeID,
                                              null,
                                              null,
                                              null,
                                              true,
                                              false,
                                              Convert.ToDateTime(emp.StartDate),
                                              DateTime.MinValue,
                                              DateTime.MinValue,
                                              DateTime.MinValue,
                                              DateTime.MinValue);

                return user;
            }
            else
                return null;
        }

        /// <summary>
        /// required implementation
        /// </summary>
        /// <param name="email">required implementation</param>
        /// <returns>required implementation</returns>
        public override string GetUserNameByEmail(string email)
        {
            return null;
        }

        /// <summary>
        /// required implementation
        /// </summary>
        /// <param name="username">required implementation</param>
        /// <param name="answer">required implementation</param>
        /// <returns>required implementation</returns>
        public override string ResetPassword(string username, string answer)
        {
            return null;
        }

        /// <summary>
        /// required implementation
        /// </summary>
        /// <param name="userName">required implementation</param>
        /// <returns>required implementation</returns>
        public override bool UnlockUser(string userName)
        {
            return false;
        }
        /// <summary>
        /// required implementation
        /// </summary>
        /// <param name="user">required implementation</param>
        public override void UpdateUser(MembershipUser user)
        {

        }

        //public override bool ValidateUser(string username, string password)
        //{
        //    var cimNumber = username;
        //    if ((int)Data.Intranet.SPs.PrIntranetChkLoginUser(cimNumber, password).ExecuteScalar() > 0)
        //        return true;
        //    return false;
        //}

        public override bool ValidateUser(string username, string password)
        {
            //var cimNumber = username;
            //var db = new TranscomUniversityV3ModelContainer();
            //var result = new List<pr_Intranet_Chk_LoginUserResult>(db.pr_Intranet_Chk_LoginUser(cimNumber, password)).FirstOrDefault().GlobalEmployeeID;

            return bool.Parse(username); //result > 0;
        }

        #endregion
    }

    //public class TWWRoleProvider : RoleProvider
    //{
    //    #region privatevariables

    //    private string _applicationName;
    //    private string _remoteProviderName;

    //    #endregion

    //    public TWWRoleProvider()
    //    {

    //    }

    //    #region properties

    //    /// <summary>
    //    /// required implementation
    //    /// </summary>
    //    public override string ApplicationName
    //    {
    //        get
    //        {
    //            return _applicationName;
    //        }
    //        set
    //        {
    //            _applicationName = value;
    //        }
    //    }

    //    #endregion

    //    #region overridable fuctions

    //    /// <summary>
    //    /// handle the Initiate override and extract our parameters
    //    /// </summary>
    //    /// <param name="name">name of the provider</param>
    //    /// <param name="config">configuration collection</param>
    //    public override void Initialize(string name, NameValueCollection config)
    //    {
    //        _applicationName = config["applicationName"];
    //        if (string.IsNullOrEmpty(_applicationName))
    //        {
    //            _applicationName = ProviderUtility.GetDefaultAppName();
    //        }

    //        _remoteProviderName = config["remoteProviderName"];

    //        base.Initialize(name, config);
    //    }

    //    /// <summary>
    //    /// required implementation
    //    /// </summary>
    //    /// <param name="usernames">a list of usernames</param>
    //    /// <param name="roleNames">a list of roles</param>
    //    public override void AddUsersToRoles(string[] usernames, string[] roleNames)
    //    {

    //    }

    //    /// <summary>
    //    /// required implementation
    //    /// </summary>
    //    /// <param name="roleName">a role name</param>
    //    public override void CreateRole(string roleName)
    //    {

    //    }

    //    /// <summary>
    //    /// required implementation
    //    /// </summary>
    //    /// <param name="roleName">a role</param>
    //    /// <param name="throwOnPopulatedRole">get upset of users are in a role</param>
    //    /// <returns></returns>
    //    public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
    //    {
    //        return false;
    //    }

    //    /// <summary>
    //    /// required implemention
    //    /// </summary>
    //    /// <param name="roleName">a role</param>
    //    /// <param name="usernameToMatch">a username to look for in the role</param>
    //    /// <returns></returns>
    //    public override string[] FindUsersInRole(string roleName, string usernameToMatch)
    //    {
    //        return null;
    //    }

    //    /// <summary>
    //    /// required implementation
    //    /// </summary>
    //    /// <returns></returns>
    //    public override string[] GetAllRoles()
    //    {
    //        //SubSonic.Query qry = new SubSonic.Query(TWW.Data.BlackBox.Schemas.TblSecurityLkpUserGroup);

    //        //string[] strRoles;

    //        //string sRoleList = string.Empty;

    //        //System.Data.IDataReader reader = qry.ExecuteReader();

    //        //while (reader.Read())
    //        //{
    //        //    sRoleList += sRoleList + ",";
    //        //}

    //        //if (sRoleList.Length > 0)
    //        //    sRoleList.Remove(sRoleList.Length - 1, 1);

    //        //strRoles = sRoleList.Split(',');

    //        //return strRoles;

    //        var strRoles = new[] { "Learner", "Manager", "Trainer", "Coordinator", "Admin" };

    //        return strRoles;
    //    }

    //    /// <summary>
    //    /// required implementation
    //    /// </summary>
    //    /// <param name="username">a username</param>
    //    /// <returns>a list of roles</returns>
    //    //public override string[] GetRolesForUser(string username)
    //    //{
    //    //    var db = new IntranetDBDataContext();
    //    //    var strRoles = new string[] { };

    //    //    var emp = new Employee();

    //    //    if (username.Contains("@"))
    //    //    {
    //    //        strRoles = new string[] { };
    //    //    }
    //    //    else
    //    //    {
    //    //        if (emp.LoadEmployeeByCIM(int.Parse(username), DateTime.Today)
    //    //            && DateTime.Parse(emp.EndDate) > DateTime.Today)
    //    //        {
    //    //            //var hasRole = new List<pr_TranscomUniversity_Rst_AdminAccessResult>(db.pr_TranscomUniversity_Rst_AdminAccess(int.Parse(emp.EmployeeID))).FirstOrDefault();

    //    //            //if (hasRole.Return > 0)
    //    //            //{
    //    //            var userRoles = new List<pr_TranscomUniversity_Lkp_RoleResult>(db.pr_TranscomUniversity_Lkp_Role(int.Parse(emp.EmployeeID))).ToArray();

    //    //            strRoles = new string[userRoles.Count()];
    //    //            int i = 0;

    //    //            foreach (var role in userRoles)
    //    //            {
    //    //                strRoles[i++] = role.UserRole;
    //    //            }

    //    //            //strRoles = new StringBuilder(strRoles.ToString().TrimEnd(','));
    //    //            //}
    //    //            //else
    //    //            //{
    //    //            //    strRoles = new string[] { };
    //    //            //}
    //    //        }
    //    //    }

    //    //    return strRoles;
    //    //}

    //    //public override string[] GetRolesForUser(string username)
    //    //{
    //    //    string[] strRoles;

    //    //    var emp = new Employee();

    //    //    if (username.Contains("@"))
    //    //    {
    //    //        strRoles = new[] {""};

    //    //        return strRoles;
    //    //    }
    //    //    else
    //    //    {
    //    //        if (emp.LoadEmployeeByCIM(int.Parse(username), DateTime.Today)
    //    //            && DateTime.Parse(emp.EndDate) > DateTime.Today)

    //    //            if (
    //    //                (int)
    //    //                Data.Intranet.SPs.PrTranscomUniversityRstAdminAccess(int.Parse(emp.EmployeeID)).ExecuteScalar() >
    //    //                0)
    //    //            {
    //    //                strRoles = new[] {"Admin"};
    //    //                return strRoles;
    //    //            }
    //    //            else
    //    //            {
    //    //                strRoles = new[] {""};
    //    //                return strRoles;
    //    //            }
    //    //    }
    //    //    return null;
    //    //}

    //    /// <summary>
    //    /// required implementation
    //    /// </summary>
    //    /// <param name="roleName">a role</param>
    //    /// <returns>a list of users</returns>
    //    public override string[] GetUsersInRole(string roleName)
    //    {
    //        return null;
    //    }

    //    /// <summary>
    //    /// required implementation
    //    /// </summary>
    //    /// <param name="username">a username</param>
    //    /// <param name="roleName">a role</param>
    //    /// <returns>true or false</returns>
    //    //public override bool IsUserInRole(string username, string roleName)
    //    //{
    //    //    string[] strRoles;

    //    //    strRoles = new[] { "Admin" };

    //    //    var emp = new Employee();

    //    //    if (emp.LoadEmployeeByCIM(int.Parse(username), DateTime.Today)
    //    //            && DateTime.Parse(emp.EndDate) > DateTime.Today)

    //    //        if ((int)Data.Intranet.SPs.PrTranscomUniversityRstAdminAccess(int.Parse(emp.EmployeeID)).ExecuteScalar() > 0)
    //    //        {
    //    //            return true;
    //    //        }
    //    //        else
    //    //        {
    //    //            return false;
    //    //        }

    //    //    return false;
    //    //}

    //    //public override bool IsUserInRole(string username, string roleName)
    //    //{
    //    //    var emp = new Employee();

    //    //    if (emp.LoadEmployeeByCIM(int.Parse(username), DateTime.Today)
    //    //        && DateTime.Parse(emp.EndDate) > DateTime.Today)
    //    //    {
    //    //        var db = new IntranetDBDataContext();
    //    //        var result = new List<pr_TranscomUniversity_Rst_AdminAccessResult>(db.pr_TranscomUniversity_Rst_AdminAccess(int.Parse(emp.EmployeeID))).FirstOrDefault().Return;

    //    //        return result > 0;
    //    //    }

    //    //    return false;
    //    //}

    //    /// <summary>
    //    /// required implementation
    //    /// </summary>
    //    /// <param name="usernames">a list of usernames</param>
    //    /// <param name="roleNames">a list of roles</param>
    //    public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
    //    {

    //    }

    //    /// <summary>
    //    /// required implementation
    //    /// </summary>
    //    /// <param name="roleName">a role</param>
    //    /// <returns>true or false</returns>
    //    public override bool RoleExists(string roleName)
    //    {
    //        return false;
    //    }

    //    #endregion
    //}

    sealed internal class ProviderUtility
    {
        /// <summary>
        /// Get a default application name
        /// </summary>
        /// <returns></returns>
        internal static string GetDefaultAppName()
        {
            string defPath;
            try
            {
                var vPath = System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath;
                if (string.IsNullOrEmpty(vPath))
                {
                    vPath = System.Diagnostics.Process.GetCurrentProcess().MainModule.ModuleName;
                    var num1 = vPath.IndexOf('.');
                    if (num1 != -1)
                    {
                        vPath = vPath.Remove(num1);
                    }
                }
                if (string.IsNullOrEmpty(vPath))
                {
                    return "/";
                }
                defPath = vPath;
            }
            catch
            {
                defPath = "/";
            }
            return defPath;
        }

        /// <summary>
        /// return a value from a collection of a default value if not in that collection
        /// </summary>
        /// <param name="config">the collection</param>
        /// <param name="valueName">the value to look up</param>
        /// <param name="defaultValue">the default value</param>
        /// <returns>a value from the collection or the default value</returns>
        internal static bool GetBooleanValue(NameValueCollection config, string valueName, bool defaultValue)
        {
            bool result;
            var valueToParse = config[valueName];
            if (valueToParse == null)
            {
                return defaultValue;
            }
            if (bool.TryParse(valueToParse, out result))
            {
                return result;
            }
            throw new Exception("Value must be boolean");
        }

        /// <summary>
        /// return a value from a collection of a default value if not in that collection
        /// </summary>
        /// <param name="config">a collection</param>
        /// <param name="valueName">the name of the value in the collection</param>
        /// <param name="defaultValue">a default value</param>
        /// <param name="zeroAllowed">is zero allowed</param>
        /// <param name="maxValueAllowed">what is the largest number that will be accepted</param>
        /// <returns>a value</returns>
        internal static int GetIntValue(NameValueCollection config, string valueName, int defaultValue, bool zeroAllowed, int maxValueAllowed)
        {
            int result;
            var valueToParse = config[valueName];
            if (valueToParse == null)
            {
                return defaultValue;
            }
            if (!int.TryParse(valueToParse, out result))
            {
                if (zeroAllowed)
                {
                    throw new Exception("Value must be non negative integer");
                }
                throw new Exception("Value must be positive integer");
            }
            if (zeroAllowed && (result < 0))
            {
                throw new Exception("Value must be non negative integer");
            }
            if (!zeroAllowed && (result <= 0))
            {
                throw new Exception("Value must be positive integer");
            }
            if ((maxValueAllowed > 0) && (result > maxValueAllowed))
            {
                throw new Exception("Value too big");
            }
            return result;
        }
    }

}
