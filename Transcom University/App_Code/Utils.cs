﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.IO;
using System.Text;

/// <summary>
/// Summary description for Utils
/// </summary>
public class Utils
{
    public static string Decrypt(string cipherText)
    {
        try
        {
            string EncryptionKey = "TRNSCMV32017111";
            cipherText = cipherText.Replace(" ", "+");
            byte[] cipherBytes = Convert.FromBase64String(cipherText);

            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }
        catch {
            return null;
        }
    }

    public static string Encrypt(int? text)
    {
        string EncryptionKey = "TRNSCMV32017111";
        string clearText = text.ToString();
        byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);

        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(clearBytes, 0, clearBytes.Length);
                    cs.Close();
                }
                clearText = Convert.ToBase64String(ms.ToArray());
            }
        }
        return clearText;
    }


    public static string callClientScript(string functionName, string functionParams)
    {
        return "function f(){" + functionName + "('" + functionParams + "'); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
    }

    public static string callClientScript(string functionName, string functionParams1, string functionParams2)
    {
        return "function f(){" + functionName + "('" + functionParams1 + "','" + functionParams2 + "'); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
    }

    public static string callClientScript(string functionName, string functionParams1, string functionParams2, string functionParamm3)
    {
        return "function f(){" + functionName + "('" + functionParams1 + "','" + functionParams2 + "','" + functionParamm3 + "'); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
    }

    public static string callClientScript(string functionName)
    {
        return "function f(){" + functionName + "(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
    }

    public static string ClipStringTo100(string str)
    {
        bool t = str.Length > 100;
        return str.Substring(0, t ? 100 : str.Length) + (t ? "..." : "");
    }

    public static string ClipStringTo500(string str)
    {
        bool t = str.Length > 500;
        return str.Substring(0, t ? 500 : str.Length) + (t ? "..." : "");
    }

    public static string ClipStringTo(string str, int len)
    {
        bool t = str.Length > len;
        return str.Substring(0, t ? len : str.Length) + (t ? "..." : "");
    }
}