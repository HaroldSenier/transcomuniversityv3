﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TranscomUniversityV3.Master" AutoEventWireup="true"
    CodeFile="AssessmentResultSummary.aspx.cs" Inherits="AssessmentResultSummary" %>

<%@ Register Src="UserControl/Statics/CourseCatalogSidebarUserCtrl.ascx" TagName="courseCatalogSidebar"
    TagPrefix="ucCcSidebar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="contentPlaceHolderLeftPanel" runat="Server">
    <rad:RadWindowManager ID="rwm" RenderMode="Lightweight" EnableShadow="true" Skin="Bootstrap"
        Modal="true" VisibleOnPageLoad="false" Behaviors="Close, Move" DestroyOnClose="true"
        RestrictionZoneID="RestrictionZone" Opacity="99" runat="server" VisibleStatusbar="false">
    </rad:RadWindowManager>
    <rad:RadScriptBlock ID="rsb1" runat="server">
        <script type="text/javascript">
            function pageLoad() {
                var lblDateStartedVal = $("#<%= lblDateStartedVal.ClientID %>").text();
                var lblDateFinishedVal = $("#<%= lblDateFinishedVal.ClientID %>").text();

                var dtStartedVal = convertTimeZone(lblDateStartedVal);
                var dtFinishedVal = convertTimeZone(lblDateFinishedVal);

                $("#<%= lblDateStartedVal.ClientID %>").text(moment(dtStartedVal).format("MMMM DD, YYYY"));
                $("#<%= lblDateFinishedVal.ClientID %>").text(moment(dtFinishedVal).format("MMMM DD, YYYY"));
            }

            function btnBackToCoursePreview_ClientClick() {
                window.location.href = "CourseLauncher.aspx?CourseID=" + '<%= Request.QueryString["CourseID"] %>' + "&tz=" + '<%= Request.QueryString["tz"] %>';
            }
        </script>
    </rad:RadScriptBlock>
    <ucCcSidebar:courseCatalogSidebar ID="ccSidebar" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolderMain" runat="Server">
    <asp:Panel runat="server" ID="pnlAssessmentDetailContainer" CssClass="col-md-12">
        <div class="row course-preview-row-container bottom-margin-md">
            <div class="col-md-12 course-preview-header">
                <ul id="ulBreadcrumb" runat="server" class="breadcrumb">
                    <li id="bc1" runat="server"><a>
                        <asp:Label ID="lblTab0" runat="server" Text="<%$ Resources: LocalizedResource, Learner %>" /></a></li>
                    <li id="bc2" runat="server"><a>
                        <asp:Label ID="lblTab1" runat="server" Text="<%$ Resources: LocalizedResource, CourseCatalog %>" /></a></li>
                    <li id="bc3" runat="server"><a>
                        <asp:Label ID="lblTab2" runat="server" Text="" /></a></li>
                    <li id="bc4" runat="server">
                        <asp:Label ID="lblTab3" runat="server" Text="" ForeColor="Yellow" /></li>
                </ul>
            </div>
        </div>
        <asp:Panel runat="server" ID="pnlExamContainer" CssClass="">
            <div class="col-md-4 text-center">
                <div class="col-md-12">
                    <div class="exam-image-container">
                        <asp:Label Text="Exam Title" runat="server" ID="lblExamTitle" CssClass="exam-title" />
                        <img id="takeAssessmentCourseImage" src="#" alt="Course Image" width="350" height="250"
                            onerror="this.onerror=null;this.src='Media/Uploads/CourseImg/No_image.jpg'" class="margin-bottom-10px exam-image"
                            runat="server" />
                    </div>
                    <asp:Panel runat="server" ID="pnlActionButtonContainer" CssClass="container">
                        <div class="col-md-12">
                            <asp:Button ID="btnBack" Text="Back to Course" CssClass="btn tc-btn-md btn-teal btn-flat"
                                runat="server" OnClientClick="btnBackToCoursePreview_ClientClick(); return false;" />
                        </div>
                    </asp:Panel>
                </div>
            </div>
            <div class="col-md-4 no-padding" style="margin-left: -30px;">
                <span class="bold">
                    <asp:Literal ID="ltInstructionLabel" Text="Assessment Instruction" runat="server" /></span>
                <p runat="server" id="pInstruction">
                    <asp:Label runat="server" Text="Instruction" />
                </p>
                <br />
                <br />
                <span class="bold">
                    <asp:Literal ID="ltDetailsLabel" Text="Assessment Details:" runat="server" /></span>
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:Label ID="Label3" runat="server" Text="<%$ Resources: LocalizedResource, StartDateTime %>" />:
                        </td>
                        <td>
                            <asp:Literal ID="ltStartDate" Text="January 1, 2019" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label4" runat="server" Text="<%$ Resources: LocalizedResource, EndDateTime %>" />:
                        </td>
                        <td>
                            <asp:Literal ID="ltEndDate" Text="January 1, 2019" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label5" runat="server" Text="<%$ Resources: LocalizedResource, Duration %>" />:
                        </td>
                        <td>
                            <asp:Literal ID="ltDuration" Text="0" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label9" runat="server" Text="<%$ Resources: LocalizedResource, NoofQuestions %>" />:
                        </td>
                        <td>
                            <asp:Literal ID="ltNumbOfQuestion" Text="99" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label10" runat="server" Text="<%$ Resources: LocalizedResource, AssessmentType %>" />:&nbsp;
                        </td>
                        <td>
                            <asp:Literal ID="ltType" Text="<%$ Resources: LocalizedResource, Assessment %>" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-md-4 no-padding">
                <asp:Panel ID="pnlMain" runat="server" Font-Names="Arial" HorizontalAlign="left"
                    Width="100%">
                    <span class="bold">
                        <asp:Label ID="Label11" runat="server" Text="<%$ Resources: LocalizedResource, AssessmentResultSummary %>" />:</span>
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:Label ID="Label6" runat="server" Text="<%$ Resources: LocalizedResource, Status %>" />
                            </td>
                            <td colspan="2">
                                <asp:Label ID="lblResult" runat="server" CssClass="testQuestion" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label12" runat="server" Text="<%$ Resources: LocalizedResource, DateTimeStarted %>" />:
                            </td>
                            <td>
                                <asp:Label ID="lblDateStartedVal" runat="server" Font-Bold="true" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label13" runat="server" Text="<%$ Resources: LocalizedResource, DateTimeFinished %>" />:
                            </td>
                            <td>
                                <asp:Label ID="lblDateFinishedVal" runat="server" Font-Bold="true" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label14" runat="server" Text="<%$ Resources: LocalizedResource, NoofQuestions %>" />:
                            </td>
                            <td>
                                <asp:Label ID="lblTotalValue" runat="server" Font-Bold="true" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label7" runat="server" Text="<%$ Resources: LocalizedResource, CorrectAnswers %>" />
                            </td>
                            <td>
                                <asp:Label ID="lblCorrectValue" runat="server" Font-Bold="true" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label8" runat="server" Text="<%$ Resources: LocalizedResource, PassingScore %>" />
                            </td>
                            <td>
                                <asp:Label ID="lblPassingValue" runat="server" Font-Bold="true" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label15" runat="server" Text="<%$ Resources: LocalizedResource, AttemptsUsed %>" />:
                            </td>
                            <td>
                                <asp:Label ID="lblTriesValue" runat="server" Font-Bold="true" />
                            </td>
                        </tr>
                        <%--ADDED BY RAYMARK COSME <11/19/2015>--%>
                        <%--  <tr>
                            <td>
                                <asp:Label ID="lblRate" runat="server" Text="Rate:" />
                            </td>
                            <td>
                                <asp:Label ID="lblRateValue" runat="server" Font-Bold="true" />
                            </td>
                        </tr>--%>
                        <%--ADDED BY RAYMARK COSME <11/19/2015>--%>
                        <%--        <tr>
                            <td>
                                <asp:Label ID="lblComments" runat="server" Text="Comments:" />
                            </td>
                            <td>
                                <asp:Label ID="lblCommentsValue" runat="server" Font-Bold="true" />
                            </td>
                        </tr>--%>
                    </table>
                </asp:Panel>
                <br />
                <asp:Panel ID="pnlThanks" runat="server" Font-Names="Arial" HorizontalAlign="left"
                    Width="100%" Visible="false">
                    <table cellpadding="10" cellspacing="0" width="100%">
                        <tr>
                            <td align="center">
                                <asp:Label ID="lblThanks" runat="server" CssClass="testQuestion" Text="<%$ Resources: LocalizedResource, ThankYouForTakingTheExam %>" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <%--ADDED BY RAYMARK COSME <11/19/2015>--%>
                <asp:Panel ID="pnlThanksSurvey" runat="server" Font-Names="Arial" HorizontalAlign="left"
                    Width="100%" Visible="false">
                    <table cellpadding="10" cellspacing="0" width="100%">
                        <tr>
                            <td align="center">
                                <asp:Label ID="Label1" runat="server" Font-Bold="true" Text="<%$ Resources: LocalizedResource, ThankYouForTakingTheTimeToProvideUsWithYourFeedback %>" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Label ID="Label2" runat="server" Font-Bold="true" Text="<%$ Resources: LocalizedResource, WeWillIncorporateYourSuggestionsIntoUpcomingSessions %>" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="pnlBackButton" runat="server" Font-Names="Arial" HorizontalAlign="left"
                    Width="100%">
                    <table cellpadding="10" cellspacing="0" width="100%">
                        <tr>
                            <td align="right">
                                <asp:Button ID="btnRetry" runat="server" Text="<%$ Resources: LocalizedResource, Retake %>"
                                    Width="80px" OnClick="btnRetry_Click" CssClass="btn tc-btn-md btn-teal btn-flat"
                                    Visible="False" />
                                <%--    <asp:Button ID="btnContinue" runat="server" Text="Return" Width="80px" OnClick="btnContinue_Click"
                                    CssClass="btn tc-btn-md btn-teal btn-flat" />--%>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <!-- message boxes -->
                <asp:HiddenField ID="TestTakenID" runat="server" />
                <asp:HiddenField ID="TestCategoryID" runat="server" />
            </div>
            <div class="col-md-12" style="width: -webkit-fill-available; margin-right: 50px;
                margin-top: 15px; margin-left: 50px; height: fit-content;">
                <br />
                <br />
                <asp:GridView ID="gvResults" runat="server" AutoGenerateColumns="false" Width="80%"
                    CellPadding="5" AllowPaging="false" AllowSorting="false" EmptyDataText="No items found."
                    BorderColor="black">
                    <Columns>
                        <asp:TemplateField HeaderText="<%$ Resources: LocalizedResource, Question %>" HeaderStyle-HorizontalAlign="center">
                            <ItemStyle Width="90%" />
                            <ItemTemplate>
                                <asp:Label ID="lblQuestionName" runat="server" Text='<%#Bind("Question") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="<%$ Resources: LocalizedResource, Correct %>" HeaderStyle-HorizontalAlign="center">
                            <ItemStyle Width="10%" HorizontalAlign="center" />
                            <ItemTemplate>
                                <asp:Image ID="imgTestStatus" runat="server" ImageUrl='<%#Bind("ImageUrl") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <br />
                <br />
                <br />
            </div>
        </asp:Panel>
    </asp:Panel>
</asp:Content>
