﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TranscomUniversityV3.Master" AutoEventWireup="true" CodeFile="Certificate.aspx.cs" Inherits="Certificate" %>
<%@ Register Src="UserControl/Pages/Achievements_Certificates.ascx" TagName="AchievementCertificates"
    TagPrefix="acf" %>
<asp:Content ID="Content1" ContentPlaceHolderID="contentPlaceHolderLeftPanel" runat="server">
    <%--<div class="col-sm-1 side-menu-container display-none">
        <div class="overlay">
        </div>
        <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
            <ul class="nav sidebar-nav">
                <li class="sidebar-brand" style="z-index: 1000; color: #fff;">
                    <a href="#" class="close"><i class="fa fa-times" aria-hidden="true"></i></a> My Profile
                </li>
                <rad:RadTabStrip ID="RadTabStrip1" runat="server" MultiPageID="RadMultiPage1" RenderMode="Lightweight" SelectedIndex="0" Skin="Black" AutoPostBack="true" OnClientTabSelected="OnClientTabSelected">
                    <Tabs>
                        <rad:RadTab runat="server" Text="My Profile Summary" Value="1" Width="285px" Selected="true" SelectedCssClass="rtsSelected"></rad:RadTab>
                        <rad:RadTab runat="server" Text="My Certifications" Value="2" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                        <rad:RadTab runat="server" Text="My Career Map" Value="3" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                    </Tabs>
                </rad:RadTabStrip>
            </ul>
        </nav>
        <div class="hamburger-container text-center" style="font-size: 30px; color: #fff;">
            <a href="#" class="open-nav is-closed animated fadeInLeft"><i class="fa fa-bars"
                aria-hidden="true"></i></a>
        </div>
    </div>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolderMain" runat="server">
    <div class="col-sm-11 col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12 thirty-px-padding">
                        <div class="col-md-12 no-paddings">
                            <ul id="ulBreadcrumb" runat="server" class="breadcrumb">
                                <li><a>
                                    <asp:Label ID="lblTab1" runat="server" Text="<%$ Resources:LocalizedResource, Learner %>" /></a></li>
                                <li><a>
                                    <asp:Label ID="lblTab2" runat="server" Text="<%$ Resources:LocalizedResource, MyProfile %>" /></a></li>
                                <li>
                                    <a><asp:Label ID="lblTab3" runat="server" Text="<%$ Resources:LocalizedResource, MyCertifications %>" /></a></li>
                                <li>
                                    <a><asp:Label ID="Label1" runat="server" Text="Internal Certification" /></a></li>
                               <li>
                                    <a><asp:Label ID="LblCourse" runat="server" Text="" /></a></li>
                                <li>
                                    <asp:Label ID="Label2" runat="server" Text="<%$ Resources:LocalizedResource, ViewCertificate %>" /></li>
                            </ul>
                            <div style="margin-top: 15px;">     
                                    <asp:Panel ID="pnlCert" runat="server" style="width: 100%; height: 500px;" class="container">                             
                                    <acf:AchievementCertificates ID="AchievementCerts" runat="server" width="100% !important" height="500px !important"/>  </asp:Panel>                   
                            <%--<iframe src="../RealCertificate.aspx?CertID=<% Response.Write(Request.QueryString["CertID"]); %>#zoom=50" width="100%" height="500" />--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>   
</asp:Content>
