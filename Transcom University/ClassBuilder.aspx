﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TranscomUniversityV3.Master" AutoEventWireup="true"
    CodeFile="ClassBuilder.aspx.cs" Inherits="ClassBuilder" ValidateRequest="false" %>

<%@ Register TagPrefix="ucas" TagName="AdminSidebar" Src="~/UserControl/Statics/AdminSiderbarUserCtrl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="contentPlaceHolderLeftPanel" runat="Server">
    <rad:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Transparency="25"
        IsSticky="true" CssClass="Loading" />
    <rad:RadAjaxLoadingPanel ID="localLoadingPanel" runat="server" MinDisplayTime="1000"
        CssClass="Loading2" Transparency="25" />
    <rad:RadAjaxLoadingPanel ID="noLoadingPanel" runat="server" CssClass="Loading-Empty"
        Transparency="25" />
    <rad:RadScriptBlock ID="RadScriptBlock" runat="server">
        <script type="text/javascript">
            var trainerCIMValid = false;
            function pageLoad() {
                highlightSelectedSubmenu();

                $("#txtTrainerCIM").blur(function (e) {
                    e.stopImmediatePropagation();
                    if ($("#txtTrainerCIM").val() != "") {
                        hideTrainerDetailLoading();
                        showTrainerDetailLoading();

                        setTrainerSupDetails();
                        //$("#<%= btnLoadTrainerDetails.ClientID %>").click();
                    } else {
                        trainerCIMValid = true;
                        $("#txtTrainerName").val("");
                        $("#txtSupName").val("");
                    }
                })

                $("#txtTrainerCIM").keypress(function (e) {
                    e.stopImmediatePropagation();
                    if (e.which == 13) {
                        $(this).blur();
                        //                        hideTrainerDetailLoading();
                        //                        showTrainerDetailLoading();

                        //                        setTrainerSupDetails();
                        //$("#<%= btnLoadTrainerDetails.ClientID %>").click();
                    }

                });

            }

            function setTrainerSupDetails() {
                trainerCIMValid = false;
                var trainerCIM = $("#txtTrainerCIM").val().trim();
                if (trainerCIM.length > 0) {
                    $.ajax({
                        type: "POST",
                        data: "{userCIM: " + trainerCIM + "}",
                        contentType: "application/json; charset=utf-8",
                        url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "GetSupervisorDetails",
                        dataType: 'json',
                        success: function (res) {

                            if (res.d == null) {

                                radalert("Trainer CIM is not valid or the user is not enrolled in TU.", 330, 180, "Error");
                                //$("#txtTrainerCIM").select();
                                $("#txtTrainerName").val("");
                                $("#txtSupName").val("");
                                //$("#txtTrainerCIM").focus();
                            }
                            else {
                                trainerCIMValid = true;
                                var trainername = res.d.Name;
                                var supname = res.d.SupervisorName;
                                $("#txtTrainerName").val(trainername);
                                $("#txtSupName").val(supname);
                            }

                            hideAllLocalLoading();



                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            var code = XMLHttpRequest.status;
                            if (code == "401")
                                window.location.reload();
                            hideAllLocalLoading();
                            //$("#txtTrainerCIM").focus();
                        }
                    });
                }

            }

            function OnClientBlurHandler(sender, eventArgs) {
                var textInTheCombo = sender.get_text();
                var item = sender.findItemByText(textInTheCombo);
                //if there is no item with that text
                if (!item) {
                    sender.set_text("");
                    setTimeout(function () {
                        var inputElement = sender.get_inputDomElement();
                        inputElement.focus();
                        var empty = sender.get_emptyMessage();
                        sender.set_text(empty);
                    }, 20);
                }
            }

            function showLoadingManager(lobid) {
                $("#<%= hfSelectedLob.ClientID %>").val(lobid);
                var showLoading = $find("<%= RadAjaxLoadingPanel1.ClientID %>");
                showLoading.show("lvCampaign");
            }

            function hideLoading() {
                $(".Loading").hide();
            }

            function redirectToClassManagement(classId) {


                console.log("redirect to class management");
                window.location.href = "ClassManagement.aspx?ClassId=" + classId;

            }

            function ConfirmCreateClass(sender, args) {

                trainerCIMValid = $("#txtTrainerCIM").val().trim() == "" ? true : trainerCIMValid;
                var callBackFunction = function (shouldSubmit) {
                    if (shouldSubmit) {
                        //initiate the original postback again
                        sender.click();
                        if (Telerik.Web.Browser.ff) { //work around a FireFox issue with form submission
                            sender.get_element().click();
                        }
                    }
                };

                //                var dateValid = Page_ClientValidate('dateValid');
                //                var minimumValidation = Page_ClientValidate('minimumValidation');
                //                var validated = Page_ClientValidate('addClass');

                if (Page_ClientValidate('addClass')) {

                    if (Page_ClientValidate('dateValid')) {

                        if (Page_ClientValidate('minimumValidation')) {
                            if (trainerCIMValid) {
                                var text = "Are you sure you want create this class?";
                                radconfirm(text, callBackFunction, 330, 180, null, "Confirm Create");
                            } else
                                radalert("Trainer CIM is not valid or the user is not enrolled in TU.", 330, 180, "Error");

                        }

                    }


                } else {
                    $('body,html').animate({
                        scrollTop: $("#txtNoOfSessions").offset().top - 65
                    }, 300);
                    return;
                }
                // && dateValid == true
                //&& dateValid == true
                //                if (validated == true && dateValid == true && minimumValidation == true) {
                //                    var text = "Are you sure you want create this class?";
                //                    radconfirm(text, callBackFunction, 330, 180, null, "Confirm Create");
                //                } else if (validated == false && (dateValid == true || minimumValidation == true)) {
                //                    //$("#txtNoOfSessions").focus();
                //                   
                //                    //window.scrollTop();

                //                    $('body,html').animate({
                //                        scrollTop: $("#txtNoOfSessions").offset().top - 65
                //                    }, 300);
                //                    return;
                //                }
                //                
                args.set_cancel(true);

            }

            function fileUploaded(sender, args) {
                //                alert("s");
                var upload = $find("<%= ClassImage.ClientID %>");
                var inputs = upload.getUploadedFiles();

                if (!upload.isExtensionValid(inputs[0]))
                    alert("extension is invalid!");

                var manager = $find("<%=rampCreateClass.ClientID %>");
                manager.ajaxRequest();

                setTimeout(function () {
                    hideAllLocalLoading();
                }, 3000);

            }

            function fileSelected(sender, args) {

                showImageDropzonePreviewLoading();
                var fileExtention = args.get_fileName().substring(args.get_fileName().lastIndexOf('.') + 1, args.get_fileName().length);
                if (args.get_fileName().lastIndexOf('.') != -1) {//this checks if the extension is correct
                    if (sender.get_allowedFileExtensions().toLowerCase().indexOf(fileExtention.toLowerCase()) == -1) {
                        $("#" + "<%= classImagePreview.ClientID%>").attr('src', "").hide();
                        removePreviewImage();
                        radalert("Invalid File Type.<br> Allowed Files are JPG, GIF, PNG.", 330, 180, "Error", "");
                    }
                    else {
                        console.log("File Supported.");
                    }
                }
                else {
                    console.log("not correct extension.");

                }

            }

            function showImageDropzonePreviewLoading() {
                var panel = $find("<%= localLoadingPanel.ClientID %>");
                panel.show("classImagePreview");
            }
            function hideAllLocalLoading() {
                $(".Loading2").hide();
                console.log("hide loading");
            }

            function dropzoneEnter() {

                if ($("#" + "<%= classImagePreview.ClientID %>").is('[src]')) {
                    $(".tc-overlay").css({ "display": "block", "opacity": "1" });
                }
            }
            function dropzoneExit() {
                var a = $("#" + "<%= classImagePreview.ClientID %>").attr('src');
                $(".tc-overlay").css({ "display": "none", "opacity": "0" });

            }

            function removePreviewImage() {
                try {
                    var upload = $find("<%= ClassImage.ClientID %>");
                    upload.deleteAllFileInputs();
                    hideAllLocalLoading();
                } catch (e) {

                }


            }

            function openFileUploader() {
                $telerik.$(".ruFileInput").click();
            }

//            function showDateLoading(sender, args) {
//                hideDateLoading();
//                console.log("date computing loading");
//                var panel = $find("<%= localLoadingPanel.ClientID %>");

//                panel.show("rdpStartDatePanel");
//                panel.show("rdpEndDatePanel");

//                var isStartDate = $(sender.get_element().parentElement).hasClass("js-startdate");
//                dateSelected(sender, args, isStartDate);

//            }
            function hideDateLoading() {
                var panel = $find("<%= localLoadingPanel.ClientID %>");
                panel.hide("rdpStartDatePanel");
                panel.hide("rdpEndDatePanel");

            }

            function showTrainerDetailLoading() {
                var panel = $find("<%= localLoadingPanel.ClientID %>");
                panel.show("pnlTrainerName");
                panel.show("pnlTrainerSup");

            }
            function hideTrainerDetailLoading() {
                var panel = $find("<%= localLoadingPanel.ClientID %>");
                panel.hide("pnlTrainerName");
                panel.hide("pnlTrainerSup");

            }
        </script>
        <%--additional for time picker--%>
        <script type="text/javascript">
            // using a variable to prevent infinite loop
            var isTimeSet = false;
            function timeSelecting(sender, args) {
                isTimeSet = true;
            }

            function startDateSelected(sender, args) {
                
                if (args.get_newDate() && !isTimeSet) {
                    isTimeSet = true;
                    $find("<%= rdpStartDate.ClientID %>").get_timeView().setTime(00, 0, 0, 0);
                    args.set_cancel(true);
                }
                if (isTimeSet) {
                    isTimeSet = false;
                }

                var StartDate = $find("<%= rdpStartDate.ClientID %>")
                var EndDate = $find("<%= rdpEndDate.ClientID %>");

                if (StartDate.get_selectedDate() == null || EndDate.get_selectedDate() == null) {
                    $("#txtNoTrainingDays").val("");
                    $("#txtTotalTrainingHours").val("");
                } else {
                    var totalWorkingDays = workingDaysBetweenDates(new Date(StartDate.get_selectedDate()), new Date(EndDate.get_selectedDate()));
                    var totalHours = totalWorkingDays * 8;
                    //workingHoursBetweenDates(startDate, endDate)
                    $("#txtNoTrainingDays").val(totalWorkingDays == 0 ? "" : totalWorkingDays);
                    $("#txtTotalTrainingHours").val(totalHours == 0 ? "" : totalHours);
                }

                ////////////
                var _EndDate = EndDate.get_selectedDate();
                var _StartDate = StartDate.get_selectedDate();
                _EndDate = _EndDate == null ? new Date(2099, 12, 30, 23, 0, 0) : _EndDate;
                _StartDate = new Date(_StartDate);
                _EndDate = new Date(_EndDate);
                var valid = (_EndDate > _StartDate);

                if (!valid) {
                    radalert("The Start date should be less than End Date", 330, 180, "Error");
                    // $find("<%= rdpStartDate.ClientID %>").get_textBox().focus();
                }
            }


            function EndDateSelected(sender, args) {

                if (args.get_newDate() && !isTimeSet) {
                    isTimeSet = true;
                    $find("<%= rdpEndDate.ClientID %>").get_timeView().setTime(23, 0, 0, 0);
                    args.set_cancel(true);
                }
                if (isTimeSet) {
                    isTimeSet = false;
                }
                var StartDate = $find("<%= rdpStartDate.ClientID %>")
                var EndDate = $find("<%= rdpEndDate.ClientID %>");

                if (StartDate.get_selectedDate() == null || EndDate.get_selectedDate() == null) {
                    $("#txtNoTrainingDays").val("");
                    $("#txtTotalTrainingHours").val("");
                } else {
                    var totalWorkingDays = workingDaysBetweenDates(new Date(StartDate.get_selectedDate()), new Date(EndDate.get_selectedDate()));
                    var totalHours = totalWorkingDays * 8;
                    //workingHoursBetweenDates(startDate, endDate)
                    $("#txtNoTrainingDays").val(totalWorkingDays == 0 ? "" : totalWorkingDays);
                    $("#txtTotalTrainingHours").val(totalHours == 0 ? "" : totalHours);
                }

                ////////////
                //                var _EndDate = EndDate.get_selectedDate();
                //                var _StartDate = StartDate.get_selectedDate();
                //                _EndDate = _EndDate == null ? new Date(2099, 12, 30, 23, 0, 0) : _EndDate;
                //                _StartDate = new Date(_StartDate);
                //                _EndDate = new Date(_EndDate);
                //                var valid = (_EndDate > _StartDate);

                //                if (!valid) {
                //                    radalert("The End date should be greated than Start Date", 330, 180, "Error");
                //                    //$find("<%= rdpEndDate.ClientID %>").get_textBox().focus();
                //                }
            }
            function valueChanging(sender, args) {
                if (args.get_newValue().indexOf(":") > 0) {
                    isTimeSet = true;
                }
            }

            //CHECK LENGTH GIVEN BY THE PARAMETER
            function CheckMaxLength(sender, Maxlength) {

                var length = $(sender).val().length;
                if (sender.value.length > Maxlength) {

                    sender.value = sender.value.substr(0, Maxlength);
                }

                var length = Maxlength - length;
                if (!$(sender).hasClass("MultiLineTextBox"))
                    $(sender).siblings(".js-char-counter").find("span").text(length);
                else
                    $(sender).parent().siblings(".js-char-counter").find("span").text(length);

            }

            function workingDaysBetweenDates(startDate, endDate) {

                // Validate input
                if (endDate < startDate)
                    return 0;

                // Calculate days between dates
                var millisecondsPerDay = 86400 * 1000; // Day in milliseconds
                startDate.setHours(0, 0, 0, 1);  // Start just after midnight
                endDate.setHours(23, 59, 59, 999);  // End just before midnight
                var diff = endDate - startDate;  // Milliseconds between datetime objects    
                var days = Math.ceil(diff / millisecondsPerDay);

                // Subtract two weekend days for every week in between
                var weeks = Math.floor(days / 7);
                days = days - (weeks * 2);

                // Handle special cases
                var startDay = startDate.getDay();
                var endDay = endDate.getDay();

                // Remove weekend not previously removed.   
                if (startDay - endDay > 1)
                    days = days - 2;

                // Remove start day if span starts on Sunday but ends before Saturday
                if (startDay == 0 && endDay != 6)
                    days = days - 1

                // Remove end day if span ends on Saturday but starts after Sunday
                if (endDay == 6 && startDay != 0)
                    days = days - 1

                return days;
            }

            function workingHoursBetweenDates(startDate, endDate) {

                // Validate input
                if (endDate < startDate)
                    return 0;

                // Calculate days between dates
                var millisecondsPerDay = 86400 * 1000; // Day in milliseconds
                var millisecondsPerHour = 3600 * 1000; // Day in milliseconds
                startDate.setHours(0, 0, 0, 1);  // Start just after midnight
                endDate.setHours(23, 59, 59, 999);  // End just before midnight
                var diff = endDate - startDate;  // Milliseconds between datetime objects    
                var days = Math.ceil(diff / millisecondsPerDay);
                var hours = Math.ceil(diff / millisecondsPerHour);

                // Subtract two weekend days for every week in between
                var weeks = Math.floor(hours / 168);
                hours = hours - (weeks * 48);

                // Handle special cases
                var startDay = startDate.getDay();
                var endDay = endDate.getDay();

                // Remove weekend not previously removed.   
                if (startDay - endDay > 1)
                    hours = hours - 48;

                // Remove start day if span starts on Sunday but ends before Saturday
                if (startDay == 0 && endDay != 6)
                    hours = hours - 24

                // Remove end day if span ends on Saturday but starts after Sunday
                if (endDay == 6 && startDay != 0)
                    hours = hours - 24

                //return hours - (workingDaysBetweenDates );
            }

            function validateStartEndDate(sender, args) {

                var startDate = $find("<%= rdpStartDate.ClientID %>").get_selectedDate();
                var endDate = $find("<%= rdpEndDate.ClientID %>").get_selectedDate();

                args.IsValid = false;
                if (startDate != null && endDate != null) {
                    endDate = endDate == null ? new Date(2099, 12, 30, 23, 0, 0) : endDate;
                    //                console.log(startDate);
                    //                console.log(endDate);
                    //                console.log(endDate > startDate);
                    var valid = (endDate > startDate);

                    args.IsValid = valid;
                    if ((!valid && !isTimeSet)) {
                        radalert("The End date should be greater than Start Date", 330, 180, "Error");
                        //                        $('body,html').animate({
                        //                            scrollTop: $("#txtNoOfSessions").offset().top - 65
                        //                        }, 100);


                        //$find("<%= rdpEndDate.ClientID %>").get_dateInput().focus();
                    }
                } else {
                    if (startDate == null)
                        $find("<%= rdpStartDate.ClientID %>").get_dateInput().focus();
                    else if (endDate == null)
                        $find("<%= rdpEndDate.ClientID %>").get_dateInput().focus();
                }
            }

            function filterNumberOnly(evt) {
                var theEvent = evt || window.event;

                // Handle paste
                if (theEvent.type === 'paste') {
                    key = event.clipboardData.getData('text/plain');
                } else {
                    // Handle key press
                    var key = theEvent.keyCode || theEvent.which;
                    key = String.fromCharCode(key);
                }
                var regex = /[0-9]/;
                if (!regex.test(key)) {
                    theEvent.returnValue = false;
                    if (theEvent.preventDefault) theEvent.preventDefault();
                }
            }


            function setInputFilter(textbox, inputFilter) {
                ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function (event) {
                    textbox.addEventListener(event, function () {
                        if (inputFilter(this.value)) {
                            this.oldValue = this.value;
                            this.oldSelectionStart = this.selectionStart;
                            this.oldSelectionEnd = this.selectionEnd;
                        } else if (this.hasOwnProperty("oldValue")) {
                            this.value = this.oldValue;
                            this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                        }
                    });
                });
            }

            function validateGreaterOne(sender, args) {
                debugger;
                try {
                    var numberInput = parseInt(args.Value.trim() == "" ? "0" : args.Value.trim());
                    var valid = numberInput >= 1
                    args.IsValid = valid;
                    if (!valid) {
                        $("#" + sender.controltovalidate).focus();
                    }
                } catch (e) {
                    alert("The input is not a valid number.");
                }


            }

            function DatePopupOpening(sender, args) {
                if (sender.get_selectedDate() != null)
                    isTimeSet = true;
                else
                    isTimeSet = false;

            }

        </script>
    </rad:RadScriptBlock>
    <rad:RadAjaxManager ID="rampCreateClass" runat="server">
        <AjaxSettings>
            <rad:AjaxSetting AjaxControlID="rcbSite">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="rcbSite" LoadingPanelID="noLoadingPanel" />
                    <rad:AjaxUpdatedControl ControlID="rcbVenue" LoadingPanelID="localLoadingPanel" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnCreateClass">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlCreateClass" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <%-- <rad:AjaxSetting AjaxControlID="rdpEndDate">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="txtNoTrainingDays" LoadingPanelID="noLoadingPanel" />
                    <rad:AjaxUpdatedControl ControlID="txtTotalTrainingHours" LoadingPanelID="noLoadingPanel" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="rdpStartDate">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="txtNoTrainingDays" LoadingPanelID="noLoadingPanel" />
                    <rad:AjaxUpdatedControl ControlID="txtTotalTrainingHours" LoadingPanelID="noLoadingPanel" />
                </UpdatedControls>
            </rad:AjaxSetting>--%>
            <rad:AjaxSetting AjaxControlID="rampCreateClass">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="ClassImage" LoadingPanelID="noLoadingPanel" />
                    <rad:AjaxUpdatedControl ControlID="classImagePreview" LoadingPanelID="localLoadingPanel" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <%--  <rad:AjaxSetting AjaxControlID="btnLoadTrainerDetails">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="txtSupName" LoadingPanelID="noLoadingPanel" />
                    <rad:AjaxUpdatedControl ControlID="txtTrainerName" LoadingPanelID="noLoadingPanel" />
                </UpdatedControls>
            </rad:AjaxSetting>--%>
        </AjaxSettings>
    </rad:RadAjaxManager>
    <rad:RadWindowManager ID="RadWindowManager1" RenderMode="Lightweight" EnableShadow="true"
        Skin="Bootstrap" Modal="true" VisibleOnPageLoad="false" Behaviors="Close, Move"
        RestrictionZoneID="RestrictionZone" Opacity="99" runat="server">
        <Windows>
        </Windows>
    </rad:RadWindowManager>
    <ucas:AdminSidebar ID="AdminSidebar" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolderMain" runat="Server">
    <div class="col-sm-11 col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12 thirty-px-padding">
                        <%-- BreadCrumb --%>
                        <div class="col-md-12 no-paddings">
                            <ul id="ulBreadcrumb" runat="server" class="breadcrumb">
                                <li><a>
                                    <asp:Label ID="lblTab1" runat="server" Text="Admin Dashboard" /></a></li>
                                <li><a>
                                    <asp:Label ID="lblTab2" runat="server" Text="Create Class" /></a></li>
                                <li id="item3" runat="server"><a>
                                    <asp:Label ID="lblTab3" runat="server" Text="Class Builder" ClientIDMode="Static" /></a></li>
                                <li id="item4" runat="server" visible="false"><a>
                                    <asp:Label ID="lblTab4" runat="server" Text="" /></a></li>
                            </ul>
                        </div>
                        <%-- Main Content --%>
                        <asp:Panel ID="pnlCreateClass" runat="server">
                            <br />
                            <br />
                            <div class="container createCourseContainer createClassContainer" style="margin-top: 1%">
                                <asp:Label ID="lblReqFields" runat="server" Tex="" ForeColor="Red" ClientIDMode="Static"></asp:Label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="font-bold">
                                            Program / Account Information
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="font-bold">
                                            Training Details
                                        </div>
                                    </div>
                                    <hr style="border-top: dotted 1px;" />
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-sm-12" style="font-weight: normal;">
                                        <div class="row">
                                            <div class="col-md-4">
                                                Program / Account
                                            </div>
                                            <div class="col-md-6 pull-left form-group no-padding">
                                                <rad:RadComboBox ID="rcbProgramAccount" runat="server" RenderMode="Lightweight" DataTextField="Client"
                                                    DataValueField="ClientID" EmptyMessage="-Select-" DataSourceID="dsProgramAccount"
                                                    EnableVirtualScrolling="true" ToolTip="Select Program" CssClass="width-fill"
                                                    MarkFirstMatch="true" AllowCustomText="false" EnableItemCaching="true" Height="250px"
                                                    AppendDataBoundItems="true">
                                                </rad:RadComboBox>
                                                <asp:SqlDataSource ID="dsProgramAccount" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection%>"
                                                    SelectCommand="select ClientID, Client from tbl_TranscomUniversity_Lkp_Client where HideFromList = 0">
                                                </asp:SqlDataSource>
                                            </div>
                                            <div class="col-md-1 pull-left no-padding">
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                                    ForeColor="Red" ControlToValidate="rcbProgramAccount" SetFocusOnError="True"
                                                    Display="Dynamic" CssClass="displayerror pull-left" ValidationGroup="addClass"
                                                    Text="*" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                Course
                                            </div>
                                            <div class="col-md-6 pull-left form-group no-padding">
                                                <rad:RadComboBox ID="rcbCourse" runat="server" DefaultValue="0" RenderMode="Lightweight"
                                                    DataValueField="ClassCourseID" EmptyMessage="- Select -" ToolTip="Select Class Course"
                                                    DataSourceID="dsClassCourse" CssClass="width-fill" DataTextField="ClassCourseName">
                                                </rad:RadComboBox>
                                                <asp:SqlDataSource ID="dsClassCourse" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection%>"
                                                    SelectCommand="SELECT ClassCourseID, ClassCourseName FROM tbl_TranscomUniversity_lkp_classCourse WHERE HideFromList = 0">
                                                </asp:SqlDataSource>
                                            </div>
                                            <div class="col-md-1 pull-left no-padding">
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                                                    ForeColor="Red" ControlToValidate="rcbCourse" SetFocusOnError="True" Display="Dynamic"
                                                    CssClass="displayerror pull-left" ValidationGroup="addClass" Text="*" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                Class
                                            </div>
                                            <div class="col-md-6 pull-left form-group no-padding">
                                                <rad:RadComboBox ID="rcbClass" runat="server" DefaultValue="0" RenderMode="Lightweight"
                                                    DataValueField="ClassID" EmptyMessage="- Select -" ToolTip="Select Class" DataSourceID="dsClasses"
                                                    CssClass="width-fill" DataTextField="ClassName">
                                                </rad:RadComboBox>
                                                <asp:SqlDataSource ID="dsClasses" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection%>"
                                                    SelectCommand="SELECT ClassID, ClassName FROM tbl_TranscomUniversity_lkp_classes WHERE HideFromList = 0">
                                                </asp:SqlDataSource>
                                            </div>
                                            <div class="col-md-1 pull-left no-padding">
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*"
                                                    ForeColor="Red" ControlToValidate="rcbClass" SetFocusOnError="True" Display="Dynamic"
                                                    CssClass="displayerror pull-left" ValidationGroup="addClass" Text="*" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                Audience
                                            </div>
                                            <div class="col-md-6 pull-left form-group no-padding">
                                                <rad:RadComboBox ID="rcbAudience" runat="server" DefaultValue="0" RenderMode="Lightweight"
                                                    DataSourceID="dsAudience" EmptyMessage="- Select -" ToolTip="Select Audience"
                                                    CssClass="width-fill" DataValueField="AudienceID" DataTextField="Audience">
                                                </rad:RadComboBox>
                                                <asp:SqlDataSource ID="dsAudience" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection%>"
                                                    SelectCommand="SELECT AudienceID,Audience FROM tbl_TranscomUniversity_lkp_audience WHERE HideFromList = 0">
                                                </asp:SqlDataSource>
                                            </div>
                                            <div class="col-md-1 pull-left no-padding">
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                                                    ForeColor="Red" ControlToValidate="rcbAudience" SetFocusOnError="True" Display="Dynamic"
                                                    CssClass="displayerror pull-left" ValidationGroup="addClass" Text="*" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                Site
                                            </div>
                                            <div class="col-md-6 pull-left form-group no-padding">
                                                <rad:RadComboBox ID="rcbSite" runat="server" DefaultValue="0" RenderMode="Lightweight"
                                                    DataSourceID="dsSite" DataValueField="CompanySiteID" DataTextField="CompanySite"
                                                    EmptyMessage="- Select -" AutoPostBack="true" OnSelectedIndexChanged="rcbSite_SelectedIndexChanged"
                                                    ToolTip="Select Site" CssClass="width-fill">
                                                </rad:RadComboBox>
                                                <asp:SqlDataSource ID="dsSite" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection%>"
                                                    SelectCommandType="StoredProcedure" SelectCommand="pr_TranscomUniversity_Lkp_Sites">
                                                    <SelectParameters>
                                                        <asp:Parameter Name="CompanySiteID" DefaultValue="0" DbType="Int32" />
                                                    </SelectParameters>
                                                </asp:SqlDataSource>
                                            </div>
                                            <div class="col-md-1 pull-left no-padding">
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                                                    ForeColor="Red" ControlToValidate="rcbSite" SetFocusOnError="True" Display="Dynamic"
                                                    CssClass="displayerror pull-left" ValidationGroup="addClass" Text="*" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                Venue
                                            </div>
                                            <div class="col-md-6 pull-left form-group no-padding">
                                                <rad:RadComboBox ID="rcbVenue" runat="server" DefaultValue="0" RenderMode="Lightweight"
                                                    DataValueField="VenueID" DataTextField="Venue" EmptyMessage="- Select -" ToolTip="Select Venue"
                                                    CssClass="width-fill">
                                                </rad:RadComboBox>
                                            </div>
                                            <div class="col-md-1 pull-left no-padding">
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="*"
                                                    ForeColor="Red" ControlToValidate="rcbVenue" SetFocusOnError="True" Display="Dynamic"
                                                    CssClass="displayerror pull-left" ValidationGroup="addClass" Text="*" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-sm-12" style="font-weight: normal;">
                                        <div class="row">
                                            <div class="col-md-6 col-md-offset-4 no-padding">
                                                <asp:CustomValidator ID="CustomValidator2" runat="server" ErrorMessage="Session should be greater than 0"
                                                    SetFocusOnError="true" Display="Dynamic" ForeColor="Red" ControlToValidate="txtNoOfSessions"
                                                    ClientValidationFunction="validateGreaterOne" ValidationGroup="minimumValidation"
                                                    ValidateEmptyText="true"></asp:CustomValidator>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                Total No. of Sessions
                                            </div>
                                            <div class="col-md-6 pull-left form-group no-padding">
                                                <asp:TextBox ID="txtNoOfSessions" runat="server" CssClass="form-control no-paddings js-number-filter"
                                                    Text="" ToolTip="Number of Sessions" ClientIDMode="Static" Style="padding: 0 0 0 5px;"
                                                    onkeypress='filterNumberOnly(event)' onpaste="filterNumberOnly(event)" placeholder="Session should be greater than 0"
                                                    TextMode="SingleLine" MaxLength="4"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                Start Date / Time
                                            </div>
                                            <%--OnSelectedDateChanged="rdpStartDate_SelectedDateChanged"--%>
                                            <div class="col-md-6 pull-left form-group no-padding" id="rdpStartDatePanel">
                                                <rad:RadDateTimePicker ID="rdpStartDate" runat="server" RenderMode="Lightweight"
                                                    ToolTip="Set Start Date" CssClass="left-calendar-icon width-fill js-startdate"
                                                    EnableTyping="false">
                                                    <%--<ClientEvents OnDateSelected="dateSelected" />--%>
                                                    <TimeView ID="TimeView1" runat="server" StartTime="00:00" EndTime="23:59" OnClientTimeSelecting="timeSelecting" />
                                                    <DateInput ID="DateInput1" runat="server" OnClientDateChanged="startDateSelected">
                                                    </DateInput>
                                                    <Calendar ID="Calendar2" runat="server">
                                                        <SpecialDays>
                                                            <rad:RadCalendarDay Repeatable="Today" ItemStyle-BackColor="Coral">
                                                            </rad:RadCalendarDay>
                                                        </SpecialDays>
                                                    </Calendar>
                                                    <ClientEvents OnPopupOpening="DatePopupOpening" />
                                                </rad:RadDateTimePicker>
                                            </div>
                                            <div class="col-md-1 pull-left no-padding">
                                                <%--  <asp:CustomValidator ID="cvDate" runat="server" ErrorMessage="Start Date should be less than End Date"
                                                SetFocusOnError="true" Display="Dynamic" ForeColor="Red" ControlToValidate="rdpStartDate"
                                                ClientValidationFunction="validateStartEndDate" ValidationGroup="addClass"></asp:CustomValidator>--%>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                                                    ForeColor="Red" ControlToValidate="rdpStartDate" SetFocusOnError="True" Display="Dynamic"
                                                    CssClass="displayerror pull-left" ValidationGroup="addClass" Text="*" />
                                            </div>
                                        </div>
                                        <%--<div class="row">
                                            <div class="col-md-6 col-md-offset-4 no-padding">
                                                <asp:CustomValidator ID="CustomValidator4" runat="server" ErrorMessage=""
                                                    SetFocusOnError="true" Display="Dynamic" ForeColor="Red" ControlToValidate="rdpStartDate"
                                                    ClientValidationFunction="validateStartEndDate" ValidationGroup="addClass"></asp:CustomValidator>
                                            </div>
                                        </div>--%>
                                        <div class="row">
                                            <div class="col-md-6 col-md-offset-4 no-padding">
                                                <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="" SetFocusOnError="true"
                                                    Display="Dynamic" ForeColor="Red" ControlToValidate="rdpEndDate" ClientValidationFunction="validateStartEndDate"
                                                    ValidationGroup="dateValid" ValidateEmptyText="true"></asp:CustomValidator>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                End Date / Time
                                            </div>
                                            <%--OnSelectedDateChanged="rdpEndDate_SelectedDateChanged"--%>
                                            <div class="col-md-6 pull-left form-group no-padding" id="rdpEndDatePanel">
                                                <rad:RadDateTimePicker ID="rdpEndDate" runat="server" RenderMode="Lightweight" ToolTip="Set End Date"
                                                    CssClass="left-calendar-icon width-fill" EnableTyping="false">
                                                    <%--<ClientEvents OnDateSelected="EndDateSelected" />--%>
                                                    <TimeView ID="TimeView2" runat="server" StartTime="00:00" EndTime="23:59" OnClientTimeSelecting="timeSelecting" />
                                                    <%--<DateInput ID="DateInput2" runat="server">--%>
                                                    <DateInput ID="DateInput2" runat="server" OnClientDateChanged="EndDateSelected">
                                                    </DateInput>
                                                    <Calendar ID="Calendar1" runat="server">
                                                        <SpecialDays>
                                                            <rad:RadCalendarDay Repeatable="Today" ItemStyle-BackColor="Coral">
                                                            </rad:RadCalendarDay>
                                                        </SpecialDays>
                                                    </Calendar>
                                                    <ClientEvents OnPopupOpening="DatePopupOpening" />
                                                </rad:RadDateTimePicker>
                                            </div>
                                            <div class="col-md-1 pull-left no-padding">
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="*"
                                                    ForeColor="Red" ControlToValidate="rdpEndDate" SetFocusOnError="True" Display="Dynamic"
                                                    CssClass="displayerror pull-left" ValidationGroup="addClass" Text="*" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 col-md-offset-4 no-padding">
                                                <asp:CustomValidator ID="CustomValidator3" runat="server" ErrorMessage="Starting head count should be greater than 0"
                                                    SetFocusOnError="true" Display="Dynamic" ForeColor="Red" ControlToValidate="txtHeadCount"
                                                    ClientValidationFunction="validateGreaterOne" ValidationGroup="minimumValidation"
                                                    ValidateEmptyText="true"></asp:CustomValidator>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                Starting Headcount
                                            </div>
                                            <div class="col-md-6 pull-left form-group no-padding">
                                                <asp:TextBox ID="txtHeadCount" runat="server" CssClass="form-control no-paddings"
                                                    Text="" ToolTip="Starting Headcount" ClientIDMode="Static" Style="padding: 0 0 0 5px;"
                                                    onkeypress='filterNumberOnly(event)' onpaste="filterNumberOnly(event)" placeholder="Starting head count should be greater than 0"
                                                    TextMode="SingleLine" MaxLength="4"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                No. of Training Days
                                            </div>
                                            <div class="col-md-6 pull-left form-group no-padding">
                                                <asp:TextBox ID="txtNoTrainingDays" runat="server" CssClass="form-control no-paddings"
                                                    ToolTip="No. of Training Days" ClientIDMode="Static" Style="padding: 0 0 0 5px;"
                                                    placeholder="Select a valid Start date and End Date To Fill this" ReadOnly></asp:TextBox>
                                            </div>
                                            <div class="col-md-1 pull-left no-padding">
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="*"
                                                    ForeColor="Red" ControlToValidate="txtNoTrainingDays" Display="Dynamic" CssClass="displayerror pull-left"
                                                    ValidationGroup="addClass" Text="*" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                Total Training Hours
                                            </div>
                                            <div class="col-md-6 pull-left form-group no-padding">
                                                <asp:TextBox ID="txtTotalTrainingHours" runat="server" CssClass="form-control no-paddings"
                                                    ToolTip="Total Training Hours" ClientIDMode="Static" Style="padding: 0 0 0 5px;"
                                                    placeholder="Select a valid Start date and End Date To Fill this" ReadOnly></asp:TextBox>
                                            </div>
                                            <div class="col-md-1 pull-left no-padding">
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="*"
                                                    ForeColor="Red" ControlToValidate="txtTotalTrainingHours" Display="Dynamic" CssClass="displayerror pull-left"
                                                    ValidationGroup="addClass" Text="*" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="font-bold">
                                            Class Name and Description
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="font-bold">
                                            Trainer Details
                                        </div>
                                    </div>
                                    <hr style="border-top: dotted 1px;" />
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-sm-12" style="font-weight: normal;">
                                        <div class="row">
                                            <div class="col-md-4">
                                                Class Name
                                            </div>
                                            <div class="col-md-6 pull-left form-group no-padding">
                                                <asp:TextBox ID="txtClassName" runat="server" CssClass="form-control no-paddings"
                                                    MaxLength="100" onkeyup="CheckMaxLength(this,100);" ToolTip="Class Name" ViewStateMode="Enabled"
                                                    ClientIDMode="Static" ondragstart="return false;" ondrop="return false;" placeholder="Enter the Class Name"></asp:TextBox>
                                                <label class="text-danger text-right js-char-counter">
                                                    <span id="txtClassNameChars">100</span> characters remaining</label>
                                            </div>
                                            <div class="col-md-1 pull-left no-padding">
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="*"
                                                    ForeColor="Red" ControlToValidate="txtClassName" SetFocusOnError="True" Display="Dynamic"
                                                    CssClass="displayerror pull-left" ValidationGroup="addClass" Text="*" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                Class Description
                                            </div>
                                            <div class="col-md-6 pull-left form-group no-padding">
                                                <%--   <asp:TextBox ID="txtClassDesc" runat="server" CssClass="form-control no-paddings MultiLineTextBox"
                                                    ToolTip="Class Description" ViewStateMode="Enabled" Height="120px" TextMode="MultiLine"
                                                    ClientIDMode="Static" placeholder="What is this class about?" MaxLength="1500"
                                                    onkeyup="CheckMaxLength(this,1500);"></asp:TextBox>--%>
                                                <rad:RadTextBox ID="txtClassDesc" TextMode="MultiLine" runat="server" CssClass="form-control MultiLineTextBox btn-flat"
                                                    RenderMode="Lightweight" Skin="Bootstrap" ViewStateMode="Enabled" Height="120px"
                                                    Width="100%" MaxLength="1500" ClientIDMode="Static" placeholder="What is this class about?"
                                                    onkeyup="CheckMaxLength(this,1500);">
                                                </rad:RadTextBox>
                                                <label class="text-danger text-right text-right js-char-counter">
                                                    <span id="Span1">1500</span> characters remaining</label>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="col-md-6 col-md-offset-4">
                                            <div class="row img-dropzone-md" style="margin: 0; width: 100% !important;">
                                                <div class="dropzone-empty" style="padding-top: 2em;">
                                                    <asp:Image ID="Image4" ImageUrl="~/Media/Images/icon-upload.png" runat="server" AlternateText="Upload Image"
                                                        Height="2.875em" Widt h="2.875em" />
                                                    <br />
                                                    <p>
                                                        Drag image file here
                                                    </p>
                                                </div>
                                                <rad:RadAsyncUpload ID="ClassImage" runat="server" MultipleFileSelection="Disabled"
                                                    ViewStateMode="Enabled" MaxFileInputsCount="1" ControlObjectsVisibility="None"
                                                    Width="100%" HideFileInput="false" ToolTip="Select Class Image" CssClass="ClassImage display-none"
                                                    DropZones=".img-dropzone-md" ForeColor="#ddd" OnClientFilesUploaded="fileUploaded"
                                                    OnClientFileSelected="fileSelected" OnFileUploaded="upload_FileUploaded" AllowedFileExtensions="jpeg,jpg,gif,png">
                                                    <%-- <FileFilters>
                                                                <rad:FileFilter Description="Images(jpeg;jpg;gif;png)" Extensions="jpeg,jpg,gif,png" />
                                                            </FileFilters>--%>
                                                </rad:RadAsyncUpload>
                                                <div id="DropzonePreview" class="dropzone-preview" onmouseover="dropzoneEnter()"
                                                    onmouseout="dropzoneExit()" runat="server">
                                                    <rad:RadBinaryImage ID="classImagePreview" runat="server" CssClass="box-fluid" ClientIDMode="Static" />
                                                    <div class="box-fluid tc-overlay">
                                                        <asp:LinkButton ID="btnRemovePreviewImg" runat="server" ToolTip="Remove Image" OnClick="btnRemovePreviewImg_Click"
                                                            OnClientClick="showImageDropzonePreviewLoading();">
                                                                    <i class="fa fa-times-circle-o previewClose" style="color: white; position: absolute;
                                                                    top: 50%; left: 50%; transform: translate(-50%,-50%); font-size: 5em; cursor: pointer;">
                                                                    </i>
                                                        </asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                            <br />
                                            <asp:Button ID="Button1" Text="Select File" runat="server" CssClass="btn btn-default border-lightgray btn-sm btn-flat btn-thin relative-hr-center "
                                                OnClientClick="openFileUploader(); return false;" />
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-sm-12" style="font-weight: normal;">
                                        <div class="row">
                                            <div class="col-md-4">
                                                CIM
                                            </div>
                                            <div class="col-md-6 pull-left form-group no-padding">
                                                <asp:TextBox ID="txtTrainerCIM" runat="server" CssClass="form-control no-paddings"
                                                    TextMode="SingleLine" MaxLength="10" ToolTip="Trainer CIM" ViewStateMode="Enabled"
                                                    ClientIDMode="Static" Style="padding: 0 0 0 5px;" placeholder="Enter the Trainer CIM"
                                                    onkeypress='filterNumberOnly(event)'></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                Name
                                            </div>
                                            <div class="col-md-6 pull-left form-group no-padding" id="pnlTrainerName">
                                                <asp:TextBox ID="txtTrainerName" runat="server" CssClass="form-control no-paddings"
                                                    ReadOnly="true" Enabled="false" ToolTip="Trainer Name" ViewStateMode="Enabled"
                                                    ClientIDMode="Static" Style="padding: 0 0 0 5px;"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                Trainer Supervisor Name
                                            </div>
                                            <div class="col-md-6 pull-left form-group no-padding" id="pnlTrainerSup">
                                                <asp:TextBox ID="txtSupName" runat="server" CssClass="form-control no-paddings" ReadOnly="true"
                                                    Enabled="false" ToolTip="Training Supervisor Name" ViewStateMode="Enabled" ClientIDMode="Static"
                                                    Style="padding: 0 0 0 5px;"></asp:TextBox>
                                            </div>
                                        </div>
                                        <asp:Button ID="btnLoadTrainerDetails" runat="server" CssClass="display-none" OnClick="btnLoadTrainerDetails_Click" />
                                    </div>
                                </div>
                                <br />
                                <br />
                            </div>
                            <br />
                            <br />
                            <div class="row text-center">
                                <rad:RadButton ID="btnCreateClass" runat="server" ToolTip="Create Class" ValidationGroup="addClass"
                                    CausesValidation="true" OnClientClicking="ConfirmCreateClass" OnClick="btnCreateClass_Click"
                                    CssClass="btn btn-sm btn-teal" Text="Create Class" ButtonType="SkinnedButton"
                                    RenderMode="Lightweight" Skin="Bootstrap">
                                </rad:RadButton>
                            </div>
                            <br />
                            <br />
                        </asp:Panel>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hfSelectedLob" runat="server" />
</asp:Content>
