﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using TranscomUniversityV3Model;
using Telerik.Web.UI.Calendar;
using System.IO;
using System.Configuration;
using System.Data.SqlClient;

public partial class ClassBuilder : BasePage
{
    private static string campaignName;

    private static int CLASSID;

    private static string global_classImage;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CLASSID = lastInsertedClassID() + "" == null ? 1 : lastInsertedClassID() + 1;
            global_classImage = null;
            deleteImageInServer(CLASSID);
        }


    }

    protected void rcbSite_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        try
        {
            int siteId = Convert.ToInt32(rcbSite.SelectedValue);

            var db = new TranscomUniversityV3ModelContainer();
            var venue = db.tbl_TranscomUniversity_Lkp_Venue
                            .Where(v => v.SiteID == siteId)
                            .ToList();

            rcbVenue.DataSource = venue;
            rcbVenue.DataBind();
        }
        catch (Exception)
        {
            RadWindowManager1.RadAlert("Invalid input. Please try again", 330, 180, "Error Message", "");
            throw;
        }
    }

    protected void rdpStartDate_SelectedDateChanged(object sender, SelectedDateChangedEventArgs e)
    {
        DateTime firstDay = Convert.ToDateTime(rdpStartDate.SelectedDate);
        DateTime lastDay = Convert.ToDateTime(rdpEndDate.SelectedDate);

        if (firstDay > lastDay)
        {
            txtNoTrainingDays.Text = "";
            txtTotalTrainingHours.Text = "";


            if (rdpEndDate.SelectedDate != null)
                RadWindowManager1.RadAlert("End Date should be greater than Start Date", 330, 180, "Error Date", "");
        }
        else
        {
            double trainingDays = Math.Round(1 + ((lastDay - firstDay).TotalDays * 5 - (firstDay.DayOfWeek - lastDay.DayOfWeek) * 2) / 7);
            int trainingHours = Convert.ToInt32(trainingDays) * 8;

            txtNoTrainingDays.Text = trainingDays.ToString();
            txtTotalTrainingHours.Text = trainingHours.ToString();
        }
        ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", Utils.callClientScript("hideDateLoading"), true);
    }

    protected void rdpEndDate_SelectedDateChanged(object sender, SelectedDateChangedEventArgs e)
    {
        DateTime firstDay = Convert.ToDateTime(rdpStartDate.SelectedDate);
        DateTime lastDay = Convert.ToDateTime(rdpEndDate.SelectedDate);

        if (firstDay > lastDay)
        {
            txtNoTrainingDays.Text = "";
            txtTotalTrainingHours.Text = "";


            if (rdpStartDate.SelectedDate != null)
                RadWindowManager1.RadAlert("End Date should be greater than Start Date", 330, 180, "Error Date", "");

        }
        else
        {
            double trainingDays = Math.Round(1 + ((lastDay - firstDay).TotalDays * 5 - (firstDay.DayOfWeek - lastDay.DayOfWeek) * 2) / 7);
            int trainingHours = Convert.ToInt32(trainingDays) * 8;

            txtNoTrainingDays.Text = trainingDays.ToString();
            txtTotalTrainingHours.Text = trainingHours.ToString();
        }
        ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", Utils.callClientScript("hideDateLoading"), true);

    }

    protected void btnCreateClass_Click(object sender, EventArgs e)
    {
        if (rcbProgramAccount.SelectedValue == "" || rcbSite.SelectedValue == "" || rdpStartDate.SelectedDate == null || rdpEndDate.SelectedDate == null)
        {
            lblReqFields.Text = "* Required fields";
            rcbProgramAccount.Focus();
            RadWindowManager1.RadAlert("Please fill all required fields", 330, 180, "Error Message", "");
        }
        else
        {
            try
            {
                DateTime firstDay = Convert.ToDateTime(rdpStartDate.SelectedDate);
                DateTime lastDay = Convert.ToDateTime(rdpEndDate.SelectedDate);

                int trainingDays = BusinessDaysUntil(firstDay, lastDay);
                int trainingHours = Convert.ToInt32(trainingDays) * 8;

                string classImageUrl = global_classImage;

                var db = new TranscomUniversityV3ModelContainer();

                //Logger variables Start
                string _userIP = DataHelper.GetIPAddress();
                string _userID = DataHelper.GetCurrentUserCIM();
                double _duration = 0;

                DateTime startTime;
                DateTime endTime;

                var dts = db.CreateQuery<DateTime>("CurrentDateTime() ");
                startTime = dts.AsEnumerable().First();
                //Logger variables End
                int? trainerCIM;
                if (txtTrainerCIM.Text.Trim() == "")
                    trainerCIM = null;
                else if (DataHelper.GetSupervisorDetails(Convert.ToInt32(txtTrainerCIM.Text.Trim())) == null)
                {
                    RadWindowManager1.RadAlert("Trainer CIM is not Valid.", 330, 180, "Invalid Trainer CIM", "");
                    return;
                }
                else
                    trainerCIM = Convert.ToInt32(txtTrainerCIM.Text.Trim());



                pr_TranscomUniversity_InsertClass_Result res = db.pr_TranscomUniversity_InsertClass(
                    Convert.ToInt32(rcbProgramAccount.SelectedValue),
                    txtClassName.Text.Trim(),
                     txtClassDesc.Text.Trim(),
                        trainerCIM,
                     Convert.ToInt32(rcbCourse.SelectedValue),
                     Convert.ToInt32(rcbClass.SelectedValue),
                     Convert.ToInt32(rcbAudience.SelectedValue),
                     Convert.ToInt32(rcbSite.SelectedValue),
                     Convert.ToInt32(rcbVenue.SelectedValue),
                     Convert.ToInt32(txtNoOfSessions.Text.Trim()),
                     rdpStartDate.SelectedDate,
                     rdpEndDate.SelectedDate,
                     Convert.ToInt32(txtHeadCount.Text.Trim()),
                     trainingDays,
                     trainingHours,
                     classImageUrl
                     ).SingleOrDefault();

                string classId = Utils.Encrypt((int)res.ClassID);

                //logger start transaction
                string _action = "Class was created by " + _userID;

                var dte = db.CreateQuery<DateTime>("CurrentDateTime() ");

                endTime = dte.AsEnumerable().First();
                _duration = (endTime - startTime).TotalMilliseconds;

                int l_actionID = DataHelper.getLastLogID() + 1;
                DataHelper.logClass(CLASSID, l_actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);
                //logger end transaction


                RadWindowManager1.RadAlert("Class was successfully created.", 330, 180, "Success", "redirectToClassManagement('" + classId + "')");
            }
            catch (Exception)
            {
                RadWindowManager1.RadAlert("Error creating class. Please contact your System Administrator.", 330, 180, "Error", "");
                throw;
            }
        }
    }

    protected string imageClass(int classId, UploadedFile f)
    {

        string error = null;

        if (string.IsNullOrWhiteSpace(error))
        {
            string uploadFolder;
            var userCim = DataHelper.GetCurrentUserCIM();

            if (HttpContext.Current.Request.Url.Host == "localhost")
                uploadFolder = Request.PhysicalApplicationPath + "Media\\Uploads\\ClassImg\\" + classId + "\\";
            else
                uploadFolder = HttpRuntime.AppDomainAppPath + "Media\\Uploads\\ClassImg\\" + classId + "\\";

            string directoryPath = Server.MapPath(string.Format("~/{0}/", "Media/Uploads/ClassImg/" + classId + "\\"));

            if (!Directory.Exists(directoryPath))
                Directory.CreateDirectory(directoryPath);

            if (f.ContentType == "image/jpeg" || f.ContentType == "image/png" || f.ContentType == "image/jpg" || f.ContentType == "image/gif")
            {
                if (f.ContentLength < 1024000000)
                {
                    Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                    string extension = Path.GetExtension(f.FileName);
                    f.SaveAs(uploadFolder + f.FileName, true);
                    //f.SaveAs(uploadFolder + unixTimestamp.ToString() + extension, true);
                    //global_classImage = unixTimestamp.ToString() + extension;
                    global_classImage = f.FileName;
                }
                else
                {
                    global_classImage = "error";
                    RadWindowManager1.RadAlert("  File size is too large, please select another one!", 330, 180, "Image too Large", "");
                }
            }
            else
            {
                global_classImage = "error";
                RadWindowManager1.RadAlert("Error file format please select another one!", 330, 180, "Invalid File Format", "");
            }
        }

        return global_classImage;
    }

    private static bool IsImage(HttpPostedFile file)
    {
        return ((file != null) && System.Text.RegularExpressions.Regex.IsMatch(file.ContentType, "image/\\S+") && (file.ContentLength > 0));
    }

    protected void upload_FileUploaded(object sender, FileUploadedEventArgs e)
    {

        if (e.IsValid)
        {
            imageClass(CLASSID, e.File);
            classImagePreview.ImageUrl = "Media/Uploads/ClassImg/" + CLASSID + "/" + global_classImage;
        }
        else
        {
            ClassImage.UploadedFiles.Clear();
            RadWindowManager1.RadAlert("Invalid File. Allowed Files (jpeg,jpg,gif,png)", null, null, "Error Message", "");

        }

    }

    protected void btnRemovePreviewImg_Click(object sender, EventArgs e)
    {

        //courseImagePreview.DataValue = null;
        global_classImage = "No_image.jpg";
        classImagePreview.ImageUrl = "";
        deleteImageInServer(CLASSID);
        ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", Utils.callClientScript("hideAllLocalLoading"), true);

    }

    private void deleteImageInServer(int courseID)
    {
        try
        {
            var folderPath = Server.MapPath("Media/Uploads/ClassImg/" + courseID + "\\");
            System.IO.DirectoryInfo folderInfo = new DirectoryInfo(folderPath);

            foreach (FileInfo file in folderInfo.GetFiles())
            {
                file.Delete();
            }
        }
        catch { }

    }

    private int lastInsertedClassID()
    {
        string constr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        string getQuery = "SELECT MAX(ClassId) AS classId FROM [dbo].[tbl_TranscomUniversity_Cor_Classes]";
        int lastClassID;

        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand(getQuery))
            {
                try
                {
                    cmd.Connection = con;
                    con.Open();
                    lastClassID = Convert.ToInt32(cmd.ExecuteScalar());
                    con.Close();
                }
                catch (Exception)
                {
                    lastClassID = 0;
                }
            }
        }

        return lastClassID;
    }

    private static int BusinessDaysUntil(DateTime firstDay, DateTime lastDay)
    {
        firstDay = firstDay.Date;
        lastDay = lastDay.Date;
        if (firstDay > lastDay)
            throw new ArgumentException("Incorrect last day " + lastDay);

        TimeSpan span = lastDay - firstDay;
        int businessDays = span.Days + 1;
        int fullWeekCount = businessDays / 7;
        // find out if there are weekends during the time exceedng the full weeks
        if (businessDays > fullWeekCount * 7)
        {
            // we are here to find out if there is a 1-day or 2-days weekend
            // in the time interval remaining after subtracting the complete weeks
            int firstDayOfWeek = firstDay.DayOfWeek == DayOfWeek.Sunday ? 7 : (int)firstDay.DayOfWeek;
            int lastDayOfWeek = lastDay.DayOfWeek == DayOfWeek.Sunday ? 7 : (int)lastDay.DayOfWeek;
            if (lastDayOfWeek < firstDayOfWeek)
                lastDayOfWeek += 7;
            if (firstDayOfWeek <= 6)
            {
                if (lastDayOfWeek >= 7)// Both Saturday and Sunday are in the remaining time interval
                    businessDays -= 2;
                else if (lastDayOfWeek >= 6)// Only Saturday is in the remaining time interval
                    businessDays -= 1;
            }
            else if (firstDayOfWeek <= 7 && lastDayOfWeek >= 7)// Only Sunday is in the remaining time interval
                businessDays -= 1;
        }

        // subtract the weekends during the full weeks in the interval
        businessDays -= fullWeekCount + fullWeekCount;

        //// subtract the number of bank holidays during the time interval
        //foreach (DateTime bankHoliday in bankHolidays)
        //{
        //    DateTime bh = bankHoliday.Date;
        //    if (firstDay <= bh && bh <= lastDay)
        //        --businessDays;
        //}

        return businessDays;
    }

    protected void btnLoadTrainerDetails_Click(object sender, EventArgs e)
    {
        try
        {
            int trainerCIM = Convert.ToInt32(txtTrainerCIM.Text);
            var d = DataHelper.GetSupervisorDetails(trainerCIM);
            txtTrainerName.Text = d == null ? "" : d.Name;
            txtSupName.Text = d == null ? "" : d.SupervisorName;

        }
        catch
        {
            txtTrainerName.Text = "";
            txtSupName.Text = "";
        }

        ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", Utils.callClientScript("hideTrainerDetailLoading"), true);
    }
}