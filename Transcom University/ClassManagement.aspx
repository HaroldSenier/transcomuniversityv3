﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TranscomUniversityV3.Master" AutoEventWireup="true"
    CodeFile="ClassManagement.aspx.cs" Inherits="ClassManagement" %>

<%@ Register TagPrefix="ucas" TagName="AdminSidebar" Src="~/UserControl/Statics/AdminSiderbarUserCtrl.ascx" %>
<%@ Register TagPrefix="ucts" TagName="TrainerSidebar" Src="~/UserControl/Statics/TrainerSidebarUserCtrl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="contentPlaceHolderLeftPanel" runat="Server">
    <style>
        .RadGrid .rgRow td, .RadGrid .rgAltRow td
        {
            white-space: nowrap;
        }
    </style>
    <rad:RadAjaxLoadingPanel ID="fullPageLoading" runat="server" Transparency="25" IsSticky="true"
        CssClass="Loading" />
    <rad:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Transparency="25"
        IsSticky="true" CssClass="Loading" />
    <rad:RadAjaxLoadingPanel ID="localLoading" runat="server" CssClass="Loading2" Transparency="25" />
    <rad:RadAjaxLoadingPanel ID="noLoadingPanel" runat="server" CssClass="Loading-Empty" />
    <rad:RadAjaxManager ID="RadAjaxManager1_cm" runat="server">
        <AjaxSettings>
            <rad:AjaxSetting AjaxControlID="RadTabStrip1">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlTabsWrapper" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <%-- <rad:AjaxSetting AjaxControlID="lvClassType">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="lvClassType" LoadingPanelID="noLoadingPanel" />
                </UpdatedControls>
            </rad:AjaxSetting>--%>
            <rad:AjaxSetting AjaxControlID="rcbTrainingType">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlClassListContainer" LoadingPanelID="localLoading" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="rcbStatus">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlClassListContainer" LoadingPanelID="localLoading" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="RadAjaxManager1_cm">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="ClassImage" />
                    <rad:AjaxUpdatedControl ControlID="classImagePreview" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="gridClasses">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="gridClasses" LoadingPanelID="localLoading" />
                </UpdatedControls>
            </rad:AjaxSetting>
        </AjaxSettings>
    </rad:RadAjaxManager>
    <rad:RadScriptBlock ID="RadScriptBlock_ClassBuilder" runat="server">
        <script type="text/javascript">
            var selectedTab;
            function pageLoad() {
                //resizeParticipant();
                //resizeTrainerGrid();
                if ('<%= Request.QueryString["ClassId"] %>')
                    initializeTrainerDetailLoader();

                resizeGridClasses();
            }

            function onTabSelecting(sender, args) {
                selectedTab = args.get_tab().get_text().trim();
                if (args.get_tab().get_pageViewID()) {
                    args.get_tab().set_postBack(false);
                    if (selectedTab == "PARTICIPANTS")
                        showParticipantGrid();
                    else if (selectedTab == "TRAINER")
                        showTrainerGrid();
                } else {
                    showPageLoading();
                }

            }

            function redirectToClass(classid) {
                window.location.href = "ClassManagement.aspx?ClassId=" + classid
            }


            function showGridLoading(el) {
                var panel = $find("<%= localLoading.ClientID %>");
                var id = $(el).attr("id");
                var grid = $("#" + id).parent().parent().parent().parent().parent().attr("id");
                panel.show(grid);
            }

            function hideGridLoading(el) {
                var panel = $find("<%= localLoading.ClientID %>");
                panel.hide(el);
            }

            function btnViewAll_Click(el) {
                $(el).parent().parent().parent().parent().parent().find(".js_viewall").click();
            }

            function refreshClassPreview(classname, classdesc, classimage) {
                debugger;
                $("#lblClassName").html(classname);
                $("#<%= lblClassDesc.ClientID %>").html(classdesc);
                $("#<%= lblClassTitle.ClientID %>").html(classname);
                $("#<%= imagePreviewContainer.ClientID %>").css({ "background-image": 'url("' + classimage + '")' });
                //radalert("Class was successfully updated.", 330, 180, "Success Message", "");
            }

            function showPageLoading() {
                console.log("page loading");
                var panel = $find("<%= fullPageLoading.ClientID %>");
                panel.show("ulBreadcrumb");
            }

            function hidePageLoading() {
                //console.log("hide page loading");
                var panel = $find("<%= fullPageLoading.ClientID %>");
                panel.hide("ulBreadcrumb");
                setTimeout(function () {
                    if (selectedTab == "PARTICIPANTS")
                        resizeParticipantGrid();
                    else if (selectedTab == "TRAINER")
                        resizeTrainerGrid();

                }, 100);
            }
            

        </script>
    </rad:RadScriptBlock>
    <rad:RadScriptBlock ID="rsbcm" runat="server">
        <script type="text/javascript">

            function resizeGridClasses() {
                if ($("#<%= gridClasses.ClientID %>").length > 0) {
                    var grid = $find("<%= gridClasses.ClientID %>");

                    //showParticipantGrid();
                    var columns = grid.get_masterTableView().get_columns();
                    for (var i = 0; i < columns.length; i++) {
                        columns[i].resizeToFit();
                    }
                }


            }

        </script>
    </rad:RadScriptBlock>
    <ucas:AdminSidebar ID="AdminSidebar" runat="server" Visible="false" />
    <ucts:TrainerSidebar ID="TrainerSidebar1" runat="server" Visible="false" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolderMain" runat="Server">
    <div class="col-sm-11 col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12 thirty-px-padding">
                        <div class="col-md-12 no-paddings" style="z-index: 1;">
                            <ul id="ulBreadcrumb" runat="server" class="breadcrumb" clientidmode="Static">
                                <li><a>
                                    <asp:Label ID="lblTab1" runat="server" Text="<%$ Resources:LocalizedResource, AdminDashboard%>" /></a></li>
                                <li><a>
                                    <asp:Label ID="lblTab2" runat="server" Text="<%$ Resources:LocalizedResource, ClassManagement%>" /></a></li>
                                <li id="item3" runat="server" visible="false"><a>
                                    <asp:Label ID="lblTab3" runat="server" Text="" ClientIDMode="Static" /></a></li>
                                <li id="item4" runat="server" visible="false"><a>
                                    <asp:Label ID="lblTab4" runat="server" Text="" /></a></li>
                            </ul>
                        </div>
                        <asp:Panel ID="pnlClassManagement" runat="server">
                            <div class="container" style="padding-top: 50px;">
                                <rad:RadComboBox ID="rcbStatus" Label="Status:&nbsp;&nbsp;" runat="server" RenderMode="Lightweight"
                                    EmptyMessage="Select Status" ToolTip="Select Status" AutoPostBack="true" EnableItemCaching="true"
                                    Width="250px" OnSelectedIndexChanged="rcbStatus_SelectedIndexChanged" CssClass="pull-right">
                                  <%--  <DefaultItem Text="Select Status" Value="-1" Selected="true" />--%>
                                </rad:RadComboBox>
                            </div>
                            <asp:Panel runat="server" ID="pnlClassListContainer" CssClass="container" Style="margin-top: 5%">
                                <rad:RadGrid RenderMode="Lightweight" ID="gridClasses" runat="server" AllowSorting="True"
                                    CssClass="GridLess" AllowPaging="true" ShowFooter="false" PageSize="20" Height="800px">
                                    <ClientSettings EnableAlternatingItems="false">
                                        <Scrolling UseStaticHeaders="true" AllowScroll="true" />
                                        <Resizing AllowColumnResize="true" AllowResizeToFit="true" ResizeGridOnColumnResize="false" />
                                    </ClientSettings>
                                    <PagerStyle Position="Bottom" />
                                    <MasterTableView AutoGenerateColumns="false" HeaderStyle-ForeColor="Teal" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center" AlternatingItemStyle-HorizontalAlign="Center">
                                        <Columns>
                                            <rad:GridTemplateColumn SortExpression="ClassName" DataField="ClassName" UniqueName="ClassName"
                                                HeaderText="Title" AllowFiltering="true" >
                                                <ItemTemplate>
                                                    <a href="ClassManagement.aspx?ClassId=<%# Utils.Encrypt(Convert.ToInt32(Eval("ClassID"))) %>"
                                                        class="blue">
                                                        <%# Eval("ClassName")%></a>
                                                </ItemTemplate>
                                            </rad:GridTemplateColumn>
                                            <rad:GridBoundColumn DataField="ClassDescription" HeaderText="Description" UniqueName="ClassDescription">
                                            </rad:GridBoundColumn>
                                            <rad:GridBoundColumn DataField="StartDate" HeaderText="StartDate" UniqueName="StartDate">
                                            </rad:GridBoundColumn>
                                            <rad:GridBoundColumn DataField="EndDate" HeaderText="EndDate" UniqueName="EndDate">
                                            </rad:GridBoundColumn>
                                            <rad:GridBoundColumn DataField="TrainerName" HeaderText="TrainerName" UniqueName="TrainerName">
                                            </rad:GridBoundColumn>
                                            <rad:GridBoundColumn DataField="Venue" HeaderText="Venue" UniqueName="Venue">
                                            </rad:GridBoundColumn>
                                            <rad:GridBoundColumn DataField="Progress" HeaderText="Status" UniqueName="Progress">
                                            </rad:GridBoundColumn>
                                        </Columns>
                                    </MasterTableView>
                                    <FooterStyle HorizontalAlign="Center" />
                                </rad:RadGrid>
                                <br />
                                <br />
                                <br />
                            </asp:Panel>
                        </asp:Panel>
                        <asp:Panel ID="pnlClassDetails" runat="server" Visible="false">
                            <asp:Panel ID="pnlClassPreview" runat="server" Style="margin-bottom: 15vh;">
                                <div class="container no-padding">
                                    <asp:Panel ID="imagePreviewContainer" runat="server" class="col-sm-4 image-full"
                                        Style="position: relative;">
                                        <div class="layer">
                                        </div>
                                        <div class="Course-Photo-Holder-Inside text-center">
                                            <p>
                                                <asp:Label ID="lblClassTitle" runat="server" CssClass="break-all"></asp:Label></p>
                                        </div>
                                    </asp:Panel>
                                    <div class="col-md-8" style="margin-top: 15px;">
                                        <h3 class="teal border-bottom-lightgray break-all" id="lblClassName">
                                            <asp:Literal Text="Class Title" runat="server" ID="ltClassTitle" />
                                        </h3>
                                        <asp:Label Text="Class Description" runat="server" ID="lblClassDesc" CssClass="break-all">
                                        </asp:Label>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:Panel runat="server" ID="pnlTabsWrapper">
                                <div class="panelRadStrip">
                                    <rad:RadTabStrip RenderMode="Lightweight" ID="RadTabStrip1" OnClientTabSelecting="onTabSelecting"
                                        SelectedIndex="0" runat="server" MultiPageID="RadMultiPage1" Width="100%" Align="Left"
                                        OnTabClick="RadTabStrip1_TabClick" Skin="">
                                    </rad:RadTabStrip>
                                </div>
                                <rad:RadMultiPage ID="RadMultiPage1" CssClass="margin-bottom-5vh" runat="server"
                                    SelectedIndex="0" OnPageViewCreated="RadMultiPage1_PageViewCreated" RenderMode="Lightweight">
                                </rad:RadMultiPage>
                            </asp:Panel>
                        </asp:Panel>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <rad:RadWindowManager ID="RadWindowManager1" RenderMode="Lightweight" EnableShadow="true"
        Skin="Bootstrap" Modal="true" VisibleOnPageLoad="false" Behaviors="Close, Move"
        RestrictionZoneID="RestrictionZone" Opacity="99" runat="server">
        <Windows>
        </Windows>
    </rad:RadWindowManager>
</asp:Content>
