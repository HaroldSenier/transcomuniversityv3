﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using TranscomUniversityV3Model;
using System.Data;

public partial class ClassManagement : System.Web.UI.Page
{
    private static int CLASSID;
    private static string CLASSNAME;
    private static bool isTrainer;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            try
            {
                CLASSID = Request.QueryString["ClassId"] == null ? 0 : Convert.ToInt32(Utils.Decrypt(Request.QueryString["ClassId"]));
                CLASSNAME = DataHelper.GetClassNameById(CLASSID);
                isTrainer = System.Web.Security.Roles.IsUserInRole(HttpContext.Current.User.Identity.Name.Split('|')[0], "Trainer");

                if (System.Web.Security.Roles.IsUserInRole(HttpContext.Current.User.Identity.Name.Split('|')[0], "Trainer"))
                {
                    lblTab1.Text = Resources.LocalizedResource.TrainerDashboard.ToString(); ;
                    TrainerSidebar1.Visible = true;
                    AdminSidebar.Visible = false;
                }
                else if (System.Web.Security.Roles.IsUserInRole(HttpContext.Current.User.Identity.Name.Split('|')[0], "Admin"))
                {
                    lblTab1.Text = Resources.LocalizedResource.AdminDashboard.ToString(); ;
                    TrainerSidebar1.Visible = false;
                    AdminSidebar.Visible = true;

                }
                else
                    Response.Redirect("~/Unauthorized.aspx");

            }
            catch (Exception)
            {
                RadWindowManager1.RadAlert("The Page you are trying to access doesn't exist!", 330, 180, "Invalid Class", "");
                CLASSNAME = "";
                Response.Redirect("~/ClassManagement.aspx");
            }
            finally
            {
                if (Request.QueryString["ClassId"] == null || CLASSNAME == "")
                {
                    //RadScriptBlock_ClassBuilder.Visible = false;
                    item3.Visible = false;
                    pnlClassDetails.Visible = false;
                    pnlClassManagement.Visible = true;
                }
                else
                {
                    //RadScriptBlock_ClassBuilder.Visible = true;
                    item3.Visible = true;
                    lblTab3.Text = CLASSNAME;
                    pnlClassDetails.Visible = true;
                    pnlClassManagement.Visible = false;
                }
            }
            var db = new TranscomUniversityV3ModelContainer();

            bindRcbStatus();
            bindGridClass();
            if (CLASSID > 0)
                loadImageAndTitle();
        }
        else
        {
            string statusId = rcbStatus.SelectedValue;
            if (Convert.ToInt32(statusId) >= 1)
            {
                bindGridClass(statusId);
            }
            else
            {
                bindGridClass();
            }
        }


    }

    private void bindRcbStatus()
    {
        RadComboBoxItem def = new RadComboBoxItem();
        def.Text =  Resources.LocalizedResource.Select.ToString() + " " + Resources.LocalizedResource.Status.ToString();
        def.Value = "0";
        rcbStatus.Items.Add(def);

        RadComboBoxItem comp = new RadComboBoxItem();
        comp.Text = Resources.LocalizedResource.Completed.ToString();
        comp.Value = "1";
        rcbStatus.Items.Add(comp);

        RadComboBoxItem inp = new RadComboBoxItem();
        inp.Text = Resources.LocalizedResource.Inprogress.ToString();
        inp.Value = "2";
        rcbStatus.Items.Add(inp);

        RadComboBoxItem ns = new RadComboBoxItem();
        ns.Text = Resources.LocalizedResource.NotStarted.ToString();
        ns.Value = "3";
        rcbStatus.Items.Add(ns);

        rcbStatus.SelectedIndex = 0;
    }

    //protected void gridClasses_OnItemCreated(object sender, GridItemEventArgs e)
    //{
    //    if (e.Item is GridFooterItem)
    //    {

    //        GridFooterItem footerItem = (GridFooterItem)e.Item;
    //        footerItem.Cells.RemoveAt(0);
    //        footerItem.Cells.RemoveAt(1);
    //        footerItem.Cells.RemoveAt(2);
    //        footerItem.Cells.RemoveAt(3);
    //        footerItem.Cells.RemoveAt(4);
    //        footerItem.Cells.RemoveAt(0);
    //        LinkButton btnViewMore = new LinkButton();
    //        btnViewMore.Text = "View More <i class=\"fa fa-chevron-down pull-right fa-gray line-height-md\"></i>";
    //        btnViewMore.ID = "btnViewMore";
    //        btnViewMore.OnClientClick = "showGridLoading(this)";
    //        btnViewMore.CssClass = "btn btn-default btn-sm margin-top-10px ";
    //        btnViewMore.Click += new System.EventHandler(btnViewMore_Click);
    //        footerItem.Cells[2].Controls.Add(btnViewMore);

    //        Button btnViewAll = new Button();
    //        btnViewAll.ID = "btnViewAll";
    //        btnViewAll.OnClientClick = "showGridLoading(this)";
    //        btnViewAll.CssClass = "display-none js_viewall";
    //        btnViewAll.Click += new System.EventHandler(btnViewAll_Click);
    //        footerItem.Cells[2].Controls.Add(btnViewAll);

    //        footerItem.Cells[2].ColumnSpan = 7;
    //        footerItem.CssClass = "bg-transparent";

    //    }
    //}

    protected void btnViewMore_Click(object sender, EventArgs e)
    {
        LinkButton btnViewMore = sender as LinkButton;
        RadGrid lvItem = btnViewMore.NamingContainer.NamingContainer.NamingContainer.NamingContainer as RadGrid;
        try
        {
            //RadGrid lvItem = dataItem.FindControl("gridClasses") as RadGrid;
            int tid = Convert.ToInt32(lvItem.Items[0].GetDataKeyValue("TrainingTypeID"));
            int count = lvItem.Items.Count;
            // var result = getClass(tid, count);

            //lvItem.DataSource = result;
            //lvItem.DataBind();


        }
        catch
        {

        }

        ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", Utils.callClientScript("hideGridLoading", lvItem.ClientID), true);
    }

    protected void btnViewAll_Click(object sender, EventArgs e)
    {
        Button btnViewAll = sender as Button;
        RadGrid lvItem = btnViewAll.NamingContainer.NamingContainer.NamingContainer.NamingContainer as RadGrid;
        try
        {
            //RadGrid lvItem = dataItem.FindControl("gridClasses") as RadGrid;
            int tid = Convert.ToInt32(lvItem.Items[0].GetDataKeyValue("TrainingTypeID"));
            int count = 9999;
            var ds = lvItem.DataSource as DataTable;
            //lvItem.DataSource = getClass(tid, count);
            lvItem.DataBind();
        }
        catch
        {

        }

        ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", Utils.callClientScript("hideGridLoading", lvItem.ClientID), true);
    }

    protected void rcbStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        string statusId = rcbStatus.SelectedValue;
        var db = new TranscomUniversityV3ModelContainer();
        if (statusId == "0")
        {
            bindGridClass();
        }
        else
        {
            bindGridClass(statusId);
        }
    }

    protected void bindGridClass()
    {
        var db = new TranscomUniversityV3ModelContainer();
        int CIM = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
        if (isTrainer)
        {
            var classids = db.tbl_transcomuniversity_rlt_ClassTrainers
                            .Where(c => c.TrainerCIM == CIM && c.HideFromList == false)
                            .Select(c => c.ClassID)
                            .ToArray();
            var classes = db.vw_TranscomUniversity_ClassDetails
                            .Select(c => new {
                                ClassName = c.ClassName.Length > 30 ? c.ClassName.Substring(0, 30) + "..." : c.ClassName,
                                ClassDescription = c.ClassDescription.Length > 30 ? c.ClassDescription.Substring(0, 30) + "..." : c.ClassDescription,
                                c.StartDate,
                                c.EndDate,
                                c.TrainerName,
                                c.Venue,
                                c.Progress,
                                c.TrainerCIM,
                                c.ClassID,
                                c.ProgressId
                                
                            })
                            .Where(c => classids.Contains(c.ClassID))
                            .OrderByDescending(c => c.StartDate)
                            .ToList();

            gridClasses.DataSource = classes;
            gridClasses.DataBind();
        }
        else
        {
            var classes = db.vw_TranscomUniversity_ClassDetails
                .Select(c => new
                {
                    ClassName = c.ClassName.Length > 30 ? c.ClassName.Substring(0, 30) + "..." : c.ClassName,
                    ClassDescription = c.ClassDescription.Length > 30 ? c.ClassDescription.Substring(0, 30) + "..." : c.ClassDescription,
                    c.StartDate,
                    c.EndDate,
                    c.TrainerName,
                    c.Venue,
                    c.Progress,
                    c.TrainerCIM,
                    c.ClassID,
                    c.ProgressId

                })
                .OrderByDescending(c => c.StartDate).ToList();
            gridClasses.DataSource = classes;
            gridClasses.DataBind();
        }



    }

    protected void bindGridClass(string statusID)
    {
        var db = new TranscomUniversityV3ModelContainer();
        int CIM = Convert.ToInt32(DataHelper.GetCurrentUserCIM());

        if (isTrainer)
        {
            var trainers = db.tbl_transcomuniversity_rlt_ClassTrainers
                            .Where(c => c.TrainerCIM == CIM && c.HideFromList == false)
                            .Select(c => c.ClassID)
                            .ToArray();
            var classes = db.vw_TranscomUniversity_ClassDetails
                            .Select(c => new {
                                ClassName = c.ClassName.Length > 30 ? c.ClassName.Substring(0, 30) + "..." : c.ClassName,
                                ClassDescription = c.ClassDescription.Length > 30 ? c.ClassDescription.Substring(0, 30) + "..." : c.ClassDescription,
                                c.StartDate,
                                c.EndDate,
                                c.TrainerName,
                                c.Venue,
                                c.Progress,
                                c.TrainerCIM,
                                c.ClassID,
                                c.ProgressId
                                
                            })
                            .Where(c => trainers.Contains(c.ClassID) && c.ProgressId == statusID)
                            .OrderByDescending(c => c.StartDate)
                            .ToList();

            gridClasses.DataSource = classes;
            gridClasses.DataBind();
        }
        else
        {
            var classes = db.vw_TranscomUniversity_ClassDetails
                .Select(c => new
                {
                    ClassName = c.ClassName.Length > 30 ? c.ClassName.Substring(0, 30) + "..." : c.ClassName,
                    ClassDescription = c.ClassDescription.Length > 30 ? c.ClassDescription.Substring(0, 30) + "..." : c.ClassDescription,
                    c.StartDate,
                    c.EndDate,
                    c.TrainerName,
                    c.Venue,
                    c.Progress,
                    c.TrainerCIM,
                    c.ClassID,
                    c.ProgressId

                })
                .Where(c => c.ProgressId == statusID).OrderByDescending(c => c.StartDate).ToList();
            gridClasses.DataSource = classes;
            gridClasses.DataBind();
        }

    }


    #region TabMaker

    protected void Page_Init(object sender, System.EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            AddTab("CLASS DETAILS");
            AddPageView(RadTabStrip1.FindTabByText("CLASS DETAILS"));
            AddTab("PARTICIPANTS");
            AddTab("TRAINER");
            AddTab("DTR");
            AddTab("HISTORY");
        }
    }

    private void AddTab(string tabName)
    {
        RadTab tab = new RadTab();
        tab.Text = tabName;
        //tab.Width = Unit.Pixel(200);
        tab.CssClass = "selectedTab";
        RadTabStrip1.Tabs.Add(tab);
    }

    protected void RadMultiPage1_PageViewCreated(object sender, RadMultiPageEventArgs e)
    {
        string userControlName = "~/UserControl/Pages/ClassManagement_" + e.PageView.ID.Replace(" ", string.Empty) + ".ascx";

        Control userControl = Page.LoadControl(userControlName);
        userControl.ID = e.PageView.ID + "_userControl";

        e.PageView.Controls.Add(userControl);
    }

    private void AddPageView(RadTab tab)
    {
        RadPageView pageView = new RadPageView();
        pageView.ID = tab.Text.Replace(" ", "");
        pageView.CssClass = "contentWrapper" + tab.Index;
        RadMultiPage1.PageViews.Add(pageView);
        tab.PageViewID = pageView.ID;
    }

    protected void RadTabStrip1_TabClick(object sender, RadTabStripEventArgs e)
    {
        AddPageView(e.Tab);
        e.Tab.PageView.Selected = true;
        ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", Utils.callClientScript("hidePageLoading"), true);
    }

    private void loadImageAndTitle()
    {

        var db = new TranscomUniversityV3ModelContainer();
        var res = db.vw_TranscomUniversity_ClassDetails
            .Where(c => c.ClassID == CLASSID)
            .Select(c => new
            {
                c.ClassName,
                c.ClassDescription,
                c.ClassImageUrl
            }).SingleOrDefault();

        string classImage = res.ClassImageUrl == null ? "No_Image.jpg" : res.ClassImageUrl;
        ltClassTitle.Text = res.ClassName.Length > 100 ? Utils.ClipStringTo100(res.ClassName) : res.ClassName;
        lblClassDesc.Text = res.ClassDescription.Length > 500 ? Utils.ClipStringTo500(res.ClassDescription) : res.ClassDescription;
        lblClassTitle.Text = res.ClassName;
        if (classImage != "No_image.jpg")
            imagePreviewContainer.Attributes["style"] = "background-image: url(" + string.Format("Media/Uploads/ClassImg/{0}/{1}", CLASSID, HttpUtility.UrlEncode(classImage)) + "); margin:40px; height: 250px; width: 350px;margin-right:0;";
        else
            imagePreviewContainer.Attributes["style"] = "background-image: url(" + "Media/Uploads/ClassImg/No_image.jpg" + ");  margin:40px; height: 250px; width: 350px;margin-right:0;";
    }

    #endregion
}