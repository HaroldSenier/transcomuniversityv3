﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TranscomUniversityV3.Master" AutoEventWireup="true"
    CodeFile="CourseBuilder.aspx.cs" Inherits="CourseBuilder" %>

<%@ Register Src="UserControl/Pages/SettingsCBUserCtrl.ascx" TagName="SettingsCBUserCtrl"
    TagPrefix="uc1" %>
<%@ Register Src="UserControl/Pages/GradeBookCBUserCtrl.ascx" TagName="GradeBookCBUserCtrl"
    TagPrefix="uc2" %>
<%@ Register Src="UserControl/Sidebars/CourseBuilderDetails.ascx" TagName="CourseBuilderDetails"
    TagPrefix="uc3" %>
<%@ Register Src="UserControl/Sidebars/CourseBuilderDripContent.ascx" TagName="CourseBuilderDripContent"
    TagPrefix="uc4" %>
<%@ Register Src="UserControl/Sidebars/CourseBuilderNotifs.ascx" TagName="CourseBuilderNotifs"
    TagPrefix="uc5" %>
<%@ Register Src="UserControl/Sidebars/CourseBuilderEnrol.ascx" TagName="CourseBuilderEnrol"
    TagPrefix="uc6" %>
<%@ Register Src="UserControl/Sidebars/CourseBuilderWidgets.ascx" TagName="CourseBuilderWidgets"
    TagPrefix="uc7" %>
<%@ Register Src="UserControl/Pages/UsersCBUserCtrl.ascx" TagName="usersTab" TagPrefix="ucUsers" %>
<%@ Register Src="UserControl/Sidebars/CourseUsersRightPanelCtrl.ascx" TagName="usersPanel"
    TagPrefix="ucUsersPanel" %>
<%@ Register Src="UserControl/Pages/TrainersCBUserCtrl.ascx" TagName="trainersTab"
    TagPrefix="ucTrainers" %>
<%@ Register Src="UserControl/Sidebars/CourseTrainersRightPanelCtrl.ascx" TagName="trainersPanel"
    TagPrefix="ucTrainersPanel" %>
<%@ Register Src="UserControl/Pages/CourseGroupTabCtrl.ascx" TagName="groupTab" TagPrefix="ucGroup" %>
<%@ Register Src="UserControl/Sidebars/CourseGroupRightPanelCtrl.ascx" TagName="groupPanel"
    TagPrefix="ucGroupPanel" %>
<%@ Register Src="UserControl/Pages/CourseHistoryUserCtrl.ascx" TagName="historyTab"
    TagPrefix="ucHistory" %>
<%@ Register TagPrefix="ucUploadContent" TagName="UploadContent" Src="~/SCORM/DnsControls/UploadContent.ascx" %>
<%@ Register TagPrefix="ucContentPanel" TagName="contentPanel" Src="UserControl/Sidebars/CourseContentRightPanelCtrl.ascx" %>
<%@ Register TagPrefix="ucContent" TagName="Content" Src="UserControl/Pages/ContentCBUserCtrl.ascx" %>
<%@ Register Src="UserControl/Sidebars/CourseBuilderBadges.ascx" TagName="CourseBuilderBadges"
    TagPrefix="uc8" %>
<%@ Register Src="UserControl/Pages/BadgeUserCtrl.ascx" TagName="BadgeUserCtrl" TagPrefix="uc9" %>
<%@ Register Src="UserControl/Statics/AdminSiderbarUserCtrl.ascx" TagName="AdminSidebar"
    TagPrefix="ucadminside" %>
<%@ Register Src="UserControl/Statics/TrainerSidebarUserCtrl.ascx" TagName="TrainerSidebar"
    TagPrefix="uctrainerside" %>
<asp:Content ID="Content1" ContentPlaceHolderID="contentPlaceHolderLeftPanel" runat="server">
    <style>
       <%-- Change Highlighted Grey // MQUERUBIN 12072018 --%>
       .RadComboBox_Default .rcbReadOnly.rcbFocused
       {
           border-color: #676767;
           color: #000;
           background-color: #b5b5b5;
           background-image: linear-gradient(#ffffff,#e3e3e3);
       }
       .RadDropDownTree_Default .rddtFocused 
       {
           border-color: #676767;
           color: #000;
           background-color: #b5b5b5;
           background-image: linear-gradient(#ffffff,#e3e3e3);
       }
   </style>
    <rad:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Transparency="25"
        IsSticky="true" CssClass="Loading" />
    <rad:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel2" runat="server" Transparency="25"
        MinDisplayTime="1000" CssClass="Loading2" />
    <rad:RadAjaxManager ID="RadAjaxManager1" runat="server" DefaultLoadingPanelID="RadAjaxLoadingPanel1">
        <ClientEvents OnRequestStart="RequestStart" />
        <AjaxSettings>
            <rad:AjaxSetting AjaxControlID="RadTabStrip2">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="content_changer" />
                    <rad:AjaxUpdatedControl ControlID="RadTabStrip2" UpdatePanelRenderMode="Inline" />
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage2" />
                    <rad:AjaxUpdatedControl ControlID="sidebar_changer" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnAddGroup">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="btnAddGroup" LoadingPanelID="noLoadingPanel" />
                    <rad:AjaxUpdatedControl ControlID="rgCourseGroup" LoadingPanelID="noLoadingPanel" />
                    <rad:AjaxUpdatedControl ControlID="rdGroupList" LoadingPanelID="noLoadingPanel" />
                    <rad:AjaxUpdatedControl ControlID="pnlGroupList" LoadingPanelID="localLoadingPanel" />
                    <rad:AjaxUpdatedControl ControlID="rgTransactionSummary" LoadingPanelID="noLoadingPanel" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnAssignGroup">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="btnAssignGroup" LoadingPanelID="noLoadingPanel" />
                    <rad:AjaxUpdatedControl ControlID="rgCourseGroup" />
                    <rad:AjaxUpdatedControl ControlID="rdCourseListNew" />
                    <rad:AjaxUpdatedControl ControlID="gtrgTransactionSummary" LoadingPanelID="noLoadingPanel" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnRemoveUser">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="btnRemoveUser" LoadingPanelID="noLoadingPanel" />
                    <rad:AjaxUpdatedControl ControlID="rgMembers" LoadingPanelID="localLoadingPanel" />
                    <rad:AjaxUpdatedControl ControlID="rgCourseGroup" LoadingPanelID="noLoadingPanel" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadAsyncUpload1" />
                    <rad:AjaxUpdatedControl ControlID="Content" />
                    <rad:AjaxUpdatedControl ControlID="contentPanel" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="udpSub">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="udpSub" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnSwitchToBadgeListView">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="BadgeUserCtrl1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnSwitchBadgeToGridView">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="BadgeUserCtrl1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnSwitchBadgeToGridView">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="BadgeUserCtrl1" />
                </UpdatedControls>
            </rad:AjaxSetting>
        </AjaxSettings>
    </rad:RadAjaxManager>
    <rad:RadScriptBlock ID="RadScriptBlock1" runat="server">
        <script type="text/javascript">
            function pageLoad() {
                initializeContentAccordion();
                fixNoSection();
                //deleteCourseUploaderFiles();
                //resizeGrid();
            }
            function RequestStart(sender, args) {
                // the following Javascript code takes care of expanding the RadAjaxLoadingPanel
                // to the full height of the page, if it is more than the browser window viewport

                var loadingPanel = $get("<%= RadAjaxLoadingPanel1.ClientID %>");
                var pageHeight = document.documentElement.scrollHeight;
                var viewportHeight = document.documentElement.clientHeight;

                if (pageHeight > viewportHeight) {
                    loadingPanel.style.height = pageHeight + "px";
                }

                var pageWidth = document.documentElement.scrollWidth;
                var viewportWidth = document.documentElement.clientWidth;

                if (pageWidth > viewportWidth) {
                    loadingPanel.style.width = pageWidth + "px";
                }

                // the following Javascript code takes care of centering the RadAjaxLoadingPanel
                // background image, taking into consideration the scroll offset of the page content

                if ($telerik.isSafari) {
                    var scrollTopOffset = document.body.scrollTop;
                    var scrollLeftOffset = document.body.scrollLeft;
                }
                else {
                    var scrollTopOffset = document.documentElement.scrollTop;
                    var scrollLeftOffset = document.documentElement.scrollLeft;
                }

                var loadingImageWidth = 55;
                var loadingImageHeight = 55;

                loadingPanel.style.backgroundPosition = (parseInt(scrollLeftOffset) + parseInt(viewportWidth / 2) - parseInt(loadingImageWidth / 2)) + "px " + (parseInt(scrollTopOffset) + parseInt(viewportHeight / 2) - parseInt(loadingImageHeight / 2)) + "px";
            }

            function onTabSelecting(sender, args) {

                if (args.get_tab().get_pageViewID()) {
                    args.get_tab().set_postBack(false);
                }


            }
        </script>
        <script type="text/javascript">
            //close radWindow
            var from;
            function onBeforeClose(sender, arg) {
                function callbackFunction(arg) {
                    if (arg) {
                        sender.remove_beforeClose(onBeforeClose);
                        sender.close();
                    }
                }
                arg.set_cancel(true);

                if (from == '1') {
                    sender.remove_beforeClose(onBeforeClose);
                    sender.close();
                }
                else {
                    radconfirm("Are you sure you want to close this window?", callbackFunction, 350, 180, null, "Close Window");
                }
            }

            function View_UploadWindow1(sender, args) {

                var oWindow = window.radopen(null, "windowUpload1");

                oWindow.add_beforeClose(onBeforeClose);
                oWindow.center();
            }

            function Close_UploadWindow1(from1) {
                from = from1;
                var window = $find('<%=windowUpload1.ClientID %>');

                setTimeout(function () {
                    window.close();
                }, 100);
            }

            function View_UploadWindow2(sender, args) {
                var oWindow = window.radopen(null, "windowUpload2");

                oWindow.add_beforeClose(onBeforeClose);
                oWindow.center();
            }

            function Close_UploadWindow2(from1) {
                from = from1;
                var window = $find('<%=windowUpload2.ClientID %>');

                setTimeout(function () {
                    window.close();
                }, 100);
            }

            //            Telerik.Web.UI.RadAsyncUpload.prototype.getUploadedFiles = function () {
            //                var files = [];
            //                // debugger;
            //                $telerik.$(".ruUploadSuccess", this.get_element()).each(function (index, value) {
            //                    files[index] = $telerik.$(value).text();
            //                });

            //                return files;
            //            }

            function OnClientSelectedIndexChanged(sender, eventArgs) {
                var item = eventArgs.get_item();

                if (item.get_value() == "1") {
                    View_UploadWindow1("1");
                    Close_UploadWindow2("1");
                }

                if (item.get_value() == "2") {
                    View_UploadWindow2("1");
                    Close_UploadWindow1("1");
                }
            }

            function RadAsyncUpload1_FilesUploaded() {
                setTimeout(function () {
                    var RadAjaxManager1 = $find("<%=RadAjaxManager1.ClientID %>");

                    RadAjaxManager1.ajaxRequest();
                }, 200);
            }
            
        </script>
    </rad:RadScriptBlock>
    <asp:Panel runat="server" ID="sidebarAdmin" Visible="false">
        <ucadminside:AdminSidebar ID="AdminSidebar1" runat="server" />
    </asp:Panel>
    <asp:Panel runat="server" ID="sidebarTrainer" Visible="false">
        <uctrainerside:TrainerSidebar ID="TrainerSidebar1" runat="server" />
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolderMain" runat="server">
    <rad:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true" RenderMode="Lightweight"
        Skin="Bootstrap">
        <Windows>
            <rad:RadWindow ID="windowUpload1" runat="server" Title="Upload SCO" Behaviors="Close, Move"
                DestroyOnClose="True" ShowContentDuringLoad="True" VisibleStatusbar="False" Modal="True"
                ReloadOnShow="True" Width="480px" Height="250px">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="panelUpload1">
                        <div class="insidecontainer2">
                            <div runat="server" id="div3">
                                <ucUploadContent:UploadContent ID="UploadContent" runat="server" />
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </rad:RadWindow>
            <rad:RadWindow ID="windowUpload2" runat="server" Title="Upload SCO" Behaviors="Close, Move"
                DestroyOnClose="True" ShowContentDuringLoad="True" VisibleStatusbar="False" Modal="True"
                ReloadOnShow="True" Width="750" Height="500">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="panelUpload2">
                        <div class="insidecontainer2">
                            <div style="padding-bottom: 10px">
                                <rad:RadComboBox runat="server" ID="cbUploadType2" EmptyMessage="- upload type -"
                                    OnClientSelectedIndexChanged="OnClientSelectedIndexChanged" Width="130">
                                    <Items>
                                        <rad:RadComboBoxItem runat="server" Text="Upload New" Value="1" />
                                        <rad:RadComboBoxItem runat="server" Text="Add from Catalog" Value="2" Selected="True" />
                                    </Items>
                                </rad:RadComboBox>
                                <hr />
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </rad:RadWindow>
        </Windows>
    </rad:RadWindowManager>
    <div class="col-lg-12">
        <div class="row">
            <div class="col-md-12 thirty-px-padding">
                <div class="col-md-12 no-paddings">
                    <ul id="ulBreadcrumb" runat="server" class="breadcrumb">
                        <li>
                            <asp:Label ID="lblTab1" runat="server" Text="<%$ Resources:LocalizedResource, Admin%>" /></li>
                        <li>
                            <asp:Label ID="lblTab2" runat="server" Text="<%$ Resources:LocalizedResource, CourseManagement%>" /></li>
                        <li>
                            <asp:Label ID="lblTab3" runat="server" Text="<%$ Resources:LocalizedResource, CourseTitle%>" /></li>
                    </ul>
                </div>
                <div class="col-md-9  no-paddings" id="content_changer" runat="server">
                    <asp:Panel ID="pnlCoursePreview" runat="server" Style="margin-bottom: 15vh;">
                        <div class="container no-padding">
                            <div id="imagePreviewContainer" runat="server" class="col-sm-4 image-full" style="position: relative;">
                                <div class="layer">
                                </div>
                                <div class="Course-Photo-Holder-Inside text-center">
                                    <p>
                                        <asp:Label ID="CourseTitle" runat="server"></asp:Label></p>
                                </div>
                            </div>
                        </div>
                        <div class="row no-paddings">
                            <div class="col-sm-4" style="width: 350px; margin: 0 40px;">
                                <div class="text-center">
                                    <asp:Button ID="btnPreviewCourse" runat="server" Text="<%$ Resources:LocalizedResource, PreviewCourse%>" ToolTip="<%$ Resources:LocalizedResource, PreviewCourse%>"
                                        CssClass="btn btn-md btn-teal btn-flat" Width="200px" OnClick="btnPreviewCourse_Click" />
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <div class="row">
                        <div class="panelRadStrip" style="margin-top: 30px;">
                            <rad:RadAjaxPanel ID="raPanelCourseContent" runat="server">
                                <rad:RadTabStrip ID="RadTabStrip2" runat="server" RenderMode="Lightweight" SelectedIndex="0"
                                    MultiPageID="RadMultiPage2" AutoPostBack="True" EnableEmbeddedSkins="false" Skin="">
                                    <Tabs>
                                        <rad:RadTab runat="server" Text="<%$ Resources:LocalizedResource, Content%>" Value="1" CssClass="fakeborder font-uppercase" SelectedCssClass="selectedTab"
                                            ForeColor="#636363">
                                        </rad:RadTab>
                                        <rad:RadTab runat="server" Text="<%$ Resources:LocalizedResource, Users%>" Value="2" CssClass="tab font-uppercase" SelectedCssClass="selectedTab"
                                            ForeColor="#636363">
                                        </rad:RadTab>
                                        <rad:RadTab runat="server" Text="<%$ Resources:LocalizedResource, Trainers%>" Value="3" CssClass="tab font-uppercase" SelectedCssClass="selectedTab"
                                            ForeColor="#636363">
                                        </rad:RadTab>
                                        <rad:RadTab runat="server" Text="<%$ Resources:LocalizedResource, GROUPS%>" Value="4" CssClass="tab font-uppercase" SelectedCssClass="selectedTab"
                                            ForeColor="#636363">
                                        </rad:RadTab>
                                        <rad:RadTab runat="server" Text="<%$ Resources:LocalizedResource, GRADEBOOK%>" Value="5" CssClass="tab font-uppercase" SelectedCssClass="selectedTab"
                                            ForeColor="#636363">
                                        </rad:RadTab>
                                        <rad:RadTab runat="server" Text="<%$ Resources:LocalizedResource, History%>" Value="6" CssClass="tab font-uppercase" SelectedCssClass="selectedTab"
                                            ForeColor="#636363">
                                        </rad:RadTab>
                                        <rad:RadTab runat="server" Text="<%$ Resources:LocalizedResource, Settings%>" Value="7" CssClass="tab font-uppercase" SelectedCssClass="selectedTab"
                                            ForeColor="#636363">
                                        </rad:RadTab>
                                    </Tabs>
                                </rad:RadTabStrip>
                                <rad:RadMultiPage ID="RadMultiPage2" runat="server" RenderMode="Lightweight" SelectedIndex="0"
                                    CssClass="outerMultiPage" RenderSelectedPageOnly="true">
                                    <rad:RadPageView ID="RadPageView9" runat="server">
                                        <br />
                                        <ucContent:Content ID="Content" runat="server" />
                                    </rad:RadPageView>
                                    <rad:RadPageView ID="RadPageView10" runat="server">
                                        <br />
                                        <br />
                                        <ucUsers:usersTab ID="gtUsers" runat="server" />
                                    </rad:RadPageView>
                                    <rad:RadPageView ID="RadPageView11" runat="server">
                                        <br />
                                        <br />
                                        <ucTrainers:trainersTab ID="gtTrainers" runat="server" />
                                    </rad:RadPageView>
                                    <rad:RadPageView ID="RadPageView12" runat="server">
                                        <br />
                                        <br />
                                        <ucGroup:groupTab ID="gtcourseGroup" runat="server" />
                                    </rad:RadPageView>
                                    <rad:RadPageView ID="RadPageView13" runat="server">
                                        <br />
                                        <br />
                                        <uc2:GradeBookCBUserCtrl ID="ucGradebook" runat="server" />
                                    </rad:RadPageView>
                                    <rad:RadPageView ID="RadPageView14" runat="server">
                                        <br />
                                        <br />
                                        <ucHistory:historyTab ID="gtCourseHistory" runat="server" />
                                    </rad:RadPageView>
                                    <rad:RadPageView ID="RadPageView15" runat="server">
                                        <br />
                                        <uc1:SettingsCBUserCtrl ID="SettingsCBUserCtrl1" runat="server" />
                                        <uc9:BadgeUserCtrl ID="BadgeUserCtrl1" runat="server" Visible="false" />
                                    </rad:RadPageView>
                                </rad:RadMultiPage>
                                <%-- <rad:RadTabStrip ID="RadTabStrip2" runat="server" RenderMode="Lightweight" SelectedIndex="0" OnClientTabSelecting="onTabSelecting"
                                    MultiPageID="RadMultiPage2" OnTabClick="RadTabStrip2_TabClick" EnableEmbeddedSkins="false" Skin="">
                                    </rad:RadTabStrip>

                                    <rad:RadMultiPage ID="RadMultiPage2" runat="server" RenderMode="Lightweight" SelectedIndex="0"
                                    CssClass="outerMultiPage" RenderSelectedPageOnly="true" OnPageViewCreated="RadMultiPage2_PageViewCreated">
                                    </rad:RadMultiPage>--%>
                            </rad:RadAjaxPanel>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 no-paddings" id="sidebar_changer" runat="server">
                    <!-- Start of sidebars -->
                    <!--End of sidebars-->
                    <!--<asp:Panel ID="SettingsSideBarGroup" runat="server">-->
                    <uc3:CourseBuilderDetails ID="CourseBuilderDetails1" runat="server" />
                    <uc5:CourseBuilderNotifs ID="CourseBuilderNotifs1" runat="server" />
                    <uc4:CourseBuilderDripContent ID="CourseBuilderDripContent1" runat="server" />
                    <uc6:CourseBuilderEnrol ID="CourseBuilderEnrol1" runat="server" />
                    <uc7:CourseBuilderWidgets ID="CourseBuilderWidgets1" runat="server" />
                    <uc8:CourseBuilderBadges ID="CourseBuilderBadges1" runat="server" />
                    <!--</asp:Panel>-->
                    <ucGroupPanel:groupPanel ID="gtGroupPanel" runat="server" />
                    <ucUsersPanel:usersPanel ID="gtUsersPanel" runat="server" />
                    <ucTrainersPanel:trainersPanel ID="gtTrainersPanel" runat="server" />
                    <ucContentPanel:contentPanel ID="contentPanel" runat="server" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
