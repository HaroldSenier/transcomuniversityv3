﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using TranscomUniversityV3Model;
using System.Web.UI.HtmlControls;
using System.Data;

public partial class CourseBuilder : BasePage
{
    protected string Encrypt(string clearText)
    {
        string EncryptionKey = "TRNSCMV32017111";
        byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);

        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(clearBytes, 0, clearBytes.Length);
                    cs.Close();
                }
                clearText = Convert.ToBase64String(ms.ToArray());
            }
        }
        return clearText;
    }

    protected string Decrypt(string cipherText)
    {
        string EncryptionKey = "TRNSCMV32017111";
        cipherText = cipherText.Replace(" ", "+");
        byte[] cipherBytes = Convert.FromBase64String(cipherText);

        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(cipherBytes, 0, cipherBytes.Length);
                    cs.Close();
                }
                cipherText = Encoding.Unicode.GetString(ms.ToArray());
            }
        }
        return cipherText;
    }
 
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Form.Attributes.Add("enctype", "multipart/form-data");

        if (!Page.IsPostBack)
        {
            string frompage = System.Web.Security.Roles.IsUserInRole(HttpContext.Current.User.Identity.Name.Split('|')[0], "Trainer") == true ? "Trainer" : "Admin";
            lblTab1.Text = frompage;
            sidebarTrainer.Visible = false;
            sidebarAdmin.Visible = false;

            if (frompage == "Trainer")
                sidebarTrainer.Visible = true;
            else
                sidebarAdmin.Visible = true;


            if (!(System.Web.Security.Roles.IsUserInRole(HttpContext.Current.User.Identity.Name.Split('|')[0], "Admin") || System.Web.Security.Roles.IsUserInRole(HttpContext.Current.User.Identity.Name.Split('|')[0], "Trainer")))
                Response.Redirect("~/Unauthorized.aspx");

        }

        RadTab content = RadTabStrip2.Tabs.FindTabByValue("1");
        RadTab users = RadTabStrip2.Tabs.FindTabByValue("2");
        RadTab trainers = RadTabStrip2.Tabs.FindTabByValue("3");
        RadTab groups = RadTabStrip2.Tabs.FindTabByValue("4");
        RadTab gradeBook = RadTabStrip2.Tabs.FindTabByValue("5");
        RadTab history = RadTabStrip2.Tabs.FindTabByValue("6");
        RadTab settings = RadTabStrip2.Tabs.FindTabByValue("7");

        if (content.Selected)
        {
            RadTabStrip2.SelectedIndex = 0;
            content_changer.Attributes.Add("class", "col-md-9");
            sidebar_changer.Visible = true;
            contentPanel.Visible = true;
        }
        else
        {
            contentPanel.Visible = false;
        }

        if (users.Selected)
        {
            RadTabStrip2.SelectedIndex = 1;
            content_changer.Attributes.Add("class", "col-md-9");
            sidebar_changer.Visible = true;
            gtUsersPanel.Visible = true;
        }
        else
        {
            gtUsersPanel.Visible = false;
        }

        if (trainers.Selected)
        {
            RadTabStrip2.SelectedIndex = 2;
            content_changer.Attributes.Add("class", "col-md-9");
            sidebar_changer.Visible = true;
            gtTrainersPanel.Visible = true;
        }
        else
        {
            gtTrainersPanel.Visible = false;
        }

        if (groups.Selected)
        {
            RadTabStrip2.SelectedIndex = 3;
            content_changer.Attributes.Add("class", "col-md-9");
            sidebar_changer.Visible = true;
            gtGroupPanel.Visible = true;

        }
        else
        {
            gtGroupPanel.Visible = false;
        }

        if (gradeBook.Selected)
        {
            RadTabStrip2.SelectedIndex = 4;
            content_changer.Attributes["class"] = "col-md-12";
            sidebar_changer.Visible = false;
        }

        if (history.Selected)
        {
            RadTabStrip2.SelectedIndex = 5;
            content_changer.Attributes["class"] = "col-md-12";
            sidebar_changer.Visible = false;
            gtGroupPanel.Visible = false;
        }

        if (settings.Selected)
        {
            RadTabStrip2.SelectedIndex = 6;
            content_changer.Attributes["class"] = "col-md-9";
            sidebar_changer.Visible = true;
            //CourseBuilderDetails1.Visible = true;

            /*if (sidebar_changer.Visible == true)
            {
                content_changer.Attributes["class"] = "col-md-9";
                sidebar_changer.Visible = true;
            }*/

            /*CourseBuilderDetails1.Visible = true;
            CourseBuilderEnrol1.Visible = true;
            CourseBuilderDripContent1.Visible = true;
            CourseBuilderWidgets1.Visible = true;
            CourseBuilderNotifs1.Visible = true;*/
        }
        else
        {
            CourseBuilderDetails1.Visible = false;
            CourseBuilderEnrol1.Visible = false;
            CourseBuilderDripContent1.Visible = false;
            CourseBuilderWidgets1.Visible = false;
            CourseBuilderNotifs1.Visible = false;
            CourseBuilderBadges1.Visible = false;
            BadgeUserCtrl1.Visible = false;
        }

        try
        {
            var db = new TranscomUniversityV3ModelContainer();

            var courseId = Convert.ToInt32(Decrypt(Request.QueryString["CourseID"]));
            var courseImage = (from c in db.tbl_TranscomUniversity_Cor_Course
                               where c.CourseID == courseId
                               select c.CourseImage).FirstOrDefault().ToString();

            var courseTitle = (from c in db.tbl_TranscomUniversity_Cor_Course
                               where c.CourseID == courseId
                               select c.Title).FirstOrDefault().ToString();

            var ssCourseTitle = string.Empty;
            string[] words = courseTitle.Split(' ');
            if (words.Count() == 1 && courseTitle.Length > 30)
            {
                double leng = Convert.ToDouble(courseTitle.Length) / 30;
                var value = Math.Ceiling(leng);
                var a= 0;
                var b = 30;
                for (var i = 1; i <= value; i++)
                {
                    if (courseTitle.Length < b*i)
                        b = (courseTitle.Length -1) - (b*(i-1));
                    ssCourseTitle += courseTitle.Substring(a,b) + " ";                   
                    a = (30 * i) +1;                  
                }
            }

            else
            {
                ssCourseTitle = courseTitle;
            }
            CourseTitle.Text = ssCourseTitle;
            if (courseTitle.Length > 80)
                ssCourseTitle = courseTitle.Substring(0, 79) + "...";
            else
            {
                ssCourseTitle = courseTitle;
            }
            lblTab3.Text = ssCourseTitle;
            if (courseImage != "No_image.jpg")
                imagePreviewContainer.Attributes["style"] = "background-image: url('" + string.Format("Media/Uploads/CourseImg/{0}/{1}", courseId, HttpUtility.HtmlEncode(courseImage)) + "'); margin:40px; height: 250px; width: 350px;";
            else
                imagePreviewContainer.Attributes["style"] = "background-image: url('" + "Media/Uploads/CourseImg/No_image.jpg" + "');  margin:40px; height: 250px; width: 350px;";
        }
        catch
        {
            RadWindowManager1.RadAlert("There is an error on the page. Please contact your System Administrator.", 330, 180, Resources.LocalizedResource.ErrorMessage.ToString(), "");
        }

        //breadcrumb

       

    }

    protected void btnPreviewCourse_Click(object sender, EventArgs e)
    {
        var courseId = Convert.ToInt32(Decrypt(Request.QueryString["CourseID"]));
        var url = string.Format("CoursePreview.aspx?CourseID={0}", Encrypt(courseId.ToString()));

        Response.Redirect(url);
    }

    #region Dynamic Tab

    //protected void Page_Init(object sender, System.EventArgs e)
    //{
    //    if (!Page.IsPostBack)
    //    {
    //        AddTab("CONTENT");          
    //        AddTab("USERS");
    //        AddPageView(RadTabStrip2.FindTabByText("CONTENT"));  
    //        AddPageView(RadTabStrip2.FindTabByText("USERS"));
    //        AddPageView(RadTabStrip2.FindTabByText("CONTENT"));  
    //        AddTab("TRAINER");
    //        AddTab("GROUPS");
    //        AddTab("GRADEBOOK");
    //        AddTab("HISTORY");
    //        AddTab("SETTING");
    //    }
    //}

    //private void AddTab(string tabName)
    //{
    //    RadTab tab = new RadTab();
    //    tab.Text = tabName;       
    //    tab.CssClass = "selectedTab";
    //    RadTabStrip2.Tabs.Add(tab);
    //}

    //protected void RadTabStrip2_TabClick(object sender, RadTabStripEventArgs e)
    //{
    //    try
    //    {            
    //        AddPageView(e.Tab);
    //        e.Tab.PageView.Selected = true;
    //    }
    //    catch { }
    //}

    //private void AddPageView(RadTab tab)
    //{
    //    RadPageView pageView = new RadPageView();
    //    pageView.ID = tab.Text;
    //    pageView.CssClass = "contentWrapper" + tab.Index;
    //    var check = RadMultiPage2.FindPageViewByID(changeRadMultipageID(pageView.ID));
    //    if (check == null)
    //    {
    //        RadMultiPage2.PageViews.Add(pageView);
    //        tab.PageViewID = pageView.ID;            
    //    }
       
        
    //}

    //protected void RadMultiPage2_PageViewCreated(object sender, RadMultiPageEventArgs e)    {           
    //        e.PageView.ID = changeRadMultipageID(e.PageView.ID.ToString());
    //        string userControlName = "~/UserControl/Pages/" + e.PageView.ID.Replace(" ", string.Empty) + ".ascx";

    //        Control userControl = Page.LoadControl(userControlName);
    //        userControl.ID = e.PageView.ID + "_userControl";

    //        var check = Page.FindControl(userControl.ID);
            

    //        if (check == null)
    //        {
    //            e.PageView.Controls.Add(userControl);

    //            if (e.PageView.ID.ToString() == "SettingsCBUserCtrl")
    //            {
    //                var name = "BadgeUserCtrl";
    //                Control userControl1 = Page.LoadControl("~/UserControl/Pages/" + name.Replace(" ", string.Empty) + ".ascx");
    //                userControl1.ID = "BadgeUserCtrl1_userControl";
    //                userControl1.Visible = false;
    //                e.PageView.Controls.Add(userControl1);
    //            }
    //        }
               
    //}

    //string changeRadMultipageID(string IDName)
    //{
    //    var newIDName = IDName;
    //    if (IDName == "CONTENT")
    //    {
    //        newIDName = "ContentCBUserCtrl";
    //        content_changer.Attributes.Add("class", "col-md-9");
    //        sidebar_changer.Visible = true;
    //        contentPanel.Visible = true;
    //    }
    //    else
    //        contentPanel.Visible = false;
    //    if (IDName == "USERS")
    //    {
    //        newIDName = "UsersCBUserCtrl";
    //        content_changer.Attributes.Add("class", "col-md-9");
    //        sidebar_changer.Visible = true;
    //        gtUsersPanel.Visible = true;
    //    }
    //    else
    //    {
    //        gtUsersPanel.Visible = false;
    //    }

    //    if (IDName == "TRAINER")
    //    {
    //        newIDName = "TrainersCBUserCtrl";
    //        content_changer.Attributes.Add("class", "col-md-9");
    //        sidebar_changer.Visible = true;
    //        gtTrainersPanel.Visible = true;
    //    }
    //    else
    //    {
    //        gtTrainersPanel.Visible = false;
    //    }

    //    if (IDName == "GROUPS")
    //    {
    //        newIDName = "CourseGroupTabCtrl";
    //        content_changer.Attributes.Add("class", "col-md-9");
    //        sidebar_changer.Visible = true;
    //        gtGroupPanel.Visible = true;
    //    }
    //    else
    //    {
    //        gtGroupPanel.Visible = false;
    //    }

    //    if (IDName == "GRADEBOOK")
    //    {
    //        newIDName = "GradeBookCBUserCtrl";
    //        content_changer.Attributes["class"] = "col-md-12";
    //        sidebar_changer.Visible = false;
    //    }

    //    if (IDName == "HISTORY")
    //    {
    //        newIDName = "CourseHistoryUserCtrl";
    //        content_changer.Attributes["class"] = "col-md-12";
    //        sidebar_changer.Visible = false;
    //        gtGroupPanel.Visible = false;
    //    }

    //    if (IDName == "SETTING")
    //    {
    //        newIDName = "SettingsCBUserCtrl";
    //        content_changer.Attributes["class"] = "col-md-9";
    //        sidebar_changer.Visible = true;           
    //        }
    //        else
    //        {
    //            CourseBuilderDetails1.Visible = false;
    //            CourseBuilderEnrol1.Visible = false;
    //            CourseBuilderDripContent1.Visible = false;
    //            CourseBuilderWidgets1.Visible = false;
    //            CourseBuilderNotifs1.Visible = false;
    //            CourseBuilderBadges1.Visible = false;
    //    }
    //    return newIDName;
    //}

#endregion
}