﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TranscomUniversityV3.Master" AutoEventWireup="true"
    CodeFile="CourseCatalog.aspx.cs" Inherits="TranscomUniversityV3.CourseCatalog" %>

<%@ Register Src="UserControl/Statics/CourseCatalogSidebarUserCtrl.ascx" TagName="courseCatalogSidebar"
    TagPrefix="ucCcSidebar" %>
<%@ Register Src="UserControl/Pages/CourseCategoryUserCtrl.ascx" TagName="courseCatalogTab"
    TagPrefix="ucCourseCatalogTab" %>
<asp:Content ID="Content1" ContentPlaceHolderID="contentPlaceHolderLeftPanel" runat="server">
    <style>
    .clamp-line-1
    {
         overflow: hidden;
         -webkit-line-clamp: 1;
         -webkit-box-orient: vertical;
         /* FOR IE */
         text-overflow: ellipsis;
         word-wrap: break-word;
         /* FOR IE */
         max-height: 2em;
    }
    .clamp-line-2
    {
         overflow: hidden;
         -webkit-line-clamp: 2;
         -webkit-box-orient: vertical;
         /* FOR IE */
         text-overflow: ellipsis;
         word-wrap: break-word;
         /* FOR IE */
         max-height: 3em;
    }
    .clamp-line-3
    {
         overflow: hidden;
         -webkit-line-clamp: 3;
         -webkit-box-orient: vertical;
         /* FOR IE */
         text-overflow: ellipsis;
         word-wrap: break-word;
         /* FOR IE */
         max-height: 6em;
    }
    .font-desc
    {
        font-size:0.9em;
    }
    </style>
    <rad:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Transparency="25"
        IsSticky="true" CssClass="Loading" />
    <rad:RadScriptBlock ID="rsbRecommendedLeadershipCourses" runat="server">
        <script type="text/javascript">
            var recommendedLeadershipCoursesListView;
            var recommendedLeadershipCoursesGridView;
            var arrRecommendeLeadership;
            var GLOBAL_PAGESIZE;
            var firstLoad;
            var viewAllLeadership;
            var viewAllMandatory;
            var viewAllSkillDev;
            var viewAllRecently;
            //--Start RecommendedLeadershipCourses Binding

            //initialize Recommender Leaderhip Course
            function initializeRecommendedLeadershipCourse() {
                $ = $telerik.$;
                //this will fire the OnCommand event

                recommendedLeadershipCoursesListView = $find("<%= lvRecommendedLeadershipCourses.ClientID%>");
                recommendedLeadershipCoursesListView.set_pageSize(GLOBAL_PAGESIZE);
                recommendedLeadershipCoursesListView.rebind();

                recommendedLeadershipCoursesListView.page(0);

                recommendedLeadershipCoursesGridView = $find("<%= gridRecommendLeadershipCourses .ClientID%>");

                $(".rlcPager .pagePrev").click(function (e) {

                    recommendedLeadershipCoursesListView.page(recommendedLeadershipCoursesListView.get_currentPageIndex() - 1);

                });
                $(".rlcPager .pageNext").click(function (e) {

                    recommendedLeadershipCoursesListView.page(recommendedLeadershipCoursesListView.get_currentPageIndex() + 1);

                });

//                $("#recommendedLeadershipCoursesListView").on("click", ".rlc-item", function (e) {

//                    recommendedLeadershipCoursesListView.toggleSelection($(this).index());

//                });

                viewAllLeadership = false;

            }

            function bindRecommendedLeadeshipCourseList() {
                debugger;

                recommendedLeadershipCoursesListView = $find("<%= lvRecommendedLeadershipCourses.ClientID %>");
                var startRowIndex = recommendedLeadershipCoursesListView.get_currentPageIndex() * recommendedLeadershipCoursesListView.get_pageSize(),
                maximumRows = recommendedLeadershipCoursesListView.get_pageSize(),
                sortExpression = recommendedLeadershipCoursesListView.get_sortExpressions().toLinq();

                //check if theres a sessioned data
                var sData = sessionStorage.getItem('lp' + recommendedLeadershipCoursesListView.get_currentPageIndex());
                console.log(viewAllLeadership);
                console.log(sData);

                if (sData != null && viewAllLeadership == false) {
                    //scrollTo("recommendedLeadershipCoursesListView");
                    sData = JSON.parse(sData);
                    recommendedLeadershipCoursesListView.set_dataSource(sData);
                    recommendedLeadershipCoursesListView.dataBind();

                    recommendedLeadershipCoursesGridView.set_dataSource(sData);
                    recommendedLeadershipCoursesGridView.dataBind();
                    lazyload();


                    $(".rlcPager .pagePrev").removeClass("display-none");
                    $(".rlcPager .pageNext").removeClass("display-none");

                    console.log("loaded sessioned leadership");

                } else {

                    $(".js-rlc-loader").show();
                    $("#recommendedLeadershipCoursesListView").addClass("display-none");
                    $("#recommendedLeadershipCoursesGridView").addClass("display-none");

                    $.ajax({
                        type: "POST",
                        data: "{startRowIndex: " + startRowIndex + ", maximumRows: " + maximumRows + ", sortExpression: '" + sortExpression + "', catID: '" + $("#hfLeadershipCatID").val() + "'}",
                        contentType: "application/json; charset=utf-8",
                        url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "GetCourseByCategory",
                        dataType: 'json',
                        success: function (res) {
                            var count = res.d.count;
                            var result = res.d.data;

                            $("#recommendedLeadershipCoursesListView").removeClass("display-none");
                            $("#recommendedLeadershipCoursesGridView").removeClass("display-none");

                            recommendedLeadershipCoursesListView.set_virtualItemCount(count);
                            if (count > 4) {
                                $(".rlcPager .pagePrev").removeClass("display-none");
                                $(".rlcPager .pageNext").removeClass("display-none");
                                $(".rlcViews").removeClass("display-none");
                            }

                            if (count <= 4) {
                                $("#<%= btnViewAllListRCFY.ClientID %>").addClass("display-none");
                                $("#<%= btnBackListRCFY.ClientID %>").addClass("display-none");

                                $("#<%= btnViewAllGridRLCFY.ClientID %>").addClass("display-none");
                                $("#<%= btnBackGridRLCFY.ClientID %>").addClass("display-none");
                            }

                            if (recommendedLeadershipCoursesListView.get_pageSize() > 4 && count > 4) {
                                $("#<%= btnViewAllListRCFY.ClientID %>").addClass("display-none");
                                $("#<%= btnBackListRCFY.ClientID %>").removeClass("display-none");

                                $("#<%= btnViewAllGridRLCFY.ClientID %>").addClass("display-none");
                                $("#<%= btnBackGridRLCFY.ClientID %>").removeClass("display-none");
                            } else {
                                viewAllLeadership = false;
                                $("#<%= btnViewAllListRCFY.ClientID %>").removeClass("display-none");
                                $("#<%= btnBackListRCFY.ClientID %>").addClass("display-none");

                                $("#<%= btnViewAllGridRLCFY.ClientID %>").removeClass("display-none");
                                $("#<%= btnBackGridRLCFY.ClientID %>").addClass("display-none");
                            }

                            if (viewAllLeadership) {
                                $(".rlcPager .pagePrev").addClass("display-none");
                                $(".rlcPager .pageNext").addClass("display-none");


                            }
                            recommendedLeadershipCoursesListView.set_dataSource(result);
                            recommendedLeadershipCoursesListView.dataBind();

                            recommendedLeadershipCoursesGridView.set_dataSource(result);
                            recommendedLeadershipCoursesGridView.dataBind();

                            debugger;
                            console.log(platform);

                            if (!platform.name.includes("IE")) 
                            {
                                console.log("DAPAT HINDI TOH PUMASOK");
                            }


                            lazyload();

                            $(".js-rlc-loader").hide();
                            $(".js-rlc-loader").addClass("display-none");
                            console.log("loader hide");

                            if (sData == null)
                                sessionStorage.setItem('lp' + recommendedLeadershipCoursesListView.get_currentPageIndex(), JSON.stringify(result));

                            if ($("#pnlLvMandatoryCourses").length > 0 && firstLoad == true)
                                initializeMandatoryCourse();

                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            var code = XMLHttpRequest.status;
                            if (code == "401")
                                window.location.reload();
                            //else
                            //alert(XMLHttpRequest.responseText);
                        }
                    });
                }
            }

            //Client Events Recommended Leadership Course

            function rlcfyOnListViewDataBinding(sender, args) {
                sender.set_selectedIndexes([]);
            }

            function rlcfyOnListViewCommand(sender, args) {
                args.set_cancel(true);
                bindRecommendedLeadeshipCourseList();
            }

            //btnView/btnBack Click Recommended Leadership Course

            function btnViewAllListRCFY_Click() {
                viewAllLeadership = true;
                recommendedLeadershipCoursesListView.set_pageSize(999);
                recommendedLeadershipCoursesListView.page(0);
                recommendedLeadershipCoursesListView.rebind();
                recommendedLeadershipCoursesGridView.set_pageSize(999);
                recommendedLeadershipCoursesGridView.page(0);

                sidebarFull();

                //return false;
            }

            function btnBackListRCFY_Click() {
                viewAllLeadership = false;

                $("#<%= btnViewAllListRCFY.ClientID %>").removeClass("display-none");
                $("#<%= btnBackListRCFY.ClientID %>").addClass("display-none");

                $("#<%= btnViewAllGridRLCFY.ClientID %>").removeClass("display-none");
                $("#<%= btnBackGridRLCFY.ClientID %>").addClass("display-none");

                recommendedLeadershipCoursesListView.set_pageSize(GLOBAL_PAGESIZE);
                recommendedLeadershipCoursesListView.page(0);
                recommendedLeadershipCoursesListView.rebind();
                recommendedLeadershipCoursesGridView.set_pageSize(GLOBAL_PAGESIZE);
                recommendedLeadershipCoursesGridView.page(0);

                sidebarFull();

            }

            $(window).scroll(function () {
                debugger;
                sidebarFull();
            });


        </script>
    </rad:RadScriptBlock>
    <rad:RadScriptBlock ID="rsbMandatory" runat="server">
        <script type="text/javascript">
            var mandatoryListView;
            var mandatoryGridView;
            var arrMandatory;
            //--Start MandatoryCourses Binding

            //initialize mandatory Course
            function initializeMandatoryCourse() {
                $ = $telerik.$;

                //this will fire the OnCommand event

                mandatoryListView = $find("<%= lvMandatoryCourses.ClientID%>");
                mandatoryGridView = $find("<%= gridMandatoryCourses.ClientID%>");

                mandatoryListView.set_pageSize(GLOBAL_PAGESIZE);
                mandatoryListView.rebind();
                mandatoryListView.page(0);

                $(".mcPager .pagePrev").click(function (e) {

                    mandatoryListView.page(mandatoryListView.get_currentPageIndex() - 1);

                });
                $(".mcPager .pageNext").click(function (e) {

                    mandatoryListView.page(mandatoryListView.get_currentPageIndex() + 1);

                });

//                $("#mandatoryListView").on("click", ".mc-item", function (e) {

//                    mandatoryListView.toggleSelection($(this).index());

//                });

                viewAllMandatory = false;
            }

            function bindMandatoryCourseList() {
                
                mandatoryListView = $find("<%= lvMandatoryCourses.ClientID %>");
                var startRowIndex = mandatoryListView.get_currentPageIndex() * mandatoryListView.get_pageSize(),
                maximumRows = mandatoryListView.get_pageSize(),
                sortExpression = mandatoryListView.get_sortExpressions().toLinq();

                //check if theres a sessioned data
                var sData = sessionStorage.getItem('mp' + mandatoryListView.get_currentPageIndex());

                if (sData != null && viewAllMandatory == false) {
                    //scrollTo("mandatoryListView");
                    sData = JSON.parse(sData);
                    mandatoryListView.set_dataSource(sData);
                    mandatoryListView.dataBind();

                    mandatoryGridView.set_dataSource(sData);
                    mandatoryGridView.dataBind();
                    lazyload();

                    $(".mcPager .pagePrev").removeClass("display-none");
                    $(".mcPager .pageNext").removeClass("display-none");

                    console.log("loaded sessioned mandatory" + mandatoryListView.get_currentPageIndex());

                } else {

                    $(".js-mc-loader").show();
                    $("#mandatoryListView").addClass("display-none");
                    $("#mandatoryGridView").addClass("display-none");

                    console.log("loaded server mandatory page" + mandatoryListView.get_currentPageIndex());

                    //make a call to get the data
                    $.ajax({
                        type: "POST",
                        data: "{startRowIndex: " + startRowIndex + ", maximumRows: " + maximumRows + ", sortExpression: '" + sortExpression + "', catID: '" + $("#hfMandatoryCatID").val() + "'}",
                        contentType: "application/json; charset=utf-8",
                        url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "GetCourseByCategory",
                        dataType: 'json',
                        success: function (res) {
                            var count = res.d.count;
                            var result = res.d.data;
                            mandatoryListView.set_virtualItemCount(count);


                            $("#mandatoryListView").removeClass("display-none");
                            $("#mandatoryGridView").removeClass("display-none");

                            if (count > 4) {
                                $(".mcPager .pagePrev").removeClass("display-none");
                                $(".mcPager .pageNext").removeClass("display-none");
                                $(".mcViews").removeClass("display-none");
                            }
                            if (count < 4) {
                                $("#<%= btnViewAllListMC.ClientID %>").addClass("display-none");
                                $("#<%= btnBackListMC.ClientID %>").addClass("display-none");

                                $("#<%= btnViewAllGridMC.ClientID %>").addClass("display-none");
                                $("#<%= btnBackGridMC.ClientID %>").addClass("display-none");
                            }

                            if (mandatoryListView.get_pageSize() > 4 && count > 4) {
                                $("#<%= btnViewAllListMC.ClientID %>").addClass("display-none");
                                $("#<%= btnBackListMC.ClientID %>").removeClass("display-none");

                                $("#<%= btnViewAllGridMC.ClientID %>").addClass("display-none");
                                $("#<%= btnBackGridMC.ClientID %>").removeClass("display-none");

                            } else {
                                viewAllMandatory = false;

                                $("#<%= btnViewAllListMC.ClientID %>").removeClass("display-none");
                                $("#<%= btnBackListMC.ClientID %>").addClass("display-none");

                                $("#<%= btnViewAllGridMC.ClientID %>").removeClass("display-none");
                                $("#<%= btnBackGridMC.ClientID %>").addClass("display-none");
                            }

                            if (viewAllMandatory) {
                                $(".mcPager .pagePrev").addClass("display-none");
                                $(".mcPager .pageNext").addClass("display-none");
                            }



                            mandatoryListView.set_dataSource(result);
                            //console.log(result);
                            $(".js-mc-loader").hide();
                            mandatoryListView.dataBind();

                            mandatoryGridView.set_dataSource(result);
                            mandatoryGridView.dataBind();
                            lazyload();

                            if (sData == null)
                                sessionStorage.setItem('mp' + mandatoryListView.get_currentPageIndex(), JSON.stringify(result));

                            if ($("#pnlLvTrendingCourses").length > 0 && firstLoad == true)
                                initializeTrendingCourse();

                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            var code = XMLHttpRequest.status;
                            if (code == "401")
                                window.location.reload();
                            //else
                            //alert(XMLHttpRequest.responseText);
                        }
                    });
                }
            }

            //Client Events mandatoryCourse

            function mcOnListViewDataBinding(sender, args) {
                sender.set_selectedIndexes([]);
            }

            function mcOnListViewCommand(sender, args) {
                args.set_cancel(true);
                bindMandatoryCourseList();
            }

            //btnView/btnBack Click Mandatory

            function btnViewAllListMC_Click() {
                viewAllMandatory = true;
                mandatoryListView.set_pageSize(999);
                mandatoryListView.page(0);
                mandatoryListView.rebind();
                mandatoryGridView.set_pageSize(999);
                mandatoryGridView.page(0);

                return false;
            }

            function btnBackListMC_Click() {
                viewAllMandatory = false;

                $("#<%= btnViewAllListMC.ClientID %>").removeClass("display-none");
                $("#<%= btnBackListMC.ClientID %>").addClass("display-none");

                $("#<%= btnViewAllGridMC.ClientID %>").removeClass("display-none");
                $("#<%= btnBackGridMC.ClientID %>").addClass("display-none");

                mandatoryListView.set_pageSize(GLOBAL_PAGESIZE);
                mandatoryListView.page(0);
                mandatoryListView.rebind();
                mandatoryGridView.set_pageSize(GLOBAL_PAGESIZE);
                mandatoryGridView.page(0);

            }


        </script>
    </rad:RadScriptBlock>
    <rad:RadScriptBlock ID="rsbSkillDevelopment" runat="server">
        <script type="text/javascript">
            var SkillDevelopmentListView;
            var SkillDevelopmentGridView;

            //initialize Skill Development Course
            function initializeSkillDevelopment() {
                $ = $telerik.$;
                //this will fire the OnCommand event
                SkillDevelopmentListView = $find("<%= lvSkillDevelopments.ClientID %>");
                SkillDevelopmentGridView = $find("<%= gridSkillDevelopments.ClientID %>");

                var listView = SkillDevelopmentListView;

                listView.set_pageSize(GLOBAL_PAGESIZE);
                listView.rebind();

                listView.page(0);



                $(".mtPager .pagePrev").click(function (e) {
                    listView.page(listView.get_currentPageIndex() - 1);
                });
                $(".mtPager .pageNext").click(function (e) {
                    listView.page(listView.get_currentPageIndex() + 1);
                });

//                $("#SkillDevelopmentListView").on("click", ".mt-item", function (e) {
//                    listView.toggleSelection($(this).index());
//                });

                viewAllSkillDev = false;
            }

            //Bind SkillDevelopmentCourse 
            function bindSkillDevelopmentList() {
                var listView = SkillDevelopmentListView;
                var gridView = SkillDevelopmentGridView;
                var startRowIndex = listView.get_currentPageIndex() * listView.get_pageSize(),
                maximumRows = listView.get_pageSize(),
                sortExpression = listView.get_sortExpressions().toLinq();

                //check if theres a sessioned data
                var sData = sessionStorage.getItem('sp' + listView.get_currentPageIndex());
                
                if (sData != null && viewAllSkillDev == false) {
                    //scrollTo("mandatoryListView");
                    sData = JSON.parse(sData);
                    listView.set_dataSource(sData);
                    listView.dataBind();

                    gridView.set_dataSource(sData);
                    gridView.dataBind();
                    lazyload();

                    console.log("loaded sessioned skill dev" + listView.get_currentPageIndex());

                } else {

                    $(".js-mt-loader").show();
                    $("#SkillDevelopmentTakeListView").addClass("display-none");
                    $("#SkillDevelopmentGridView").addClass("display-none");

                    console.log("loaded server skill dev" + listView.get_currentPageIndex());


                    $.ajax({
                        type: "POST",
                        data: "{startRowIndex: " + startRowIndex + ", maximumRows: " + maximumRows + ", sortExpression: '" + sortExpression + "', catID: '" + $("#hfSkillDevCatID").val() + "'}",
                        contentType: "application/json; charset=utf-8",
                        url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "GetCourseByCategory",
                        dataType: 'json',
                        success: function (res) {

                            $("#SkillDevelopmentTakeListView").removeClass("display-none");
                            $("#SkillDevelopmentGridView").removeClass("display-none");
                            var result = res.d.data;
                            var count = res.d.count;

                            listView.set_virtualItemCount(count);
                            if (count > 4) {
                                $(".mtPager .pagePrev").removeClass("display-none");
                                $(".mtPager .pageNext").removeClass("display-none");
                                $(".mtViews").removeClass("display-none");
                            }

                            if (listView.get_pageSize() > 4 && count > 4) {
                                $("#<%= btnViewAllListMT.ClientID %>").addClass("display-none");
                                $("#<%= btnBackListMT.ClientID %>").removeClass("display-none");

                                $("#<%= btnViewAllGridMT.ClientID %>").addClass("display-none");
                                $("#<%= btnBackGridMT.ClientID %>").removeClass("display-none");
                            } else {
                                viewAllSkillDev = false;
                                $("#<%= btnViewAllListMT.ClientID %>").removeClass("display-none");
                                $("#<%= btnBackListMT.ClientID %>").addClass("display-none");

                                $("#<%= btnViewAllGridMT.ClientID %>").removeClass("display-none");
                                $("#<%= btnBackGridMT.ClientID %>").addClass("display-none");
                            }

                            if (count <= 4) {
                                $("#<%= btnViewAllListMT.ClientID %>").addClass("display-none");
                                $("#<%= btnBackListMT.ClientID %>").addClass("display-none");

                                $("#<%= btnViewAllGridMT.ClientID %>").addClass("display-none");
                                $("#<%= btnBackGridMT.ClientID %>").addClass("display-none");
                            }

                            if (viewAllSkillDev) {
                                $(".mtPager .pagePrev").addClass("display-none");
                                $(".mtPager .pageNext").addClass("display-none");
                            }


                            //console.log(result);
                            $(".js-mt-loader").hide();
                            $("#SkillDevelopmentTakeListView").removeClass("display-none");
                            $("#SkillDevelopmentGridView").removeClass("display-none");
                            listView.set_dataSource(result);
                            listView.dataBind();

                            gridView.set_dataSource(result);
                            gridView.dataBind();

                            lazyload();

                            if (sData == null)
                                sessionStorage.setItem('sp' + listView.get_currentPageIndex(), JSON.stringify(result));

                            if ($("#pnlLvRecentlyAddedCourses").length > 0 && firstLoad == true)
                                initializeRecentCourse();


                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            var code = XMLHttpRequest.status;
                            if (code == "401")
                                window.location.reload();
                            //else
                            //alert(XMLHttpRequest.responseText);
                        }
                    });
                }
            }

            //Client Events RecentlyAddedCourse
            function mtOnListViewDataBinding(sender, args) {
                sender.set_selectedIndexes([]);
            }

            function mtOnListViewCommand(sender, args) {
                //cancel command event to prevent postback
                args.set_cancel(true);
                bindSkillDevelopmentList();
            }

            function btnViewAllListMT_Click() {
                viewAllSkillDev = true;
                SkillDevelopmentListView.set_pageSize(999);
                SkillDevelopmentListView.page(0);
                SkillDevelopmentListView.rebind();
                SkillDevelopmentGridView.set_pageSize(999);
                SkillDevelopmentGridView.page(0);

                return false;
            }

            function btnBackListMT_Click() {
                viewAllSkillDev = false;

                $("#<%= btnViewAllListMT.ClientID %>").removeClass("display-none");
                $("#<%= btnBackListMT.ClientID %>").addClass("display-none");

                $("#<%= btnViewAllGridMT.ClientID %>").removeClass("display-none");
                $("#<%= btnBackGridMT.ClientID %>").addClass("display-none");

                SkillDevelopmentListView.set_pageSize(GLOBAL_PAGESIZE);
                SkillDevelopmentListView.page(0);
                SkillDevelopmentListView.rebind();
                SkillDevelopmentGridView.set_pageSize(GLOBAL_PAGESIZE);
                SkillDevelopmentGridView.page(0);

                return false;
            }

        </script>
    </rad:RadScriptBlock>
    <rad:RadScriptBlock ID="rsbRecentlyAdded" runat="server">
        <script type="text/javascript">
            var recentListView;
            var recentGridView;

            //initialize RecentlyAddedCourse Course
            function initializeRecentCourse() {
                $ = $telerik.$;
                //this will fire the OnCommand event
                recentListView = $find("<%= lvRecentlyAddedCourses.ClientID%>");
                var listView = recentListView;
                listView.set_pageSize(GLOBAL_PAGESIZE);
                listView.rebind();

                listView.page(0);

                recentGridView = $find("<%= gridRecentCourses.ClientID%>");

                $(".rcPager .pagePrev").click(function (e) {
                    listView.page(listView.get_currentPageIndex() - 1);
                });
                $(".rcPager .pageNext").click(function (e) {
                    listView.page(listView.get_currentPageIndex() + 1);
                });

//                $("#recentListView").on("click", ".rc-item", function (e) {
//                    listView.toggleSelection($(this).index());
//                });

                viewAllRecently = false;
            }

            //Bind RecentlyAddedCourse 
            function bindRecentCourseList() {
                var listView = recentListView;
                var gridView = recentGridView;
                var startRowIndex = listView.get_currentPageIndex() * listView.get_pageSize(),
                maximumRows = listView.get_pageSize(),
                sortExpression = listView.get_sortExpressions().toLinq();

                //check if theres a sessioned data
                var sData = sessionStorage.getItem('rp' + listView.get_currentPageIndex());

                if (sData != null && viewAllRecently == false) {
                    //scrollTo("mandatoryListView");
                    sData = JSON.parse(sData);
                    listView.set_dataSource(sData);
                    listView.dataBind();

                    gridView.set_dataSource(sData);
                    gridView.dataBind();
                    lazyload();

                    console.log("loaded sessioned recently added" + listView.get_currentPageIndex());

                } else {

                    $(".js-rc-loader").show();
                    $("#recentListView").addClass("display-none");
                    $("#recentGridView").addClass("display-none");

                    console.log("loaded server recently added" + listView.get_currentPageIndex());



                    $.ajax({
                        type: "POST",
                        data: "{startRowIndex: " + startRowIndex + ", maximumRows: " + maximumRows + ", sortExpression: '" + sortExpression + "'}",
                        contentType: "application/json; charset=utf-8",
                        url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "GetRecentlyAddedList",
                        dataType: 'json',
                        success: function (res) {
                            var count = res.d.count;
                            var result = res.d.data;
                            $("#recentListView").removeClass("display-none");
                            $("#recentGridView").removeClass("display-none");

                            listView.set_virtualItemCount(count);
                            if (count > 4) {
                                $(".rcPager .pagePrev").removeClass("display-none");
                                $(".rcPager .pageNext").removeClass("display-none");
                                $(".rcViews").removeClass("display-none");
                            }

                            if (listView.get_pageSize() > 4 && count > 4) {
                                $("#<%= btnViewAllListRAC.ClientID %>").addClass("display-none");
                                $("#<%= btnBackListRAC.ClientID %>").removeClass("display-none");

                                $("#<%= btnViewAllGridRAC.ClientID %>").addClass("display-none");
                                $("#<%= btnBackGridRAC.ClientID %>").removeClass("display-none");
                            } else {
                                viewAllRecently = false;
                                $("#<%= btnViewAllListRAC.ClientID %>").removeClass("display-none");
                                $("#<%= btnBackListRAC.ClientID %>").addClass("display-none");

                                $("#<%= btnViewAllGridRAC.ClientID %>").removeClass("display-none");
                                $("#<%= btnBackGridRAC.ClientID %>").addClass("display-none");
                            }

                            if (viewAllRecently) {
                                $(".rcPager .pagePrev").addClass("display-none");
                                $(".rcPager .pageNext").addClass("display-none");
                            }

                            listView.set_dataSource(result);
                            //console.log(result);
                            $(".js-rc-loader").hide();
                            listView.dataBind();

                            recentGridView.set_dataSource(result);
                            recentGridView.dataBind();



                            lazyload();

                            if (sData == null)
                                sessionStorage.setItem('rp' + listView.get_currentPageIndex(), JSON.stringify(result));

                            if (firstLoad == true) {
                                firstLoad = false;
                            }
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            var code = XMLHttpRequest.status;
                            if (code == "401")
                                window.location.reload();
                            //else
                            //alert(XMLHttpRequest.responseText);
                        }
                    });
                }
            }

            //Client Events RecentlyAddedCourse
            function rcOnListViewDataBinding(sender, args) {
                sender.set_selectedIndexes([]);
            }

            function rcOnListViewCommand(sender, args) {
                //cancel command event to prevent postback
                args.set_cancel(true);
                bindRecentCourseList();
            }




            function btnViewAllListRAC_Click() {
                viewAllRecently = true;
                recentListView.set_pageSize(999);
                recentListView.page(0);
                recentListView.rebind();
                recentGridView.set_pageSize(999);
                recentGridView.page(0);

                return false;
            }

            function btnBackListRAC_Click() {
                viewAllRecently = false;
                $("#<%= btnViewAllListRAC.ClientID %>").removeClass("display-none");
                $("#<%= btnBackListRAC.ClientID %>").addClass("display-none");

                $("#<%= btnViewAllGridRAC.ClientID %>").removeClass("display-none");
                $("#<%= btnBackGridRAC.ClientID %>").addClass("display-none");
                recentListView.set_pageSize(GLOBAL_PAGESIZE);
                recentListView.page(0);
                recentListView.rebind();

                recentGridView.set_pageSize(GLOBAL_PAGESIZE);
                recentGridView.page(0);


                return false;
            }


        </script>
    </rad:RadScriptBlock>
    <rad:RadScriptBlock ID="rsbTrending" runat="server">
        <script type="text/javascript">
            var trendingListView;
            var trendingGridView;
            var viewAllTrending;
            //--Start TrendingCourse Binding

            //initialize TrendingCourse Course
            function initializeTrendingCourse() {
                $ = $telerik.$;
                //this will fire the OnCommand event
                trendingListView = $find("<%= lvTrendingCourses.ClientID%>");
                trendingGridView = $find("<%= gridTrendingCourses.ClientID%>");
                var listView = trendingListView;
                listView.rebind();

                listView.set_pageSize(4);
                listView.page(0);


                $(".tcPager .pagePrev").click(function (e) {
                    listView.page(listView.get_currentPageIndex() - 1);
                });
                $(".tcPager .pageNext").click(function (e) {
                    listView.page(listView.get_currentPageIndex() + 1);
                });

//                $("#trendingListView").on("click", ".tc-item", function (e) {
//                    listView.toggleSelection($(this).index());
//                });

                viewAllTrending = false;
            }

            //Bind TrendingCourse 
            function bindTrendingCourseList() {
                var listView = trendingListView;
                var gridView = trendingGridView;
                var startRowIndex = listView.get_currentPageIndex() * listView.get_pageSize(),
                maximumRows = listView.get_pageSize(),
                sortExpression = listView.get_sortExpressions().toLinq();

                //check if theres a sessioned data
                var sData = sessionStorage.getItem('tp' + listView.get_currentPageIndex());

                if (sData != null && viewAllTrending == false) {
                    //scrollTo("recommendedLeadershipCoursesListView");
                    sData = JSON.parse(sData);
                    listView.set_dataSource(sData);
                    listView.dataBind();

                    gridView.set_dataSource(sData);
                    gridView.dataBind();
                    lazyload();


                    $(".tcPager .pagePrev").removeClass("display-none");
                    $(".tcPager .pageNext").removeClass("display-none");

                    console.log("loaded sessioned leadership");

                } else {

                    $(".js-tc-loader").show();
                    $("#trendingListView").addClass("display-none");
                    $("#trendingGridView").addClass("display-none");

                    $.ajax({
                        type: "POST",
                        data: "{startRowIndex: " + startRowIndex + ", maximumRows: " + maximumRows + ", sortExpression: '" + sortExpression + "'}",
                        contentType: "application/json; charset=utf-8",
                        url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "GetTrendingCourseList",
                        dataType: 'json',
                        success: function (res) {
                            var count = res.d.count;
                            var result = res.d.data;
                            listView.set_virtualItemCount(count);

                            $("#trendingListView").removeClass("display-none");
                            $("#trendingGridView").removeClass("display-none");

                            if (count > 0) {
                                $(".tcPager .pagePrev").removeClass("display-none");
                                $(".tcPager .pageNext").removeClass("display-none");
                                $(".tcViews").removeClass("display-none");
                            }
                            if (count <= 4) {
                                $("#<%= btnViewAllListTC.ClientID %>").addClass("display-none");
                                $("#<%= btnBackListTC.ClientID %>").addClass("display-none");

                                $("#<%= btnViewAllGridTC.ClientID %>").addClass("display-none");
                                $("#<%= btnBackGridTC.ClientID %>").addClass("display-none");
                            }

                            if (listView.get_pageSize() > 4 && count > 4) {
                                $("#<%= btnViewAllListTC.ClientID %>").addClass("display-none");
                                $("#<%= btnBackListTC.ClientID %>").removeClass("display-none");

                                $("#<%= btnViewAllGridTC.ClientID %>").addClass("display-none");
                                $("#<%= btnBackGridTC.ClientID %>").removeClass("display-none");
                            } else {
                                viewAllTrending = false;
                                $("#<%= btnViewAllListTC.ClientID %>").removeClass("display-none");
                                $("#<%= btnBackListTC.ClientID %>").addClass("display-none");

                                $("#<%= btnViewAllGridTC.ClientID %>").removeClass("display-none");
                                $("#<%= btnBackGridTC.ClientID %>").addClass("display-none");
                            }

                            if (viewAllTrending) {
                                $(".tcPager .pagePrev").addClass("display-none");
                                $(".tcPager .pageNext").addClass("display-none");
                            }

                            listView.set_dataSource(result);
                            listView.dataBind();

                            gridView.set_dataSource(result);
                            gridView.dataBind();
                            lazyload();

                            $(".js-tc-loader").hide();

                            if (sData == null)
                                sessionStorage.setItem('tp' + listView.get_currentPageIndex(), JSON.stringify(result));

                            if ($("#pnlLvSkillDevelopments").length > 0 && firstLoad == true)
                                initializeSkillDevelopment();

                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            var code = XMLHttpRequest.status;
                            if (code == "401")
                                window.location.reload();
                            //else
                            //alert(XMLHttpRequest.responseText);
                        }
                    });
                }
            }

            //Client Events TrendingCourse
            function tcOnListViewDataBinding(sender, args) {
                sender.set_selectedIndexes([]);
            }

            function tcOnListViewCommand(sender, args) {
                //cancel command event to prevent postback
                args.set_cancel(true);
                //dabind RadListView programmatically
                bindTrendingCourseList();
            }


            //btnView/btnBack Click Trending

            function btnViewAllListTC_Click() {
                viewAllTrending = true;
                trendingListView.set_pageSize(999);
                trendingListView.page(0);
                trendingListView.rebind();
                trendingGridView.set_pageSize(999);
                trendingGridView.page(0);

                return false;
            }

            function btnBackListTC_Click() {
                viewAllTrending = false;

                $("#<%= btnViewAllListTC.ClientID %>").removeClass("display-none");
                $("#<%= btnBackListTC.ClientID %>").addClass("display-none");

                $("#<%= btnViewAllGridTC.ClientID %>").removeClass("display-none");
                $("#<%= btnBackGridTC.ClientID %>").addClass("display-none");


                trendingListView.set_pageSize(4);
                trendingListView.page(0);
                trendingListView.rebind();

                trendingGridView.set_pageSize(4);
                trendingGridView.page(0);

                return false;
            }
        </script>
    </rad:RadScriptBlock>

    <rad:RadScriptBlock ID="rsbMainScript" runat="server">
        <script type="text/javascript">
            function pageLoad() {
                sessionStorage.clear();
                var tab = '<%= Request.QueryString["Tab"] %>';
      
                if (ISMOBILE)
                    GLOBAL_PAGESIZE = 1
                else
                    GLOBAL_PAGESIZE = 4

                if (tab.length == 0) {

                    //                    if ($("#pnlLvTrendingCourses").length > 0)
                    //                        initializeTrendingCourse();
                    //                    if ($("#pnlLvRecommendedCourses").length > 0)
                    //                        initializeRecommendedCourse();
                    firstLoad = true;
                    if ($("#pnlLvRecommendedCoursesForYou").length > 0)
                        initializeRecommendedLeadershipCourse();
                    if ($("#pnlLvMandatoryCourses").length > 0 && (<%= isLoggedIn == true ? 1 : 0 %>) == 0)
                        initializeMandatoryCourse();
                }
            }

            function scrollTo(elementID) {
                elementID = "#" + elementID;
                if ($("#recommendedLeadershipCoursesListView").offset().top - $(window).scrollTop() - 50 < 0)
                $('body,html').animate({
                    scrollTop: $(elementID).offset().top - 150
                }, 300);
            }

            function btnSwitchView_Click() {
                if ($('#<%= pnlListView.ClientID %>').hasClass("display-none")) {

                    $("#<%= pnlListView.ClientID %>").removeClass("display-none");
                    $("#<%= pnlGridView.ClientID %>").addClass("display-none");
                    $("#<%= btnSwitchToGridView.ClientID %>").addClass("display-none");
                    $("#<%= btnSwitchToListView.ClientID %>").removeClass("display-none");
                    //switch to gridview
                    $("#<%= lblTab2.ClientID %>").html("Grid View");
                    sidebarFull();
                    return false;
                } else {
                    //switch to list view

                    $("#<%= pnlListView.ClientID %>").addClass("display-none");
                    $("#<%= pnlGridView.ClientID %>").removeClass("display-none");
                    $("#<%= btnSwitchToListView.ClientID %>").addClass("display-none");
                    $("#<%= btnSwitchToGridView.ClientID %>").removeClass("display-none");
                    
                    
                    $("#<%= lblTab2.ClientID %>").html("List View");
                    sidebarFull();
                    return false;
                }
            }

            function inGridView() {
                if ($('#<%= pnlListView.ClientID %>').hasClass("display-none"))
                    return false;
                else
                    return true;
            }

            function sortMandatory() {
                mandatoryListView.get_sortExpressions().clear();
                mandatoryListView.get_sortExpressions().add('CourseTitle', "DESC");
                mandatoryListView.rebind();
            }

        </script>
    </rad:RadScriptBlock>

    <rad:RadScriptBlock ID="rsbRecommended" runat="server">
        <script type="text/javascript">
            var recommendedListView;
            var recommendedGridView;

            //initialize Recommended Course
            function initializeRecommendedCourse() {
                $ = $telerik.$;
                //this will fire the OnCommand event
                recommendedListView = $find("<%= lvRecommendedCourses.ClientID%>");
                var listView = recommendedListView;
                listView.rebind();
                listView.set_pageSize(4);
                listView.page(0);

                recommendedGridView = $find("<%= gridRecommendedCourses.ClientID%>");

                $(".rmPager .pagePrev").click(function (e) {
                    listView.page(listView.get_currentPageIndex() - 1);
                });
                $(".rmPager .pageNext").click(function (e) {
                    listView.page(listView.get_currentPageIndex() + 1);
                });

                $("#recommendedListView").on("click", ".rm-item", function (e) {
                    listView.toggleSelection($(this).index());
                });

            }

            //Bind RecommendedAddedCourse 
            function bindRecommendedCourseList() {
                var listView = recommendedListView;
                var startRowIndex = listView.get_currentPageIndex() * listView.get_pageSize(),
                maximumRows = listView.get_pageSize(),
                sortExpression = listView.get_sortExpressions().toLinq();

                //make a call to get the data
                PageMethods.GetRecommendedCourseData(startRowIndex, maximumRows, sortExpression,
                function (res) {

                    var result = res.data;
                    var count = res.count;

                    listView.set_virtualItemCount(count);
                    if (count > 0) {
                        $(".rmPager .pagePrev").removeClass("display-none");
                        $(".rmPager .pageNext").removeClass("display-none");
                        $(".rmViews").removeClass("display-none");
                    }

                    listView.set_dataSource(result);
                    //console.log(result);
                    $(".js-rm-loader").hide();
                    listView.dataBind();

                    recommendedGridView.set_dataSource(result);
                    recommendedGridView.dataBind();

                    if (listView.get_pageSize() > 4) {
                        $("#<%= btnViewAllListRM.ClientID %>").addClass("display-none");
                        $("#<%= btnBackListRM.ClientID %>").removeClass("display-none");

                        $("#<%= btnViewAllGridRM.ClientID %>").addClass("display-none");
                        $("#<%= btnBackGridRM.ClientID %>").removeClass("display-none");
                    } else {
                        $("#<%= btnViewAllListRM.ClientID %>").removeClass("display-none");
                        $("#<%= btnBackListRM.ClientID %>").addClass("display-none");

                        $("#<%= btnViewAllGridRM.ClientID %>").removeClass("display-none");
                        $("#<%= btnBackGridRM.ClientID %>").addClass("display-none");
                    }


                },
                function (error) {
                    var code = error._statusCode;
                    if (code == 401)
                        location.reload();
                    else if (code == 500)
                        radalert("Something went while submitting your report. Please try again.", 330, 180, "Error Message", "");
                });

            }

            //Client Events RecentlyAddedCourse
            function rmOnListViewDataBinding(sender, args) {
                sender.set_selectedIndexes([]);
            }

            function rmOnListViewCommand(sender, args) {
                //cancel command event to prevent postback
                args.set_cancel(true);
                bindRecommendedCourseList();
            }

            function btnViewAllListRM_Click() {
                recommendedListView.set_pageSize(999);
                recommendedListView.page(0);
                recommendedListView.rebind();
                recommendedGridView.set_pageSize(999);
                recommendedGridView.page(0);

                return false;
            }

            function btnBackListRM_Click() {

                recommendedListView.set_pageSize(4);
                recommendedListView.page(0);
                recommendedListView.rebind();
                recommendedGridView.set_pageSize(4);
                recommendedGridView.page(0);

                return false;
            }

        </script>
    </rad:RadScriptBlock>
    <rad:RadAjaxLoadingPanel ID="localLoadingPanel" runat="server" CssClass="Loading2"
        Transparency="25" />
    <rad:RadAjaxLoadingPanel ID="noLoadingPanel" runat="server" CssClass="Loading-Empty"
        Transparency="25" />
    <ucCcSidebar:courseCatalogSidebar ID="ccSidebar" runat="server" />
    <rad:RadAjaxManager ID="rampCategorCatalog" runat="server">
        <AjaxSettings>
        </AjaxSettings>
    </rad:RadAjaxManager>
    <rad:RadWindowManager ID="rwmCourseCatalog" RenderMode="Lightweight" EnableShadow="true"
        VisibleOnPageLoad="false" Behaviors="Close, Move" DestroyOnClose="true" Modal="true"
        Opacity="99" runat="server" VisibleStatusbar="false" Skin="Bootstrap">
    </rad:RadWindowManager>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolderMain" runat="server">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/platform/1.3.5/platform.min.js"></script>
    <div class="col-sm-11 col-xs-12 col-md-12 bottom-margin-md">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12 thirty-px-padding">
                        <div class="col-md-12 no-paddings">
                            <ul id="ulBreadcrumb" runat="server" class="breadcrumb">
                                <li><a>
                                    <asp:Label ID="lblTab1" runat="server" Text="Course Catalog" /></a></li>
                                <li>
                                    <asp:Label ID="lblTab2" runat="server" Text="" /></li>
                            </ul>
                        </div>
                        <rad:RadMultiPage ID="RadMultiPage1" runat="server" RenderMode="Lightweight" CssClass="outerMultiPage"
                            RenderSelectedPageOnly="true">
                            <rad:RadPageView ID="rpvCourseCatalog" runat="server">
                                <div class="th">
                                    <asp:LinkButton ID="btnSwitchToListView" runat="server" OnClientClick="btnSwitchView_Click(); return false;"
                                        Font-Underline="false" aria-hidden="true" ToolTip="Switch To List View">
                                        <i id="switchClassListView" runat="server" class="fa fa-th-list"></i>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnSwitchToGridView" runat="server" OnClientClick="btnSwitchView_Click(); return false;"
                                        Font-Underline="false" aria-hidden="true" CssClass="display-none" ToolTip="Switch To Grid View">
                                        <i id="switchClassGridView" runat="server" class="fa fa-th-large"></i>
                                    </asp:LinkButton>
                                </div>
                                <asp:Panel ID="pnlListView" runat="server">
                                    <div class="col-md-12">
                                        <div class="row gray-border-no-pads">
                                            <asp:Panel ID="pnlLvRecommendedCoursesForYou" runat="server" ClientIDMode="Static"
                                                CssClass="height-300">
                                                <div class="widgets black-header m-category-header">
                                                    <div class="col-md-6 font-bold col-xs-9 pull-left no-padding">
                                                        <asp:Label ID="lblLeadershipCatalogTitle" Text="Leadership Courses" runat="server" />
                                                    </div>
                                                    <div class="col-md-6 font-bold col-xs-3 pull-right no-padding">
                                                        <div class="row-fluid rlcViews display-none">
                                                            <div class="pull-right">
                                                                <asp:LinkButton ID="btnViewAllListRCFY" runat="server" Text="View All" OnClientClick="btnViewAllListRCFY_Click(); return false;"
                                                                    CssClass="gold"></asp:LinkButton>
                                                                <asp:LinkButton ID="btnBackListRCFY" runat="server" OnClientClick="btnBackListRCFY_Click(); return false;"
                                                                    Font-Underline="false" aria-hidden="true" CssClass="display-none gold"><i class="fa fa-long-arrow-left"></i></asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="rlc-container" class="rlc-container col-md-12">
                                                    <div class="loader js-rlc-loader">
                                                    </div>
                                                    <rad:RadListView ID="lvRecommendedLeadershipCourses" runat="server" AllowPaging="true"
                                                        PageSize="4" AllowMultiFieldSorting="true">
                                                        <LayoutTemplate>
                                                            <div id="recommendedLeadershipCoursesListView">
                                                                <div class="rlcPager">
                                                                    <a class="pagePrev fa fa-chevron-left pull-left relative-vr-left-center gray-arrow no-underline-hover display-none"
                                                                        href="javascript:void(0);" title="Go to previous page"></a><a class="pageNext fa fa-chevron-right pull-right relative-vr-right-center gray-arrow no-underline-hover display-none"
                                                                            href="javascript:void(0);" title="Go to next page"></a>
                                                                    <div id="rlc-items" class="display-flow margin-side-15">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </LayoutTemplate>
                                                        <ClientSettings>
                                                            <DataBinding ItemPlaceHolderID="rlc-items">
                                                                <ItemTemplate>
                                                                <div class="col-md-3">
                                                                 <%-- <a id="hllvRecommendedLeadershipCourses" onclick='confirmLaunchCourse("#= EncryptedCourseID #");return false;'  href="javascript:void(0);" style="color:Black;" title="#= CourseTitle #">--%>
                                                                    <div id="divRecommendedLeadershipCourses" runat="server" class="rlc-item lv rlvI" onclick='confirmLaunchCourse("#= EncryptedCourseID #");return false;' style="color:Black;" title="#= CourseTitle #" >
                                                                        <div class="photo-container">
                                                                            <img src='Media/Images/empty.png' data-src='Media/Uploads/CourseImg/#= CourseID #/#= CourseImage #' style="height:130px;width:190px;" onerror="this.src='Media/Uploads/CourseImg/No_image.jpg'" class="lazyload"></img>
                                                                        </div>
                                                                            <div class="category font-bold text-trim">
                                                                                <p class="clamp-line-1">
                                                                                    #= CourseTitle #
                                                                                </p>
                                                                            </div>
                                                                             <%--<div class="category">
                                                                                <p class="clamp-line-1">
                                                                                    #= CourseType #
                                                                                </p>
                                                                            </div>--%>
                                                                            <div class="category">
                                                                                <p class="clamp-line-1 font-desc">
                                                                                    #= CourseDuration # 
                                                                                </p>
                                                                            </div>
                                                                            <%--<div class="category">
                                                                                <p class="clamp-line-1">
                                                                                    #= CourseCategory #
                                                                                </p>
                                                                            </div>--%>
                                                                            <%--<div class="category">
                                                                                <p class="clamp-line-1">
                                                                                    #= CourseSubcategory #
                                                                                </p>
                                                                            </div>--%>
                                                                            <div class="category">
                                                                                <p class="clamp-line-3 font-desc">
                                                                                    
                                                                                    #= CourseDescription #
                                                                                </p>
                                                                            </div>
                                                                            <div class="clearfix">
                                                                            </div>
                                                                    </div>
                                                                    <%--</a>--%>
                                                                    </div>
                                                                </ItemTemplate>
                                                                <EmptyDataTemplate>
                                                                No Recommended Leadership Course.
                                                                </EmptyDataTemplate>
                                                                <DataService EnableCaching="true" />
                                                            </DataBinding>
                                                            <ClientEvents OnCommand="rlcfyOnListViewCommand" OnDataBinding="rlcfyOnListViewDataBinding">
                                                            </ClientEvents>
                                                        </ClientSettings>
                                                    </rad:RadListView>
                                                </div>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlLvMandatoryCourses" runat="server" ClientIDMode="Static" CssClass="height-300">
                                                <div class="widgets black-header m-category-header">
                                                    <div class="col-md-6 font-bold col-xs-9 pull-left no-padding">
                                                        <asp:Label runat="server" ID="lblMandatoryCourses" Text="<%$ Resources:LocalizedResource, MandatoryCourses %>" ></asp:Label> </div>
                                                    <div class="col-md-6 font-bold col-xs-3 pull-right no-padding">
                                                        <div class="row-fluid mcViews display-none">
                                                            <div class="pull-right">
                                                                <asp:LinkButton ID="btnViewAllListMC" runat="server" Text="View All" OnClientClick="btnViewAllListMC_Click(); return false;"
                                                                    CssClass="gold"></asp:LinkButton>
                                                                <asp:LinkButton ID="btnBackListMC" runat="server" OnClientClick="btnBackListMC_Click(); return false;"
                                                                    Font-Underline="false" aria-hidden="true" CssClass="display-none gold"><i class="fa fa-long-arrow-left"></i></asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="mc-container" class="mc-container col-md-12">
                                                    <div class="loader js-mc-loader">
                                                    </div>
                                                    <rad:RadListView ID="lvMandatoryCourses" runat="server" AllowPaging="true" PageSize="4"
                                                        AllowMultiFieldSorting="true">
                                                        <LayoutTemplate>
                                                            <div id="mandatoryListView">
                                                                <div class="mcPager">
                                                                    <a class="pagePrev fa fa-chevron-left pull-left relative-vr-left-center gray-arrow no-underline-hover display-none"
                                                                        href="javascript:void(0);" title="Go to previous page"></a><a class="pageNext fa fa-chevron-right pull-right relative-vr-right-center gray-arrow no-underline-hover display-none"
                                                                            href="javascript:void(0);" title="Go to next page"></a>
                                                                    <div id="mc-items" class="display-flow margin-side-15">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </LayoutTemplate>
                                                        <ClientSettings>
                                                            <DataBinding ItemPlaceHolderID="mc-items">
                                                                <ItemTemplate>
                                                                <div class="col-md-3">
                                                                  <a id="hllvMandatoryCourses" onclick='confirmLaunchCourse("#= EncryptedCourseID #");return false;'  href="javascript:void(0);" style="color:Black;" title="#= CourseTitle #">
                                                                    <div id="divMandatoryCourses" runat="server" class="mc-item lv rlvI">
                                                                        <div class="photo-container">
                                                                            <img src='Media/Images/empty.png' data-src='Media/Uploads/CourseImg/#= CourseID #/#= CourseImage #' style="height:130px;width:190px;" onerror="this.src='Media/Uploads/CourseImg/No_image.jpg'"  class="lazyload"></img>
                                                                        </div>
                                                                            <div class="category font-bold text-trim">
                                                                                <p class="clamp-line-1">
                                                                                    #= CourseTitle #
                                                                                </p>
                                                                            </div>
                                                                             <%--<div class="category">
                                                                                <p class="clamp-line-1">
                                                                                    #= CourseType #
                                                                                </p>
                                                                            </div>--%>
                                                                            <div class="category">
                                                                                <p class="clamp-line-1 font-desc">
                                                                                    #= CourseDuration # 
                                                                                </p>
                                                                            </div>
                                                                            <%--<div class="category">
                                                                                <p class="clamp-line-1">
                                                                                    #= CourseCategory #
                                                                                </p>
                                                                            </div>--%>
                                                                            <%--<div class="category">
                                                                                <p class="clamp-line-1">
                                                                                    #= CourseSubcategory #
                                                                                </p>
                                                                            </div>--%>
                                                                            <div class="category">
                                                                                <p class="clamp-line-3 font-desc">
                                                                                    
                                                                                    #= CourseDescription #
                                                                                </p>
                                                                            </div>
                                                                            <div class="clearfix">
                                                                            </div>
                                                                    </div>
                                                                    </a>
                                                                    </div>
                                                                </ItemTemplate>
                                                                <EmptyDataTemplate>
                                                                No Mandatory Course.
                                                                </EmptyDataTemplate>
                                                                <DataService EnableCaching="true" />
                                                            </DataBinding>
                                                            <ClientEvents OnCommand="mcOnListViewCommand" OnDataBinding="mcOnListViewDataBinding">
                                                            </ClientEvents>
                                                        </ClientSettings>
                                                    </rad:RadListView>
                                                </div>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlLvRecommendedCourses" runat="server" ClientIDMode="Static" CssClass="height-300"
                                                Visible="false">
                                                <div class="widgets black-header m-category-header">
                                                    <div class="col-md-6 font-bold col-xs-9 pull-left no-padding ">
                                                        Recommended Courses</div>
                                                    <div class="col-md-6 font-bold col-xs-3 pull-right no-padding">
                                                        <div class="row-fluid rcViews display-none">
                                                            <div class="pull-right">
                                                                <asp:LinkButton ID="btnViewAllListRM" runat="server" Text="View All" OnClientClick="btnViewAllListRM_Click(); return false;"
                                                                    CssClass="gold"></asp:LinkButton>
                                                                <asp:LinkButton ID="btnBackListRM" runat="server" OnClientClick="btnBackListRM_Click(); return false;"
                                                                    Font-Underline="false" aria-hidden="true" CssClass="display-none gold"><i class="fa fa-long-arrow-left"></i></asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="Div1" class="demo-container col-md-12">
                                                    <div class="loader js-rm-loader">
                                                    </div>
                                                    <rad:RadListView ID="lvRecommendedCourses" runat="server" AllowPaging="true" PageSize="4">
                                                        <LayoutTemplate>
                                                            <div id="recommendedListView">
                                                                <div class="rmPager">
                                                                    <a class="pagePrev fa fa-chevron-left pull-left relative-vr-left-center gray-arrow no-underline-hover display-none"
                                                                        href="javascript:void(0);" title="Go to previous page"></a><a class="pageNext fa fa-chevron-right pull-right relative-vr-right-center gray-arrow no-underline-hover display-none"
                                                                            href="javascript:void(0);" title="Go to next page"></a>
                                                                    <div id="rmItems" class="display-flow margin-side-15">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </LayoutTemplate>
                                                        <ClientSettings>
                                                            <DataBinding ItemPlaceHolderID="rmItems">
                                                                <ItemTemplate>
                                                                <div class="col-md-3">
                                                                  <a id="hllvRecommendedCourses" onclick='confirmLaunchCourse("#= EncryptedCourseID #");return false;'  href="javascript:void(0);" style="color:Black;"  title=" #= CourseTitle #" class="hover-pointer">
                                                                    <div id="divRecommendedCourses" runat="server" class="tc-item lv rlvI">
                                                                        <div class="photo-container">
                                                                            <img src='Media/Images/empty.png' data-src='Media/Uploads/CourseImg/#= CourseID #/#= CourseImage #' onerror="this.src='Media/Uploads/CourseImg/No_image.jpg'" style="height:130px;width:190px;"  class="lazyload"></img>
                                                                        </div>
                                                                        <div class="category font-bold text-trim">
                                                                                <p class="clamp-line-1">
                                                                                    #= CourseTitle #
                                                                                </p>
                                                                            </div>
                                                                             <%--<div class="category">
                                                                                <p class="clamp-line-1">
                                                                                    #= CourseType #
                                                                                </p>
                                                                            </div>--%>
                                                                            <div class="category">
                                                                                <p class="clamp-line-1 font-desc">
                                                                                    #= CourseDuration # 
                                                                                </p>
                                                                            </div>
                                                                            <%--<div class="category">
                                                                                <p class="clamp-line-1">
                                                                                    #= CourseCategory #
                                                                                </p>
                                                                            </div>--%>
                                                                            <%--<div class="category">
                                                                                <p class="clamp-line-1">
                                                                                    #= CourseSubcategory #
                                                                                </p>
                                                                            </div>--%>
                                                                            <div class="category">
                                                                                <p class="clamp-line-3 font-desc" style="display: -webkit-box;">
                                                                                    
                                                                                    #= CourseDescription #
                                                                                </p>
                                                                            </div>
                                                                        <div class="clearfix">
                                                                        </div>
                                                                    </div>
                                                                    </a>
                                                                    </div>
                                                                </ItemTemplate>
                                                                <EmptyDataTemplate>
                                                                    No Recently Added Course.
                                                                </EmptyDataTemplate>
                                                            </DataBinding>
                                                            <ClientEvents OnCommand="rmOnListViewCommand" OnDataBinding="rmOnListViewDataBinding">
                                                            </ClientEvents>
                                                        </ClientSettings>
                                                    </rad:RadListView>
                                                </div>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlLvSkillDevelopments" runat="server" ClientIDMode="Static" CssClass="height-300">
                                                <div class="widgets black-header m-category-header">
                                                    <div class="col-md-6 font-bold col-xs-9 pull-left no-padding">
                                                        <asp:Label runat="server" ID="lblSkillDevelopmentCourses" Text="<%$ Resources:LocalizedResource, SkillDevelopmentCourses %>" ></asp:Label></div>
                                                    <div class="col-md-6 font-bold col-xs-3 pull-right no-padding">
                                                        <div class="row-fluid mtViews display-none">
                                                            <div class="pull-right">
                                                                <asp:LinkButton ID="btnViewAllListMT" runat="server" Text="View All" OnClientClick="btnViewAllListMT_Click(); return false;"
                                                                    CssClass="gold"></asp:LinkButton>
                                                                <asp:LinkButton ID="btnBackListMT" runat="server" OnClientClick="btnBackListMT_Click(); return false;"
                                                                    Font-Underline="false" aria-hidden="true" CssClass="display-none gold"><i class="fa fa-long-arrow-left"></i></asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="Div4" class="demo-container col-md-12">
                                                    <div class="loader js-mt-loader">
                                                    </div>
                                                    <rad:RadListView ID="lvSkillDevelopments" runat="server" AllowPaging="true" PageSize="4">
                                                        <LayoutTemplate>
                                                            <div id="SkillDevelopmentTakeListView">
                                                                <div class="mtPager">
                                                                    <a class="pagePrev fa fa-chevron-left pull-left relative-vr-left-center gray-arrow no-underline-hover display-none"
                                                                        href="javascript:void(0);" title="Go to previous page"></a><a class="pageNext fa fa-chevron-right pull-right relative-vr-right-center gray-arrow no-underline-hover display-none"
                                                                            href="javascript:void(0);" title="Go to next page"></a>
                                                                    <div id="mtItems" class="display-flow margin-side-15">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </LayoutTemplate>
                                                        <ClientSettings>
                                                            <DataBinding ItemPlaceHolderID="mtItems">
                                                                <ItemTemplate>
                                                                <div class="col-md-3">
                                                                  <a id="hllvSkillDevelopments" onclick='confirmLaunchCourse("#= EncryptedCourseID #");return false;'  href="javascript:void(0);" style="color:Black;"  title=" #= CourseTitle #" class="hover-pointer">
                                                                    <div id="divSkillDevelopments" runat="server" class="tc-item lv rlvI">
                                                                        <div class="photo-container">
                                                                            <img src='Media/Images/empty.png' data-src='Media/Uploads/CourseImg/#= CourseID #/#= CourseImage #' onerror="this.src='Media/Uploads/CourseImg/No_image.jpg'" style="height:130px;width:190px;"  class="lazyload"></img>
                                                                        </div>
                                                                        <div class="category font-bold text-trim">
                                                                                <p class="clamp-line-1">
                                                                                    #= CourseTitle #
                                                                                </p>
                                                                            </div>
                                                                             <%--<div class="category">
                                                                                <p class="clamp-line-1">
                                                                                    #= CourseType #
                                                                                </p>
                                                                            </div>--%>
                                                                            <div class="category">
                                                                                <p class="clamp-line-1 font-desc">
                                                                                    #= CourseDuration # 
                                                                                </p>
                                                                            </div>
                                                                            <%--<div class="category">
                                                                                <p class="clamp-line-1">
                                                                                    #= CourseCategory #
                                                                                </p>
                                                                            </div>--%>
                                                                            <%--<div class="category">
                                                                                <p class="clamp-line-1">
                                                                                    #= CourseSubcategory #
                                                                                </p>
                                                                            </div>--%>
                                                                            <div class="category">
                                                                                <p class="clamp-line-3 font-desc" style="display: -webkit-box;">
                                                                                    
                                                                                    #= CourseDescription #
                                                                                </p>
                                                                            </div>
                                                                        <div class="clearfix">
                                                                        </div>
                                                                    </div>
                                                                    </a>
                                                                    </div>
                                                                </ItemTemplate>
                                                                <EmptyDataTemplate>
                                                                    No Skill Development Course.
                                                                </EmptyDataTemplate>
                                                            </DataBinding>
                                                            <ClientEvents OnCommand="mtOnListViewCommand" OnDataBinding="mtOnListViewDataBinding">
                                                            </ClientEvents>
                                                        </ClientSettings>
                                                    </rad:RadListView>
                                                </div>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlLvRecentlyAddedCourses" runat="server" ClientIDMode="Static" CssClass="height-300">
                                                <div class="widgets black-header m-category-header">
                                                    <div class="col-md-6 font-bold col-xs-9 pull-left no-padding">
                                                        <asp:Label runat="server" ID="lblRecentlyAddedCourses" Text="<%$ Resources:LocalizedResource, RecentlyAddedCourses %>" ></asp:Label></div>
                                                    <div class="col-md-6 font-bold col-xs-3 pull-right no-padding">
                                                        <div class="row-fluid rcViews display-none">
                                                            <div class="pull-right">
                                                                <asp:LinkButton ID="btnViewAllListRAC" runat="server" Text="View All" OnClientClick="btnViewAllListRAC_Click(); return false;"
                                                                    CssClass="gold"></asp:LinkButton>
                                                                <asp:LinkButton ID="btnBackListRAC" runat="server" OnClientClick="btnBackListRAC_Click(); return false;"
                                                                    Font-Underline="false" aria-hidden="true" CssClass="display-none gold"><i class="fa fa-long-arrow-left"></i></asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="Div3" class="demo-container col-md-12">
                                                    <div class="loader js-rc-loader">
                                                    </div>
                                                    <rad:RadListView ID="lvRecentlyAddedCourses" runat="server" AllowPaging="true" PageSize="4">
                                                        <LayoutTemplate>
                                                            <div id="recentListView">
                                                                <div class="rcPager">
                                                                    <a class="pagePrev fa fa-chevron-left pull-left relative-vr-left-center gray-arrow no-underline-hover display-none"
                                                                        href="javascript:void(0);" title="Go to previous page"></a><a class="pageNext fa fa-chevron-right pull-right relative-vr-right-center gray-arrow no-underline-hover display-none"
                                                                            href="javascript:void(0);" title="Go to next page"></a>
                                                                    <div id="rcItems" class="display-flow margin-side-15">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </LayoutTemplate>
                                                        <ClientSettings>
                                                            <DataBinding ItemPlaceHolderID="rcItems">
                                                                <ItemTemplate>
                                                                <div class="col-md-3">
                                                                  <a id="hllvRecentlyAddedCourses" onclick='confirmLaunchCourse("#= EncryptedCourseID #");return false;'  href="javascript:void(0);" style="color:Black;"  title=" #= CourseTitle #" class="hover-pointer">
                                                                    <div id="divRecentCourses" runat="server" class="tc-item lv rlvI">
                                                                        <div class="photo-container">
                                                                            <img src='Media/Images/empty.png' data-src='Media/Uploads/CourseImg/#= CourseID #/#= CourseImage #' onerror="this.src='Media/Uploads/CourseImg/No_image.jpg'" style="height:130px;width:190px;"  class="lazyload"></img>
                                                                        </div>
                                                                        <div class="category font-bold text-trim">
                                                                                <p class="clamp-line-1">
                                                                                    #= CourseTitle #
                                                                                </p>
                                                                            </div>
                                                                             <%--<div class="category">
                                                                                <p class="clamp-line-1">
                                                                                    #= CourseType #
                                                                                </p>
                                                                            </div>--%>
                                                                            <div class="category">
                                                                                <p class="clamp-line-1 font-desc">
                                                                                    #= CourseDuration # 
                                                                                </p>
                                                                            </div>
                                                                            <%--<div class="category">
                                                                                <p class="clamp-line-1">
                                                                                    #= CourseCategory #
                                                                                </p>
                                                                            </div>--%>
                                                                            <%--<div class="category">
                                                                                <p class="clamp-line-1">
                                                                                    #= CourseSubcategory #
                                                                                </p>
                                                                            </div>--%>
                                                                            <div class="category">
                                                                                <p class="clamp-line-3 font-desc" style="display: -webkit-box;">
                                                                                    
                                                                                    #= CourseDescription #
                                                                                </p>
                                                                            </div>
                                                                        <div class="clearfix">
                                                                        </div>
                                                                    </div>
                                                                    </a>
                                                                    </div>
                                                                </ItemTemplate>
                                                                <EmptyDataTemplate>
                                                                    No Recently Added Course.
                                                                </EmptyDataTemplate>
                                                            </DataBinding>
                                                            <ClientEvents OnCommand="rcOnListViewCommand" OnDataBinding="rcOnListViewDataBinding">
                                                            </ClientEvents>
                                                        </ClientSettings>
                                                    </rad:RadListView>
                                                </div>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlLvTrendingCourses" runat="server" ClientIDMode="Static" CssClass="height-300">
                                                <div class="widgets black-header m-category-header">
                                                    <div class="col-md-6 font-bold col-xs-9 pull-left no-padding">
                                                        <asp:Label runat="server" ID="lblTrendingCourses" Text="<%$ Resources:LocalizedResource, TrendingCourses %>" ></asp:Label></div>
                                                    <div class="col-md-6 font-bold col-xs-3 pull-right no-padding">
                                                        <div class="row-fluid tcViews display-none">
                                                            <div class="pull-right">
                                                                <asp:LinkButton ID="btnViewAllListTC" runat="server" Text="View All" OnClientClick="btnViewAllListTC_Click(); return false;"
                                                                    CssClass="gold"></asp:LinkButton>
                                                                <asp:LinkButton ID="btnBackListTC" runat="server" OnClientClick="btnBackListTC_Click(); return false;"
                                                                    Font-Underline="false" aria-hidden="true" CssClass="display-none gold"><i class="fa fa-long-arrow-left"></i></asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="Div2" class="demo-container col-md-12">
                                                    <div class="loader js-tc-loader">
                                                    </div>
                                                    <rad:RadListView ID="lvTrendingCourses" runat="server" AllowPaging="true" PageSize="4">
                                                        <LayoutTemplate>
                                                            <div id="trendingListView">
                                                                <div class="tcPager">
                                                                    <a class="pagePrev fa fa-chevron-left pull-left relative-vr-left-center gray-arrow no-underline-hover display-none"
                                                                        href="javascript:void(0);" title="Go to previous page"></a><a class="pageNext fa fa-chevron-right pull-right relative-vr-right-center gray-arrow no-underline-hover display-none"
                                                                            href="javascript:void(0);" title="Go to next page"></a>
                                                                    <div id="tc-items" class="display-flow margin-side-15">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </LayoutTemplate>
                                                        <ClientSettings>
                                                            <DataBinding ItemPlaceHolderID="tc-items">
                                                                <ItemTemplate>
                                                                 <div class="col-md-3">
                                                                  <a id="hllvTrendingCourses" onclick='confirmLaunchCourse("#= EncryptedCourseID #");return false;'  href="javascript:void(0);" style="color:Black;" title=" #= CourseTitle #" class="hover-pointer">
                                                                    <div id="divTrendingCourses" runat="server" class="tc-item lv rlvI">
                                                                        <div class="photo-container">
                                                                            <img src='Media/Images/empty.png' data-src='Media/Uploads/CourseImg/#= CourseID #/#= CourseImage #' onerror="this.src='Media/Uploads/CourseImg/No_image.jpg'" style="height:130px;width:190px;" class="lazyload"></img>
                                                                        </div>
                                                                        <div class="category font-bold text-trim">
                                                                                <p class="clamp-line-1">
                                                                                    #= CourseTitle #
                                                                                </p>
                                                                            </div>
                                                                             <%--<div class="category">
                                                                                <p class="clamp-line-1">
                                                                                    #= CourseType #
                                                                                </p>
                                                                            </div>--%>
                                                                            <div class="category">
                                                                                <p class="clamp-line-1 font-desc">
                                                                                    #= CourseDuration # 
                                                                                </p>
                                                                            </div>
                                                                            <%--<div class="category">
                                                                                <p class="clamp-line-1">
                                                                                    #= CourseCategory #
                                                                                </p>
                                                                            </div>--%>
                                                                            <%--<div class="category">
                                                                                <p class="clamp-line-1">
                                                                                    #= CourseSubcategory #
                                                                                </p>
                                                                            </div>--%>
                                                                            <div class="category">
                                                                                <p class="clamp-line-3 font-desc" style="display: -webkit-box;">
                                                                                    
                                                                                    #= CourseDescription #
                                                                                </p>
                                                                            </div>
                                                                        <div class="clearfix">
                                                                        </div>
                                                                    </div>
                                                                    </a>
                                                                    </div>
                                                                    
                                                                </ItemTemplate>
                                                                <EmptyDataTemplate>
                                                                No Trending Course.
                                                                </EmptyDataTemplate>
                                                            </DataBinding>
                                                            <ClientEvents OnCommand="tcOnListViewCommand" OnDataBinding="tcOnListViewDataBinding">
                                                            </ClientEvents>
                                                        </ClientSettings>
                                                    </rad:RadListView>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="pnlGridView" runat="server" CssClass="display-none">
                                    <div class="col-md-12">
                                        <div class="row gray-border-no-pads">
                                            <asp:Panel ID="pnlGridRecommendedLeadershipCourses" runat="server" CssClass="height-300">
                                                <div class="widgets black-header m-category-header">
                                                    <div class="col-md-6 font-bold col-xs-9 pull-left no-padding">
                                                           <asp:Label ID="lblLeadershipCourseGrid" Text="Leadership Courses" runat="server" /></div>
                                                    <div class="col-md-6 font-bold col-xs-3 pull-right no-padding">
                                                        <div class="row-fluid rlcViews display-none">
                                                            <div class="pull-right">
                                                                <asp:LinkButton ID="btnViewAllGridRLCFY" runat="server" Text="View All" CssClass="gold"
                                                                    OnClientClick="btnViewAllListRCFY_Click(); return false;"></asp:LinkButton>
                                                                <asp:LinkButton ID="btnBackGridRLCFY" runat="server" OnClientClick="btnBackListRCFY_Click(); return false;"
                                                                    Font-Underline="false" aria-hidden="true" CssClass="display-none gold"><i class="fa fa-long-arrow-left"></i></asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <rad:RadListView ID="gridRecommendLeadershipCourses" runat="server" RenderMode="Lightweight"
                                                    CssClass="GridLess" AllowPaging="true" PageSize="4">
                                                    <LayoutTemplate>
                                                        <div class="loader js-rlc-loader">
                                                        </div>
                                                        <div id="recommendedLeadershipCoursesGridView">
                                                            <table class="gridMainTable table table-bordered table-striped course-grid">
                                                                <thead>
                                                                    <tr class="rlvHeader">
                                                                        <th class="btn-teal">
                                                                            Title
                                                                        </th>
                                                                        <th class="btn-teal">
                                                                            Description
                                                                        </th>
                                                                        <th class="btn-teal">
                                                                            Duration
                                                                        </th>
                                                                        <th class="btn-teal">
                                                                            Last Updated
                                                                        </th>
                                                                        <th class="btn-teal">
                                                                            Course Path
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="gridrlcitem">
                                                                </tbody>
                                                                <tfoot>
                                                                </tfoot>
                                                            </table>
                                                            <div class="rlcPager margin-bottom-10px">
                                                                <a class="pagePrev fa fa-caret-left gray-arrow no-underline-hover display-none left-arrow-pager"
                                                                    href="javascript:void(0);" title="Go to previous page"></a><a class="pageNext fa fa-caret-right gray-arrow no-underline-hover display-none right-arrow-pager"
                                                                        href="javascript:void(0);" title="Go to next page"></a>
                                                            </div>
                                                        </div>
                                                    </LayoutTemplate>
                                                    <ClientSettings>
                                                        <DataBinding ItemPlaceHolderID="gridrlcitem">
                                                            <ItemTemplate>
                                                                <tr class="rlvI">
                                                                    <td>
                                                                        <p class="clamp-line-2" style="display: -webkit-box;">
                                                                            <a id="hlgridRecommendedLeadershipCourses" onclick='confirmLaunchCourse("#= EncryptedCourseID #");return false;'  href="javascript:void(0);" title=" #= CourseTitle #" class="hover-pointer"> #= CourseTitle #</a>
                                                                        </p>
                                                                    </td>
                                                                    <td>
                                                                        <p class="clamp-line-2" style="display: -webkit-box;">
                                                                            <asp:Label ID="Label2" runat="server"> #= CourseDescription #</asp:Label>
                                                                        </p>
                                                                    </td>
                                                                    <td>
                                                                        <p class="clamp-line-2" style="display: -webkit-box;">
                                                                            <asp:Label ID="Label3" runat="server"> #= CourseDuration #</asp:Label>
                                                                        </p>
                                                                    </td>
                                                                    <td>
                                                                        <p class="clamp-line-2" style="display: -webkit-box;">
                                                                            <asp:Label ID="Label4" runat="server"> #= DateLastModified #</asp:Label>
                                                                        </p>
                                                                    </td>
                                                                    <td>
                                                                        <p class="clamp-line-2" style="display: -webkit-box;">
                                                                            <asp:Label ID="Label5" runat="server"> #= CoursePath #</asp:Label>
                                                                        </p>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <EmptyDataTemplate>
                                                                No Recommended Leadership Course.
                                                            </EmptyDataTemplate>
                                                            <DataService EnableCaching="true" />
                                                        </DataBinding>
                                                    </ClientSettings>
                                                </rad:RadListView>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlgridMandatoryCourses" runat="server" CssClass="height-300">
                                                <div class="widgets black-header m-category-header">
                                                    <div class="col-md-6 font-bold col-xs-9 pull-left no-padding">
                                                        Mandatory Courses</div>
                                                    <div class="col-md-6 font-bold col-xs-3 pull-right no-padding">
                                                        <div class="row-fluid mcViews display-none">
                                                            <div class="pull-right">
                                                                <asp:LinkButton ID="btnViewAllGridMC" runat="server" Text="View All" OnClientClick="btnViewAllListMC_Click(); return false;"
                                                                    CssClass="gold"></asp:LinkButton>
                                                                <asp:LinkButton ID="btnBackGridMC" runat="server" OnClientClick="btnBackListMC_Click(); return false;"
                                                                    Font-Underline="false" aria-hidden="true" CssClass="display-none gold"><i class="fa fa-long-arrow-left"></i></asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <rad:RadListView ID="gridMandatoryCourses" runat="server" RenderMode="Lightweight"
                                                    CssClass="GridLess" AllowPaging="true" PageSize="4">
                                                    <LayoutTemplate>
                                                        <div class="loader js-mc-loader">
                                                        </div>
                                                        <div id="mandatoryGridView">
                                                            <table class="gridMainTable table table-bordered table-striped course-grid">
                                                                <thead>
                                                                    <tr class="rlvHeader">
                                                                        <th class="btn-teal">
                                                                            Title
                                                                        </th>
                                                                        <th class="btn-teal">
                                                                            Description
                                                                        </th>
                                                                        <th class="btn-teal">
                                                                            Duration
                                                                        </th>
                                                                        <th class="btn-teal">
                                                                            Last Updated
                                                                        </th>
                                                                        <th class="btn-teal">
                                                                            Course Path
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="gridmcitem">
                                                                </tbody>
                                                                <tfoot>
                                                                </tfoot>
                                                            </table>
                                                            <div class="mcPager margin-bottom-10px">
                                                                <a class="pagePrev fa fa-caret-left gray-arrow no-underline-hover display-none left-arrow-pager"
                                                                    href="javascript:void(0);" title="Go to previous page"></a><a class="pageNext fa fa-caret-right gray-arrow no-underline-hover display-none right-arrow-pager"
                                                                        href="javascript:void(0);" title="Go to next page"></a>
                                                            </div>
                                                        </div>
                                                    </LayoutTemplate>
                                                    <ClientSettings>
                                                        <DataBinding ItemPlaceHolderID="gridmcitem">
                                                            <ItemTemplate>
                                                                <tr class="rlvI">
                                                                    <td>
                                                                        <p class="clamp-line-2" style="display: -webkit-box;">
                                                                            <a id="hlgridMandatoryCourses" onclick='confirmLaunchCourse("#= EncryptedCourseID #");return false;'  href="javascript:void(0);" title=" #= CourseTitle #" class="hover-pointer"> #= CourseTitle #</a>
                                                                        </p>
                                                                    </td>
                                                                    <td>
                                                                        <p class="clamp-line-2" style="display: -webkit-box;">
                                                                            <asp:Label ID="Label2" runat="server"> #= CourseDescription #</asp:Label>
                                                                        </p>
                                                                    </td>
                                                                    <td>
                                                                        <p class="clamp-line-2" style="display: -webkit-box;">
                                                                            <asp:Label ID="Label3" runat="server"> #= CourseDuration #</asp:Label>
                                                                        </p>
                                                                    </td>
                                                                    <td>
                                                                        <p class="clamp-line-2" style="display: -webkit-box;">
                                                                            <asp:Label ID="Label4" runat="server"> #= DateLastModified #</asp:Label>
                                                                        </p>
                                                                    </td>
                                                                    <td>
                                                                        <p class="clamp-line-2" style="display: -webkit-box;">
                                                                            <asp:Label ID="Label5" runat="server"> #= CoursePath #</asp:Label>
                                                                        </p>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <EmptyDataTemplate>
                                                                No Mandatory Course.
                                                            </EmptyDataTemplate>
                                                            <DataService EnableCaching="true" />
                                                        </DataBinding>
                                                    </ClientSettings>
                                                </rad:RadListView>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlgridRecommendedCourses" runat="server" CssClass="height-300" Visible="false">
                                                <div class="widgets black-header m-category-header">
                                                    <div class="col-md-6 font-bold">
                                                        Recommended Courses</div>
                                                    <div class="col-md-6 font-bold">
                                                        <div class="row-fluid rmViews display-none">
                                                            <div class="pull-right">
                                                                <asp:LinkButton ID="btnViewAllGridRM" runat="server" OnClientClick="btnViewAllListRM_Click(); return false;"
                                                                    Text="View All" CssClass="gold"></asp:LinkButton>
                                                                <asp:LinkButton ID="btnBackGridRM" runat="server" OnClientClick="btnBackListRM_Click(); return false;"
                                                                    Font-Underline="false" aria-hidden="true" CssClass="display-none gold"><i class="fa fa-long-arrow-left"></i></asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <rad:RadListView ID="gridRecommendedCourses" runat="server" RenderMode="Lightweight"
                                                    CssClass="GridLess" AllowPaging="true" PageSize="4">
                                                    <LayoutTemplate>
                                                        <div id="recommendedGridView">
                                                            <table class="recommendedMainTable table table-bordered table-striped course-grid">
                                                                <thead>
                                                                    <tr class="rlvHeader">
                                                                        <th class="btn-teal">
                                                                            Title
                                                                        </th>
                                                                        <th class="btn-teal">
                                                                            Description
                                                                        </th>
                                                                        <th class="btn-teal">
                                                                            Duration
                                                                        </th>
                                                                        <th class="btn-teal">
                                                                            Last Updated
                                                                        </th>
                                                                        <th class="btn-teal">
                                                                            Course Path
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="gridrmitem">
                                                                </tbody>
                                                                <tfoot>
                                                                </tfoot>
                                                            </table>
                                                            <div class="rmPager margin-bottom-10px">
                                                                <a class="pagePrev fa fa-caret-left gray-arrow no-underline-hover display-none left-arrow-pager"
                                                                    href="javascript:void(0);" title="Go to previous page"></a><a class="pageNext fa fa-caret-right gray-arrow no-underline-hover display-none right-arrow-pager"
                                                                        href="javascript:void(0);" title="Go to next page"></a>
                                                            </div>
                                                        </div>
                                                    </LayoutTemplate>
                                                    <ClientSettings>
                                                        <DataBinding ItemPlaceHolderID="gridrmitem">
                                                            <ItemTemplate>
                                                                <tr class="rlvI">
                                                                    <td>
                                                                        <p class="clamp-line-2" style="display: -webkit-box;">
                                                                            <a id="hlgridRecommendedCourses" onclick='confirmLaunchCourse("#= EncryptedCourseID #");return false;'  href="javascript:void(0);" title=" #= CourseTitle #" class="hover-pointer"> #= CourseTitle #</a>
                                                                        </p>
                                                                    </td>
                                                                    <td>
                                                                        <p class="clamp-line-2" style="display: -webkit-box;">
                                                                            <asp:Label ID="Label2" runat="server"> #= CourseDescription #</asp:Label>
                                                                        </p>
                                                                    </td>
                                                                    <td>
                                                                        <p class="clamp-line-2" style="display: -webkit-box;">
                                                                            <asp:Label ID="Label3" runat="server"> #= CourseDuration #</asp:Label>
                                                                        </p>
                                                                    </td>
                                                                    <td>
                                                                        <p class="clamp-line-2" style="display: -webkit-box;">
                                                                            <asp:Label ID="Label4" runat="server"> #= DateLastModified #</asp:Label>
                                                                        </p>
                                                                    </td>
                                                                    <td>
                                                                        <p class="clamp-line-2" style="display: -webkit-box;">
                                                                            <asp:Label ID="Label5" runat="server"> #= CoursePath #</asp:Label>
                                                                        </p>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <EmptyDataTemplate>
                                                                No Recommended Course.
                                                            </EmptyDataTemplate>
                                                            <DataService EnableCaching="true" />
                                                        </DataBinding>
                                                    </ClientSettings>
                                                </rad:RadListView>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlgridSkillDevelopments" runat="server" CssClass="height-300">
                                                <div class="widgets black-header m-category-header">
                                                    <div class="col-md-6 font-bold col-xs-9 pull-left no-padding">
                                                        Skill Development Courses</div>
                                                    <div class="col-md-6 font-bold col-xs-3 pull-right no-padding">
                                                        <div class="row-fluid mtViews display-none">
                                                            <div class="pull-right">
                                                                <asp:LinkButton ID="btnViewAllGridMT" runat="server" Text="View All" OnClientClick="btnViewAllListMT_Click(); return false;"
                                                                    CssClass="gold"></asp:LinkButton>
                                                                <asp:LinkButton ID="btnBackGridMT" runat="server" OnClientClick="btnBackListMT_Click(); return false;"
                                                                    Font-Underline="false" aria-hidden="true" CssClass="display-none gold"><i class="fa fa-long-arrow-left"></i></asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <rad:RadListView ID="gridSkillDevelopments" runat="server" RenderMode="Lightweight"
                                                    CssClass="GridLess" AllowPaging="true" PageSize="4">
                                                    <LayoutTemplate>
                                                        <div class="loader js-mc-loader">
                                                        </div>
                                                        <div id="SkillDevelopmentGridView">
                                                            <table class="SkillDevelopmentMainTable table table-bordered table-striped course-grid">
                                                                <thead>
                                                                    <tr class="rlvHeader">
                                                                        <th class="btn-teal">
                                                                            Title
                                                                        </th>
                                                                        <th class="btn-teal">
                                                                            Description
                                                                        </th>
                                                                        <th class="btn-teal">
                                                                            Duration
                                                                        </th>
                                                                        <th class="btn-teal">
                                                                            Last Updated
                                                                        </th>
                                                                        <th class="btn-teal">
                                                                            Course Path
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="gridmtitem">
                                                                </tbody>
                                                                <tfoot>
                                                                </tfoot>
                                                            </table>
                                                            <div class="mtPager margin-bottom-10px">
                                                                <a class="pagePrev fa fa-caret-left gray-arrow no-underline-hover display-none left-arrow-pager"
                                                                    href="javascript:void(0);" title="Go to previous page"></a><a class="pageNext fa fa-caret-right gray-arrow no-underline-hover display-none right-arrow-pager"
                                                                        href="javascript:void(0);" title="Go to next page"></a>
                                                            </div>
                                                        </div>
                                                    </LayoutTemplate>
                                                    <ClientSettings>
                                                        <DataBinding ItemPlaceHolderID="gridmtitem">
                                                            <ItemTemplate>
                                                                <tr class="rlvI">
                                                                    <td>
                                                                        <p class="clamp-line-2" style="display: -webkit-box;">
                                                                            <a id="hlgridSkillDevelopments" onclick='confirmLaunchCourse("#= EncryptedCourseID #");return false;'  href="javascript:void(0);" title=" #= CourseTitle #" class="hover-pointer"> #= CourseTitle #</a>
                                                                        </p>
                                                                    </td>
                                                                    <td>
                                                                        <p class="clamp-line-2" style="display: -webkit-box;">
                                                                            <asp:Label ID="Label2" runat="server"> #= CourseDescription #</asp:Label>
                                                                        </p>
                                                                    </td>
                                                                    <td>
                                                                        <p class="clamp-line-2" style="display: -webkit-box;">
                                                                            <asp:Label ID="Label3" runat="server"> #= CourseDuration #</asp:Label>
                                                                        </p>
                                                                    </td>
                                                                    <td>
                                                                        <p class="clamp-line-2" style="display: -webkit-box;">
                                                                            <asp:Label ID="Label4" runat="server"> #= DateLastModified #</asp:Label>
                                                                        </p>
                                                                    </td>
                                                                    <td>
                                                                        <p class="clamp-line-2" style="display: -webkit-box;">
                                                                            <asp:Label ID="Label5" runat="server"> #= CoursePath #</asp:Label>
                                                                        </p>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <EmptyDataTemplate>
                                                                No Skill Development Course.
                                                            </EmptyDataTemplate>
                                                            <DataService EnableCaching="true" />
                                                        </DataBinding>
                                                    </ClientSettings>
                                                </rad:RadListView>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlgridRecentlyAddedCourses" runat="server" CssClass="height-300">
                                                <div class="widgets black-header m-category-header">
                                                    <div class="col-md-6 font-bold col-xs-9 pull-left no-padding">
                                                        Recently Added Courses</div>
                                                    <div class="col-md-6 font-bold col-xs-3 pull-right no-padding">
                                                        <div class="row-fluid rcViews display-none">
                                                            <div class="pull-right">
                                                                <asp:LinkButton ID="btnViewAllGridRAC" runat="server" OnClientClick="btnViewAllListRAC_Click(); return false;"
                                                                    Text="View All" CssClass="gold"></asp:LinkButton>
                                                                <asp:LinkButton ID="btnBackGridRAC" runat="server" OnClientClick="btnBackListRAC_Click(); return false;"
                                                                    Font-Underline="false" aria-hidden="true" CssClass="display-none gold"><i class="fa fa-long-arrow-left"></i></asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <rad:RadListView ID="gridRecentCourses" runat="server" RenderMode="Lightweight" CssClass="GridLess"
                                                    AllowPaging="true" PageSize="4">
                                                    <LayoutTemplate>
                                                        <div id="recentGridView">
                                                            <table class="recentMainTable table table-bordered table-striped course-grid">
                                                                <thead>
                                                                    <tr class="rlvHeader">
                                                                        <th class="btn-teal">
                                                                            Title
                                                                        </th>
                                                                        <th class="btn-teal">
                                                                            Description
                                                                        </th>
                                                                        <th class="btn-teal">
                                                                            Duration
                                                                        </th>
                                                                        <th class="btn-teal">
                                                                            Last Updated
                                                                        </th>
                                                                        <th class="btn-teal">
                                                                            Course Path
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="gridrcitem">
                                                                </tbody>
                                                                <tfoot>
                                                                </tfoot>
                                                            </table>
                                                            <div class="rcPager margin-bottom-10px">
                                                                <a class="pagePrev fa fa-caret-left gray-arrow no-underline-hover display-none left-arrow-pager"
                                                                    href="javascript:void(0);" title="Go to previous page"></a><a class="pageNext fa fa-caret-right gray-arrow no-underline-hover display-none right-arrow-pager"
                                                                        href="javascript:void(0);" title="Go to next page"></a>
                                                            </div>
                                                        </div>
                                                    </LayoutTemplate>
                                                    <ClientSettings>
                                                        <DataBinding ItemPlaceHolderID="gridrcitem">
                                                            <ItemTemplate>
                                                                <tr class="rlvI">
                                                                    <td>
                                                                        <p class="clamp-line-2" style="display: -webkit-box;">
                                                                            <a id="hlgridRecentCourses" onclick='confirmLaunchCourse("#= EncryptedCourseID #");return false;'  href="javascript:void(0);" title=" #= CourseTitle #" class="hover-pointer"> #= CourseTitle #</a>
                                                                        </p>
                                                                    </td>
                                                                    <td>
                                                                        <p class="clamp-line-2" style="display: -webkit-box;">
                                                                            <asp:Label ID="Label2" runat="server"> #= CourseDescription #</asp:Label>
                                                                        </p>
                                                                    </td>
                                                                    <td>
                                                                        <p class="clamp-line-2" style="display: -webkit-box;">
                                                                            <asp:Label ID="Label3" runat="server"> #= CourseDuration #</asp:Label>
                                                                        </p>
                                                                    </td>
                                                                    <td>
                                                                        <p class="clamp-line-2" style="display: -webkit-box;">
                                                                            <asp:Label ID="Label4" runat="server"> #= DateLastModified #</asp:Label>
                                                                        </p>
                                                                    </td>
                                                                    <td>
                                                                        <p class="clamp-line-2" style="display: -webkit-box;">
                                                                            <asp:Label ID="Label5" runat="server"> #= CoursePath #</asp:Label>
                                                                        </p>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <EmptyDataTemplate>
                                                                No Recently Added Course.
                                                            </EmptyDataTemplate>
                                                            <DataService EnableCaching="true" />
                                                        </DataBinding>
                                                    </ClientSettings>
                                                </rad:RadListView>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlgridTrendingCourses" runat="server" CssClass="height-300">
                                                <div class="widgets black-header m-category-header">
                                                    <div class="col-md-6 font-bold">
                                                        Trending Courses</div>
                                                    <div class="col-md-6 font-bold">
                                                        <div class="row-fluid tcViews display-none">
                                                            <div class="pull-right">
                                                                <asp:LinkButton ID="btnViewAllGridTC" runat="server" Text="View All" OnClientClick="btnViewAllListTC_Click(); return false;"
                                                                    CssClass="gold"></asp:LinkButton>
                                                                <asp:LinkButton ID="btnBackGridTC" runat="server" OnClientClick="btnBackListTC_Click(); return false;"
                                                                    Font-Underline="false" aria-hidden="true" CssClass="display-none gold"><i class="fa fa-long-arrow-left"></i></asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <rad:RadListView ID="gridTrendingCourses" runat="server" RenderMode="Lightweight"
                                                    CssClass="GridLess" AllowPaging="true" PageSize="4">
                                                    <LayoutTemplate>
                                                        <div id="trendingGridView">
                                                            <table class="trendingMainTable table table-bordered table-striped course-grid">
                                                                <thead>
                                                                    <tr class="rlvHeader">
                                                                        <th class="btn-teal">
                                                                            Title
                                                                        </th>
                                                                        <th class="btn-teal">
                                                                            Description
                                                                        </th>
                                                                        <th class="btn-teal">
                                                                            Duration
                                                                        </th>
                                                                        <th class="btn-teal">
                                                                            Last Updated
                                                                        </th>
                                                                        <th class="btn-teal">
                                                                            Course Path
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="gridtcitem">
                                                                </tbody>
                                                                <tfoot>
                                                                </tfoot>
                                                            </table>
                                                            <div class="tcPager margin-bottom-10px">
                                                                <a class="pagePrev fa fa-caret-left gray-arrow no-underline-hover display-none left-arrow-pager"
                                                                    href="javascript:void(0);" title="Go to previous page"></a><a class="pageNext fa fa-caret-right gray-arrow no-underline-hover display-none right-arrow-pager"
                                                                        href="javascript:void(0);" title="Go to next page"></a>
                                                            </div>
                                                        </div>
                                                    </LayoutTemplate>
                                                    <ClientSettings>
                                                        <DataBinding ItemPlaceHolderID="gridtcitem">
                                                            <ItemTemplate>
                                                                <tr class="rlvI">
                                                                    <td>
                                                                        <p class="clamp-line-2" style="display: -webkit-box;">
                                                                            <a id="hlgridTrendingCourses" onclick='confirmLaunchCourse("#= EncryptedCourseID #");return false;'  href="javascript:void(0);" title=" #= CourseTitle #" class="hover-pointer"> #= CourseTitle #</a>
                                                                        </p>
                                                                    </td>
                                                                    <td>
                                                                        <p class="clamp-line-2" style="display: -webkit-box;">
                                                                            <asp:Label ID="Label2" runat="server"> #= CourseDescription #</asp:Label>
                                                                        </p>
                                                                    </td>
                                                                    <td>
                                                                        <p class="clamp-line-2" style="display: -webkit-box;">
                                                                            <asp:Label ID="Label3" runat="server"> #= CourseDuration #</asp:Label>
                                                                        </p>
                                                                    </td>
                                                                    <td>
                                                                        <p class="clamp-line-2" style="display: -webkit-box;">
                                                                            <asp:Label ID="Label4" runat="server"> #= DateLastModified #</asp:Label>
                                                                        </p>
                                                                    </td>
                                                                    <td>
                                                                        <p class="clamp-line-2" style="display: -webkit-box;">
                                                                            <asp:Label ID="Label5" runat="server"> #= CoursePath #</asp:Label>
                                                                        </p>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <EmptyDataTemplate>
                                                                No Trending Course.
                                                            </EmptyDataTemplate>
                                                            <DataService EnableCaching="true" />
                                                        </DataBinding>
                                                    </ClientSettings>
                                                </rad:RadListView>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </rad:RadPageView>
                            <rad:RadPageView ID="rpvSubcategoriesCourses" runat="server">
                                <div style="height: 60vh;">
                                    <ucCourseCatalogTab:courseCatalogTab ID="ccTab" runat="server" />
                                </div>
                            </rad:RadPageView>
                        </rad:RadMultiPage>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hfLeadershipCatID" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hfSkillDevCatID" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hfMandatoryCatID" runat="server" ClientIDMode="Static" />
    
</asp:Content>
