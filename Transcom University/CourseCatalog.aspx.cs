﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using Telerik.Web.UI;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using TranscomUniversityV3Model;
using System.Web.Script.Serialization;

namespace TranscomUniversityV3
{
    public partial class CourseCatalog : BasePage
    {
        public static bool isLoggedIn;

        private static IEnumerable<CourseSubCategories> _subcategory;

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["SessionRoles"] != null && Session["SessionRoles"] == "Admin")
                Response.Redirect("Admin.aspx?Tab=CourseMgmt");

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                Session["IsNewCourseView"] = true;

            if (Request.QueryString["Id"] != null)
            {
                int categoryId = Convert.ToInt32(Utils.Decrypt(Request.QueryString["Id"].ToString()));
                string category = Request.QueryString["Tab"].ToString();
                RadMultiPage1.SelectedIndex = 1;
                lblTab2.Text = category;
               
            }
            else
            {
                RadMultiPage1.SelectedIndex = 0;
                lblTab2.Text = "Grid View";
                //rcbCourseCatalog.Visible = true;
            }

            int leadershipCatID = 17;
            int skillDevCatID = 18;
            int mandaytoryCatID = 19;
            hfLeadershipCatID.Value = Utils.Encrypt(leadershipCatID);
            hfSkillDevCatID.Value = Utils.Encrypt(skillDevCatID);
            hfMandatoryCatID.Value = Utils.Encrypt(mandaytoryCatID);

            isLoggedIn = DataHelper.isLoggedIn();

            if (isLoggedIn == false)
            {
                ccSidebar.Visible = false;
            }

            if (!isLoggedIn)
            {
                lblLeadershipCatalogTitle.Text = Resources.LocalizedResource.LeadershipCourses.ToString();
                lblLeadershipCourseGrid.Text = Resources.LocalizedResource.LeadershipCourses.ToString();
            }
            else {
                lblLeadershipCatalogTitle.Text = Resources.LocalizedResource.RecommendedLeadershipCoursesForYou.ToString();
                lblLeadershipCourseGrid.Text = Resources.LocalizedResource.RecommendedLeadershipCoursesForYou.ToString(); 
            }

            if (Request.QueryString["view"] == "MyRecommended" && Request.QueryString["Tab"] == "Leadership")
            {
                lblTab1.Text = "Recommended Leadership Courses";
                lblTab2.Text = "See All";
            }else if(Request.QueryString["view"] == "SeeAll")
            {
                lblTab1.Text = Request.QueryString["Tab"] + " Courses";
                lblTab2.Text = "See All";
            }

        }


        [System.Web.Services.WebMethod()]
        public static IEnumerable<CourseSubCategories> GetSubCategoryData(int startRowIndex, int maximumRows, string sortExpression)
        {
            return RadGrid.GetBindingData(LoadSubCategory().AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data;
        }

        private static int clientSubcatId;
        private static int clientCatId;
        [System.Web.Services.WebMethod()]
        public static int GetSubCategoryCount()
        {
            return LoadSubCategory().Count();

        }

        [System.Web.Services.WebMethod()]
        public static IEnumerable<CourseSubCategories> LoadSubCategory()
        {
            TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();

            _subcategory = db.tbl_TranscomUniversity_Lkp_Subcategory
                                .AsEnumerable()
                                .Where(c =>
                                    c.CategoryID == 1
                                )
                                .Select(c => new CourseSubCategories
                                {
                                    SubCategoryID = c.SubcategoryID,
                                    SubCategory = c.Subcategory
                                })
                                .OrderBy(c => c.SubCategory)
                                .ToList();
            return _subcategory;

        }


        //Subcategory Courses Data
        [System.Web.Services.WebMethod()]
        public static CourseData GetSubCategoryCoursesData(int startRowIndex, int maximumRows, string sortExpression, int subcat, int cat, int myRecommended)
        {
            clientSubcatId = subcat;
            clientCatId = cat;
            TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();

            //if (isLoggedIn)
            if (myRecommended == 1)
            {
                var currentCIM = Convert.ToInt32(DataHelper.GetCurrentUserCIM());

                if (cat == 17)//recommended leaderhip
                {
                    //IEnumerable<CourseDetails> _recommendedLeadeshipCourse = db.pr_TranscomUniversity_getCompetencies(currentCIM)
                    //_recommendedLeadeshipCourse = db.pr_TranscomUniversity_RecommendedCourse(currentCIM)
                    IEnumerable<CourseDetails> _recommendedLeadeshipCourse = db.pr_TranscomUniversity_getCompetencies(currentCIM)
                                    .AsEnumerable()
                                    .Where(c => c.CourseCategory.Contains("Leadership") && c.SubcategoryID == subcat)
                                    .Select(c => new CourseDetails
                                    {
                                        EncryptedCourseID = Utils.Encrypt(c.CourseID),
                                        CourseID = c.CourseID,
                                        CourseImage = c.CourseImage,
                                        CourseTitle = c.CourseTitle,
                                        CourseType = c.CourseType,
                                        CourseDuration = c.CourseDuration == 0 ? "0" : ToReadableString(Convert.ToInt32(c.CourseDuration)),
                                        CourseCategory = c.CourseCategory,
                                        CourseSubcategory = c.CourseSubcategory,
                                        CourseDescription = c.CourseDescription == "" ? "No Description" : c.CourseDescription,
                                        DateLastModified = string.Format("{0: yyyy/MM/dd hh:mm:ss tt}", c.DateLastModified),
                                        CoursePath = c.CoursePath,

                                    })

                                    .ToList();
                    var gridData = new CourseData()
                    {
                        count = _recommendedLeadeshipCourse.Count(),
                        data = RadGrid.GetBindingData(_recommendedLeadeshipCourse.AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data
                    };
                    return gridData;
                }
                else
                {
                    IEnumerable<CourseDetails> _courses = db.vw_TranscomUniversity_AllCourses
                                        .AsEnumerable()
                                        .Where(c =>
                                            c.SubcategoryID == clientSubcatId &&
                                             c.CategoryID == clientCatId
                                        )
                                        .Select(c => new CourseDetails
                                        {
                                            EncryptedCourseID = Utils.Encrypt(c.CourseID),
                                            CourseID = c.CourseID,
                                            CourseImage = c.CourseImage,
                                            CourseTitle = c.CourseTitle,
                                            CourseType = c.CourseType,
                                            CourseDuration = c.CourseDuration == 0 ? "0" : ToReadableString(Convert.ToInt32(c.CourseDuration)),
                                            CourseCategory = c.CourseCategory,
                                            CourseSubcategory = c.CourseSubcategory,
                                            CourseDescription = c.CourseDescription == "" ? "No Description" : c.CourseDescription,
                                            DateLastModified = string.Format("{0: yyyy/MM/dd hh:mm:ss tt}", c.DateLastModified),
                                            CoursePath = c.CoursePath
                                        })
                                        .OrderBy(c => c.CourseTitle)
                                        .ToList();
                    var gridData = new CourseData()
                    {
                        count = _courses.Count(),
                        data = RadGrid.GetBindingData(_courses.AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data
                    };
                    return gridData;
                }
            }
            else
            {
                IEnumerable<CourseDetails> _courses = db.vw_TranscomUniversity_AllCourses
                                    .AsEnumerable()
                                    .Where(c =>
                                        c.SubcategoryID == clientSubcatId &&
                                         c.CategoryID == clientCatId
                                    )
                                    .Select(c => new CourseDetails
                                    {
                                        EncryptedCourseID = Utils.Encrypt(c.CourseID),
                                        CourseID = c.CourseID,
                                        CourseImage = c.CourseImage,
                                        CourseTitle = c.CourseTitle,
                                        CourseType = c.CourseType,
                                        CourseDuration = c.CourseDuration == 0 ? "0" : ToReadableString(Convert.ToInt32(c.CourseDuration)),
                                        CourseCategory = c.CourseCategory,
                                        CourseSubcategory = c.CourseSubcategory,
                                        CourseDescription = c.CourseDescription == "" ? "No Description" : c.CourseDescription,
                                        DateLastModified = string.Format("{0: yyyy/MM/dd hh:mm:ss tt}", c.DateLastModified),
                                        CoursePath = c.CoursePath
                                    })
                                    .OrderBy(c => c.CourseTitle)
                                    .ToList();
                var gridData = new CourseData()
                {
                    count = _courses.Count(),
                    data = RadGrid.GetBindingData(_courses.AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data
                };
                return gridData;

            }

        }

        [System.Web.Services.WebMethod()]
        public static int GetSubCategoryCoursesCount()
        {
            TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();

            int _coursesCount = db.vw_TranscomUniversity_AllCourses
                                 .AsEnumerable()
                                 .Where(c =>
                                     c.SubcategoryID == clientSubcatId &&
                                      c.CategoryID == clientCatId
                                 ).Count();
            return _coursesCount;
        }

        public static string ToReadableString(int getTime)
        {
            TimeSpan span = TimeSpan.FromMinutes(getTime);

            string formatted = string.Format("{0}{1}{2}{3}",
                span.Duration().Days > 0 ? string.Format("{0:0} Day{1} and ", span.Days, span.Days == 1 ? String.Empty : "s") : string.Empty,
                span.Duration().Hours > 0 ? string.Format("{0:0} Hour{1} and ", span.Hours, span.Hours == 1 ? String.Empty : "s") : string.Empty,
                span.Duration().Minutes > 0 ? string.Format("{0:0} Minute{1} and ", span.Minutes, span.Minutes == 1 ? String.Empty : "s") : string.Empty,
                span.Duration().Seconds > 0 ? string.Format("{0:0} Second{1}", span.Seconds, span.Seconds == 1 ? String.Empty : "s") : string.Empty);

            if (formatted.EndsWith("and ")) formatted = formatted.Substring(0, formatted.Length - 4);

            if (string.IsNullOrEmpty(formatted)) formatted = "0 seconds";

            return formatted;
        }
    }
    public class CourseDetails
    {
        public string EncryptedCourseID { get; set; }

        public int? CourseID { get; set; }

        public string CourseImage { get; set; }

        public string CourseTitle { get; set; }

        public string CourseType { get; set; }

        public string CourseDuration { get; set; }

        public string CourseCategory { get; set; }

        public string CourseSubcategory { get; set; }

        public string CourseDescription { get; set; }

        public string DateLastModified { get; set; }

        public string CoursePath { get; set; }
    }

    public class CourseSubCategories
    {
        public int SubCategoryID { get; set; }
        public string SubCategory { get; set; }
    }

    public class CourseData
    {
        public int count { get; set; }
        public IEnumerable<CourseDetails> data { get; set; }
    }

}