﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TranscomUniversityV3.Master" AutoEventWireup="true"
    CodeFile="CourseLauncher.aspx.cs" Inherits="TranscomUniversityV3.CourseLauncher" %>

<%@ Register Src="UserControl/Statics/CourseCatalogSidebarUserCtrl.ascx" TagName="courseCatalogSidebar"
    TagPrefix="ucCcSidebar" %>
<%@ Register Src="~/UserControl/Pages/CoursePreviewUserCtrl.ascx" TagName="coursePreview"
    TagPrefix="ucCoursePreview" %>
<%--<%@ Register Src="~/UserControl/Pages/CourseForumUserCtrl.ascx" TagName="courseForum"
    TagPrefix="ucCourseForum" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="contentPlaceHolderLeftPanel" runat="server">
    

    <rad:RadWindowManager ID="rwmCourseLauncher" runat="server" RenderMode="Lightweight"
        EnableShadow="true" Modal="true" VisibleOnPageLoad="false" Behaviors="Close, Move"
        DestroyOnClose="true" Opacity="99" VisibleStatusbar="false" Skin="Bootstrap">
    </rad:RadWindowManager>
    <rad:RadAjaxLoadingPanel ID="localLoadingPanel" runat="server" CssClass="Loading2"
        Transparency="25" />
    <rad:RadAjaxLoadingPanel ID="noLoadingPanel" runat="server" CssClass="Loading-Empty"
        Transparency="25" />
         <%--OnAjaxRequest="serverRebindBundle"--%>
    <rad:RadAjaxManager ID="rampGridBundle" runat="server">
        <AjaxSettings>
            <rad:AjaxSetting AjaxControlID="gridBundle">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="gridBundle" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnShowTakeAssessment">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="btnShowTakeAssessment" LoadingPanelID="noLoadingPanel" />
                    <rad:AjaxUpdatedControl ControlID="holderTakeAssessment" LoadingPanelID="noLoadingPanel" />
                </UpdatedControls>
            </rad:AjaxSetting>
        </AjaxSettings>
    </rad:RadAjaxManager>
    <ucCcSidebar:courseCatalogSidebar ID="ccSidebar" runat="server" />
    <rad:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function rebindBundle() {
                showLoading();
                var panel = $find("<%= localLoadingPanel.ClientID %>");
                panel.show("gridBundle");
                var radManager;

                radManager = $find('<%= rampGridBundle.ClientID %>');

                console.log("server rebind");

                //Fire ajax request (optionally pass an event arg value)
                setTimeout(function () {
                    radManager.ajaxRequest();
                }, 1000);

//                progressLoad();

            }
            
            function showPleaseSignin(type) {
                Telerik.Web.UI.RadWindowUtils.Localization =   
                {  
                    "OK" : "Sign in",  
                    "Cancel" : "No",
                    "Close" : "Close",    

                };

                var signin;
                switch(type){
                    case 1:
                         signin = radalert($('#<%= hfPleasesignintoenrolltothiscourse.ClientID %>').val(), 400, 180, $('#<%= hfSignin.ClientID %>').val(),callBackSigninFN, "");
                        break;
                    case 2:
                        signin = radalert($('#<%= hfPleasesignintoaddthiscoursetoyourtakelaterlist.ClientID %>').val(), 400, 180, $('#<%= hfSignin.ClientID %>').val(),callBackSigninFN, "");
                        break;
                    case 3:
                        signin = radalert($('#<%= hfPleasesignintoviewthiscourse.ClientID %>').val(), 400, 180, $('#<%= hfSignin.ClientID %>').val(), callBackSigninFN, "");
                        break;
                    default:
                        

                }
               
                var rwAlert = "#RadWindowWrapper_" +signin.get_name();
                $(rwAlert).addClass("rwCustom1");
                var leftoffset = ($(rwAlert).find(".rwDialog").width()) / 2;
                var buttonoffset = ($(rwAlert).find(".rwOkBtn").outerWidth()) / 2;
                var offset = -1 * (leftoffset - buttonoffset);
                $(rwAlert).find(".rwOkBtn").css({"position" : "relative", "left" : offset});

                  Telerik.Web.UI.RadWindowUtils.Localization =   
                {  
                    "OK" : $('#<%= hfOk.ClientID %>').val(),  
                    "Cancel" : $('#<%= hfNo.ClientID %>').val(),
                    "Close" : $('#<%= hfClose.ClientID %>').val(),    

                };
            }

            function callBackSigninFN(arg)
            {
                console.log(arg);
                 if (arg) {
                        $("#<%= btnSignin.ClientID %>").click();
                    }
            }

            var roweqoheight;
            function openNonScormAssessment(url, tid, cim) {

                $("#coursePreviewContainer").hide();
                $("#<%= hfTid.ClientID %>").val(tid);

                //animate scroll to top
                 $('body,html').animate({
                    scrollTop: 0
                }, 300);

                $("#<%= pnlTakeAssessment.ClientID %>").removeClass("display-none");

                //set height to fit
                $("#<%= pnlTakeAssessment.ClientID %>").height("90vh");
                roweqoheight = $(".row-eq-height").height();
                $(".row-eq-height").height("100vh");
                $(".sidebar-relative").height("unset");
                
                var panel = $find("<%= localLoadingPanel.ClientID %>");
                panel.show("<%= pnlTakeAssessment.ClientID %>");

                //click to load assessment details
                $("#<%= btnShowTakeAssessment.ClientID %>").click();

                window.location.hash = "TakeAssessment";

                return false;
            }

            //function btnBackToCoursePreview_ClientClick() {
//                $(".row-eq-height").height(roweqoheight);
//                $(".sidebar-relative").height(roweqoheight);
//                $("#coursePreviewContainer").show();
//                $("#<%= pnlTakeAssessment.ClientID %>").addClass("display-none");
           // }

           $(document).ready(function(){
                $(window).scroll(function () {
                    sidebarFull();
                });
           })
           
           
        </script>
    </rad:RadCodeBlock>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolderMain" runat="server">
    <div class="row course-preview-row-container bottom-margin-md">
        <div class="col-md-12 course-preview-header">
            <ul id="ulBreadcrumb" runat="server" class="breadcrumb">
                <li id="bc1" runat="server"><a>
                    <asp:Label ID="lblTab0" runat="server" Text="Learner" /></a></li>
                <li id="bc2" runat="server"><a>
                    <asp:Label ID="lblTab1" runat="server" Text="Course Catalog" /></a></li>
                <li id="bc3" runat="server"><a>
                    <asp:Label ID="lblTab2" runat="server" Text="" /></a></li>
                <li id="bc4" runat="server">
                    <asp:Label ID="lblTab3" runat="server" Text="" ForeColor="Yellow" /></li>
            </ul>
        </div>
        <div class="col-md-12 course-preview-container" id="coursePreviewContainer">
            <div class="row">
                <asp:Panel ID="pnlCoursePrev" runat="server" CssClass="col-sm-12 js-course-preview">
                    <ucCoursePreview:coursePreview ID="clCoursePreview" runat="server" />
                </asp:Panel>
                <asp:Panel ID="pnlCourseForum" runat="server" CssClass="col-sm-4 js-course-forum display-none"
                    Visible="false" ClientIDMode="Static" >
                    <div class="container">
                        <div class="row black-header ten-px-padding vertical-align btn-flat">
                            <div class="col-md-12 font-bold" style="line-height: 30px;">
                                <asp:Label ID="ltForumHeader" runat="server">
                                        Course Forum Threads
                                </asp:Label>
                            </div>
                        </div>
                    </div>
                    <asp:Panel ID="forumContainer" runat="server" CssClass="container pnl-forum">
                        <asp:PlaceHolder ID="phForum" runat="server"></asp:PlaceHolder>
                        <%-- <ucCourseForum:courseForum ID="forumsPanel" runat="server" />--%>
                    </asp:Panel>
                </asp:Panel>
            </div>
        </div>
        <asp:Panel ID="pnlTakeAssessment" runat="server" CssClass="display-none min-height-60vh" style="margin-top:50px;" >
            <asp:Button ID="btnShowTakeAssessment" runat="server" OnClick="btnShowTakeAssessment_Click"
                CssClass="display-none" />
            <asp:HiddenField ID="hfTid" runat="server" />
            <asp:Label ID="lblTaking" Text="Loading Assessment..." runat="server" Visible="false"  />
            <asp:PlaceHolder ID="holderTakeAssessment" runat="server"></asp:PlaceHolder>
        </asp:Panel>
    </div>
    <asp:HiddenField ID="hfSelectedThread" ClientIDMode="Static" runat="server" />
    <asp:Button ID="btnSignin" runat="server" CssClass="display-none" OnClick="btnRequestLogin_Click" />
          <asp:HiddenField runat="server" ID="hfPleasesignintoenrolltothiscourse" Value="<%$ Resources:LocalizedResource, Pleasesignintoenrolltothiscourse%>" />
        <asp:HiddenField runat="server" ID="hfPleasesignintoaddthiscoursetoyourtakelaterlist" Value="<%$ Resources:LocalizedResource, Pleasesignintoaddthiscoursetoyourtakelaterlist%>" />
        <asp:HiddenField runat="server" ID="hfPleasesignintoviewthiscourse" Value="<%$ Resources:LocalizedResource, Pleasesignintoviewthiscourse%>" />
        <asp:HiddenField runat="server" ID="hfSignin" Value="<%$ Resources:LocalizedResource, Signin%>" />
        <asp:HiddenField runat="server" ID="hfOk" Value="<%$ Resources:LocalizedResource, Ok%>" />
        <asp:HiddenField runat="server" ID="hfNo" Value="<%$ Resources:LocalizedResource, No%>" />
        <asp:HiddenField runat="server" ID="hfClose" Value="<%$ Resources:LocalizedResource, Close%>" />
</asp:Content>
