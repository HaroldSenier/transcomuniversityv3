﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TranscomUniversityV3Model;
using System.Data;
using Telerik.Web.UI;
using Microsoft.AspNet.Membership.OpenAuth;
using System.IO;
using NuComm.Security.Encryption;
using NuSkill.Business;
using TheLibrary.ErrorLogger;

namespace TranscomUniversityV3
{

    public partial class CourseLauncher : System.Web.UI.Page
    {
        static int global_CourseID;
        static string global_EncCourseID;
        static int global_UserCIM;
        RadGrid gridBundle;
        static string permanentPath;
        public string ReturnUrl { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

            global_UserCIM = DataHelper.GetCurrentUserCIM() == "" ? 0 : Convert.ToInt32(DataHelper.GetCurrentUserCIM());

            global_EncCourseID = Request.QueryString["CourseID"];
            global_CourseID = Convert.ToInt32(Utils.Decrypt(global_EncCourseID));

            if (!Page.IsPostBack)
            {
                ReturnUrl = HttpContext.Current.Request.Url.PathAndQuery;
                
                setBreadcrumb();

                if (Session["IsNewCourseView"] != null)
                {
                    var db = new TranscomUniversityV3ModelContainer();
                    db.pr_TranscomUniversity_CourseViewLogin(global_UserCIM, global_CourseID);
                }

                Session["IsNewCourseView"] = null;
                    

            }
            if (global_UserCIM != 0)
            {
                if (isForumEnabled(global_CourseID))
                {
                    UserControl myControl = (UserControl)Page.LoadControl("~/UserControl/Pages/CourseForumUserCtrl.ascx");
                    phForum.Controls.Add(myControl);
                }
            }

            //var manager = RadAjaxManager.GetCurrent(this);

            //Create a new delegate to handle the AjaxRequest event 
            //manager.AjaxRequest += new RadAjaxControl.AjaxRequestDelegate(serverRebindBundle);
            //gridBundle = (RadGrid)(clCoursePreview.FindControl("cLaunch").FindControl("gridBundle"));
            //Add your ajax settings programmatically (with ref to Master manager) 
            //manager.AjaxSettings.AddAjaxSetting(manager, gridBundle);

        }

        private bool isForumEnabled(int courseId)
        {

            var db = new TranscomUniversityV3ModelContainer();
            var showForum = db.tbl_TranscomUniversity_Rlt_CourseSetting.Where(c => c.CourseID == courseId && c.ForumWidget == true).SingleOrDefault();
            if (showForum != null)
                return true;

            return false;

        }

        private void setBreadcrumb()
        {
            string courseName = DataHelper.getCourseName(global_CourseID);
            string Subcat = DataHelper.GetSubcategoryNameByCourseID(global_CourseID);
            if (DataHelper.GetCurrentUserCIM() == null || DataHelper.GetCurrentUserCIM() == "")
            {
                lblTab0.Text = "Search Results";
                lblTab1.Text = courseName;
                bc1.Visible = true;
                bc2.Visible = true;
                bc3.Visible = false;
                bc4.Visible = false;
            }
            else
            {
                bc1.Visible = true;
                bc2.Visible = true;
                bc3.Visible = true;
                bc4.Visible = true;
                lblTab0.Text = "Learner";
                lblTab1.Text = "Course Catalog";
                lblTab2.Text = Subcat;
                lblTab3.Text = courseName;
            }

        }

        public void serverRebindBundle(object sender, AjaxRequestEventArgs e)
        {
            gridBundle.Rebind();
        }

        [System.Web.Services.WebMethod()]
        public static int btnHelpfulRes(int postId)
        {
            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                int postid = Convert.ToInt32(postId);
                int usercim = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
                pr_TranscomUniversity_InsertHelpfulPost_Result res = db.pr_TranscomUniversity_InsertHelpfulPost(postid, usercim).SingleOrDefault();
                return res.Response;
            }
        }

        [System.Web.Services.WebMethod()]
        public static int btnSubmitReport(int postId, string txtReportMessage)
        {
            int usercim = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
            string reportmessage = txtReportMessage;
            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                pr_TranscomUniversity_InsertReportPost_Result res = db.pr_TranscomUniversity_InsertReportPost(postId, usercim, reportmessage).SingleOrDefault();
                return res.Response;
            }
        }

        [System.Web.Services.WebMethod()]
        public static int btnSubmitReply(int postId, int threadId, string txtMessage)
        {

            var userCim = DataHelper.GetCurrentUserCIM();


            int usercim = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                pr_TranscomUniversity_InsertPost_Result res = db.pr_TranscomUniversity_InsertPost(threadId, postId, usercim, txtMessage).SingleOrDefault();

                //if (res.Response == 1) {
                //    var posts = DataHelper.threadPosts(_threadid, usercim);
                //    System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                //    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                //    Dictionary<string, object> row;
                //    foreach (DataRow dr in posts.Rows)
                //    {
                //        row = new Dictionary<string, object>();
                //        foreach (DataColumn col in posts.Columns)
                //        {
                //            row.Add(col.ColumnName, dr[col]);
                //        }
                //        rows.Add(row);
                //    }
                //    return new { res = res.Response, postData = serializer.Serialize(rows) };
                //}else
                //    return new { res = res.Response};

                return res.Response;
            }
            //refreshForum();
        }
        private static IEnumerable<dynamic> _similarCourse;
        private static int clientCourseId;

        [System.Web.Services.WebMethod()]
        public static IEnumerable<dynamic> GetSimilarCourseData(int startRowIndex, int maximumRows, string sortExpression, string CourseId)
        {

            clientCourseId = Convert.ToInt32(Utils.Decrypt(CourseId));

            return RadGrid.GetBindingData(LoadSimilarCourse().AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data;
        }

        [System.Web.Services.WebMethod()]
        public static int GetSimilarCount()
        {
            return LoadSimilarCourse().Count();

        }

        [System.Web.Services.WebMethod()]
        public static IEnumerable<dynamic> LoadSimilarCourse()
        {
            TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();

            _similarCourse = db.pr_TranscomUniversity_SimilarCourses(clientCourseId)
                                .AsEnumerable()
                                .Select(c => new
                                {
                                    EncryptedCourseID = Utils.Encrypt((int)c.CourseID),
                                    CourseID = (int)c.CourseID,
                                    c.CourseImage,
                                    c.CourseTitle
                                })
                                .ToList();
            //string json = new JavaScriptSerializer().Serialize(similarCourse);
            return _similarCourse;

        }

        [System.Web.Services.WebMethod()]
        public static int btnLeaveForum_Click(string encCourseId)
        {

            var db = new TranscomUniversityV3ModelContainer();
            int courseId = Convert.ToInt16(Utils.Decrypt(encCourseId));
            int userCim = Convert.ToInt32(DataHelper.GetCurrentUserCIM());

            //leave
            pr_TranscomUniversity_RemoveForumMember_Result res = db.pr_TranscomUniversity_RemoveForumMember(courseId, userCim).SingleOrDefault();
            return res.Response;
        }

        public class CourseDetails
        {
            public string EncryptedCourseID { get; set; }

            public int CourseID { get; set; }

            public string CourseImage { get; set; }

        }

        protected void btnRequestLogin_Click(object sender, EventArgs e)
        {
            var redirectUrl = "~/ExternalLandingPage.aspx";

            if (!String.IsNullOrEmpty(ReturnUrl))
            {
                var resolvedReturnUrl = ResolveUrl(ReturnUrl);
                redirectUrl += "?ReturnUrl=" + HttpUtility.UrlEncode(resolvedReturnUrl);
            }

            OpenAuth.RequestAuthentication("google", redirectUrl);
        }

        protected void imagePermanentPath(int threadID, UploadedFile f)
        {

            string error = null;

            if (string.IsNullOrWhiteSpace(error))
            {
                string uploadFolder;
                var userCim = DataHelper.GetCurrentUserCIM();

                if (HttpContext.Current.Request.Url.Host == "localhost")
                    uploadFolder = Request.PhysicalApplicationPath + "Media\\Uploads\\Thread\\" + threadID + "\\";
                else
                    uploadFolder = HttpRuntime.AppDomainAppPath + "Media\\Uploads\\Thread\\" + threadID + "\\";

                string directoryPath = Server.MapPath(string.Format("~/{0}/", "Media/Uploads/Thread/" + threadID + "\\"));

                if (!Directory.Exists(directoryPath))
                    Directory.CreateDirectory(directoryPath);

                Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                string extension = Path.GetExtension(f.FileName);
                permanentPath = uploadFolder + unixTimestamp.ToString() + f.FileName;
                f.SaveAs(permanentPath, true);
            }


        }

        protected void btnShowTakeAssessment_Click(object sender, EventArgs e)
        {
            //SessionManager_Testing.SessionTestCategoryID = Convert.ToInt32(UTF8.DecryptText(HttpUtility.UrlDecode(hfTid.Value)));
            global_UserCIM = DataHelper.GetCurrentUserCIM() == "" ? 0 : Convert.ToInt32(DataHelper.GetCurrentUserCIM());
            loadExamHotLink();
            
        }


        //hotlink

        private void GotoTestDefault()
        {
            //lblTaking.Visible = false;
            //UserControl takeAssessmentUC = (UserControl)Page.LoadControl("~/UserControl/Pages/Assessment/TakeAssessment.ascx");
            //holderTakeAssessment.Controls.Add(takeAssessmentUC);
            Response.Redirect("TakeAssessment.aspx?CourseID=" + global_EncCourseID + "&tid=" + hfTid.Value, true);
        }

        private static string examID;

        private void loadExamHotLink()
        {
            string username = global_UserCIM.ToString();
            int testCategoryIDValue = 0;
            try
            {
                //testCategoryIDValue = Convert.ToInt32(UTF8.DecryptText(Request.QueryString["tid"]));
                testCategoryIDValue = Convert.ToInt32(UTF8.DecryptText(HttpUtility.UrlDecode(hfTid.Value)));
                examID = testCategoryIDValue.ToString();
            }
            catch (Exception ex)
            {
                ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
                service.WriteError(4, "hotlink.aspx", "Page_Load", ex.Message);
                lblTaking.Visible = true;
                this.lblTaking.Text = Resources.LocalizedResource.Theexamyouwishtotakeisinvalid.ToString();
            }
            TestCategory category = TestCategory.Select(testCategoryIDValue, false);
            if (category == null)
            {
                lblTaking.Visible = true;
                this.lblTaking.Text = Resources.LocalizedResource.Theexamyouwishtotakehasexpired.ToString();
            }
            else
            {
                try
                {

                    int temp = 0;
                    if (int.TryParse(username, out temp))
                    {

                        if (LoadVerifyUser())
                        {
                            SessionManager_Testing.SessionSessionID = Guid.NewGuid();
                            SessionManager_Testing.SessionUsername = username;
                            LoginSession session = LoginSession.Select(username);
                            SessionManager_Testing.SessionCim = Convert.ToInt32(username);
                            //SessionManager_Testing.SessionCompanySite = this.ddlSites.SelectedValue;
                            if (session == null)
                            {
                                try
                                {
                                    session = new LoginSession(username, SessionManager_Testing.SessionSessionID);
                                    session.Insert();
                                }
                                catch
                                {
                                    SessionManager_Testing.SessionMainText = "System could not log you in. Please try again later.";
                                    Response.Redirect("~/hotlink.aspx?" + Request.QueryString.ToString());
                                }
                            }
                            else if (session.LoginSessionID != SessionManager_Testing.SessionSessionID)
                                try
                                {
                                    session.Update();
                                }
                                catch
                                {
                                    SessionManager_Testing.SessionMainText = "System could not log you in. Please try again later.";
                                }
                            SessionManager_Testing.SessionSite = "INT";// this.ddlSites.SelectedValue;TestCategory category = TestCategory.Select(Convert.ToInt32(lblTestCategoryID.Text.Trim()), false);
                            this.GoToExam();
                        }
                        else
                            rwmCourseLauncher.RadAlert("Internal Error please report to the administrator!", 330, 180, "Invalid Credentials", "");  
                    }
                    //if (InternalCredentials.LoginUser(temp, this.txtPassword.Text.Trim()).Result == 0)

                    else if (Registration.ValidateLogin(username, ""))
                    {
                        SessionManager_Testing.SessionSessionID = Guid.NewGuid();
                        SessionManager_Testing.SessionUsername = username;
                        LoginSession session = LoginSession.Select(username);
                        //SessionManager_Testing.SessionCompanySite = this.ddlSites.SelectedValue;
                        if (session == null)
                        {
                            try
                            {
                                session = new LoginSession(username, SessionManager_Testing.SessionSessionID);
                                session.Insert();
                            }
                            catch (Exception ex)
                            {
                                ErrorLogger.Write(Config.ApplicationName, "hotlink.aspx", "lnkLogin_Click", ex.Message);
                                SessionManager_Testing.SessionMainText = "System could not log you in. Please try again later.";
                                rwmCourseLauncher.RadAlert("System could not log you in. Please try again later.", 330, 180, "Invalid Credentials", "");
                                // Response.Redirect("~/hotlink.aspx?" + Request.QueryString.ToString());
                            }
                        }
                        else if (session.LoginSessionID != SessionManager_Testing.SessionSessionID)
                            try
                            {
                                session.Update();
                            }
                            catch (Exception ex)
                            {
                                ErrorLogger.Write(Config.ApplicationName, "hotlink.aspx", "lnkLogin_Click", ex.Message);
                                SessionManager_Testing.SessionMainText = "System could not log you in. Please try again later.";
                                rwmCourseLauncher.RadAlert("System could not log you in. Please try again later.", 330, 180, "Invalid Credentials", "");
                                //Response.Redirect("~/hotlink.aspx?" + Request.QueryString.ToString());
                            }
                        SessionManager_Testing.SessionSite = "EXT";//this.ddlSites.SelectedValue;TestCategory category = TestCategory.Select(Convert.ToInt32(lblTestCategoryID.Text.Trim()), false);
                        GoToExam();
                    }
                }
                catch (Exception ex)
                {
                    if (!ex.Message.Contains("Thread was being aborted"))
                    {
                        ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
                        service.WriteError(4, "hotlink.aspx", "lnkLogin_Click", ex.Message);
                        lblTaking.Visible = true;
                        this.lblTaking.Text = "System has encountered an unknown error. Please contact your examiner/administrator.";
                        //Response.Redirect("~/hotlink.aspx?" + Request.QueryString.ToString());
                    }
                }
            }

        }

        private bool LoadVerifyUser()
        {
            if (DataHelper.GetCurrentUserCIM() != null)
                return true;
            else
                return false;
        }

        protected void GoToExam()
        {
            //ADDED BY RAYMARK COSME <02/13/2016>
            TestCategory category = TestCategory.Select(Convert.ToInt32(examID), false);

            if (category != null)
            {
                SaveTestResponse[] responses = SaveTestResponse.CheckPendingEssays(SessionManager_Testing.SessionUsername, Convert.ToInt32(examID));
                if (responses.Length > 0)
                {
                    lblTaking.Visible = true;
                    this.lblTaking.Text = Resources.LocalizedResource.YouhavetakenthistestItisstillpendingcompletionchecking.ToString();
                    //this.pnlMain.Visible = false;
                    //this.trExamName.Visible = false;
                }
                else
                {
                    int testCount = TestTaken.Count(SessionManager_Testing.SessionUsername, category.TestCategoryID);
                    int testRetake = TestCategory.checkRetake(category.TestCategoryID);
                    if (testCount < category.TestLimit || category.TestLimit == 0)
                    {
                        bool hasPassed = false;
                        TestTaken[] taken = TestTaken.SelectByExam(category.TestCategoryID);
                        foreach (TestTaken test in taken)
                        {
                            if (test.Passed && test.UserID == SessionManager_Testing.SessionUsername)
                            {
                                hasPassed = true;
                                break;
                            }
                        }
                        if (hasPassed == true)
                        {
                            if (testRetake == 1)
                            {
                                SessionManager_Testing.SessionTestCategoryID = category.TestCategoryID;
                                Questionnaire[] questionnaire = Questionnaire.SelectByCategory(category.TestCategoryID);
                                if (questionnaire.Length > 0)
                                {
                                    SessionManager_Testing.SessionQuestionnaire = new List<Questionnaire>();
                                    foreach (Questionnaire question in questionnaire)
                                        SessionManager_Testing.SessionQuestionnaire.Add(question);
                                    SessionManager_Testing.SessionExamAction = "newexam";
                                    SessionManager_Testing.SessionExamIsRetake = "retake"; //TODO: GLA 01092014
                                    //Response.Redirect("~/testdefault.aspx?" + Request.QueryString.ToString());
                                    //goto test default
                                    GotoTestDefault();
                                }
                                else
                                {
                                    lblTaking.Visible = true;
                                    this.lblTaking.Text = Resources.LocalizedResource.Thisexamhasnoitems.ToString();
                                }
                            }
                            else
                            {
                                lblTaking.Visible = true;
                                this.lblTaking.Text = Resources.LocalizedResource.ThisexamhasnoretakePleasecontactyourexaminer.ToString();
                                //this.pnlMain.Visible = false;
                                //this.trExamName.Visible = false;
                            }
                        }
                        else
                        {
                            SessionManager_Testing.SessionTestCategoryID = category.TestCategoryID;
                            Questionnaire[] questionnaire = Questionnaire.SelectByCategory(category.TestCategoryID);
                            if (questionnaire.Length > 0)
                            {
                                SessionManager_Testing.SessionQuestionnaire = new List<Questionnaire>();
                                foreach (Questionnaire question in questionnaire)
                                    SessionManager_Testing.SessionQuestionnaire.Add(question);
                                SessionManager_Testing.SessionExamAction = "newexam";
                                //Response.Redirect("~/testdefault.aspx?" + Request.QueryString.ToString());
                                //goto test default
                                GotoTestDefault();
                            }
                        }
                    }
                    else
                    {
                        lblTaking.Visible = true;
                        this.lblTaking.Text = Resources.LocalizedResource.Youhavealreadyusedupalltriesavailableforthistest.ToString();
                    }
                }
            }
            else
            {
                lblTaking.Visible = true;
                lblTaking.Text = Resources.LocalizedResource.ThisexamhasbeenremovedfromthesystemorhasalreadyexpiredPleasecontactyourexaminer.ToString();
            }
        }

    }

}

