﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TranscomUniversityV3.Master" AutoEventWireup="true"
    CodeFile="CourseLauncher2.aspx.cs" Inherits="CourseLauncher2" %>

<%@ Register Src="UserControl/Statics/CourseCatalogSidebarUserCtrl.ascx" TagName="courseCatalogSidebar"
    TagPrefix="ucCcSidebar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="contentPlaceHolderLeftPanel" runat="Server">
    <rad:RadWindowManager ID="rwmCourseLauncher" runat="server" RenderMode="Lightweight"
        EnableShadow="true" Modal="true" VisibleOnPageLoad="false" Behaviors="Close, Move"
        DestroyOnClose="true" Opacity="99" VisibleStatusbar="false" Skin="Bootstrap">
    </rad:RadWindowManager>
    <rad:RadAjaxLoadingPanel ID="localLoadingPanel" runat="server" CssClass="Loading2"
        Transparency="25" />
    <rad:RadAjaxLoadingPanel ID="noLoadingPanel" runat="server" CssClass="Loading-Empty"
        Transparency="25" />
    <ucCcSidebar:courseCatalogSidebar ID="ccSidebar" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolderMain" runat="Server">
    <div class="row course-preview-row-container bottom-margin-md">
        <div class="col-md-12 course-preview-header">
            <ul id="ulBreadcrumb" runat="server" class="breadcrumb">
                <li id="bc1" runat="server"><a>
                    <asp:Label ID="lblTab0" runat="server" Text="Learner" /></a></li>
                <li id="bc2" runat="server"><a>
                    <asp:Label ID="lblTab1" runat="server" Text="Course Catalog" /></a></li>
                <li id="bc3" runat="server"><a>
                    <asp:Label ID="lblTab2" runat="server" Text="" /></a></li>
                <li id="bc4" runat="server">
                    <asp:Label ID="lblTab3" runat="server" Text="" ForeColor="Yellow" /></li>
            </ul>
        </div>
        <div class="col-md-12 course-preview-container" id="coursePreviewContainer">
            <div class="row">
                <asp:Panel ID="pnlCoursePrev" runat="server" CssClass="col-sm-12 js-course-preview">
                    <div class="row black-header ten-px-padding vertical-align m-launcher-nav">
                        <div class="col-md-6 col-xs-2 font-bold m-launcher-back">
                            <%--        <a href="CourseCatalog.aspx" class="fa fa-arrow-circle-left link-no-hover" ></a>--%>
                            <button onclick="back();" style="background: transparent;">
                                <i class="fa fa-chevron-circle-left link-no-hover" style="font-size: 30px; color: White;
                                    text-decoration: none;"></i>
                            </button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 course-prev-cover-photo" id="BgCourse" runat="server" clientidmode="Static">
                            <div class="layer">
                            </div>
                            <div class="course-preview-title text-center">
                                <h1>
                                    <asp:Label ID="LblCourseTitle" runat="server" Text="Sample course 2017" ClientIDMode="Static" />
                                </h1>
                            </div>
                            <div class="row m-preview-description">
                                <div class="absolute-horizontal-center padding-side-10 color-white border-bottom-white ">
                                    <h4>
                                        Description
                                    </h4>
                                </div>
                            </div>
                            <div class="row m-preview-description">
                                <div class="col-md-10 col-md-offset-1 text-center color-white course-preview-description scrollbox">
                                    <div class="scrollbox-content">
                                        <asp:Label ID="LblDesc" runat="server" Text="Descriptio" ClientIDMode="Static" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="right-side-content text-right">
                                    <b>
                                        <asp:Label ID="LblType" runat="server" Text="OLT" /></b>
                                    <br />
                                    <b>Status:</b>
                                    <asp:Label ID="lblCourseStatus" runat="server" Text="Not yet started" ClientIDMode="Static" />
                                    <br />
                                    <b>Upload Date:</b>
                                    <asp:Label ID="LblUploadDate" runat="server" Text="Jan. 1, 2000" ClientIDMode="Static" />
                                    <br />
                                    <b>Valid Until:</b>
                                    <asp:Label ID="LblValidDate" runat="server" Text="Jan. 1, 2000" ClientIDMode="Static" />
                                    <br />
                                    <b>Category:</b>
                                    <asp:Label ID="lblCategory" runat="server" Text="Transcom" ClientIDMode="Static" />
                                    <br />
                                    <b>Sub-category:</b>
                                    <asp:Label ID="lblSubcategory" runat="server" Text="Transcom" ClientIDMode="Static" />
                                    <asp:HiddenField ID="hfIsDue" runat="server" Value="" ClientIDMode="Static" />
                                    <asp:HiddenField ID="hfIsInProgress" runat="server" Value="" ClientIDMode="Static" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <asp:Panel ID="MainCourseDetails" runat="server">
                        <div class="display-inline-flex relative-hr-center">
                        </div>
                        <div class="row">
                            <div class="col-sm-12 border-top-light-dark-gray">
                            </div>
                        </div>
                        <div class="row text-center">
                            <div class="col-sm-offset-3 col-sm-6">
                                <div class="row">
                                    <div class="col-sm-4 col-xs-4">
                                        <span class="text-center">
                                            <asp:Image ImageUrl="~/Media/Images/star-outline-in-a-circle-line.png" ID="ImgStar"
                                                runat="server" /></span></div>
                                    <div class="col-sm-4 col-xs-4">
                                        <asp:Image ImageUrl="~/Media/Images/search-with-magnifier-in-circular-button.png"
                                            ID="Image1" runat="server" /></div>
                                    <div class="col-sm-4 col-xs-4">
                                        <span class="text-center">
                                            <asp:Image ImageUrl="~/Media/Images/chat-circular-button.png" ID="Image2" runat="server" /></span></div>
                                </div>
                            </div>
                            <div class="col-sm-offset-3 col-sm-6">
                                <div class="row">
                                    <div class="col-sm-4 col-xs-4 text-center">
                                        <asp:Label ID="lblStarRating" runat="server" ClientIDMode="Static"> 5.5 </asp:Label>
                                        Star Rating</div>
                                    <div class="col-sm-4 col-xs-4 text-center">
                                        <asp:Label ID="lblViews" runat="server" ClientIDMode="Static"> 9,999 </asp:Label>
                                        Views</div>
                                    <div class="col-sm-4 col-xs-4 text-center">
                                        <asp:Label ID="lblReviews" runat="server" ClientIDMode="Static"> 9,999 </asp:Label>
                                        Reviews</div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 border-top-light-dark-gray">
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="row">
                                        <div class="col-md-12 col-xs-12 m-rating-container">
                                            <span class="pull-right">
                                                <h2>
                                                    <asp:Label ID="lblCourseStatRating" runat="server" ClientIDMode="Static"> 5.5 </asp:Label>
                                                </h2>
                                            </span>
                                        </div>
                                        <div class="col-md-12 col-xs-12 m-star-container">
                                            <span class="pull-right">
                                                <rad:RadRating RenderMode="Lightweight" ID="rrCourseStatRating" runat="server" ItemCount="5"
                                                    Value="4.5" SelectionMode="Continuous" Precision="Exact" Orientation="Horizontal"
                                                     Enabled="true" >
                                                </rad:RadRating>
                                            </span>
                                        </div>
                                        <div class="col-md-12 col-xs-12 m-user-container">
                                            <span class="pull-right">
                                                <h4>
                                                    <i class="fa fa-users"></i>
                                                    <asp:Label ID="lblUserCount" runat="server" ClientIDMode="Static">999,999</asp:Label>
                                                    Total Users</h4>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="col-md-12">
                                                <div class="row vertical-align">
                                                    <div class="col-xs-2 no-paddings">
                                                        <h5 class="font-bold">
                                                            <i class="fa fa-star"></i>&nbsp;&nbsp;&nbsp;5</h5>
                                                    </div>
                                                    <div class="col-xs-10  no-paddings nowrap">
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-fivestar" role="progressbar" runat="server"
                                                                id="FiveStarBar">
                                                            </div>
                                                            <asp:Label ID="lblFiveStarCount" runat="server"></asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="row vertical-align">
                                                    <div class="col-xs-2  no-paddings">
                                                        <h5 class="font-bold">
                                                            <i class="fa fa-star"></i>&nbsp;&nbsp;&nbsp;4</h5>
                                                    </div>
                                                    <div class="col-xs-10  no-paddings nowrap">
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-fourstar" role="progressbar" style="width: 40%"
                                                                runat="server" id="FourStarBar">
                                                            </div>
                                                            <asp:Label ID="lblFourStarCount" runat="server"></asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="row vertical-align">
                                                    <div class="col-xs-2  no-paddings">
                                                        <h5 class="font-bold">
                                                            <i class="fa fa-star"></i>&nbsp;&nbsp;&nbsp;3</h5>
                                                    </div>
                                                    <div class="col-xs-10  no-paddings nowrap">
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-threestar" role="progressbar" style="width: 40%"
                                                                runat="server" id="ThreeStarBar">
                                                            </div>
                                                            <asp:Label ID="lblThreeStarCount" runat="server"></asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="row vertical-align">
                                                    <div class="col-xs-2 no-paddings">
                                                        <h5 class="font-bold">
                                                            <i class="fa fa-star"></i>&nbsp;&nbsp;&nbsp;2</h5>
                                                    </div>
                                                    <div class="col-xs-10 no-paddings nowrap">
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-twostar" role="progressbar" style="width: 40%"
                                                                runat="server" id="TwoStarBar">
                                                            </div>
                                                            <asp:Label ID="lblTwoStarCount" runat="server"></asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="row vertical-align">
                                                    <div class="col-xs-2 no-paddings">
                                                        <h5 class="font-bold">
                                                            <i class="fa fa-star"></i>&nbsp;&nbsp;&nbsp;1</h5>
                                                    </div>
                                                    <div class="col-xs-10 no-paddings nowrap">
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-onestar" role="progressbar" runat="server"
                                                                id="OneStarBar">
                                                            </div>
                                                            <asp:Label ID="lblOneStarCount" runat="server"></asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="horizontal-dotted-gray margin-bottom-10px display-none">
                            </div>
                        </div>
                        <div class="modal fade success-popup tc-modal-1 top-9999" id="forumGuidelinesWindown"
                            tabindex="-1">
                            <div class="modal-dialog modal-sm" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span></button>
                                        <h4 class="modal-title" id="H1">
                                            Remove Group
                                        </h4>
                                    </div>
                                    <div class="modal-body text-center">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <i class="fa fa-exclamation-triangle warning-icon"></i>
                                            </div>
                                            <div class="col-md-10">
                                                <p class="confirmationMsg" style="text-align: left;">
                                                    Are you sure you want to
                                                    <br />
                                                    remove <i id="modalGroupName" class="bold-only"></i>in this Course?
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <i data-toggle="modal" data-target="#confirmationModalGroup" style="font-style: normal;
                                            font-weight: bold;" class="pull-right ">
                                            <asp:LinkButton ID="btnRemoveGroup" runat="server" Text="YES" runat="server" CssClass="btn tc-btn-md btn-teal tc-btn-flat padding-side-10"></asp:LinkButton></i>
                                        <a class="btn tc-btn-md btn-default pull-right tc-btn-flat" data-toggle="modal" data-target="#confirmationModalGroup">
                                            NO</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel ID="pnlCourseForum" runat="server" CssClass="col-sm-4 js-course-forum display-none"
                    Visible="false">
                    <div class="container">
                        <div class="row black-header ten-px-padding vertical-align btn-flat">
                            <div class="col-md-12 font-bold" style="line-height: 30px;">
                                <asp:Label ID="ltForumHeader" runat="server">
                                        Course Forum Threads
                                </asp:Label>
                            </div>
                        </div>
                    </div>
                    <asp:Panel ID="forumContainer" runat="server" CssClass="container pnl-forum">
                        <asp:PlaceHolder ID="phForum" runat="server"></asp:PlaceHolder>
                    </asp:Panel>
                </asp:Panel>
            </div>
        </div>
        <asp:HiddenField ID="hfSelectedThread" ClientIDMode="Static" runat="server" />
        <asp:Button ID="btnSignin" runat="server" CssClass="display-none" OnClick="btnRequestLogin_Click" />
        <asp:HiddenField ID="hfCourseImage" runat="server" ClientIDMode="Static" />
    </div>
    <rad:RadScriptBlock ID="RadScriptBlock1" runat="server">
        <script type="text/javascript">

            function pageLoad() {
                loadCourseDetails();
            }

            function loadCourseDetails() {
                PageMethods.GetCourseDetails(
                function (res) {
                    debugger;
                    CourseID = res.CourseID;
                    Title = res.Title;
                    CourseImage = res.CourseImage;
                    Description = res.Description;
                    StartDate = res.StartDate;
                    EndDate = res.EndDate;
                    CategoryName = res.CategoryName;
                    Subcategory = res.Subcategory;

                    $("#LblCourseTitle").get(0).innerHTML = Title;
                    $("#LblDesc").get(0).innerHTML = Description;
                    $("#LblUploadDate").get(0).innerHTML = moment(StartDate).format("MMMM DD, YYYY");
                    $("#LblValidDate").get(0).innerHTML = moment(EndDate).format("MMMM DD, YYYY");
                    $("#lblCategory").get(0).innerHTML = CategoryName;
                    $("#lblSubcategory").get(0).innerHTML = Subcategory;


                    cimagePath = "Media/Uploads/CourseImg/" + CourseID + "/" + CourseImage;
                    $("#BgCourse").get(0).style.backgroundImage = 'url("' + cimagePath + '"), url("Media/Uploads/CourseImg/No_image.jpg")';
                    loadCourseReviewDetails();

                },
                function (error) {
                    var code = error._statusCode;
                    if (code == 401)
                        location.reload();
                    else if (code == 500) {
                        console.log(error);
                        radalert("Internal Server Error please inform the Page Admin", 330, 180, "Error Message", "");
                    }


                });
            }

            function loadCourseReviewDetails() {
                PageMethods.GetCourseReviewDetails(
                function (res) {
                    //res = res[0];
                    debugger;
                    cStats = res.reviewStats;
                    cRatings = res.reviewRatings;

                    Rating = cStats.Rating;
                    Views = cStats.Views;
                    Reviews = cStats.Reviews;
                    Users = cStats.Users;

                    $("#lblStarRating").get(0).innerHTML = parseFloat(Rating).toFixed(1)
                    $("#lblViews").get(0).innerHTML = Views.toLocaleString();
                    $("#lblReviews").get(0).innerHTML = Reviews.toLocaleString();
                    $("#lblUserCount").get(0).innerHTML = Users.toLocaleString();
                    //$("#rrCourseStatRating").get(0).innerHTML = parseFloat(Rating).toFixed(1)
                    $("#lblCourseStatRating").get(0).innerHTML = parseFloat(Rating).toFixed(1);
                    var oRating = getRatingObject();
                    oRating.set_value(parseFloat(Rating).toFixed(1));
                    return;

                },
                function (error) {
                    var code = error._statusCode;
                    if (code == 401)
                        location.reload();
                    else if (code == 500) {
                        console.log(error);
                        radalert("Internal Server Error please inform the Page Admin", 330, 180, "Error Message", "");
                    }


                });
            }

            function getRatingObject() {
                return $find("<%= rrCourseStatRating.ClientID %>")
            }

        </script>
    </rad:RadScriptBlock>
</asp:Content>
