﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using TranscomUniversityV3Model;
using System.IO;
using Microsoft.AspNet.Membership.OpenAuth;
using System.Data;
using System.Net;

public partial class CourseLauncher2 : System.Web.UI.Page
{
    static int global_CourseID;
    static string global_EncCourseID;
    static int global_UserCIM;
    static bool global_isLoggedIn;
    static bool global_isEnrolled;
    RadGrid gridBundle;
    static string permanentPath;
    public string ReturnUrl { get; set; }
    static TranscomUniversityV3ModelContainer db;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            db = new TranscomUniversityV3ModelContainer();
            global_isLoggedIn = (System.Web.HttpContext.Current.User != null) && System.Web.HttpContext.Current.User.Identity.IsAuthenticated;
            global_UserCIM = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
            global_CourseID = Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"]));


            //if (global_isLoggedIn)
            //    loadCourseDetails(global_UserCIM, global_CourseID);
            //else
            //    loadCourseDetails(global_CourseID);


        }
    }

    private void setBreadcrumb()
    {
        string courseName = DataHelper.getCourseName(global_CourseID);
        string Subcat = DataHelper.GetSubcategoryNameByCourseID(global_CourseID);
        if (DataHelper.GetCurrentUserCIM() == null || DataHelper.GetCurrentUserCIM() == "")
        {
            lblTab0.Text = "Search Results";
            lblTab1.Text = courseName;
            bc1.Visible = true;
            bc2.Visible = true;
            bc3.Visible = false;
            bc4.Visible = false;
        }
        else
        {
            bc1.Visible = true;
            bc2.Visible = true;
            bc3.Visible = true;
            bc4.Visible = true;
            lblTab0.Text = "Learner";
            lblTab1.Text = "Course Catalog";
            lblTab2.Text = Subcat;
            lblTab3.Text = courseName;
        }

    }

    #region Forum

    private bool isForumEnabled(int courseId)
    {

        
        var showForum = db.tbl_TranscomUniversity_Rlt_CourseSetting.Where(c => c.CourseID == courseId && c.ForumWidget == true).SingleOrDefault();
        if (showForum != null)
            return true;

        return false;

    }

    [System.Web.Services.WebMethod()]
    public static int btnHelpfulRes(int postId)
    {
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            int postid = Convert.ToInt32(postId);
            int usercim = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
            pr_TranscomUniversity_InsertHelpfulPost_Result res = db.pr_TranscomUniversity_InsertHelpfulPost(postid, usercim).SingleOrDefault();
            return res.Response;
        }
    }

    [System.Web.Services.WebMethod()]
    public static int btnSubmitReport(int postId, string txtReportMessage)
    {
        int usercim = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
        string reportmessage = txtReportMessage;
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            pr_TranscomUniversity_InsertReportPost_Result res = db.pr_TranscomUniversity_InsertReportPost(postId, usercim, reportmessage).SingleOrDefault();
            return res.Response;
        }
    }

    [System.Web.Services.WebMethod()]
    public static int btnSubmitReply(int postId, int threadId, string txtMessage)
    {

        var userCim = DataHelper.GetCurrentUserCIM();


        int usercim = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            pr_TranscomUniversity_InsertPost_Result res = db.pr_TranscomUniversity_InsertPost(threadId, postId, usercim, txtMessage).SingleOrDefault();

            //if (res.Response == 1) {
            //    var posts = DataHelper.threadPosts(_threadid, usercim);
            //    System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            //    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            //    Dictionary<string, object> row;
            //    foreach (DataRow dr in posts.Rows)
            //    {
            //        row = new Dictionary<string, object>();
            //        foreach (DataColumn col in posts.Columns)
            //        {
            //            row.Add(col.ColumnName, dr[col]);
            //        }
            //        rows.Add(row);
            //    }
            //    return new { res = res.Response, postData = serializer.Serialize(rows) };
            //}else
            //    return new { res = res.Response};

            return res.Response;
        }
        //refreshForum();
    }
    private static IEnumerable<CourseDetails> _similarCourse;
    private static int clientCourseId;

    [System.Web.Services.WebMethod()]
    public static IEnumerable<CourseDetails> GetSimilarCourseData(int startRowIndex, int maximumRows, string sortExpression, string CourseId)
    {

        clientCourseId = Convert.ToInt32(Utils.Decrypt(CourseId));

        return RadGrid.GetBindingData(LoadSimilarCourse().AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data;
    }

    [System.Web.Services.WebMethod()]
    public static int GetSimilarCount()
    {
        return LoadSimilarCourse().Count();

    }

    [System.Web.Services.WebMethod()]
    public static IEnumerable<CourseDetails> LoadSimilarCourse()
    {
        TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();

        _similarCourse = db.pr_TranscomUniversity_SimilarCourses(clientCourseId)
                            .AsEnumerable()
                            .Select(c => new CourseDetails
                            {
                                EncryptedCourseID = Utils.Encrypt((int)c.CourseID),
                                CourseID = (int)c.CourseID,
                            })
                            .ToList();
        //string json = new JavaScriptSerializer().Serialize(similarCourse);
        return _similarCourse;

    }

    [System.Web.Services.WebMethod()]
    public static int btnLeaveForum_Click(string encCourseId)
    {

        var db = new TranscomUniversityV3ModelContainer();
        int courseId = Convert.ToInt16(Utils.Decrypt(encCourseId));
        int userCim = Convert.ToInt32(DataHelper.GetCurrentUserCIM());

        //leave
        pr_TranscomUniversity_RemoveForumMember_Result res = db.pr_TranscomUniversity_RemoveForumMember(courseId, userCim).SingleOrDefault();
        return res.Response;
    }

    public class CourseDetails
    {
        public string EncryptedCourseID { get; set; }

        public int CourseID { get; set; }

        public string CourseImage { get; set; }

    }

    protected void imagePermanentPath(int threadID, UploadedFile f)
    {

        string error = null;

        if (string.IsNullOrWhiteSpace(error))
        {
            string uploadFolder;
            var userCim = DataHelper.GetCurrentUserCIM();

            if (HttpContext.Current.Request.Url.Host == "localhost")
                uploadFolder = Request.PhysicalApplicationPath + "Media\\Uploads\\Thread\\" + threadID + "\\";
            else
                uploadFolder = HttpRuntime.AppDomainAppPath + "Media\\Uploads\\Thread\\" + threadID + "\\";

            string directoryPath = Server.MapPath(string.Format("~/{0}/", "Media/Uploads/Thread/" + threadID + "\\"));

            if (!Directory.Exists(directoryPath))
                Directory.CreateDirectory(directoryPath);

            Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            string extension = Path.GetExtension(f.FileName);
            permanentPath = uploadFolder + unixTimestamp.ToString() + f.FileName;
            f.SaveAs(permanentPath, true);
        }


    }

    #endregion

    #region LoadCourseDetails

    void loadCourseDetails(int enrolleeCIM, int CourseID)
    {
        DataSet ds = DataHelper.GetCourseDetails(CourseID);
        LblCourseTitle.Text = ds.Tables[0].Rows[0]["Title"].ToString();
        LblDesc.Text = ds.Tables[0].Rows[0]["Description"].ToString();
        LblUploadDate.Text = ((DateTime)(ds.Tables[0].Rows[0]["StartDate"])).ToString("MMMM dd, yyyy");
        LblValidDate.Text = ((DateTime)(ds.Tables[0].Rows[0]["EndDate"])).ToString("MMMM dd, yyyy");
        lblCategory.Text = (ds.Tables[0].Rows[0]["CategoryName"]).ToString();
        lblSubcategory.Text = (ds.Tables[0].Rows[0]["Subcategory"]).ToString();
        string CourseImg = "Media/Uploads/CourseImg/" + CourseID.ToString() + "/" + HttpUtility.HtmlEncode(ds.Tables[0].Rows[0]["CourseImage"].ToString());
        BgCourse.Attributes["style"] = "background-image: url('" + CourseImg + "'), url('Media/Uploads/CourseImg/No_image.jpg')";

        int type = Convert.ToInt32(ds.Tables[0].Rows[0]["TrainingTypeID"]);

        if (type == 1)
        {
            //OLT
            LblType.Text = "OLT";

        }
        else
        {
            //ILT
            LblType.Text = "ILT";
        }

       

        lblCourseStatus.Text = "------";



        hfCourseImage.Value = WebUtility.HtmlEncode(CourseImg);
       
        int status = DataHelper.EnrollmentStatus(enrolleeCIM, CourseID);
        //ltCourseName.Text = ds.Tables[0].Rows[0]["Title"].ToString();
        //lblValid.Text = ((DateTime)(ds.Tables[0].Rows[0]["EndDate"])).ToString("MMMM dd, yyyy");
        //imgPicStatus.Src = HttpContext.Current.User.Identity.Name.Split('|')[2].ToString();
        //updateBtnEnrolled(status, DataHelper.isCourseValid(CourseID));
        //hfRating.Value = userRating.ToString();
        //double userCount = db.vw_TranscomUniversity_ApprovedEnrollments.Where(a => a.CourseID == CourseID).Count();
        decimal userRating = db.vw_TranscomUniversity_CourseReview.Where(u => u.EnrolleeCIM == enrolleeCIM && u.CourseID == CourseID).Select(u => (decimal)u.Rating).SingleOrDefault();
        // txtComment.Text = db.vw_TranscomUniversity_CourseReview.Where(u => u.EnrolleeCIM == enrolleeCIM && u.CourseID == CourseID).Select(u => u.Comment).SingleOrDefault();
        //rrCourse.Value = userRating;
        //lblName.Text = DataHelper.getUserName(enrolleeCIM);
        double userCount = 0.0;
        try
        {
            var courseStats = DataHelper.courseReviewStats(CourseID);
            lblStarRating.Text = string.Format("{0:0.0}", courseStats[0].Rating);
            lblViews.Text = string.Format("{0:n0}", courseStats[0].Views);
            lblReviews.Text = string.Format("{0:n0}", courseStats[0].Reviews);
            userCount = Convert.ToInt32(string.Format("{0:n0}", courseStats[0].Users));
            lblUserCount.Text = string.Format("{0:n0}", courseStats[0].Users);
            rrCourseStatRating.Value = courseStats[0].Rating;
            lblCourseStatRating.Text = string.Format("{0:0.0}", courseStats[0].Rating);
        }
        catch (Exception)
        {

            lblStarRating.Text = string.Format("{0:0.0}", 0);
            lblViews.Text = string.Format("{0:n0}", 0);
            lblReviews.Text = string.Format("{0:n0}", 0);
            lblUserCount.Text = string.Format("{0:n0}", userCount);
            rrCourseStatRating.Value = 0;
            lblCourseStatRating.Text = string.Format("{0:0.0}", 0);
        }

        var comments = db.vw_TranscomUniversity_CourseComments.Where(c => c.CourseID == CourseID);
        double FiveStarCount = comments.Where(c => c.Rating == 5).Count();
        double FourStarCount = comments.Where(c => c.Rating == 4).Count();
        double ThreeStarCount = comments.Where(c => c.Rating == 3).Count();
        double TwoStarCount = comments.Where(c => c.Rating == 2).Count();
        double OneStarCount = comments.Where(c => c.Rating == 1).Count();

        lblFiveStarCount.Text = string.Format("{0:n0}", FiveStarCount.ToString());
        lblFourStarCount.Text = string.Format("{0:n0}", FourStarCount.ToString());
        lblThreeStarCount.Text = string.Format("{0:n0}", ThreeStarCount.ToString());
        lblTwoStarCount.Text = string.Format("{0:n0}", TwoStarCount.ToString());
        lblOneStarCount.Text = string.Format("{0:n0}", OneStarCount.ToString());
        FiveStarBar.Attributes.Add("style", "width:" + calculateRate(FiveStarCount, userCount) + "%");
        FourStarBar.Attributes.Add("style", "width:" + calculateRate(FourStarCount, userCount) + "%");
        ThreeStarBar.Attributes.Add("style", "width:" + calculateRate(ThreeStarCount, userCount) + "%");
        TwoStarBar.Attributes.Add("style", "width:" + calculateRate(TwoStarCount, userCount) + "%");
        OneStarBar.Attributes.Add("style", "width:" + calculateRate(OneStarCount, userCount) + "%");
    }

    void loadCourseDetails(int CourseID)
    {
        bool isDue = false;

        DataSet ds = DataHelper.GetCourseDetails(CourseID);
        LblCourseTitle.Text = ds.Tables[0].Rows[0]["Title"].ToString();
        LblDesc.Text = ds.Tables[0].Rows[0]["Description"].ToString();


        int type = 1;

        if (type == 1)
        {
            //OLT
            LblType.Text = "OLT";

            lblCourseStatus.Visible = false;
            LblUploadDate.Text = ((DateTime)(ds.Tables[0].Rows[0]["StartDate"])).ToString("MMMM dd, yyyy");
            LblValidDate.Text = ((DateTime)(ds.Tables[0].Rows[0]["EndDate"])).ToString("MMMM dd, yyyy");
            //lblValid.Text = ((DateTime)(ds.Tables[0].Rows[0]["EndDate"])).ToString("MMMM dd, yyyy");
            lblCategory.Text = (ds.Tables[0].Rows[0]["CategoryName"]).ToString();
            lblSubcategory.Text = (ds.Tables[0].Rows[0]["Subcategory"]).ToString();
            isDue = (DateTime)(ds.Tables[0].Rows[0]["EndDate"]) < DataHelper.serverDate();
            hfIsDue.Value = isDue == true ? "1" : "0";
            int? isInProgress = 0;
            hfIsInProgress.Value = isInProgress == null ? "0" : isInProgress.ToString();
        }
        else
        {
            //ILT

        }


        //pnlRating.Visible = false;

        string CourseImg = "Media/Uploads/CourseImg/" + CourseID.ToString() + "/" + ds.Tables[0].Rows[0]["CourseImage"].ToString();
        BgCourse.Attributes["style"] = "background-image: url('" + CourseImg + "'), url('Media/Uploads/CourseImg/No_image.jpg')";


        //txtComment.Visible = false;
        //rrCourse.Visible = false;
        //lblName.Visible = false;
        //hfOldValue.Visible = false;

        double userCount = 0.0;
        try
        {
            var courseStats = DataHelper.courseReviewStats(CourseID);
            lblStarRating.Text = string.Format("{0:0.0}", courseStats[0].Rating);
            lblViews.Text = string.Format("{0:n0}", courseStats[0].Views);
            lblReviews.Text = string.Format("{0:n0}", courseStats[0].Reviews);
            userCount = Convert.ToInt32(string.Format("{0:n0}", courseStats[0].Users));
            lblUserCount.Text = string.Format("{0:n0}", courseStats[0].Users);
            rrCourseStatRating.Value = courseStats[0].Rating;
            lblCourseStatRating.Text = string.Format("{0:0.0}", courseStats[0].Rating);

        }
        catch (Exception)
        {

            lblStarRating.Text = string.Format("{0:0.0}", 0);
            lblViews.Text = string.Format("{0:n0}", 0);
            lblReviews.Text = string.Format("{0:n0}", 0);
            lblUserCount.Text = string.Format("{0:n0}", userCount);
            rrCourseStatRating.Value = 0;
            lblCourseStatRating.Text = string.Format("{0:0.0}", 0);

        }


        double FiveStarCount = db.vw_TranscomUniversity_CourseComments.Where(c => c.CourseID == CourseID && c.Rating == 5).Count();
        double FourStarCount = db.vw_TranscomUniversity_CourseComments.Where(c => c.CourseID == CourseID && c.Rating == 4).Count();
        double ThreeStarCount = db.vw_TranscomUniversity_CourseComments.Where(c => c.CourseID == CourseID && c.Rating == 3).Count();
        double TwoStarCount = db.vw_TranscomUniversity_CourseComments.Where(c => c.CourseID == CourseID && c.Rating == 2).Count();
        double OneStarCount = db.vw_TranscomUniversity_CourseComments.Where(c => c.CourseID == CourseID && c.Rating == 1).Count();

        lblFiveStarCount.Text = string.Format("{0:n0}", FiveStarCount.ToString());
        lblFourStarCount.Text = string.Format("{0:n0}", FourStarCount.ToString());
        lblThreeStarCount.Text = string.Format("{0:n0}", ThreeStarCount.ToString());
        lblTwoStarCount.Text = string.Format("{0:n0}", TwoStarCount.ToString());
        lblOneStarCount.Text = string.Format("{0:n0}", OneStarCount.ToString());

        FiveStarBar.Attributes.Add("style", "width:" + calculateRate(FiveStarCount, userCount) + "%");
        FourStarBar.Attributes.Add("style", "width:" + calculateRate(FourStarCount, userCount) + "%");
        ThreeStarBar.Attributes.Add("style", "width:" + calculateRate(ThreeStarCount, userCount) + "%");
        TwoStarBar.Attributes.Add("style", "width:" + calculateRate(TwoStarCount, userCount) + "%");
        OneStarBar.Attributes.Add("style", "width:" + calculateRate(OneStarCount, userCount) + "%");

        //BtnJoinForum.Visible = false;
        //BtnJoinForumFake.Visible = false;
        //BtnLeaveForum.Visible = false;
        //BtnLeaveForumFake.Visible = false;
        //radTakeLaterCourses.Visible = false;
        //pnlOverallProgress.Visible = false;
        //BtnEnrolled.OnClientClick = "showPleaseSignin(1); return false;";
        //pnlAdditional.Visible = false;
        //pnlTrainer.Visible = false;

    }

    private double calculateRate(double starCount, double userCount)
    {
        double rating = (starCount / userCount);
        return Math.Round(rating * 100, 2);
    }

    #endregion

    #region logged out state
    protected void btnRequestLogin_Click(object sender, EventArgs e)
    {
        var redirectUrl = "~/ExternalLandingPage.aspx";

        if (!String.IsNullOrEmpty(ReturnUrl))
        {
            var resolvedReturnUrl = ResolveUrl(ReturnUrl);
            redirectUrl += "?ReturnUrl=" + HttpUtility.UrlEncode(resolvedReturnUrl);
        }

        OpenAuth.RequestAuthentication("google", redirectUrl);
    }
    #endregion

    [System.Web.Services.WebMethod()]
    public static dynamic GetCourseDetails()
    {
        //int courseID = Convert.ToInt32(Utils.Decrypt(encCourseID));
        var courseDetails = DataHelper.GetCourseDetails2(global_CourseID);

        return courseDetails;
      

    }

    [System.Web.Services.WebMethod()]
    public static dynamic GetCourseReviewDetails()
    {
        //int courseID = Convert.ToInt32(Utils.Decrypt(encCourseID));
        var courseDetails = DataHelper.courseReviewStats(global_CourseID);
        var courseRatings = DataHelper.GetCourseRating2(global_CourseID);
        CourseReviewDetails cd = new CourseReviewDetails();
        cd.reviewRatings = courseRatings;
        cd.reviewStats = courseDetails;
        return cd;
    }

    public class CourseReviewDetails{
        public dynamic reviewStats { get; set; }
        public dynamic reviewRatings { get; set; }
    }
}