﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TranscomUniversityV3.Master" AutoEventWireup="true"
    CodeFile="CoursePreview.aspx.cs" Inherits="CoursePreview" %>

<%@ Register TagPrefix="ucUC" TagName="UploadContent" Src="~/SCORM/DnsControls/UploadContent.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="contentPlaceHolderLeftPanel" runat="Server">
<rad:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        function back() {
            window.history.back();
        }
    </script>
</rad:RadScriptBlock>
    <div class="sidebar-relative">
        <div class="side-menu-container">
            <div class="overlay">
            </div>
            <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
                <ul class="nav sidebar-nav">
                    <li class="sidebar-brand" style="z-index: 1000; color: #fff;">
                        <a href="#" class="close"><i class="fa fa-times" aria-hidden="true"></i></a> Admin Task Menu
                    </li>
                    <rad:RadTabStrip ID="RadTabStrip1" runat="server" RenderMode="Lightweight" SelectedIndex="0" Skin="Black" AutoPostBack="true" ><%--OnClientTabSelected="onClientTabSelected"--%>
                        <Tabs>
                            <rad:RadTab runat="server" Text="<%$ Resources:LocalizedResource, CourseManagement %>" Value="1" NavigateUrl="Admin.aspx?Tab=CourseMgmt" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                            <rad:RadTab runat="server" Text="<%$ Resources:LocalizedResource, CreateACourse %>" Value="2" NavigateUrl="Admin.aspx?Tab=CreateCourse" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                            <rad:RadTab runat="server" Text="<%$ Resources:LocalizedResource, CourseCategories %>" Value="3" NavigateUrl="Admin.aspx?Tab=CourseCat" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                            <rad:RadTab runat="server" Text="<%$ Resources:LocalizedResource, ClassManagement %>" Value="4" NavigateUrl="Admin.aspx?Tab=ClassMgmt" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                            <rad:RadTab runat="server" Text="<%$ Resources:LocalizedResource, CreateAClass %>" Value="5" NavigateUrl="Admin.aspx?Tab=CreateClass" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                            <rad:RadTab runat="server" Text="<%$ Resources:LocalizedResource, PageEditor %>" Width="285px" SelectedCssClass="rtsSelected">
                                <Tabs>
                                    <rad:RadTab Text="Sign-in Page Layout" Font-Underline="false" Enabled="false" ForeColor="Yellow"></rad:RadTab>
                                    <rad:RadTab Text="<%$ Resources:LocalizedResource, ImageSlider %>Image Slider" NavigateUrl="PlaylistManager.aspx"></rad:RadTab>
                                    <rad:RadTab Text="<%$ Resources:LocalizedResource, FeaturedCourses %>Featured Courses" NavigateUrl="PlaylistManager.aspx?tab=featured-courses"></rad:RadTab>
                                    <rad:RadTab Text="<%$ Resources:LocalizedResource, LatestNews %>Latest News" NavigateUrl="PlaylistManager.aspx?tab=news"></rad:RadTab>
                                </Tabs>
                                <Tabs>
                                    <rad:RadTab Text="Displayed Courses" Font-Underline="false" Enabled="false" ForeColor="Yellow"></rad:RadTab>
                                    <rad:RadTab Text="<%$ Resources:LocalizedResource, CourseCatalog %>Course Catalog" NavigateUrl="#"></rad:RadTab>
                                    <rad:RadTab Text="<%$ Resources:LocalizedResource, TrendingCourses %>Trending Courses" NavigateUrl="#"></rad:RadTab>
                                    <rad:RadTab Text="<%$ Resources:LocalizedResource, RecommendedCourses %>Recommended Courses" NavigateUrl="#"></rad:RadTab>
                                    <rad:RadTab Text="<%$ Resources:LocalizedResource, MustTakeCourses %>Must Take Courses" NavigateUrl="#"></rad:RadTab>
                                    <rad:RadTab Text="<%$ Resources:LocalizedResource, RecentlyAddedCourses %>Recently Added Courses" NavigateUrl="#"></rad:RadTab>
                                </Tabs>
                            </rad:RadTab>
                            <rad:RadTab runat="server" Text="<%$ Resources:LocalizedResource, UserManagement %>User Management" Value="6" NavigateUrl="Admin.aspx?Tab=UserMgmt" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                            <rad:RadTab runat="server" Text="<%$ Resources:LocalizedResource, Forums %>Forums" Value="7" NavigateUrl="Admin.aspx?Tab=Forums" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                        </Tabs>
                    </rad:RadTabStrip>
                </ul>
            </nav>
            <div class="hamburger-container text-center" style="font-size: 20px; color: #fff;
                margin-top: 10px">
                <a href="#" class="open-nav is-closed animated fadeInLeft"><i class="fa fa-bars"
                    aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolderMain" runat="Server">
    <rad:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true" RenderMode="Lightweight"
        Skin="Bootstrap">
    </rad:RadWindowManager>
    <div class="col-lg-12">
        <div class="row">
            <div class="col-md-12 thirty-px-padding">
                <div class="col-md-12 no-paddings">
                    <ul id="ulBreadcrumb" runat="server" class="breadcrumb">
                        <li><a>
                            <asp:Label ID="lblTab1" runat="server" Text="Admin" /></a></li>
                        <li><a>
                            <asp:Label ID="lblTab2" runat="server" Text="Course Management" /></a></li>
                        <li>
                            <asp:Label ID="Label1" runat="server" Text="Course Preview" /></li>
                    </ul>
                </div>
                <div class="col-md-12 no-paddings">
                    <asp:Panel ID="pnlCoursePreview" runat="server">
                        <div class="container no-padding">
                            <div id="imagePreviewContainer" runat="server" class="col-sm-4 image-full" style="position: relative;">
                                <div class="layer">
                                </div>
                                <div class="Course-Photo-Holder-Inside text-center">
                                    <p>
                                        <asp:Label ID="lblCourseTitle" runat="server"></asp:Label></p>
                                </div>
                            </div>
                        </div>
                        <div class="row no-paddings">
                            <div class="col-sm-4" style="width: 350px; margin: 0 40px;">
                                <div class="text-center">
                                   <asp:Button ID="btnBackToCB" runat="server" Text="Back to Course Builder"
                                        ToolTip="Back to Course Builder" CssClass="btn btn-md btn-teal btn-flat" Width="200px" OnClick="btnBackToCB_Click" />
                                         <%--OnClientClick="back(); return false;"--%>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="panelRadStrip" style="margin-top: 30px;">
                                <rad:RadAjaxPanel ID="raPanelCourseContent" runat="server">
                                    <rad:RadTabStrip ID="RadTabStrip2" runat="server" RenderMode="Lightweight" SelectedIndex="0"
                                        MultiPageID="RadMultiPage1" AutoPostBack="True" EnableEmbeddedSkins="false" Skin="">
                                        <Tabs>
                                            <rad:RadTab runat="server" Text="COURSE CONTENT" Value="1" CssClass="fakeborder"
                                                SelectedCssClass="selectedTab" ForeColor="#636363">
                                            </rad:RadTab>
                                        </Tabs>
                                    </rad:RadTabStrip>
                                    <rad:RadMultiPage ID="RadMultiPage1" runat="server" RenderMode="Lightweight" SelectedIndex="0"
                                        CssClass="outerMultiPage">
                                        <rad:RadPageView ID="RadPageView1" runat="server">
                                            <br />
                                            <ucUC:UploadContent ID="PreviewScormCourse" ClientIDMode="Static" runat="server"
                                                Visible="false" />
                                            <asp:Panel ID="pnlCoursePreviewListView" runat="server">
                                                <div class="col-md-14" style="margin-left: 1%; margin-right: 1%">
                                                    <div class="col-md-12">
                                                        <asp:Panel ID="pnlCoursePreviewLv" runat="server">
                                                            <div id="divCoursePreviewLv" class="demo-container col-md-14">
                                                                <rad:RadListView ID="lvCoursePreview" runat="server" ItemPlaceholderID="ListViewContainer1"
                                                                    AllowPaging="true">
                                                                    <ItemTemplate>
                                                                        <asp:Panel ID="pnlCoursePreview" runat="server" CssClass="container">
                                                                            <div class="row panel-gray panel-course-category">
                                                                                <div class="col-xs-12">
                                                                                    <i id="I1" runat="server" class="fa fa-file pull-left" visible='<%# isFile(Eval("ScoTitle").ToString()) %>'>
                                                                                    </i><i id="I2" runat="server" class="fa fa-file-pdf-o pull-left" visible='<%# isPdf(Eval("ScoTitle").ToString()) %>'>
                                                                                    </i><i id="I3" runat="server" class="fa fa-file-word-o pull-left" visible='<%# isWord(Eval("ScoTitle").ToString()) %>'>
                                                                                    </i><i id="I4" runat="server" class="fa fa-file-excel-o pull-left" visible='<%# isExcel(Eval("ScoTitle").ToString()) %>'>
                                                                                    </i><i id="I5" runat="server" class="fa fa-file-powerpoint-o pull-left" visible='<%# isPpt(Eval("ScoTitle").ToString()) %>'>
                                                                                    </i><i id="I6" runat="server" class="fa fa-file-image-o pull-left" visible='<%# isImg(Eval("ScoTitle").ToString()) %>'>
                                                                                    </i><i id="I7" runat="server" class="fa fa-file-archive-o pull-left" visible='<%# isZip(Eval("ScoTitle").ToString()) %>'>
                                                                                    </i><i id="I8" runat="server" class="fa fa-file-audio-o pull-left" visible='<%# isMp3(Eval("ScoTitle").ToString()) %>'>
                                                                                    </i><i id="I9" runat="server" class="fa fa-file-video-o pull-left" visible='<%# isMp4(Eval("ScoTitle").ToString()) %>'>
                                                                                    </i>
                                                                                    <asp:Label ID="lblTitle" runat="server" CssClass="course-title">
                                                                                    <%#Eval("ScoTitle")%>
                                                                                    </asp:Label>
                                                                                </div>
                                                                            </div>
                                                                        </asp:Panel>
                                                                    </ItemTemplate>
                                                                    <LayoutTemplate>
                                                                        <asp:PlaceHolder ID="ListViewContainer1" runat="server" />
                                                                    </LayoutTemplate>
                                                                </rad:RadListView>
                                                                <br />
                                                            </div>
                                                            <br />
                                                            <br />
                                                            <br />
                                                        </asp:Panel>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </rad:RadPageView>
                                    </rad:RadMultiPage>
                                </rad:RadAjaxPanel>
                            </div>
                        </div>
                    </asp:Panel>
                <%--    <div class="row col-md-12 no-paddings">
                        <div id="imagePreviewContainer" runat="server" class="col-sm-offset-0 col-sm-4" style="position: relative;">
                            <div class="layer">
                            </div>
                            <div class="Course-Photo-Holder-Inside text-center">
                                <p>
                                    <asp:Label ID="lblCourseTitle" runat="server"></asp:Label></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                            </div>
                            <div class="col-sm-4" style="margin-left: 2%;">
                                <div class="text-center">
                                    <asp:Button ID="btnBackToCB" runat="server" Text="Back to Course Builder" OnClick="btnBackToCB_Click"
                                        ToolTip="Back to Course Builder" CssClass="btn btn-md btn-teal btn-flat" Width="200px" />
                                </div>
                            </div>
                            <div class="col-sm-8">
                            </div>
                        </div>
                    </div>--%>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
