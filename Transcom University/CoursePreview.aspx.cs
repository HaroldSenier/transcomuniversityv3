﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Security.Cryptography;
using System.IO;
using System.Text;
using TranscomUniversityV3Model;

public partial class CoursePreview : System.Web.UI.Page
{
    private DataHelper _db;
    private static SqlConnection _oconn;
    private static SqlCommand _ocmd;

    public DataTable DtOverAllBundle
    {
        get
        {
            return ViewState["DtOverAllBundle"] as DataTable;
        }
        set
        {
            ViewState["DtOverAllBundle"] = value;
        }
    }

    private static void Dbconn(string connStr)
    {
        _oconn = new SqlConnection
        {
            ConnectionString = ConfigurationManager.ConnectionStrings[connStr].ConnectionString
        };
        _oconn.Open();
    }

    protected string Encrypt(string clearText)
    {
        string EncryptionKey = "TRNSCMV32017111";
        byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);

        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(clearBytes, 0, clearBytes.Length);
                    cs.Close();
                }
                clearText = Convert.ToBase64String(ms.ToArray());
            }
        }
        return clearText;
    }

    protected string Decrypt(string cipherText)
    {
        string EncryptionKey = "TRNSCMV32017111";
        cipherText = cipherText.Replace(" ", "+");
        byte[] cipherBytes = Convert.FromBase64String(cipherText);

        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(cipherBytes, 0, cipherBytes.Length);
                    cs.Close();
                }
                cipherText = Encoding.Unicode.GetString(ms.ToArray());
            }
        }
        return cipherText;
    }

    public void CreateOverAllPackageDataTableColumns()
    {
        DtOverAllBundle = new DataTable();
        var column = new DataColumn
        {
            DataType = Type.GetType("System.Int32"),
            ColumnName = "ScoId",
            AutoIncrement = true,
            AutoIncrementSeed = 1,
            AutoIncrementStep = 1
        };
        DtOverAllBundle.Columns.Add(column);
        DtOverAllBundle.Columns.Add("ScoPackageId", typeof(int));
        DtOverAllBundle.Columns.Add("ScoTitle", typeof(String));
        DtOverAllBundle.Columns.Add("ScoTypeId", typeof(int));
        DtOverAllBundle.Columns.Add("ScoType", typeof(String));
        DtOverAllBundle.Columns.Add("CourseTypeID", typeof(int));
        DtOverAllBundle.Columns.Add("curriculumcourseID", typeof(int));
    }

    private DataSet GetCurriculumPackages(int CurriculumID, int CurriculumCourseID)
    {
        _db = new DataHelper("DefaultConnection");
        var ds = _db.GetCurriculumPackages(CurriculumID, CurriculumCourseID);
        return ds;
    }

    public void GetDBPackages(int CurriculumID, DNSControls_UploadContent PreviewScormCourse)
    {
        Dbconn("DefaultConnection");
        _ocmd = new SqlCommand("pr_Scorm_Cor_GetCurriculumPackages", _oconn)
        {
            CommandType = CommandType.StoredProcedure
        };

        _ocmd.Parameters.AddWithValue("@CurriculumID", CurriculumID);
        _ocmd.Parameters.AddWithValue("@isBundle", 1);
        SqlDataReader oDataReader = _ocmd.ExecuteReader();

        if (oDataReader.Read())
        {
            CreateOverAllPackageDataTableColumns();

            int i = 0;
            do
            {
                i += 1;

                RadGrid gridPackage = (RadGrid)PreviewScormCourse.FindControl("gridPackage");
                var db = new TranscomUniversityV3ModelContainer();
                var dt1 = db.pr_Scorm_Cor_GetCurriculumPackages(CurriculumID, Convert.ToInt32(oDataReader["curriculumcourseID"].ToString()), 0)
                        .AsEnumerable()
                        .Select(p => new
                        {
                            ScoId = p.scoid,
                            ScoPackageId = p.ScoPackageId,
                            ScoTitle = p.ScoTitle,
                            ScoTypeId = p.ScoTypeId,
                            ScoType = p.ScoType,
                            CourseTypeID = p.CourseTypeID,
                            curriculumcourseID = p.curriculumcourseID
                        })
                        .OrderBy(p => p.ScoId)
                        .ToList();

                gridPackage.DataSource = dt1;
                gridPackage.DataBind();

                lvCoursePreview.DataSource = dt1;
            }
            while (oDataReader.Read());
        }

        oDataReader.Close();
        _oconn.Close();
    }

    #region FileExtension

    public static bool isFile(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (!isPdf(filename) && !isWord(filename) && !isExcel(filename) && !isPpt(filename) && !isImg(filename) && !isZip(filename) && !isMp3(filename) && !isMp4(filename))
            return true;
        else
            return false;
    }

    public static bool isPdf(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (ext == "pdf")
            return true;
        else
            return false;
    }

    public static bool isWord(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (ext == "doc" || ext == "docx")
            return true;
        else
            return false;
    }

    public static bool isExcel(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (ext == "xls" || ext == "xlsx")
            return true;
        else
            return false;
    }

    public static bool isPpt(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (ext == "ppt" || ext == "pptx")
            return true;
        else
            return false;
    }

    public static bool isImg(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (ext == "jpg" || ext == "jpeg" || ext == "png")
            return true;
        else
            return false;
    }

    public static bool isZip(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (ext == "rar" || ext == "zip")
            return true;
        else
            return false;
    }

    public static bool isMp3(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (ext == "mp3")
            return true;
        else
            return false;
    }

    public static bool isMp4(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (ext == "mp4")
            return true;
        else
            return false;
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        var courseId = Convert.ToInt32(Decrypt(Request.QueryString["CourseID"]));

        GetDBPackages(courseId, PreviewScormCourse);

        try
        {
            var db = new TranscomUniversityV3ModelContainer();
            var courseImage = (from c in db.tbl_TranscomUniversity_Cor_Course
                               where c.CourseID == courseId
                               select c.CourseImage).FirstOrDefault().ToString();

            var courseTitle = (from c in db.tbl_TranscomUniversity_Cor_Course
                               where c.CourseID == courseId
                               select c.Title).FirstOrDefault().ToString();

            lblCourseTitle.Text = courseTitle;

            if (courseImage != "No_image.jpg")
                imagePreviewContainer.Attributes["style"] = "background-image: url(" + string.Format("Media/Uploads/CourseImg/{0}/{1}", courseId, courseImage) + "); margin:40px; height: 250px; width: 350px;";
            else
                imagePreviewContainer.Attributes["style"] = "background-image: url(" + "Media/Uploads/CourseImg/No_image.jpg" + ");  margin:40px; height: 250px; width: 350px;";
        }
        catch
        {
            RadWindowManager1.RadAlert("There is an error on the page. Please contact your System Administrator.", 330, 180, "Error Message", "");
        }
    }

    protected void btnBackToCB_Click(object sender, EventArgs e)
    {
        var courseId = Convert.ToInt32(Decrypt(Request.QueryString["CourseID"]));
        var url = string.Format("CourseBuilder.aspx?CourseID={0}", Encrypt(courseId.ToString()));

        Response.Redirect(url);
    }
}