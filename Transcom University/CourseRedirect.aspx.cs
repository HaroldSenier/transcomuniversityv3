﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TranscomUniversityV3Model;

public partial class CourseRedirect : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            string CourseRedirect = Request.QueryString["CourseID"];
            /////////transferred Course Launch
            int CourseID = Convert.ToInt16(Utils.Decrypt(CourseRedirect));
            if (DataHelper.isLoggedIn())
            {
                int enrolleeCIM = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
                var db = new TranscomUniversityV3ModelContainer();
                
                //if (DataHelper.isEnrolled(enrolleeCIM, CourseID) != true && DataHelper.isCourseOpen(CourseID) == true) 
                if (DataHelper.isCourseOpen(CourseID) == true)
                {
                    DataHelper.selfEnrollment(enrolleeCIM, CourseID);
                }
                db.pr_TranscomUniversity_CourseViewLogin(enrolleeCIM, CourseID);
            }

            Response.Redirect("CourseLauncher.aspx?CourseID=" + CourseRedirect);
        }
    }
}