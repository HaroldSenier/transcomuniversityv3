﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TranscomUniversityV3.Master" AutoEventWireup="true"
    CodeFile="CreateCourse.aspx.cs" Inherits="CreateCourse" %>

<%@ Register TagPrefix="ucas" TagName="AdminSidebar" Src="~/UserControl/Statics/AdminSiderbarUserCtrl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="contentPlaceHolderLeftPanel" runat="Server">
    <ucas:AdminSidebar ID="AdminSidebar" runat="server" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolderMain" runat="Server">
    <div class="col-sm-11 col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12 thirty-px-padding">
                        <div class="col-md-12 no-paddings">
                            <ul id="ulBreadcrumb" runat="server" class="breadcrumb">
                                <li><a>
                                    <asp:Label ID="lblTab1" runat="server" Text="Admin Dashboard" /></a></li>
                                <li><a>
                                    <asp:Label ID="lblTab2" runat="server" Text="Create Course" /></a></li>
                                <li id="item3" runat="server" visible="false"><a>
                                    <asp:Label ID="lblTab3" runat="server" Text="" ClientIDMode="Static" /></a></li>
                                <li id="item4" runat="server" visible="false"><a>
                                    <asp:Label ID="lblTab4" runat="server" Text="" /></a></li>
                            </ul>
                        </div>
                        <div class="container createCourseContainer display-none" id="createCourseLayer"
                            style="height: 90vh;">
                        </div>
                        <div class="container createCourseContainer" id="createCourseContainer">
                            <form id="frmAddCourse">
                            <div class="row" style="padding-bottom: 20px;">
                                <div class="col-md-2">
                                    <div class="font-bold">
                                        COURSE DETAILS
                                        <asp:Label ID="lblInvalidError" Visible="false" Font-Italic="true" Text="Please enter all required fields (*)."
                                            ForeColor="Red" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-sm-12 create-course-col-1">
                                    <div class="row">
                                        <div class="col-md-3">
                                            Course Type:
                                        </div>
                                        <div class="col-md-6 pull-left form-group">
                                            <rad:RadComboBox ID="rcbCourseType" runat="server" DefaultValue="0" RenderMode="Lightweight"
                                                Height="80px" EmptyMessage="- Select -" ToolTip="Select Course Type" CssClass="width-fill"
                                                ValidationGroup="addCourse">
                                            </rad:RadComboBox>
                                        </div>
                                        <div class="col-md-1">
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                                                ControlToValidate="rcbCourseType" SetFocusOnError="True" Display="Dynamic" ValidationGroup="addCourse"
                                                ForeColor="Red" CssClass="pull-left displayerror" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            Course Title:
                                        </div>
                                        <div class="col-md-6 pull-left form-group">
                                            <asp:TextBox ID="txtCourseTitle" runat="server" CssClass="form-control" ToolTip="Enter Course Title"
                                                ViewStateMode="Enabled"></asp:TextBox>
                                        </div>
                                        <div class="col-md-1">
                                            <asp:RequiredFieldValidator ID="rfvTxtCourseTitle" runat="server" ErrorMessage="*"
                                                EnableClientScript="true" ControlToValidate="txtCourseTitle" SetFocusOnError="True"
                                                Display="Dynamic" ValidationGroup="addCourse" ForeColor="Red" CssClass="pull-left displayerror" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            Category:
                                        </div>
                                        <div class="col-md-6 pull-left form-group">
                                            <rad:RadComboBox ID="rcbCategory" runat="server" DefaultValue="0" AutoPostBack="true"
                                                RenderMode="Lightweight" Height="250px" EmptyMessage="- Select -" ToolTip="Select Course Category"
                                                CssClass="width-fill" OnSelectedIndexChanged="rcbCategory_SelectedIndexChanged">
                                            </rad:RadComboBox>
                                        </div>
                                        <div class="col-md-1">
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                                                ControlToValidate="rcbCategory" SetFocusOnError="True" Display="Dynamic" ValidationGroup="addCourse"
                                                ForeColor="Red" CssClass="pull-left displayerror" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            Sub-category:
                                        </div>
                                        <div class="col-md-6 pull-left form-group">
                                            <rad:RadDropDownTree ID="ddtSubcategory" runat="server" DataTextField="Subcategory"
                                                DataValueField="SubcategoryId" DataFieldID="SubcategoryId" DataFieldParentID="ParentSubcategoryId"
                                                TextMode="Default" RenderMode="Lightweight" ViewStateMode="Enabled" ToolTip="Select Course Sub-category"
                                                CssClass="width-fill" Enabled="false" DefaultMessage="- Select -">
                                                <DropDownSettings CloseDropDownOnSelection="true" />
                                            </rad:RadDropDownTree>
                                        </div>
                                        <div class="col-md-1">
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*"
                                                ControlToValidate="ddtSubcategory" SetFocusOnError="True" Display="Dynamic" CssClass="displayerror pull-left"
                                                ValidationGroup="addCourse" ForeColor="Red" />
                                        </div>
                                    </div>
                                    <asp:Panel ID="pnlLevelContainer" runat="server" CssClass="row" Visible="false">
                                        <div class="col-md-3">
                                            Level:
                                        </div>
                                        <div class="col-md-6 pull-left form-group">
                                            <rad:RadComboBox RenderMode="Lightweight" ID="rcbLevel" runat="server" CheckBoxes="true"
                                                CssClass="width-fill" DefaultMessage="- Select Level -" ToolTip="Select Job Level that can take the course">
                                                <Items>
                                                    <rad:RadComboBoxItem Text="1" Value="1" />
                                                    <rad:RadComboBoxItem Text="2" Value="2" />
                                                    <rad:RadComboBoxItem Text="3" Value="3" />
                                                    <rad:RadComboBoxItem Text="4" Value="4" />
                                                    <rad:RadComboBoxItem Text="5" Value="5" />
                                                </Items>
                                            </rad:RadComboBox>
                                        </div>
                                        <div class="col-md-1">
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                                                ControlToValidate="rcbLevel" SetFocusOnError="True" Display="Dynamic" CssClass="displayerror pull-left"
                                                ValidationGroup="addCourse" ForeColor="Red" />
                                        </div>
                                    </asp:Panel>
                                    <div class="row">
                                        <div class="col-md-3">
                                            Course ID:
                                        </div>
                                        <div class="col-md-6 pull-left form-group">
                                            <asp:TextBox ID="txtCourseID" runat="server" CssClass="form-control" MaxLength="11"
                                                ReadOnly="true" ToolTip="Course ID" Text="- Auto pop -"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 form-group">
                                            Description:
                                            <div class="row" style="margin-top: 5px;">
                                                <div class="col-md-9">
                                                    <asp:Label ID="lblDescription" runat="server"></asp:Label>
                                                    <asp:TextBox ID="txtDescription" runat="server" CssClass="form-control resize-none rounded-corner"
                                                        MaxLength="11" TextMode="MultiLine" Rows="4" ToolTip="Enter Course Description"
                                                        placeholder="What is this course about?"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            Date Created:
                                        </div>
                                        <div class="col-md-6 pull-left form-group">
                                            <asp:TextBox ID="txtDateCreated" runat="server" CssClass="form-control" MaxLength="11"
                                                ReadOnly="true" ValidationGroup="addCourse" ToolTip="Date Created" Text="- Auto pop -"></asp:TextBox>
                                            <asp:Label ID="lblDateCreated" Visible="false" Text="*" ForeColor="Red" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            Start Date/Time:
                                        </div>
                                        <div class="col-md-6 pull-left form-group">
                                            <rad:RadDateTimePicker ID="dpStartDateTime" runat="server" RenderMode="Lightweight"
                                                DateInput-ValidationGroup="addCourse" ToolTip="Set Start Date" CssClass="left-calendar-icon width-fill">
                                                <TimeView ID="TimeView1" runat="server" StartTime="00:00" EndTime="23:59" OnClientTimeSelecting="timeSelecting" />
                                                <ClientEvents OnDateSelected="startDateSelected" />
                                                <DateInput ID="DateInput1" runat="server">
                                                </DateInput>
                                                <Calendar ID="Calendar2" runat="server">
                                                    <SpecialDays>
                                                        <rad:RadCalendarDay Repeatable="Today" ItemStyle-BackColor="Gray">
                                                        </rad:RadCalendarDay>
                                                    </SpecialDays>
                                                </Calendar>
                                            </rad:RadDateTimePicker>
                                        </div>
                                        <div class="col-md-1">
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                                ForeColor="Red" ControlToValidate="dpStartDateTime" SetFocusOnError="True" Display="Dynamic"
                                                CssClass="displayerror pull-left" ValidationGroup="addCourse" Text="*" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            End Date/Time:
                                        </div>
                                        <div class="col-md-6 pull-left form-group">
                                            <rad:RadDateTimePicker ID="dpEndDateTime" runat="server" RenderMode="Lightweight"
                                                DateInput-ValidationGroup="addCourse, dateValid" ToolTip="Set End Date Time"
                                                CssClass="left-calendar-icon width-fill">
                                                <TimeView ID="TimeView2" runat="server" StartTime="00:00" EndTime="23:59" OnClientTimeSelecting="timeSelecting" />
                                                <ClientEvents OnDateSelected="endDateSelected" />
                                                <DateInput ID="DateInput2" runat="server">
                                                </DateInput>
                                                <Calendar ID="Calendar1" runat="server">
                                                    <SpecialDays>
                                                        <rad:RadCalendarDay Repeatable="Today" ItemStyle-BackColor="Gray">
                                                        </rad:RadCalendarDay>
                                                    </SpecialDays>
                                                </Calendar>
                                            </rad:RadDateTimePicker>
                                            <asp:CustomValidator ID="cvDate" runat="server" ErrorMessage="End Date should be greater than Start Date"
                                                SetFocusOnError="true" Display="Dynamic" ForeColor="Red" ControlToValidate="dpEndDateTime"
                                                ClientValidationFunction="validateStartEndDate" ValidationGroup="dateValid"></asp:CustomValidator>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            Duration:
                                        </div>
                                        <div class="col-md-2 pull-left form-group" style="display: inline;">
                                            <%-- <asp:DropDownList ID="ddlDuration" runat="server" CssClass="form-control width-fill"
                                                ToolTip="Set Course Duration" Width="100px">
                                            </asp:DropDownList>--%>
                                            <rad:RadNumericTextBox ID="txtDuration" runat="server" RenderMode="Lightweight" Skin="Bootstrap"
                                                MaxLength="3" Value="0" CssClass="btn-flat" Width="100px" Height="25px">
                                                <NumberFormat GroupSeparator="" DecimalDigits="0" />
                                            </rad:RadNumericTextBox>
                                        </div>
                                        <div class="col-md-4 pull-left form-group" style="margin-left: 0; padding-left: 0;
                                            padding-top: 5px;">
                                            minutes
                                        </div>
                                    </div>
                                </div>
                                <%--Col 1 End--%>
                                <div class="col-lg-6 col-sm-12 create-course-col-2">
                                    <div class="row">
                                        <div class="col-md-3">
                                            Author:
                                        </div>
                                        <div class="col-md-6 pull-left form-group">
                                            <asp:TextBox ID="txtAuthor" runat="server" CssClass="form-control" ToolTip="Author Name"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            Department Owner:
                                        </div>
                                        <div class="col-md-6 pull-left form-group">
                                            <asp:Label ID="lblDepartment" runat="server"></asp:Label>
                                            <rad:RadComboBox RenderMode="Lightweight" ID="rcbDepartment" runat="server" DataTextField="Department"
                                                DataValueField="Department" EmptyMessage="Type Name or Select" DataSourceID="dsDepartment"
                                                ToolTip="Select Department" CssClass="width-fill">
                                            </rad:RadComboBox>
                                            <asp:SqlDataSource ID="dsDepartment" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection%>"
                                                SelectCommand="SELECT DepartmentID, Department FROM [tbl_TranscomUniversity_Lkp_Department] WHERE [HideFromList] = 0">
                                            </asp:SqlDataSource>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            Required Program:
                                        </div>
                                        <div class="col-md-6 pull-left form-group">
                                            <asp:Label ID="lblRequiredProgram" runat="server"></asp:Label>
                                            <rad:RadComboBox RenderMode="Lightweight" ID="rcbReqProgram" runat="server" DataTextField="Client"
                                                DataValueField="ClientID" EmptyMessage="-Select program-" DataSourceID="dsProgram"
                                                EnableAutomaticLoadOnDemand="True" ItemsPerRequest="10" EnableVirtualScrolling="true"
                                                ToolTip="Select Program" CssClass="width-fill" OnClientBlur="OnClientBlurHandler"
                                                MarkFirstMatch="true" AllowCustomText="false" EnableItemCaching="true" Height="250px">
                                            </rad:RadComboBox>
                                            <asp:SqlDataSource ID="dsProgram" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection%>"
                                                SelectCommand="select * from tbl_TranscomUniversity_Lkp_Client where HideFromList=0">
                                            </asp:SqlDataSource>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            Required Course:
                                        </div>
                                        <div class="col-md-6 pull-left form-group">
                                            <asp:Label ID="lblReqCourse" runat="server"></asp:Label>
                                            <rad:RadComboBox ID="rcbReqCourse" runat="server" RenderMode="Lightweight" AutoPostBack="true"
                                                EmptyMessage="- Select -" OnSelectedIndexChanged="rcbReqCourse_IndexChanged"
                                                OnClientSelectedIndexChanged="rcbReqCourse_OnClientSelectedIndexChanged" ToolTip="Select Category"
                                                CssClass="width-fill">
                                            </rad:RadComboBox>
                                            <div id="panelSubReqCategory" style="display: none;">
                                                <br />
                                                <rad:RadDropDownTree ID="ddtReqSubcategory" runat="server" DataTextField="Subcategory"
                                                    DataValueField="SubcategoryId" DataFieldID="SubcategoryId" DataFieldParentID="ParentSubcategoryId"
                                                    TextMode="Default" RenderMode="Lightweight" OnClientEntryAdded="reqSubcategoryIndexChange"
                                                    OnClientBlur="OnClientBlurHandler" ToolTip="Select Sub-category" CssClass="width-fill">
                                                    <DropDownSettings CloseDropDownOnSelection="true" />
                                                </rad:RadDropDownTree>
                                            </div>
                                            <div id="panelSelectCourse" style="display: none;">
                                                <br />
                                                <asp:LinkButton ID="btnSelectCourse" runat="server" Text="Select Course" Font-Underline="true"
                                                    OnClick="btnSelectCourse_Click" ToolTip="Select Required Course/s"></asp:LinkButton>
                                            </div>
                                            <div id="panelAssignedCourse" style="display: none;">
                                                <br />
                                                <rad:RadListBox ID="lbAssignedCourses" runat="server" Height="100px" Width="100%"
                                                    RenderMode="Lightweight" EmptyMessage="No Selected Courses" AllowDelete="true"
                                                    ToolTip="Select Required Course/s">
                                                    <ButtonSettings ShowDelete="true" ShowReorder="false" HorizontalAlign="Right" VerticalAlign="Top" />
                                                </rad:RadListBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-5">
                                            Required # of years in tenure:
                                        </div>
                                        <div class="col-md-4 pull-left form-group">
                                            <asp:DropDownList ID="ddlTenureYears" runat="server" CssClass="form-control width-fill"
                                                ToolTip="Select Years of Tenure" Style="margin-left: -25%;">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-5">
                                            Required # of years in role:
                                        </div>
                                        <div class="col-md-4 pull-left form-group">
                                            <asp:DropDownList ID="ddlRoleYears" runat="server" CssClass="form-control width-fill"
                                                ToolTip="Select Years of Role" Style="margin-left: -25%;">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            Access Type:
                                        </div>
                                        <div class="col-md-6 pull-left form-group">
                                            <asp:DropDownList ID="ddlAccessType" runat="server" CssClass="form-control" ToolTip="Set Access Type">
                                                <asp:ListItem Value="Internal">Internal</asp:ListItem>
                                                <asp:ListItem Value="External">External</asp:ListItem>
                                                <asp:ListItem Value="Both">Both</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:Label ID="lblAccessMode" Visible="false" Text="*" ForeColor="Red" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            Course Image:
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="row img-dropzone-md">
                                                <div class="dropzone-empty" style="padding-top: 2em;">
                                                    <asp:Image ID="Image4" ImageUrl="~/Media/Images/icon-upload.png" runat="server" AlternateText="Upload Image"
                                                        Height="2.875em" Widt h="2.875em" />
                                                    <br />
                                                    <p>
                                                        Drag image file here
                                                    </p>
                                                </div>
                                                <rad:RadAsyncUpload ID="CourseImage" runat="server" MultipleFileSelection="Disabled"
                                                    ViewStateMode="Enabled" MaxFileInputsCount="1" ControlObjectsVisibility="None"
                                                    Width="100%" HideFileInput="false" ToolTip="Select Course Image" CssClass="CourseImage display-none"
                                                    DropZones=".img-dropzone-md" ForeColor="#ddd" OnClientFilesUploaded="fileUploaded"
                                                    OnClientFileSelected="fileSelected" OnFileUploaded="upload_FileUploaded" AllowedFileExtensions="jpeg,jpg,gif,png">
                                                    <%-- <FileFilters>
                                                                <rad:FileFilter Description="Images(jpeg;jpg;gif;png)" Extensions="jpeg,jpg,gif,png" />
                                                            </FileFilters>--%>
                                                </rad:RadAsyncUpload>
                                                <div id="DropzonePreview" class="dropzone-preview" onmouseover="dropzoneEnter()"
                                                    onmouseout="dropzoneExit()" runat="server">
                                                    <rad:RadBinaryImage ID="courseImagePreview" runat="server" CssClass="box-fluid" ClientIDMode="Static" />
                                                    <div class="box-fluid tc-overlay">
                                                        <asp:LinkButton ID="btnRemovePreviewImg" runat="server" ToolTip="Remove Image" OnClick="btnRemovePreviewImg_Click"
                                                            OnClientClick="showImageDropzonePreviewLoading();">
                                                                    <i class="fa fa-times-circle-o previewClose" style="color: white; position: absolute;
                                                                    top: 50%; left: 50%; transform: translate(-50%,-50%); font-size: 5em; cursor: pointer;">
                                                                    </i>
                                                        </asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row center-all" style="position: relative; transform: translate(0, 20%);
                                                width: 85% !important; padding: 0px; text-align: center; font-weight: normal;
                                                font-style: normal; margin-left: 7.5%;">
                                                or
                                            </div>
                                            <asp:Button ID="Button1" Text="Select File" runat="server" CssClass="btn btn-default border-lightgray btn-sm btn-flat btn-thin relative-hr-center"
                                                OnClientClick="openFileUploader(); return false;" Style="margin-top: 10px;" />
                                        </div>
                                    </div>
                                </div>
                                <%--Col 2 End--%>
                            </div>
                            <div class="row btn-group-center">
                                <button class="btn btn-sm btn-teal pull-left" onclick="cancelCreate(); return false;">
                                    Cancel</button>
                                <button class="btn btn-sm btn-teal pull-right" onclick="lbtnPreview_OnClientClick(); return false;">
                                    Preview</button>
                                <button id="fakelbtnSaveCreateCourse" class="btn btn-sm btn-teal pull-right" onclick="validateCreateCourse(); return false;">
                                    Create Course</button>
                                <asp:Button ID="lbtnSaveCreateCourse" runat="server" ValidationGroup="addCourse, dateValid"
                                    OnClick="btnSaveCreateCourse_Click" CssClass="display-none" />
                            </div>
                            </form>
                        </div>
                        <asp:Panel ID="pnlCoursePreview" runat="server" Style="display: none;">
                            <div class="container no-padding">
                                <div id="imagePreviewContainer" runat="server" class="col-sm-4 image-full" style="position: relative;">
                                    <div class="layer">
                                    </div>
                                    <div class="Course-Photo-Holder-Inside text-center">
                                        <p>
                                            <asp:Label ID="lblCoursePreviewTitle" runat="server"></asp:Label></p>
                                    </div>
                                </div>
                            </div>
                            <div class="row no-paddings">
                                <div class="col-sm-4" style="width: 350px; margin: 0 40px;">
                                    <div class="text-center">
                                        <asp:Button ID="btnBackToCB" runat="server" Text="Back to Create Course" OnClientClick="redirectToCreateCourse(); return false;"
                                            ToolTip="Back to Course Builder" CssClass="btn btn-md btn-teal btn-flat" Width="200px" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                            </div>
                            <div class="row">
                                <div class="panelRadStrip" style="margin-top: 30px;">
                                    <rad:RadAjaxPanel ID="raPanelCourseContent" runat="server">
                                        <rad:RadTabStrip ID="RadTabStrip2" runat="server" RenderMode="Lightweight" SelectedIndex="0"
                                            MultiPageID="RadMultiPage1" AutoPostBack="True" EnableEmbeddedSkins="false" Skin="">
                                            <Tabs>
                                                <rad:RadTab runat="server" Text="COURSE CONTENT" Value="1" CssClass="fakeborder"
                                                    SelectedCssClass="selectedTab" ForeColor="#636363">
                                                </rad:RadTab>
                                            </Tabs>
                                        </rad:RadTabStrip>
                                    </rad:RadAjaxPanel>
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <rad:RadScriptBlock ID="RadScriptBlock" runat="server">
        <script type="text/javascript">
            function pageLoad() {
                $telerik.$('.ruFileInput').attr('accept', 'image/*');

            }
            function fileUploaded(sender, args) {
                //                alert("s");
                var upload = $find("<%= CourseImage.ClientID %>");
                var inputs = upload.getUploadedFiles();

                if (!upload.isExtensionValid(inputs[0]))
                    alert("extension is invalid!");

                var manager = $find("<%=RadAjaxManager1.ClientID %>");
                manager.ajaxRequest();

                setTimeout(function () {
                    hideAllLocalLoading();
                }, 3000);

            }

            function fileSelected(sender, args) {

                showImageDropzonePreviewLoading();
                var fileExtention = args.get_fileName().substring(args.get_fileName().lastIndexOf('.') + 1, args.get_fileName().length);
                if (args.get_fileName().lastIndexOf('.') != -1) {//this checks if the extension is correct
                    if (sender.get_allowedFileExtensions().toLowerCase().indexOf(fileExtention.toLowerCase()) == -1) {
                        $("#" + "<%= courseImagePreview.ClientID%>").attr('src', "").hide();
                        removePreviewImage();
                        radalert("Invalid File Type.<br> Allowed Files are JPG, GIF, PNG.", 330, 180, "Error", "");
                    }
                    else {
                        console.log("File Supported.");
                    }
                }
                else {
                    console.log("not correct extension.");

                }
                //                var manager = $find("<%=RadAjaxManager1.ClientID %>");
                //                manager.ajaxRequest();

            }

            function showImageDropzonePreviewLoading() {
                var panel = $find("<%= fullPageLoading.ClientID %>");
                panel.show("courseImagePreview");
            }

            function hideAllLocalLoading() {
                $(".Loading2").hide();
                console.log("hide loading");
            }

            function dropzoneEnter() {

                if ($("#" + "<%= courseImagePreview.ClientID %>").is('[src]')) {
                    $(".tc-overlay").css({ "display": "block", "opacity": "1" });
                }
            }

            function dropzoneExit() {
                var a = $("#" + "<%= courseImagePreview.ClientID %>").attr('src');
                $(".tc-overlay").css({ "display": "none", "opacity": "0" });

            }

            function removePreviewImage() {
                try {
                    var upload = $find("<%= CourseImage.ClientID %>");
                    upload.deleteAllFileInputs();
                    $(".Loading").hide();
                } catch (e) {
                }
            }

            function openCourseWindow() {
                var radwindow = $find('<%=radCourseWindow.ClientID %>');
                radwindow.show();
            }

            function ClosePrerequisiteForm() {
                var window = $find('<%=radCourseWindow.ClientID %>');
                window.close();
            }

            function redirectToCourseBuilder(courseID) {
                //$("#form1").addClass("display-none");
                //$(".js-redirectMessage").removeClass("display-none");
                //var message = $(".js-redirectMessage").parent().insertAfter($("#form1"));

                window.location.href = "CourseBuilder.aspx?CourseID=" + courseID;
            }

            function validateStartEndDate(sender, args) {
                debugger;
                var startDate = $find("<%= dpStartDateTime.ClientID %>").get_selectedDate();
                var endDate = $find("<%= dpEndDateTime.ClientID %>").get_selectedDate();
                endDate = endDate == null ? new Date(2099, 12, 30, 23, 0, 0) : endDate;
                //                console.log(startDate);
                //                console.log(endDate);
                //                console.log(endDate > startDate);
                var valid = (endDate > startDate);
                args.IsValid = valid;
                if (valid) {
                    var dpEndDateTime = $find("<%= dpEndDateTime.ClientID %>");
                    dpEndDateTime.get_dateInput().focus();
                }

            }

            function validateCreateCourse() {
                radconfirm("Are you sure you want to create this course?", confirmCreateFn, 330, 180, "", "Confirm Create");
            }

            function confirmCreateFn(arg) {
                if (arg) {
                    if (Page_ClientValidate("addCourse") && Page_ClientValidate("dateValid")) {
                        //console.log("validated");
                        $("#<%= lbtnSaveCreateCourse.ClientID %>").click();

                    } else {
                        return false;
                    }
                }
                //console.log(arg);

            }

            function focusThisDate(sender, args) {
                console.log(sender);
                sender.get_dateInput().focus();
            }

            function cancelCreate() {
                radconfirm("Are you sure you want to cancel?", confirmCancelCreateFn, 330, 180, "", "Confirm Cancel");

            }

            function confirmCancelCreateFn(arg) {
                if (arg) {
                    window.location.href = "Admin.aspx";
                }
                return false;
            }

            function hideRequiredCoursePanel() {
                $("#panelSubReqCategory").hide();
                $("#panelSelectCourse").hide();
                $("#panelAssignedCourse").hide();
            }

            function rcbReqCourse_OnClientSelectedIndexChanged() {

                try {
                    var rcbReqCourse = $find("<%= rcbReqCourse.ClientID %>");
                    var value = rcbReqCourse.get_value();
                    console.log(value);
                    var size = rcbReqCourse.get_items().get_count();

                    if (value == "-1") {
                        rcbReqCourse.get_items().clear();
                        hideRequiredCoursePanel();
                    } else if (value > 0) {
                        //&& value < size
                        $("#panelSubReqCategory").show();
                    }
                } catch (e) {
                    alert("Something went wrong. Please reload the page.");
                }
            }


            function showLoading(panelid) {
                var panel = $find("<%= localLoading.ClientID %>");
                panel.show(panelid);
            }

            function AddCourses_OnClientClick() {
                var assignedCourses = $find("<%= lbAssignedCourses.ClientID %>");
                var availableCourses = $find("<%= lbAvailableCourses.ClientID %>");
                var selectedItems = availableCourses.get_checkedItems();
                //console.log(selectedItems);
                selectedItems.forEach(function (item) {
                    availableCourses.transferItem(item, availableCourses, assignedCourses);
                });
                var panel = $find("<%= localLoading.ClientID %>");
                panel.show("Panel2");
                setTimeout(function () {

                    $("#panelAssignedCourse").show();
                    var courses = $find("<%= radCourseWindow.ClientID %>");
                    $(".Loading2").hide();
                    courses.close();
                }, 800);
                return false;
            }

            var courseTitle;
            function lbtnPreview_OnClientClick() {
                courseTitle = $("#<%= txtCourseTitle.ClientID %>").val();
                $("#<%= lblCoursePreviewTitle.ClientID %>").html(courseTitle);
                //window.location.href = "CoursePreview.aspx?Action=CoursePreview";
                var imageUrl = $("#" + "<%= courseImagePreview.ClientID%>").attr('src');
                $("#<%= imagePreviewContainer.ClientID %>").css({ 'background-image': 'url(' + imageUrl + '), url(Media/Uploads/CourseImg/No_image.jpg)' });

                var panel = $find("<%= fullPageLoading.ClientID %>");
                panel.show("createCourseContainer");
                setTimeout(function () {
                    var addBreadcrumb = ('<li id="fakeBreadcrumb"><a><span>Course Preview</span></a></li>');
                    $(".breadcrumb").append(addBreadcrumb);
                    $(".Loading").hide();
                    $("#createCourseContainer").hide();
                    $("#<%= pnlCoursePreview.ClientID %>").show();
                }, 500);

            }

            function redirectToCreateCourse() {
                setTimeout(function () {
                    $("#<%= pnlCoursePreview.ClientID %>").hide();
                    $("#createCourseContainer").show();
                    $("#fakeBreadcrumb").remove();
                }, 500);

            }

            function openFileUploader() {
                $telerik.$(".ruFileInput").click();
            }

            function OnClientBlurHandler(sender, eventArgs) {
                var textInTheCombo = sender.get_text();
                var item = sender.findItemByText(textInTheCombo);
                //if there is no item with that text
                if (!item) {
                    sender.set_text("");
                    setTimeout(function () {
                        var inputElement = sender.get_inputDomElement();
                        inputElement.focus();
                        var empty = sender.get_emptyMessage();
                        sender.set_text(empty);
                        //inputElement.style.backgroundColor = "red";
                    }, 20);
                }
            }

            function reqSubcategoryIndexChange(sender, args) {
                var panel = $("#panelSelectCourse");
                setTimeout(function () {
                    panel.show();
                }, 200);
            }

            function openSuccessModal() {

                $("#SwitchReqYearsInRole").attr("checked", false);
                $("#SwitchReqYearsInTenure").attr("checked", true);
                $("#successModal").modal("toggle");
                var panel1 = $("#panelSubReqCategory");
                panel1.hide();
                var panel1 = $("#panelSelectCourse");
                panel1.hide();
                var panel1 = $("#panelAssignedCourse");
                panel1.hide();
            }

            function hideAllLoading() {
                $(".Loading").hide();
            }

        </script>

        <%--additional for time picker--%>
        <script type="text/javascript">
            // using a variable to prevent infinite loop
            var isTimeSet = false;
            function timeSelecting(sender, args) {
                isTimeSet = true;
            }

            function startDateSelected(sender, args) {
                if (args.get_newDate() && !isTimeSet) {
                    args.set_cancel(true);
                    isTimeSet = true;
                    sender.get_timeView().setTime(00, 0, 0, 0);
                }
                if (isTimeSet) {
                    isTimeSet = false;
                }
            }

            function endDateSelected(sender, args) {
                if (args.get_newDate() && !isTimeSet) {
                    args.set_cancel(true);
                    isTimeSet = true;
                    sender.get_timeView().setTime(23, 0, 0, 0);
                }
                if (isTimeSet) {
                    isTimeSet = false;
                }
            }

            function valueChanging(sender, args) {
                if (args.get_newValue().indexOf(":") > 0) {
                    isTimeSet = true;
                }
            }
</script>
    </rad:RadScriptBlock>
    <rad:RadWindowManager ID="RadWindowManager1" RenderMode="Lightweight" EnableShadow="true"
        Skin="Bootstrap" Modal="true" VisibleOnPageLoad="false" Behaviors="Close, Move"
        DestroyOnClose="true" RestrictionZoneID="RestrictionZone" Opacity="99" runat="server"
        Width="800" Height="400px">
        <Windows>
            <rad:RadWindow ReloadOnShow="true" RenderMode="Lightweight" ID="radCourseWindow"
                VisibleOnPageLoad="false" Title="Select Course" runat="server" CssClass="top-9999"
                VisibleStatusbar="false">
                <ContentTemplate>
                    <asp:Panel ID="Panel2" runat="server" ClientIDMode="Static">
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="selectCourseContainer top-9999">
                                            <rad:RadListBox ID="lbAvailableCourses" runat="server" Height="275px" Width="100%"
                                                EmptyMessage="No Available Courses" SelectionMode="Multiple" AllowTransfer="true"
                                                TransferToID="lbAssignedCourses" TransferMode="Move" CheckBoxes="true">
                                                <ButtonSettings ShowTransfer="false" ShowTransferAll="false" />
                                            </rad:RadListBox>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td style="padding-top: 10px;">
                                        <asp:LinkButton ID="AddCourses" runat="server" Text="Save" CssClass="btn btn-teal btn-md pull-right"
                                            OnClientClick="AddCourses_OnClientClick(); return false;"> </asp:LinkButton>
                                        <%-- OnClick="btnAddCourses_Click" --%>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </asp:Panel>
                </ContentTemplate>
            </rad:RadWindow>
        </Windows>
    </rad:RadWindowManager>
    <rad:RadAjaxManager ID="RadAjaxManager1" runat="server" DefaultLoadingPanelID="fullPageLoading">
        <AjaxSettings>
            <rad:AjaxSetting AjaxControlID="btnCourseMngt">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" UpdatePanelRenderMode="Inline" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="rcbCategory">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="ddtSubcategory" LoadingPanelID="localLoading" />
                    <rad:AjaxUpdatedControl ControlID="pnlLevelContainer" LoadingPanelID="noLoadingPanel" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <%-- <rad:AjaxSetting AjaxControlID="ddtSubcategory">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="ddtSubcategory" LoadingPanelID="localLoading" />
                </UpdatedControls>
            </rad:AjaxSetting>--%>
            <rad:AjaxSetting AjaxControlID="rcbReqCourse">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="ddtReqSubcategory" UpdatePanelRenderMode="Inline"
                        LoadingPanelID="localLoading" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <%--  <rad:AjaxSetting AjaxControlID="ddtReqSubcategory">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="btnSelectCourse" UpdatePanelRenderMode="Inline" />
                </UpdatedControls>
            </rad:AjaxSetting>--%>
            <rad:AjaxSetting AjaxControlID="lbtnSaveCreateCourse">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="lbtnSaveCreateCourse" UpdatePanelRenderMode="Inline" />
                    <rad:AjaxUpdatedControl ControlID="frmAddCourse" UpdatePanelRenderMode="Inline" />
                    <rad:AjaxUpdatedControl ControlID="lblStartDate" UpdatePanelRenderMode="Inline" />
                    <rad:AjaxUpdatedControl ControlID="txtCourseTitle" UpdatePanelRenderMode="Inline" />
                    <rad:AjaxUpdatedControl ControlID="txtCourseID" UpdatePanelRenderMode="Inline" />
                    <rad:AjaxUpdatedControl ControlID="rcbCategory" UpdatePanelRenderMode="Inline" />
                    <rad:AjaxUpdatedControl ControlID="ddtSubcategory" UpdatePanelRenderMode="Inline" />
                    <rad:AjaxUpdatedControl ControlID="txtDescription" UpdatePanelRenderMode="Inline" />
                    <rad:AjaxUpdatedControl ControlID="txtDateCreated" UpdatePanelRenderMode="Inline" />
                    <rad:AjaxUpdatedControl ControlID="dpStartDateTime" UpdatePanelRenderMode="Inline" />
                    <rad:AjaxUpdatedControl ControlID="dpEndDateTime" UpdatePanelRenderMode="Inline" />
                    <rad:AjaxUpdatedControl ControlID="ddlDuration" UpdatePanelRenderMode="Inline" />
                    <rad:AjaxUpdatedControl ControlID="txtAuthor" UpdatePanelRenderMode="Inline" />
                    <rad:AjaxUpdatedControl ControlID="rcbDepartment" UpdatePanelRenderMode="Inline" />
                    <rad:AjaxUpdatedControl ControlID="rcbReqProgram" UpdatePanelRenderMode="Inline" />
                    <rad:AjaxUpdatedControl ControlID="rcbReqCourse" UpdatePanelRenderMode="Inline" />
                    <rad:AjaxUpdatedControl ControlID="ddtReqSubcategory" UpdatePanelRenderMode="Inline" />
                    <rad:AjaxUpdatedControl ControlID="lbAvailableCourses" UpdatePanelRenderMode="Inline" />
                    <rad:AjaxUpdatedControl ControlID="lbAssignedCourses" UpdatePanelRenderMode="Inline" />
                    <rad:AjaxUpdatedControl ControlID="txtReqYearsInRole" UpdatePanelRenderMode="Inline" />
                    <rad:AjaxUpdatedControl ControlID="ddlAccessType" UpdatePanelRenderMode="Inline" />
                    <rad:AjaxUpdatedControl ControlID="ddlTenureYears" UpdatePanelRenderMode="Inline" />
                    <rad:AjaxUpdatedControl ControlID="CourseImage" UpdatePanelRenderMode="Inline" />
                    <rad:AjaxUpdatedControl ControlID="rcbCourseType" UpdatePanelRenderMode="Inline" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnSelectCourse">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="btnSelectCourse" UpdatePanelRenderMode="Inline" />
                    <rad:AjaxUpdatedControl ControlID="lbAvailableCourses" UpdatePanelRenderMode="Inline" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <%--  <rad:AjaxSetting AjaxControlID="AddCourses">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="lbAssignedCourses" UpdatePanelRenderMode="Inline" />
                    <rad:AjaxUpdatedControl ControlID="lbAvailableCourses" UpdatePanelRenderMode="Inline" />
                </UpdatedControls>
            </rad:AjaxSetting>--%>
            <rad:AjaxSetting AjaxControlID="gridMandatoryCourses">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="gridMandatoryCourses" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="gridTrendingCourses">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="gridTrendingCourses" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="gridRecentlyAddedCourses">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="gridRecentlyAddedCourses" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnSwitchCatListView">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                    <rad:AjaxUpdatedControl ControlID="ulBreadcrumb" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnSwitchCatGridView">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                    <rad:AjaxUpdatedControl ControlID="ulBreadcrumb" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="rdpCourseCategory">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="gvCourseCategory">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="gvCourseCategory" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="rcmCourseCategory">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnAddCategory">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlAddCategory" LoadingPanelID="localLoading" />
                    <rad:AjaxUpdatedControl ControlID="lvCourseCategory" />
                    <rad:AjaxUpdatedControl ControlID="gvCourseCategory" />
                    <rad:AjaxUpdatedControl ControlID="ltCourseCategory" UpdatePanelRenderMode="Inline" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnCatDelete">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="gvDeleteCategory">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlDelCategory" LoadingPanelID="localLoading" />
                    <rad:AjaxUpdatedControl ControlID="lvCourseCategory" />
                    <rad:AjaxUpdatedControl ControlID="gvCourseCategory" />
                    <rad:AjaxUpdatedControl ControlID="ltCourseCategory" UpdatePanelRenderMode="Inline" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="gvClassNewHire">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="gvClassNewHire" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="gvClassCrossTraining">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="gvClassCrossTraining" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="gvClassRefresherTraining">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="gvClassRefresherTraining" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="gvClassUpTraining">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="gvClassUpTraining" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="gvClassProfessionalDevelopment">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="gvClassProfessionalDevelopment" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnViewAllNH">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnViewAllCT">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnViewAllRT">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnViewAllUT">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnViewAllPD">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnViewLessNH">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnViewLessCT">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnViewLessRT">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnViewLessUT">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnViewLessPD">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="ddlTrainingType">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="ddlStatus">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <%--  <rad:AjaxSetting AjaxControlID="CourseImage">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="CourseImage" />
                    <rad:AjaxUpdatedControl ControlID="DropzonePreview" LoadingPanelID="localLoading" />
                </UpdatedControls>
            </rad:AjaxSetting>--%>
            <rad:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="CourseImage" LoadingPanelID="noLoadingPanel" />
                    <rad:AjaxUpdatedControl ControlID="courseImagePreview" LoadingPanelID="fullPageLoading" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnRemovePreviewImg">
                <UpdatedControls>
                    <%--<rad:AjaxUpdatedControl ControlID="CourseImage" LoadingPanelID="noLoadingPanel" />--%>
                    <rad:AjaxUpdatedControl ControlID="courseImagePreview" LoadingPanelID="noLoadingPanel" />
                </UpdatedControls>
            </rad:AjaxSetting>
        </AjaxSettings>
    </rad:RadAjaxManager>
    <rad:RadAjaxLoadingPanel ID="fullPageLoading" runat="server" Transparency="25" IsSticky="true"
        CssClass="Loading" />
    <rad:RadAjaxLoadingPanel ID="localLoading" runat="server" MinDisplayTime="1000" CssClass="Loading2"
        Transparency="25" />
    <rad:RadAjaxLoadingPanel ID="noLoadingPanel" runat="server" CssClass="Loading-Empty"
        Transparency="25" />
</asp:Content>
