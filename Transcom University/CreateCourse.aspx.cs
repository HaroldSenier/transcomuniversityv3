﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Telerik.Web.UI;
using System.Configuration;
using System.Data.SqlClient;

public partial class CreateCourse : BasePage
{
    private int newCourseID;

    private DateTime serverDate;

    private static string global_courseImage;

    protected void Page_Load(object sender, EventArgs e)
    {
        newCourseID = DataHelper.lastInsertedCourseID() + 1;
        if (!Page.IsPostBack)
        {
            clearCourseInput();

            if (!System.Web.Security.Roles.IsUserInRole(HttpContext.Current.User.Identity.Name.Split('|')[0], "Admin"))
                Response.Redirect("~/Unauthorized.aspx");

            //populate tenure years
            for (int i = 0; i <= 20; i++)
            {
                ListItem item = new ListItem();
                item.Value = i.ToString();
                item.Text = i.ToString();
                ddlTenureYears.Items.Add(item);
                ddlRoleYears.Items.Add(item);
            }

            ////populate duration dropdown
            //for (int i = 0; i <= 60; i++)
            //{
            //    ListItem item = new ListItem();
            //    item.Value = i.ToString();
            //    item.Text = i.ToString();
            //    ddlDuration.Items.Add(item);

            //}

            //Load Course Type

            rcbCourseType.DataTextField = "CourseType";
            rcbCourseType.DataValueField = "CourseTypeID";
            rcbCourseType.DataSource = DataHelper.GetCourseType();
            rcbCourseType.DataBind();
            //Load Categories
            rcbCategory.DataTextField = "Category";
            rcbCategory.DataValueField = "CategoryID";
            rcbCategory.DataSource = DataHelper.GetAllCategories();
            rcbCategory.DataBind();

            rcbReqCourse.DataTextField = "Category";
            rcbReqCourse.DataValueField = "CategoryID";
            rcbReqCourse.AppendDataBoundItems = true;
            rcbReqCourse.DataSource = DataHelper.GetAllCategories();
            rcbReqCourse.Items.Insert(0, new RadComboBoxItem("- Select -", "-1"));
            rcbReqCourse.DataBind();

            //rcbAuthor.Filter = RadComboBoxFilter.StartsWith;
            rcbDepartment.Filter = RadComboBoxFilter.StartsWith;
            rcbReqProgram.Filter = RadComboBoxFilter.StartsWith;
            serverDate = DataHelper.serverDate();
            txtDateCreated.Text = serverDate.ToShortDateString();
            ddtReqSubcategory.DefaultMessage = "";

            //if image directory exist delete all files in it
            global_courseImage = null;
            deleteImageInServer(newCourseID);

            txtCourseID.Text = newCourseID.ToString();
        }
    }

    #region CreateCourse

    protected void btnSaveCreateCourse_Click(object sender, EventArgs e)
    {
        //int len2 = ddtSubcategory.SelectedValue.Length;
        //if (len2 > 1)
        //{
        //    ddtSubcategory.SelectedValue = ddtSubcategory.SelectedValue.Substring(len2 - 1, len2 - 2);
        //}
        //else if (len2 == 1)
        //{
        //    ddtSubcategory.SelectedValue = ddtSubcategory.SelectedValue;
        //}
        newCourseID = DataHelper.lastInsertedCourseID() + 1;
        if (dpStartDateTime.SelectedDate != null)
        {
            if (Page.IsValid)
            {
                DateTime defaultMax = new DateTime(2099, 12, 1, 23, 00, 00);
                int dateValid = DateTime.Compare(Convert.ToDateTime(dpStartDateTime.SelectedDate), dpEndDateTime.SelectedDate == null ? defaultMax : Convert.ToDateTime(dpEndDateTime.SelectedDate));

                if (dateValid < 0)
                {
                    int courseId = newCourseID;
                    var courseImage = global_courseImage == null ? "No_image.jpg" : global_courseImage;

                    if (courseImage != "error")
                    {
                        int courseTypeID = Convert.ToInt32(rcbCourseType.SelectedValue);
                        string courseTitle = txtCourseTitle.Text.Trim();
                        int categoryID = Convert.ToInt32(rcbCategory.SelectedValue == "" ? "0" : rcbCategory.SelectedValue);
                        //int subCategoryID = Convert.ToInt32(ddtSubcategory.SelectedValue == "" ? "0" : ddtSubcategory.SelectedValue);
                        //****
                        string[] subcats = ddtSubcategory.SelectedValue.Split(',');
                        int subCategoryID = Convert.ToInt32(subcats[subcats.Length - 1].Trim());
                        //**
                        string description = txtDescription.Text.Trim();
                        DateTime dateCreated = Convert.ToDateTime(txtDateCreated.Text);
                        DateTime startDate = Convert.ToDateTime(dpStartDateTime.SelectedDate);
                        DateTime endDate = dpEndDateTime.SelectedDate.ToString() == "" ? defaultMax : Convert.ToDateTime(dpEndDateTime.SelectedDate);
                        //int duration = Convert.ToInt32(ddlDuration.SelectedValue == "" ? "0" : ddlDuration.SelectedValue);
                        int duration = Convert.ToInt32(txtDuration.Text.Trim() == "" ? "0" : txtDuration.Text.Trim());
                        
                        string AuthorCim = txtAuthor.Text.Trim();
                        string department = rcbDepartment.SelectedValue == "" ? "0" : rcbDepartment.SelectedValue;
                        int reqProgramID = Convert.ToInt32(rcbReqProgram.SelectedValue == "" ? "0" : rcbReqProgram.SelectedValue);
                        int reqCourseID;
                        string level = "";

                        var levelList = rcbLevel.CheckedItems;
                        level = string.Join(",", levelList.Select(l => l.Value));

                        if (rcbReqCourse.SelectedValue == "-1" || rcbReqCourse.SelectedValue == "")
                        {
                            reqCourseID = 0;
                        }
                        else
                        {
                            reqCourseID = Convert.ToInt32(rcbReqCourse.SelectedValue);
                        }

                        int yearsInTenure = Convert.ToInt32(ddlTenureYears.SelectedValue);
                        int yearsInRole = Convert.ToInt32(ddlRoleYears.SelectedValue);

                        int publishedBy = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
                        string accessMode = ddlAccessType.SelectedValue;

                        DataHelper.InsertCourse(courseTitle, categoryID, subCategoryID, description, dateCreated, startDate, endDate, publishedBy, duration, AuthorCim, department, reqProgramID, yearsInTenure, yearsInRole, accessMode, courseImage, courseTypeID, level);

                        IList<RadListBoxItem> collection = lbAssignedCourses.Items;
                        if (collection.Count > 0)
                        {
                            List<int> selectedReqCourse = new List<int>();
                            string str = "";
                            foreach (RadListBoxItem item in collection)
                            {
                                selectedReqCourse.Add(Convert.ToInt32(item.Value));
                                str += item + ",";
                            }
                            DataHelper.insertRequiredCourse(courseId, selectedReqCourse);
                        }
                        clearCourseInput();
                        //string script = "function f(){openSuccessModal(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
                        //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, true);
                        Response.Redirect("CourseBuilder.aspx?CourseID=" + Utils.Encrypt(courseId));
                        //Response.Redirect("CourseBuilder.aspx?CourseID=" + );
                    }
                }
                else
                {
                    RadWindowManager1.RadAlert("End Date should", 330, 180, "Error Start Date", "");
                    //string script = "function f(){openErrorDateModal(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
                    //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, true);
                }
            }
        }
        else
        {

            RadWindowManager1.RadAlert(" Start Date is not defined!", 330, 180, "Error Start Date", "");
            //string script = "function f(){noStartDateError(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, true);
        }
    }

    void clearCourseInput()
    {
        txtCourseTitle.Text = "";
        newCourseID = DataHelper.lastInsertedCourseID() + 1;
        txtCourseID.Text = newCourseID.ToString();
        rcbCategory.ClearSelection();
        ddtSubcategory.Entries.Clear();
        txtDescription.Text = "";
        txtDateCreated.Text = serverDate.ToShortDateString();
        dpStartDateTime.SelectedDate = null;
        dpEndDateTime.SelectedDate = null;
        //ddlDuration.SelectedIndex = 0;
        txtDuration.Text = "0";
        //rcbAuthor.ClearSelection();
        txtAuthor.Text = "";
        rcbDepartment.ClearSelection();
        rcbReqProgram.ClearSelection();
        rcbReqProgram.Text = "";
        rcbReqCourse.ClearSelection();
        rcbReqCourse.SelectedIndex = 0;
        ddtReqSubcategory.Entries.Clear();
        lbAvailableCourses.Items.Clear();
        lbAssignedCourses.Items.Clear();
        ddlRoleYears.SelectedIndex = 0;
        ddlTenureYears.SelectedIndex = 0;
        ddlAccessType.SelectedIndex = 0;
        CourseImage.UploadedFiles.Clear();
        rcbCourseType.ClearSelection();
        rcbLevel.ClearCheckedItems();
        rcbLevel.ClearSelection();
    }

    protected string imageCourse(int courseID, UploadedFile f)
    {

        string error = null;

        if (string.IsNullOrWhiteSpace(error))
        {
            string uploadFolder;
            //string FolderPath = ConfigurationManager.AppSettings["CourseImageDirectoryPath"];

            var userCim = DataHelper.GetCurrentUserCIM();

            if (HttpContext.Current.Request.Url.Host == "localhost")
                uploadFolder = Request.PhysicalApplicationPath + "Media\\Uploads\\CourseImg\\" + courseID + "\\";
            else
                uploadFolder = HttpRuntime.AppDomainAppPath + "Media\\Uploads\\CourseImg\\" + courseID + "\\";

            //uploadFolder =  Server.MapPath(FolderPath + "\\" + courseID);

            if (!Directory.Exists(uploadFolder))
                Directory.CreateDirectory(uploadFolder);

            if (f.ContentType == "image/jpeg" || f.ContentType == "image/png" || f.ContentType == "image/jpg")
            {
                if (f.ContentLength < 1024000000)
                {
                    Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                    string extension = Path.GetExtension(f.FileName);
                    f.SaveAs(uploadFolder + "\\" + f.FileName, true);
                    //f.SaveAs(uploadFolder + unixTimestamp.ToString() + extension, true);
                    //global_courseImage = unixTimestamp.ToString() + extension;
                    global_courseImage = f.FileName;
                }
                else
                {
                    global_courseImage = "error";
                    RadWindowManager1.RadAlert("  File size is too large, please select another one!", 330, 180, "Image too Large", "");
                }
            }
            else
            {
                global_courseImage = "error";
                RadWindowManager1.RadAlert("Error file format please select another one!", 330, 180, "Invalid File Format", "");
            }
        }

        return global_courseImage;
    }

    protected void rcbCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddtSubcategory.Enabled = true;
        ddtSubcategory.Entries.Clear();
        int selectCategoryValue = Convert.ToInt32(rcbCategory.SelectedValue);
        LoadSubcategories(selectCategoryValue);

        if (selectCategoryValue == 17)
            pnlLevelContainer.Visible = true;
        else
            pnlLevelContainer.Visible = false;
    }

    protected void LoadSubcategories(int categoryID)
    {
        ddtSubcategory.DataTextField = "Subcategory";
        ddtSubcategory.DataValueField = "SubcategoryID";
        ddtSubcategory.DataSource = DataHelper.GetSubcategory(categoryID);
        ddtSubcategory.DefaultMessage = "Select Subcategory";
        try
        {

            ddtSubcategory.DataBind();
        }
        catch (ArgumentOutOfRangeException aore)
        {
            RadWindowManager1.RadAlert("Something went wrong. Please reload the page and try again.", 330, 180, "Error", "");


        }
    }

    private static bool IsImage(HttpPostedFile file)
    {
        return ((file != null) && System.Text.RegularExpressions.Regex.IsMatch(file.ContentType, "image/\\S+") && (file.ContentLength > 0));
    }

    protected void LoadReqSubcategories(int categoryID)
    {
        ddtReqSubcategory.DataSource = DataHelper.GetSubcategory(categoryID);
        ddtReqSubcategory.DefaultMessage = "-Select Subcategory-";
        ddtReqSubcategory.DefaultValue = "-1";
        try
        {
            ddtReqSubcategory.DataBind();
        }
        catch (ArgumentOutOfRangeException aore)
        {

        }

    }

    protected void rcbReqCourse_IndexChanged(object sender, EventArgs e)
    {

        if (rcbReqCourse.SelectedValue == "-1")
        {
            //string script = "function f(){hideRequiredCoursePanel(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, true);
            lbAvailableCourses.Items.Clear();
            lbAssignedCourses.Items.Clear();
            ddtReqSubcategory.Entries.Clear();
        }
        else
        {
            //string script = "function f(){showPanel('panelSubReqCategory'); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, true);
            ddtReqSubcategory.Entries.Clear();
            int selectCategoryValue = Convert.ToInt32(rcbReqCourse.SelectedValue);
            LoadReqSubcategories(selectCategoryValue);
        }

    }

    protected void btnSelectCourse_Click(object sender, EventArgs e)
    {
        //load data on rad window
        //show rad window
        int categoryid = 0;
        int subcategoryid = 0;
        try
        {
            categoryid = Convert.ToInt32(rcbReqCourse.SelectedValue);
            string[] d = ddtReqSubcategory.SelectedValue.Split(',');

            if (ddtReqSubcategory.SelectedValue.Contains(','))
                subcategoryid = Convert.ToInt32(d[d.Count() - 1]);
            else// if (len == 1)
                subcategoryid = Convert.ToInt32(ddtReqSubcategory.SelectedValue);


        }
        catch (Exception error)
        {
            categoryid = 0;
            subcategoryid = 0;
        }
        finally
        {
            lbAvailableCourses.DataValueField = "CourseID";
            lbAvailableCourses.DataTextField = "Title";
            lbAvailableCourses.DataSource = DataHelper.GetFilterCourses(categoryid, subcategoryid);
            lbAvailableCourses.DataBind();

        }
        IList<RadListBoxItem> assignedList = lbAssignedCourses.Items;
        IList<RadListBoxItem> availableList = lbAvailableCourses.Items;
        foreach (RadListBoxItem a in assignedList)
        {
            foreach (RadListBoxItem b in availableList)
            {
                if (a.Value == b.Value)
                {
                    b.Checked = true;
                }


            }
        }
        string script = "function f(){openCourseWindow(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
        ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", script, true);

    }

    protected void btnAddCourses_Click(object sender, EventArgs e)
    {
        IList<RadListBoxItem> collection = lbAvailableCourses.CheckedItems;

        if (collection.Count > 0)
        {
            foreach (RadListBoxItem item in collection)
            {
                lbAvailableCourses.Transfer(item, lbAvailableCourses, lbAssignedCourses);
            }
            string script = "function f(){showPanel('panelAssignedCourse'); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", script, true);
        }
        else
        {
            string script = "function f(){hidePanel('panelAssignedCourse'); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", script, true);
        }
        ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox", "ClosePrerequisiteForm()", true);
    }

    protected void DateValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = (args.Value == "");
    }

    //protected void lbtnCancelCreateCourse_Click(object sender, EventArgs e)
    //{
    //    Response.Redirect("Admin.aspx");
    //}

    protected void upload_FileUploaded(object sender, FileUploadedEventArgs e)
    {

        if (e.IsValid)
        {
            imageCourse(newCourseID, e.File);
            courseImagePreview.ImageUrl = "~/Media/Uploads/CourseImg/" + newCourseID + "/" + global_courseImage;
        }
        else
        {
            CourseImage.UploadedFiles.Clear();
            RadWindowManager1.RadAlert("Invalid File. Allowed Files (jpeg,jpg,gif,png)", null, null, "Error Message", "");

        }

    }

    protected void btnRemovePreviewImg_Click(object sender, EventArgs e)
    {

        //courseImagePreview.DataValue = null;
        global_courseImage = "No_image.jpg";
        courseImagePreview.ImageUrl = "";
        deleteImageInServer(newCourseID);
        ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", Utils.callClientScript("hideAllLoading"), true);

    }

    private void deleteImageInServer(int courseID)
    {
        try
        {
            var folderPath = Server.MapPath("Media/Uploads/CourseImg/" + courseID + "\\");
            System.IO.DirectoryInfo folderInfo = new DirectoryInfo(folderPath);

            foreach (FileInfo file in folderInfo.GetFiles())
            {
                file.Delete();
            }
        }
        catch { }

    }

    //protected void validateStartEndDate(object sender, ServerValidateEventArgs  e) 
    //{
    //      var startDate = dpStartDateTime.SelectedDate;
    //      var endDate = dpEndDateTime.SelectedDate;
    //        endDate = endDate == null ? new DateTime(9999, 12, 30) : endDate;
    //        e.IsValid = (endDate > startDate);
    //}

    #endregion
}