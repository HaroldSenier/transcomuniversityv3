﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TranscomUniversityV3.Master" AutoEventWireup="true" CodeFile="DefaultError.aspx.cs" Inherits="DefaultError" %>



<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolderMain" Runat="Server">
<div class="container text-center">
    <h1><i class="fa fa-warning" style="font-size:50px;color:#ff5046"></i><asp:Label runat="server" ID="AnErrorhasOccured" Text= "<%$ Resources:LocalizedResource, AnErrorhasOccured %>"></asp:Label></h1>
    <span>
    <asp:Label runat="server" ID="adminworking" Text="<%$ Resources:LocalizedResource, AdminisworkingonitMeanwhileyoumayreturntoyourlastvisited %>"></asp:Label><button class="btn-link" onclick="goBack()" style="color:#ff5046"><asp:Label runat="server" ID="Label1" Text="<%$ Resources:LocalizedResource, Page %>"></asp:Label></button>.</span>
</div>


<rad:RadScriptBlock runat="server" ID="rsb1">   
<script type="text/javascript">
    function goBack() {
        window.history.back();
    }
</script>

</rad:RadScriptBlock>
</asp:Content>

