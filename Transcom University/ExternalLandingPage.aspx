﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TranscomUniversityV3.Master" AutoEventWireup="true"
    CodeFile="ExternalLandingPage.aspx.cs" Inherits="ExternalLandingPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="contentPlaceHolderLeftPanel" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolderMain" runat="server">
    <asp:Panel runat="server" ID="pnlMain">
        <div class="container">
            <hgroup class="title">
        <h1><asp:Label runat="server" ID="RegisterwithyourGoogleaccount" Text= "<%$ Resources:LocalizedResource, RegisterwithyourGoogleaccount %>"></asp:Label></h1>
        <h2><asp:Label ID="lblEmail" runat="server"></asp:Label><asp:Label ID="lblGoogleName" visible="false" runat="server"></asp:Label><asp:Label ID="lblProfileImage" visible="false" runat="server"></asp:Label></h2>
    </hgroup>
            <p>
                <asp:Label runat="server" ID="Youveauthenticatedwith" Text= "<%$ Resources:LocalizedResource, Youveauthenticatedwith %>"></asp:Label>  <strong><asp:Label runat="server" ID="Google" Text= "<%$ Resources:LocalizedResource, Google %>"></asp:Label></strong> <asp:Label runat="server" ID="as" Text= "<%$ Resources:LocalizedResource, Wordas %>"></asp:Label>  <strong class="primary">
                    <asp:Label ID="lblUsername" runat="server"></asp:Label></strong> <asp:Label runat="server" ID="PleaseconfirmbelowbyclickingtheLoginInbutton" Text= "<%$ Resources:LocalizedResource, PleaseconfirmbelowbyclickingtheLoginInbutton %>"></asp:Label> 
            </p>
            <asp:Button ID="Button1" runat="server" Text="<%$ Resources:LocalizedResource, Login %>" CssClass="btn btn-teal btn-md"
                ValidationGroup="NewUser" OnClick="logIn_Click" />
            <asp:Button ID="Button2" runat="server" Text="<%$ Resources:LocalizedResource, Cancel %>" CssClass="btn btn-teal btn-md"
                CausesValidation="false" OnClick="cancel_Click" />
        </div>
    </asp:Panel>
    <asp:Label ID="ModelErrorMessage1" runat="server" />
</asp:Content>
