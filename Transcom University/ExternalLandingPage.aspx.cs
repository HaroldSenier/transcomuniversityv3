﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Membership.OpenAuth;
using DotNetOpenAuth.AspNet;
using System.Web.Security;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Newtonsoft.Json;
using TranscomUniversityV3Model;
using System.Net;
using System.IO;
using System.Web.Configuration;

public partial class ExternalLandingPage : BasePage
{
    string ProviderName
    {
        get { return (string)ViewState["ProviderName"] ?? String.Empty; }
        set { ViewState["ProviderName"] = value; }
    }

    string ProviderDisplayName
    {
        get { return (string)ViewState["ProviderDisplayName"] ?? String.Empty; }
        set { ViewState["ProviderDisplayName"] = value; }
    }

    string ProviderUserId
    {
        get { return (string)ViewState["ProviderUserId"] ?? String.Empty; }
        set { ViewState["ProviderUserId"] = value; }
    }

    public string ProviderUserName
    {
        get { return (string)ViewState["ProviderUserName"] ?? String.Empty; }
        set { ViewState["ProviderUserName"] = value; }
    }

    protected void Page_Load()
    {
        

        if (!IsPostBack)
        {
            ProcessProviderResult();
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        /*if (!IsPostBack)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
            Response.Cache.SetNoStore();
        }*/
    }

    protected void logIn_Click(object sender, EventArgs e)
    {
        CreateAndLoginUser();
    }

    protected void cancel_Click(object sender, EventArgs e)
    {
        RedirectToReturnUrl();
    }

    private void ProcessProviderResult()
    {
        DotNetOpenAuth.GoogleOAuth2.GoogleOAuth2Client.RewriteRequest();
        // Process the result from an auth provider in the request
        ProviderName = OpenAuth.GetProviderNameFromCurrentRequest();

        if (String.IsNullOrEmpty(ProviderName))
        {
            Response.Redirect("LandingPage.aspx");
        }

        // Build the redirect url for OpenAuth verification
        var redirectUrl = "~/ExternalLandingPage.aspx";
        var returnUrl = Request.QueryString["ReturnUrl"];
        if (!String.IsNullOrEmpty(returnUrl))
        {
            redirectUrl += "?ReturnUrl=" + HttpUtility.UrlEncode(returnUrl);
        }

        // Verify the OpenAuth payload
        AuthenticationResult authResult = OpenAuth.VerifyAuthentication(redirectUrl);
        ProviderDisplayName = OpenAuth.GetProviderDisplayName(ProviderName);
        //unauthenticated_msg.Visible = false;

        //if (DataHelper.isInSAP(ProviderUserName))
        //{
            if (!authResult.IsSuccessful)
            {
                Title = "External login failed";
                //ModelState.AddModelError("Provider", String.Format("External login {0} failed.", ProviderDisplayName));

                // To view this error, enable page tracing in web.config (<system.web><trace enabled="true"/></system.web>) and visit ~/Trace.axd
                Trace.Warn("OpenAuth", String.Format("There was an error verifying authentication with {0})", ProviderDisplayName), authResult.Error);
                // original_message.Visible = false;

                //unauthenticated_msg.Visible = true;
                //unauthenticated_msg.InnerText = "You haven't signed in yet" + String.Format("There was an error verifying authentication with {0})", ProviderDisplayName, authResult.Error);

                return;
            }

            // Strip the query string from action
            Form.Action = ResolveUrl(redirectUrl);
            var extraData = JsonConvert.DeserializeObject<dynamic>(JsonConvert.SerializeObject(authResult.ExtraData));

            // Store the provider details in ViewState
            ProviderName = authResult.Provider;
            ProviderUserId = authResult.ProviderUserId;
            ProviderUserName = extraData.email;

            //Session["GoogleName"] = authResult.UserName;
            lblUsername.Text = ProviderUserName;
            lblEmail.Text = ProviderUserName;
            lblGoogleName.Text = authResult.UserName;
            lblProfileImage.Text = extraData.picture;
            
            if (extraData.hd != null && extraData.hd.ToString() == "transcom.com")
            {
                //original_message.Visible = true;
                TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();
                var profileUrl = extraData.picture.Value;
                if (OpenAuth.Login(authResult.Provider, authResult.ProviderUserId, createPersistentCookie: true))
                {

                    db.pr_TranscomUniversity_InsertOrUpdateProfileImage(ProviderUserName, profileUrl);
                    RedirectToReturnUrl(ProviderUserName + "|" + authResult.UserName + "|" + lblProfileImage.Text + "|" + ProviderUserId);
                }

                // Check if user is already registered locally
                if (User.Identity.IsAuthenticated)
                {
                    // User is already authenticated, add the external login and redirect to return url
                    OpenAuth.AddAccountToExistingUser(ProviderName, ProviderUserId, ProviderUserName, User.Identity.Name);
                    db.pr_TranscomUniversity_InsertOrUpdateProfileImage(ProviderUserName, profileUrl);
                    RedirectToReturnUrl(ProviderUserName + "|" + authResult.UserName + "|" + lblProfileImage.Text + "|" + ProviderUserId);
                }

            }
            else
            {
                //OpenAuth.DeleteAccount(ProviderUserName, ProviderName, ProviderUserId);
                //https://mail.google.com/mail/u/0/?logout&hl=en
                //string strRetMsg ="<script>window.location.href = 'https://www.google.com/accounts/Logout?continue='" + Request.Url.Host + ";</script>";

                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect",
                "window.location='https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http://" +
                HttpContext.Current.Request.Url.Authority + (HttpContext.Current.Request.Url.Host == "localhost" ? "" : HttpContext.Current.Request.ApplicationPath) + "/LandingPage.aspx';", true);

                FormsAuthentication.SignOut();
                // Session.Abandon();
                //AdditionalCaveat.Visible = false;
                Button1.Visible = false;
                Button2.Visible = false;
                ModelErrorMessage1.Text = "Your domain is not allowed";
            }
        //}
        //else {
        //    ModelErrorMessage1.Text = "Your CIM/TWWID does not exist please notify the Administrator";
        //    pnlMain.Visible = false;
        //}
      
    }

    private void CreateAndLoginUser()
    {
        if (!IsValid)
        {
            return;
        }

        var createResult = OpenAuth.CreateUser(ProviderName, ProviderUserId, ProviderUserName, lblUsername.Text);
        if (!createResult.IsSuccessful)
        {
            //ModelState.AddModelError("UserName", createResult.ErrorMessage);
        }
        else
        {
            // User created & associated OK
            if (OpenAuth.Login(ProviderName, ProviderUserId, createPersistentCookie: false))
            {
                RedirectToReturnUrl(lblUsername.Text + "|" + lblGoogleName.Text + "|" + lblProfileImage.Text + "|" + ProviderUserId);
            }
        }
    }

    private void RedirectToReturnUrl()
    {
        var returnUrl = Request.QueryString["ReturnUrl"];

        if (!String.IsNullOrEmpty(returnUrl) && OpenAuth.IsLocalUrl(returnUrl))
        {
            Response.Redirect(returnUrl);
        }
        else
        {
            Response.Redirect("~/Home.aspx");
        }
    }

    private void RedirectToReturnUrl(string userID)
    {
        var returnUrl = Request.QueryString["ReturnUrl"];
        FormsAuthentication.SetAuthCookie(userID, true);

        //if (!String.IsNullOrEmpty(returnUrl) && OpenAuth.IsLocalUrl(returnUrl))
        //{
        //    Response.Redirect(returnUrl);
        //}
        //else
        //{
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {

            var CIM_Number = (from r in db.vw_TranscomUniversity_SelectUserCIM
                              where r.UserName == ProviderUserName
                              select r.CIM_Number).FirstOrDefault();
            userID = userID + "|" + CIM_Number;
            FormsAuthentication.SetAuthCookie(userID, true);
            //if (CIM_Number != null)
            //{
                var userId = (from r in db.vw_TranscomUniversity_SelectUserCIM
                              where r.UserName == ProviderUserName
                              select r.UserId).FirstOrDefault();

                string serviceUrl = WebConfigurationManager.AppSettings["LokiV2RestApi"] + Convert.ToInt32(CIM_Number) + "/35";

                HttpWebRequest httpRequest = (HttpWebRequest)WebRequest.Create(new Uri(serviceUrl));
                httpRequest.Accept = "application/json";
                httpRequest.ContentType = "application/json";
                httpRequest.Method = "GET";

                using (HttpWebResponse httpResponse = (HttpWebResponse)httpRequest.GetResponse())
                {
                    using (Stream stream = httpResponse.GetResponseStream())
                    {
                        string output = (new StreamReader(stream)).ReadToEnd();

                        if (output != "[]")
                        {
                            string[] role = output.Split(new string[] { "\"" }, StringSplitOptions.None);
                            string roleName = role[11].ToString();

                            if (roleName == "Admin")
                                db.pr_TranscomUniversity_InsertUsersInRoles(userId.ToString(), WebConfigurationManager.AppSettings["AdminRole"]);
                            else if (roleName == "Trainer")
                                db.pr_TranscomUniversity_InsertUsersInRoles(userId.ToString(), WebConfigurationManager.AppSettings["TrainerRole"]);
                            else
                                db.pr_TranscomUniversity_InsertUsersInRoles(userId.ToString(), WebConfigurationManager.AppSettings["LearnerRole"]);


                            Session["SessionRoles"] = "Learner";
                            Response.Redirect("~/Home.aspx");
                        }
                        else
                        {

                            db.pr_TranscomUniversity_InsertUsersInRoles(userId.ToString(), WebConfigurationManager.AppSettings["LearnerRole"]);
                            Response.Redirect("~/Home.aspx");
                            //}
                           

                        }
                    }
                }
                //}
            //} else {
            //    pnlMain.Visible = false;
            //    Response.Write("User Is Not in SAP CIM Number is NULL");
            //}

        }
    }
}