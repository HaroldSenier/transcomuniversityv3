﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TranscomUniversityV3.Master" AutoEventWireup="true"
    CodeFile="Home.aspx.cs" Inherits="Home2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="contentPlaceHolderLeftPanel" runat="Server">
    <style>
    @media (min-width: 768px){
        .modal-dialog-lg {
            width: 600px;
            margin: 30px 125px 0px 125px;
        }
    }
    .modal-content {
        width: 1300px;
        max-width: 1300px;
    }
    </style>
    <rad:RadScriptBlock ID="RadScriptBlock1" runat="server">
        <script type="text/javascript">
            var heightSet;
            var recommendedLeadershipCoursesListView;
            var recommendedLeadershipCoursesGridView;
            var arrRecommendeLeadership;
            var GLOBAL_PAGESIZE;
            var firstLoad;
            var viewAllLeadership;
            var viewAllMandatory;
            var viewAllSkillDev;
            var viewAllRecently;

            function pageLoad() {
                console.log("p");
                //initializeVideo(); //set the onlick events of videos
                //set the height of the left and right content equally to fill the space
                initializeVideoModal(); //set the events of the modal
                lazyload();

                if (ISDESKTOP == true) {
                    setContentHeight();
                }



                if ($("#pnlLvRecommendedCoursesForYou").length > 0) {
                    initializeRecommendedLeadershipCourse();
                }


                initializeMandatoryCourse();
                initializeSkillDevelopment();
                initializeRecentCourse();
                initializeTrendingCourse();

            }



            //COMMENTED FOR NOW
            //            $(window).scroll(function () {
            //                if (ISDESKTOP == true) {
            //                    setContentHeight();
            //                }
            //            });

            function setContentHeight() {
                var height = $("#<%= home_container.ClientID %>").height();
                $(".left-side").height(height);
                $(".right-side").height(height);
                console.log("setheight - " + height);
            }

            function initializeVideoModal() {
                var modal = $('#videoModal');

                $(".video-thumbnail").click(function (event) {
                    var modal_video = $(".modal-video").get(0);
                    var src = $(this).find("input").val();
                    //                    modal_video.type = 'video/mp4';
                    //                    modal_video.src = src;
                    //                    modal_video.play();
                    $("#modalVideoContainer").empty();
                    var video = document.createElement("video");
                    video.src = src;
                    $(video).attr('class', 'video modal-content btn-flat modal-video');
                    $(video).attr('width', '466');
                    $(video).attr('data-height', '329');
                    $(video).attr('controls', '');
                    $(video).attr('style', 'height: 50vh');
                    video.type = "video/mp4";
                    video.preload = "metadata";
                    video.play();


                    var bottom = document.getElementById("modalVideoContainer");
                    bottom.appendChild(video);
                });

                // Get the <span> element that closes the modal
                var span = $("#videoModal span")

                // When the user clicks on <span> (x), close the modal
                span.click(function () {
                    modal.fadeOut();

                });

                $('#videoModal').on('hidden.bs.modal', function () {
                    var modal_video = $(".modal-video").get(0);
                    modal_video.pause();
                    modal_video.removeAttribute('src'); // empty source
                    modal_video.load();

                    //modal.fadeOut();
                })

            }

            function disableBodyScroll() {
                //$("body").addClass("noscroll");
                //var scrollTop = ($('html').scrollTop()) ? $('html').scrollTop() : $('body').scrollTop(); // Works for Chrome, Firefox, IE...
                $('html').addClass('noscroll');
                //.css('top', -scrollTop);
            }

            function enableBodyScroll() {
                //$("body").removeClass("noscroll");
                //var scrollTop = parseInt($('html').css('top'));

                $('html').removeClass('noscroll');

                //$('html,body').scrollTop(-scrollTop);
            }

            //            function initializeVideo() {
            //                $('.video-container').click(function () {
            //                    console.log("play/pause");
            //                    var video = $(this).find(".video").get(0);
            //                    if (video.paused) {
            //                        video.play();
            //                    } else {
            //                        video.pause();
            //                    }
            //                });

            //                $('.video').on('play', function (e) {
            //                    console.log("playing");
            //                    var video = $(this).get(0);
            //                    $(this).parent().find(".video-title").fadeOut();
            //                    $(this).parent().find(".video-overlay").addClass("display-none");
            //                    if (!video.hasAttribute("controls") && $(this).hasClass("video-md")) {
            //                        video.setAttribute("controls", "controls")
            //                    }

            //                });
            //                $('.video').on('pause', function (e) {
            //                    console.log("paused");
            //                    var video = $(this).get(0);
            //                    $(this).parent().find(".video-title").fadeIn();
            //                    $(this).parent().find(".video-overlay").removeClass("display-none");
            //                    if ($(this).hasClass("video-md"))
            //                        video.removeAttribute("controls")
            //                });
            //            }



            function gotoLearnerDashboard() {
                var tz = $("#hfEncryptedOffset").val();
                window.location.href = "Learner.aspx?Tab=enrolledCourses&tz=" + tz;
            }

            function mandatory_scrollLeft(left) {
                $('.m-mandatory-course-nav ul').animate({ scrollLeft: left }, 800);


            }

            //SKILL DEVELOPMENT//
            //initialize mandatory Course
            function initializeMandatoryCourse() {
                var x700 = window.matchMedia('(min-width:0px) and (max-width: 700px)')
                var x1000 = window.matchMedia('(min-width:701px) and (max-width: 1000px)')
                $ = $telerik.$;

                //this will fire the OnCommand event

                mandatoryListView = $find("<%= lvMandatoryCourses.ClientID%>");

                if (x700.matches) {
                    recommendedLeadershipCoursesListView.set_pageSize(1);
                }
                if (x1000.matches) {
                    recommendedLeadershipCoursesListView.set_pageSize(2);
                }
                else { recommendedLeadershipCoursesListView.set_pageSize(3); }
                mandatoryListView.rebind();
                mandatoryListView.page(0);

                $(".mcPager .pagePrev").click(function (e) {

                    mandatoryListView.page(mandatoryListView.get_currentPageIndex() - 1);

                });
                $(".mcPager .pageNext").click(function (e) {

                    mandatoryListView.page(mandatoryListView.get_currentPageIndex() + 1);

                });

            }

            function mcOnListViewDataBinding(sender, args) {
                sender.set_selectedIndexes([]);
            }

            function mcOnListViewCommand(sender, args) {
                args.set_cancel(true);
                bindMandatoryCourseList();
            }

            function bindMandatoryCourseList() {

                mandatoryListView = $find("<%= lvMandatoryCourses.ClientID %>");
                var startRowIndex = mandatoryListView.get_currentPageIndex() * mandatoryListView.get_pageSize(),
                maximumRows = mandatoryListView.get_pageSize(),
                sortExpression = mandatoryListView.get_sortExpressions().toLinq();

                //check if theres a sessioned data
//                var sData = sessionStorage.getItem('mp' + mandatoryListView.get_currentPageIndex());

//                if (sData != null && viewAllMandatory == false) {
//                    //scrollTo("mandatoryListView");
//                    sData = JSON.parse(sData);
//                    mandatoryListView.set_dataSource(sData);
//                    mandatoryListView.dataBind();

//                    lazyload();

//                    $(".mcPager .pagePrev").removeClass("display-none");
//                    $(".mcPager .pageNext").removeClass("display-none");

//                } else {

//                    
                //                }
                $(".mcPager .pagePrev").hide();
                $(".mcPager .pageNext").hide();
                $(".js-mc-loader").show();
                $("#mc-items").hide();


                console.log("loaded server mandatory page" + mandatoryListView.get_currentPageIndex());

                //make a call to get the data
                $.ajax({
                    type: "POST",
                    data: "{startRowIndex: " + startRowIndex + ", maximumRows: " + maximumRows + ", sortExpression: '" + sortExpression + "'}",
                    contentType: "application/json; charset=utf-8",
                    url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "GetAllCoursesNew",
                    dataType: 'json',
                    success: function (res) {
                        var count = res.d.count;
                        var result = res.d.data;
                        mandatoryListView.set_virtualItemCount(count);

                        if (count <= 3) {
                            $(".mcPager .pagePrev").removeClass("display-none");
                            $(".mcPager .pageNext").removeClass("display-none");
                            $(".mcViews").removeClass("display-none");
                        }

                        $(".js-mc-loader").hide();
                        $(".mcPager .pagePrev").show();
                        $(".mcPager .pageNext").show();
                        $("#mc-items").show();

                        if (count < 4) {
                            $(".mcPager .pagePrev").removeClass("display-none");
                            $(".mcPager .pageNext").removeClass("display-none");
                        }

                        if (count <= 0) {
                            $("<%= divSkillDev.ClientID %>").hide();
                        }

                        $(".js-mc-loader").hide();

                        mandatoryListView.set_dataSource(result);
                        mandatoryListView.dataBind();

                        lazyload();

                        if (sData == null)
                            sessionStorage.setItem('mp' + mandatoryListView.get_currentPageIndex(), JSON.stringify(result));

                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        var code = XMLHttpRequest.status;
                        if (code == "401")
                            window.location.reload();
                    }
                });
            }
            //END SKILL DEVELOPMENT//

            //START TRANSCOM WIDE//
            //initialize Recommender Leaderhip Course
            function initializeRecommendedLeadershipCourse() {
                var x700 = window.matchMedia('(min-width:0px) and (max-width: 700px)')

                $ = $telerik.$;
                //this will fire the OnCommand event

                recommendedLeadershipCoursesListView = $find("<%= lvRecommendedLeadershipCourses.ClientID%>");
                if (x700.matches) {
                    recommendedLeadershipCoursesListView.set_pageSize(1);
                }
                else { recommendedLeadershipCoursesListView.set_pageSize(3); }

                recommendedLeadershipCoursesListView.rebind();

                recommendedLeadershipCoursesListView.page(0);

                $(".rlcPager .pagePrev").click(function (e) {

                    recommendedLeadershipCoursesListView.page(recommendedLeadershipCoursesListView.get_currentPageIndex() - 1);

                });
                $(".rlcPager .pageNext").click(function (e) {

                    recommendedLeadershipCoursesListView.page(recommendedLeadershipCoursesListView.get_currentPageIndex() + 1);

                });


            }
            function rlcfyOnListViewDataBinding(sender, args) {
                sender.set_selectedIndexes([]);
            }

            function rlcfyOnListViewCommand(sender, args) {
                args.set_cancel(true);
                bindRecommendedLeadeshipCourseList();
            }

            function bindRecommendedLeadeshipCourseList() {
                recommendedLeadershipCoursesListView = $find("<%= lvRecommendedLeadershipCourses.ClientID %>");
                var startRowIndex = recommendedLeadershipCoursesListView.get_currentPageIndex() * recommendedLeadershipCoursesListView.get_pageSize(),
                maximumRows = recommendedLeadershipCoursesListView.get_pageSize(),
                sortExpression = recommendedLeadershipCoursesListView.get_sortExpressions().toLinq();

                //check if theres a sessioned data
                var sData = sessionStorage.getItem('lp' + recommendedLeadershipCoursesListView.get_currentPageIndex());

//                if (sData != null && viewAllLeadership == false) {
//                    //scrollTo("recommendedLeadershipCoursesListView");
//                    sData = JSON.parse(sData);
//                    recommendedLeadershipCoursesListView.set_dataSource(sData);
//                    recommendedLeadershipCoursesListView.dataBind();

//                    lazyload();


//                    $(".rlcPager .pagePrev").removeClass("display-none");
//                    $(".rlcPager .pageNext").removeClass("display-none");

//                } else {
//                    
                //                }
                $(".rlcPager .pagePrev").hide();
                $(".rlcPager .pageNext").hide();
                $(".js-rlc-loader").show();
                $("#lc-items").hide();

                $.ajax({
                    type: "POST",
                    data: "{startRowIndex: " + startRowIndex + ", maximumRows: " + maximumRows + ", sortExpression: '" + sortExpression + "'}",
                    contentType: "application/json; charset=utf-8",
                    url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "GetCourseMandatoryCourseNew",
                    dataType: 'json',
                    success: function (res) {
                        var count = res.d.count;
                        var result = res.d.data;

                        $("#recommendedLeadershipCoursesListView").removeClass("display-none");
                        $("#recommendedLeadershipCoursesGridView").removeClass("display-none");

                        recommendedLeadershipCoursesListView.set_virtualItemCount(count);
                        if (count < 4) {
                            $(".rlcPager .pagePrev").removeClass("display-none");
                            $(".rlcPager .pageNext").removeClass("display-none");
                            $(".rlcViews").removeClass("display-none");
                        }

                        recommendedLeadershipCoursesListView.set_dataSource(result);
                        recommendedLeadershipCoursesListView.dataBind();

                        lazyload();

                        if (count <= 3) {
                            $(".rlcPager .pagePrev").removeClass("display-none");
                            $(".rlcPager .pageNext").removeClass("display-none");
                        }
                        if (count <= 0) {
                            $(".rlcPager .pagePrev").hide();
                            $(".rlcPager .pageNext").hide();
                        }
                        else {
                            $(".rlcPager .pagePrev").show();
                            $(".rlcPager .pageNext").show();
                        }

                        $(".js-rlc-loader").hide();
                        $("#lc-items").show();

                        if (sData == null)
                            sessionStorage.setItem('lp' + recommendedLeadershipCoursesListView.get_currentPageIndex(), JSON.stringify(result));


                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        var code = XMLHttpRequest.status;
                        if (code == "401")
                            window.location.reload();
                    }
                });
            }
            //END TRANSCOM WIDE//

            //START PROGRAMS SPECIFIC//
            //initialize Skill Development Course
            function initializeSkillDevelopment() {
                var x700 = window.matchMedia('(min-width:0px) and (max-width: 700px)')

                $ = $telerik.$;
                //this will fire the OnCommand event
                var listView = $find("<%= lvSkillDevelopments.ClientID %>");
                if (x700.matches) {
                    listView.set_pageSize(1);
                }
                else {
                    listView.set_pageSize(3);
                }

                listView.rebind();

                listView.page(0);



                $(".mtPager .pagePrev").click(function (e) {
                    listView.page(listView.get_currentPageIndex() - 1);
                });
                $(".mtPager .pageNext").click(function (e) {
                    listView.page(listView.get_currentPageIndex() + 1);
                });
            }
            function mtOnListViewDataBinding(sender, args) {
                sender.set_selectedIndexes([]);
            }
            function mtOnListViewCommand(sender, args) {
                //cancel command event to prevent postback
                args.set_cancel(true);
                bindSkillDevelopmentList();
            }
            //Bind SkillDevelopmentCourse 
            function bindSkillDevelopmentList() {
                var listView = $find("<%= lvSkillDevelopments.ClientID %>");
                var startRowIndex = listView.get_currentPageIndex() * listView.get_pageSize(),
                maximumRows = listView.get_pageSize(),
                sortExpression = listView.get_sortExpressions().toLinq();

                //check if theres a sessioned data
//                var sData = sessionStorage.getItem('sp' + listView.get_currentPageIndex());

//                if (sData != null && viewAllSkillDev == false) {
//                    //scrollTo("mandatoryListView");
//                    sData = JSON.parse(sData);
//                    listView.set_dataSource(sData);
//                    listView.dataBind();

//                    lazyload();

//                } else {

//                    
                //                }
                $(".mtPager .pagePrev").hide();
                $(".mtPager .pageNext").hide();
                $(".js-mt-loader").show();
                $("#mtItems").hide();


                $.ajax({
                    type: "POST",
                    data: "{startRowIndex: " + startRowIndex + ", maximumRows: " + maximumRows + ", sortExpression: '" + sortExpression + "', cim: '" + $("#hdnCimNo").val() + "', SubcategoryID: '" + $("#hdnSubCategoryID").val() + "'}",
                    contentType: "application/json; charset=utf-8",
                    url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "GetProgramSpecificCourseNew",
                    dataType: 'json',
                    success: function (res) {

                        $("#SkillDevelopmentTakeListView").removeClass("display-none");
                        $("#SkillDevelopmentGridView").removeClass("display-none");
                        var result = res.d.data;
                        var count = res.d.count;

                        listView.set_virtualItemCount(count);

                        if (count <= 3) {
                            $(".mtPager .pagePrev").removeClass("display-none");
                            $(".mtPager .pageNext").removeClass("display-none");
                        }

                        if (count <= 0) {
                            $(".mtPager .pagePrev").hide();
                            $(".mtPager .pageNext").hide();
                        }
                        else {
                            $(".mtPager .pagePrev").show();
                            $(".mtPager .pageNext").show();
                        }
                        $(".js-mt-loader").hide();
                        $("#mtItems").show();

                        listView.set_dataSource(result);
                        listView.dataBind();

                        lazyload();

                        if (sData == null)
                            sessionStorage.setItem('sp' + listView.get_currentPageIndex(), JSON.stringify(result));


                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        var code = XMLHttpRequest.status;
                        if (code == "401")
                            window.location.reload();
                        //else
                        //alert(XMLHttpRequest.responseText);
                    }
                });
            }
            //END PROGRAMS SPECIFIC//

            //START REGIONAL COURSES//
            function initializeRecentCourse() {
                var x700 = window.matchMedia('(min-width:0px) and (max-width: 700px)')

                $ = $telerik.$;
                //this will fire the OnCommand event
                var listView = $find("<%= lvRecentlyAddedCourses.ClientID%>");

                if (x700.matches) {
                    listView.set_pageSize(1);
                }
                else { listView.set_pageSize(3); }

                listView.rebind();

                listView.page(0);

                $(".rcPager .pagePrev").click(function (e) {
                    listView.page(listView.get_currentPageIndex() - 1);
                });
                $(".rcPager .pageNext").click(function (e) {
                    listView.page(listView.get_currentPageIndex() + 1);
                });
            }
            //Client Events RecentlyAddedCourse
            function rcOnListViewDataBinding(sender, args) {
                sender.set_selectedIndexes([]);
            }
            function rcOnListViewCommand(sender, args) {
                //cancel command event to prevent postback
                args.set_cancel(true);
                bindRecentCourseList();
            }
            function bindRecentCourseList() {
                var listView = $find("<%= lvRecentlyAddedCourses.ClientID%>"); ;
                var startRowIndex = listView.get_currentPageIndex() * listView.get_pageSize(),
                maximumRows = listView.get_pageSize(),
                sortExpression = listView.get_sortExpressions().toLinq();

                //check if theres a sessioned data
//                var sData = sessionStorage.getItem('rp' + listView.get_currentPageIndex());

//                if (sData != null && viewAllRecently == false) {
//                    //scrollTo("mandatoryListView");
//                    sData = JSON.parse(sData);
//                    listView.set_dataSource(sData);
//                    listView.dataBind();

//                    lazyload();

//                } else {

//                    
                //                }

                $(".rcPager .pagePrev").hide();
                $(".rcPager .pageNext").hide();
                $(".js-rc-loader").show();
                $("#rcItems").hide();

                $.ajax({
                    type: "POST",
                    data: "{startRowIndex: " + startRowIndex + ", maximumRows: " + maximumRows + ", sortExpression: '" + sortExpression + "'}",
                    contentType: "application/json; charset=utf-8",
                    url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "GetCourseRegionalCourseNew",
                    dataType: 'json',
                    success: function (res) {
                        var count = res.d.count;
                        var result = res.d.data;
                        $("#recentListView").removeClass("display-none");
                        $("#recentGridView").removeClass("display-none");

                        listView.set_virtualItemCount(count);
                        if (count <= 3) {
                            $(".rcPager .pagePrev").removeClass("display-none");
                            $(".rcPager .pageNext").removeClass("display-none");
                        }

                        if (count <= 0) {
                            $(".rcPager .pagePrev").hide();
                            $(".rcPager .pageNext").hide();
                        }
                        else {
                            $(".rcPager .pagePrev").show();
                            $(".rcPager .pageNext").show();
                        }

                        $(".js-rc-loader").hide();
                        $("#rcItems").show();

                        listView.set_dataSource(result);
                        listView.dataBind();

                        lazyload();

                        if (sData == null)
                            sessionStorage.setItem('rp' + listView.get_currentPageIndex(), JSON.stringify(result));

                        if (firstLoad == true) {
                            firstLoad = false;
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        var code = XMLHttpRequest.status;
                        if (code == "401")
                            window.location.reload();
                        //else
                        //alert(XMLHttpRequest.responseText);
                    }
                });
            }
            //END REGIONAL COURSES//

            //START COUNTRY COURSES//
            function initializeTrendingCourse() {
                debugger;
                var x700 = window.matchMedia('(min-width:0px) and (max-width: 700px)')

                $ = $telerik.$;
                //this will fire the OnCommand event
                var listView = $find("<%= lvTrendingCourses.ClientID%>");

                if (x700.matches) {
                    listView.set_pageSize(1);
                }
                else { listView.set_pageSize(3); }

                listView.rebind();
                listView.page(0);


                $(".tcPager .pagePrev").click(function (e) {
                    listView.page(listView.get_currentPageIndex() - 1);
                });
                $(".tcPager .pageNext").click(function (e) {
                    listView.page(listView.get_currentPageIndex() + 1);
                });
            }

            //Client Events TrendingCourse
            function tcOnListViewDataBinding(sender, args) {
                sender.set_selectedIndexes([]);
            }

            function tcOnListViewCommand(sender, args) {
                //cancel command event to prevent postback
                args.set_cancel(true);
                //dabind RadListView programmatically
                bindTrendingCourseList();
            }
            function bindTrendingCourseList() {
                debugger;
                var listView = $find("<%= lvTrendingCourses.ClientID%>");
                var startRowIndex = listView.get_currentPageIndex() * listView.get_pageSize(),
                maximumRows = listView.get_pageSize(),
                sortExpression = listView.get_sortExpressions().toLinq();

                //check if theres a sessioned data
//                var sData = sessionStorage.getItem('tp' + listView.get_currentPageIndex());

//                if (sData != null) {
//                    $(".tcPager .pagePrev").hide();
//                    $(".tcPager .pageNext").hide();
//                    $(".js-ct-loader").show();
//                    $("#tc-items").hide();

//                    //scrollTo("recommendedLeadershipCoursesListView");
//                    sData = JSON.parse(sData);
//                    listView.set_dataSource(sData);
//                    listView.dataBind();

//                    lazyload();

//                    $(".js-ct-loader").hide();
//                    $("#tc-items").show();

//                    $(".tcPager .pagePrev").removeClass("display-none");
//                    $(".tcPager .pageNext").removeClass("display-none");


//                } else {

//                    


                //                }
                $(".tcPager .pagePrev").hide();
                $(".tcPager .pageNext").hide();
                $(".js-ct-loader").show();
                $("#tc-items").hide();

                $.ajax({
                    type: "POST",
                    data: "{startRowIndex: " + startRowIndex + ", maximumRows: " + maximumRows + ", sortExpression: '" + sortExpression + "'}",
                    contentType: "application/json; charset=utf-8",
                    url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "GetCourseCountryCourseNew",
                    dataType: 'json',
                    success: function (res) {
                        var count = res.d.count;
                        var result = res.d.data;
                        listView.set_virtualItemCount(count);

                        if (count <= 3) {
                            $(".tcPager .pagePrev").removeClass("display-none");
                            $(".tcPager .pageNext").removeClass("display-none");
                        }

                        if (count <= 0) {
                            $(".tcPager .pagePrev").hide();
                            $(".tcPager .pageNext").hide();
                        }
                        else {
                            $(".tcPager .pagePrev").show();
                            $(".tcPager .pageNext").show();
                        }

                        $(".js-ct-loader").hide();
                        $("#tc-items").show();


                        listView.set_dataSource(result);
                        listView.dataBind();

                        lazyload();

                        if (sData == null)
                            sessionStorage.setItem('tp' + listView.get_currentPageIndex(), JSON.stringify(result));

                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        var code = XMLHttpRequest.status;
                        if (code == "401")
                            window.location.reload();
                    }
                });

            }
            //END COUNTRY COURSES//
        </script>
    </rad:RadScriptBlock>
    <rad:RadWindowManager ID="RadWindowManager1" RenderMode="Lightweight" EnableShadow="true"
        Modal="true" VisibleOnPageLoad="false" Behaviors="Close, Move" DestroyOnClose="true"
        RestrictionZoneID="RestrictionZone" Opacity="99" runat="server" Width="800" Height="400px"
        Skin="Bootstrap">
    </rad:RadWindowManager>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolderMain" runat="Server">
    <%--<asp:HiddenField runat="server" ID="hfAreYouSureYouWantToLaunchThisCourse" Value="<%$ Resources: LocalizedResource, AreYouSureYouWantToLaunchThisCourse %>" />
    <asp:HiddenField runat="server" ID="hfConfirmLaunch" Value="<%$ Resources: LocalizedResource, ConfirmLaunch %>" />--%>
    <asp:Panel ID="main_container" runat="server" CssClass="container no-padding">
        <asp:Panel ID="home_container" runat="server" CssClass="container no-padding home_container">
            <div class="col-sm-12 col-lg-8 bg-white ResponsiveLastPage" style="height: 100%">
                <div class="section row padding-side-10 bg-white m-category-container m-category-container"
                    style="margin-top: 16px;">
                    <asp:Panel runat="server" ID="divLeadership">
                        <div class="article container no-padding">
                            <h4 class="orange pull-left m-title">
                                <asp:Label ID="Label1" runat="server" Text="<%$ Resources: LocalizedResource, RecommendedLeadershipCoursesForYou %>" />
                            </h4>
                            <a id="lnkSeeAllLeadership" runat="server" class="orange pull-right underline m-lnk-seeall"
                                style="margin-top: 1%;">
                                <asp:Label ID="Label3" runat="server" Text="<%$ Resources: LocalizedResource, SeeAll %>" /></a>
                        </div>
                        <div class="course-container" style="margin-left: -15px; margin-top: 15px;">
                            <asp:Repeater ID="rptLeadership" runat="server">
                                <ItemTemplate>
                                    <div class="col-md-4 col-xs-12 m-course-container">
                                        <div class="thumbnail">
                                            <img src='Media/Images/empty.png' data-src='Media/Uploads/CourseImg/<%# Eval("CourseID") %>/<%# Eval("CourseImage") %>'
                                                onerror="this.src='Media/Uploads/CourseImg/No_image.jpg'" style="height: 140px;
                                                width: 100%" title="<%#Eval("CourseTitle")%>" class="lazyload" />
                                            <div class="caption">
                                                <label class="truncate-line-1">
                                                    <%#Eval("CourseTitle")%></label>
                                                <p class="truncate-line-1">
                                                    Duration</p>
                                                <p class="truncate-line-3" title="<%# Eval("CourseDescription") %>">
                                                    <%# Eval("CourseDescription") %>
                                                </p>
                                                <div class="thumbnail-footer">
                                                    <a class="pointer font-small" onclick='<%# string.Format("confirmLaunchCourse(\"{0}\")", Eval("EncryptedCourseID")) %>'>
                                                        [See Details]</a>
                                                    <p class="truncate-line-1">
                                                        Viewers</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Panel ID="emptyData" runat="server" Visible="false">
                                        <asp:Label ID="Label4" runat="server" Text="<%$ Resources: LocalizedResource, NoRecommendedLeadershipCoursesToDisplay %>" />
                                    </asp:Panel>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </asp:Panel>
                </div>
                <div class="section row padding-side-10 bg-white m-category-container">
                    <div class="course-container" style="margin: 5px 0 0px -15px;">
                        <asp:Panel runat="server" ID="divSkillDev">
                            <div class="article container no-padding" style="padding-left: 14px;">
                                <h4 class="orange pull-left  m-title">
                                    <asp:Label ID="Label4" runat="server" Text="<%$ Resources: LocalizedResource, SkillDevelopmentCourses %>" />
                                </h4>
                                <a id="lnkSeeAllSkillDev" runat="server" class="orange pull-right underline m-lnk-seeall"
                                    style="margin-top: 1%;">
                                    <asp:Label ID="Label5" runat="server" Text="<%$ Resources: LocalizedResource, SeeAll %>" /></a>
                            </div>
                            <rad:RadListView ID="lvMandatoryCourses" runat="server" AllowPaging="true" PageSize="3"
                                AllowMultiFieldSorting="true">
                                <LayoutTemplate>
                                    <div id="mandatoryListView" class="container-fluid">
                                        <div class="mcPager row-thumbnail">
                                            <div class="loader js-mc-loader">
                                            </div>
                                            <a class="pagePrev navigation-left-center no-underline-hover" href="javascript:void(0);"
                                                title="Go to previous page">
                                                <img src="Media/Images/left-navigation-arrow.png" class="hover-button" style="height: 40px;" /></a>
                                            <a class="pageNext navigation-right-center no-underline-hover" href="javascript:void(0);"
                                                title="Go to next page">
                                                <img src="Media/Images/right-navigation-arrow.png" class="hover-button" style="height: 40px;" /></a>
                                            <div id="mc-items">
                                            </div>
                                        </div>
                                    </div>
                                </LayoutTemplate>
                                <ClientSettings>
                                    <DataBinding ItemPlaceHolderID="mc-items">
                                        <ItemTemplate>
                                        <div class="col-xs-12 col-sm-6 col-md-4  m-course-container">
                                            <div id="divRecommendedLeadershipCourses" runat="server" style="color:Black;" title="#= CourseTitle #" >
                                                <div class="thumbnail">
                                                    <img src='Media/Images/empty.png' data-src='Media/Uploads/CourseImg/#= CourseID #/#= CourseImage #' style="height:120px;width:100%;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;" onerror="this.src='Media/Uploads/CourseImg/No_image.jpg'" class="lazyload"></img>
                                                                            
                                                    <div class="caption">
                                                        <label class="truncate-line-1" style="font-size: 15px;">#= CourseTitle #</label>
                                                        <p class="truncate-line-1">#= CourseDuration #</p>
                                                        <p class="truncate-line-3" title=" #= CourseDuration # "> #= CourseDescription #  </p>
                                                        <a class="pointer font-small" onclick='confirmLaunchCourse("#= EncryptedCourseID #");return false;'>[See Details]</a>
                                                        <div class="thumbnail-footer">
                                                                                        
                                                            <p class="truncate-line-1">#= CourseViews # viewers</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </ItemTemplate>
                                        <EmptyDataTemplate>
                                                                No Mandatory Course.
                                        </EmptyDataTemplate>
                                        <DataService EnableCaching="true" />
                                    </DataBinding>
                                    <ClientEvents OnCommand="mcOnListViewCommand" OnDataBinding="mcOnListViewDataBinding">
                                    </ClientEvents>
                                </ClientSettings>
                            </rad:RadListView>
                        </asp:Panel>
                    </div>
                </div>
                <div class=" section row padding-side-10 bg-white m-category-container" style="padding-bottom: 20px;">
                    <div class="article container no-padding">
                        <h4 class="orange pull-left m-title">
                            <asp:Label ID="Label6" runat="server" Text="<%$ Resources: LocalizedResource, MandatoryCourses %>" />
                        </h4>
                        <a id="lnkSeeAllMandatory" runat="server" class="orange pull-right underline m-lnk-seeall display-none"
                            style="margin-top: 3%;">
                            <asp:Label ID="Label7" runat="server" Text="<%$ Resources: LocalizedResource, SeeAll %>" /></a>
                    </div>
                    <div class="course-container" style="margin: 5px 0 0px -15px;">
                        <div class="m-mandatory-course-nav display-inline-flex pull-left col-lg-12">
                            <i class="fa fa-angle-left m-arrow-left margin-side-10" onclick='mandatory_scrollLeft(-1000);'
                                style="line-height: 42px; display: none"></i>
                            <ul class="nav nav-tabs tab-bottom-border black-font ">
                                <li class="active"><a data-toggle="tab" href="#tabTranscomWide">
                                    <asp:Label ID="Label9" runat="server" Text="<%$ Resources: LocalizedResource, TranscomDashWide %>" /></a></li>
                                <li><a data-toggle="tab" href="#tabProgramSpecific">
                                    <asp:Label ID="Label8" runat="server" Text="<%$ Resources: LocalizedResource, Programspecific %>" /></a></li>
                                <li><a data-toggle="tab" href="#tabRegional">
                                    <asp:Label ID="Label10" runat="server" Text="<%$ Resources: LocalizedResource, Regional %>" /></a></li>
                                <li><a data-toggle="tab" href="#tabCountry">
                                    <asp:Label ID="Label11" runat="server" Text="<%$ Resources: LocalizedResource, ByCountry %>" /></a></li>
                                <li><i onclick="mandatory-nav-scrollLeft();"></i></li>
                            </ul>
                            <i class="fa fa-angle-right pull-left m-arrow-right margin-side-10" onclick='mandatory_scrollLeft(1000);'
                                style="line-height: 42px; display: none"></i>
                        </div>
                        <div class="tab-content">
                            <div id="tabTranscomWide" class="tab-pane fade  in active">
                                <asp:Panel ID="pnlLvRecommendedCoursesForYou" runat="server" ClientIDMode="Static">
                                    <a id="lnkSeeAllMandatoryTW" runat="server" class="orange pull-right underline m-lnk-seeall"
                                        style="margin-top: -9%;">
                                        <asp:Label ID="Label13" runat="server" Text="<%$ Resources: LocalizedResource, SeeAll %>" /></a>
                                    <rad:RadListView ID="lvRecommendedLeadershipCourses" runat="server" AllowPaging="true" PageSize="3" AllowMultiFieldSorting="true">
                                        <LayoutTemplate>
                                            <div id="recommendedLeadershipCoursesListView" class="container-fluid">
                                                <div class="rlcPager row-thumbnail">
                                                    <div class="loader js-rlc-loader"></div>
                                                    <a class="pagePrev navigation-left-center no-underline-hover" href="javascript:void(0);"
                                                        title="Go to previous page">
                                                        <img src="Media/Images/left-navigation-arrow.png" class="hover-button" style="height: 40px;" /></a>
                                                    <a class="pageNext navigation-right-center no-underline-hover" href="javascript:void(0);"
                                                        title="Go to next page">
                                                        <img src="Media/Images/right-navigation-arrow.png" class="hover-button" style="height: 40px;" /></a>
                                                    <div id="lc-items">
                                                    </div>
                                                </div>
                                            </div>
                                        </LayoutTemplate>
                                        <ClientSettings>
                                            <DataBinding ItemPlaceHolderID="lc-items">
                                                <ItemTemplate>
                                                    <div class="col-xs-12 col-sm-6 col-md-4  m-course-container">
                                                        <div id="divRecommendedLeadershipCourses" runat="server" style="color:Black;" title="#= CourseTitle #" >
                                                            <div class="thumbnail">
                                                                <img src='Media/Images/empty.png' data-src='Media/Uploads/CourseImg/#= CourseID #/#= CourseImage #' style="height:120px;width:100%;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;" onerror="this.src='Media/Uploads/CourseImg/No_image.jpg'" class="lazyload"></img>
                                                                            
                                                                <div class="caption">
                                                                    <label class="truncate-line-1" style="font-size: 15px;">#= CourseTitle #</label>
                                                                    <p class="truncate-line-1">#= CourseDuration #</p>
                                                                    <p class="truncate-line-3" title=" #= CourseDuration # "> #= CourseDescription #  </p>
                                                                    <a class="pointer font-small" onclick='confirmLaunchCourse("#= EncryptedCourseID #");return false;'>[See Details]</a>
                                                                    <div class="thumbnail-footer">
                                                                                        
                                                                        <p class="truncate-line-1">#= CourseViews # viewers</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                                <EmptyDataTemplate>
                                                    No Recommended Leadership Course.</p>
                                                </EmptyDataTemplate>
                                                <DataService EnableCaching="true" />
                                            </DataBinding>
                                            <ClientEvents OnCommand="rlcfyOnListViewCommand" OnDataBinding="rlcfyOnListViewDataBinding">
                                            </ClientEvents>
                                        </ClientSettings>
                                    </rad:RadListView>
                                </asp:Panel>
                            </div>
                            <div id="tabProgramSpecific" class="tab-pane fade">
                                <a id="lnkSeeAllMandatoryPS" runat="server" class="orange pull-right underline m-lnk-seeall"
                                    style="margin-top: -9%;">
                                    <asp:Label ID="Label12" runat="server" Text="<%$ Resources: LocalizedResource, SeeAll %>" /></a>
                                <rad:RadListView ID="lvSkillDevelopments" runat="server" AllowPaging="true" PageSize="3">
                                    <LayoutTemplate>
                                        <div id="SkillDevelopmentTakeListView">
                                            <div class="mtPager row-thumbnail">
                                                <div class="loader js-mt-loader"></div>
                                                    <a class="pagePrev navigation-left-center no-underline-hover" href="javascript:void(0);"
                                                        title="Go to previous page">
                                                        <img src="Media/Images/left-navigation-arrow.png" class="hover-button" style="height: 40px;" /></a>
                                                    <a class="pageNext navigation-right-center no-underline-hover" href="javascript:void(0);"
                                                        title="Go to next page">
                                                        <img src="Media/Images/right-navigation-arrow.png" class="hover-button" style="height: 40px;" /></a>
                                                <div id="mtItems">
                                                </div>
                                            </div>
                                        </div>
                                    </LayoutTemplate>
                                    <ClientSettings>
                                        <DataBinding ItemPlaceHolderID="mtItems">
                                            <ItemTemplate>
                                                <div class="col-xs-12 col-sm-6 col-md-4  m-course-container">
                                                        <div id="divSkillDevelopments" runat="server" style="color:Black;" title="#= CourseTitle #" >
                                                            <div class="thumbnail">
                                                                <img src='Media/Images/empty.png' data-src='Media/Uploads/CourseImg/#= CourseID #/#= CourseImage #' style="height:120px;width:100%;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;" onerror="this.src='Media/Uploads/CourseImg/No_image.jpg'" class="lazyload"></img>
                                                                            
                                                                <div class="caption">
                                                                    <label class="truncate-line-1" style="font-size: 15px;">#= CourseTitle #</label>
                                                                    <p class="truncate-line-1">#= CourseDuration #</p>
                                                                    <p class="truncate-line-3" title=" #= CourseDuration # "> #= CourseDescription #  </p>
                                                                    <a class="pointer font-small" onclick='confirmLaunchCourse("#= EncryptedCourseID #");return false;'>[See Details]</a>
                                                                    <div class="thumbnail-footer">
                                                                                        
                                                                        <p class="truncate-line-1">#= CourseViews # viewers</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </ItemTemplate>
                                            <EmptyDataTemplate>
                                                <p style="margin-left: 45px;">No Skill Development Course.</p>
                                            </EmptyDataTemplate>
                                        </DataBinding>
                                        <ClientEvents OnCommand="mtOnListViewCommand" OnDataBinding="mtOnListViewDataBinding">
                                        </ClientEvents>
                                    </ClientSettings>
                                </rad:RadListView>
                            </div>
                            <div id="tabRegional" class="tab-pane fade">
                                <a id="lnkSeeAllMandatoryR" runat="server" class="orange pull-right underline m-lnk-seeall"
                                    style="margin-top: -9%;">
                                    <asp:Label ID="Label14" runat="server" Text="<%$ Resources: LocalizedResource, SeeAll %>" /></a>
                                

                                <rad:RadListView ID="lvRecentlyAddedCourses" runat="server" AllowPaging="true" PageSize="3">
                                    <LayoutTemplate>
                                        <div id="recentListView">
                                            <div class="rcPager row-thumbnail">
                                                <div class="loader js-rc-loader"></div>
                                                <a class="pagePrev navigation-left-center no-underline-hover" href="javascript:void(0);"
                                                title="Go to previous page">
                                                <img src="Media/Images/left-navigation-arrow.png" class="hover-button" style="height: 40px;" /></a>
                                            <a class="pageNext navigation-right-center no-underline-hover" href="javascript:void(0);"
                                                title="Go to next page">
                                                <img src="Media/Images/right-navigation-arrow.png" class="hover-button" style="height: 40px;" /></a>
                                                <div id="rcItems">
                                                </div>
                                            </div>
                                        </div>
                                    </LayoutTemplate>
                                    <ClientSettings>
                                        <DataBinding ItemPlaceHolderID="rcItems">
                                            <ItemTemplate>
                                            <div class="col-xs-12 col-sm-6 col-md-4  m-course-container">
                                            <div id="divRecommendedLeadershipCourses" runat="server" style="color:Black;" title="#= CourseTitle #" >
                                                <div class="thumbnail">
                                                    <img src='Media/Images/empty.png' data-src='Media/Uploads/CourseImg/#= CourseID #/#= CourseImage #' style="height:120px;width:100%;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;" onerror="this.src='Media/Uploads/CourseImg/No_image.jpg'" class="lazyload"></img>
                                                                            
                                                    <div class="caption">
                                                        <label class="truncate-line-1" style="font-size: 15px;">#= CourseTitle #</label>
                                                        <p class="truncate-line-1">#= CourseDuration #</p>
                                                        <p class="truncate-line-3" title=" #= CourseDuration # "> #= CourseDescription #  </p>
                                                        <a class="pointer font-small" onclick='confirmLaunchCourse("#= EncryptedCourseID #");return false;'>[See Details]</a>
                                                        <div class="thumbnail-footer">
                                                                                        
                                                            <p class="truncate-line-1">#= CourseViews # viewers</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                            
                                            </ItemTemplate>
                                            <EmptyDataTemplate>
                                                <p style="margin-left: 45px;">No Recently Added Course.</p>
                                            </EmptyDataTemplate>
                                        </DataBinding>
                                        <ClientEvents OnCommand="rcOnListViewCommand" OnDataBinding="rcOnListViewDataBinding">
                                        </ClientEvents>
                                    </ClientSettings>
                                </rad:RadListView>
                            </div>
                            <div id="tabCountry" class="tab-pane fade">
                                <a id="lnkSeeAllMandatoryC" runat="server" class="orange pull-right underline m-lnk-seeall"
                                    style="margin-top: -9%;">
                                    <asp:Label ID="Label15" runat="server" Text="<%$ Resources: LocalizedResource, SeeAll %>" /></a>

                                <rad:RadListView ID="lvTrendingCourses" runat="server" AllowPaging="true" PageSize="3">
                                    <LayoutTemplate>
                                        <div id="trendingListView">
                                            <div class="tcPager row-thumbnail">
                                                <div class="loader js-ct-loader"></div>
                                                <a class="pagePrev navigation-left-center no-underline-hover" href="javascript:void(0);"
                                                title="Go to previous page">
                                                <img src="Media/Images/left-navigation-arrow.png" class="hover-button" style="height: 40px;" /></a>
                                                <a class="pageNext navigation-right-center no-underline-hover" href="javascript:void(0);"
                                                title="Go to next page">
                                                <img src="Media/Images/right-navigation-arrow.png" class="hover-button" style="height: 40px;" /></a>
                                                <div id="tc-items">
                                                </div>
                                            </div>
                                        </div>
                                    </LayoutTemplate>
                                    <ClientSettings>
                                        <DataBinding ItemPlaceHolderID="tc-items">
                                            <ItemTemplate>
                                                <div class="col-xs-12 col-sm-6 col-md-4  m-course-container">
                                                    <div id="divRecommendedLeadershipCourses" runat="server" style="color:Black;" title="#= CourseTitle #" >
                                                        <div class="thumbnail">
                                                            <img src='Media/Images/empty.png' data-src='Media/Uploads/CourseImg/#= CourseID #/#= CourseImage #' style="height:120px;width:100%;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;" onerror="this.src='Media/Uploads/CourseImg/No_image.jpg'" class="lazyload"></img>
                                                                            
                                                            <div class="caption">
                                                                <label class="truncate-line-1" style="font-size: 15px;">#= CourseTitle #</label>
                                                                <p class="truncate-line-1">#= CourseDuration #</p>
                                                                <p class="truncate-line-3" title=" #= CourseDuration # "> #= CourseDescription #  </p>
                                                                <a class="pointer font-small" onclick='confirmLaunchCourse("#= EncryptedCourseID #");return false;'>[See Details]</a>
                                                                <div class="thumbnail-footer">
                                                                                        
                                                                    <p class="truncate-line-1">#= CourseViews # viewers</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>           
                                            </ItemTemplate>
                                            <EmptyDataTemplate>
                                                <p style="margin-left: 45px;">No Recently Added Course.</p>
                                            </EmptyDataTemplate>
                                        </DataBinding>
                                        <ClientEvents OnCommand="tcOnListViewCommand" OnDataBinding="tcOnListViewDataBinding">
                                        </ClientEvents>
                                    </ClientSettings>
                                </rad:RadListView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-lg-4 m-container-latest-happenings ResponsiveFirstPage">
                <div class="section row " style="height: auto;margin-top: 20px;">
                    <div class="article border-bottom-light-yellow container" style="width: 94%;">
                        <h4 class="orange pull-left" style="margin-left: -17px;">
                            <asp:Label ID="Label17" runat="server" Text="<%$ Resources: LocalizedResource, LatestHappenings %>"
                                Style="color: #c9ae1c;" />
                        </h4>
                        <%--<a href="#" class="orange pull-right underline" style="margin-top: 3%;"><asp:Label ID="Label20" runat="server" Text="<%$ Resources: LocalizedResource, SeeAll %>"  style="color: #c9ae1c;" /></a>--%>
                    </div>
                    <a  href="#" data-toggle="modal" data-target="#TuCalendar">
                    <div class="panel panel-default" style="border-radius: 0;margin: 5px 14px 10px 15px;border-top: 0px;border-left: 0px;border-right: 0px;border-bottom: 1px solid #C9802B;">
                      <div class="panel-heading" style="background-color: transparent; border-color: transparent;padding: 0px 0px 0px 0px;">
                        <h3 class="panel-title" style="font-weight:bold;">T:U Digital Calendar</h3>
                        <label class="font-small" style="font-weight:normal;">Thursday, December 21, 2018</label>
                      </div>
                      <div class="panel-body" style="padding: 10px 0px 10px 0px;">
                        <div class="row">
                            <div class="col-sm-12 col-md-5 col-lg-5">
                                <img src="Media/Uploads/CourseImg/No_image.jpg" data-src="Media/Uploads/CourseImg/2/Lighthouse.jpg" style="height:120px;width:100%;" onerror="this.src='Media/Uploads/CourseImg/No_image.jpg'" class="lazyload">
                            </div>
                            <div class="col-sm-12 col-md-7 col-lg-7 MobileResponsiveContent" style="padding: 0px 0px 0px 0px;">
                                
                                <p class="font-small">The T:U Calendar is a comprehensive guide to all courses, programs, live learning sessions, and curated content offered by T:University on a monthly basis.</p>
                            </div>
                        </div>
                      </div>
                    </div>
                    </a>

                    <a  href="#" data-toggle="modal" data-target="#Launch">
                    <div class="panel panel-default" style="border-radius: 0;margin: 5px 14px 10px 15px;border-top: 0px;border-left: 0px;border-right: 0px;border-bottom: 1px solid #C9802B;"">
                      <div class="panel-heading" style="background-color: transparent; border-color: transparent;padding: 0px 0px 0px 0px;">
                        <h3 class="panel-title" style="font-weight:bold;">TU Launch to be held in January 31</h3>
                        <label class="font-small" style="font-weight:normal;">Thursday, December 31, 2018</label>
                      </div>
                      <div class="panel-body" style="padding: 10px 0px 10px 0px;">
                        <div class="row">
                            <div class="col-sm-12 col-md-5 col-lg-5">
                                <div class="video-container">
                                    <img src="Media/Images/TU_Launch_Thumbnail.jpg" style="width:100%;height:150px;" />
                                    <div class="video-overlay">
                                        <div class="video-title">
                                            <asp:Label ID="Label20" Text="<%$ Resources: LocalizedResource, TranscomUniversityRelaunch %>"
                                                runat="server" CssClass="bold" />
                                            <br />
                                            <i class="fa fa-play fa-white fa-lg" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-7 col-lg-7" style="padding: 0px 0px 0px 0px;">
                                <p class="font-small">The new Transcom University will be introduced to the Global Transcom Community on Thursday, January 31 at 8pm Philippine Time.</p>
                            </div>
                        </div>
                      </div>
                    </div>
                    </a>
                    <a  href="#" data-toggle="modal" data-target="#FeaturedCoursesForJan">
                    <div class="panel panel-default" style="border-radius: 0;margin: 5px 14px 10px 15px;border-top: 0px;border-left: 0px;border-right: 0px;border-bottom: 1px solid #C9802B;">
                      <div class="panel-heading" style="background-color: transparent; border-color: transparent;padding: 0px 0px 0px 0px;">
                        <h3 class="panel-title" style="font-weight:bold;">Featured Courses for January</h3>
                        <label class="font-small" style="font-weight:normal;">Thursday, December 31, 2018</label>
                      </div>
                      <div class="panel-body" style="padding: 10px 0px 10px 0px;">
                        <div class="row">
                            <div class="col-sm-12 col-md-5 col-lg-5">
                                <img src="Media/Uploads/CourseImg/No_image.jpg" data-src="Media/Uploads/CourseImg/2/Lighthouse.jpg" style="height:120px;width:100%;" onerror="this.src='Media/Uploads/CourseImg/No_image.jpg'" class="lazyload">
                            </div>
                            <div class="col-sm-12 col-md-7 col-lg-7" style="padding: 0px 0px 0px 0px;">
                                <p class="font-small">This month's Featured Courses focuses on self-awareness and how one's values can influence leadership style.</p>
                            </div>
                        </div>
                      </div>
                    </div>
                    </a>

                    <a  href="#" data-toggle="modal" data-target="#Webcast">
                    <div class="panel panel-default" style="border-radius: 0;margin: 5px 14px 10px 15px;border-top: 0px;border-left: 0px;border-right: 0px;border-bottom: 1px solid #C9802B;">
                      <div class="panel-heading" style="background-color: transparent; border-color: transparent;padding: 0px 0px 0px 0px;">
                        <h3 class="panel-title" style="font-weight:bold;">Webcast: How to be a Transformation Leader</h3>
                        <label class="font-small" style="font-weight:normal;">Thursday, December 31, 2018</label>
                      </div>
                      <div class="panel-body" style="padding: 10px 0px 10px 0px;">
                        <div class="row">
                            <div class="col-sm-12 col-md-5 col-lg-5">
                                <img src="Media/Uploads/CourseImg/No_image.jpg" data-src="Media/Uploads/CourseImg/2/Lighthouse.jpg" style="height:120px;width:100%;" onerror="this.src='Media/Uploads/CourseImg/No_image.jpg'" class="lazyload">
                            </div>
                            <div class="col-sm-12 col-md-7 col-lg-7" style="padding: 0px 0px 0px 0px;">
                                <p class="font-small">The GER Talent Development Team is hosting a webcast on Leadership on January 25. This event is open to all supervisors and managers.</p>
                            </div>
                        </div>
                      </div>
                    </div>
                    </a>

                    <div class="panel panel-default" style="border-radius: 0;margin: 5px 14px 10px 15px;border-top: 0px;border-left: 0px;border-right: 0px;border-bottom: 1px solid #C9802B;">
                      <div class="panel-heading" style="background-color: transparent; border-color: transparent;padding: 0px 0px 0px 0px;">
                        <h3 class="panel-title" style="font-weight:bold;">Engaging Leader - A Minute with John Maxwell</h3>
                        <label class="font-small" style="font-weight:normal;">Thursday, December 31, 2018</label>
                      </div>
                      <div class="panel-body" style="padding: 10px 0px 10px 0px;">
                        <div class="row">
                            <div class="col-sm-12 col-md-5 col-lg-5">
                                <img src="Media/Uploads/CourseImg/No_image.jpg" data-src="Media/Uploads/CourseImg/2/Lighthouse.jpg" style="height:120px;width:100%;" onerror="this.src='Media/Uploads/CourseImg/No_image.jpg'" class="lazyload">
                            </div>
                            <div class="col-sm-12 col-md-7 col-lg-7" style="padding: 0px 0px 0px 0px;">
                                <p class="font-small">Exceptional leaders have empathy. Their self-awareness allows them to easily see situations from another person's point of view.</p>
                            </div>
                        </div>
                      </div>
                    </div>

                    <%--<div class="container" style="margin-top: 10px;">
                        <div class="video-container">
                            <video class="video video-md" width="100%" height="329" preload="metadata" type="video/mp4">
                                <source src="Media/Videos/Teaser v12.mp4#t=0.5" type="video/mp4">
                                <asp:Label ID="Label16" runat="server" Text="<%$ Resources: LocalizedResource, YourbrowserdoesnotsupportHTML5video %>" />
                            </video>
                            <div class="video-overlay">
                                <div class="video-title">
                                    <asp:Label ID="Label18" Text="<%$ Resources: LocalizedResource, TranscomUniversityRelaunch %>"
                                        runat="server" CssClass="bold" />
                                    <br />
                                    <i class="fa fa-play fa-white fa-lg" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </div>--%>
                    <%--<div class="container" style="margin-top: 5px;">
                        <div class="video-container">
                            <video class="video video-md" width="100%" height="329" preload="metadata" type="video/mp4">
                                <source src="Media/Videos/Teaser for Critical Thinking.mp4#t=0.5" type="video/mp4">
                                <asp:Label ID="Label2" Text="<%$ Resources: LocalizedResource, YourbrowserdoesnotsupportHTML5video %>"
                                    runat="server" CssClass="bold" />
                            </video>
                            <div class="video-overlay">
                                <div class="video-title">
                                    <asp:Label ID="Label45" Text="<%$ Resources: LocalizedResource, WhatisCriticalThinkingPreview %>"
                                        runat="server" CssClass="bold" />
                                    <br />
                                    <i class="fa fa-play fa-white fa-lg" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </div>--%>
                    <%--<div class="container" style="margin-top: 5px;">
                        <div class="col-lg-4 col-xs-4 video-thumbnail no-padding pointer" data-toggle="modal"
                            data-target="#videoModal">
                            <img src="Media/Images/empty.png" data-src="Media/Videos/Thumbnails/vid1_thumbnail.jpeg"
                                alt="Sample Video 0" title="Sample Video 0" class="lazyload" />
                            <asp:HiddenField ID="video_src" runat="server" Value="Media/Videos/Kinesthetic Learners.mp4?" />
                        </div>
                        <div class="col-lg-4 col-xs-4 video-thumbnail no-padding pointer" data-target="#videoModal"
                            data-toggle="modal">
                            <img src="Media/Images/empty.png" data-src="Media/Videos/Thumbnails/vid2_thumbnail.jpeg"
                                alt="Sample Video 1" title="Sample Video 1" class="lazyload" />
                            <asp:HiddenField ID="HiddenField1" runat="server" Value="Media/Videos/Intro to VARK.mp4#t=0.5" />
                        </div>
                        <div class="col-lg-4 col-xs-4 video-thumbnail no-padding pointer" data-target="#videoModal"
                            data-toggle="modal">
                            <img src="Media/Images/empty.png" data-src="Media/Videos/Thumbnails/vid3_thumbnail.jpeg"
                                alt="Sample Video 2" title="Sample Video 2" class="lazyload" />
                            <asp:HiddenField ID="HiddenField2" runat="server" Value="Media/Videos/Visual Learner.mp4#t=0.5" />
                        </div>
                        <div class="col-lg-4 col-xs-4 video-thumbnail no-padding pointer" data-target="#videoModal"
                            data-toggle="modal">
                            <img src="Media/Images/empty.png" data-src="Media/Videos/Thumbnails/vid4_thumbnail.jpeg"
                                alt="Sample Video 3" title="Sample Video 3" class="lazyload" />
                            <asp:HiddenField ID="HiddenField3" runat="server" Value="Media/Videos/ReadingWriting Learners.mp4#t=0.5" />
                        </div>
                        <div class="col-lg-4 col-xs-4 video-thumbnail no-padding pointer" data-target="#videoModal"
                            data-toggle="modal">
                            <img src="Media/Images/empty.png" data-src="Media/Videos/Thumbnails/vid5_thumbnail.jpeg"
                                alt="Sample Video 4" title="Sample Video 4" class="lazyload" />
                            <asp:HiddenField ID="HiddenField4" runat="server" Value="Media/Videos/Brain-Mind Principles .mp4#t=0.5" />
                        </div>
                        <div class="col-lg-4 col-xs-4 video-thumbnail no-padding pointer" data-target="#videoModal"
                            data-toggle="modal">
                            <img src="Media/Images/empty.png" data-src="Media/Videos/Thumbnails/vid6_thumbnail.jpeg"
                                alt="Sample Video 5" title="Sample Video 5" class="lazyload" />
                            <asp:HiddenField ID="HiddenField5" runat="server" Value="Media/Videos/Auditory Learners.mp4#t=0.5" />
                        </div>
                        <div class="col-lg-4 col-xs-4 video-thumbnail no-padding pointer" data-target="#videoModal"
                            data-toggle="modal">
                            <img src="Media/Images/empty.png" data-src="Media/Videos/Thumbnails/vid7_thumbnail.jpeg"
                                alt="Sample Video 6" title="Sample Video 6" class="lazyload" />
                            <asp:HiddenField ID="HiddenField6" runat="server" Value="Media/Videos/Person Typing Fast.mp4#t=0.5" />
                        </div>
                        <div class="col-lg-4 col-xs-4 video-thumbnail no-padding pointer" data-target="#videoModal"
                            data-toggle="modal">
                            <img src="Media/Images/empty.png" data-src="Media/Videos/Thumbnails/vid8_thumbnail.jpeg"
                                alt="Sample Video 7" title="Sample Video 7" class="lazyload" />
                            <asp:HiddenField ID="HiddenField7" runat="server" Value="Media/Videos/People Infront Of Laptops.mp4#t=0.5" />
                        </div>
                        <div class="col-lg-4 col-xs-4 video-thumbnail no-padding pointer" data-target="#videoModal"
                            data-toggle="modal">
                            <img src="Media/Images/empty.png" data-src="Media/Videos/Thumbnails/vid9_thumbnail.jpeg"
                                alt="Sample Video 8" title="Sample Video 8" class="lazyload" />
                            <asp:HiddenField ID="HiddenField8" runat="server" Value="Media/Videos/Preparation For Conference Meeting.mp4#t=0.5" />
                        </div>
                    </div>--%>
                </div>
            </div>
        </asp:Panel>
        <%--<asp:Panel ID="wrapper_signin" runat="server" CssClass="container col-lg-12 bg-transparent wrapper_signin">
            <asp:Panel ID="Panel1" runat="server" CssClass="relative-vr-center z-index-2 text-center col-md-12">
                <h1 class="inner-message white" style="font-size: 40px;">
                    <asp:Label runat="server" Text="<%$ Resources: LocalizedResource, NeedToCheckYourCourses %>" />
                </h1>
                <asp:Button ID="btnSign" Text="<%$ Resources: LocalizedResource, GoToMyDashboard %>" runat="server" CssClass="btn btn-lg btn-transparent btn-gray soft border-white"
                    OnClientClick="gotoLearnerDashboard();return false;" />
            </asp:Panel>
        </asp:Panel>--%>
        <div class="modal fade bd-example-modal-lg" id="TuCalendar" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" data-backdrop="static" data-keyboard="false">
          <div class="modal-dialog-lg modal-lg modal-dialog-centered">
            <div class="modal-content" style="border-radius: 0px;border: 1px solid #000;">
              <div class="modal-header" style="border-bottom: 0px;padding: 10px;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: #000;">&times;</button>
              </div>
              <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <embed src="Media/TUCalendar/TU_H1_Calendar_010419.pdf" type="application/pdf" class="responsivepdf"  height="700px" width="100%">
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal fade bd-example-modal-lg" id="FeaturedCoursesForJan" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" data-backdrop="static" data-keyboard="false">
          <div class="modal-dialog-lg modal-lg modal-dialog-centered">
            <div class="modal-content" style="border-radius: 0px;border: 1px solid #000;">
              <div class="modal-header" style="border-bottom: 0px;padding: 10px;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: #000;">&times;</button>
              </div>
              <div class="modal-body" style="padding: 15px;">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="panel panel-default" style="border-radius: 0;margin: 5px 14px 10px 15px;border-top: 0px;border-left: 0px;border-right: 0px;border-bottom: 0px;">
                          <div class="panel-heading" style="background-color: transparent; border-color: transparent;padding: 0px 0px 0px 0px;">
                            <h3 class="panel-title" style="font-weight:bold;">Featured Courses for January</h3>
                            <label class="font-small" style="font-weight:normal;">Thursday, December 31, 2018</label>
                          </div>
                          <div class="panel-body" style="padding: 10px 0px 10px 0px;">
                            <div class="row">
                                <div class="col-sm-2 col-md-2 col-lg-2">
                                    <img src="Media/Uploads/CourseImg/No_image.jpg" data-src="Media/Uploads/CourseImg/2/Lighthouse.jpg" style="height:140px;width: 100%;" onerror="this.src='Media/Uploads/CourseImg/No_image.jpg'" class="lazyload">
                                </div>
                                <div class="col-sm-10 col-md-10 col-lg-10" style="padding: 0px 0px 0px 0px;">
                                    <p class="font-small">This month's Featured Courses focuses on self-awareness and how one's values can influence leadership style.</p>
                                </div>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <label class="font-small" style="font-weight:normal;margin-left: 15px;">There are 4 Featured Courses for January</label>
                    </div>
                </div>

                <div class="row" style="margin-right: 0px;margin-left: 0px;">
                    <div class="col-xs-12 col-sm-6 col-md-3  m-course-container">
                        <div id="div1" runat="server" style="color:Black;" title="#= CourseTitle #" >
                            <div class="thumbnail">
                                <img src='Media/Images/empty.png' data-src='Media/Uploads/CourseImg/#= CourseID #/#= CourseImage #' style="height:120px;width:100%;" onerror="this.src='Media/Uploads/CourseImg/No_image.jpg'" class="lazyload"></img>
                                                                            
                                <div class="caption">
                                    <label class="truncate-line-1" style="font-size: 15px;">#= CourseTitle #</label>
                                    <p class="truncate-line-1">#= CourseDuration #</p>
                                    <p class="truncate-line-3" title=" #= CourseDuration # "> #= CourseDescription #  </p>
                                    <a class="pointer font-small" onclick='confirmLaunchCourse("#= EncryptedCourseID #");return false;'>[See Details]</a>
                                    <div class="thumbnail-footer">
                                                                                        
                                        <p class="truncate-line-1">#= CourseViews # viewers</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3  m-course-container">
                        <div id="div2" runat="server" style="color:Black;" title="#= CourseTitle #" >
                            <div class="thumbnail">
                                <img src='Media/Images/empty.png' data-src='Media/Uploads/CourseImg/#= CourseID #/#= CourseImage #' style="height:120px;width:100%;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;" onerror="this.src='Media/Uploads/CourseImg/No_image.jpg'" class="lazyload"></img>
                                                                            
                                <div class="caption">
                                    <label class="truncate-line-1" style="font-size: 15px;">#= CourseTitle #</label>
                                    <p class="truncate-line-1">#= CourseDuration #</p>
                                    <p class="truncate-line-3" title=" #= CourseDuration # "> #= CourseDescription #  </p>
                                    <a class="pointer font-small" onclick='confirmLaunchCourse("#= EncryptedCourseID #");return false;'>[See Details]</a>
                                    <div class="thumbnail-footer">
                                                                                        
                                        <p class="truncate-line-1">#= CourseViews # viewers</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3  m-course-container">
                        <div id="div3" runat="server" style="color:Black;" title="#= CourseTitle #" >
                            <div class="thumbnail">
                                <img src='Media/Images/empty.png' data-src='Media/Uploads/CourseImg/#= CourseID #/#= CourseImage #' style="height:120px;width:100%;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;" onerror="this.src='Media/Uploads/CourseImg/No_image.jpg'" class="lazyload"></img>
                                                                            
                                <div class="caption">
                                    <label class="truncate-line-1" style="font-size: 15px;">#= CourseTitle #</label>
                                    <p class="truncate-line-1">#= CourseDuration #</p>
                                    <p class="truncate-line-3" title=" #= CourseDuration # "> #= CourseDescription #  </p>
                                    <a class="pointer font-small" onclick='confirmLaunchCourse("#= EncryptedCourseID #");return false;'>[See Details]</a>
                                    <div class="thumbnail-footer">
                                                                                        
                                        <p class="truncate-line-1">#= CourseViews # viewers</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>  
                    <div class="col-xs-12 col-sm-6 col-md-3  m-course-container">
                        <div id="div4" runat="server" style="color:Black;" title="#= CourseTitle #" >
                            <div class="thumbnail">
                                <img src='Media/Images/empty.png' data-src='Media/Uploads/CourseImg/#= CourseID #/#= CourseImage #' style="height:120px;width:100%;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;" onerror="this.src='Media/Uploads/CourseImg/No_image.jpg'" class="lazyload"></img>
                                                                            
                                <div class="caption">
                                    <label class="truncate-line-1" style="font-size: 15px;">#= CourseTitle #</label>
                                    <p class="truncate-line-1">#= CourseDuration #</p>
                                    <p class="truncate-line-3" title=" #= CourseDuration # "> #= CourseDescription #  </p>
                                    <a class="pointer font-small" onclick='confirmLaunchCourse("#= EncryptedCourseID #");return false;'>[See Details]</a>
                                    <div class="thumbnail-footer">
                                                                                        
                                        <p class="truncate-line-1">#= CourseViews # viewers</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>

              </div>
            </div>
          </div>
        </div>
        <div class="modal fade" id="Webcast" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" data-backdrop="static" data-keyboard="false">
          <div class="modal-dialog modal-dialog-centered"`style="margin: 30px auto!important;">
            <div class="modal-content" style="border-radius: 0px;border: 1px solid #000;width: 700px;max-width: 700px;">
              <div class="modal-header" style="border-bottom: 0px;padding: 10px;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: #000;">&times;</button>
              </div>
              <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="panel panel-default" style="border-radius: 0;margin: 5px 14px 10px 15px;border-top: 0px;border-left: 0px;border-right: 0px;border-bottom: 0px;">
                            <div class="panel-heading" style="background-color: transparent; border-color: transparent;padding: 0px 0px 0px 0px;">
                            <h3 class="panel-title" style="font-weight:bold;">Webcast: How to be a Transformation Leader</h3>
                            <label class="font-small" style="font-weight:normal;">Thursday, December 31, 2018</label>
                            </div>
                            <div class="panel-body" style="padding: 10px 0px 10px 0px;">
                                <div class="row">
                                    <div class="col-sm-4 col-md-4 col-lg-4">
                                        <img src="Media/Uploads/CourseImg/No_image.jpg" data-src="Media/Uploads/CourseImg/2/Lighthouse.jpg" style="height:140px;width: 100%;" onerror="this.src='Media/Uploads/CourseImg/No_image.jpg'" class="lazyload">
                                    </div>
                                    <div class="col-sm-8 col-md-8 col-lg-8" style="padding: 0px 0px 0px 0px;">
                                        <p class="font-small">The GER Talent Development Team is hosting a webcast on Leadership on January 25. This event is open to all supervisors and managers.</p>
                                    </div>
                                </div>
                                <div class="row" style="margin:10px 0px 0px 0px;">
                                    <div class="col-12"><label>Self-Awareness and Mastery to Inspire and Influence Others</label></div>
                                    <div class="col-12"><p class="font-small">Transformational Leadership</p></div>
                                    </br>
                                    <div class="col-12"><label>Objectives:</label></div>
                                    <div class="col-12"><p class="font-small">1. Understand how one's values influence leadership style</p></div>
                                    <div class="col-12"><p class="font-small">2. Discuss the relationship between leadership and followership</p></div>
                                    <div class="col-12"><p class="font-small">3. Appreciate the role of a leader in building a healthy and resilient organisation</p></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal fade bd-example-modal-lg" id="Launch" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" data-backdrop="static" data-keyboard="false">
          <div class="modal-dialog-lg modal-lg modal-dialog-centered">
            <div class="modal-content" style="border-radius: 0px;border: 1px solid #000;">
              <div class="modal-header" style="border-bottom: 0px;padding: 10px;">
                <label>TU Launch to be Held on January 31</label> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: #000;">&times;</button>
              </div>
              <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="video-container">
                            <video class="video video-md" width="100%" height="500px" preload="metadata" type="video/mp4">
                                <source src="Media/Videos/Teaser v12.mp4#t=0.5" type="video/mp4">
                                <asp:Label ID="Label16" runat="server" Text="<%$ Resources: LocalizedResource, YourbrowserdoesnotsupportHTML5video %>" />
                            </video>
                            <div class="video-overlay">
                                <div class="video-title">
                                    <asp:Label ID="Label18" Text="<%$ Resources: LocalizedResource, TranscomUniversityRelaunch %>"
                                        runat="server" CssClass="bold" />
                                    <br />
                                    <i class="fa fa-play fa-white fa-lg" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div id="videoModal" class="modal top-9999">
            <span class="close" data-dismiss="modal">&times;</span>
            <div id="modalVideoContainer">
                <video class="video modal-content btn-flat modal-video" width="466" height="329"
                    preload="metadata" controls style="height: 50vh;" type='video/mp4'>
                    <%-- <source src=""  >--%>
                </video>
            </div>
            <%--<asp:Label ID="Label19" Text="<%$ Resources: LocalizedResource, YourbrowserdoesnotsupportHTML5video %>" runat="server" CssClass="bold" />
            --%>
        </div>
    </asp:Panel>

    <asp:HiddenField ID="hfLeadershipCatID" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hfSkillDevCatID" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hfMandatoryCatID" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnCimNo" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnSubCategoryID" runat="server" ClientIDMode="Static" />
</asp:Content>
