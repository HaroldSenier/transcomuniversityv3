﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TranscomUniversityV3Model;
using System.Data;

public partial class Home2 : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Session["IsNewCourseView"] = true;
            var db = new TranscomUniversityV3ModelContainer();
            int cim = Convert.ToInt32(DataHelper.GetCurrentUserCIM());

            hdnCimNo.Value = cim.ToString();
            hdnSubCategoryID.Value = "28";

            rptLeadership.DataSource = DataHelper.GetLeadershipCourses();
            rptLeadership.DataBind();

            if (rptLeadership.Items.Count == 0)
            {
                //Control FooterTemplate = rptLeadership.Controls[rptLeadership.Controls.Count - 1].Controls[0];
                //FooterTemplate.FindControl("emptyData").Visible = true;
                divLeadership.Visible = false;
            }
            else { divLeadership.Visible = true; }

            //rptSkillDev.DataSource = DataHelper.GetSkillDevelopmentCourses();
            //rptSkillDev.DataBind();

            //if (rptSkillDev.Items.Count == 0)
            //{
            //    //Control FooterTemplate = rptSkillDev.Controls[rptSkillDev.Controls.Count - 1].Controls[0];
            //    //FooterTemplate.FindControl("emptyData").Visible = true;

            //    divSkillDev.Visible = false;
            //}
            //else { divSkillDev.Visible = true; }

            //rptProgram.DataSource = DataHelper.GetMandatoryProgramSpecificCourses(cim, 28);
            //rptProgram.DataBind();

            //if (rptProgram.Items.Count == 0)
            //{
            //    Control FooterTemplate = rptProgram.Controls[rptProgram.Controls.Count - 1].Controls[0];
            //    FooterTemplate.FindControl("emptyData").Visible = true;
            //}

            //rptRegional.DataSource = DataHelper.GetMandatoryRegionalCourses();
            //rptRegional.DataBind();

            //if (rptRegional.Items.Count == 0)
            //{
            //    Control FooterTemplate = rptRegional.Controls[rptRegional.Controls.Count - 1].Controls[0];
            //    FooterTemplate.FindControl("emptyData").Visible = true;
            //}

            int leadershipCatID = 17;
            hfLeadershipCatID.Value = Utils.Encrypt(leadershipCatID);

            //rptCountry.DataSource = DataHelper.GetMandatoryCountryCourses();
            //rptCountry.DataBind();

            //if (rptCountry.Items.Count == 0)
            //{
            //    Control FooterTemplate = rptCountry.Controls[rptCountry.Controls.Count - 1].Controls[0];
            //    FooterTemplate.FindControl("emptyData").Visible = true;
            //}

            //rptTranscomWide.DataSource = DataHelper.GetMandatoryTranscomWideCourses();
            //rptTranscomWide.DataBind();

            //if (rptTranscomWide.Items.Count == 0)
            //{
            //    Control FooterTemplate = rptTranscomWide.Controls[rptTranscomWide.Controls.Count - 1].Controls[0];
            //    FooterTemplate.FindControl("emptyData").Visible = true;
            //}

            string recCourse = Utils.Encrypt(17);
            string skillDev = Utils.Encrypt(18);
            string mandatory = Utils.Encrypt(19);



            //set see all links
            lnkSeeAllLeadership.HRef = "CourseCatalog.aspx?Tab=Leadership&Id=" + recCourse + "&view=MyRecommended";
            lnkSeeAllSkillDev.HRef = "CourseCatalog.aspx?Tab=Skill Development&Id=" + skillDev + "&view=SeeAll";
            lnkSeeAllMandatory.HRef = "CourseCatalog.aspx?Tab=Mandatory&Id=" + mandatory + "&view=SeeAll";
            lnkSeeAllMandatoryC.HRef = "CourseCatalog.aspx?Tab=Mandatory&Id=" + mandatory + "&view=SeeAll&Mand=31";
            lnkSeeAllMandatoryPS.HRef = "CourseCatalog.aspx?Tab=Mandatory&Id=" + mandatory + "&view=SeeAll&Mand=28";
            lnkSeeAllMandatoryR.HRef = "CourseCatalog.aspx?Tab=Mandatory&Id=" + mandatory + "&view=SeeAll&Mand=30";
            lnkSeeAllMandatoryTW.HRef = "CourseCatalog.aspx?Tab=Mandatory&Id=" + mandatory + "&view=SeeAll&Mand=29";



        }
    }
}