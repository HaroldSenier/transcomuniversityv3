$(document).ready(function () {
    //$('.bg-color-0D9E9E').parent().addClass('progress-black');

    var open = $('.open-nav'),
				close = $('.close'),
				overlay = $('.overlay');

    open.click(function () {
        overlay.show();
        $('.side-menu-container').addClass('toggled');
    });

    close.click(function () {
        overlay.hide();
        $('.side-menu-container').removeClass('toggled');
    });

    $("#myModal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));

    });

    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

    elems.forEach(function (html) {
        new Switchery(html, {
            size: 'small',
            color: '#7C7C7C',
            secondaryColor: '#7C7C7C',
            jackColor: '#009999',
            jackSecondaryColor: '#ffffff'
        });
    });

    var elems2 = Array.prototype.slice.call(document.querySelectorAll('.js-switch2'));

    elems2.forEach(function (html) {
        new Switchery(html, {
            size: 'small',
            color: '#FFFFFF',
            secondaryColor: '#FFFFFF', //off
            jackColor: '#09A8A4', //on
            jackSecondaryColor: '#BBBBBB' //off
        });
    });



});



$(document).on('change', ':file', function () {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
});

// We can watch for our custom `fileselect` event like this
$(document).ready(function () {
    $(':file').on('fileselect', function (event, numFiles, label) {

        var input = $(this).parents('.input-group').find(':text'),
              log = numFiles > 1 ? numFiles + ' files selected' : label;

        if (input.length) {
            input.val(log);
        } else {
            //if (log) alert(log);
        }

    });
});

function convertTimeZone(strDateTime) {

    var d = new Date(strDateTime);
    var offset = new Date().getTimezoneOffset();
    //d.setHours(d.getHours() - (offset / 60));
    //d.setHours(d.getHours() + (d.getHours() - (offset / 60)));
    d.setHours(d.getHours() - (-7 + (offset / 60)));
    var dateNow = new Date();
    
    d.setMinutes(d.getMinutes() - (offset % 60));

    return d;
}

function getWeekDay(date) {
    var d = new Date(date);
    var day = d.getDay();

    var weekDays = [ 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday' ];
    
    return weekDays[day];
}




/*$(document).ready(function () {
$('.popoverx').click({
placement: 'top'
});
});*/
