﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LandingPage.aspx.cs" Inherits="LandingPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" />
    <meta http-equiv="X-UA-Compatible" content="IE=8" />
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <meta http-equiv="X-UA-Compatible" content="IE=7" />
    <meta http-equiv="X-UA-Compatible" content="IE=5" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Transcom University</title>
    <link rel="icon" type="image/png" href="Media/Images/favicon.png" />
    <link href="Font-Awesome/CSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Cabin|Open+Sans|Roboto" rel="stylesheet" />
    <link href="CSS/animate.css" rel="stylesheet" type="text/css" />
    <link href="CSS/bootstrap4.1.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div style="border-top: 3px solid #cd2d2d; top: 0;">
    </div>
    <div class="container-fluid">
        <div class="row mt-2 mb-2">
            <div class="col-12">
                <ul class="nav justify-content-end">
                    <li class="nav-item">
                        <label runat="server" id="Language" style="color: #cd2d2d;">
                            <asp:Label ID="Label2" runat="server" Font-Bold="true" Text="<%$ Resources: LocalizedResource, SelectLanguage %>" /></label>
                    </li>
                    <li class="nav-item mr-5">
                        <div class="btn-group">
                            <div class="btn-group dropleft" role="group">
                                <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                                    aria-expanded="false" style="color: #cd2d2d;">
                                    <i class="fa fa-language" aria-hidden="true" style="font-size: 20px;"></i>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <div class="card" style="border: 3px solid #cd2d2d; width: 250px;">
                                        <h5 class="card-header" style="background: #cd2d2d; color: #fff;">
                                            <i class="fa fa-language mr-2" aria-hidden="true" style="font-size: 20px;"></i><asp:Label ID="Label1" runat="server" Font-Bold="true" Text="<%$ Resources: LocalizedResource, SelectLanguage %>" /></h5>
                                        <div class="card-body">
                                            <asp:LinkButton runat="server" ID="lnkEnglish" OnClick="lnkLanguage_Click" Text="English" CssClass="dropdown-item"></asp:LinkButton>
                                            <asp:LinkButton runat="server" ID="lnkGerman" OnClick="lnkLanguage_Click" Text="German" CssClass="dropdown-item"></asp:LinkButton>
                                            <asp:LinkButton runat="server" ID="lnkItalian" OnClick="lnkLanguage_Click" Text="Italian" CssClass="dropdown-item"></asp:LinkButton>
                                            <asp:LinkButton runat="server" ID="lnkFrench" OnClick="lnkLanguage_Click" Text="French" CssClass="dropdown-item"></asp:LinkButton>
                                            <asp:LinkButton runat="server" ID="lnkSpanish" OnClick="lnkLanguage_Click" Text="Spanish" CssClass="dropdown-item"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="nav-item mr-5">
                        <asp:LinkButton runat="server" ID="lnkSignIn" OnClick="lnkSignIn_Click" Style="color: #cd2d2d;" Text="<%$ Resources: LocalizedResource, Signin %>"></asp:LinkButton>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container" style="margin-top: 150px;">
        <div id="carouselExampleSlidesOnly" class="carousel slide d-none" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item text-center active">
                    <img src="Media/Images/1.png" alt="First slide" />
                </div>
                <div class="carousel-item text-center">
                    <img src="Media/Images/2.png" alt="Second slide">
                </div>
                <div class="carousel-item text-center">
                    <img src="Media/Images/3.png" alt="Third slide">
                </div>
            </div>
        </div>
        <div id="MarqueeSlider" class="Marquee">
            <div class="Marquee-content">
                <div class="Marquee-tag">
                    <img src="Media/Images/1.png" /></div>
                <div class="Marquee-tag">
                    <img src="Media/Images/2.png" /></div>
                <div class="Marquee-tag">
                    <img src="Media/Images/3.png" /></div>
                <div class="Marquee-tag">
                    <img src="Media/Images/1.png" /></div>
                <div class="Marquee-tag">
                    <img src="Media/Images/2.png" /></div>
                <div class="Marquee-tag">
                    <img src="Media/Images/3.png" /></div>
            </div>
        </div>
    </div>
    <div class="container" style="margin-bottom: 200px;">
        <div class="row mt-5 mb-5">
            <div class="col-12 text-center">
                <img src="Media/Images/Logo2.png" style="height: 100px;" />
            </div>
            <div class="col-12">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" style="background-color: transparent; border: 0; border-bottom: 3px solid #d45858;
                                border-radius: 0;"><i class="fa fa-search" aria-hidden="true" style="font-size: 20px;">
                                </i></span>
                        </div>
                        <asp:TextBox ID="txtSearch" runat="server" placeholder="<%$ Resources: LocalizedResource, SearchForACourse %>" CssClass="form-control"
                            onkeypress="return enterPressed(event)"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer">
        <div class="container-fluid bottom_border">
            <div class="row align-items-center" style="height: 48px;">
                <div class="col-12">
                    <nav aria-label="breadcrumb align-middle">
                        <ol class="breadcrumb" style="padding-bottom: 0px;font-size:0.8em;">
                            <%--<li class="breadcrumb-item"><a href="#" runat="server" id="FooterPolicy"><asp:Label ID="Label3" runat="server" Font-Bold="true" Text="<%$ Resources: LocalizedResource, Policy %>" /></a></li>--%>
                            <li class="breadcrumb-item"><a href="#" runat="server" id="FooterAboutUs" data-toggle="modal" data-target="#AboutUs"><asp:Label ID="Label4" runat="server" Font-Bold="true" Text="<%$ Resources: LocalizedResource, AboutUs %>" /></a></li>
                            <li class="breadcrumb-item"><a href="#" runat="server" id="FooterContactUs"><asp:Label ID="Label5" runat="server" Font-Bold="true" Text="<%$ Resources: LocalizedResource, ContactUs %>" /></a></li>
                            <li class="breadcrumb-item"><a href="#" runat="server" id="FooterFAQ"><asp:Label ID="Label6" runat="server" Font-Bold="true" Text="<%$ Resources: LocalizedResource, Faqs %>" /></a></li>
                            <li class="breadcrumb-item"><a href="#" runat="server" id="FooterSiteMap"><asp:Label ID="Label7" runat="server" Font-Bold="true" Text="<%$ Resources: LocalizedResource, SiteMap %>" /></a></li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="row align-items-center" style="border-top: 1px #b5b5b5 solid;">
                <div class="col-6" style="padding-top: 9px;">
                    <p class="pb-10" style="margin-left: 25px;" runat="server" id="FooterCopyright"><asp:Label ID="Label8" runat="server" Font-Bold="true" Text="<%$ Resources: LocalizedResource, Copyright2018AllRightsReservedTranscomPhilippines %>" /></p>
                </div>
                <div class="col-6 text-right" style="padding-top: 7px;">
                    <label style="color: #ffffff; font-size: 36px; font-family: 'Roboto'; font-weight: bold;">
                        <asp:Label ID="Label9" runat="server" Font-Bold="true" Text="<%$ Resources: LocalizedResource, Transcom %>" /></label>
                </div>
            </div>
        </div>
        <asp:HiddenField runat="server" ID="TimezoneOffset" />
    </footer>

    <%--<div class="modal fade" id="AboutUs" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered"`style="margin: 30px auto!important;">
            <div class="modal-content" style="border-radius: 0px;border: 1px solid #000;width: 700px;max-width: 700px;">
                <div class="modal-header" style="border-bottom: 0px;padding: 10px;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: #000;">&times;</button>
                </div>
                <div class="modal-body">
                <div class="row" style="margin: 25px 25px 25px 25px;">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <label style="color: #000;">T: About Transcom University</label>
                        <p style="color: #000;font-weight: normal;">To stay ahead in today's fast-paced business environment, you need to have the right skill-set. More than a portal for compliance training, <label style="color:#EC3E33;">T:University</label> provides you access to learning experiences that help you to be successful in your current role and advance your career.
                        From Call Handling Skills to Performance Management, <label style="color:#EC3E33;">T:University</label> strives to contact all employees to relevant, on-demand and just-in-time content.</p>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>--%>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="JS/jquery-3.3.1.js" type="text/javascript"></script>
    <script src="JS/popper.js" type="text/javascript"></script>
    <script src="JS/bootstrap4.1.js" type="text/javascript"></script>
    <script src="JS/DetectBrowser.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(window).on('load', function () {
            //Bowser Name - Detecting What Kind of Browser Currently Using
            FullLoader(bowser.name);
        });

        function FullLoader(BrowserName) {
            var GetBrowserName = BrowserName;
            setTimeout(removeLoader(GetBrowserName), 2000);
        }

        function removeLoader(BrowserName) {
            var GetBrowserName = BrowserName;
            //If Browser Does Not Support This Code Then Use If Statement
            $("#mainLoading").fadeOut(500, function () {
                $('#mainLoading').css('display', 'none');
            });
        }

        function enterPressed(e) {
            if (e.keyCode == 13) {
                var q = document.getElementById("txtSearch").value;
                window.location.href = "SearchResult.aspx?q=" + q;
                return false;
            }
        }

        function pageLoad() {
            var d = new Date();

            $("#<%= TimezoneOffset.ClientID %>").val(d.getTimezoneOffset());

            if (bowser.name == "Internet Explorer") {
                $('#MarqueeSlider').addClass("d-none");
                
                $('#carouselExampleSlidesOnly').removeClass("d-none");
                $('#carouselExampleSlidesOnly').carousel({interval: 1000, cycle: true});
            } else {
                $('#MarqueeSlider').removeClass("d-none");
                $('#carouselExampleSlidesOnly').addClass("d-none");
            }
        }
    </script>
    </form>
</body>
</html>
