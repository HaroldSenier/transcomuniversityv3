﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Membership.OpenAuth;
using System.Globalization;

public partial class LandingPage : BasePage
{
    public string ReturnUrl { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        ReturnUrl = Request.QueryString["ReturnUrl"];

        bool isAuth = (System.Web.HttpContext.Current.User != null) && System.Web.HttpContext.Current.User.Identity.IsAuthenticated;

        if (isAuth)
            Response.Redirect("Home.aspx");
        if (!IsPostBack)
        {
            try
            {
                String CurrLang = Session["CurrLang"].ToString();
                GetCurrentLanguage(CurrLang);
                
            }
            catch(Exception ex){}
        }
    }

    private void GetCurrentLanguage(string CurrLang)
    {
        switch (CurrLang)
        {
            case "en-US":
                lnkEnglish.CssClass = "dropdown-item active";
                break;

            case "de-DE":
                lnkGerman.CssClass = "dropdown-item active";
                break;
            case "it-IT":
                lnkItalian.CssClass = "dropdown-item active";
                break;
            case "es-MX":
                lnkSpanish.CssClass = "dropdown-item active";
                break;
            case "fr-FR":
                lnkFrench.CssClass = "dropdown-item active";
                break;
            default:
                lnkEnglish.CssClass = "dropdown-item active";
                break;
        }
    }

    private void GetLang()
    {
        #region HEADER
        lnkSignIn.Text = Resources.LocalizedResource.ContactUs.ToString();
        #endregion

        #region FOOTER
        //FooterPolicy.InnerText = Resources.LocalizedResource.Policy.ToString();
        FooterAboutUs.InnerText = Resources.LocalizedResource.AboutUs.ToString();
        FooterContactUs.InnerText = Resources.LocalizedResource.ContactUs.ToString();
        FooterFAQ.InnerText = Resources.LocalizedResource.Faqs.ToString();
        FooterSiteMap.InnerText = Resources.LocalizedResource.SiteMap.ToString();

        FooterCopyright.InnerText = Resources.LocalizedResource.Transcom.ToString();
        #endregion

        //var asd = Resources.LocalizedResource.
        //Course successfully saved
        //    <%$ Resources:LocalizedResource, Coursesuccessfullysaved%>
        //<asp:Label runat="server" ID="label1" Text=" <%$ Resources:LocalizedResource, PlaylistName%>" ></asp:Label>
            
    }

    protected void lnkSignIn_Click(object sender, EventArgs e)
    {

        Session["TimezoneOffset"] = TimezoneOffset.Value;

        var redirectUrl = "~/ExternalLandingPage.aspx";

        if (!String.IsNullOrEmpty(ReturnUrl))
        {
            var resolvedReturnUrl = ResolveUrl(ReturnUrl);
            redirectUrl += "?ReturnUrl=" + HttpUtility.UrlEncode(resolvedReturnUrl);
        }

        OpenAuth.RequestAuthentication("google", redirectUrl);
    }
    protected void lnkLanguage_Click(object sender, EventArgs e)
    {
        LinkButton Language = (LinkButton)sender;
        string SelectedLanguage = "";
        switch (Language.Text)
        {
            case "English":
                SelectedLanguage = "en-US";
                break;

            case "German":
                SelectedLanguage = "de-DE";
                break;
            case "Italian":
                SelectedLanguage = "it-IT";
                break;
            case "Spanish":
                SelectedLanguage = "es-MX";
                break;
            case "French":
                SelectedLanguage = "fr-FR";
                break;
            default:
                SelectedLanguage = "en-US";
                break;
        }
        Session["CurrLang"] = SelectedLanguage;
        Response.Redirect(Request.Path);
    }


}