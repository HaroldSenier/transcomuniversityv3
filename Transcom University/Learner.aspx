﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TranscomUniversityV3.Master" AutoEventWireup="true"
    CodeFile="Learner.aspx.cs" Inherits="Learner" %>

<%@ Register TagName="TakeLaterCourses" TagPrefix="ucTakeLaterCourses" Src="UserControl/Pages/TakeLaterCoursesUserCtrl.ascx" %>
<%@ Register TagName="MyCalendar" TagPrefix="ucMyCalendar" Src="UserControl/Pages/LearnerCalendar.ascx" %>
<%@ Register TagName="EnrolledCourses" TagPrefix="ucEnrolledCourses" Src="UserControl/Pages/EnrolledCoursesUserCtrl.ascx" %>
<%@ Register TagName="CompletedCourses" TagPrefix="ucCompletedCourses" Src="UserControl/Pages/CompletedCoursesUserCtrl.ascx" %>
<%@ Register TagName="InProgressCourses" TagPrefix="ucInProgressCourses" Src="UserControl/Pages/InProgressCoursesUserCtrl.ascx" %>
<%@ Register TagName="OverdueCourses" TagPrefix="ucOverdueCourses" Src="UserControl/Pages/OverdueCoursesUserCtrl.ascx" %>
<%@ Register TagName="NotYetStartedCourses" TagPrefix="ucNotYetStartedCourses" Src="UserControl/Pages/NotYetStartedCoursesUserCtrl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="contentPlaceHolderLeftPanel" runat="Server">
    <rad:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Transparency="25"
        IsSticky="true" CssClass="Loading" />
    <rad:RadWindowManager ID="RadWindowManager1" RenderMode="Lightweight" EnableShadow="true"
        Modal="true" VisibleOnPageLoad="false" Behaviors="Close, Move" DestroyOnClose="true"
        RestrictionZoneID="RestrictionZone" Opacity="99" runat="server" Width="800" Height="400px"
        Skin="Bootstrap">
    </rad:RadWindowManager>
    <rad:RadAjaxManager ID="RadAjaxManager1" runat="server" DefaultLoadingPanelID="RadAjaxLoadingPanel1">
        <ClientEvents OnRequestStart="RequestStart" />
        <AjaxSettings>
            <%--            <rad:AjaxSetting AjaxControlID="btnEnrolledCourses">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="ulBreadcrumb" />
                    <rad:AjaxUpdatedControl ControlID="lblBreadcrumbs" />
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage2" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnCompletedCourses">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="ulBreadcrumb" />
                    <rad:AjaxUpdatedControl ControlID="lblBreadcrumbs" />
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage2" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnInProgress">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="ulBreadcrumb" />
                    <rad:AjaxUpdatedControl ControlID="lblBreadcrumbs" />
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage2" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnOverdueCourses">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="ulBreadcrumb" />
                    <rad:AjaxUpdatedControl ControlID="lblBreadcrumbs" />
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage2" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnNotyetStarted">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="ulBreadcrumb" />
                    <rad:AjaxUpdatedControl ControlID="lblBreadcrumbs" />
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage2" />
                </UpdatedControls>
            </rad:AjaxSetting>--%>
        </AjaxSettings>
    </rad:RadAjaxManager>
    <rad:RadScriptBlock ID="RadScriptBlock1" runat="server">
        <script type="text/javascript">
            function RequestStart(sender, args) {
                // the following Javascript code takes care of expanding the RadAjaxLoadingPanel
                // to the full height of the page, if it is more than the browser window viewport

                var loadingPanel = $get("<%= RadAjaxLoadingPanel1.ClientID %>");
                var pageHeight = document.documentElement.scrollHeight;
                var viewportHeight = document.documentElement.clientHeight;

                if (pageHeight > viewportHeight) {
                    loadingPanel.style.height = pageHeight + "px";
                }

                var pageWidth = document.documentElement.scrollWidth;
                var viewportWidth = document.documentElement.clientWidth;

                if (pageWidth > viewportWidth) {
                    loadingPanel.style.width = pageWidth + "px";
                }

                // the following Javascript code takes care of centering the RadAjaxLoadingPanel
                // background image, taking into consideration the scroll offset of the page content

                if ($telerik.isSafari) {
                    var scrollTopOffset = document.body.scrollTop;
                    var scrollLeftOffset = document.body.scrollLeft;
                }
                else {
                    var scrollTopOffset = document.documentElement.scrollTop;
                    var scrollLeftOffset = document.documentElement.scrollLeft;
                }

                var loadingImageWidth = 55;
                var loadingImageHeight = 55;

                loadingPanel.style.backgroundPosition = (parseInt(scrollLeftOffset) + parseInt(viewportWidth / 2) - parseInt(loadingImageWidth / 2)) + "px " + (parseInt(scrollTopOffset) + parseInt(viewportHeight / 2) - parseInt(loadingImageHeight / 2)) + "px";
            }
        </script>
        <script type="text/javascript" language="javascript">
            function onClientTabSelected(sender, args) {
                if (sender.get_selectedTab().get_text() == "My Calendar") {
                    var href = sender.get_selectedTab().get_navigateUrl();
                    var tz = $("#hfEncryptedOffset").val();
                    sender.get_selectedTab().set_navigateUrl(href + "&tz=" + tz);
                }

                $(document).ready(function () {
                    $('.close').click();
                });
            }
        </script>
    </rad:RadScriptBlock>
    <div class="sidebar-relative">
        <div class="side-menu-container">
            <div class="overlay">
            </div>
            <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
                <ul class="nav sidebar-nav">
                    <li class="sidebar-brand" style="z-index: 1000; color: #fff;">
                        <a href="#" class="close"><i class="fa fa-times" aria-hidden="true"></i></a> <asp:Label runat="server" Text="<%$ Resources: LocalizedResource, LearnerDashboard %>" />
                    </li>
                    <rad:RadTabStrip ID="RadTabStrip1" runat="server" RenderMode="Lightweight" Skin="Black" AutoPostBack="true" OnClientTabSelected="onClientTabSelected">
                        <Tabs>
                            <rad:RadTab runat="server" Text="<%$ Resources: LocalizedResource, MyCalendar %>" Value="1" NavigateUrl="Learner.aspx?Tab=myCalendar" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                            <rad:RadTab runat="server" Text="<%$ Resources: LocalizedResource, TakeLaterCourses %>" Value="2" NavigateUrl="Learner.aspx?Tab=takeLaterCourses" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                        </Tabs>
                    </rad:RadTabStrip>
                </ul>

                <div style="margin-top:100%;position:relative;">
                <rad:RadCalendar RenderMode="Lightweight" runat="server" ID="RadCalendar1" EnableMultiSelect="false"
                    DayNameFormat="FirstTwoLetters" EnableNavigation="true" Visible="false" 
                    EnableMonthYearFastNavigation="true" Skin="Metro" CssClass="Calendar1" >
                    <%--<ClientEvents OnDateSelected="OnCalendar1DateSelected" OnCalendarViewChanged="OnCalendar1ViewChanged" />--%>
                       <SpecialDays>
                            <rad:RadCalendarDay Repeatable="Today">
                                <ItemStyle CssClass="rcToday" />
                            </rad:RadCalendarDay>
                        </SpecialDays>
                </rad:RadCalendar>
                </div>

            </nav>
            <div class="hamburger-container text-center" style="font-size: 20px; color: #fff;
                margin-top: 10px">
                <a href="#" class="open-nav is-closed animated fadeInLeft"><i class="fa fa-bars"
                    aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolderMain" runat="Server">
    <div class="col-sm-11 col-md-12 bottom-margin-md">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12 thirty-px-padding">
                        <div class="col-md-12 no-paddings">
                            <ul id="ulBreadcrumb" runat="server" class="breadcrumb">
                                <li><a>
                                    <asp:Label ID="lblTab1" runat="server" Text="<%$ Resources: LocalizedResource, Learner %>" /></a></li>
                                <li>
                                    <asp:Label ID="lblTab2" runat="server" Text="<%$ Resources: LocalizedResource, EnrolledCourses %>" /></li>
                            </ul>
                        </div>
                        <rad:RadMultiPage ID="RadMultiPage1" runat="server" RenderMode="Lightweight" CssClass="outerMultiPage"
                            RenderSelectedPageOnly="true">
                            <%-- <rad:RadPageView ID="RadPageView1" runat="server">
                                <br />
                                <br />
                                <br />
                                <ucMyLearningPlan:MyLearningPlan ID="MyLearningPlan1" runat="server" />
                            </rad:RadPageView>
                            <rad:RadPageView ID="RadPageView2" runat="server">
                                <br />
                                <br />
                                <br />
                                <ucMyTranscripts:MyTranscripts ID="MyTranscripts" runat="server" />
                            </rad:RadPageView>--%>
                            <rad:RadPageView ID="RadPageView3" runat="server">
                                <br />
                                <br />
                                <br />
                                <ucMyCalendar:MyCalendar ID="MyCalendar1" runat="server" />
                            </rad:RadPageView>
                            <rad:RadPageView ID="RadPageView4" runat="server">
                                <br />
                                <br />
                                <br />
                                <ucTakeLaterCourses:TakeLaterCourses ID="TakeLaterCourses1" runat="server" />
                            </rad:RadPageView>
                        </rad:RadMultiPage>
                        <asp:Panel ID="pnlLearnerDashboard" runat="server" Style="width: 90%; height: 100%;
                            display: inline-block; margin: 1% 5%;">
                            <div class="row text-center">
                                <div style="width: 18%; display: inline-block; text-align: left;">
                                    <div class="big-menu-dashboard pointer" title="#= Resources: LocalizedResource, EnrolledCourses #"
                                        onclick="document.getElementById('<%= btnEnrolledCourses.ClientID %>').click()">
                                        <div class="icon-holder">
                                            <div class="col-xs-6 counter text-center">
                                                <asp:Literal ID="ltEnrolledCourses" runat="server"></asp:Literal>
                                            </div>
                                            <div class="col-xs-6 icon-glyph text-center">
                                                <span class="fa fa-graduation-cap" aria-hidden="true"></span>
                                            </div>
                                        </div>
                                        <p id="anchorEnrolledCourses">
                                            Enrolled Courses
                                        </p>
                                    </div>
                                    <asp:HyperLink ID="btnEnrolledCourses" runat="server" NavigateUrl="Learner.aspx?Tab=enrolledCourses"></asp:HyperLink>
                                </div>
                                <div style="width: 18%; display: inline-block; text-align: left;">
                                    <div class="big-menu-dashboard pointer" title="#= Resources: LocalizedResource, CompletedCourses #"
                                        onclick="document.getElementById('<%= btnCompletedCourses.ClientID %>').click()">
                                        <div class="icon-holder">
                                            <div class="col-xs-6 counter text-center">
                                                <asp:Literal ID="ltCompletedCourses" runat="server"></asp:Literal>
                                            </div>
                                            <div class="col-xs-6 icon-glyph text-center">
                                                <span class="fa fa-check" aria-hidden="true"></span>
                                            </div>
                                        </div>
                                        <p>
                                            <asp:Label runat="server" Text="<%$ Resources: LocalizedResource, CompletedCourses %>" />
                                        </p>
                                    </div>
                                    <asp:HyperLink ID="btnCompletedCourses" runat="server" NavigateUrl="Learner.aspx?Tab=completedCourses"></asp:HyperLink>
                                </div>
                                <div style="width: 18%; display: inline-block; text-align: left;">
                                    <div class="big-menu-dashboard pointer" title="#= Resources: LocalizedResource, Inprogress #"
                                        onclick="document.getElementById('<%= btnInProgressCourses.ClientID %>').click()">
                                        <div class="icon-holder">
                                            <div class="col-xs-6 counter text-center">
                                                <asp:Literal ID="ltInProgressCourses" runat="server"></asp:Literal>
                                            </div>
                                            <div class="col-xs-6 icon-glyph text-center">
                                                <span class="fa fa-trophy" aria-hidden="true"></span>
                                            </div>
                                        </div>
                                        <p>
                                            <asp:Label runat="server" Text="<%$ Resources: LocalizedResource, Inprogress %>" />
                                        </p>
                                    </div>
                                    <asp:HyperLink ID="btnInProgressCourses" runat="server" NavigateUrl="Learner.aspx?Tab=inProgressCourses"></asp:HyperLink>
                                </div>
                                <div style="width: 18%; display: inline-block; text-align: left;">
                                    <div class="big-menu-dashboard pointer" title="#= Resources: LocalizedResource, OverdueCourses #"
                                        onclick="document.getElementById('<%= btnOverdueCourses.ClientID %>').click()">
                                        <div class="icon-holder">
                                            <div class="col-xs-6 counter text-center">
                                                <asp:Literal ID="ltOverdueCourses" runat="server"></asp:Literal>
                                            </div>
                                            <div class="col-xs-6 icon-glyph text-center">
                                                <span class="fa fa-clock-o" aria-hidden="true"></span>
                                            </div>
                                        </div>
                                        <p>
                                            <asp:Label runat="server" Text="<%$ Resources: LocalizedResource, OverdueCourses %>" />
                                        </p>
                                    </div>
                                    <asp:HyperLink ID="btnOverdueCourses" runat="server" NavigateUrl="Learner.aspx?Tab=overdueCourses"></asp:HyperLink>
                                </div>
                                <div style="width: 18%; display: inline-block; text-align: left;">
                                    <div class="big-menu-dashboard pointer" title="#= Resources: LocalizedResource, NotYetStarted #"
                                        onclick="document.getElementById('<%= btnNotyetStartedCourses.ClientID %>').click()">
                                        <div class="icon-holder">
                                            <div class="col-xs-4 counter text-center">
                                                <asp:Literal ID="ltNotYetStartedCourses" runat="server"></asp:Literal>
                                            </div>
                                            <div class="col-xs-6 icon-glyph text-center">
                                                <span class="fa fa-hourglass-o" aria-hidden="true"></span>
                                            </div>
                                        </div>
                                        <p>
                                            <asp:Label runat="server" Text="<%$ Resources: LocalizedResource, NotYetStarted %>" />
                                        </p>
                                    </div>
                                    <asp:HyperLink ID="btnNotyetStartedCourses" runat="server" NavigateUrl="Learner.aspx?Tab=notYetStartedCourses"></asp:HyperLink>
                                </div>
                                <div class="widgets dark-gray-header" style="margin-top: 5px; width: 91.256%; display: inline-block;
                                    text-align: left; text-indent: 5px;">
                                    <div class="text-transform-uppercase" style="padding: 5px 0;">
                                        <rad:RadCodeBlock ID="RadCodeBlock1" runat="server">
                                            <asp:Label ID="lblBreadcrumbs" runat="server" Text="<%$ Resources: LocalizedResource, EnrolledCourses %>"></asp:Label>
                                        </rad:RadCodeBlock>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                        <rad:RadMultiPage ID="RadMultiPage2" runat="server" RenderMode="Lightweight" SelectedIndex="0"
                            CssClas="outerMultiPage" RenderSelectedPageOnly="true">
                            <rad:RadPageView ID="RadPageView5" runat="server">
                                <ucEnrolledCourses:EnrolledCourses ID="EnrolledCourses" runat="server" />
                            </rad:RadPageView>
                            <rad:RadPageView ID="RadPageView6" runat="server">
                                <ucCompletedCourses:CompletedCourses ID="CompletedCourses" runat="server" />
                            </rad:RadPageView>
                            <rad:RadPageView ID="RadPageView7" runat="server">
                                <ucInProgressCourses:InProgressCourses ID="InProgressCourses" runat="server" />
                            </rad:RadPageView>
                            <rad:RadPageView ID="RadPageView8" runat="server">
                                <ucOverdueCourses:OverdueCourses ID="OverdueCourses" runat="server" />
                            </rad:RadPageView>
                            <rad:RadPageView ID="RadPageView9" runat="server">
                                <ucNotYetStartedCourses:NotYetStartedCourses ID="NotYetStartedCourses" runat="server" />
                            </rad:RadPageView>
                        </rad:RadMultiPage>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
