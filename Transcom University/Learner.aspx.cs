﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;
using System.Web.Configuration;
using System.Data.SqlClient;
using Telerik.Web.UI.GridExcelBuilder;
using TranscomUniversityV3Model;

public class TakeLaterCourseData
{
    public int count { get; set; }
    public IEnumerable<dynamic> data { get; set; }
}

public class EnrolledCoursesData
{
    public int count { get; set; }
    public IEnumerable<dynamic> data { get; set; }
}

public class CompletedCoursesData
{
    public int count { get; set; }
    public IEnumerable<dynamic> data { get; set; }
}

public class InProgressCoursesData
{
    public int count { get; set; }
    public IEnumerable<dynamic> data { get; set; }
}

public class OverdueCoursesData
{
    public int count { get; set; }
    public IEnumerable<dynamic> data { get; set; }
}

public class NotYetStartedCoursesData
{
    public int count { get; set; }
    public IEnumerable<dynamic> data { get; set; }
}

public partial class Learner : BasePage
{
    #region TakeLaterCoursesDataSource

    [System.Web.Services.WebMethod()]
    public static TakeLaterCourseData GetSearchCourseData(int startRowIndex, int maximumRows, string sortExpression)
    {
        TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();
        var CIM = Convert.ToInt32(DataHelper.GetCurrentUserCIM());

        //take later course data
        var _courses = db.vw_TranscomUniversity_TakeLateCoursesFolders
                .Join(db.vw_TranscomUniversity_AllCourses, f => f.OFileID, c => c.CourseID, (f, c)                
                   => new
                   {

                       FileType = f.FileType,
                       CimNumber = f.EnrolleeCIM,
                       CourseID = c.CourseID,
                       CourseImage = c.CourseImage,
                       CourseTitle = c.CourseTitle,
                       CourseType = c.CourseType,
                       CourseDuration = c.CourseDuration,
                       CourseCategory = c.CourseCategory,
                       CourseSubcategory = c.CourseSubcategory,
                       CourseDescription = c.CourseDescription,
                       DateLastModified = c.DateLastModified,
                       CoursePath = c.CoursePath,

                   })                   
                   .AsEnumerable()
                   .Select(c => new
                   {
                       c.FileType,
                       c.CimNumber,
                       EncryptedCourseID = Utils.Encrypt(c.CourseID),
                       c.CourseID,
                       c.CourseImage,
                       c.CourseTitle,
                       c.CourseType,
                       c.CourseDuration,
                       c.CourseCategory,
                       c.CourseSubcategory,
                       c.CourseDescription,
                       DateLastModified = string.Format("{0: yyyy/MM/dd hh:mm:ss tt}", c.DateLastModified),
                       CoursePath = c.CoursePath

                   })
               .Where(c => c.CimNumber == CIM && c.FileType == 1)
                .ToArray().OrderBy(c => c.CourseTitle);
        

        //add data and count so that we don't make 2 trips to the server in retrieving the count and the data
        var gridData = new TakeLaterCourseData()
        {
            count = _courses.Count(),
            data = RadGrid.GetBindingData(_courses.AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data
        };

        return gridData;
    }

    [System.Web.Services.WebMethod()]
    public static dynamic getCourseComments(string encCourseID)
    {
        int CourseID = Convert.ToInt32(Utils.Decrypt(encCourseID));
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            try
            {
                var courseComments = db.vw_TranscomUniversity_CourseComments
                     .AsEnumerable()
                    .Where(c => c.CourseID == CourseID)
                    .Select(c => new
                    {
                        c.CourseID,
                        FormattedDateModified = string.Format("{0:MMMM dd, yyyy}", Convert.ToDateTime(c.DateModified)),
                        c.Name,
                        c.Comment,
                        c.Rating,
                        c.UserImageUrl,
                        c.EnrolleeCIM
                    })
                    .ToList();

                if (courseComments != null)
                    return courseComments;

            }
            catch
            {
                return null;
            }
        }

        return null;
    }

    #endregion

    #region EnrolledCoursesUserCtrl

    public class EnrolledCourseDetails
    {
        public string EncryptedCourseID { get; set; }

        public int CourseID { get; set; }

        public int EnrolleeCIM { get; set; }

        public string CourseTitle { get; set; }

        public string CourseDescription { get; set; }

        public string CourseType { get; set; }

        public int? CourseDuration { get; set; }

        public string Status { get; set; }

        public string CourseCategory { get; set; }

        public string CourseSubcategory { get; set; }

        public string CourseImage { get; set; }
    }

    [System.Web.Services.WebMethod()]
    public static EnrolledCoursesData GetEnrolledCoursesData(int startRowIndex, int maximumRows, string sortExpression)
    {
        var currentCIM = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
        TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();

        try
        {
            var _enrolledCourses = db.pr_Transcomuniversity_EnrolledCourses(currentCIM)
                             .AsEnumerable()
                             .Select(c => new EnrolledCourseDetails
                             {
                                 EncryptedCourseID = Utils.Encrypt(Convert.ToInt32(c.CourseID)),
                                 CourseID = Convert.ToInt32(c.CourseID),
                                 CourseTitle = c.CourseTitle,
                                 CourseDescription = c.CourseDescription,
                                 CourseType = c.CourseType,
                                 CourseDuration = c.CourseDuration,
                                 Status = c.Status,
                                 CourseCategory = c.CourseCategory,
                                 CourseSubcategory = c.CourseSubcategory,
                                 CourseImage = c.CourseImage
                             })
                             .ToList();

            EnrolledCoursesData dac = new EnrolledCoursesData()
            {
                count = _enrolledCourses.Count(),
                data = RadGrid.GetBindingData(_enrolledCourses.AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data
            };

            return dac;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    #endregion

    #region CompletedCoursesUserCtrl

    public class CompletedCourseDetails
    {
        public string EncryptedCourseID { get; set; }

        public int CourseID { get; set; }

        public int EnrolleeCIM { get; set; }

        public string CourseTitle { get; set; }

        public string CourseDescription { get; set; }

        public string CourseType { get; set; }

        public int? CourseDuration { get; set; }

        public string Status { get; set; }

        public string CourseCategory { get; set; }

        public string CourseSubcategory { get; set; }

        public string CourseImage { get; set; }
    }

    [System.Web.Services.WebMethod()]
    public static CompletedCoursesData GetCompletedCoursesData(int startRowIndex, int maximumRows, string sortExpression)
    {
        TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();

        try
        {
            var _completedCourses = db.pr_TranscomUniversity_CompletedCourses(currentCIM)
                             .AsEnumerable()
                             .Select(c => new CompletedCourseDetails
                             {
                                 EncryptedCourseID = Utils.Encrypt(Convert.ToInt32(c.CourseID)),
                                 CourseID = Convert.ToInt32(c.CourseID),
                                 CourseTitle = c.CourseTitle,
                                 CourseDescription = c.CourseDescription,
                                 CourseType = c.CourseType,
                                 CourseDuration = c.CourseDuration,
                                 Status = c.Status,
                                 CourseCategory = c.CourseCategory,
                                 CourseSubcategory = c.CourseSubcategory,
                                 CourseImage = c.CourseImage
                             })
                             .ToList();

            CompletedCoursesData dac = new CompletedCoursesData()
            {
                count = _completedCourses.Count(),
                data = RadGrid.GetBindingData(_completedCourses.AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data
            };

            return dac;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    #endregion

    #region InProgressCoursesUserCtrl

    public class InProgressCourseDetails
    {
        public string EncryptedCourseID { get; set; }

        public int CourseID { get; set; }

        public int EnrolleeCIM { get; set; }

        public string CourseTitle { get; set; }

        public string CourseDescription { get; set; }

        public string CourseType { get; set; }

        public int? CourseDuration { get; set; }

        public string Status { get; set; }

        public string CourseCategory { get; set; }

        public string CourseSubcategory { get; set; }

        public string CourseImage { get; set; }
    }

    [System.Web.Services.WebMethod()]
    public static InProgressCoursesData GetInProgressCoursesData(int startRowIndex, int maximumRows, string sortExpression)
    {
        var currentCIM = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
        TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();

        try
        {
            var _inProgressCourses = db.pr_TranscomUniversity_InProgressCourses(currentCIM)
                             .AsEnumerable()
                             .Select(c => new InProgressCourseDetails
                             {
                                 EncryptedCourseID = Utils.Encrypt(Convert.ToInt32(c.CourseID)),
                                 CourseID = Convert.ToInt32(c.CourseID),
                                 CourseTitle = c.CourseTitle,
                                 CourseDescription = c.CourseDescription,
                                 CourseType = c.CourseType,
                                 CourseDuration = c.CourseDuration,
                                 Status = c.Status,
                                 CourseCategory = c.CourseCategory,
                                 CourseSubcategory = c.CourseSubcategory,
                                 CourseImage = c.CourseImage
                             })
                             .ToList();

            InProgressCoursesData dac = new InProgressCoursesData()
            {
                count = _inProgressCourses.Count(),
                data = RadGrid.GetBindingData(_inProgressCourses.AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data
            };

            return dac;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    #endregion

    #region OverdueCoursesUserCtrl

    public class OverdueCourseDetails
    {
        public string EncryptedCourseID { get; set; }

        public int CourseID { get; set; }

        public int EnrolleeCIM { get; set; }

        public string CourseTitle { get; set; }

        public string CourseDescription { get; set; }

        public string CourseType { get; set; }

        public int? CourseDuration { get; set; }

        public string Status { get; set; }

        public string CourseCategory { get; set; }

        public string CourseSubcategory { get; set; }

        public string CourseImage { get; set; }
    }

    [System.Web.Services.WebMethod()]
    public static OverdueCoursesData GetOverdueCoursesData(int startRowIndex, int maximumRows, string sortExpression)
    {
        TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();

        try
        {
            var _overdueCourses = db.pr_TranscomUniversity_OverdueCourses(currentCIM)
                             .AsEnumerable()
                             .Select(c => new OverdueCourseDetails
                             {
                                 EncryptedCourseID = Utils.Encrypt(Convert.ToInt32(c.CourseID)),
                                 CourseID = Convert.ToInt32(c.CourseID),
                                 CourseTitle = c.CourseTitle,
                                 CourseDescription = c.CourseDescription,
                                 CourseType = c.CourseType,
                                 CourseDuration = c.CourseDuration,
                                 Status = c.Status,
                                 CourseCategory = c.CourseCategory,
                                 CourseSubcategory = c.CourseSubcategory,
                                 CourseImage = c.CourseImage
                             })
                             .ToList();

            OverdueCoursesData dac = new OverdueCoursesData()
            {
                count = _overdueCourses.Count(),
                data = RadGrid.GetBindingData(_overdueCourses.AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data
            };

            return dac;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    #endregion

    #region NotYetStartedCoursesUserCtrl

    public class NotYetStartedCourseDetails
    {
        public string EncryptedCourseID { get; set; }

        public int CourseID { get; set; }

        public int EnrolleeCIM { get; set; }

        public string CourseTitle { get; set; }

        public string CourseDescription { get; set; }

        public string CourseType { get; set; }

        public int? CourseDuration { get; set; }

        public string Status { get; set; }

        public string CourseCategory { get; set; }

        public string CourseSubcategory { get; set; }

        public string CourseImage { get; set; }
    }

    [System.Web.Services.WebMethod()]
    public static NotYetStartedCoursesData GetNotYetStartedCoursesData(int startRowIndex, int maximumRows, string sortExpression)
    {
        TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();

        try
        {
            var _notYetStartedCourses = db.pr_TranscomUniversity_NotYetStartedCourses(currentCIM)
                             .AsEnumerable()
                             .Select(c => new NotYetStartedCourseDetails
                             {
                                 EncryptedCourseID = Utils.Encrypt(Convert.ToInt32(c.CourseID)),
                                 CourseID = Convert.ToInt32(c.CourseID),
                                 CourseTitle = c.CourseTitle,
                                 CourseDescription = c.CourseDescription,
                                 CourseType = c.CourseType,
                                 CourseDuration = c.CourseDuration,
                                 Status = c.Status,
                                 CourseCategory = c.CourseCategory,
                                 CourseSubcategory = c.CourseSubcategory,
                                 CourseImage = c.CourseImage
                             })
                             .ToList();

            NotYetStartedCoursesData dac = new NotYetStartedCoursesData()
            {
                count = _notYetStartedCourses.Count(),
                data = RadGrid.GetBindingData(_notYetStartedCourses.AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data
            };

            return dac;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    #endregion

    //#region MyLearningPlan

    //[System.Web.Services.WebMethod()]
    //public static TakeLaterCourseData GetMyLearningPlan(int startRowIndex, int maximumRows, string sortExpression)
    //{
    //    TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();

    //    int userCim = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
    //    string email = HttpContext.Current.User.Identity.Name.Split('|')[0];
    //    string userId = null;
    //    var _courses = db.pr_TranscomUniversity_Lkp_AssignedEnrolledCourses(userCim, userId, email)
    //                    .AsEnumerable()
    //                    .Select(c => new
    //                    {
    //                        CourseID = c.CourseId,
    //                        EncryptedCourseID = Utils.Encrypt(Convert.ToInt32(c.CourseId)),
    //                        CourseTitle = c.Title,
    //                        DateAdded = string.Format("{0: MM/dd/yyyy}", c.CreateDate) == "" ? "N/A" : string.Format("{0: MM/dd/yyyy}", c.CreateDate),
    //                        DueDate = string.Format("{0: MM/dd/yyyy}", c.DueDate) == "" ? "N/A" : string.Format("{0: MM/dd/yyyy}", c.DueDate),
    //                        CourseType = c.CourseType,
    //                        Status = c.Status,
    //                        StatusId = c.StatusId,
    //                        Progress = progressVal(c.CourseTypeID, c.HasTest, c.CourseLogin.ToString(), c.Passed, c.CourseId.ToString()),
    //                        CourseImage = c.CourseImage
    //                    })
    //                    .OrderBy(c => c.DateAdded)
    //                    .ToList();
    //    //add data and count so that we don't make 2 trips to the server in retrieving the count and the data
    //    var gridData = new TakeLaterCourseData()
    //    {
    //        count = _courses.Count(),
    //        data = RadGrid.GetBindingData(_courses.AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data
    //    };

    //    return gridData;
    //}

    //#endregion

    #region CalendarEventDetails

    [System.Web.Services.WebMethod()]
    public static dynamic GetCalendarEventDetails(int calendarID, string tz)
    {
        int decryptTz = Convert.ToInt32(Utils.Decrypt(tz));

        var offset = new TimeSpan(decryptTz / 60, decryptTz % 60, 0);

        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var details = db.pr_TranscomUniversity_CalendarEventDetails(calendarID)
            //var details = db.vw_TranscomUniversity_CalendarEventDetails1
                        .AsEnumerable()
                        //.Where(e => e.CALENDARID == calendarID)
                        .Select(e => new
                        {
                            e.CalendarID,
                            e.Subject,
                            //EventTimeZone = e.StartTime.,
                            Title = e.Title,
                            EventWhen = string.Format("{0: dddd MMMM dd, yyyy}", (e.StartTime - offset)),
                            EventTime = string.Format("{0: hh:mm tt}", (e.StartTime - offset)) + " to " + string.Format("{0: hh:mm tt}", (e.EndTime - offset)),
                            CalendarDay = Convert.ToDateTime((e.StartTime - offset)).Day,
                            CalendarMonth = string.Format("{0: MMMM}", e.StartTime).ToUpper(),
                            e.SessionStatusID,
                            e.EventTypeID,
                            e.EventType,
                            e.Location,
                            e.Description,
                            e.RegisteredParticipants,
                            e.AvailableSeats,
                            e.WaitListed,
                            e.TrainerName,
                            e.TrainerCIM,
                            e.CreateDate
                            
                        })
                        .SingleOrDefault();

            if (details != null)
                return details;

            return null;
        }
    }
    #endregion

    static int currentCIM;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            
            Session["IsNewCourseView"] = true;
           
            currentCIM = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
        }
        Page.Form.Attributes.Add("enctype", "multipart/form-data");

        

        //RadTab learningPlan = RadTabStrip1.Tabs.FindTabByValue("1");
        //RadTab transcript = RadTabStrip1.Tabs.FindTabByValue("2");
        RadTab calendar = RadTabStrip1.Tabs.FindTabByValue("1");
        RadTab takeLaterCourses = RadTabStrip1.Tabs.FindTabByValue("2");

        //if (Request.QueryString["Tab"] != null && Request.QueryString["Tab"].ToString() == "myLearningPlan")
        //{
        //    RadMultiPage1.SelectedIndex = 0;
        //    learningPlan.Selected = true;
        //    pnlLearnerDashboard.Visible = false;
        //    RadMultiPage2.Visible = false;
        //    lblTab2.Text = "My Learning Plan";
        //}

        //if (Request.QueryString["Tab"] != null && Request.QueryString["Tab"].ToString() == "myTranscripts")
        //{
        //    RadMultiPage1.SelectedIndex = 1;
        //    transcript.Selected = true;
        //    pnlLearnerDashboard.Visible = false;
        //    RadMultiPage2.Visible = false;
        //    lblTab2.Text = "My Transcripts";
        //}

        if (Request.QueryString["Tab"] != null && Request.QueryString["Tab"].ToString() == "myCalendar")
        {
            RadMultiPage1.SelectedIndex = 0;
            calendar.Selected = true;
            pnlLearnerDashboard.Visible = false;
            RadMultiPage2.Visible = false;
            lblTab2.Text = Resources.LocalizedResource.MyCalendar;

            RadCalendar1.Visible = true;
            RadCalendar1.FocusedDate = DataHelper.serverDate();
        }

        else if (Request.QueryString["Tab"] != null && Request.QueryString["Tab"].ToString() == "takeLaterCourses")
        {
            RadMultiPage1.SelectedIndex = 1;
            takeLaterCourses.Selected = true;
            pnlLearnerDashboard.Visible = false;
            RadMultiPage2.Visible = false;
            lblTab2.Text = Resources.LocalizedResource.TakeLaterCourses;
        }else
        {
            GetEnrolledCourses();
            GetCompletedCourses();
            GetInProgressCourses();
            GetOverdueCourses();
            GetNotYetStartedCourses();

            if (Request.QueryString["Tab"] != null && Request.QueryString["Tab"].ToString() == "enrolledCourses")
            {
                RadMultiPage2.SelectedIndex = 0;
                lblTab2.Text = Resources.LocalizedResource.EnrolledCourses;
                lblBreadcrumbs.Text = Resources.LocalizedResource.EnrolledCourses;

                RadCodeBlock rcbEnrolledCourses = (RadCodeBlock)(EnrolledCourses).FindControl("rcbEnrolledCourses");
                rcbEnrolledCourses.Visible = true;
            }

            else if (Request.QueryString["Tab"] != null && Request.QueryString["Tab"].ToString() == "completedCourses")
            {
                RadMultiPage2.SelectedIndex = 1;
                lblTab2.Text = Resources.LocalizedResource.CompletedCourses;
                lblBreadcrumbs.Text = Resources.LocalizedResource.CompletedCourses;

                RadCodeBlock rcbCompletedCourses = (RadCodeBlock)(CompletedCourses).FindControl("rcbCompletedCourses");
                rcbCompletedCourses.Visible = true;
            }

            else if (Request.QueryString["Tab"] != null && Request.QueryString["Tab"].ToString() == "inProgressCourses")
            {
                RadMultiPage2.SelectedIndex = 2;
                lblTab2.Text = Resources.LocalizedResource.Inprogress;
                lblBreadcrumbs.Text = Resources.LocalizedResource.Inprogress;

                RadCodeBlock rcbInProgressCourses = (RadCodeBlock)(InProgressCourses).FindControl("rcbInProgressCourses");
                rcbInProgressCourses.Visible = true;
            }

            else if (Request.QueryString["Tab"] != null && Request.QueryString["Tab"].ToString() == "overdueCourses")
            {
                RadMultiPage2.SelectedIndex = 3;
                lblTab2.Text = Resources.LocalizedResource.OverdueCourses;
                lblBreadcrumbs.Text = Resources.LocalizedResource.OverdueCourses;

                RadCodeBlock rcbOverdueCourses = (RadCodeBlock)(OverdueCourses).FindControl("rcbOverdueCourses");
                rcbOverdueCourses.Visible = true;
            }

            else if (Request.QueryString["Tab"] != null && Request.QueryString["Tab"].ToString() == "notYetStartedCourses")
            {
                RadMultiPage2.SelectedIndex = 4;
                lblTab2.Text = Resources.LocalizedResource.NotYetStarted;
                lblBreadcrumbs.Text = Resources.LocalizedResource.NotYetStarted;

                RadCodeBlock rcbNotYetStartedCourses = (RadCodeBlock)(NotYetStartedCourses).FindControl("rcbNotYetStartedCourses");
                rcbNotYetStartedCourses.Visible = true;
            }

            else if (Request.QueryString["Tab"] != null && Request.QueryString["Tab"].ToString() == "notYetStartedCourses")
            {
                RadMultiPage2.SelectedIndex = 4;
                lblTab2.Text = Resources.LocalizedResource.NotYetStarted;
                lblBreadcrumbs.Text = Resources.LocalizedResource.NotYetStarted;

                RadCodeBlock rcbNotYetStartedCourses = (RadCodeBlock)(NotYetStartedCourses).FindControl("rcbNotYetStartedCourses");
                rcbNotYetStartedCourses.Visible = true;
            }
        
        }

        //--

        

        //set Sesssion
        Session["SessionRoles"] = "Learner";
    }

    public void GetEnrolledCourses()
    {
        var currentCim = Convert.ToInt32(DataHelper.GetCurrentUserCIM());

        var db = new TranscomUniversityV3ModelContainer();
        int countEnrolledCourses = db.vw_TranscomUniversity_ApprovedEnrollments_Simple
                             .Where(e => e.EnrolleeCIM == currentCim)
                             .Count();

        ltEnrolledCourses.Text = countEnrolledCourses != 0 ? countEnrolledCourses.ToString() : "0";
    }

    public void GetCompletedCourses()
    {
        var db = new TranscomUniversityV3ModelContainer();
        var countCompletedCourses = db.pr_TranscomUniversity_CompletedCourses(currentCIM)
                                .AsEnumerable()
                                .Select(c => new CompletedCourseDetails
                                {
                                    EncryptedCourseID = Utils.Encrypt(Convert.ToInt32(c.CourseID)),
                                    CourseID = Convert.ToInt32(c.CourseID),
                                    CourseTitle = c.CourseTitle,
                                    CourseDescription = c.CourseDescription,
                                    CourseType = c.CourseType,
                                    CourseDuration = c.CourseDuration,
                                    Status = c.Status,
                                    CourseCategory = c.CourseCategory,
                                    CourseSubcategory = c.CourseSubcategory,
                                    CourseImage = c.CourseImage
                                })
                                .ToList()
                                .Count().ToString();

        ltCompletedCourses.Text = countCompletedCourses != "0" ? countCompletedCourses : "0";
    }

    public void GetInProgressCourses()
    {
        var db = new TranscomUniversityV3ModelContainer();
        var countInProgressCourses = db.pr_TranscomUniversity_InProgressCourses(currentCIM)
                             .AsEnumerable()
                             .Select(c => new InProgressCourseDetails
                             {
                                 EncryptedCourseID = Utils.Encrypt(Convert.ToInt32(c.CourseID)),
                                 CourseID = Convert.ToInt32(c.CourseID),
                                 CourseTitle = c.CourseTitle,
                                 CourseDescription = c.CourseDescription,
                                 CourseType = c.CourseType,
                                 CourseDuration = c.CourseDuration,
                                 Status = c.Status,
                                 CourseCategory = c.CourseCategory,
                                 CourseSubcategory = c.CourseSubcategory,
                                 CourseImage = c.CourseImage
                             })
                             .ToList()
                             .Count().ToString();

        ltInProgressCourses.Text = countInProgressCourses != "0" ? countInProgressCourses : "0";
    }

    public void GetOverdueCourses()
    {
        var db = new TranscomUniversityV3ModelContainer();
        var countOverdueCourses = db.pr_TranscomUniversity_OverdueCourses(currentCIM)
                             .AsEnumerable()
                             .Select(c => new OverdueCourseDetails
                             {
                                 EncryptedCourseID = Utils.Encrypt(Convert.ToInt32(c.CourseID)),
                                 CourseID = Convert.ToInt32(c.CourseID),
                                 CourseTitle = c.CourseTitle,
                                 CourseDescription = c.CourseDescription,
                                 CourseType = c.CourseType,
                                 CourseDuration = c.CourseDuration,
                                 Status = c.Status,
                                 CourseCategory = c.CourseCategory,
                                 CourseSubcategory = c.CourseSubcategory,
                                 CourseImage = c.CourseImage
                             })
                             .ToList()
                             .Count().ToString();

        ltOverdueCourses.Text = countOverdueCourses != "0" ? countOverdueCourses : "0";
    }

    public void GetNotYetStartedCourses()
    {
        var db = new TranscomUniversityV3ModelContainer();
        var countNotYetStartedCourses = db.pr_TranscomUniversity_NotYetStartedCourses(currentCIM)
                             .AsEnumerable()
                             .Select(c => new NotYetStartedCourseDetails
                             {
                                 EncryptedCourseID = Utils.Encrypt(Convert.ToInt32(c.CourseID)),
                                 CourseID = Convert.ToInt32(c.CourseID),
                                 CourseTitle = c.CourseTitle,
                                 CourseDescription = c.CourseDescription,
                                 CourseType = c.CourseType,
                                 CourseDuration = c.CourseDuration,
                                 Status = c.Status,
                                 CourseCategory = c.CourseCategory,
                                 CourseSubcategory = c.CourseSubcategory,
                                 CourseImage = c.CourseImage
                             })
                             .ToList()
                             .Count().ToString();

        ltNotYetStartedCourses.Text = countNotYetStartedCourses != "0" ? countNotYetStartedCourses : "0";
    }

    public static double progressVal(int? courseTypeID, int? hasTest, string login, int? passed, string courseId)
    {
        double progressval = 0.00;

        if (courseTypeID == 1)
        {
            if (hasTest > 0) //COURSE WITH ASSESSMENT
            {
                if (login == "1")
                {
                    progressval = 20;

                    if (passed == 1)
                    {
                        progressval = progressval + 80;
                    }
                }
            }
            else //COURSE WO ASSESSMENT
            {
                if (login == "1")
                {
                    progressval = 100;
                }
            }
        }

        else if (courseTypeID == 2)
        {

            var email = HttpContext.Current.User.Identity.Name.Split('|')[0];
            progressval = Convert.ToDouble(ScormDataHelper.GetOverallProgressByCourseID(email, Convert.ToInt32(courseId)));
        }

        return progressval;
    }
}