﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<%@ Register Src="UserControl/Pages/HomeContentUserCtrl.ascx" TagName="Home" TagPrefix="ucHome" %>
<%@ Register Src="~/UserControl/Pages/Footer.ascx" TagName="footer" TagPrefix="ucFooter" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Transcom University</title>
    <link href="CSS/bootstrap.min.css" rel="stylesheet" />
    <link href="Media/main.css" rel="stylesheet" />
    <link href="Media/mediaQuery.css" rel="stylesheet" />
    <link href="Media/circle.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Cabin|Open+Sans" rel="stylesheet" />
    <link href="Font-Awesome/CSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="CSS/Slick/slick.css" rel="stylesheet" type="text/css" />
    <link href="CSS/Slick/slick-theme.css" rel="stylesheet" type="text/css" />
    <link rel="icon" type="image/png" href="Media/Images/favicon.png" />
</head>
<body>
    <script src="JS/jquery.js"></script>
    <script type="text/javascript">
        var ISMOBILE = false;
        var ISTABLET = false;
        var ISDESKTOP = false;

        $(document).ready(function () {
            ISMOBILE = false;
            ISTABLET = false;
            ISDESKTOP = false;
            //get useragenet screen size
            if (window.matchMedia("(max-width: 480px)").matches) {
                // window width is at less than 480px
                console.log("is mobile view");
                ISMOBILE = true;
            } else if (window.matchMedia("(max-width: 1024px)").matches) {
                // window width is at less than 1024px
                console.log("is tablet view");
                ISTABLET = true;
            } else {
                console.log("is laptop/desktop view");
                ISDESKTOP = true;
            }
                var d = new Date();

                $("#<%= TimezoneOffset.ClientID %>").val(d.getTimezoneOffset());
        });

    </script>
    <form id="form1" runat="server">
    <asp:HiddenField runat="server" ID="TimezoneOffset" />
    <div id="main-wrapper" class="no-bottom-margin">
        <div class="container-fluid no-padding">
            <div class="notifbar-holder pull-right">
                <div class="col-md-8">
                    <asp:LoginView ID="loginView1" runat="server">
                        <AnonymousTemplate>
                            <a href="#" data-toggle="dropdown">
                                <div class="profile-info" data-lock-name="Login" data-lock-email="Login">
                                    <span class="role">
                                        <asp:LoginStatus ID="loginName1" LogoutAction="RedirectToLoginPage" runat="server"
                                            LoginText=" " LogoutText="Sign Out" />
                                    </span>
                                </div>
                            </a>
                        </AnonymousTemplate>
                        <LoggedInTemplate>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img id="imgPicStatus" width="50px" src="<%= HttpContext.Current.User.Identity.Name.Split('|')[2] %>"
                                    class="border-circle img-circle" data-lock-picture="<%= HttpContext.Current.User.Identity.Name.Split('|')[2] %>" />
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right" style="padding: 5px; max-width: 100px;">
                                <%
                                    if (System.Web.Security.Roles.IsUserInRole(HttpContext.Current.User.Identity.Name.Split('|')[0], "Admin"))
                                    {
                                %>
                                <li><a href="UserRoles.aspx"><asp:Label ID="Label25" runat="server" Text="<%$ Resources: LocalizedResource, UserRoles %>" /></a></li>
                                <%
                                    }
                                %>
                                <li><a href="MyProfile.aspx"><asp:Label runat="server" Text="<%$ Resources: LocalizedResource, MyProfile %>" /></a></li>
                                <li>
                                    <asp:LoginStatus ID="LoginStatus1" runat="server" LogoutAction="RedirectToLoginPage"
                                        LogoutText="Sign Out" LogoutPageUrl="~/Home.aspx" />
                                </li>
                            </ul>
                        </LoggedInTemplate>
                    </asp:LoginView>
                </div>
            </div>
        </div>
        <nav class="navbar diplay-inline-flex" role="navigation">
                <asp:LoginView ID="loginView3" runat="server">
            <AnonymousTemplate>
                <div class="container-fluid no-padding m-signin" style="position:absolute;right:0;top:2vh;">
                    <asp:Button ID="Button1" Text="<%$ Resources: LocalizedResource, Signin %>" runat="server" OnClick="btnRequestLogin_Click"
                        CssClass="pull-right btn-link btn-signin" />
                </div>
            </AnonymousTemplate>
        </asp:LoginView>
            <div class="container no-paddings">
                       <%-- <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>--%>
                        <div class="pull-left">
                         <a href="Home.aspx">
                                    <img src="Media/Images/logo2.png" style="width:20vw;" class="m-logo" />
                                </a>
                        </div>
                        
                        <div class="navbar-collapse in no-padding pull-left m-login-navbar" id="bs-example-navbar-collapse-1" style="margin-top:5vh;">
                            <ul class="nav navbar-nav tu-nav">
                                <asp:LoginView ID="loginView2" runat="server">
                                    <AnonymousTemplate>
                                        <li class="active"><a href="Login.aspx"><asp:Label runat="server" Text="<%$ Resources: LocalizedResource, Home %>" /></a></li>
                                        <li class="divider-vertical"><a href="CourseCatalog.aspx"><asp:Label ID="Label8" runat="server" Text="<%$ Resources: LocalizedResource, CourseCatalog %>" /></a></li>
                                    </AnonymousTemplate>
                                    <LoggedInTemplate>
                                        <%
                                            if (System.Web.Security.Roles.IsUserInRole(HttpContext.Current.User.Identity.Name.Split('|')[0], "Admin"))
                                            {
                                        %>
                                            <li class="active"><a href="Home.aspx"><asp:Label ID="Label9" runat="server" Text="<%$ Resources: LocalizedResource, Home %>" /></a></li>
                                            <li class="divider-vertical"><a href="CourseCatalog.aspx"><asp:Label ID="Label8" runat="server" Text="<%$ Resources: LocalizedResource, CourseCatalog %>" /></a></li>
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><asp:Label ID="Label10" runat="server" Text="<%$ Resources: LocalizedResource, Dashboard %>" /><b class="caret"></b></a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="Learner.aspx?Tab=enrolledCourses"><asp:Label ID="Label11" runat="server" Text="<%$ Resources: LocalizedResource, Learner %>" /></a></li>
                                                    <li><a href="Admin.aspx"><asp:Label ID="Label12" runat="server" Text="<%$ Resources: LocalizedResource, Admin %>" /></a></li>
                                                </ul>
                                            </li>
                                            <li class="divider-vertical"><a href="#"><asp:Label ID="Label13" runat="server" Text="<%$ Resources: LocalizedResource, MyTeam %>" /></a></li>
                                            <li><a href="#"><asp:Label ID="Label14" runat="server" Text="<%$ Resources: LocalizedResource, MyReports %>" /></a></li>
                                            <%
                                                }
                                        %>
                                                <%
                                                    if (System.Web.Security.Roles.IsUserInRole(HttpContext.Current.User.Identity.Name.Split('|')[0], "Trainer"))
                                                    {
                                        %>
                                                    <li class="active"><a href="Home.aspx"><asp:Label ID="Label15" runat="server" Text="<%$ Resources: LocalizedResource, Home %>" /></a></li>
                                                    <li class="divider-vertical"><a href="CourseCatalog.aspx"><asp:Label ID="Label16" runat="server" Text="<%$ Resources: LocalizedResource, CourseCatalog %>" /></a></li>
                                                    <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><asp:Label ID="Label17" runat="server" Text="<%$ Resources: LocalizedResource, Dashboard %>" /><b class="caret"></b></a>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="Learner.aspx?Tab=enrolledCourses"><asp:Label ID="Label18" runat="server" Text="<%$ Resources: LocalizedResource, Learner %>" /></a></li>
                                                            <li><a href="#"><asp:Label ID="Label19" runat="server" Text="<%$ Resources: LocalizedResource, Trainer %>" /></a></li>
                                                        </ul>
                                                    </li>
                                                    <li class="divider-vertical"><a href="#"><asp:Label ID="Label20" runat="server" Text="<%$ Resources: LocalizedResource, MyTeam %>" /></a></li>
                                                    <li><a href="#">My Reports</a></li>
                                                    <%
                                                        }
                                        %>
                                                        <%
                                                            if (System.Web.Security.Roles.IsUserInRole(HttpContext.Current.User.Identity.Name.Split('|')[0], "Learner"))
                                                            {
                                        %>
                                                            <li class="active"><a href="Home.aspx"><asp:Label ID="Label21" runat="server" Text="<%$ Resources: LocalizedResource, Home %>" /></a></li>
                                                            <li class="divider-vertical"><a href="CourseCatalog.aspx"><asp:Label ID="Label22" runat="server" Text="<%$ Resources: LocalizedResource, CourseCatalog %>" /></a></li>
                                                            <li class="dropdown">
                                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><asp:Label ID="Label23" runat="server" Text="<%$ Resources: LocalizedResource, Dashboard %>" /><b class="caret"></b></a>
                                                                <ul class="dropdown-menu">
                                                                    <li><a href="Learner.aspx?Tab=enrolledCourses"><asp:Label ID="Label24" runat="server" Text="<%$ Resources: LocalizedResource, Learner %>" /></a></li>
                                                                </ul>
                                                            </li>
                                                            <%
                                                                }
                                        %>
                                    </LoggedInTemplate>
                                </asp:LoginView>
                            </ul>
                            <div class="navbar-form pull-right search-input no-padding m-search" role="search" style="position:absolute;right:0;">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for a course..." name="q" onkeypress="return enterPressed(event)"/>
                                    <div>
                                    <asp:LinkButton ID="btnSearchSettings" runat="server" Font-Underline="false" ClientIDMode="Static" OnClientClick="showSearchToolTip(); return false;" CssClass="fa fa-caret-down dropdown-caret" aria-hidden="true"></asp:LinkButton>
                                    <rad:RadToolTip ID="tooltipSearch" runat="server" Modal="false" RenderMode="Lightweight" ShowEvent="OnClick" 
                                        HideEvent="ManualClose" Position="BottomLeft" Skin="Silk" TargetControlID="btnSearchSettings" Height="400" Width="370" ContentScrolling="Y" >
                                        <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>--%>
                                                <asp:Panel ID="pnlProfileSettings" runat="server" CssClass="container">
                                                    <p><asp:Label runat="server" Text="<%$ Resources: LocalizedResource, FindResultsThatHave %>" /></p>
                                                    <asp:label ID="Label1" text="<%$ Resources: LocalizedResource, AllOfTheseWords %>" runat="server" AssociatedControlID="txtWords"/>
                                                    <asp:TextBox ID="txtWords" runat="server" CssClass="form-control" ClientIDMode="Static" ViewStateMode="Enabled" ></asp:TextBox>
                                                    <asp:label ID="Label2" text="<%$ Resources: LocalizedResource, ThisExactPhrase %>" runat="server" AssociatedControlID="txtPhrase"/>
                                                    <asp:TextBox ID="txtPhrase" runat="server" CssClass="form-control" ClientIDMode="Static" ></asp:TextBox>

                                                    <asp:label ID="lblLookIn" text="<%$ Resources: LocalizedResource, LookIn %>" runat="server" />
                                                    <asp:label ID="lblLookinError" ClientIDMode="Static" text="<%$ Resources: LocalizedResource, Pleaseselectacriteria %>" runat="server" Font-Size="Small" ForeColor="Red" CssClass="display-none"/>
                                                    <rad:RadListBox RenderMode="Lightweight" ID="rlbLookIn" runat="server" CheckBoxes="true" Width="300"
                                                        BorderWidth="0" ClientIDMode="Static" ShowCheckAll="true">
                                                    <Items>
                                                        <rad:RadListBoxItem Text="<%$ Resources: LocalizedResource, CourseTitles %>" Value="0"></rad:RadListBoxItem>
                                                        <rad:RadListBoxItem Text="<%$ Resources: LocalizedResource, CourseCategories %>" Value="1"></rad:RadListBoxItem>
                                                        <rad:RadListBoxItem Text="<%$ Resources: LocalizedResource, CourseSubcategories %>" Value="2"></rad:RadListBoxItem>
                                                        <rad:RadListBoxItem Text="<%$ Resources: LocalizedResource, Description %>" Value="3"></rad:RadListBoxItem>
                                                        <rad:RadListBoxItem Text="<%$ Resources: LocalizedResource, Content %>" Value="4"></rad:RadListBoxItem>
                                                        <%--<rad:RadListBoxItem Text="All" Value="5"></rad:RadListBoxItem>--%>
                                                    </Items>
                                                    <Localization CheckAll="All"/>
                                                    </rad:RadListBox>
                                                    <asp:label ID="Label3" text="<%$ Resources: LocalizedResource, RefineSearchBy %>" runat="server" />
                                                    <br />
                                                    <asp:label ID="Label4" text="<%$ Resources: LocalizedResource, DateUpload %>" runat="server" AssociatedControlID="rdtDateUploaded" />
                                                    <br />
                                                    <rad:RadDatePicker ID="rdtDateUploaded" runat="server" RenderMode="Lightweight" Width="250px"
                                                    ToolTip="<%$ Resources: LocalizedResource, SetDateUploaded %>" ZIndex="8001" DateInput-EmptyMessage="<%$ Resources: LocalizedResource, AnyTime %>" ClientIDMode="Static" >
                                                    </rad:RadDatePicker>
                                                     
                                                    <br />
                                                    <asp:label ID="lblDepartment" text="<%$ Resources: LocalizedResource, DepartmentOwner %>" runat="server" AssociatedControlID="rcbDepartment" />
                                                    <br />
                                                    <rad:RadComboBox RenderMode="Lightweight" ID="rcbDepartment" runat="server" Height="250px"
                                                        Width="300px" EmptyMessage="<%$ Resources: LocalizedResource, TypeNameOrSelect %>" DataSourceID="dsDepartment"
                                                        DataTextField="Department" DataValueField="DepartmentID" ToolTip="<%$ Resources: LocalizedResource, SelectDepartment %>" ZIndex="8001" ClientIDMode="Static" >
                                                        <DefaultItem Text="<%$ Resources: LocalizedResource, Select %>" Value="0" />
                                                    </rad:RadComboBox>
                                                    <asp:SqlDataSource  ID="dsDepartment" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection%>"
                                                        SelectCommand="SELECT DepartmentID, Department FROM [tbl_TranscomUniversity_Lkp_Department] WHERE [HideFromList] = 0">
                                                    </asp:SqlDataSource>
                                                        
                                                    <br />
                                                    <asp:Label ID="lblAuthor" runat="server" Text="<%$ Resources: LocalizedResource, Author %>"></asp:Label>
                                                    <br />
                                                    <rad:RadComboBox RenderMode="Lightweight" ID="rcbAuthor" runat="server" Height="250px"
                                                        Width="300px" EmptyMessage="<%$ Resources: LocalizedResource, AnyAuthor %>" DataSourceID="dsAuthor" DataTextField="CIM_Number"
                                                        DataValueField="CIM_Number" EnableItemCaching="True" EnableAutomaticLoadOnDemand="true"
                                                        EnableVirtualScrolling="true" ItemsPerRequest="10" ToolTip="<%$ Resources: LocalizedResource, SelectAuthorCIMNumber %>" ZIndex="8001" ClientIDMode="Static" >
                                                    </rad:RadComboBox>
                                                    <asp:SqlDataSource runat="server" ID="dsAuthor" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
                                                        ProviderName="System.Data.SqlClient" SelectCommand="select CIM_Number from SAP.[dbo].[tbl_SAP_Hld_Export] where End_Date='00000000'">
                                                    </asp:SqlDataSource>
                                                        
                                                    <br />
                                                    <asp:label ID="Label5" text="<%$ Resources: LocalizedResource, StartendDates %>" runat="server" />
                                                    <br />
                                                        <rad:RadDatePicker ID="rdtStarEndDate" runat="server"
                                                    Width="250px" RenderMode="Lightweight" DateInput-EmptyMessage="<%$ Resources: LocalizedResource, AnyTime %>"
                                                    ToolTip="<%$ Resources: LocalizedResource, SetDateStartorEndDate %>" ZIndex="8001">
                                                    </rad:RadDatePicker>
                                                    <br />
                                                    <asp:label ID="Label6" text="<%$ Resources: LocalizedResource, Duration %>" runat="server" />
                                                    <br />
                                                    <rad:RadComboBox RenderMode="Lightweight" ID="rcbDuration" runat="server"
                                                    Width="300px" ToolTip="<%$ Resources: LocalizedResource, SelectRating %>" ZIndex="8001" ClientIDMode="Static" >
                                                    <DefaultItem Text="<%$ Resources: LocalizedResource, Select %>" Value="-1" />
                                                    </rad:RadComboBox>

                                                    <br />
                                                    <asp:Label ID="Label7" runat="server" Text="<%$ Resources: LocalizedResource, Rating %>"></asp:Label>
                                                    <br />
                                                    <rad:RadComboBox RenderMode="Lightweight" ID="rcbRating" runat="server"
                                                    Width="300px" ToolTip="<%$ Resources: LocalizedResource, SelectRating %>" ZIndex="8001" ClientIDMode="Static" >
                                                        <Items>
                                                            <rad:RadComboBoxItem Value="-1" Text="<%$ Resources: LocalizedResource, Select %>" runat="server" />
                                                            <rad:RadComboBoxItem Value="1" Text="1" runat="server" />
                                                            <rad:RadComboBoxItem Value="2" Text="2" runat="server" />
                                                            <rad:RadComboBoxItem Value="3" Text="3" runat="server" />
                                                            <rad:RadComboBoxItem Value="4" Text="4" runat="server" />
                                                            <rad:RadComboBoxItem Value="5" Text="5" runat="server" />
                                                        </Items>
                                                    </rad:RadComboBox>

                                                    <br />
                                                    <div class="row" style="margin: 10px 0 10px 0;">
                                                        <asp:LinkButton ID="btnComplexSearch" runat="server" CssClass="btn btn-teal" OnClientClick="searchCourse(); return false;">
                                                            <i class="glyphicon glyphicon-search"></i>
                                                        </asp:LinkButton>
                                                        <%--<button validationgroup="ComplexSearch" class="btn btn-teal" type="submit" onclick="searchCourse(); return false;"><i class="glyphicon glyphicon-search"></i></button>--%>
                                                    </div>
                                                </asp:Panel>
                                            <%-- </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="btnSearchSettings" EventName="Click"></asp:AsyncPostBackTrigger>
                                            </Triggers>
                                        </asp:UpdatePanel>--%>
                                    </rad:RadToolTip>
                                    </div>
                                    <div class="input-group-btn">
                                        <button class="btn btn-success" type="submit" onclick="simpleSearchCourse(); return false;"><i class="glyphicon glyphicon-search"></i></button>
                                    </div>
                                    <script type="text/javascript">
                                        function enterPressed(e) {
                                            if (e.keyCode == 13) {
                                                simpleSearchCourse();
                                                return false;
                                            }
                                        }

                                        function searchCourse() {
                                            try {
                                                var words = encodeURI(escapeRegExp($('#txtWords').val().trim()));
                                                words = words == "" ? "" : "words=" + words + "&";

                                                var phrase = encodeURI(escapeRegExp($('#txtPhrase').val().trim()));
                                                phrase = phrase == "" ? "" : "phrase=" + phrase + "&";

                                                var lookin = "";
                                                var cbLookIn = $find("<%= rlbLookIn.ClientID %>").get_checkedItems();
                                                for (var i = 0; i < cbLookIn.length; i++) {
                                                    lookin += cbLookIn[i].get_value();
                                                }
                                                var temp_lookin = encodeURI(escapeRegExp(lookin.trim()));
                                                lookin = encodeURI(escapeRegExp(lookin.trim()));
                                                lookin = lookin == "" ? "" : "lookin=" + lookin + "&";

                                                var tempdu = $find("<%= rdtDateUploaded.ClientID %>").get_selectedDate();
                                                var dateupload = encodeURI(escapeRegExp(tempdu == null ? "" : tempdu.toString()));
                                                dateupload = dateupload == "" ? "" : "dateupload=" + dateupload + "&";

                                                var department = encodeURI(escapeRegExp($find("<%= rcbDepartment.ClientID %>").get_text()));
                                                department = department == "" ? "" : "department=" + department + "&";

                                                var author = encodeURI(escapeRegExp($find("<%= rcbAuthor.ClientID %>").get_value()));
                                                author = author == "" ? "" : "author=" + author + "&";

                                                var tempse = $find("<%= rdtStarEndDate.ClientID %>").get_selectedDate();
                                                var startenddate = encodeURI(escapeRegExp(tempse == null ? "" : tempse.toString()));
                                                startenddate = startenddate == "" ? "" : "startenddate=" + startenddate + "&"; ;

                                                var duration = encodeURI(escapeRegExp($find("<%= rcbDuration.ClientID %>").get_value() == "-1" ? "" : $find("<%= rcbDuration.ClientID %>").get_value()));
                                                duration = duration == "" ? "" : "duration=" + duration + "&";

                                                var rating = encodeURI(escapeRegExp($find("<%= rcbRating.ClientID %>").get_value() == "-1" ? "" : $find("<%= rcbRating.ClientID %>").get_value()));
                                                rating = rating == "" ? "" : "rating=" + rating + "&";
                                                //alert($find("<%= rcbDuration.ClientID %>").get_value());
                                                //console.log("words = " + words);
                                                //console.log("phrase = " + phrase);
                                                //console.log("lookin = " + lookin);
                                                //console.log("dateupload = " + dateupload);
                                                //console.log("department = " + department);
                                                //console.log("author = " + author);
                                                //console.log("startenddate = " + startenddate);
                                                //console.log("duration = " + duration);
                                                //console.log("rating = " + rating);

                                                var fullSearch = words + phrase + lookin + dateupload + department + author + startenddate + duration + rating;

                                                if (temp_lookin != "")
                                                    $(location).attr('href', 'SearchResult.aspx?' + fullSearch);
                                                else {
                                                    $("#lblLookinError").removeClass("display-none").focus();
                                                }




                                            } catch (e) {
                                                console.log(e);
                                                $('input[name=q]').focus();
                                                $('input[name=q]').css({ "border": "1px solid red" });
                                            }

                                            return false;
                                        }



                                        function simpleSearchCourse() {
                                            try {
                                                var q = encodeURI(escapeRegExp($('input[name=q]').val().trim()));

                                                if (q == "") {
                                                    $('input[name=q]').focus();
                                                } else {
                                                    $(location).attr('href', 'SearchResult.aspx?q=' + q);
                                                }

                                                //$(location).attr('href', 'SearchResult.aspx?' + fullSearch);
                                            } catch (e) {
                                                console.log(e);
                                                $('input[name=q]').focus();
                                                $('input[name=q]').css({ "border": "1px solid red" });
                                            }

                                            return false;
                                        }

                                        function escapeRegExp(str) {
                                            return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
                                        }

                                        function showSearchToolTip() {
                                            window.setTimeout(function () {
                                                var tooltip = $find("<%= tooltipSearch.ClientID %>");

                                                Telerik.Web.UI.RadToolTip.prototype._getPosRelativeToMouse = function (targetBounds) {
                                                    var elemX = targetBounds.x;
                                                    var elemY = targetBounds.y;

                                                    //Get last recorded mouse position
                                                    var pos = this._getMousePosition();
                                                    var mouseX = pos.clientX;
                                                    var mouseY = pos.clientY;

                                                    //Take into consideration the offsetScroll!
                                                    var standard = $telerik.standardsMode;
                                                    //$telerik.standardsMode does not do a good job! Extra check is needed for FF!!
                                                    //And yet another check needed for Safari! It should always be set to false in order to get the calculations right
                                                    if (!$telerik.isIE && document.compatMode != "CSS1Compat") standard = false;
                                                    else if ($telerik.isSafari && !(Telerik.Web.Browser.chrome && Telerik.Web.Browser.version >= 61)) standard = false;

                                                    if (standard) {
                                                        elemX -= $telerik.getCorrectScrollLeft(document.documentElement);
                                                        elemY -= document.documentElement.scrollTop;
                                                    }
                                                    else //NEW: Add support for quircksmode
                                                    {
                                                        elemX -= $telerik.getCorrectScrollLeft(document.body);
                                                        elemY -= document.body.scrollTop;
                                                    }

                                                    //Calculate the position of the mouse, relative to the targetcontrol
                                                    var deltaX = mouseX - elemX;
                                                    var deltaY = mouseY - elemY;

                                                    return { x: deltaX, y: deltaY };
                                                }

                                                //API: show the tooltip
                                                //                                                    var button = $find("<%= btnSearchSettings.ClientID %>");
                                                //                                                    tooltip.set_targetControl(button);
                                                //                                                    tooltip.show();
                                                //                                                    window.setTimeout(function () { tooltip.set_targetControl(null); }, 100);  
                                                tooltip.show();
                                            }, 10);
                                        }
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
        </nav>
        <div class="container-fluid no-paddings" id="main-content">
            <ucHome:Home ID="Home1" runat="server" EnableViewState="true" />
        </div>
    </div>
    <footer class="bold m-login-footer container">
            <ucFooter:footer ID="Footer1" runat="server" />
    </footer>
    <rad:RadScriptManager ID="ScriptManager1" runat="server" LoadScriptsBeforeUI="false"
        ScriptMode="Release" EnablePartialRendering="true" EnableTheming="True" AsyncPostBackTimeout="360000"
        EnablePageMethods="true">
    </rad:RadScriptManager>
    
    <script src="JS/bootstrap.min.js"></script>
    <script src="JS/Plugins/Slick/slick.min.js" type="text/javascript"></script>
    </form>

</body>
</html>
