﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Membership.OpenAuth;
using Telerik.Web.UI;
using System.Data;

public partial class Login : BasePage
{
    public string ReturnUrl { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        ReturnUrl = Request.QueryString["ReturnUrl"];
        Session["SessionRoles"] = "Learner";
        if (ReturnUrl == null) {
            ReturnUrl = "Home.aspx";
        }
        if (DataHelper.isLoggedIn())
            Response.Redirect(ReturnUrl);

    }

    public IEnumerable<ProviderDetails> GetProviderNames()
    {
        return OpenAuth.AuthenticationClients.GetAll();
    }

    protected void btnRequestLogin_Click(object sender, EventArgs e)
    {
        var redirectUrl = "~/ExternalLandingPage.aspx";

        if (!String.IsNullOrEmpty(ReturnUrl))
        {
            var resolvedReturnUrl = ResolveUrl(ReturnUrl);
            redirectUrl += "?ReturnUrl=" + HttpUtility.UrlEncode(resolvedReturnUrl);
        }

        OpenAuth.RequestAuthentication("google", redirectUrl);
    }
}