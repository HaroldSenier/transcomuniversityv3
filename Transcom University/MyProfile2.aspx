﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TranscomUniversityV3.Master" AutoEventWireup="true"
    CodeFile="MyProfile2.aspx.cs" Inherits="MyProfile2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="contentPlaceHolderLeftPanel" runat="Server">
    <rad:RadAjaxLoadingPanel ID="fullPageLoading" runat="server" Transparency="25" IsSticky="true"
        CssClass="Loading" />
    <rad:RadAjaxLoadingPanel ID="localLoading" runat="server" CssClass="Loading2" Transparency="25" />
    <rad:RadAjaxLoadingPanel ID="noLoadingPanel" runat="server" CssClass="Loading-Empty" />
    <rad:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <rad:AjaxSetting AjaxControlID="btnChangeCoverPhoto">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="btnChangeCoverPhoto" UpdatePanelRenderMode="Inline"
                        LoadingPanelID="fullPageLoading" />
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnRemoveCoverPhoto">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" UpdatePanelRenderMode="Inline"
                        LoadingPanelID="fullPageLoading" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnEditProfile">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="btnEditProfile" UpdatePanelRenderMode="Inline"
                        LoadingPanelID="fullPageLoading" />
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnUpdateProfile">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="btnUpdateProfile" UpdatePanelRenderMode="Inline"
                        LoadingPanelID="fullPageLoading" />
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnCancel">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="btnCancel" UpdatePanelRenderMode="Inline" LoadingPanelID="fullPageLoading" />
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </rad:AjaxSetting>
       <%--     <rad:AjaxSetting AjaxControlID="LinkBtnUploadCert">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="LinkBtnUploadCert" UpdatePanelRenderMode="Inline"
                        LoadingPanelID="fullPageLoading" />
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </rad:AjaxSetting>--%>
          <%--  <rad:AjaxSetting AjaxControlID="cbCourseList">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="cbCourseList" UpdatePanelRenderMode="Inline" LoadingPanelID="fullPageLoading" />
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </rad:AjaxSetting>--%>
            <rad:AjaxSetting AjaxControlID="cbSelectDept">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="cbSelectDept" UpdatePanelRenderMode="Inline" LoadingPanelID="fullPageLoading" />
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="targetElement">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadToolTip2" UpdatePanelRenderMode="Inline" LoadingPanelID="fullPageLoading" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="RadTabStrip1">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadTabStrip1" />
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" LoadingPanelID="fullPageLoading" />
                </UpdatedControls>
            </rad:AjaxSetting>
        </AjaxSettings>
    </rad:RadAjaxManager>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolderMain" runat="Server">
    <rad:RadToolTip ID="RadToolTip1" runat="server" Modal="false" RenderMode="Lightweight"
        ShowEvent="FromCode" HideEvent="ManualClose" Position="BottomLeft" RelativeTo="Element"
        TargetControlID="btnProfileSettings" Skin="Silk">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="pnlProfileSettings" runat="server">
                    <asp:HiddenField runat="server" ID="EditProfileStatus" />
                    <div class="bg-color-light-teal color-white font-bold five-px-padding">
                        <asp:Label ID="Label1" runat="server" Font-Bold="true" Text="<%$ Resources: LocalizedResource, ProfileSettings %>" />
                    </div>
                    <br />
                    <table>
                        <tr>
                            <td style="width: 100%">
                                <asp:Label ID="Label2" runat="server" Font-Bold="true" Text="<%$ Resources: LocalizedResource, ProfileDetails %>" />:
                                <br />
                                <div style="padding-top: 1px; padding-bottom: 1px;" class="form-group">
                                    <asp:Button ID="btnEditProfile" runat="server" CssClass="btn btn-teal" Text="<%$ Resources: LocalizedResource, EditMyProfileDetails %>"
                                        Width="165px" OnClientClick="btnEditProfile_Click(); return false;" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100%">
                                <asp:Label ID="Label3" runat="server" Font-Bold="true" Text="<%$ Resources: LocalizedResource, ProfileBackground %>" />:
                                <br />
                                <div style="padding-top: 1px; padding-bottom: 1px;" class="form-group">
                                    <asp:Button ID="btnChangeCoverPhoto" runat="server" CssClass="btn btn-teal" Text="<%$ Resources: LocalizedResource, Change %>"
                                        Width="165px" OnClick="btnChangeCoverPhoto_Click" />
                                </div>
                                <div style="padding-top: 1px; padding-bottom: 1px;" class="form-group">
                                    <asp:Button ID="btnRemoveCoverPhoto" runat="server" CssClass="btn btn-teal" Text="<%$ Resources: LocalizedResource, Remove %>"
                                        Width="165px" OnClick="btnRemoveCoverPhoto_Click" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnProfileSettings" EventName="Click"></asp:AsyncPostBackTrigger>
            </Triggers>
        </asp:UpdatePanel>
    </rad:RadToolTip>
    <div class="col-lg-12">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12 thirty-px-padding">
                        <div class="col-md-12 no-paddings">
                            <ul id="ulBreadcrumb" runat="server" class="breadcrumb">
                                <li><a>
                                    <asp:Label ID="lblTab1" runat="server" Text="<%$ Resources: LocalizedResource, MyProfile %>" /></a></li>
                                <li>
                                    <asp:Label ID="lblTab3" runat="server" Text="" /></li>
                            </ul>
                            <rad:RadCodeBlock ID="RadCodeBlock3" runat="server">
                                <div id="coverPhoto" runat="server" class="header-cover-photo">
                                    <div class="header-cover-photo-profile-pic-edit-icon">
                                        <asp:LinkButton ID="btnProfileSettings" runat="server" Font-Underline="false" CssClass="fa fa-pencil-square-o"
                                            aria-hidden="true"></asp:LinkButton>
                                    </div>
                                    <div class="header-cover-photo-profile-pic">
                                        <img id="imgProfilePic" class="img-circle border-circle-violet" width="150" src="<%= HttpContext.Current.User.Identity.Name.Split('|')[2] %>" />
                                    </div>
                                    <div class="header-cover-photo-profile-pic-details">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h2 class="color-whites">
                                                    <asp:Label ID="lblFirstName" runat="server"></asp:Label>&nbsp;<asp:Label ID="lblLastName"
                                                        runat="server"></asp:Label></h2>
                                            </div>
                                            <div class="col-md-12">
                                                <h3 class="color-whites">
                                                    <asp:Label ID="lblRole" runat="server"></asp:Label></h3>
                                            </div>
                                            <div class="col-md-12">
                                                <h4 class="color-whites">
                                                    <asp:Label ID="lblCompanySite" runat="server"></asp:Label></h4>
                                            </div>
                                            <div class="col-md-12">
                                                <h4 class="color-whites">
                                                    <asp:Label ID="lblStatus" runat="server"></asp:Label></h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </rad:RadCodeBlock>
                        </div>
                        <div class="panelRadStrip">
                          <i class="fa fa-angle-left m-arrow-left margin-side-10" onclick='nav_scrollLeft(-1000);'
                                style="line-height: 42px; display: none"></i>
                            <rad:RadTabStrip RenderMode="Lightweight" ID="RadTabStrip1" OnClientTabSelecting="onTabSelecting"
                                SelectedIndex="0" runat="server" MultiPageID="RadMultiPage1" Skin="" Width="100%"
                                Align="Left" OnTabClick="RadTabStrip1_TabClick" CssClass="m-profile-nav-container">
                                <%-- <Tabs>
                                    <rad:RadTab Text="PROFILE" TabIndex="0">
                                    </rad:RadTab>
                                </Tabs>--%>
                            </rad:RadTabStrip>
                              <i class="fa fa-angle-right m-arrow-right margin-side-10" onclick='nav_scrollLeft(1000);'
                                style="line-height: 42px; display: none"></i>
                        </div>
                        <rad:RadMultiPage ID="RadMultiPage1" CssClass="RadMultiPage m-my-profile" runat="server" SelectedIndex="0"
                            OnPageViewCreated="RadMultiPage1_PageViewCreated" RenderMode="Lightweight"  Style="height: auto;">
                        </rad:RadMultiPage>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <rad:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true" RenderMode="Lightweight"
        Skin="Bootstrap">
        <Windows>
            <rad:RadWindow ID="RadWindow1" runat="server" RenderMode="Lightweight" Width="450px"
                Height="235px" Title="Change Cover Photo" Behaviors="Close" Modal="true" VisibleStatusbar="false">
                <ContentTemplate>
                    <div class="col-md-6">
                        <asp:Label ID="Label4" runat="server" Font-Bold="true" Text="<%$ Resources: LocalizedResource, Photo %>" />:
                    </div>
                    <asp:FileUpload ID="fileUpload" runat="server" ToolTip="Select your photo" CssClass="btn btn-default btn-file"
                        Width="380px" />
                    <br />
                    <div class="form-group">
                        <asp:Button ID="btnUploadCoverPhoto" runat="server" CssClass="btn btn-default" Text="<%$ Resources: LocalizedResource, Upload %>"
                            OnClick="btnUploadCoverPhoto_Click" />
                    </div>
                </ContentTemplate>
            </rad:RadWindow>
            <rad:RadWindow ID="RadWindow2" runat="server" RenderMode="Lightweight" Width="350px"
                Height="210px" Title="<%$ Resources: LocalizedResource, RemoveCoverPhoto %>" Behaviors="Close" Modal="true" VisibleStatusbar="false">
                <ContentTemplate>
                    <div class="col-md-6">
                        <asp:Label ID="Label5" runat="server" Font-Bold="true" Text="<%$ Resources: LocalizedResource, AreYouSureYouWantToDelete %>" />?
                    </div>
                    <br />
                    <br />
                    <br />
                    <div class="form-group">
                        <asp:Button ID="btnDeleteCoverPhoto" runat="server" CssClass="btn btn-default" Text="<%$ Resources: LocalizedResource, Delete %>"
                            OnClick="btnDeleteCoverPhoto_Click" />
                    </div>
                </ContentTemplate>
            </rad:RadWindow>
        </Windows>
    </rad:RadWindowManager>
    <rad:RadScriptBlock ID="RadScriptBlock2" runat="server">
        <script type="text/javascript" language="javascript">
            


            (function (global, undefined) {
                var toHide = true, tooltip1;

                function beginRequest(sender, args) {
                    toHide = false;
                    if (args.get_postBackElement().id.indexOf("btnProfileSettings") > -1) {
                        tooltip1.show();
                    }
                }

                function GetActiveToolTip() {
                    return Telerik.Web.UI.RadToolTip.getCurrent();
                }

                var telerikDemo = global.telerikDemo = {
                    onClientBeforeHide: function (sender, args) {
                        args.set_cancel(!toHide);
                    },
                    GetActiveToolTip: GetActiveToolTip
                };

                Sys.Application.add_load(function () {
                    tooltip1 = $find(telerikDemo.tooltip1ID);
                });

                var pageRequestManager = Sys.WebForms.PageRequestManager.getInstance();
                //Attach to event that will indicate when ajax request begins
                pageRequestManager.add_beginRequest(beginRequest);
                //Attach to event that will indicate when ajax request is complete
                //pageRequestManager.add_endRequest(endRequest);

            })(window);
        </script>
        <script type="text/javascript" language="javascript">
            telerikDemo.tooltip1ID = "<%=RadToolTip1.ClientID%>";
        </script>
        <script type="text/javascript" language="javascript">
            function openRadWindow1() {
                var radwindow = $find('<%=RadWindow1.ClientID %>');
                radwindow.show();
            }
        </script>
        <script type="text/javascript" language="javascript">
            function openRadWindow2() {
                var radwindow = $find('<%=RadWindow2.ClientID %>');
                radwindow.show();
            }
        </script>
        <script type="text/javascript">
            Sys.Application.add_load(function () {
                $('.progressBarContainer').each(function (i, obj) {
                    var hidValue = $('input[type=hidden]', this).val();
                    //alert(parseFloat(hidValue) + '%');
                    jQuery('[id=spanval]', this).text(parseFloat(hidValue) + '%');

                    var progressBarWidth = hidValue * $('#progressBar').width() / 100;
                    jQuery('[id=progress]', this).animate({ width: progressBarWidth }, 3000).html("&nbsp;");
                    //alert(progressBarWidth);

                    if (hidValue >= 0 && hidValue <= 50) {
                        jQuery('[id=progress]', this).addClass('red');
                    }
                    if (hidValue >= 50 && hidValue <= 75) {
                        jQuery('[id=progress]', this).addClass('yellow');
                    }
                    if (hidValue >= 75 && hidValue <= 100) {
                        jQuery('[id=progress]', this).addClass('green');
                    }
                });
            });
            function onTabSelecting(sender, args) {
                
                $(".Loading2").hide();
                
                if (args.get_tab().get_pageViewID()) {
                    args.get_tab().set_postBack(false);
                }
                var tabTitle = "";
                var tabIndex = args.get_tab().get_index()
                if (tabIndex == 0)
                    tabTitle = "My Profile Summary";
                else if (tabIndex == 1)
                    tabTitle = "Starc Assessment";
                else if (tabIndex == 2)
                    tabTitle = "Learning Path";
                else if (tabIndex == 3)
                    tabTitle = "Transcript";
                else if (tabIndex == 4)
                    tabTitle = "Achievements";
                $("#<%= lblTab3.ClientID %>").html(tabTitle);
            }

            function btnEditProfile_Click() {

                showPageLoading();
                selectTab("PROFILE");
                $("#btnHiddenEdit").click();             
                
            }

            function selectTab(text) {
                var tabStrip = $find("<%= RadTabStrip1.ClientID %>");
                var tab = tabStrip.findTabByText(text);
                if (tab) {
                    tab.select();
                    console.log(text);
                }               
            } 

            function showPageLoading() {
                //console.log("show loading");
                var panel = $find("<%= fullPageLoading.ClientID %>");
                panel.show("btnHiddenEdit");
            }

            function hidePageLoading() {
                console.log("hide page loading");
                var panel = $find("<%= fullPageLoading.ClientID %>");
                panel.hide("btnHiddenEdit");
            }
        

            function nav_scrollLeft(left) {
                $('.m-profile-nav-container ul').animate({ scrollLeft: left }, 800);


            }


        </script>
        
    </rad:RadScriptBlock>
</asp:Content>
