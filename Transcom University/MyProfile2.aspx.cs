﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TranscomUniversityV3Model;
using Telerik.Web.UI;

public partial class MyProfile2 : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            int tabIndex = RadTabStrip1.SelectedIndex;
            if (tabIndex == 0)
                lblTab3.Text = Resources.LocalizedResource.MyProfileSummary.ToString().ToUpper();
            else if (tabIndex == 1)
                lblTab3.Text = Resources.LocalizedResource.StarcAssessment.ToString().ToUpper();
            else if (tabIndex == 2)
                lblTab3.Text = Resources.LocalizedResource.LearningPath.ToString().ToUpper();
            else if (tabIndex == 3)
                lblTab3.Text = Resources.LocalizedResource.Transcripts.ToString().ToUpper();
            else if (tabIndex == 4)
                lblTab3.Text = Resources.LocalizedResource.Achievements.ToString().ToUpper();
            btnEditProfile.Enabled = true;
            SetDetailsOnView();
            //SetDetailsTrue();
        }
       
    }


    public void SetDetailsOnView()
    {
        try
        {
            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                
                var userInfo = DataHelper.GetUsersInfo();

                lblFirstName.Text = userInfo[0].FirstName;
                lblLastName.Text = userInfo[0].LastName;
                lblRole.Text = userInfo[0].Role;
                lblCompanySite.Text = userInfo[0].CompanySite;
                lblStatus.Text = userInfo[0].SAP_STATUS;
                //imgCoverPhoto.Src = "Resources/PhotoHandler.ashx?id=" + userInfo[0].CIMNumber.ToString();
                //coverPhoto.Attributes["style"] = "background-image: url(" + "Resources/PhotoHandler.ashx?id=" + ((decimal)userInfo[0].CIMNumber).ToString() + ")";
                coverPhoto.Attributes["style"] = "background-image: url('" + "Media/Uploads/Profile/" +userInfo[0].GoogleID +"/" + userInfo[0].UploadedPhoto + "'";
            }
        }
        catch
        {
            //RadWindowManager1.RadAlert("Error updating your profile. Please contact your System Administrator.", 330, 180, "Error Message", "");
            RadWindowManager1.RadAlert("Failed to update your Profile.", 330, 180, "Error Message", "");
        }
    }


    //protected void btnEditProfile_Click(object sender, EventArgs e)
    //{
    //    Button btnEdit = this.NamingContainer.FindControl("btnHiddenEdit") as Button;
    //    btnEdit.Click();
    //    //SetDetailsOnEdit();
    //    //SetDetailsFalse();
    //}

    protected void btnChangeCoverPhoto_Click(object sender, EventArgs e)
    {
        showUpload();
    }

    protected void btnRemoveCoverPhoto_Click(object sender, EventArgs e)
    {
        string script = "function f(){openRadWindow2(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
        ScriptManager.RegisterStartupScript(Page, typeof(Page), "RemoveCoverkey", script, true);
    }

    void showUpload() {
        string script = "function f(){openRadWindow1(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
        ScriptManager.RegisterStartupScript(Page, typeof(Page), "ChangeCoverkey", script, true);
    }
    protected void btnDeleteCoverPhoto_Click(object sender, EventArgs e)
    {
        try
        {
            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                var googleId = DataHelper.GetGoogleID();

                db.pr_TranscomUniversity_DeleteCoverPhoto(googleId);

            }

            //remove bg image
            //coverPhoto.Attributes["style"] = "background-image: url(" + "Resources/PhotoHandler.ashx?id=" + (DataHelper.GetCurrentUserCIM()).ToString() + ")";
            coverPhoto.Attributes["style"] = "background-image: url(NONE)";
            RadWindowManager1.RadAlert("Cover Photo remove.", 330, 180, "Message", "");
        }
        catch
        {
            //RadWindowManager1.RadAlert("Error deleting your cover photo. Please contact your System Administrator.", 330, 180, "Error Message", "");
            RadWindowManager1.RadAlert("Failed to Delete your cover photo.", 330, 180, Resources.LocalizedResource.ErrorMessage.ToString(), "");
        }
    }

    private static bool IsImage(HttpPostedFile file)
    {
        return ((file != null) && System.Text.RegularExpressions.Regex.IsMatch(file.ContentType, "image/\\S+") && (file.ContentLength > 0));
    }

    protected void btnUploadCoverPhoto_Click(object sender, EventArgs e)
    {
        var file = fileUpload.PostedFile;
        //var bytes = new byte[] { };

        if ((file != null) && (file.ContentLength > 0))
        {
            if (IsImage(file) == false)
            {

                RadWindowManager1.RadAlert(Resources.LocalizedResource.OnlyJPGJPEGorPNGformatwillbeaccepted.ToString(), 330, 180, Resources.LocalizedResource.ErrorMessage.ToString(), "");
                return;
            }

            var iFileSize = file.ContentLength;

            if (iFileSize > 2097152)
            {
                RadWindowManager1.RadAlert(Resources.LocalizedResource.Maximumof2MBfilesizewillbeacceptedYouaretryingtouploadlargerthanthelimit.ToString(), 330, 180, Resources.LocalizedResource.ErrorMessage.ToString(), "");
                return;
            }

            //bytes = new byte[file.ContentLength];
            //file.InputStream.Read(bytes, 0, file.ContentLength);   

            try
            {
                using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
                {
                    var googleId = DataHelper.GetGoogleID();
                    var pathString = Server.MapPath("~/Media/Uploads/Profile/").ToString() + googleId;
                    System.IO.Directory.CreateDirectory(pathString);
                    fileUpload.SaveAs(pathString + "/" + file.FileName);
                    db.pr_TranscomUniversity_UpdateCoverPhoto(googleId, file.FileName.ToString());
                    coverPhoto.Attributes["style"] = "background-image: url('" + "Media/Uploads/Profile/" + googleId + "/" + file.FileName + "'";
                }

                RadWindowManager1.RadAlert(Resources.LocalizedResource.CoverPhotoSuccessfullyUpdated.ToString(), 330, 180, Resources.LocalizedResource.CoverPhotoUpdated.ToString(), "", "");

            }
            catch
            {
                //RadWindowManager1.RadAlert("Error updating your cover photo. Please contact your System Administrator.", 330, 180, "Error Message", "");
                RadWindowManager1.RadAlert("Failed to update your cover photo.", 330, 180, Resources.LocalizedResource.ErrorMessage.ToString(), "");
            }
        }
        else
        {

            RadWindowManager1.RadAlert(Resources.LocalizedResource.FailedtoupdateyourcovephotoPleaseSelectafile.ToString(), 330, 180, Resources.LocalizedResource.ErrorMessage.ToString(), "openRadWindow1");         
        }
    }

    //public void SetDetailsFalse()
    //{
    //    lblRegion.Visible = false;
    //    lblCountry.Visible = false;
    //    lblDepartment.Visible = false;
    //    lblClient.Visible = false;
    //    lblCampaign.Visible = false;
    //    lblPrimaryLanguage.Visible = false;
    //    lblMobileNumber.Visible = false;
    //    lblLandline.Visible = false;
    //    lblPersonalEmail.Visible = false;
    //    lblPrimaryEduc.Visible = false;
    //    lblPrimaryPeriodFrom.Visible = false;
    //    lblPrimaryPeriodTo.Visible = false;
    //    lblSecondaryEduc.Visible = false;
    //    lblSecondaryPeriodFrom.Visible = false;
    //    lblSecondaryPeriodTo.Visible = false;
    //    lblTertiaryEduc.Visible = false;
    //    lblTertiaryPeriodFrom.Visible = false;
    //    lblTertiaryPeriodTo.Visible = false;
    //    lblCourse.Visible = false;
    //    lblPreviousJob.Visible = false;
    //    lblCompanyName.Visible = false;
    //    lblJobDesc.Visible = false;

    //    cbRegion.Visible = true;
    //    cbCountry.Visible = true;
    //    cbDepartment.Visible = true;
    //    cbClient.Visible = true;
    //    cbCampaign.Visible = true;
    //    cbPrimaryLanguage.Visible = true;
    //    txtMobileNumber.Visible = true;
    //    txtLandline.Visible = true;
    //    txtPersonalEmail.Visible = true;
    //    txtPrimaryEduc.Visible = true;
    //    dpPrimaryPeriodFrom.Visible = true;
    //    dpPrimaryPeriodTo.Visible = true;
    //    txtSecondaryEduc.Visible = true;
    //    dpSecondaryPeriodFrom.Visible = true;
    //    dpSecondaryPeriodTo.Visible = true;
    //    txtTertiaryEduc.Visible = true;
    //    dpTertiaryPeriodFrom.Visible = true;
    //    dpTertiaryPeriodTo.Visible = true;
    //    txtCourse.Visible = true;
    //    txtPreviousJob.Visible = true;
    //    txtCompanyName.Visible = true;
    //    txtJobDesc.Visible = true;
    //    btnUpdateProfile.Visible = true;
    //    btnCancel.Visible = true;
    //}

    //#region MyProfileSummary

 

    //protected void btnEditProfile_Click(object sender, EventArgs e)
    //{
    //    SetDetailsOnEdit();
    //    SetDetailsFalse();
    //}

    //protected void btnChangeCoverPhoto_Click(object sender, EventArgs e)
    //{
    //    string script = "function f(){openRadWindow1(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
    //    ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", script, true);
    //}

   

    //protected void btnRemoveCoverPhoto_Click(object sender, EventArgs e)
    //{
    //    string script = "function f(){openRadWindow2(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
    //    ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", script, true);
    //}

    //protected void btnDeleteCoverPhoto_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
    //        {
    //            var googleId = DataHelper.GetGoogleID();

    //            db.pr_TranscomUniversity_DeleteCoverPhoto(googleId);
    //        }

    //        SetDetailsOnView();
    //    }
    //    catch
    //    {
    //        RadWindowManager1.RadAlert("Error deleting your cover photo. Please contact your System Administrator.", 330, 180, "Error Message", "");
    //    }
    //}

    //protected void btnUpdateProfile_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
    //        {
    //            var googleId = DataHelper.GetGoogleID();

    //            db.pr_TranscomUniversity_UpdateUsersInfo(googleId, Convert.ToInt32(lblCIMNumber.Text), lblFirstName.Text, lblLastName.Text,
    //                lblGender.Text == "Male" ? 1 : 2, DateTime.Parse(lblDateOfBirth.Text), DateTime.Parse(lblStartDateWithTranscom.Text),
    //                Convert.ToInt32(cbPrimaryLanguage.SelectedValue), txtMobileNumber.Text, txtLandline.Text, lblTranscomEmail.Text,
    //                txtPersonalEmail.Text, lblRole.Text, lblSupervisor.Text, Convert.ToInt32(cbRegion.SelectedValue), Convert.ToInt32(cbCountry.SelectedValue),
    //                Convert.ToInt32(cbDepartment.SelectedValue), Convert.ToInt32(cbClient.SelectedValue), Convert.ToInt32(cbCampaign.SelectedValue),
    //                txtPrimaryEduc.Text, dpPrimaryPeriodFrom.SelectedDate != null ? Convert.ToDateTime(dpPrimaryPeriodFrom.SelectedDate) : DateTime.Today,
    //                dpPrimaryPeriodTo.SelectedDate != null ? Convert.ToDateTime(dpPrimaryPeriodTo.SelectedDate) : DateTime.Today.AddDays(1), txtSecondaryEduc.Text,
    //                dpSecondaryPeriodFrom.SelectedDate != null ? Convert.ToDateTime(dpSecondaryPeriodFrom.SelectedDate) : DateTime.Today,
    //                dpSecondaryPeriodTo.SelectedDate != null ? Convert.ToDateTime(dpSecondaryPeriodTo.SelectedDate) : DateTime.Today.AddDays(1),
    //                txtTertiaryEduc.Text, dpTertiaryPeriodFrom.SelectedDate != null ? Convert.ToDateTime(dpTertiaryPeriodFrom.SelectedDate) : DateTime.Today,
    //                dpTertiaryPeriodTo.SelectedDate != null ? Convert.ToDateTime(dpTertiaryPeriodTo.SelectedDate) : DateTime.Today.AddDays(1),
    //                txtCourse.Text, txtPreviousJob.Text, txtCompanyName.Text, txtJobDesc.Text);
    //        }

    //        RadWindowManager1.RadAlert("Profile Successfully Updated.", 330, 180, "Profile Updated", "", "");
    //    }
    //    catch
    //    {
    //        RadWindowManager1.RadAlert("Error updating your profile. Please contact your System Administrator.", 330, 180, "Error Message", "");
    //    }

    //    SetDetailsClear();
    //    SetDetailsOnView();
    //    SetDetailsTrue();
    //}

    //protected void btnCancel_Click(object sender, EventArgs e)
    //{
    //    SetDetailsOnView();
    //    SetDetailsTrue();
    //}

    //public void SetDetailsClear()
    //{
    //    cbRegion.Items.Clear();
    //    cbCountry.Items.Clear();
    //    cbDepartment.Items.Clear();
    //    cbClient.Items.Clear();
    //    cbCampaign.Items.Clear();
    //    cbPrimaryLanguage.Items.Clear();
    //    txtMobileNumber.Text = "";
    //    txtLandline.Text = "";
    //    txtPersonalEmail.Text = "";
    //    txtPrimaryEduc.Text = "";
    //    dpPrimaryPeriodFrom.Clear();
    //    dpPrimaryPeriodTo.Clear();
    //    txtSecondaryEduc.Text = "";
    //    dpSecondaryPeriodFrom.Clear();
    //    dpSecondaryPeriodTo.Clear();
    //    txtTertiaryEduc.Text = "";
    //    dpSecondaryPeriodFrom.Clear();
    //    dpSecondaryPeriodTo.Clear();
    //    txtCourse.Text = "";
    //    txtPreviousJob.Text = "";
    //    txtCompanyName.Text = "";
    //    txtJobDesc.Text = "";
    //}

    //public void SetDetailsTrue()
    //{
    //    lblRegion.Visible = true;
    //    lblCountry.Visible = true;
    //    lblDepartment.Visible = true;
    //    lblClient.Visible = true;
    //    lblCampaign.Visible = true;
    //    lblPrimaryLanguage.Visible = true;
    //    lblMobileNumber.Visible = true;
    //    lblLandline.Visible = true;
    //    lblPersonalEmail.Visible = true;
    //    lblPrimaryEduc.Visible = true;
    //    lblPrimaryPeriodFrom.Visible = true;
    //    lblPrimaryPeriodTo.Visible = true;
    //    lblSecondaryEduc.Visible = true;
    //    lblSecondaryPeriodFrom.Visible = true;
    //    lblSecondaryPeriodTo.Visible = true;
    //    lblTertiaryEduc.Visible = true;
    //    lblTertiaryPeriodFrom.Visible = true;
    //    lblTertiaryPeriodTo.Visible = true;
    //    lblCourse.Visible = true;
    //    lblPreviousJob.Visible = true;
    //    lblCompanyName.Visible = true;
    //    lblJobDesc.Visible = true;

    //    cbRegion.Visible = false;
    //    cbCountry.Visible = false;
    //    cbDepartment.Visible = false;
    //    cbClient.Visible = false;
    //    cbCampaign.Visible = false;
    //    cbPrimaryLanguage.Visible = false;
    //    txtMobileNumber.Visible = false;
    //    txtLandline.Visible = false;
    //    txtPersonalEmail.Visible = false;
    //    txtPrimaryEduc.Visible = false;
    //    dpPrimaryPeriodFrom.Visible = false;
    //    dpPrimaryPeriodTo.Visible = false;
    //    txtSecondaryEduc.Visible = false;
    //    dpSecondaryPeriodFrom.Visible = false;
    //    dpSecondaryPeriodTo.Visible = false;
    //    txtTertiaryEduc.Visible = false;
    //    dpTertiaryPeriodFrom.Visible = false;
    //    dpTertiaryPeriodTo.Visible = false;
    //    txtCourse.Visible = false;
    //    txtPreviousJob.Visible = false;
    //    txtCompanyName.Visible = false;
    //    txtJobDesc.Visible = false;
    //    btnUpdateProfile.Visible = false;
    //    btnCancel.Visible = false;
    //}

    //public void SetDetailsFalse()
    //{
    //    lblRegion.Visible = false;
    //    lblCountry.Visible = false;
    //    lblDepartment.Visible = false;
    //    lblClient.Visible = false;
    //    lblCampaign.Visible = false;
    //    lblPrimaryLanguage.Visible = false;
    //    lblMobileNumber.Visible = false;
    //    lblLandline.Visible = false;
    //    lblPersonalEmail.Visible = false;
    //    lblPrimaryEduc.Visible = false;
    //    lblPrimaryPeriodFrom.Visible = false;
    //    lblPrimaryPeriodTo.Visible = false;
    //    lblSecondaryEduc.Visible = false;
    //    lblSecondaryPeriodFrom.Visible = false;
    //    lblSecondaryPeriodTo.Visible = false;
    //    lblTertiaryEduc.Visible = false;
    //    lblTertiaryPeriodFrom.Visible = false;
    //    lblTertiaryPeriodTo.Visible = false;
    //    lblCourse.Visible = false;
    //    lblPreviousJob.Visible = false;
    //    lblCompanyName.Visible = false;
    //    lblJobDesc.Visible = false;

    //    cbRegion.Visible = true;
    //    cbCountry.Visible = true;
    //    cbDepartment.Visible = true;
    //    cbClient.Visible = true;
    //    cbCampaign.Visible = true;
    //    cbPrimaryLanguage.Visible = true;
    //    txtMobileNumber.Visible = true;
    //    txtLandline.Visible = true;
    //    txtPersonalEmail.Visible = true;
    //    txtPrimaryEduc.Visible = true;
    //    dpPrimaryPeriodFrom.Visible = true;
    //    dpPrimaryPeriodTo.Visible = true;
    //    txtSecondaryEduc.Visible = true;
    //    dpSecondaryPeriodFrom.Visible = true;
    //    dpSecondaryPeriodTo.Visible = true;
    //    txtTertiaryEduc.Visible = true;
    //    dpTertiaryPeriodFrom.Visible = true;
    //    dpTertiaryPeriodTo.Visible = true;
    //    txtCourse.Visible = true;
    //    txtPreviousJob.Visible = true;
    //    txtCompanyName.Visible = true;
    //    txtJobDesc.Visible = true;
    //    btnUpdateProfile.Visible = true;
    //    btnCancel.Visible = true;
    //}

    

    //#endregion

    #region TabMaker

    protected void Page_Init(object sender, System.EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            AddTab(Resources.LocalizedResource.Profile.ToString().ToUpper());
            AddPageView(RadTabStrip1.FindTabByText(Resources.LocalizedResource.Profile.ToString().ToUpper()));
            AddTab(Resources.LocalizedResource.StarcAssessment.ToString().ToUpper());
            AddTab(Resources.LocalizedResource.LearningPath.ToString().ToUpper());
            AddTab(Resources.LocalizedResource.Transcripts.ToString().ToUpper());
            AddTab(Resources.LocalizedResource.Achievements.ToString().ToUpper());
        }
    }

    private void AddTab(string tabName)
    {
        RadTab tab = new RadTab();
        tab.Text = tabName.Replace(" ", string.Empty);
        //tab.Width = Unit.Pixel(200);
        tab.CssClass = "selectedTab";
        RadTabStrip1.Tabs.Add(tab);


        string language = String.Empty;
        try
        {
            language = Session["CurrLang"].ToString();
        }
        catch
        { language = null; }
        if(language == null || language == "en-US")
         RadTabStrip1.Tabs.FindTabByText(tab.Text).Text = pageName(tabName);
    }

    private string pageName(string PageName)
    {
        if (PageName.ToUpper().Replace(" ", string.Empty) == Resources.LocalizedResource.LearningPath.ToString().ToUpper().Replace(" ", string.Empty))
        {
            PageName = "LEARNING PLAN";
        }
        else if (Resources.LocalizedResource.Profile.ToString().ToUpper().Replace(" ", string.Empty) == PageName.ToUpper().Replace(" ", string.Empty))
        {
            PageName = "PROFILE";
        }
        else if (Resources.LocalizedResource.StarcAssessment.ToString().ToUpper().Replace(" ", string.Empty) == PageName.ToUpper().Replace(" ", string.Empty))
        {
            PageName = "STARC ASSESSMENT";
        }
        else if (Resources.LocalizedResource.Transcripts.ToString().ToUpper().Replace(" ", string.Empty) == PageName.ToUpper().Replace(" ", string.Empty))
        {
            PageName = "TRANSCRIPTS";
        }
        else if (Resources.LocalizedResource.Achievements.ToString().ToUpper().Replace(" ", string.Empty) == PageName.ToUpper().Replace(" ", string.Empty))
        {
            PageName = "ACHIEVEMENTS";
        }

        return PageName;
    }

    protected void RadMultiPage1_PageViewCreated(object sender, RadMultiPageEventArgs e)
    {
        string PageName = e.PageView.ID;

        string name = pageName(PageName);

        string userControlName = "~/UserControl/Pages/MyProfile_" + name.Replace(" ", string.Empty) + ".ascx";

        Control userControl = Page.LoadControl(userControlName);
        userControl.ID = e.PageView.ID + "_userControl";

        e.PageView.Controls.Add(userControl);
    }

    private void AddPageView(RadTab tab)
    {       
        RadPageView pageView = new RadPageView();
        pageView.ID = tab.Text.Replace(" ", "");
        pageView.CssClass = "contentWrapper" + tab.Index;
        RadMultiPage1.PageViews.Add(pageView);
        tab.PageViewID = pageView.ID.Replace(" ", "");        
    }

    protected void RadTabStrip1_TabClick(object sender, RadTabStripEventArgs e)
    {
        AddPageView(e.Tab);
        e.Tab.PageView.Selected = true;
    }

    #endregion
}