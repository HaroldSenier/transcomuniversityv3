﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TranscomUniversityV3.Master" AutoEventWireup="true"
    CodeFile="MyReports.aspx.cs" Inherits="MyReports" %>

<%@ Register TagPrefix="ucad" TagName="ucAdminDashboard" Src="~/UserControl/Pages/AdminReportDashboardUserCtrl.ascx" %>
<%@ Register Src="UserControl/Pages/ReportsGraphCourseRatingUserCtrl.ascx" TagName="ReportGraphCourseRating" TagPrefix="ucgcr" %>
<%@ Register Src="UserControl/Pages/CourseLoginReports.ascx" TagName="ReportCourseLogins" TagPrefix="ucrcl" %>
<%@ Register Src="UserControl/Pages/SurveyReports.ascx" TagName="ReportSurvey" TagPrefix="ucrs" %>
<%@ Register Src="UserControl/Pages/AssessmentReports.ascx" TagName="ReportAssessment" TagPrefix="ucra" %>

<asp:Content ID="Content1" ContentPlaceHolderID="contentPlaceHolderLeftPanel" runat="Server">
    <rad:RadAjaxLoadingPanel ID="localLoadingPanel" runat="server" CssClass="Loading2"
        MinDisplayTime="1000" Transparency="25" />
    <rad:RadAjaxManager ID="ramReports" runat="server">
        <AjaxSettings>
        </AjaxSettings>
    </rad:RadAjaxManager>
    <rad:RadScriptBlock ID="rsbMyReports" runat="server">
        <script type="text/javascript" language="javascript">
            function pageLoad() {
                highlightSelectedSubmenu();
            }

            function highlightSelectedSubmenu() {

                var tab = '<%= Request.QueryString["Tab"] %>';
                var selectedTab = "#tab" + tab;
                $(selectedTab).addClass("active");
                $(selectedTab).parent().parent().find("a.submenu-title").addClass("active");
            }

            function tabClick(el) {

                var caret = $(el).find(".js-collapse-event");

                if ($(caret).hasClass("fa-sort-up")) {
                    $(caret).removeClass("fa-sort-up").addClass("fa-sort-down");
                } else {
                    $(caret).removeClass("fa-sort-down").addClass("fa-sort-up");
                }
            }
              
        </script>
    </rad:RadScriptBlock>
    <div class="sidebar-relative">
        <div class="side-menu-container">
            <div class="overlay">
            </div>
            <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
                <ul class="nav sidebar-nav">
                    <li class="sidebar-brand" style="z-index: 1000; color: #fff;">
                        <a href="#" class="close"><i class="fa fa-times" aria-hidden="true"></i></a> Reports
                    </li>
                    <rad:RadTabStrip ID="RadTabStrip1" runat="server" RenderMode="Lightweight" Skin="Black" AutoPostBack="true" >
                        <Tabs>
                            <rad:RadTab runat="server" Text="Reports Dashboard" Value="1" NavigateUrl="MyReports.aspx?Tab=Dashboard" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                            <rad:RadTab runat="server" Text="Training Reports" Value="2" NavigateUrl="MyReports.aspx?Tab=TrainingReports" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                            <rad:RadTab runat="server" Value="3" Width="285px" PostBack="false"  SelectedCssClass="rtsSelected" CssClass="toggle-submenu">
                            <TabTemplate>
                              <ul class="nav custom-submenu" id="customSubmenu">
		                        <li>
                                
                                    <a class="tab-title" data-toggle="collapse" data-target="#submenu1" aria-expanded="false" onclick="tabClick(this);">Built-in Reports
                                         <i class="fa fa-sort-down pull-right hover-pointer js-collapse-event">
                                        </i>
                                     </a>
			                        <ul class="nav collapse submenu" id="submenu1" role="menu" aria-labelledby="customSubmenu">
				                        <li id="tabCourseLogins"><a href="MyReports.aspx?Tab=CourseLogins">Course Logins</a></li>
				                        <li id="tabSurveys"><a href="MyReports.aspx?Tab=Surveys">Surveys</a></li>
				                        <li id="tabAssessments"><a href="MyReports.aspx?Tab=Assessments">Assessments</a></li>
			                        </ul>
		                        </li>
	                        </ul>
                            </TabTemplate>
                            </rad:RadTab>
                            <rad:RadTab runat="server" Text="Report Builder" Value="4" NavigateUrl="MyReports.aspx?Tab=ReportBuilder" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                            <rad:RadTab runat="server" Text="Graphs" Value="5" PostBack="false" Width="285px" SelectedCssClass="rtsSelected" CssClass="toggle-submenu">
                                 <TabTemplate>
                                    <ul class="nav custom-submenu" id="graphMenu">
		                                <li>
                                
                                            <a class="tab-title" data-toggle="collapse" data-target="#graphSubmenu" aria-expanded="false" onclick="tabClick(this);">Graphs
                                                 <i class="fa fa-sort-down pull-right hover-pointer js-collapse-event">
                                                </i>
                                                </a>
			                                <ul class="nav collapse submenu" id="graphSubmenu" role="menu">
                                                <li id="tabCourseTakeRates"><a href="MyReports.aspx?Tab=CourseTakeRates">Course Take Rates</a></li>
				                                <li id="tabCourseRatings"><a href="MyReports.aspx?Tab=CourseRatings">Course Ratings</a></li>
			                                </ul>
		                                </li>
	                                </ul>
                                </TabTemplate>
                            </rad:RadTab>
                        </Tabs>
                    </rad:RadTabStrip>
                </ul>

            </nav>
            <div class="hamburger-container text-center" style="font-size: 20px; color: #fff;
                margin-top: 10px">
                <a href="#" class="open-nav is-closed animated fadeInLeft"><i class="fa fa-bars"
                    aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolderMain" runat="Server">
    <div class="col-sm-11 col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12 thirty-px-padding">
                        <div class="col-md-12 no-paddings">
                            <ul id="ulBreadcrumb" runat="server" class="breadcrumb">
                                <li><a>
                                    <asp:Label ID="lblTab1" runat="server" Text="Reports" /></a></li>
                                <li runat="server" id="bc2"><a>
                                    <asp:Label ID="lblTab2" runat="server" Text="Reports Dashboards" /></a></li>
                                <li runat="server" id="bc3" visible="false"><a>
                                    <asp:Label ID="lblTab3" runat="server" Text="" /></a></li>
                                <li runat="server" id="bc4" visible="false"><a>
                                    <asp:Label ID="lblTab4" runat="server" Text="" /></a></li>
                                <li runat="server" id="bc5" visible="false">
                                    <asp:Label ID="lblTab5" runat="server" Text="" /></a></li>
                            </ul>
                        </div>
                        <rad:RadMultiPage ID="RadMultiPage1" runat="server" RenderMode="Lightweight" CssClass="outerMultiPage"
                            RenderSelectedPageOnly="true">
                            <rad:RadPageView ID="RadPageView1" runat="server">
                                <ucad:ucAdminDashboard ID="AdminDashboard" runat="server" />
                            </rad:RadPageView>
                            <rad:RadPageView ID="RadPageView2" runat="server">
                                Training Reports
                            </rad:RadPageView>
                            <rad:RadPageView ID="RadPageView3" runat="server">
                               <ucrcl:ReportCourseLogins ID="CourseLogins" runat="server" />
                            </rad:RadPageView>
                            <rad:RadPageView ID="RadPageView4" runat="server">
                                <ucrs:ReportSurvey ID="Survey" runat="server" />
                            </rad:RadPageView>
                            <rad:RadPageView ID="RadPageView5" runat="server">
                                <ucra:ReportAssessment ID="Assessment" runat="server" />
                            </rad:RadPageView>
                            <rad:RadPageView ID="RadPageView6" runat="server">
                                Report Builder
                            </rad:RadPageView>
                            <rad:RadPageView ID="RadPageView7" runat="server">
                                <ucgcr:ReportGraphCourseRating ID="CourseRating" runat="server" Visible="false" />
                            </rad:RadPageView>
                        </rad:RadMultiPage>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
