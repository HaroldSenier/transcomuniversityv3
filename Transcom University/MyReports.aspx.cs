﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class MyReports : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!System.Web.Security.Roles.IsUserInRole(HttpContext.Current.User.Identity.Name.Split('|')[0], "Admin"))
            Response.Redirect("~/Unauthorized.aspx");

        RadTab dashboard = RadTabStrip1.Tabs.FindTabByValue("1");
        RadTab trainingReports = RadTabStrip1.Tabs.FindTabByValue("2");
        RadTab builtinReports = RadTabStrip1.Tabs.FindTabByValue("3");
        RadTab reportBuilder = RadTabStrip1.Tabs.FindTabByValue("4");
        RadTab graph = RadTabStrip1.Tabs.FindTabByValue("5");

        if (Request.QueryString["Tab"] != null && Request.QueryString["Tab"].ToString() == "TrainingReports")
        {
            trainingReports.Selected = true;
            RadMultiPage1.SelectedIndex = 1;
            lblTab2.Text = "Training Reports";
        }
        else if (Request.QueryString["Tab"] != null && Request.QueryString["Tab"].ToString() == "CourseLogins")
        {
            builtinReports.Selected = true;
            RadMultiPage1.SelectedIndex = 2;
            lblTab2.Text = "Built-in Reports";
            bc3.Visible = true;
            lblTab3.Text = "Course Logins";
        }
        else if (Request.QueryString["Tab"] != null && Request.QueryString["Tab"].ToString() == "Surveys")
        {
            builtinReports.Selected = true;
            RadMultiPage1.SelectedIndex = 3;
            lblTab2.Text = "Built-in Reports";
             bc3.Visible = true;
            lblTab3.Text = "Surveys";
        }
        else if (Request.QueryString["Tab"] != null && Request.QueryString["Tab"].ToString() == "Assessments")
        {
            builtinReports.Selected = true;
            RadMultiPage1.SelectedIndex = 4;
            lblTab2.Text = "Built-in Reports";
            bc3.Visible = true;
            lblTab3.Text = "Assessments";
        }
        else if (Request.QueryString["Tab"] != null && Request.QueryString["Tab"].ToString() == "ReportBuilder")
        {
            reportBuilder.Selected = true;
            RadMultiPage1.SelectedIndex = 5;
            lblTab2.Text = "Report Builder";
        }
        else if (Request.QueryString["Tab"] != null && Request.QueryString["Tab"].ToString() == "CourseTakeRates")
        {
            graph.Selected = true;
            RadMultiPage1.SelectedIndex = 6;
            lblTab2.Text = "Graph";
            bc3.Visible = true;
            lblTab3.Text = "Course Take Rates";
        }
        else if (Request.QueryString["Tab"] != null && Request.QueryString["Tab"].ToString() == "CourseRatings")
        {
            graph.Selected = true;
            RadMultiPage1.SelectedIndex = 6;
            lblTab2.Text = "Graph";
            bc3.Visible = true;
            lblTab3.Text = "Course Ratings";
            CourseRating.Visible = true;
        }
        else
        {
            dashboard.Selected = true;
            RadMultiPage1.SelectedIndex = 0;
            lblTab2.Text = "Reports Dashboard";
        }
    }
}