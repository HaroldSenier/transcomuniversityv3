﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PlaylistLoader.aspx.cs" Inherits="PlaylistLoader" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div class="row">
            <div class="col-md-12 bg-color-0D9E9E ten-px-padding">
                <div class="row row-eq-height">
                    <div class="col-md-12 text-center color-white font-bold"><asp:Label ID="PlaylistName" runat="server" /></div>
                </div>
            </div>
        </div>
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <asp:Repeater ID="rptPlaylist" runat="server" OnItemDataBound="rptPlaylist_OnItemDataBound">
                <ItemTemplate>
                    <ol class="carousel-indicators">
                        <asp:Repeater ID="rptIndicators" runat="server">
                            <ItemTemplate>
                                <li data-target="#carousel-example-generic" data-slide-to='<%# Container.ItemIndex%>'
                                    class='<%# Container.ItemIndex == 0 ? "active" : "" %>'></li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ol>
                    <div class="carousel-inner" role="listbox">
                        <asp:Repeater ID="rptSlides" runat="server">
                            <ItemTemplate>
                                <div <%# Container.ItemIndex == 0 ? "class=\"item active\"" : "class=\"item\"" %>>
                                    <img src='Media/Uploads/Sliders/<%#Eval("PlaylistID") %>/<%#Eval("PlaylistImg")%>'
                                        class="img-responsive" alt="">
                                    <div class="carousel-caption">
                                        <p class="link-yellow">
                                            <%#Eval("PlaylistName")%></p>
                                        <p>
                                            <%#Eval("PlaylistDesc") %></p>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="icon-prev" aria-hidden="true"></span><span class="sr-only">Previous</span>
        </a><a class="right carousel-control" href="#carousel-example-generic" role="button"
            data-slide="next"><span class="icon-next" aria-hidden="true"></span><span class="sr-only">
                Next</span> </a>
    </div>
    </form>
</body>
</html>
