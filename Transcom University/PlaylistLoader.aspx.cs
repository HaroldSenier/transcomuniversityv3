﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;


public partial class PlaylistLoader : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataTable dummy = new DataTable();
            dummy.Columns.Add();
            dummy.Rows.Add();
            rptPlaylist.DataSource = dummy;
            rptPlaylist.DataBind();

            //PlaylistName.Text = Request.QueryString["PName"];
            DataSet ds = DataHelper.GetSelectedPlaylist(Convert.ToInt32(Request.QueryString["PlaylistID"]));

            if (ds.Tables[0].Rows.Count > 0)
            {
                // Response.Redirect("~/Unauthorized.aspx");
                PlaylistName.Text = ds.Tables[0].Rows[0]["PlaylistName"].ToString();
            }
            else
            {
                rptPlaylist.Visible = false;
                PlaylistName.Text = "Playlist not found";
            }
        }
    }

    protected void rptPlaylist_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            int PlaylistID = Convert.ToInt32(Request.QueryString["PlaylistID"]);
            Repeater rptIndicators = e.Item.FindControl("rptIndicators") as Repeater;
            Repeater rptSlides = e.Item.FindControl("rptSlides") as Repeater;

            rptIndicators.DataSource = DataHelper.GetSelectedPlaylist(PlaylistID);
            rptIndicators.DataBind();
            rptSlides.DataSource = DataHelper.GetSelectedPlaylist(PlaylistID);
            rptSlides.DataBind();
        }
    }
}