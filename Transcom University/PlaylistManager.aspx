﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TranscomUniversityV3.Master" AutoEventWireup="true"
    CodeFile="PlaylistManager.aspx.cs" Inherits="PlaylistManager" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="UserControl/Statics/AdminSiderbarUserCtrl.ascx" TagName="AdminSiderbarUserCtrl"
    TagPrefix="uc2" %>
<%@ Register Src="UserControl/Pages/PlaylistUserCtrl.ascx" TagName="PlaylistUserCtrl"
    TagPrefix="uc1" %>
<%@ Register Src="UserControl/Pages/PlaylistEditUserCtrl.ascx" TagName="PlaylistEditUserCtrl"
    TagPrefix="uc3" %>
<%@ Register Src="UserControl/Pages/PlaylistEditSlideDetailsUserCtrl.ascx" TagName="PlaylistEditSlideDetailsUserCtrl"
    TagPrefix="uc4" %>
<%@ Register Src="UserControl/Pages/FeaturedCoursesUserCtrl.ascx" TagName="FeaturedCoursesUserCtrl"
    TagPrefix="uc5" %>
<%@ Register Src="UserControl/Pages/NewsUserCtrl.ascx" TagName="NewsUserCtrl" TagPrefix="uc6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="contentPlaceHolderLeftPanel" runat="server">
    <uc2:AdminSiderbarUserCtrl ID="AdminSiderbarUserCtrl1" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolderMain" runat="server">
    <div class="col-md-12">
        <uc3:PlaylistEditUserCtrl ID="PlaylistEditUserCtrl1" runat="server" Visible="false" />
    </div>
    <div class="col-md-12">
        <uc4:PlaylistEditSlideDetailsUserCtrl ID="PlaylistEditSlideDetailsUserCtrl1" runat="server"
            Visible="false" />
    </div>
    <div class="col-md-12">
        <uc6:NewsUserCtrl ID="NewsUserCtrl1" runat="server" />
    </div>
    <div class="col-md-12">
        <uc1:PlaylistUserCtrl ID="PlaylistUserCtrl1" runat="server" Visible="false" />
    </div>
    <div class="col-md-12">
        <uc5:FeaturedCoursesUserCtrl ID="FeaturedCoursesUserCtrl1" runat="server" />
    </div>
</asp:Content>
