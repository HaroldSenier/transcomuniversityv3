﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PlaylistManager : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        PlaylistUserCtrl1.Visible = true;
        PlaylistEditUserCtrl1.Visible = false;
        PlaylistEditSlideDetailsUserCtrl1.Visible = false;
    }
}