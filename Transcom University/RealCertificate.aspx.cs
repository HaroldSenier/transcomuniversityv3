﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Data;



public partial class RealCertificate : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        makePDF();
    }

    void makePDF()
    {
        Response.ContentType = "application/pdf";

        //Response.AddHeader("content-disposition", "attachment;filename=test.pdf");
        Response.AddHeader("content-disposition", "inline;filename=bincha.pdf");

        Response.Cache.SetCacheability(HttpCacheability.NoCache);

        string imageFilePath = Server.MapPath(".") + "/Media/Images/certificate-of-completion-29.jpg";

        iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imageFilePath);

        // Page site and margin left, right, top, bottom is defined
        Document pdfDoc = new Document(null, 20, 0, 0, 0);
        pdfDoc.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());

        //Resize image depend upon your need
        //For give the size to image
        jpg.ScaleToFit(1150, 550);

        //If you want to choose image as background then,

        jpg.Alignment = iTextSharp.text.Image.UNDERLYING;

        //If you want to give absolute/specified fix position to image.
        jpg.SetAbsolutePosition(30, 0);

        PdfWriter pW = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);

        pdfDoc.Open();

        pdfDoc.NewPage();
            
        Font brown = new Font(Font.FontFamily.HELVETICA, 20f, Font.NORMAL, BaseColor.BLACK);
        Font lightblue = new Font(Font.FontFamily.COURIER, 9f, Font.NORMAL, new BaseColor(43, 145, 175));
        Font courier = new Font(Font.FontFamily.COURIER, 45f);
        Font georgia = FontFactory.GetFont("georgia", 30f);
        georgia.Color = BaseColor.BLACK;



        Font georgiaName = new Font(Font.FontFamily.HELVETICA, 30f, Font.NORMAL,BaseColor.BLACK);

        DataSet ds = DataHelper.GetCert(Convert.ToInt32(Utils.Decrypt(Request.QueryString["CertID"])));

        string GoogleID = DataHelper.GetGoogleID();

        DataSet dsCourse = DataHelper.GetCourse(Convert.ToInt32(ds.Tables[0].Rows[0]["CourseID"].ToString()));

        var userInfo = DataHelper.GetUsersInfo();


        string FullName = userInfo[0].FirstName + " " + userInfo[0].LastName;


        Chunk c1 = new Chunk("\n\n\n\n\n\n\n", georgia);
        Chunk c2 = new Chunk("\n\n\n\n\n\n\n"+FullName+"\n\n\n\n", georgiaName);
        Chunk c3 = new Chunk(dsCourse.Tables[0].Rows[0]["Title"].ToString(), georgia);
        Chunk c4 = new Chunk("\n\n " + string.Format("{0:MMM dd, yyyy}", ds.Tables[0].Rows[0]["DateAcquired"]), brown);
        
       // Chunk c4 = new Chunk("\n\n\n\nhas completed\n\n\n\n", brown);

        

       
        Phrase p2 = new Phrase();
        p2.Add(c1);
        p2.Add(c2);
        p2.Add(c3);
        p2.Add(c4);
        Paragraph p = new Paragraph();
        p.Add(p2);
        p.Alignment = Element.ALIGN_CENTER;

        string imageURL = Server.MapPath(".") + "/Media/Images/favicon.png";
        iTextSharp.text.Image jpg2 = iTextSharp.text.Image.GetInstance(imageURL);
        //Resize image depend upon your need
        jpg2.ScaleToFit(50f, 50f);
        //Give space before image
        jpg2.SpacingAfter = -10f;
        //Give some space after the image
        //jpg2.SpacingAfter = 1f;
        jpg2.Alignment = Element.ALIGN_RIGHT;
        jpg2.SetAbsolutePosition(700, 60);

        pdfDoc.Add(p);

        pdfDoc.Add(jpg);
        pdfDoc.Add(jpg2);
            

        //  pdfDoc.Add(rect);

        /*  ColumnText ct = new ColumnText(cb);
        Phrase myText = new Phrase("TEST paragraph\nNewline");
        ct.SetSimpleColumn(myText, 34, 750, 580, 317, 15, Element.ALIGN_LEFT);
        ct.Go();*/

        /* PdfContentByte canvas = pW.DirectContent;
        var rect = new iTextSharp.text.Rectangle(200, 200, 100, 100);
        rect.Border = iTextSharp.text.Rectangle.BOX;
        rect.BorderWidth = 5;
        rect.BorderColor = new BaseColor(0, 0, 0);
        canvas.Rectangle(rect);
        */
         

        pdfDoc.Close();

        Response.Write(pdfDoc);

        Response.End();
    }
}