<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MyTrainingList.ascx.cs"
    Inherits="DNSControls_MyTrainingList" %>
<script type="text/javascript">
    function OpenTraining(strOrgOrAtt) {
        alert('Open Training: ' + strOrgOrAtt);
        // open training content; <strOrgOrAtt> is either of the form "Org:<organizationId>"
        // (for content that has not been launched yet) or "Att:<attemptId>" for content that's
        // previously been launched -- in the former case we need to create an attempt for the
        // content...
        var a;
        if ((a = strOrgOrAtt.match(/^Org:([0-9]+)$/)) != null) {
            // display the dialog to create an attempt on this organization; if the attempt is
            // successfully created, OnAttemptCreated() will be called from the the dialog to
            // update TrainingGrid and display the training
            var args = new Object;
            args.OrganizationId = a[1];
            args.OnAttemptCreated = OnAttemptCreated;
            ShowDialog("CreateAttempt.aspx", args, 450, 250, false);
        }
        else
            if ((a = strOrgOrAtt.match(/^Att:([0-9]+)$/)) != null) {
                // open training in a new window
                OpenFrameset(a[1]);
            }
    }
    function OnAttemptCreated(strOrganizationId, strAttemptId) {
        // called after CreateAttempt.aspx has successfully created an attempt; update the
        // anchor tag to include the attempt number, then open the frameset
        var anchor = document.all["Org_" + strOrganizationId];
        anchor.href = "javascript:OpenTraining('Att:" + strAttemptId + "')";
        anchor.title = "Continue training";
        anchor.parentElement.parentElement.cells[3].innerHTML =
			    "<A href=\"javascript:ShowLog(" + strAttemptId + ")\" title=\"Show Log\">Active</A>";
        OpenFrameset(strAttemptId);
    }

    function OpenFrameset(strAttemptId) {
        // open the frameset for viewing training content; <strAttemptId> is the attempt ID
        window.open("Frameset/Frameset.aspx?View=0&AttemptId=" + strAttemptId, "_blank");
    }

    function ShowLog(strAttemptId) {
        // displays the sequencing log for this attempt
        ShowDialog("SequencingLog.aspx?AttemptId=" + strAttemptId, null, 900, 650, true);
    }

    function ShowDialog(strUrl, args, cx, cy, fScroll) {
        // display a dialog box with URL <strUrl>, arguments <args>, width <cx>, height <cy>,
        // scrollbars if <fScroll>; this can be done using either showModalDialog() or
        // window.open(): the former has better modal behavior; the latter allows selection
        // within the window
        var useShowModalDialog = false;
        var strScroll = fScroll ? "yes" : "no";
        if (useShowModalDialog) {
            showModalDialog(strUrl, args,
				    "dialogWidth: " + cx + "px; dialogHeight: " + cy +
					"px; center: yes; resizable: yes; scroll: " + strScroll + ";");
        }
        else {
            dialogArguments = args; // global variable accessed by dialog
            var x = Math.max(0, (screen.width - cx) / 2);
            var y = Math.max(0, (screen.height - cy) / 2);
            window.open(strUrl, "_blank", "left=" + x + ",top=" + y +
					",width=" + cx + ",height=" + cy +
					",location=no,menubar=no,scrollbars=" + strScroll +
					",status=no,toolbar=no,resizable=yes");
        }
    }

</script>
<h2>
    My Training</h2>
<div class="dashedline">
</div>
<asp:Panel ID="TrainingPanel" runat="server">
    <!-- table of this user's training -->
    <asp:Table ID="TrainingGrid" CssClass="Grid" runat="server">
        <asp:TableHeaderRow CssClass="Header_">
            <asp:TableCell CssClass="Select_" Visible="false"><input id="SelectAll" type="checkbox" title="Select All" onclick="OnSelectAllClicked()" /></asp:TableCell>
            <asp:TableCell CssClass="Name_">Name</asp:TableCell>
            <asp:TableCell CssClass="Uploaded_">Uploaded</asp:TableCell>
            <asp:TableCell CssClass="Status_">Status</asp:TableCell>
            <asp:TableCell CssClass="Score_">Score</asp:TableCell>
        </asp:TableHeaderRow>
    </asp:Table>
</asp:Panel>
<asp:Panel ID="NoTrainingMessage" runat="server">
    <!-- message that's displayed if the TrainingGrid is empty (and is therefore hidden) -->
    <asp:Table ID="NoTrainingMessageTable" CssClass="MessageTable" runat="server">
        <asp:TableRow>
            <asp:TableCell>
                        <p>Click <span style="font-weight: bold">Upload Training Package </span> above to upload a training package.</p>
                        <p>(Training packages can be SCORM 2004, SCORM 1.2, Class Server LRM,<br/> or Class Server IMS+ format.)</p>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Panel>
<div class="dashedline">
</div>
