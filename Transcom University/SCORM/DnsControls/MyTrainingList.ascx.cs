using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.LearningComponents;
using Microsoft.LearningComponents.Storage;
using DotNetSCORM.LearningAPI;
using Schema = DotNetSCORM.LearningAPI.Schema;

public partial class DNSControls_MyTrainingList : DotNetSCORM.LearningAPI.DotNetSCORMControlBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Get a ClientScriptManager reference from the Page class.
        ClientScriptManager cs = Page.ClientScript;
        Type cstype = this.GetType();

        // Load the javaScript support file
        if (!cs.IsClientScriptBlockRegistered("DotNetSCORMjsScript"))
        {
            cs.RegisterClientScriptInclude("DotNetSCORMjsScript", "dnscontrols.js");
        }

        // set <currentUser> to information about the current user, and add the current user's list
        // of training to the TrainingGrid table; for best performance, both these requests are
        // done in a single call to the database
        LearningStoreJob job = LStore.CreateJob();
        RequestCurrentUserInfo(job);
        RequestMyTraining(job, null);
        ReadOnlyCollection<object> results = job.Execute();
        DotNetSCORM.LearningAPI.LStoreUserInfo currentUser = GetCurrentUserInfoResults((DataTable)results[0]);
        GetMyTrainingResultsToHtml((DataTable)results[1], TrainingGrid);

        if (TrainingGrid.Rows.Count == 1)
            TrainingPanel.Attributes.Add("style", "display: none");
        else
            NoTrainingMessage.Attributes.Add("style", "display: none");
    }
}
