<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UploadContent.ascx.cs"
    Inherits="DNSControls_UploadContent" %>
<rad:RadScriptBlock ID="RadScriptBlockUploader" runat="server">
    <script type="text/javascript" language="javascript">
        function deleteCourseUploaderFiles() {
            console.log("deleteCourseUploaderFiles");
            var upload = $find("<%= RadAsyncUpload1.ClientID %>");
            //            if (upload.getUploadedFiles().length == 1) {
            //                upload.deleteFileInputAt(0);

            //            }
            upload.deleteAllFileInputs();

        }
        //function validateUpload(sender, args) {
        //            var upload = $find("<%= RadAsyncUpload1.ClientID %>");
        //            args.IsValid = upload.validateExtensions();
        //            console.log(args.IsValid);
        //upload.getUploadedFiles().length != 0 &&
        //    console.log(args.IsValid);
        //}

        function Uploading(sender, args) {
            debugger;
            //var b = document.getElementById("UploadPackageButton");
            var b = $find("<%= UploadPackageButton.ClientID %>");

            var validated = Page_ClientValidate('uploadNewFile');

            if (validated) {
                b.set_text('Uploading...');
                b.set_enabled(false);
                //$("#<%= UploadPackageButton.ClientID %>").setAttribute('disabled', 'true');

                showPageLoading();
                sender.click();
                if (Telerik.Web.Browser.ff) { //work around a FireFox issue with form submission
                    sender.get_element().click();
                }
            } else {
                b.set_text('Upload');
                b.set_enabled(false);
                //$("#<%= UploadPackageButton.ClientID %>").removeAttribute('disabled');
                //radalert("Please select a file to upload.", 330, 180, "No File Selected", "");
                args.set_cancel(true);
                return;

            }
        }

        function fileUploaded(upload, args) {
            var b = $find("<%= UploadPackageButton.ClientID %>");
            b.set_text('Upload');
            enableUploadButton(true);
            enableCourseUploader(true);
            console.log("File Uploaded: " + args.get_fileName());
            if (ISDROPPED) {
                $(".course-dropzone .droppedfiles-text").html(args.get_fileName() + " was successfully loaded.").removeClass("display-none");
                showPageLoading();
                $find("<%= UploadPackageButton.ClientID %>").click();
            }
            
            //                document.getElementById('<%=UploadPackageButton.ClientID%>').click();

        }

        function fileSelected(upload, args) {
            debugger;
            console.log("File Selected " + args.get_fileName());
            ISFILEVALID = true;
            $telerik.$(".ruInputs li:first", upload.get_element()).addClass('hidden');
            upload.addFileInput();
            if (upload.isExtensionValid(args.get_fileName()) && isRcbSCOTypeNotEmpty()) {
                $telerik.$(".ruFakeInput", upload.get_element()).val(args.get_fileName());
            } 
            



            //            var currentFileName = args.get_fileName();
            //            var fileExt = '.' + currentFileName.split('.').pop().toUpperCase();
            //            if (!$find("<%= RadAsyncUpload1.ClientID %>").validateExtensions()) {
            //                args.set_cancel(true);
            //            }

            //            enableUploadButton(true);
            //            enableCourseUploader(true);

        }

        var ISDROPPED = false;
        function fileDropped(upload, args) {
            //enableCourseUploader(false);
            console.log("dropped something");
            $(".course-dropzone .dropfiles-text").hide();
            if (upload.isExtensionValid(args.get_file().name) && isRcbSCOTypeNotEmpty()) {
                console.log("dropped something valid");
                ISDROPPED = true;
                enableUploadButton(true);
                enableCourseUploader(true);

                //$find("<%= UploadPackageButton.ClientID %>").click();
                //$("#UploadPackageButton").click();


            } else {
                $telerik.$(".ruFakeInput", upload.get_element()).val("");
                enableUploadButton(false);

                $(".course-dropzone .dropfiles-text").show();
                $(".course-dropzone .droppedfiles-text").html("").addClass("display-none");
                console.log("dropped something invalid");
                radalert("Please select the Resource Type from the dropdown.", 330, 180, "Invalid Resource Type", "");
            }

        }

        function validateUploadFile(sender, args) {
            var upload = $find("<%= RadAsyncUpload1.ClientID %>");
            args.IsValid = upload.getUploadedFiles().length != 0;
        }

        function enableUploadButton(isEnable) {
            $find("<%= UploadPackageButton.ClientID %>").set_enabled(isEnable);
        }

        function enableCourseUploader(isEnable) {
            $find("<%= RadAsyncUpload1.ClientID %>").set_enabled(isEnable);
        }

        function ClientFileUploading(sender, args) {

            if (!isRcbSCOTypeNotEmpty()) {
                //radalert("Please select the Resource Type from the dropdown.", 330, 180, "Invalid Resource Type", "");
                args.set_cancel(true);
            }

            if (ISDROPPED)
                $(".course-dropzone .progress-area").removeClass("display-none");
            else
                $(".course-dropzone .progress-area").addClass("display-none");
        }

    </script>
</rad:RadScriptBlock>
<%--<rad:RadWindowManager ID="RadWindowManager1" runat="server" RenderMode="Lightweight" Skin="Bootstrap" VisibleOnPageLoad="false" ></rad:RadWindowManager>--%>
<asp:Panel ID="panelUpload1" CssClass="UploadPanel" runat="server">
    <div id="managecontrol2" style="width: 100%;" align="left">
        <div>
            <rad:RadAsyncUpload ID="RadAsyncUpload1" runat="server" InitialFileInputsCount="1"
                MaxFileInputsCount="1" CssClass="noProgressBar" Localization-Select="Select File"
                DropZones=".course-dropzone" ViewStateMode="Enabled" ControlObjectsVisibility="None"
                AllowedFileExtensions=".zip,.pdf,.xlsx,.txt,.ppt,.docx,.doc,.xls,.mp3,.wmv,.mp4,.avi,.jpeg,.jpg,.png,.gif,.html"
                OnClientFileDropped="fileDropped" OnClientFileUploaded="fileUploaded" OnClientFileSelected="fileSelected"
                OnClientFileUploading="ClientFileUploading" OnClientValidationFailed="validationFailed"
                TemporaryFolder="~/NONSCORM/FileUploads/Temp/" ChunkSize="3145728" />
        </div>
        <div>
            <asp:CustomValidator ID="CustomValidator" runat="server" ClientValidationFunction="validateUploadFile"
                Display="Dynamic" ValidationGroup="uploadNewFile">
            </asp:CustomValidator>
        </div>
        <div class="row no-margin">
            <rad:RadButton ID="UploadPackageButton" runat="server" Text="Upload" CssClass="btn btn-md btn-teal btn-flat pull-right"
                OnClientClicking="Uploading" OnClick="UploadPackageButton_OnClick" RenderMode="Lightweight"
                Skin="Bootstrap" Font-Size="8" ClientIDMode="Static" be>
            </rad:RadButton>
        </div>
        <div class="row no-margin">
            <rad:RadProgressArea Font-Size="X-Small" RenderMode="Lightweight" runat="server"
                ID="RadProgressArea1" ProgressIndicators="TransferSpeed,TotalProgressPercent,TotalProgressBar,TimeEstimated,TimeElapsed,CurrentFileName" />
        </div>
        <%--  <asp:LinkButton runat="server" ID="UploadPackageButton" Text="Upload" CausesValidation="True"
            CssClass="btn btn-md btn-teal btn-flat pull-right" ValidationGroup="upload1" Font-Size="8"
            OnClick="UploadPackageButton_OnClick" OnClientClick="Uploading();" ClientIDMode="Static" ></asp:LinkButton>--%>
    </div>
</asp:Panel>
<rad:RadGrid runat="server" ID="gridPackage" AutoGenerateColumns="False" CssClass="RadGridPackage"
    Skin="Bootstrap" AllowPaging="False" AllowSorting="False" AllowFilteringByColumn="False"
    Visible="false">
    <MasterTableView DataKeyNames="ScoId,ScoPackageId,ScoTypeId,ScoType,ScoTitle,CourseTypeID,curriculumcourseID"
        Name="MasterTable" AutoGenerateColumns="False" GridLines="None" ShowHeader="False"
        TableLayout="Auto">
        <Columns>
            <rad:GridButtonColumn ConfirmDialogType="RadWindow" ConfirmTitle="Delete" ButtonType="ImageButton"
                CommandName="Delete" Text="Delete" UniqueName="DeleteColumn" ConfirmText="Are you sure you want to cancel this SCO?"
                ImageUrl="~/Media/Images/deletesmall.png" ConfirmDialogHeight="180" ConfirmDialogWidth="350">
                <HeaderStyle Width="50px" />
            </rad:GridButtonColumn>
            <rad:GridBoundColumn UniqueName="ScoTypeId" DataField="ScoTypeId" HeaderText="ScoTypeId"
                Display="False" />
            <rad:GridBoundColumn UniqueName="ScoType" DataField="ScoType" HeaderText="ScoType"
                Display="False" />
            <rad:GridBoundColumn UniqueName="ScoTitle" DataField="ScoTitle" HeaderText="ScoTitle" />
        </Columns>
        <GroupByExpressions>
            <rad:GridGroupByExpression>
                <SelectFields>
                    <rad:GridGroupByField FieldAlias="&nbsp;" FieldName="ScoType" HeaderValueSeparator="&nbsp;" />
                </SelectFields>
                <GroupByFields>
                    <rad:GridGroupByField FieldName="ScoTypeId" SortOrder="Ascending" />
                </GroupByFields>
            </rad:GridGroupByExpression>
        </GroupByExpressions>
    </MasterTableView>
    <ClientSettings EnableRowHoverStyle="True">
        <Selecting AllowRowSelect="True" />
    </ClientSettings>
</rad:RadGrid>