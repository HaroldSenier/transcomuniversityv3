using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web.UI;
using System.Web;
using System.Web.UI.WebControls;
using Microsoft.LearningComponents;
using Microsoft.LearningComponents.Storage;
using DotNetSCORM.LearningAPI;
using System.Data.SqlClient;
using Telerik.Web.UI;
using Schema = DotNetSCORM.LearningAPI.Schema;
using System.IO;
using System.Web.Hosting;
using System.Security.Cryptography;
using System.Text;
using TranscomUniversityV3Model;
using System.Linq;
using System.Reflection;
using System.IO.Compression;
using System.IO;
using Ionic.Zip;
using System.Xml;
using System.Net;

public partial class DNSControls_UploadContent : DotNetSCORMControlBase
{
    public Boolean UploadComplete = false;

    private static SqlConnection _oconn;

    private static SqlCommand _ocmd;

    private static void Dbconn(string connStr)
    {
        _oconn = new SqlConnection
        {
            ConnectionString = ConfigurationManager.ConnectionStrings[connStr].ConnectionString
        };
        _oconn.Open();
    }

    public static string CompletePath
    {
        get
        {
            var value = HttpContext.Current.Session["CompletePath"];
            return value.ToString();
        }
        set { HttpContext.Current.Session["CompletePath"] = value; }
    }

    public DataTable DtPackage1
    {
        get
        {
            return ViewState["DtPackage1"] as DataTable;
        }
        set
        {
            ViewState["DtPackage1"] = value;
        }
    }

    public void CreatePackageDataTableColumns()
    {
        DtPackage1 = new DataTable();
        var column = new DataColumn
        {
            DataType = Type.GetType("System.Int32"),
            ColumnName = "ScoId",
            AutoIncrement = true,
            AutoIncrementSeed = 1,
            AutoIncrementStep = 1
        };
        DtPackage1.Columns.Add(column);
        DtPackage1.Columns.Add("ScoPackageId", typeof(int));
        DtPackage1.Columns.Add("ScoTitle", typeof(String));
        DtPackage1.Columns.Add("ScoTypeId", typeof(int));
        DtPackage1.Columns.Add("ScoType", typeof(String));
        DtPackage1.Columns.Add("CourseTypeID", typeof(int));
        DtPackage1.Columns.Add("dripEnable", typeof(int));
        DtPackage1.Columns.Add("curriculumcourseID", typeof(int));
    }

    protected string Decrypt(string cipherText)
    {
        string EncryptionKey = "TRNSCMV32017111";
        cipherText = cipherText.Replace(" ", "+");
        byte[] cipherBytes = Convert.FromBase64String(cipherText);

        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(cipherBytes, 0, cipherBytes.Length);
                    cs.Close();
                }
                cipherText = Encoding.Unicode.GetString(ms.ToArray());
            }
        }
        return cipherText;
    }

    public void GetDBPackages(int resourceTypeID)
    {
        UserControl content = (UserControl)((RadPageView)UploadPackageButton.NamingContainer.NamingContainer.NamingContainer.NamingContainer.FindControl("RadPageView9")).FindControl("Content");

        if (resourceTypeID <= 4)
        {
            RadListView lvCourseDefault_Learner = (RadListView)content.FindControl("lvCourseDefault_Learner");
            lvCourseDefault_Learner.Rebind();
        }
        else if (resourceTypeID == 5)
        {
            RadListView lvCourseDefault_Additional = (RadListView)content.FindControl("lvCourseDefault_Additional");
            lvCourseDefault_Additional.Rebind();
        }
        else
        {
            RadListView lvCourseDefault_Trainer = (RadListView)content.FindControl("lvCourseDefault_Trainer");
            lvCourseDefault_Trainer.Rebind();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            bool isUploadSuccess = false;
            try
            {
                isUploadSuccess = (bool)Session["UploadSuccess"];
            }
            catch (NullReferenceException nfe)
            {
                isUploadSuccess = false;
            }
           
            
            if (isUploadSuccess)
            {
                string uploadedFileName = Session["FileName"].ToString();
                UserControl contentRightPanel = (UserControl)((RadPageView)UploadPackageButton.NamingContainer.NamingContainer.NamingContainer.NamingContainer.FindControl("RadPageView9")).FindControl("contentPanel");
                RadWindowManager RadWindowManager1 = (RadWindowManager)contentRightPanel.FindControl("RadWindowManager1");
                RadWindowManager1.RadAlert(uploadedFileName + " was successfully uploaded.", 330, 180, "Uploaded Successfuly", "");
            }
            Session["UploadSuccess"] = false;
            Session["FileName"] = "";
        }
        gridPackage.Visible = false;
    }

    protected void UploadPackageButton_OnClick(object sender, EventArgs e)
    {
        UserControl contentRightPanel = (UserControl)((RadPageView)UploadPackageButton.NamingContainer.NamingContainer.NamingContainer.NamingContainer.FindControl("RadPageView9")).FindControl("contentPanel");
        RadWindowManager RadWindowManager1 = (RadWindowManager)contentRightPanel.FindControl("RadWindowManager1");
        int sectionID = ((RadComboBox)contentRightPanel.FindControl("rcbCourseSections")).SelectedValue == "" ? 0 : Convert.ToInt32(((RadComboBox)contentRightPanel.FindControl("rcbCourseSections")).SelectedValue);

        RadComboBox rcbSCOType = (RadComboBox)contentRightPanel.FindControl("rcbSCOType");
        RadComboBox rcbSection = (RadComboBox)contentRightPanel.FindControl("rcbCourseSections");

        var currentUserCim = DataHelper.GetCurrentUserCIM();
        var courseId = Convert.ToInt32(Decrypt(Request.QueryString["CourseID"]));

        try
        {
            var filename = RadAsyncUpload1.UploadedFiles[0].FileName;
            UploadedFile uploadedFile = RadAsyncUpload1.UploadedFiles[0];
            if (Page.IsValid)
            {
                CreatePackageDataTableColumns();
                //foreach (GridDataItem row in gridPackage.Items)
                //{
                //    var ScoTypeId = (Int32)row.GetDataKeyValue("ScoTypeId");
                //    var ScoPackageId = (Int32)row.GetDataKeyValue("ScoPackageId");
                //    var ScoType = (string)row.GetDataKeyValue("ScoType");
                //    var ScoTitle = (string)row.GetDataKeyValue("ScoTitle");
                //    var CourseTypeID = (Int32)row.GetDataKeyValue("CourseTypeID");
                //    var curriculumcourseID = (Int32)row.GetDataKeyValue("curriculumcourseID");

                //    DataRow dr = DtPackage1.NewRow();
                //    dr["ScoPackageId"] = ScoPackageId;
                //    dr["ScoTitle"] = ScoTitle;
                //    dr["ScoType"] = ScoType;
                //    dr["ScoTypeId"] = ScoTypeId;
                //    dr["CourseTypeID"] = CourseTypeID;
                //    dr["curriculumcourseID"] = curriculumcourseID;
                //    DtPackage1.Rows.Add(dr);
                //}

                string FolderPath = ConfigurationManager.AppSettings["OtherResourcesDirectoryPath"];

                if (rcbSCOType.SelectedValue == "5" || rcbSCOType.SelectedValue == "8")//additional resources or trainer resources
                {
                    string completePath;
                    string uniqName = null;

                    foreach (UploadedFile file in RadAsyncUpload1.UploadedFiles)
                    {

                        uniqName = DateTime.Now.ToString("yyyyMMddHHmmss") + file.GetName();
                        //uniqName = file.GetName();
                        completePath = Server.MapPath(FolderPath + "\\" + uniqName);

                        file.SaveAs(completePath, true);
                    }

                    DataRow drPackage = DtPackage1.NewRow();
                    drPackage["ScoPackageId"] = 0;
                    drPackage["ScoTitle"] = WebUtility.HtmlEncode(uniqName);
                    drPackage["ScoType"] = rcbSCOType.SelectedItem.Text;
                    drPackage["ScoTypeId"] = rcbSCOType.SelectedValue;
                    drPackage["CourseTypeID"] = 1;
                    drPackage["curriculumcourseID"] = "1";

                    DtPackage1.Rows.Add(drPackage);
                    gridPackage.DataSource = DtPackage1;
                    gridPackage.DataBind();
                }
                else
                {
                    if (DataHelper.GetCourseType(courseId) == 1)//NON SCORM
                    {

                        var userCim = DataHelper.GetCurrentUserCIM();
                        string fileNameNoExt = uploadedFile.GetNameWithoutExtension();
                        string uniqName = DateTime.Now.ToString("yyyyMMddHHmmss") + uploadedFile.GetNameWithoutExtension();
                        bool isZip = uploadedFile.GetExtension() == ".zip";
                        if (isZip)
                        {
                            //if (HttpContext.Current.Request.Url.Host == "localhost")
                            //    outputDirectory = Request.PhysicalApplicationPath + "NONSCORM\\FileUploads\\" + courseId + "\\";
                            //else
                            //    outputDirectory = HttpRuntime.AppDomainAppPath + "NONSCORM\\FileUploads\\" + courseId + "\\";
                           

                            string directoryPath = Server.MapPath(string.Format("~/{0}/", "NONSCORM/FileUploads/" + courseId + "\\" + uniqName + "\\"));
                            string tempPath = Server.MapPath(string.Format("~/{0}/", "NONSCORM/FileUploads/Temp/"));

                            if (!Directory.Exists(directoryPath))
                                Directory.CreateDirectory(directoryPath);

                            if (!Directory.Exists(tempPath))
                                Directory.CreateDirectory(tempPath);

                            uploadedFile.SaveAs(tempPath + filename, true);

                            ZipFile zip = ZipFile.Read(tempPath + filename);
                            Directory.CreateDirectory(directoryPath);
                            zip.ExtractAll(directoryPath, ExtractExistingFileAction.OverwriteSilently);
                        }
                        else 
                        {

                            string completePath;

                            foreach (UploadedFile file in RadAsyncUpload1.UploadedFiles)
                            {
                                string directoryPath = Server.MapPath(string.Format("~/{0}/", "SCORM/OtherResources/" ));
                                

                                if (!Directory.Exists(directoryPath))
                                    Directory.CreateDirectory(directoryPath);

                                uniqName = DateTime.Now.ToString("yyyyMMddHHmmss") + file.GetName();
                                //uniqName = file.GetName();
                                completePath = Server.MapPath(FolderPath + "\\" + uniqName);

                                file.SaveAs(completePath, true);
                            }
                        }

                        DataRow drPackage = DtPackage1.NewRow();
                        drPackage["ScoPackageId"] = 0;
                        drPackage["ScoTitle"] = WebUtility.HtmlEncode(uniqName);
                        drPackage["ScoType"] = isZip == true ? rcbSCOType.Items.FindItemByValue("1").Text : "NonScorm";
                        drPackage["ScoTypeId"] = isZip == true ? 1 : 4;
                        drPackage["CourseTypeID"] = 1;
                        drPackage["curriculumcourseID"] = "1";

                        DtPackage1.Rows.Add(drPackage);
                        gridPackage.DataSource = DtPackage1;
                        gridPackage.DataBind();
                    }
                    else if (DataHelper.GetCourseType(courseId) == 2)//SCORM
                    {
                        DotNetSCORM.LearningAPI.LStoreUserInfo currentUser = GetCurrentUserInfo();
                        PackageItemIdentifier packageId;

                        using (PackageReader packageReader = PackageReader.Create(RadAsyncUpload1.UploadedFiles[0].InputStream))
                        {
                            AddPackageResult result = PStore.AddPackage(packageReader, new PackageEnforcement(false, false, false));
                            packageId = result.PackageId;
                        }

                        LearningStoreJob job = LStore.CreateJob();
                        Dictionary<string, object> properties = new Dictionary<string, object>();
                        properties[Schema.PackageItem.Owner] = currentUser.Id;
                        properties[Schema.PackageItem.FileName] = RadAsyncUpload1.UploadedFiles[0].FileName;
                        properties[Schema.PackageItem.UploadDateTime] = DateTime.Now;

                        job.UpdateItem(packageId, properties);
                        job.Execute();
                        job = LStore.CreateJob();
                        RequestMyTraining(job, packageId);
                        job.Execute<DataTable>();

                        //get package title
                        string PackageTitle;
                        Dbconn("DefaultConnection");
                        _ocmd = new SqlCommand("pr_Scorm_Cor_GetPackageDetails", _oconn)
                        {
                            CommandType = CommandType.StoredProcedure
                        };
                        _ocmd.Parameters.AddWithValue("@ScoPackageId", packageId.ToString().Replace("ItemType:PackageItem, Key:", ""));
                        _ocmd.Parameters.Add("@Title", SqlDbType.VarChar, -1);
                        _ocmd.Parameters["@Title"].Direction = ParameterDirection.Output;
                        _ocmd.ExecuteReader();
                        _oconn.Close();
                        PackageTitle = _ocmd.Parameters["@Title"].Value.ToString();

                        DataRow drPackage = DtPackage1.NewRow();
                        drPackage["ScoPackageId"] = packageId.ToString().Replace("ItemType:PackageItem, Key:", "");
                        drPackage["ScoTitle"] = PackageTitle;
                        drPackage["ScoType"] = rcbSCOType.SelectedItem.Text;
                        drPackage["ScoTypeId"] = rcbSCOType.SelectedValue;
                        drPackage["CourseTypeID"] = 2;
                        drPackage["curriculumcourseID"] = "1";

                        DtPackage1.Rows.Add(drPackage);
                        gridPackage.DataSource = DtPackage1;
                        gridPackage.DataBind();
                        //gridPackage.Visible = true;
                    }


                   
                }

                //SaveSCODetails


                var db = new TranscomUniversityV3ModelContainer();
                var courseTitle = (from c in db.tbl_TranscomUniversity_Cor_Course
                                   where c.CourseID == courseId
                                   select c.Title).FirstOrDefault().ToString();
                var courseDesc = (from c in db.tbl_TranscomUniversity_Cor_Course
                                  where c.CourseID == courseId
                                  select c.Description).FirstOrDefault().ToString();
                int ccId = 0;

                pr_TranscomUniversity_ScoDetailChecker_Result retVal = db.pr_TranscomUniversity_ScoDetailChecker(courseId).SingleOrDefault();

                if (retVal.Msg == "0") //Not Exist
                {
                    Dbconn("DefaultConnection");
                    _ocmd = new SqlCommand("pr_Scorm_Sav_CourseDetails", _oconn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    _ocmd.Parameters.AddWithValue("@CourseID", courseId);
                    _ocmd.Parameters.AddWithValue("@Course", courseTitle != null ? courseTitle : "");
                    _ocmd.Parameters.AddWithValue("@Description", courseDesc != null ? courseDesc : "");
                    _ocmd.Parameters.Add("@curriculumcourseid", SqlDbType.Int);
                    _ocmd.Parameters["@curriculumcourseid"].Direction = ParameterDirection.Output;

                    _ocmd.ExecuteReader();
                    _oconn.Close();
                    ccId = Convert.ToInt32(_ocmd.Parameters["@curriculumcourseid"].Value.ToString());
                }

                int a;
                a = 0;

                //pr_TranscomUniversity_lkp_GetScormPackage_Result rslt = db.pr_TranscomUniversity_lkp_GetScormPackage("", courseId).SingleOrDefault();
                var rslt = (from c in db.tbl_scorm_cor_Course
                            where c.CurriculumID == courseId
                            select c.curriculumcourseID).FirstOrDefault().ToString();

                int? curriculumCourseId = rslt != null ? Int32.Parse(rslt) : ccId;

                if (gridPackage.Items.Count != 0)
                {
                    foreach (GridDataItem row in gridPackage.Items)
                    {
                        a += 1;

                        var scoTypeId = (Int32)row.GetDataKeyValue("ScoTypeId");
                        var scoPackageId = (Int32)row.GetDataKeyValue("ScoPackageId");
                        var courseTypeID = (Int32)row.GetDataKeyValue("CourseTypeID");
                        var scoTitle = (string)row.GetDataKeyValue("ScoTitle");

                        Dbconn("DefaultConnection");
                        _ocmd = new SqlCommand("pr_Scorm_SaveUpdate_ScoDetails", _oconn)
                        {
                            CommandType = CommandType.StoredProcedure
                        };
                        _ocmd.Parameters.AddWithValue("@ScoTypeId", scoTypeId);
                        _ocmd.Parameters.AddWithValue("@ScoPackageId", scoPackageId);
                        _ocmd.Parameters.AddWithValue("@CourseTypeID", courseTypeID);
                        _ocmd.Parameters.AddWithValue("@ScoTitle", scoTitle);
                        _ocmd.Parameters.AddWithValue("@CreatedBy", Convert.ToInt32(currentUserCim));
                        _ocmd.Parameters.AddWithValue("@curriculumcourseid", curriculumCourseId);
                        _ocmd.Parameters.AddWithValue("@SectionID", sectionID);
                        _ocmd.ExecuteReader();
                        _oconn.Close();
                    }
                }

                gridPackage.DataSource = new string[] { };
                gridPackage.DataBind();

                //gridPackage.Visible = false;
                //CreatePackageDataTableColumns();
                GetDBPackages(Convert.ToInt32(rcbSCOType.SelectedValue));
                rcbSCOType.ClearSelection();
                rcbSection.ClearSelection();
                rcbSection.Items.Clear();
                RadAsyncUpload1.UploadedFiles.Clear();
                //RadScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", Utils.callClientScript("clientRefreshResourcesAndSuccessUpload", filename), true);
                //RadScriptManager.RegisterStartupScript(Page, Page.GetType(), "clientRefreshResources", DataHelper.callClientScript("clientRefreshResources"), true);
                Session["UploadSuccess"] = true;
                Session["FileName"] = filename;
                Response.Redirect("CourseBuilder.aspx?CourseID=" + Request.QueryString["CourseID"], true);
                //RadScriptManager.RegisterStartupScript(Page, Page.GetType(), "clearRcbSection", DataHelper.callClientScript("clearRcbSection"), true);
                //RadWindowManager1.RadAlert(filename + " was successfully uploaded.", 330, 180, "Uploaded Successfuly", "");
                //return;
            }
        }
        catch (ArgumentOutOfRangeException aore)
        {
            RadWindowManager1.RadAlert("No Uploaded File Selected or Selected File is not valid!", 330, 180, "Failed to Upload", "");
        }
        catch (Exception ex)
        {
            RadWindowManager1.RadAlert("Failed to upload the file. " + ex.Message, 330, 180, "Failed to Upload", "");
        }
        //finally
        //{
        //    rcbSCOType.ClearSelection();
        //    rcbSection.ClearSelection();
        //    rcbSection.Items.Clear();
        //    RadAsyncUpload1.UploadedFiles.Clear();
        //}
    }

    public static void Decompress(DirectoryInfo directoryPath)
    {
        //foreach (FileInfo file in directoryPath.GetFiles())
        //{
        //    var path = directoryPath.FullName;
        //    string zipPath = path + file.Name;
        //    string extractPath = Regex.Replace(path + file.Name, ".zip", "");

        //    ZipFile.ExtractToDirectory(zipPath, extractPath);
        //}
    }

}
