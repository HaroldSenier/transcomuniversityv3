﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TranscomUniversityV3.Master" AutoEventWireup="true"
    CodeFile="SearchResult.aspx.cs" Inherits="SearchResult" %>

<asp:Content ID="Content1" ContentPlaceHolderID="contentPlaceHolderLeftPanel" runat="Server">
    <rad:RadCodeBlock ID="rcbSearchResult" runat="server">
        <script type="text/javascript">

            //listview variables
            var searchListView;

            var searchGridView;
            var arrsearch;

            var virtualCount;
            var pageSize;
            var pageIndex;
            var qs;
            var words;
            var phrase;
            var lookin;
            var dateupload;
            var department;
            var author;
            var startenddate;
            var duration;
            var rating;
            var isUserLoggedIn;

            function pageLoad() {
                isUserLoggedIn = false;

                isLoggedIn();
                qs = decodeURI('<%= Request.QueryString["q"] %>');
                words = decodeURI('<%= Request.QueryString["words"] %>');
                phrase = decodeURI('<%= Request.QueryString["phrase"] %>');
                lookin = decodeURI('<%= Request.QueryString["lookin"] %>');
                dateupload = new Date(Date.parse(decodeURI('<%= Request.QueryString["dateupload"] %>'))).toLocaleDateString() == "Invalid Date" ? "" : new Date(Date.parse(decodeURI('<%= Request.QueryString["dateupload"] %>'))).toLocaleDateString();
                department = decodeURI('<%= Request.QueryString["department"] %>');
                //department = department == "" ? -1 : department;
                author = decodeURI('<%= Request.QueryString["author"] %>');
                startenddate = new Date(Date.parse(decodeURI('<%= Request.QueryString["startenddate"] %>'))).toLocaleDateString() == "Invalid Date" ? "" : new Date(Date.parse(decodeURI('<%= Request.QueryString["startenddate"] %>'))).toLocaleDateString();
                duration = decodeURI('<%= Request.QueryString["duration"] %>') == "-1" ? "" : decodeURI('<%= Request.QueryString["duration"] %>');
                rating = decodeURI('<%= Request.QueryString["rating"] %>') == "-1" ? "" : decodeURI('<%= Request.QueryString["rating"] %>');


                $("#txtWords").val(words);
                $("#txtPhrase").val(phrase);
                var lookinListbox = $find('<%= Master.FindControl("rlbLookIn").ClientID %>');
                //console.log(lookinListbox);
                for (var i = 0; i < lookin.length; i++) {
                    lookinListbox.getItem(lookin.charAt(i)).set_checked(true);
                    //console.log(lookin.charAt(i) + "char at");
                }

                if (dateupload != "") {
                    var dpDateUpload = $find('<%= Master.FindControl("rdtDateUploaded").ClientID %>');
                    try {
                        var date = new Date(dateupload);
                        dpDateUpload.set_selectedDate(date);
                        dpDateUpload.set_focusedDate(date);
                    } catch (e) {
                        dpDateUpload.clear();
                    }

                }

                if (department != "") {
                    var rcbDepartment = $find('<%= Master.FindControl("rcbDepartment").ClientID %>');
                    try {
                        var items = rcbDepartment.get_items();
                        var comboItem = rcbDepartment.findItemByText(department);
                        var index = items.indexOf(comboItem);
                        items.getItem(index).select();
                        department = rcbDepartment.get_value();
                    } catch (e) {
                        department = "-1";
                        rcbDepartment.clearSelection();
                    }
                }
                if (author != "") {
                    var rcbAuthor = $find('<%= Master.FindControl("rcbAuthor").ClientID %>');
                    try {
                        var items = rcbAuthor.get_items();
                        var comboItem = rcbAuthor.findItemByText(author);
                        var index = items.indexOf(comboItem);
                        items.getItem(index).select();
                    } catch (e) {
                        rcbDepartment.clearSelection();
                    }
                }
                if (startenddate != "") {
                    var dpStartEndDate = $find('<%= Master.FindControl("rdtStarEndDate").ClientID %>');
                    try {
                        var date = new Date(startenddate);
                        dpStartEndDate.set_selectedDate(date);
                        dpStartEndDate.set_focusedDate(date);
                    } catch (e) {
                        dpStartEndDate.clear();
                    }

                }
                if (duration > 0) {
                    var rcbDuration = $find('<%= Master.FindControl("rcbDuration").ClientID %>');
                    try {
                        var items = rcbDuration.get_items();
                        var comboItem = rcbDuration.findItemByText(duration);
                        var index = items.indexOf(comboItem);
                        items.getItem(index).select();
                    } catch (e) {
                        rcbDuration.clearSelection();
                    }

                }
                if (rating > 0) {
                    var rcbRating = $find('<%= Master.FindControl("rcbRating").ClientID %>');
                    try {
                        var items = rcbRating.get_items();
                        var comboItem = rcbRating.findItemByText(rating);
                        var index = items.indexOf(comboItem);
                        items.getItem(index).select();
                    } catch (e) {
                        rcbRating.clearSelection();
                    }
                }

                if (qs != "") {
                    setSearchTerm();
                    initializesearchCourse();
                    $("input[name=q]").val(qs);
                    $(".search-container").removeClass("display-none");
                } else if ((words != "" || phrase != "") && lookin != "") {
                    setSearchTerm();
                    initializesearchCourse();
                    $(".search-container").removeClass("display-none");
                } else {
                    $(".js-mc-loader").hide();
                    radalert("Something went wrong. Please try again.", 330, 180, "Error Message", "");
                    $('<div class="container search-container" style="color:red;"><strong>Search Error.</strong></div>').insertAfter($(".search-container"));
                }

            }

            //--Start SearchResult Binding

            //initialize search Course
            function initializesearchCourse() {
                //                $ = $telerik.$;
                //this will fire the OnCommand event
                searchListView = $find("<%= lvSearchResult.ClientID%>");
                searchListView.rebind();
                pageSize = 12;
                searchListView.set_pageSize(pageSize);
                searchListView.page(0);

                searchGridView = $find("<%= gridSearchResult.ClientID%>");

                $(".mcPager .pagePrev").click(function (e) {

                    searchListView.page(searchListView.get_currentPageIndex() - 1);

                });

                $(".mcPager .pageNext").click(function (e) {

                    searchListView.page(searchListView.get_currentPageIndex() + 1);

                });

                $(".mcPager .pageFirst").click(function (e) {

                    searchListView.page(0);

                });
                $(".mcPager .pageLast").click(function (e) {

                    var lastpage = (Math.ceil(searchListView.get_virtualItemCount() / searchListView.get_pageSize()));
                    //console.log(lastpage);
                    searchListView.page(lastpage - 1);

                });

                $("#searchListView").on("click", ".mc-item", function (e) {

                    searchListView.toggleSelection($(this).index());
                });
            }

            function bindSimpleSearchCourseList() {
                showLoading();
                searchListView = $find("<%= lvSearchResult.ClientID %>");
                var startRowIndex = searchListView.get_currentPageIndex() * searchListView.get_pageSize(),
                maximumRows = searchListView.get_pageSize(),
                sortExpression = searchListView.get_sortExpressions().toLinq();
                var qs = decodeURI('<%= Request.QueryString["q"] %>');
                //make a call to get the data
                PageMethods.GetSimpleSearchCourseData(startRowIndex, maximumRows, sortExpression, qs,
                function (result) {
                    PageMethods.GetSimpleSearchCount(function (count) {
                        searchListView.set_virtualItemCount(count);
                        virtualCount = count;
                        if (count > 0) {
                            $(".mcPager").removeClass("display-none");
                        }

                        pageIndex = searchListView.get_currentPageIndex();
                        generatePageNumbers();
                        setPage();
                    });

                    searchListView.set_dataSource(result);
                    //console.log(result);
                    $(".js-mc-loader").hide();
                    searchListView.dataBind();

                    if (result.length > 0) {

                        $(".switchView").removeClass("display-none");
                        $(".ddlPageSize-container").removeClass("display-none");
                    }


                    arrsearch = result;
                    searchGridView.set_dataSource(result);
                    searchGridView.dataBind();
                    hideLoading();
                },
                function (error) {
                    var code = error._statusCode;
                    if (code == 401)
                        location.reload();
                    else if (code == 500)
                        radalert("Something went wrong. Please try again.", 330, 180, "Error Message", "");
                });
            }

            function bindsearchCourseList() {
                showLoading();
                searchListView = $find("<%= lvSearchResult.ClientID %>");
                var startRowIndex = searchListView.get_currentPageIndex() * searchListView.get_pageSize(),
                maximumRows = searchListView.get_pageSize(),
                sortExpression = searchListView.get_sortExpressions().toLinq();

                //                //console.log("words = " + words);
                //                //console.log("phrase = " + phrase);
                //                //console.log("lookin = " + lookin);
                //                //console.log("dateupload = " + dateupload);
                //                //console.log("department = " + department);
                //                //console.log("author = " + author);
                //                //console.log("startenddate = " + startenddate);
                //                //console.log("duration = " + duration);
                //                //console.log("rating = " + rating);

                //make a call to get the data
                PageMethods.GetSearchCourseData(startRowIndex, maximumRows, sortExpression, words, phrase, lookin, dateupload, department, author, startenddate, duration, rating,
                function (result) {
                    PageMethods.GetSearchCount(function (count) {
                        searchListView.set_virtualItemCount(count);
                        virtualCount = count;
                        if (count > 0) {
                            $(".mcPager").removeClass("display-none");
                        }

                        setPage();
                        generatePageNumbers();

                    });

                    searchListView.set_dataSource(result);
                    //console.log(result);
                    $(".js-mc-loader").hide();
                    searchListView.dataBind();

                    if (result.length > 0)
                        $(".switchView").removeClass("display-none");

                    arrsearch = result;
                    searchGridView.set_dataSource(result);
                    searchGridView.dataBind();

                    hideLoading();
                },
                function (error) {
                    var code = error._statusCode;
                    if (code == 401)
                        location.reload();
                    else if (code == 500)
                        radalert("Something went wrong. Please try again.", 330, 180, "Error Message", "");
                });
            }

            //Client Events searchCourse

            function mcOnListViewDataBinding(sender, args) {
                sender.set_selectedIndexes([]);
            }

            function mcOnListViewCommand(sender, args) {
                args.set_cancel(true);
                var qs = decodeURI('<%= Request.QueryString["q"] %>');
                if (qs != "" || qs != "")
                    bindSimpleSearchCourseList();
                else
                    bindsearchCourseList();

            }

            function btnSwitchView_Click() {
                if ($('#<%= pnlLvSearchResult.ClientID %>').hasClass("display-none")) {

                    $("#<%= pnlLvSearchResult.ClientID %>").removeClass("display-none");
                    $("#<%= pnlGridSearchResult.ClientID %>").addClass("display-none");
                    $("#<%= btnSwitchToGridView.ClientID %>").addClass("display-none");
                    $("#<%= btnSwitchToListView.ClientID %>").removeClass("display-none");
                    //switch to gridview

                    //console.log("to list");
                } else {
                    //switch to list view

                    $("#<%= pnlLvSearchResult.ClientID %>").addClass("display-none");
                    $("#<%= pnlGridSearchResult.ClientID %>").removeClass("display-none");
                    $("#<%= btnSwitchToListView.ClientID %>").addClass("display-none");
                    $("#<%= btnSwitchToGridView.ClientID %>").removeClass("display-none");
                    //console.log("to grid");
                    var stored = sessionStorage.getItem('SearchResult');
                    //console.log("tolist" + stored);

                    searchGridView.set_dataSource(arrsearch);
                    searchGridView.dataBind();
                    //searchGridView.page(0);


                }
                return false;
            }

            function inGridView() {
                if ($('#<%= pnlLvSearchResult.ClientID %>').hasClass("display-none"))
                    return false;
                else
                    return true;
            }

            function sortsearch() {
                searchListView.get_sortExpressions().clear();
                searchListView.get_sortExpressions().add('CourseTitle', "DESC");
                searchListView.rebind();
            }

            function setSearchTerm() {
                qs = decodeURI('<%= Request.QueryString["q"] %>');
                words = decodeURI('<%= Request.QueryString["words"] %>');
                phrase = decodeURI('<%= Request.QueryString["phrase"] %>');
                var st = qs == "" ? (words != "" ? (phrase != "" ? words + " and " + phrase : words) : phrase) : qs;
                //console.log("qs" + qs);
                $("#searchedTerm").html(st);
            }


            function generatePageNumbers() {

                var maxPage = Math.ceil(virtualCount / pageSize);
                //var maxPageToDisplay = maxPage > 5 ? 5 : (virtualCount % pageSize > 1 ? maxPage : maxPage);
                var pageVirtualCount = virtualCount;
                var pages = $(".js-page-numbers ul").html('');
                //end = end > maxPage ? maxPage : end;
                for (var i = 1; i <= maxPage; i++) {
                    var pages = $(".js-page-numbers ul").html();
                    var rp = i - 1;
                    $(".js-page-numbers ul").html(pages + '<li class="page-number"> <a href="javascript:void(0);" onclick="gotoPage(' + rp + ');">' + i + '</a></li>');

                }


            }
            function gotoPage(pageNum) {
                $(".js-page-numbers ul li").removeClass("selected-page");
                searchListView.page(pageNum);
                searchGridView.page(pageNum);
            }

            function setPage() {
                pageIndex = searchListView.get_currentPageIndex();
                var myPage = pageIndex + 1;
                $(".js-page-numbers ul li").removeClass("selected-page");
                $(".js-page-numbers ul li:nth-child(" + myPage + ")").addClass("selected-page");
                var itemCount = pageSize * (pageIndex + 1) <= virtualCount ? (pageSize * (pageIndex + 1)) : ((pageIndex * pageSize) + (virtualCount % pageSize));
                $(".lblShowNumber").html("Showing " + itemCount + " of " + virtualCount + " results");
            }

            var selectCourseID;
            function confirmLaunchCourseSearch(courseId) {
                selectCourseID = courseId;
                debugger;
                var hid = document.getElementById('<%=hfRoleName.ClientID%>').value;
                var errorMessage = document.getElementById('<%= hfAreYouSureYouWantToLaunchThisCourse.ClientID %>').value;
                var errorTitle = document.getElementById('<%= hfConfirmLaunch.ClientID %>').value;
                //alert(hid);
                if (hid == "Learner")
                    confirmLaunchCourse(courseId);
                else {
                    window.location.href = "CourseBuilder.aspx?CourseID=" + selectCourseID
                    setTimeout(function () {
                        $("#mainLoading").show();
                    }, 4000);
                }
            }

            function changePageSize(pageSizer) {
                showLoading();
                start = 1;
                end = 10;
                isViewAll = false;
                var pageSizerID = pageSizer.id;
                pageSize = $("#" + pageSizerID).val();

                if (!$.isNumeric(pageSize)) {
                    pageSize = defaultPageSize;
                }

                rebindPageSize(pageSize)
            }

            function rebindPageSize(ps) {
                searchListView.set_pageSize(ps);
                searchListView.page(0);
                searchListView.rebind();

                searchGridView.set_pageSize(ps);
                searchGridView.page(0);

            }

            function isLoggedIn() {
                PageMethods.isLoggedIn(function (r) {
                    isUserLoggedIn = r;
                },
                function (error) {
                    var code = error._statusCode;
                    if (code == 401)
                        location.reload();
                    else if (code == 500)
                        radalert("Something went wrong. Please try again.", 330, 180, "Error Message", "");
                });
            }

            function showLoading() {
                $("#mc-items").css({ "visibility": "hidden" });
                $('body,html').animate({
                    scrollTop: 0
                }, 300);
            }

            function hideLoading() {
                $("#mc-items").css({ "visibility": "visible" });
            }
        </script>
    </rad:RadCodeBlock>
    <rad:RadWindowManager ID="rwmSearchResult" RenderMode="Lightweight" EnableShadow="true"
        VisibleOnPageLoad="false" Behaviors="Close, Move" DestroyOnClose="true" Modal="true"
        Opacity="99" runat="server" VisibleStatusbar="false" Skin="Bootstrap">
    </rad:RadWindowManager>
    <rad:RadAjaxLoadingPanel ID="fullPageLoading" runat="server" Transparency="25" IsSticky="true"
        CssClass="Loading" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolderMain" runat="Server">
    <asp:Panel ID="searchResultContainer" runat="server" CssClass="search-container display-none"
        Style="width: 100%; min-height: calc(100vh - 200px);">
        <div class="bg-color-teal header">
            <p style="color: yellow;">
                Search Result
            </p>
        </div>
        <div class="search-view">
            You searched for
            <label id="searchedTerm" style="color: blue; font-size: 25px; font-style: italic;">
            </label>
            <div class="row no-margin">
                <div class="switchView display-none pull-left">
                    <asp:LinkButton ID="btnSwitchToListView" runat="server" OnClientClick="btnSwitchView_Click(); return false;"
                        Font-Underline="false" aria-hidden="true" ToolTip="Switch to List View">
                        <i id="switchClassListView" runat="server" class="fa fa-th-list "></i>
                    </asp:LinkButton>
                    <asp:LinkButton ID="btnSwitchToGridView" runat="server" OnClientClick="btnSwitchView_Click(); return false;"
                        Font-Underline="false" aria-hidden="true" CssClass="display-none" ToolTip="Switch to Grid View">
                        <i id="switchClassGridView" runat="server" class="fa fa-th-large "></i>
                    </asp:LinkButton>
                </div>
                <div class="ddlPageSize-container display-none col-md-6 pull-right">
                    <div class="display-inline-flex pull-right">
                        <div class="col-md-2">
                            <p style="line-height: 34px;">
                                Show
                            </p>
                        </div>
                        <div class="col-md-4">
                            <asp:DropDownList ID="ddlpageSize" runat="server" CssClass="form-control rounded-corner"
                                onchange="changePageSize(this); return false;" ClientIDMode="Static">
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-6 no-padding">
                            <p style="line-height: 34px;">
                                <asp:Label runat="server" ID="lblItemsPerPage" Text="<%$ Resources:LocalizedResource, ItemsPerPage %>"></asp:Label>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:Panel ID="pnlLvSearchResult" runat="server" CssClass="search-view">
            <div id="mc-container" class="mc-container">
                <div class="loader js-mc-loader">
                </div>
                <div class="mcPager container search-result display-none">
                    <p class="lblShowNumber" class="pull-left">
                    </p>
                </div>
                <rad:RadListView ID="lvSearchResult" runat="server" AllowPaging="true" AllowMultiFieldSorting="true"
                    PageSize="12">
                    <LayoutTemplate>
                        <div id="searchListView">
                            <div id="mc-items" class="display-flow pull-left width-fill">
                            </div>
                            <div class="mcPager container display-none">
                                <div class="js-page-numbers display-inline-flex takeLaterCourse-page" style="padding-top: 5%;">
                                    <a class="pagePrev black-arrow fa fa-caret-left no-underline-hover" href="javascript:void(0);"
                                        title="Go to previous page"></a>
                                    <ul class="nostyle display-inline-flex black-font">
                                    </ul>
                                    <a class="pageNext black-arrow fa fa-caret-right no-underline-hover" href="javascript:void(0);"
                                        title="Go to next page"></a>
                                </div>
                            </div>
                        </div>
                    </LayoutTemplate>
                    <ClientSettings>
                        <DataBinding ItemPlaceHolderID="mc-items">
                            <ItemTemplate>
                                <div class="col-md-3">
                                <a id="hllvSearchResult" onclick='confirmLaunchCourseSearch("#= EncryptedCourseID #");' style="color:Black;" title=" #= CourseTitle #">
                                <div id="divSearchResult" runat="server" class="mc-item lv rlvI">
                                    <div class="photo-container">
                                        <image src='Media/Uploads/CourseImg/#= CourseID #/#= CourseImage #' onerror="this.src='Media/Uploads/CourseImg/No_image.jpg'" style="height:130px;width:190px;"></image>
                                    </div>
                                        <div class="category font-bold wrap">
                                            #= CourseTitle #
                                        </div>
                                            <div class="category text-trim">
                                            #= CourseType #
                                        </div>
                                        <div class="category">
                                            #= CourseDuration #
                                        </div>
                                        <div class="category text-trim">
                                            #= CourseCategory #
                                        </div>
                                        <div class="category text-trim">
                                            #= CourseSubcategory #
                                        </div>
                                        <div class="clearfix">
                                        </div>
                                </div>
                                </a>
                                </div>
                            </ItemTemplate>
                            <EmptyDataTemplate>
                        <p>No Result Found
                        </p>
                            </EmptyDataTemplate>
                            <DataService EnableCaching="true" />
                        </DataBinding>
                        <ClientEvents OnCommand="mcOnListViewCommand" OnDataBinding="mcOnListViewDataBinding">
                        </ClientEvents>
                    </ClientSettings>
                </rad:RadListView>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlGridSearchResult" runat="server" CssClass="display-none search-view">
            <rad:RadListView ID="gridSearchResult" runat="server" AllowPaging="true" PageSize="8">
                <LayoutTemplate>
                    <div id="searchGridView">
                        <div class="mcPager container search-result display-none">
                            <p class="lblShowNumber" class="pull-left">
                            </p>
                            <%--<div class="js-page-numbers display-inline-flex page-numbers">
                                <a class="pageFirst fa fa-backward white-arrow no-underline-hover" href="javascript:void(0);"
                                    title="Go to first page"></a><a class="pagePrev fa fa-caret-left white-arrow no-underline-hover"
                                        href="javascript:void(0);" title="Go to previous page"></a>
                                <ul class="nostyle display-inline-flex">
                                </ul>
                                <a class="pageNext fa fa-caret-right white-arrow no-underline-hover" href="javascript:void(0);"
                                    title="Go to next page"></a><a class="pageLast fa fa-forward white-arrow no-underline-hover"
                                        href="javascript:void(0);" title="Go to last page"></a>
                            </div>--%>
                        </div>
                        <table class="gridMainTable table table-bordered table-striped course-grid">
                            <thead>
                                <tr class="rlvHeader">
                                    <th class="btn-teal">
                                        Title
                                    </th>
                                    <th class="btn-teal">
                                        Description
                                    </th>
                                    <th class="btn-teal">
                                        Duration
                                    </th>
                                    <th class="btn-teal">
                                        Last Updated
                                    </th>
                                    <th class="btn-teal">
                                        Course Path
                                    </th>
                                </tr>
                            </thead>
                            <tbody id="gridmcitem">
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                        <div class="mcPager container display-none">
                            <div class="js-page-numbers display-inline-flex takeLaterCourse-page" style="padding-top: 5%;">
                                <a class="pagePrev black-arrow fa fa-caret-left no-underline-hover" href="javascript:void(0);"
                                    title="Go to previous page"></a>
                                <ul class="nostyle display-inline-flex black-font">
                                </ul>
                                <a class="pageNext black-arrow fa fa-caret-right no-underline-hover" href="javascript:void(0);"
                                    title="Go to next page"></a>
                            </div>
                        </div>
                    </div>
                </LayoutTemplate>
                <ClientSettings>
                    <DataBinding ItemPlaceHolderID="gridmcitem">
                        <ItemTemplate>
                    <tr class="rlvI">
                        <td>
                            <a id="hlgridSearchResult" onclick='confirmLaunchCourseSearch("#= EncryptedCourseID #");' title=" #= CourseTitle #" class="hover-pointer"> #= CourseTitle #</a>
                        </td>
                        <td>
                            <asp:Label ID="Label2" runat="server"> #= CourseDescription #</asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="Label3" runat="server"> #= CourseDuration #</asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="Label4" runat="server"> #= DateLastModified #</asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="Label5" runat="server"> #= CoursePath #</asp:Label>
                        </td>
                    </tr>
                        </ItemTemplate>
                        <EmptyDataTemplate>
                        <p>No result found.
                        </p>
                        </EmptyDataTemplate>
                        <DataService EnableCaching="true" />
                    </DataBinding>
                </ClientSettings>
            </rad:RadListView>
        </asp:Panel>
    </asp:Panel>
    <asp:HiddenField runat="server" Value="" ID="hfRoleName" />
    <asp:HiddenField runat="server" Value="<%$ Resources:LocalizedResource,AreYouSureYouWantToLaunchThisCourse %>"
        ID="hfAreYouSureYouWantToLaunchThisCourse" />
    <asp:HiddenField runat="server" Value="<%$ Resources:LocalizedResource,ConfirmLaunch %>"
        ID="hfConfirmLaunch" />
</asp:Content>
