﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TranscomUniversityV3Model;
using Telerik.Web.UI;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

public partial class SearchResult : BasePage
{
    public string RoleName
    {
        get
        {
            var value = HttpContext.Current.Session["SessionRoles"];
            return value == null ? "Learner" : (string)value;
        }
        set
        {
            HttpContext.Current.Session["SessionRoles"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //populate ddl page size
        for (int i = 1; i <= 20; i++)
        {
            ListItem ddlItem = new ListItem();
            ddlItem.Text = i.ToString();
            ddlItem.Value = i.ToString();
            ddlpageSize.Items.Add(ddlItem);
        }

        ddlpageSize.SelectedValue = lvSearchResult.PageSize.ToString();
        if (!IsPostBack) 
        {
            hfRoleName.Value = RoleName;
            Session["IsNewCourseView"] = true;
        }
        
    }

   

    private static IEnumerable<CourseDetails> _searchCourse;

    private static string _words;
    private static string _phrase;
    private static string _lookin;
    private static string _dateupload;
    private static int _department;
    private static string _author;
    private static string _startenddate;
    private static string _duration;
    private static string _rating;

    //--Start search Course Page Methods
    [System.Web.Services.WebMethod()]
    public static IEnumerable<CourseDetails> GetSearchCourseData(int startRowIndex, int maximumRows, string sortExpression, string words, string phrase, string lookin, string dateupload, string department, string author, string startenddate, string duration, string rating)
    {
        _words = words;
        _phrase = phrase;
        _lookin = lookin;
        _dateupload = dateupload;
     //   DateTime d = new DateTime();
     //6d = Convert.ToDateTime(dateupload);
        _department = Convert.ToInt32(department);
        _author = author;
        _startenddate = startenddate;
        _duration = duration;
        _rating = rating;

        return RadGrid.GetBindingData(LoadSearchCourse().AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data;
    }

    [System.Web.Services.WebMethod()]
    public static int GetSearchCount()
    {
        return LoadSearchCourse().Count();

    }

    [System.Web.Services.WebMethod()]
    public static IEnumerable<CourseDetails> LoadSearchCourse()
    {
        var db = new TranscomUniversityV3ModelContainer();

      
            _searchCourse = db.pr_TranscomUniversity_ComplexSearch(_words, _phrase, _lookin, _dateupload, _department, _author, _startenddate, _duration, _rating)
                                .AsEnumerable()
                                .Select(c => new CourseDetails
                                {
                                    EncryptedCourseID = Utils.Encrypt(c.CourseID),
                                    CourseID = c.CourseID,
                                    CourseImage = c.CourseImage,
                                    CourseTitle = c.CourseTitle,
                                    CourseType = c.CourseType,
                                    CourseDuration = c.CourseDuration,
                                    CourseCategory = c.CourseCategory,
                                    CourseSubcategory = c.CourseSubcategory,
                                    CourseDescription = c.CourseDescription,
                                    DateLastModified = string.Format("{0: yyyy/MM/dd hh:mm:ss tt}", c.DateLastModified),
                                    CoursePath = c.CoursePath
                                })
                                .ToArray()
                                .OrderBy(c => c.CourseTitle);
            return _searchCourse;     

    }

    //Simple Search
    private static string searchTerm;

    [System.Web.Services.WebMethod()]
    public static IEnumerable<CourseDetails> GetSimpleSearchCourseData(int startRowIndex, int maximumRows, string sortExpression, string term)
    {
        searchTerm = term;
        return RadGrid.GetBindingData(LoadSimpleSearchCourse().AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data;
    }

    [System.Web.Services.WebMethod()]
    public static int GetSimpleSearchCount()
    {
        return LoadSimpleSearchCourse().Count();

    }

    [System.Web.Services.WebMethod()]
    public static IEnumerable<CourseDetails> LoadSimpleSearchCourse()
    {
        TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();

        _searchCourse = db.pr_TranscomUniversity_SearchCourse(searchTerm)
                            .AsEnumerable()
                            .Select(c => new CourseDetails
                            {
                                EncryptedCourseID = Utils.Encrypt(c.CourseID),
                                CourseID = c.CourseID,
                                CourseImage = c.CourseImage,
                                CourseTitle = c.CourseTitle,
                                CourseType = c.CourseType,
                                CourseDuration = c.CourseDuration,
                                CourseCategory = c.CourseCategory,
                                CourseSubcategory = c.CourseSubcategory,
                                CourseDescription = c.CourseDescription,
                                DateLastModified = string.Format("{0: yyyy/MM/dd hh:mm:ss tt}", c.DateLastModified),
                                CoursePath = c.CoursePath
                            })
                            .OrderBy(c => c.CourseTitle)
                            .ToList();
        return _searchCourse;

    }

    private static string getColumnName(int lookin)
    {
        string column = "";

        if (lookin == 0)
            column = "Title";
        else if (lookin == 1)
            column = "Category";
        else if (lookin == 2)
            column = "Subcategory";
        else if (lookin == 3)
            column = "Description";
        else if (lookin == 4)
            column = "Content";

        return column;
    }

    [System.Web.Services.WebMethod()]
    public static bool isLoggedIn()
    {
        return DataHelper.isLoggedIn();
    }

    public class CourseDetails
    {
        public string EncryptedCourseID { get; set; }

        public int CourseID { get; set; }

        public string CourseImage { get; set; }

        public string CourseTitle { get; set; }

        public string CourseType { get; set; }

        public int? CourseDuration { get; set; }

        public string CourseCategory { get; set; }

        public string CourseSubcategory { get; set; }

        public string CourseDescription { get; set; }

        public string DateLastModified { get; set; }

        public string CoursePath { get; set; }
    }
}
