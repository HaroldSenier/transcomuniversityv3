﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TranscomUniversityV3.Master" AutoEventWireup="true" CodeFile="SubCatCourses.aspx.cs" Inherits="SubCatCourses" %>

<asp:Content ID="Content1" ContentPlaceHolderID="contentPlaceHolderLeftPanel" Runat="Server">
    <rad:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Transparency="25" IsSticky="true" CssClass="Loading" />
    <rad:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel2" runat="server" Transparency="25" MinDisplayTime="1000" CssClass="Loading2" />
    <rad:RadAjaxManager ID="RadAjaxManager1" runat="server" DefaultLoadingPanelID="RadAjaxLoadingPanel1">
        <ClientEvents OnRequestStart="RequestStart" />
        <AjaxSettings>
            <rad:AjaxSetting AjaxControlID="RadTabStrip2">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadTabStrip2" UpdatePanelRenderMode="Inline" />
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                    <rad:AjaxUpdatedControl ControlID="ulBreadcrumb" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnSwitchSubCatCoursesListView">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                    <rad:AjaxUpdatedControl ControlID="ulBreadcrumb" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnSwitchSubCatCoursesGridView">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                    <rad:AjaxUpdatedControl ControlID="ulBreadcrumb" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="rdpSubCatCourses">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="rcmSubCatCourses">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <%--<rad:AjaxSetting AjaxControlID="btnAddSubCategory">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlAddSubCategory" LoadingPanelID="RadAjaxLoadingPanel2" />
                    <rad:AjaxUpdatedControl ControlID="lvCourseSubCategory" />
                    <rad:AjaxUpdatedControl ControlID="gvCourseSubCategory" />
                    <rad:AjaxUpdatedControl ControlID="ltSubCatCourses" UpdatePanelRenderMode="Inline" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnSubCatDelete">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="gvDeleteSubCategory">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlDelSubCategory" LoadingPanelID="RadAjaxLoadingPanel2" />
                    <rad:AjaxUpdatedControl ControlID="lvCourseSubCategory" />
                    <rad:AjaxUpdatedControl ControlID="gvCourseSubCategory" />
                    <rad:AjaxUpdatedControl ControlID="ltSubCatCourses" UpdatePanelRenderMode="Inline" />
                </UpdatedControls>
            </rad:AjaxSetting>--%>
        </AjaxSettings>
    </rad:RadAjaxManager>
    <rad:RadScriptBlock ID="RadScriptBlock1" runat="server">
        <script type="text/javascript">
            function RequestStart(sender, args) {
                // the following Javascript code takes care of expanding the RadAjaxLoadingPanel
                // to the full height of the page, if it is more than the browser window viewport

                var loadingPanel = $get("<%= RadAjaxLoadingPanel1.ClientID %>");
                var pageHeight = document.documentElement.scrollHeight;
                var viewportHeight = document.documentElement.clientHeight;

                if (pageHeight > viewportHeight) {
                    loadingPanel.style.height = pageHeight + "px";
                }

                var pageWidth = document.documentElement.scrollWidth;
                var viewportWidth = document.documentElement.clientWidth;

                if (pageWidth > viewportWidth) {
                    loadingPanel.style.width = pageWidth + "px";
                }

                // the following Javascript code takes care of centering the RadAjaxLoadingPanel
                // background image, taking into consideration the scroll offset of the page content

                if ($telerik.isSafari) {
                    var scrollTopOffset = document.body.scrollTop;
                    var scrollLeftOffset = document.body.scrollLeft;
                }
                else {
                    var scrollTopOffset = document.documentElement.scrollTop;
                    var scrollLeftOffset = document.documentElement.scrollLeft;
                }

                var loadingImageWidth = 55;
                var loadingImageHeight = 55;

                loadingPanel.style.backgroundPosition = (parseInt(scrollLeftOffset) + parseInt(viewportWidth / 2) - parseInt(loadingImageWidth / 2)) + "px " + (parseInt(scrollTopOffset) + parseInt(viewportHeight / 2) - parseInt(loadingImageHeight / 2)) + "px";
            }

            function onTabSelecting(sender, args) {

                if (args.get_tab().get_pageViewID()) {
                    args.get_tab().set_postBack(false);
                }
            }
        </script>
        <%--<script type="text/javascript">
            function showMenu(e) {
                var contextMenu = $find("<%= rcmSubCatCourses.ClientID %>");

                if ((!e.relatedTarget) || (!$telerik.isDescendantOrSelf(contextMenu.get_element(), e.relatedTarget))) {
                    contextMenu.show(e);
                }

                $telerik.cancelRawEvent(e);
            }

            function addSubCatCourses() {
                var radwindow = $find('<%=rwAddSubCatCourses.ClientID %>');

                radwindow.show();
            }

            function deleteSubCatCourses() {
                var radwindow = $find('<%=rwDeleteSubCatCourses.ClientID %>');

                radwindow.show();
            }
        </script>--%>
    </rad:RadScriptBlock>
    <div class="sidebar-relative">
        <div class="side-menu-container">
            <div class="overlay">
            </div>
            <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
                <ul class="nav sidebar-nav">
                    <li class="sidebar-brand" style="z-index: 1000; color: #fff;">
                        <a href="#" class="close"><i class="fa fa-times" aria-hidden="true"></i></a> Admin Task Menu
                    </li>
                    <rad:RadTabStrip ID="RadTabStrip1" runat="server" RenderMode="Lightweight" SelectedIndex="0" Skin="Black" AutoPostBack="true" ><%--OnClientTabSelected="onClientTabSelected"--%>
                        <Tabs>
                            <rad:RadTab runat="server" Text="<%$ Resources:LocalizedResource, CourseManagement%>" Value="1" NavigateUrl="Admin.aspx?Tab=CourseMgmt" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                            <rad:RadTab runat="server" Text="<%$ Resources:LocalizedResource, CreateACourse%>" Value="2" NavigateUrl="Admin.aspx?Tab=CreateCourse" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                            <rad:RadTab runat="server" Text="<%$ Resources:LocalizedResource, CourseCategories%>" Value="3" NavigateUrl="Admin.aspx?Tab=CourseCat" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                            <rad:RadTab runat="server" Text="<%$ Resources:LocalizedResource, ClassManagement%>" Value="4" NavigateUrl="Admin.aspx?Tab=ClassMgmt" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                            <rad:RadTab runat="server" Text="<%$ Resources:LocalizedResource, CreateAClass%>" Value="5" NavigateUrl="Admin.aspx?Tab=CreateClass" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                            <rad:RadTab runat="server" Text="<%$ Resources:LocalizedResource, PageEditor%>" Width="285px" SelectedCssClass="rtsSelected">
                                <Tabs>
                                    <rad:RadTab Text="<%$ Resources:LocalizedResource, SigninPageLayout%>" Font-Underline="false" Enabled="false" ForeColor="Yellow"></rad:RadTab>
                                    <rad:RadTab Text="<%$ Resources:LocalizedResource, ImageSlider%>" NavigateUrl="PlaylistManager.aspx"></rad:RadTab>
                                    <rad:RadTab Text="<%$ Resources:LocalizedResource, FeaturedCourses%>" NavigateUrl="PlaylistManager.aspx?tab=featured-courses"></rad:RadTab>
                                    <rad:RadTab Text="<%$ Resources:LocalizedResource, LatestNews%>" NavigateUrl="PlaylistManager.aspx?tab=news"></rad:RadTab>
                                </Tabs>
                                <Tabs>
                                    <rad:RadTab Text="<%$ Resources:LocalizedResource, DisplayedCourses%>" Font-Underline="false" Enabled="false" ForeColor="Yellow"></rad:RadTab>
                                    <rad:RadTab Text="<%$ Resources:LocalizedResource, CourseCatalog%>" NavigateUrl="#"></rad:RadTab>
                                    <rad:RadTab Text="<%$ Resources:LocalizedResource, TrendingCourses%>" NavigateUrl="#"></rad:RadTab>
                                    <rad:RadTab Text="<%$ Resources:LocalizedResource, RecommendedCourses%>" NavigateUrl="#"></rad:RadTab>
                                    <rad:RadTab Text="<%$ Resources:LocalizedResource, MustTakeCourses%>" NavigateUrl="#"></rad:RadTab>
                                    <rad:RadTab Text="<%$ Resources:LocalizedResource, RecentlyAddedCourses%>" NavigateUrl="#"></rad:RadTab>
                                </Tabs>
                            </rad:RadTab>
                            <rad:RadTab runat="server" Text="<%$ Resources:LocalizedResource, UserManagement%>" Value="6" NavigateUrl="Admin.aspx?Tab=UserMgmt" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                            <rad:RadTab runat="server" Text="<%$ Resources:LocalizedResource, Forums%>" Value="7" NavigateUrl="Admin.aspx?Tab=Forums" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                        </Tabs>
                    </rad:RadTabStrip>
                </ul>
            </nav>
            <div class="hamburger-container text-center" style="font-size: 20px; color: #fff;
                margin-top: 10px">
                <a href="#" class="open-nav is-closed animated fadeInLeft"><i class="fa fa-bars"
                    aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolderMain" Runat="Server">
    <rad:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true" RenderMode="Lightweight" Skin="Bootstrap">
<%--        <Windows>
            <rad:RadWindow ID="rwAddSubCategories" runat="server" RenderMode="Lightweight" Modal="true" Skin="Bootstrap" Behaviors="Move, Close"
                CssClass="tc-radwindow-1 height-inherit" Height="250px" Width="450px" VisibleStatusbar="false" Title="Add Subcategory">
                <ContentTemplate>
                    <br />
                    <asp:Panel ID="pnlAddSubCategory" runat="server">
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td>
                                        Subcategory Name : 
                                    </td>
                                    <td>
                                        <rad:RadTextBox ID="txtSubCategoryName" runat="server" RenderMode="Lightweight" EmptyMessage="- Subcategory -"></rad:RadTextBox>
                                        <asp:RequiredFieldValidator ID="rfvSubCategoryName" runat="server" ErrorMessage="*" ControlToValidate="txtSubCategoryName"
                                            SetFocusOnError="True" Display="Dynamic" ValidationGroup="addSubCategory" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Description : 
                                    </td>
                                    <td>
                                        <rad:RadTextBox ID="txtSubCategoryDesc" runat="server" RenderMode="Lightweight" EmptyMessage="- Description -" TextMode="MultiLine">
                                        </rad:RadTextBox>
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot class="footer-bottom">
                                <tr>
                                    <td style="padding-top: 10px;">
                                        <asp:LinkButton ID="btnAddSubCategory" runat="server" OnClick="btnAddSubCategory_Click" ToolTip="Add Subcategory" Text="Add"
                                            CssClass="btn tc-btn-md btn-teal button-flat pull-right" ValidationGroup="addSubCategory">
                                        </asp:LinkButton>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </asp:Panel>
                    <br />
                </ContentTemplate>
            </rad:RadWindow>
        </Windows>
        <Windows>
            <rad:RadWindow ID="rwDeleteSubCategories" runat="server" RenderMode="Lightweight" Modal="true" Skin="Bootstrap" Behaviors="Move, Close"
                CssClass="tc-radwindow-1 height-inherit" Height="450px" Width="850px" VisibleStatusbar="false" Title="Delete Subcategory" >
                <ContentTemplate>
                    <asp:Panel ID="pnlDelSubCategory" runat="server">
                        <rad:RadGrid ID="gvDeleteSubCategory" runat="server" RenderMode="Lightweight" OnNeedDataSource="gvDeleteSubCategory_NeedDataSource"
                            OnItemCommand="gvDeleteSubCategory_ItemCommand" AutoGenerateColumns="false" Height="370px" PageSize="5"  CssClass="GridLess">
                            <PagerStyle Mode="NextPrev" AlwaysVisible="true" />
                            <ClientSettings EnableAlternatingItems="true" AllowColumnsReorder="true" EnableRowHoverStyle="true">
                                <Selecting AllowRowSelect="true"></Selecting>
                                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                <Resizing AllowColumnResize="true" ResizeGridOnColumnResize="true" AllowResizeToFit="true" />
                            </ClientSettings>
                            <ItemStyle Wrap="false"></ItemStyle>
                            <MasterTableView DataKeyNames="SubcategoryID" AllowSorting="true" AllowPaging="true" CommandItemDisplay="None" HeaderStyle-ForeColor="Teal">
                                <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                <Columns>
                                    <rad:GridButtonColumn ConfirmDialogType="RadWindow" ConfirmTitle="Delete Record"
                                        ButtonType="ImageButton" CommandName="Delete" Text="Delete Record" UniqueName="DeleteColumn"
                                        ConfirmText="Are you sure you want to delete this record?" ImageUrl="~/Media/Images/deletesmall.png"
                                        ConfirmDialogHeight="180" ConfirmDialogWidth="330" HeaderStyle-Width="30">
                                    </rad:GridButtonColumn>
                                    <rad:GridBoundColumn UniqueName="Subcategory" HeaderText="SUBCATEGORY" DataField="Subcategory" ReadOnly="true"></rad:GridBoundColumn>
                                    <rad:GridBoundColumn UniqueName="Description" HeaderText="DESCRIPTION" DataField="Description" ReadOnly="true"></rad:GridBoundColumn>
                                    <rad:GridBoundColumn UniqueName="Courses" HeaderText="COURSES" DataField="Courses" ReadOnly="true"></rad:GridBoundColumn>
                                    <rad:GridBoundColumn UniqueName="CreatedBy" HeaderText="UPDATED BY" DataField="CreatedBy" ReadOnly="true"></rad:GridBoundColumn>
                                    <rad:GridBoundColumn UniqueName="DateTimeCreated" HeaderText="DATE UPDATED" DataField="DateTimeCreated" ReadOnly="true"></rad:GridBoundColumn>
                                </Columns>
                            </MasterTableView>
                        </rad:RadGrid>
                    </asp:Panel>
                </ContentTemplate>
            </rad:RadWindow>
        </Windows>--%>
    </rad:RadWindowManager>
    <div class="col-lg-12">
        <div class="row">
            <div class="col-md-12 thirty-px-padding">
                <div class="col-md-12 no-paddings">
                    <ul id="ulBreadcrumb" runat="server" class="breadcrumb">
                        <li><a>
                            <asp:Label ID="lblTab1" runat="server" Text="<%$ Resources:LocalizedResource, Admin%>" /></a></li>
                        <li><a>
                            <asp:Label ID="lblTab2" runat="server" Text="Categories" /></a></li>
                        <li><a>
                            <asp:Label ID="lblTab3" runat="server" Text="<%$ Resources:LocalizedResource, Subcategories%>" /></a></li>
                        <li>
                            <asp:Label ID="lblTab4" runat="server" Text="<%$ Resources:LocalizedResource, Courses%>" /></li>
                    </ul>
                </div>
                <div class="col-md-12  no-paddings">
                    <div class="row">
                        <div style="position: relative; margin: 3%;">
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Media/Images/CourseCat.png" Width="100px" Height="100px" />
                            <div>
                                <asp:Label ID="lblSubCategoryName" runat="server" Text="" ForeColor="Teal" Font-Size="Small" Font-Bold="true"></asp:Label>
                                <br />
                                <asp:Label ID="lblSubCategoryDesc" runat="server" Text="(Description)" Font-Size="X-Small"></asp:Label>
                            </div>
                        </div>
                    </div> 
                    <div class="row">
                        <div class="panelRadStrip" style="margin-top: 20px;">
                            <rad:RadAjaxPanel ID="raPnlSubCatCourses" runat="server">
                                <rad:RadTabStrip ID="RadTabStrip2" runat="server" RenderMode="Lightweight" SelectedIndex="0" MultiPageID="RadMultiPage1"
                                    AutoPostBack="True" EnableEmbeddedSkins="false">
                                    <Tabs>
                                        <rad:RadTab runat="server" Text="<%$ Resources:LocalizedResource, Content%>" Value="1" CssClass="fakeborder" SelectedCssClass="selectedTab" ForeColor="#636363">
                                        </rad:RadTab>
                                        <rad:RadTab runat="server" Text="<%$ Resources:LocalizedResource, History%>" Value="2" CssClass="tab" SelectedCssClass="selectedTab" ForeColor="#636363">
                                        </rad:RadTab>
                                    </Tabs>
                                </rad:RadTabStrip>
                                <rad:RadMultiPage ID="RadMultiPage1" runat="server" RenderMode="Lightweight" SelectedIndex="0" CssClass="outerMultiPage">
                                    <rad:RadPageView ID="RadPageView1" runat="server">
                                        <br />
                                        <div class="row">
                                            <div class="container" style="text-align:right;">
                                                <asp:LinkButton ID="btnSwitchSubCatCoursesListView" runat="server" OnClick="btnSwitchSubCatCoursesListView_Click" Font-Underline="false"
                                                    ForeColor="Black">
                                                    <i id="iSubCatCoursesListView" runat="server" class="fa fa-th-list" style="margin-right:15px" aria-hidden="true" title="List View"></i>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="btnSwitchSubCatCoursesGridView" runat="server" OnClick="btnSwitchSubCatCoursesGridView_Click" Font-Underline="false"
                                                    ForeColor="Black" Visible="false">
                                                    <i id="iSubCatCoursesGridView" runat="server" class="fa fa-th" style="margin-right:15px" aria-hidden="true" title="Grid View"></i>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="btnSubCatCoursesSettings" runat="server" Font-Underline="false" ForeColor="Black"> <%--OnClientClick="showMenu(event);"--%>
                                                    <i id="iSubCatCoursesSettings" runat="server" class="fa fa-ellipsis-v" style="margin-right:15px" aria-hidden="true" title="Settings"></i>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="btnSubCatCoursesDelete" runat="server" Font-Underline="false" ForeColor="Black" OnClick="btnSubCatCoursesDelete_Click">
                                                    <i id="iSubCatCoursesDelete" runat="server" class="fa fa-trash-o" aria-hidden="true" title="Delete"></i>
                                                </asp:LinkButton>
                                            </div>
                                            <div class="container">
                                                <asp:Label runat="server" ID="lblNumberOfCourseSubCat" ForeColor="Teal" Font-Bold="true" Font-Size="Small">
                                                    This subcategory contains
                                                    <asp:Literal ID="ltSubCatCourses" runat="server"></asp:Literal>
                                                    <asp:Label runat="server" ID="label1" Text=" <%$ Resources:LocalizedResource, Courses%>" ></asp:Label>
                                                </asp:Label>
                                            </div>
                                        </div>
                                        <asp:Panel ID="pnlSubCatCoursesListView" runat="server">
                                            <div class="col-md-14" style="margin-left: 1%; margin-right: 1%">
                                                <div class="col-md-12">
                                                    <asp:Panel ID="pnlSubCatCoursesLv" runat="server">
                                                        <div id="divSubCatCoursesLv" class="demo-container col-md-14">
                                                            <rad:RadListView ID="lvSubCatCourses" runat="server" ItemPlaceholderID="ListViewContainer1" AllowPaging="true"
                                                                OnNeedDataSource="lvSubCatCourses_NeedDataSource">
                                                                <ItemTemplate>
                                                                    <asp:Panel ID="pnlSubCatCourses" runat="server" CssClass="container">
                                                                        <div class="row panel-gray panel-course-category">
                                                                            <div class="col-xs-12">
                                                                                <i class="fa fa-folder pull-left"></i>
                                                                                <asp:Label ID="lblTitle" runat="server" CssClass="course-title-bold">
                                                                                    <%#Eval("Title")%>
                                                                                </asp:Label>
                                                                                <asp:HyperLink ID="btnViewDetails" runat="server" ToolTip="View in details in Course Management"
                                                                                    CssClass="action pull-right btn-action" NavigateUrl='<%# string.Format("CourseBuilder.aspx?CourseID={0}", HttpUtility.UrlEncode(Encrypt(Eval("CourseID").ToString()))) %>'>
                                                                                    View in details in Course Management
                                                                                </asp:HyperLink>
                                                                            </div>
                                                                        </div>
                                                                    </asp:Panel>
                                                                </ItemTemplate>
                                                                <LayoutTemplate>
                                                                    <asp:PlaceHolder ID="ListViewContainer1" runat="server" />
                                                                    <rad:RadDataPager ID="rdpSubCatCourses" runat="server" RenderMode="Lightweight" PagedControlID="lvSubCatCourses"
                                                                        PageSize="3" BackColor="White" BorderStyle="None">
                                                                        <Fields>
                                                                            <rad:RadDataPagerButtonField FieldType="Prev" PrevButtonText="Prev"></rad:RadDataPagerButtonField>
                                                                            <rad:RadDataPagerButtonField FieldType="Numeric" PageButtonCount="10" />
                                                                            <rad:RadDataPagerButtonField FieldType="Next" NextButtonText="Next"></rad:RadDataPagerButtonField>
                                                                        </Fields>
                                                                    </rad:RadDataPager>
                                                                </LayoutTemplate>
                                                            </rad:RadListView>
                                                            <br />
                                                        </div>
                                                    </asp:Panel>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlSubCatCoursesGridView" runat="server" Visible="false">
                                            <div class="col-md-14" style="margin-left: 1%; margin-right: 1%">
                                                <div class="col-md-12">
                                                    <asp:Panel ID="pnlSubCatCoursesGv" runat="server">
                                                        <div id="divSubCatCoursesGv" class="demo-container col-md-14">
                                                            <rad:RadGrid ID="gvSubCatCourses" runat="server" RenderMode="Lightweight" OnNeedDataSource="gvSubCatCourses_NeedDataSource"
                                                                AutoGenerateColumns="false" CssClass="GridLess" Height="370px" PageSize="5">
                                                                <PagerStyle Mode="NextPrev" AlwaysVisible="true" />
                                                                <ClientSettings EnableAlternatingItems="true" AllowColumnsReorder="true">
                                                                    <Selecting AllowRowSelect="true"></Selecting>
                                                                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                                    <Resizing AllowColumnResize="true" ResizeGridOnColumnResize="true" AllowResizeToFit="true" />
                                                                </ClientSettings>
                                                                <ItemStyle Wrap="false"></ItemStyle>
                                                                <MasterTableView DataKeyNames="CourseID" AllowSorting="true" AllowPaging="true" CommandItemDisplay="None"
                                                                    HeaderStyle-ForeColor="Teal">
                                                                    <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                                    <Columns>
                                                                        <rad:GridBoundColumn UniqueName="Title" HeaderText="COURSE" DataField="Title" ReadOnly="true"></rad:GridBoundColumn>
                                                                        <rad:GridBoundColumn UniqueName="Description" HeaderText="DESCRIPTION" DataField="Description" ReadOnly="true"></rad:GridBoundColumn>
                                                                        <rad:GridBoundColumn UniqueName="PublishedBy" HeaderText="PUBLISHED BY" DataField="PublishedBy" ReadOnly="true"></rad:GridBoundColumn>
                                                                        <rad:GridTemplateColumn HeaderText="VIEW COURSE">
                                                                            <ItemTemplate>
                                                                                <asp:HyperLink ID="hlGvSubCatCourses" runat="server" ForeColor="Teal" Font-Size="Smaller"
                                                                                   ToolTip="View in details in Course Management" NavigateUrl='<%# string.Format("CourseBuilder.aspx?CourseID={0}", HttpUtility.UrlEncode(Encrypt(Eval("CourseID").ToString()))) %>'>
                                                                                    View in details in Course Management
                                                                                </asp:HyperLink>
                                                                            </ItemTemplate>
                                                                        </rad:GridTemplateColumn>
                                                                    </Columns>
                                                                </MasterTableView>
                                                            </rad:RadGrid>
                                                            <br />
                                                            <br />
                                                            <br />
                                                            <br />
                                                            <br />
                                                            <br />
                                                            <br />
                                                            <br />
                                                            <br />
                                                        </div>
                                                    </asp:Panel>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                        <%--<rad:RadContextMenu ID="rcmSubCatCourses" runat="server" RenderMode="Lightweight" OnItemClick="rcmSubCatCourses_Click"
                                            EnableShadows="true" CssClass="RadContextMenu1" EnableScreenBoundaryDetection="false">
                                            <Items>
                                                <rad:RadMenuItem Text="Course" Value="1" />
                                            </Items>
                                        </rad:RadContextMenu>--%>
                                    </rad:RadPageView>
                                    <rad:RadPageView ID="RadPageView2" runat="server">
                                        <br />
                                        <br />

                                    </rad:RadPageView>
                                </rad:RadMultiPage>
                            </rad:RadAjaxPanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

