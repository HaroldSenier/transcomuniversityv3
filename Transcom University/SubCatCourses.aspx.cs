﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.IO;
using System.Text;
using TranscomUniversityV3Model;
using Telerik.Web.UI;

public partial class SubCatCourses : BasePage
{
    public void GetCatNameDesc()
    {
        var subcategoryId = Convert.ToInt32(Decrypt(Request.QueryString["SubcategoryID"]));

        var db = new TranscomUniversityV3ModelContainer();
        var subcategoryName = (from c in db.tbl_TranscomUniversity_Lkp_Subcategory
                                where c.SubcategoryID == subcategoryId
                                select c.Subcategory).FirstOrDefault().ToString();

        var subcategoryDesc = (from d in db.tbl_TranscomUniversity_Lkp_Subcategory
                                where d.SubcategoryID == subcategoryId
                                select d.Description).FirstOrDefault().ToString();

        lblSubCategoryName.Text = subcategoryName;
        lblSubCategoryDesc.Text = subcategoryDesc != "" ? subcategoryDesc : "(Description)";
    }

    public void GetSubCatCoursesCount()
    {
        var subcategoryId = Convert.ToInt32(Decrypt(Request.QueryString["SubcategoryID"]));
        var db = new TranscomUniversityV3ModelContainer();

        var countSubCatCourses = (from s in db.vw_TranscomUniversity_SubCatCourses
                                      where s.SubcategoryID == subcategoryId
                                      select s).Count().ToString();

        ltSubCatCourses.Text = countSubCatCourses;
    }

    protected string Encrypt(string clearText)
    {
        string EncryptionKey = "TRNSCMV32017111";
        byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);

        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(clearBytes, 0, clearBytes.Length);
                    cs.Close();
                }
                clearText = Convert.ToBase64String(ms.ToArray());
            }
        }
        return clearText;
    }

    protected string Decrypt(string cipherText)
    {
        string EncryptionKey = "TRNSCMV32017111";
        cipherText = cipherText.Replace(" ", "+");
        byte[] cipherBytes = Convert.FromBase64String(cipherText);

        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(cipherBytes, 0, cipherBytes.Length);
                    cs.Close();
                }
                cipherText = Encoding.Unicode.GetString(ms.ToArray());
            }
        }
        return cipherText;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Form.Attributes.Add("enctype", "multipart/form-data");

        if (!Page.IsPostBack)
        {
            if (!System.Web.Security.Roles.IsUserInRole(HttpContext.Current.User.Identity.Name.Split('|')[0], "Admin") && !System.Web.Security.Roles.IsUserInRole(HttpContext.Current.User.Identity.Name.Split('|')[0], "Trainer"))
                Response.Redirect("~/Unauthorized.aspx");
        }
        if (System.Web.Security.Roles.IsUserInRole(HttpContext.Current.User.Identity.Name.Split('|')[0], "Trainer"))
        {
            btnSubCatCoursesSettings.Visible = false;
            btnSubCatCoursesDelete.Visible = false;
        }
        else
        {
            btnSubCatCoursesSettings.Visible = true;
            btnSubCatCoursesDelete.Visible = true;

        }

        GetCatNameDesc();
        GetSubCatCoursesCount();
    }

    protected void btnSwitchSubCatCoursesListView_Click(object sender, EventArgs e)
    {
        btnSwitchSubCatCoursesGridView.Visible = true;
        pnlSubCatCoursesGridView.Visible = true;

        gvSubCatCourses.Rebind();

        btnSwitchSubCatCoursesListView.Visible = false;
        pnlSubCatCoursesListView.Visible = false;
    }

    protected void btnSwitchSubCatCoursesGridView_Click(object sender, EventArgs e)
    {
        btnSwitchSubCatCoursesListView.Visible = true;
        pnlSubCatCoursesListView.Visible = true;

        lvSubCatCourses.Rebind();

        btnSwitchSubCatCoursesGridView.Visible = false;
        pnlSubCatCoursesGridView.Visible = false;
    }

    protected void lvSubCatCourses_NeedDataSource(object source, RadListViewNeedDataSourceEventArgs e)
    {
        if (e.RebindReason == Telerik.Web.UI.RadListViewRebindReason.ExplicitRebind || e.RebindReason == Telerik.Web.UI.RadListViewRebindReason.PostBackEvent)
        {
            var subcategoryId = Convert.ToInt32(Decrypt(Request.QueryString["SubcategoryID"]));
            var courseSubCategory = DataHelper.GetSubCatCourses(subcategoryId);

            if (courseSubCategory != null)
            {
                lvSubCatCourses.DataSource = courseSubCategory;
            }
        }
    }

    protected void gvSubCatCourses_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        var subcategoryId = Convert.ToInt32(Decrypt(Request.QueryString["SubcategoryID"]));
        var courseSubCategory = DataHelper.GetSubCatCourses(subcategoryId);

        if (courseSubCategory != null)
            gvSubCatCourses.DataSource = courseSubCategory;
    }

    protected void btnSubCatCoursesDelete_Click(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", DataHelper.callClientScript("deleteSubCategories"), true);
    }
}