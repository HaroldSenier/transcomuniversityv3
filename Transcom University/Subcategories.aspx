﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TranscomUniversityV3.Master" AutoEventWireup="true"
    CodeFile="Subcategories.aspx.cs" Inherits="Subcategories" %>

<asp:Content ID="Content1" ContentPlaceHolderID="contentPlaceHolderLeftPanel" runat="Server">
    <rad:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Transparency="25"
        IsSticky="true" CssClass="Loading" />
    <rad:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel2" runat="server" Transparency="25"
        MinDisplayTime="1000" CssClass="Loading2" />
    <rad:RadAjaxManager ID="RadAjaxManager1" runat="server" DefaultLoadingPanelID="RadAjaxLoadingPanel1">
        <ClientEvents OnRequestStart="RequestStart" />
        <AjaxSettings>
            <rad:AjaxSetting AjaxControlID="RadTabStrip2">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadTabStrip2" UpdatePanelRenderMode="Inline" />
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                    <rad:AjaxUpdatedControl ControlID="ulBreadcrumb" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnSwitchSubCatListView">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                    <rad:AjaxUpdatedControl ControlID="ulBreadcrumb" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnSwitchSubCatGridView">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                    <rad:AjaxUpdatedControl ControlID="ulBreadcrumb" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="rdpCourseSubCategory">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="rcmCourseSubCategory">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnAddSubCategory">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlAddSubCategory" LoadingPanelID="RadAjaxLoadingPanel2" />
                    <rad:AjaxUpdatedControl ControlID="lvCourseSubCategory" />
                    <rad:AjaxUpdatedControl ControlID="gvCourseSubCategory" />
                    <rad:AjaxUpdatedControl ControlID="gvDeleteSubCategory" />
                    <rad:AjaxUpdatedControl ControlID="ltSubcategories" UpdatePanelRenderMode="Inline" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnSubCatDelete">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
                        <rad:AjaxUpdatedControl ControlID="gvDeleteSubCategory" />
                    
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="gvDeleteSubCategory">
                <UpdatedControls>
                  <rad:AjaxUpdatedControl ControlID="gvDeleteSubCategory"/>
                    <rad:AjaxUpdatedControl ControlID="lvCourseSubCategory" />
                    <rad:AjaxUpdatedControl ControlID="gvCourseSubCategory" />
                    <rad:AjaxUpdatedControl ControlID="ltSubcategories" UpdatePanelRenderMode="Inline" />                  
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="gvCourseSubCategory">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="gvCourseSubCategory" />
                </UpdatedControls>
            </rad:AjaxSetting>
              <rad:AjaxSetting AjaxControlID="lbtnRefreshHistory">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="rgHistory" LoadingPanelID="RadAjaxLoadingPanel2" />
            </UpdatedControls>
        </rad:AjaxSetting>
        </AjaxSettings>
    </rad:RadAjaxManager>
    <rad:RadScriptBlock ID="RadScriptBlock1" runat="server">
        <script type="text/javascript">
            function RequestStart(sender, args) {
                // the following Javascript code takes care of expanding the RadAjaxLoadingPanel
                // to the full height of the page, if it is more than the browser window viewport

                var loadingPanel = $get("<%= RadAjaxLoadingPanel1.ClientID %>");
                var pageHeight = document.documentElement.scrollHeight;
                var viewportHeight = document.documentElement.clientHeight;

                if (pageHeight > viewportHeight) {
                    loadingPanel.style.height = pageHeight + "px";
                }

                var pageWidth = document.documentElement.scrollWidth;
                var viewportWidth = document.documentElement.clientWidth;

                if (pageWidth > viewportWidth) {
                    loadingPanel.style.width = pageWidth + "px";
                }

                // the following Javascript code takes care of centering the RadAjaxLoadingPanel
                // background image, taking into consideration the scroll offset of the page content

                if ($telerik.isSafari) {
                    var scrollTopOffset = document.body.scrollTop;
                    var scrollLeftOffset = document.body.scrollLeft;
                }
                else {
                    var scrollTopOffset = document.documentElement.scrollTop;
                    var scrollLeftOffset = document.documentElement.scrollLeft;
                }

                var loadingImageWidth = 55;
                var loadingImageHeight = 55;

                loadingPanel.style.backgroundPosition = (parseInt(scrollLeftOffset) + parseInt(viewportWidth / 2) - parseInt(loadingImageWidth / 2)) + "px " + (parseInt(scrollTopOffset) + parseInt(viewportHeight / 2) - parseInt(loadingImageHeight / 2)) + "px";
            }

            function onTabSelecting(sender, args) {

                if (args.get_tab().get_pageViewID()) {
                    args.get_tab().set_postBack(false);
                }
            }
        </script>
        <script type="text/javascript">
            function showMenu(e) {
                var contextMenu = $find("<%= rcmCourseSubCategory.ClientID %>");

                if ((!e.relatedTarget) || (!$telerik.isDescendantOrSelf(contextMenu.get_element(), e.relatedTarget))) {
                    contextMenu.show(e);
                }

                $telerik.cancelRawEvent(e);
            }

            function addSubCategories() {
                var radwindow = $find('<%=rwAddSubCategories.ClientID %>');

                radwindow.show();
            }

            function deleteSubCategories() {
                var radwindow = $find('<%=rwDeleteSubCategories.ClientID %>');

                radwindow.show();
            }
          
        </script>
    </rad:RadScriptBlock>
    <asp:Panel runat="server" ID="sidebarAdmin" Visible="false">
        <div class="sidebar-relative">
            <div class="side-menu-container">
                <div class="overlay">
                </div>
                <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
                <ul class="nav sidebar-nav">
                    <li class="sidebar-brand" style="z-index: 1000; color: #fff;">
                        <a href="#" class="close"><i class="fa fa-times" aria-hidden="true"></i></a> Admin Task Menu
                    </li>
                    <rad:RadTabStrip ID="RadTabStrip3" runat="server" RenderMode="Lightweight" SelectedIndex="0" Skin="Black"><%--OnClientTabSelected="onClientTabSelected"--%>
                        <Tabs>
                            <rad:RadTab runat="server" Text="Course Management" Value="1" NavigateUrl="Admin.aspx?Tab=CourseMgmt" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                            <rad:RadTab runat="server" Text="Create a Course" Value="2" NavigateUrl="Admin.aspx?Tab=CreateCourse" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                            <rad:RadTab runat="server" Text="Course Categories" Value="3" NavigateUrl="Admin.aspx?Tab=CourseCat" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                            <rad:RadTab runat="server" Text="Class Management" Value="4" NavigateUrl="Admin.aspx?Tab=ClassMgmt" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                            <rad:RadTab runat="server" Text="Create a Class" Value="5" Width="285px" PostBack="false" CssClass="toggle-submenu" >
                                <TabTemplate>
                                  <ul class="nav custom-submenu panel-title" id="menuCreateClass">
		                            <li>
                                        <a class="tab-title" data-toggle="collapse" data-target="#createClassMenu" aria-expanded="false" >Create a Class
                                         <i class="fa fa-sort-down pull-right hover-pointer js-collapse-event-2" onclick="tabClick(this);">
                                        </i>
                                        </a>
			                            <ul class="nav collapse submenu" id="createClassMenu" role="menu" aria-labelledby="customSubmenu">
				                            <li id="tabBuiltinClass"><a href="Admin.aspx?Tab=BuiltinClass">Built-in Classes</a></li>
				                            <li id="tabClassBuilder"><a href="Admin.aspx?Tab=ClassBuilder">Class Builder</a></li>
			                            </ul>
		                            </li>
	                            </ul>
                                </TabTemplate>
                            </rad:RadTab>
                            <rad:RadTab runat="server" Text="Page Editor" Value="6"  Width="285px" SelectedCssClass="rtsSelected" CssClass="toggle-submenu"  PostBack="false">
                                 <TabTemplate>
                                  <ul class="nav custom-submenu panel-title" id="menuPageEditor">
		                            <li>
                                        <a class="tab-title" >Page Editor
                                         <i class="fa fa-sort-down pull-right hover-pointer js-collapse-event-2" data-toggle="collapse" data-target="#pageEditorMenu" aria-expanded="false" onclick="tabClick(this);"></i>
                                        </a>
			                            <ul class="nav collapse submenu" id="pageEditorMenu" role="menu">
				                            <li id="tabCourseLogins">
                                                <a id="tabHomepageEditor" href="Admin.aspx?Tab=HomepageEditor" class="submenu-title">Homepage Layout
                                                <i class="fa fa-sort-down pull-right hover-pointer js-collapse-event-2"  data-toggle="collapse" data-target="#homepageMenu" aria-expanded="false" onclick="tabClick(this);"></i>
                                                </a>
                                                 
                                                <ul class="nav collapse submenu" id="homepageMenu" role="menu" >
				                                    <li id="tabImageSlider"><a href="Admin.aspx?Tab=ImageSlider">Image Slider</a></li>
                                                    <li id="tabFeaturedCourses"><a href="Admin.aspx?Tab=FeaturedCourses">Featured Courses</a></li>
                                                    <li id="tabLatestNews"><a href="Admin.aspx?Tab=LatestNews">Latest News</a></li>
                                                    <li id="tabTestimonials"><a href="Admin.aspx?Tab=Testimonials">Testimonials</a></li>
			                                    </ul>
                                            </li>
				                             <li id="Li5">
                                                <a class="submenu-title" data-toggle="collapse" data-target="#courseCatalogMenu" aria-expanded="false" onclick="tabClick(this);">Course Catalog
                                                <i class="fa fa-sort-down pull-right hover-pointer js-collapse-event-2"></i>
                                                </a>
                                                <ul class="nav collapse submenu" id="courseCatalogMenu" role="menu" >
				                                    <li id="tabMandatoryCourses"><a href="Admin.aspx?Tab=MandatoryCourses">Mandatory Courses</a></li>
                                                    <li id="tabTrendingCourses"><a href="Admin.aspx?Tab=TrendingCourses">Trending Courses</a></li>
                                                    <li id="tabRecommendedCourses"><a href="Admin.aspx?Tab=RecommendedCourses">Recommended Courses</a></li>
                                                    <li id="tabMustTakeCourses"><a href="Admin.aspx?Tab=MustTakeCourses">Must Take Courses</a></li>
                                                    <li id="tabRecentCourses"><a href="Admin.aspx?Tab=RecentCourses">Recently Added Courses</a></li>
			                                    </ul>
                                            </li>
			                            </ul>
		                            </li>
	                            </ul>
                                </TabTemplate>
                            </rad:RadTab>
                            <rad:RadTab runat="server" Text="User Management" Value="7" NavigateUrl="Admin.aspx?Tab=UserMgmt" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                            <rad:RadTab runat="server" Text="Forums" Value="8" NavigateUrl="Admin.aspx?Tab=AdminForum" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                        </Tabs>
                    </rad:RadTabStrip>
                </ul>
            </nav>
                <div class="hamburger-container text-center" style="font-size: 20px; color: #fff;
                    margin-top: 10px">
                    <a href="#" class="open-nav is-closed animated fadeInLeft"><i class="fa fa-bars"
                        aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="sidebarTrainer" Visible="false">
        <div class="sidebar-relative">
            <div class="side-menu-container">
                <div class="overlay">
                </div>
                <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
                <ul class="nav sidebar-nav">
                    <li class="sidebar-brand" style="z-index: 1000; color: #fff;">
                        <a href="#" class="close"><i class="fa fa-times" aria-hidden="true"></i></a> Trainer Dashboard
                    </li>
                    <rad:RadTabStrip ID="RadTabStrip1" runat="server" RenderMode="Lightweight" SelectedIndex="0" Skin="Black"><%--OnClientTabSelected="onClientTabSelected"--%>
                        <Tabs>
                            <rad:RadTab runat="server" Text="My Class" Value="1"  Width="285px" SelectedCssClass="rtsSelected" CssClass="toggle-submenu"  PostBack="false" Visible="false" >
                                 <TabTemplate>
                                  <ul class="nav custom-submenu panel-title" id="menuPageEditor">
		                            <li>
                                        <a class="tab-title" onclick="tabClick(this);" >My Class
                                         <i class="fa fa-sort-down pull-right hover-pointer js-collapse-event-2" data-toggle="collapse" data-target="#pageEditorMenu" aria-expanded="false"></i>
                                        </a>
			                            <ul class="nav collapse submenu" id="pageEditorMenu" role="menu">
				                            <li id="tabCourseLogins">
                                                <a id="tabHomepageEditor" href="Admin.aspx?Tab=HomepageEditor" class="submenu-title">Class list
                                                </a>
                                            </li>
				                             <li id="Li5">
                                                <a class="submenu-title" data-toggle="collapse" data-target="#courseCatalogMenu" aria-expanded="false" onclick="tabClick(this);">Class Calendar
                                                </a>
                                            </li>
			                            </ul>
		                            </li>
	                            </ul>
                                </TabTemplate>
                            </rad:RadTab>
                            <rad:RadTab runat="server" Text="Class Management" Value="2" NavigateUrl="Trainer.aspx?Tab=ClassMgmt" Width="285px" SelectedCssClass="rtsSelected"  Visible="false" ></rad:RadTab>
                            <rad:RadTab runat="server" Text="Course Management" Value="3" NavigateUrl="Trainer.aspx?Tab=CourseMgmt" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                            
                            <rad:RadTab runat="server" Text="Course Categories" Value="4" NavigateUrl="Trainer.aspx?Tab=CourseCat" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                            <rad:RadTab runat="server" Text="Forums" Value="5" NavigateUrl="Trainer.aspx?Tab=TrainerForum" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                        </Tabs>
                    </rad:RadTabStrip>
                </ul>
            </nav>
                <div class="hamburger-container text-center" style="font-size: 20px; color: #fff;
                    margin-top: 10px">
                    <a href="#" class="open-nav is-closed animated fadeInLeft"><i class="fa fa-bars"
                        aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolderMain" runat="Server">
    <rad:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true" RenderMode="Lightweight"
        Skin="Bootstrap">
        <Windows>
            <rad:RadWindow ID="rwAddSubCategories" runat="server" RenderMode="Lightweight" Modal="true"
                Skin="Bootstrap" Behaviors="Move, Close" CssClass="tc-radwindow-1 height-inherit"
                Height="250px" Width="450px" VisibleStatusbar="false" Title="Add Subcategory">
                <ContentTemplate>
                    <br />
                    <asp:Panel ID="pnlAddSubCategory" runat="server">
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td>
                                        Subcategory Name :
                                    </td>
                                    <td>
                                        <rad:RadTextBox ID="txtSubCategoryName" runat="server" RenderMode="Lightweight" EmptyMessage="- Subcategory -">
                                        </rad:RadTextBox>
                                        <asp:RequiredFieldValidator ID="rfvSubCategoryName" runat="server" ErrorMessage="*"
                                            ControlToValidate="txtSubCategoryName" SetFocusOnError="True" Display="Dynamic"
                                            ValidationGroup="addSubCategory" ForeColor="Red" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Description :
                                    </td>
                                    <td>
                                        <rad:RadTextBox ID="txtSubCategoryDesc" runat="server" RenderMode="Lightweight" EmptyMessage="- Description -"
                                            TextMode="MultiLine">
                                        </rad:RadTextBox>
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot class="footer-bottom">
                                <tr>
                                    <td style="padding-top: 10px;">
                                        <asp:LinkButton ID="btnAddSubCategory" runat="server" OnClick="btnAddSubCategory_Click"
                                            ToolTip="Add Subcategory" Text="Add" CssClass="btn tc-btn-md btn-teal button-flat pull-right"
                                            ValidationGroup="addSubCategory">
                                        </asp:LinkButton>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </asp:Panel>
                    <br />
                </ContentTemplate>
            </rad:RadWindow>
        </Windows>
        <Windows>
            <rad:RadWindow ID="rwDeleteSubCategories" runat="server" RenderMode="Lightweight"
                Modal="true" Skin="Bootstrap" Behaviors="Move, Close" CssClass="tc-radwindow-1 height-inherit"
                Height="450px" Width="850px" VisibleStatusbar="false" Title="Delete Subcategory">
                <ContentTemplate>
                    <%--<asp:Panel ID="pnlDelSubCategory" runat="server">--%>
                        <rad:RadGrid ID="gvDeleteSubCategory" runat="server" RenderMode="Lightweight" OnNeedDataSource="gvDeleteSubCategory_NeedDataSource"
                            OnItemCommand="gvDeleteSubCategory_ItemCommand" AutoGenerateColumns="false" Height="370px"
                            PageSize="5" CssClass="GridLess">
                            <PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" />
                            <ClientSettings EnableAlternatingItems="true" AllowColumnsReorder="true" EnableRowHoverStyle="true">
                                <Selecting AllowRowSelect="true"></Selecting>
                                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                <Resizing AllowColumnResize="true" ResizeGridOnColumnResize="true" AllowResizeToFit="true" />
                            </ClientSettings>
                            <ItemStyle Wrap="false"></ItemStyle>
                            <MasterTableView DataKeyNames="SubcategoryID" AllowSorting="true" AllowPaging="true"
                                CommandItemDisplay="None" HeaderStyle-ForeColor="Teal">
                                <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                <Columns>
                                    <rad:GridButtonColumn ConfirmDialogType="RadWindow" ConfirmTitle="Delete Record"
                                        ButtonType="ImageButton" CommandName="Delete" Text="Delete Record" UniqueName="DeleteColumn"
                                        ConfirmText="Are you sure you want to delete this record?" ImageUrl="~/Media/Images/deletesmall.png"
                                        ConfirmDialogHeight="180" ConfirmDialogWidth="330" HeaderStyle-Width="30">
                                    </rad:GridButtonColumn>                                     
                                    <rad:GridBoundColumn UniqueName="Subcategory" HeaderText="SUBCATEGORY" DataField="Subcategory"
                                        ReadOnly="true">
                                    </rad:GridBoundColumn>
                                    <rad:GridBoundColumn UniqueName="Description" HeaderText="DESCRIPTION" DataField="Description"
                                        ReadOnly="true">
                                    </rad:GridBoundColumn>
                                    <rad:GridBoundColumn UniqueName="Courses" HeaderText="COURSES" DataField="Courses"
                                        ReadOnly="true">
                                    </rad:GridBoundColumn>
                                    <rad:GridBoundColumn UniqueName="CreatedBy" HeaderText="UPDATED BY" DataField="CreatedBy"
                                        ReadOnly="true">
                                    </rad:GridBoundColumn>
                                    <rad:GridBoundColumn UniqueName="DateTimeCreated" HeaderText="DATE UPDATED" DataField="DateTimeCreated"
                                        ReadOnly="true">
                                    </rad:GridBoundColumn>
                                </Columns>
                            </MasterTableView>
                        </rad:RadGrid>
                    <%--</asp:Panel>--%>
                </ContentTemplate>
            </rad:RadWindow>
        </Windows>
    </rad:RadWindowManager>
    <div class="col-lg-12">
        <div class="row">
            <div class="col-md-12 thirty-px-padding">
                <div class="col-md-12 no-paddings">
                    <ul id="ulBreadcrumb" runat="server" class="breadcrumb">
                        <li><a>
                            <asp:Label ID="lblTab1" runat="server" Text="Admin" /></a></li>
                        <li><a>
                            <asp:Label ID="lblTab2" runat="server" Text="Categories" /></a></li>
                        <li>
                            <asp:Label ID="lblTab3" runat="server" Text="Subcategories" /></li>
                    </ul>
                </div>
                <div class="col-md-12  no-paddings">
                    <div class="row">
                        <div style="position: relative; margin: 3%;">
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Media/Images/CourseCat.png" Width="100px"
                                Height="100px" />
                            <div>
                                <asp:Label ID="lblCategoryName" runat="server" Text="" ForeColor="Teal" Font-Size="Small"
                                    Font-Bold="true"></asp:Label>
                                <br />
                                <asp:Label ID="lblCategoryDesc" runat="server" Text="(Description)" Font-Size="X-Small"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="panelRadStrip" style="margin-top: 20px;">
                            <rad:RadAjaxPanel ID="raPnlSubcategories" runat="server">
                                <rad:RadTabStrip ID="RadTabStrip2" runat="server" RenderMode="Lightweight" SelectedIndex="0"
                                    MultiPageID="RadMultiPage1" AutoPostBack="True" EnableEmbeddedSkins="false">
                                    <Tabs>
                                        <rad:RadTab runat="server" Text="CONTENT" Value="1" CssClass="fakeborder" SelectedCssClass="selectedTab"
                                            ForeColor="#636363">
                                        </rad:RadTab>
                                        <rad:RadTab runat="server" Text="HISTORY" Value="2" CssClass="tab" SelectedCssClass="selectedTab"
                                            ForeColor="#636363">
                                        </rad:RadTab>
                                    </Tabs>
                                </rad:RadTabStrip>
                                <rad:RadMultiPage ID="RadMultiPage1" runat="server" RenderMode="Lightweight" SelectedIndex="0"
                                    CssClass="outerMultiPage">
                                    <rad:RadPageView ID="RadPageView1" runat="server">
                                        <br />
                                        <div class="row">
                                            <div class="container" style="margin-left: 90%">
                                                <asp:LinkButton ID="btnSwitchSubCatListView" runat="server" OnClick="btnSwitchSubCatListView_Click"
                                                    Font-Underline="false" ForeColor="Black">
                                                    <i id="iSubCatListView" runat="server" class="fa fa-th-list" style="margin-right: 15px"
                                                        aria-hidden="true" title="List View"></i>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="btnSwitchSubCatGridView" runat="server" OnClick="btnSwitchSubCatGridView_Click"
                                                    Font-Underline="false" ForeColor="Black" Visible="false">
                                                    <i id="iSubCatGridView" runat="server" class="fa fa-th" style="margin-right: 15px"
                                                        aria-hidden="true" title="Grid View"></i>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="btnSubCatSettings" runat="server" Font-Underline="false" ForeColor="Black"
                                                    OnClientClick="showMenu(event);">
                                                    <i id="iSubCatSettings" runat="server" class="fa fa-ellipsis-v" style="margin-right: 15px"
                                                        aria-hidden="true" title="Settings"></i>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="btnSubCatDelete" runat="server" Font-Underline="false" ForeColor="Black"
                                                    OnClick="btnSubCatDelete_Click">
                                                    <i id="iSubCatDelete" runat="server" class="fa fa-trash-o" aria-hidden="true" title="Delete">
                                                    </i>
                                                </asp:LinkButton>
                                            </div>
                                            <div class="container">
                                                <asp:Label runat="server" ID="lblNumberOfCourseSubCat" ForeColor="Teal" Font-Bold="true"
                                                    Font-Size="Small">This category contains
                                                    <asp:Literal ID="ltSubcategories" runat="server"></asp:Literal>
                                                    Subcategories </asp:Label>
                                            </div>
                                        </div>
                                        <asp:Panel ID="pnlSubCatListView" runat="server">
                                            <div class="col-md-14" style="margin-left: 1%; margin-right: 1%">
                                                <div class="col-md-12">
                                                    <asp:Panel ID="pnlSubCatLv" runat="server">
                                                        <div id="divSubCatLv" class="demo-container col-md-14">
                                                            <rad:RadListView ID="lvCourseSubCategory" runat="server" ItemPlaceholderID="ListViewContainer1"
                                                                AllowPaging="true" OnNeedDataSource="lvCourseSubCategory_NeedDataSource">
                                                                <ItemTemplate>
                                                                    <asp:Panel ID="pnlSubcategory" runat="server" CssClass="container">
                                                                        <div class="row panel-gray panel-course-category">
                                                                            <div class="col-xs-12">
                                                                                <i class="fa fa-folder pull-left"></i>
                                                                                <asp:HyperLink ID="HyperLink1" runat="server" ToolTip='<%#Eval("Subcategory")%>'
                                                                                    CssClass="title" NavigateUrl='<%# string.Format("SubCatCourses.aspx?SubcategoryID={0}", HttpUtility.UrlEncode(Encrypt(Eval("SubcategoryID").ToString()))) %>'>
                                                                                    <%#Eval("Subcategory")%>
                                                                                </asp:HyperLink>
                                                                            </div>
                                                                        </div>
                                                                    </asp:Panel>
                                                                </ItemTemplate>
                                                                <LayoutTemplate>
                                                                    <asp:PlaceHolder ID="ListViewContainer1" runat="server" />
                                                                    <rad:RadDataPager ID="rdpCourseSubCategory" runat="server" RenderMode="Lightweight"
                                                                        PagedControlID="lvCourseSubCategory" PageSize="3" BackColor="White" BorderStyle="None">
                                                                        <Fields>
                                                                            <rad:RadDataPagerButtonField FieldType="Prev" PrevButtonText="Prev"></rad:RadDataPagerButtonField>
                                                                            <rad:RadDataPagerButtonField FieldType="Numeric" PageButtonCount="10" />
                                                                            <rad:RadDataPagerButtonField FieldType="Next" NextButtonText="Next"></rad:RadDataPagerButtonField>
                                                                            <rad:RadDataPagerPageSizeField PageSizeText="Page Size: " />
                                                                        </Fields>
                                                                    </rad:RadDataPager>
                                                                </LayoutTemplate>
                                                            </rad:RadListView>
                                                            <br />
                                                        </div>
                                                    </asp:Panel>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlSubCatGridView" runat="server" Visible="false">
                                            <div class="col-md-14" style="margin-left: 1%; margin-right: 1%">
                                                <div class="col-md-12">
                                                    <asp:Panel ID="pnlSubCatGv" runat="server">
                                                        <div id="divSubCatGv" class="demo-container col-md-14">
                                                            <rad:RadGrid ID="gvCourseSubCategory" runat="server" RenderMode="Lightweight" OnNeedDataSource="gvCourseSubCategory_NeedDataSource"
                                                                AutoGenerateColumns="false" CssClass="GridLess" Height="370px" PageSize="5">
                                                                <PagerStyle Mode="NextPrev" AlwaysVisible="true" />
                                                                <ClientSettings EnableAlternatingItems="true" AllowColumnsReorder="true">
                                                                    <Selecting AllowRowSelect="true"></Selecting>
                                                                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                                    <Resizing AllowColumnResize="true" ResizeGridOnColumnResize="true" AllowResizeToFit="true" />
                                                                </ClientSettings>
                                                                <ItemStyle Wrap="false"></ItemStyle>
                                                                <MasterTableView DataKeyNames="SubcategoryID" AllowSorting="true" AllowPaging="true"
                                                                    CommandItemDisplay="None" HeaderStyle-ForeColor="Teal">
                                                                    <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                                    <Columns>
                                                                        <rad:GridTemplateColumn SortExpression="Subcategory" UniqueName="Subcategory" HeaderText="SUBCATEGORY"
                                                                            DataField="Subcategory">
                                                                            <ItemTemplate>
                                                                                <asp:HyperLink ID="hlGvCourseSubCategory" runat="server" NavigateUrl='<%# string.Format("SubCatCourses.aspx?SubcategoryID={0}", HttpUtility.UrlEncode(Encrypt(Eval("SubcategoryID").ToString()))) %>'>
                                                                                    <%#Eval("Subcategory")%>
                                                                                </asp:HyperLink>
                                                                            </ItemTemplate>
                                                                        </rad:GridTemplateColumn>
                                                                        <rad:GridBoundColumn UniqueName="Description" HeaderText="DESCRIPTION" DataField="Description"
                                                                            ReadOnly="true">
                                                                        </rad:GridBoundColumn>
                                                                        <rad:GridBoundColumn UniqueName="Courses" HeaderText="COURSES" DataField="Courses"
                                                                            ReadOnly="true">
                                                                        </rad:GridBoundColumn>
                                                                        <rad:GridBoundColumn UniqueName="CreatedBy" HeaderText="UPDATED BY" DataField="CreatedBy"
                                                                            ReadOnly="true">
                                                                        </rad:GridBoundColumn>
                                                                        <rad:GridBoundColumn UniqueName="DateTimeCreated" HeaderText="DATE UPDATED" DataField="DateTimeCreated"
                                                                            ReadOnly="true">
                                                                        </rad:GridBoundColumn>
                                                                    </Columns>
                                                                </MasterTableView>
                                                            </rad:RadGrid>
                                                            <br />
                                                            <br />
                                                            <br />
                                                            <br />
                                                            <br />
                                                            <br />
                                                            <br />
                                                            <br />
                                                            <br />
                                                        </div>
                                                    </asp:Panel>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                        <rad:RadContextMenu ID="rcmCourseSubCategory" runat="server" RenderMode="Lightweight"
                                            OnItemClick="rcmCourseSubCategory_Click" EnableShadows="true" CssClass="RadContextMenu1"
                                            EnableScreenBoundaryDetection="false">
                                            <Items>
                                                <rad:RadMenuItem Text="Add Subcategory" Value="1" />
                                            </Items>
                                        </rad:RadContextMenu>
                                    </rad:RadPageView>
                                    <rad:RadPageView ID="RadPageView2" runat="server">
                                        <div class="container">
                                            <br />
                                            <br />
                                            <asp:Label runat="server" ID="lblNumberOfLogs" ForeColor="Teal" Font-Bold="true"
                                                Font-Size="Medium">Showing
                                                <asp:Literal ID="ltLogCount" runat="server"></asp:Literal>
                                                activities</asp:Label>
                                            <asp:LinkButton ID="lbtnRefreshHistory" runat="server" OnClick="lbtnRefreshHistory_Click"
                                                CssClass="pull-right"><i class="fa fa-refresh" aria-hidden="true"></i></asp:LinkButton>
                                            <rad:RadGrid RenderMode="Lightweight" ID="rgHistory" runat="server" CssClass="GridLess"
                                                Font-Size="12px" OnNeedDataSource="rgHistory_NeedDataSource" PageSize="20">
                                                <PagerStyle Mode="NextPrev" AlwaysVisible="true" />
                                                <ItemStyle Wrap="true" />
                                                <ClientSettings EnableAlternatingItems="true">
                                                    <Selecting AllowRowSelect="true"></Selecting>
                                                    <Scrolling UseStaticHeaders="true" />
                                                    <Resizing AllowResizeToFit="true" ResizeGridOnColumnResize="false" />
                                                </ClientSettings>
                                                <ItemStyle Wrap="true"></ItemStyle>
                                                <MasterTableView AutoGenerateColumns="false" HeaderStyle-ForeColor="Teal" AllowPaging="true"
                                                    TableLayout="Auto">
                                                    <Columns>
                                                        <rad:GridTemplateColumn HeaderText="#">
                                                            <ItemTemplate>
                                                                <%#Container.ItemIndex+1 %></ItemTemplate>
                                                        </rad:GridTemplateColumn>
                                                        <rad:GridBoundColumn DataField="LogDate" HeaderText="DATE" UniqueName="LogDate">
                                                        </rad:GridBoundColumn>
                                                        <rad:GridBoundColumn DataField="UserID" HeaderText="CIM" UniqueName="UserID">
                                                        </rad:GridBoundColumn>
                                                        <rad:GridBoundColumn DataField="IpAddress" HeaderText="IP ADDRESS" UniqueName="Ip">
                                                        </rad:GridBoundColumn>
                                                        <rad:GridBoundColumn DataField="Action" HeaderText="ACTION" UniqueName="Action">
                                                        </rad:GridBoundColumn>
                                                        <rad:GridBoundColumn DataField="Duration" HeaderText="DURATION" UniqueName="Duration">
                                                        </rad:GridBoundColumn>
                                                    </Columns>
                                                </MasterTableView>
                                            </rad:RadGrid>
                                        </div>
                                        <br />
                                        <br />
                                    </rad:RadPageView>
                                </rad:RadMultiPage>
                            </rad:RadAjaxPanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
