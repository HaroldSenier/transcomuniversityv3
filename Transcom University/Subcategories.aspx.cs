﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.IO;
using System.Text;
using TranscomUniversityV3Model;
using Telerik.Web.UI;

public partial class Subcategories : BasePage
{
    public void GetCatNameDesc()
    {
        var categoryId = Convert.ToInt32(Decrypt(Request.QueryString["CategoryID"]));

        var db = new TranscomUniversityV3ModelContainer();
        var categoryName = (from c in db.tbl_TranscomUniversity_Lkp_Category
                            where c.CategoryID == categoryId
                            select c.Category).FirstOrDefault().ToString();

        var categoryDesc = (from d in db.tbl_TranscomUniversity_Lkp_Category
                            where d.CategoryID == categoryId
                            select d.Description).FirstOrDefault().ToString();

        lblCategoryName.Text = categoryName;
        lblCategoryDesc.Text = categoryDesc != "" ? categoryDesc : "(Description)";

       
        if (!Page.IsPostBack) {
            string frompage = System.Web.Security.Roles.IsUserInRole(HttpContext.Current.User.Identity.Name.Split('|')[0], "Admin") == true ? "Admin" : "Trainer";
            lblTab1.Text = frompage;
            sidebarTrainer.Visible = false;
            sidebarAdmin.Visible = false;

            if(frompage == "Trainer")
                sidebarTrainer.Visible = true;
            else
                sidebarAdmin.Visible = true;
        }
    }

    public void GetCourseSubCatCount()
    {
        var categoryId = Convert.ToInt32(Decrypt(Request.QueryString["CategoryID"]));
        var db = new TranscomUniversityV3ModelContainer();

        var countCourseSubCategory = (from s in db.vw_TranscomUniversity_Subcategory
                                      where s.CategoryID == categoryId
                                      select s).Count().ToString();

        ltSubcategories.Text = countCourseSubCategory;
    }

    protected string Encrypt(string clearText)
    {
        string EncryptionKey = "TRNSCMV32017111";
        byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);

        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(clearBytes, 0, clearBytes.Length);
                    cs.Close();
                }
                clearText = Convert.ToBase64String(ms.ToArray());
            }
        }
        return clearText;
    }

    protected string Decrypt(string cipherText)
    {
        string EncryptionKey = "TRNSCMV32017111";
        cipherText = cipherText.Replace(" ", "+");
        byte[] cipherBytes = Convert.FromBase64String(cipherText);

        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(cipherBytes, 0, cipherBytes.Length);
                    cs.Close();
                }
                cipherText = Encoding.Unicode.GetString(ms.ToArray());
            }
        }
        return cipherText;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Form.Attributes.Add("enctype", "multipart/form-data");
        if (!System.Web.Security.Roles.IsUserInRole(HttpContext.Current.User.Identity.Name.Split('|')[0], "Trainer") && !System.Web.Security.Roles.IsUserInRole(HttpContext.Current.User.Identity.Name.Split('|')[0], "Admin"))
            Response.Redirect("~/Unauthorized.aspx");

        if (!Page.IsPostBack)
        {
           

            if (System.Web.Security.Roles.IsUserInRole(HttpContext.Current.User.Identity.Name.Split('|')[0], "Trainer"))
            {
                btnSubCatSettings.Visible = false;
                btnSubCatDelete.Visible = false;
            }
            else 
            {
                btnSubCatSettings.Visible = true;
                btnSubCatDelete.Visible = true;
            
            }

        }

        RadTab subCatContent = RadTabStrip2.Tabs.FindTabByValue("1");
        RadTab subCatHistory = RadTabStrip2.Tabs.FindTabByValue("2");

        if (subCatContent.Selected)
        {
            RadMultiPage1.SelectedIndex = 0;
        }

        if (subCatHistory.Selected)
        {
            RadMultiPage1.SelectedIndex = 1;
        }

        GetCatNameDesc();
        GetCourseSubCatCount();
    }

    protected void btnSwitchSubCatListView_Click(object sender, EventArgs e)
    {
        btnSwitchSubCatGridView.Visible = true;
        pnlSubCatGridView.Visible = true;

        gvCourseSubCategory.Rebind();

        btnSwitchSubCatListView.Visible = false;
        pnlSubCatListView.Visible = false;
    }

    protected void btnSwitchSubCatGridView_Click(object sender, EventArgs e)
    {
        btnSwitchSubCatListView.Visible = true;
        pnlSubCatListView.Visible = true;

        lvCourseSubCategory.Rebind();

        btnSwitchSubCatGridView.Visible = false;
        pnlSubCatGridView.Visible = false;
    }

    protected void lvCourseSubCategory_NeedDataSource(object source, RadListViewNeedDataSourceEventArgs e)
    {
        if (e.RebindReason == Telerik.Web.UI.RadListViewRebindReason.ExplicitRebind || e.RebindReason == Telerik.Web.UI.RadListViewRebindReason.PostBackEvent)
        {
            var categoryId = Convert.ToInt32(Decrypt(Request.QueryString["CategoryID"]));
            var courseSubCategory = DataHelper.GetCourseSubCategory(categoryId);

            if (courseSubCategory != null)
            {
                lvCourseSubCategory.DataSource = courseSubCategory;
            }
        }
    }

    protected void gvCourseSubCategory_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        var categoryId = Convert.ToInt32(Decrypt(Request.QueryString["CategoryID"]));
        var courseSubCategory = DataHelper.GetCourseSubCategory(categoryId);

        if (courseSubCategory != null)
            gvCourseSubCategory.DataSource = courseSubCategory;
    }

    protected void rcmCourseSubCategory_Click(object sender, RadMenuEventArgs e)
    {
        var itemVal = e.Item.Value;

        if (itemVal == "1")
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", DataHelper.callClientScript("addSubCategories"), true);
    }

    protected void btnAddSubCategory_Click(object sender, EventArgs e)
    {
        try
        {
            var categoryId = Convert.ToInt32(Decrypt(Request.QueryString["CategoryID"]));
            var createdBy = DataHelper.GetCurrentUserCIM();
            var db = new TranscomUniversityV3ModelContainer();

            //Logger variables Start
            string _userIP = DataHelper.GetIPAddress();
            string _userID = DataHelper.GetCurrentUserCIM();
            double _duration = 0;

            DateTime startTime;
            DateTime endTime;

            var dts = db.CreateQuery<DateTime>("CurrentDateTime() ");
            startTime = dts.AsEnumerable().First();

            //Logger variables End

            pr_TranscomUniversity_InsertSubCategory_Result retVal = db.pr_TranscomUniversity_InsertSubCategory(txtSubCategoryName.Text.Trim(), txtSubCategoryDesc.Text, categoryId, Convert.ToInt32(createdBy)).SingleOrDefault();

            if (retVal.Msg != "0")
            {
                RadWindowManager1.RadAlert("Subcategory successfully added.", 330, 180, "System Message", "");

                lvCourseSubCategory.Rebind();
                gvCourseSubCategory.Rebind();
                gvDeleteSubCategory.Rebind();
                GetCourseSubCatCount();

                //logger start transaction
                string _action = "Subcategory " + txtSubCategoryName.Text.Trim() + " was added by " + _userID;

                var dte = db.CreateQuery<DateTime>("CurrentDateTime() ");

                endTime = dte.AsEnumerable().First();
                _duration = (endTime - startTime).TotalMilliseconds;

                int l_actionID = DataHelper.getLastCategoryLogID() + 1;
                DataHelper.logCategory(categoryId, l_actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);
                //logger end transaction

            }
            else
                RadWindowManager1.RadAlert("Subcategory name already exists. Please try another name.", 330, 180, "System Message", "");

            txtSubCategoryName.Text = "";
            txtSubCategoryDesc.Text = "";
        }
        catch
        {
            RadWindowManager1.RadAlert("Error adding subcategory. Please contact your System Administrator.", 330, 180, "Error Message", "");
        }
    }

    protected void btnSubCatDelete_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", DataHelper.callClientScript("deleteSubCategories"), true);
    }

    protected void gvDeleteSubCategory_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        var categoryId = Convert.ToInt32(Decrypt(Request.QueryString["CategoryID"]));
        var courseSubCategory = DataHelper.GetCourseSubCategory(categoryId);

        if (courseSubCategory != null)
            gvDeleteSubCategory.DataSource = courseSubCategory;
    }

    protected void gvDeleteSubCategory_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == RadGrid.DeleteCommandName)
        {
            var deletedItem = e.Item as GridEditableItem;

            try
            {
                var subCatId = deletedItem.GetDataKeyValue("SubcategoryID");
                var db = new TranscomUniversityV3ModelContainer();

                var categoryId = Convert.ToInt32(Decrypt(Request.QueryString["CategoryID"]));

                //Logger variables Start
                string _userIP = DataHelper.GetIPAddress();
                string _userID = DataHelper.GetCurrentUserCIM();
                double _duration = 0;

                DateTime startTime;
                DateTime endTime;

                var dts = db.CreateQuery<DateTime>("CurrentDateTime() ");
                startTime = dts.AsEnumerable().First();

                //Logger variables End

                db.pr_TranscomUniversity_DeleteSubCategory(Convert.ToInt32(subCatId));

                //logger start transaction
                string _action = "Subcategory " + txtSubCategoryName.Text.Trim() + " was deleted by " + _userID;

                var dte = db.CreateQuery<DateTime>("CurrentDateTime() ");

                endTime = dte.AsEnumerable().First();
                _duration = (endTime - startTime).TotalMilliseconds;

                int l_actionID = DataHelper.getLastCategoryLogID() + 1;
                DataHelper.logCategory(categoryId, l_actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);
                //logger end transaction
                RadWindowManager1.RadAlert("Subcategory has been Deleted!.", 330, 180, "Delete Message", "");

                gvDeleteSubCategory.Rebind();
                lvCourseSubCategory.Rebind();
                gvCourseSubCategory.Rebind();
                GetCourseSubCatCount();
            }
            catch
            {
                RadWindowManager1.RadAlert("Error deleting subcategory. Please contact your System Administrator.", 330, 180, "Error Message", "");
            }
        }
    }

    protected void rgHistory_NeedDataSource(object sender, EventArgs e)
    {
        int CategoryID = Convert.ToInt32(Utils.Decrypt(Request.QueryString["CategoryID"]));
        var db = new TranscomUniversityV3ModelContainer();
        var res = db.vw_TranscomUniversity_CategoryLog
                    .Where(c => c.CategoryID == CategoryID)
                    .ToList();
        rgHistory.DataSource = res;
        ltLogCount.Text = res.Count.ToString();
    }   

    protected void lbtnRefreshHistory_Click(object sender, EventArgs e)
    {
        rgHistory.Rebind();
    }
}