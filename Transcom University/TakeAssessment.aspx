﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TranscomUniversityV3.Master" AutoEventWireup="true"
    CodeFile="TakeAssessment.aspx.cs" Inherits="TakeAssessment" EnableEventValidation="false" %>

<%@ Register Src="UserControl/Statics/CourseCatalogSidebarUserCtrl.ascx" TagName="courseCatalogSidebar"
    TagPrefix="ucCcSidebar" %>
<%@ Register Src="UserControl/Pages/Assessment/ExamAllItems.ascx" TagName="AllItems"
    TagPrefix="ucAllItems" %>
<%@ Register TagPrefix="wus" TagName="examquestioncontrol" Src="~/UserControl/Pages/Assessment/examquestioncontrol.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="contentPlaceHolderLeftPanel" runat="Server">
    <rad:RadScriptBlock ID="RadScriptBlock1" runat="server">
        <script type="text/javascript">
            function pageLoad() {
                $(window).scroll(function () {
                    console.log($(window).scrollTop());
                    if ($(window).scrollTop() > 200) {
                        $("#<%= pnlTimer.ClientID %>").css({ "top": "8vh" })
                    }
                    if ($(window).scrollTop() < 200) {
                        $("#<%= pnlTimer.ClientID %>").css({ "top": "30vh" })
                    }
                });

                //set course image
//                var img = sessionStorage.getItem("cimg");
//                $("#takeAssessmentCourseImage").attr("src", (img));
            }
            function btnBackToCoursePreview_ClientClick() {
                window.location.href = "CourseLauncher.aspx?CourseID=" + '<%= Request.QueryString["CourseID"] %>' + "&tz=" + '<%= Request.QueryString["tz"] %>';
            }

            function btnPause_ClientClick() {
                $("#<%= btnSaveAndQuit.ClientID %>").click();
                return false;
            }

            function btnSubmit_ClientClick() {

                //radalert("You are about to submit this assessment </br> Would you like to proceed", 330, 180, "Submit Answers", callBackSubmitAssessment, "");
                $find("<%= rwConfirmSubmit.ClientID %>").show();
                return false;
            }

            function closeConfirmSubmit() {
                $find("<%= rwConfirmSubmit.ClientID %>").close();
            }

            function confirmSubmit() {
                $("#<%= btnContinue.ClientID %>").click();
            }

            function btnTakeAssessment_ClientClick() {
                //$("#<%= btnTakeAssessment.ClientID %>").attr("disabled", "true");
                $("#loadingMessage").removeClass("display-none");
            }

            function startExam() {
                $("#<%= starterTimer.ClientID %>").addClass("display-none").removeClass("display-block");
                $("#<%= pnlAllItems.ClientID %>").addClass("blur-no").removeClass("blur-md");
                $("#<%= pnlAllItems.ClientID %> .overlay").addClass("display-none");
            }
            
        </script>
    </rad:RadScriptBlock>
    <rad:RadAjaxLoadingPanel ID="localLoadingPanel" runat="server" Transparency="25"
        CssClass="Loading2" />
    <rad:RadAjaxLoadingPanel ID="fullPageLoading" runat="server" Transparency="25" CssClass="Loading"
        IsSticky="true" />
    <rad:RadWindowManager ID="rwm" RenderMode="Lightweight" EnableShadow="true" Skin="Bootstrap"
        Modal="true" VisibleOnPageLoad="false" Behaviors="Close, Move" DestroyOnClose="true"
        RestrictionZoneID="RestrictionZone" Opacity="99" runat="server" VisibleStatusbar="false">
        <Windows>
            <rad:RadWindow ID="rwConfirmSubmit" runat="server" CssClass="tc-radwindow-1" Title="Confirm action to submit assessment"
                Width="500">
                <ContentTemplate>
                    <br />
                    <br />
                    <p class="text-center bold">
                        <asp:Label runat="server" ID="youassess" Text= "<%$ Resources:LocalizedResource, Youareabouttosubmitthisassessment %>"></asp:Label> 
                    </p>
                    <br />
                    <p class="text-center bold">
                       <asp:Label runat="server" ID="liketoproc" Text= "<%$ Resources:LocalizedResource, Wouldyouliketoproceed %>"></asp:Label> 
                    </p>
                    <br />
                    <br />
                    <br />
                    <div class="text-center">
                        <button class=" btn btn-md  btn-teal btn-flat" onclick="closeConfirmSubmit(); return false;">
                            <asp:Label runat="server" ID="cancel" Text= "<%$ Resources:LocalizedResource, Cancel %>"></asp:Label> </button>
                        <button class=" btn btn-md  btn-teal  btn-flat" onclick="confirmSubmit(); return false;">
                          <asp:Label runat="server" ID="yespro" Text= "<%$ Resources:LocalizedResource, YesProceed %>"></asp:Label> </button>
                    </div>
                </ContentTemplate>
            </rad:RadWindow>
        </Windows>
    </rad:RadWindowManager>
    <rad:RadAjaxManager ID="ram1" runat="server">
        <AjaxSettings>
            <%--    <rad:AjaxSetting AjaxControlID="btnTakeAssessment">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlActionButtonContainer" LoadingPanelID="fullPageLoading" />
                    <rad:AjaxUpdatedControl ControlID="pnlAllItemsContainer" LoadingPanelID="" />
                  <rad:AjaxUpdatedControl ControlID="holderAllItems" LoadingPanelID="localLoadingPanel" />
                </UpdatedControls>
            </rad:AjaxSetting>--%>
            <rad:AjaxSetting AjaxControlID="tmrStartTimeRemaining">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlTimer" LoadingPanelID="" />
                </UpdatedControls>
            </rad:AjaxSetting>
        </AjaxSettings>
    </rad:RadAjaxManager>
    <ucCcSidebar:courseCatalogSidebar ID="ccSidebar" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolderMain" runat="Server">
    <asp:Panel runat="server" ID="pnlAssessmentDetailContainer" CssClass="col-md-12">
        <div class="row course-preview-row-container bottom-margin-md">
            <div class="col-md-12 course-preview-header">
                <ul id="ulBreadcrumb" runat="server" class="breadcrumb">
                    <li id="bc1" runat="server"><a>
                        <asp:Label ID="lblTab0" runat="server" Text="<%$ Resources:LocalizedResource, Learner %>" /></a></li>
                    <li id="bc2" runat="server"><a>
                        <asp:Label ID="lblTab1" runat="server" Text="<%$ Resources:LocalizedResource, CourseCatalog %>" /></a></li>
                    <li id="bc3" runat="server"><a>
                        <asp:Label ID="lblTab2" runat="server" Text="" /></a></li>
                    <li id="bc4" runat="server">
                        <asp:Label ID="lblTab3" runat="server" Text="" ForeColor="Yellow" /></li>
                </ul>
            </div>
        </div>
        <asp:Panel runat="server" ID="pnlExamContainer" CssClass="">
            <div class="col-md-4 text-center">
                <div class="col-md-12">
                    <div class="exam-image-container">
                        <asp:Label Text="Exam Title" runat="server" ID="lblExamTitle" CssClass="exam-title" />
                        <img id="takeAssessmentCourseImage" src="#" alt="Course Image" width="350" height="250"
                            onerror="this.src='Media/Uploads/CourseImg/No_image.jpg'" class="margin-bottom-10px exam-image" runat="server"></img>
                    </div>
                    <asp:Panel runat="server" ID="pnlActionButtonContainer" CssClass="container">
                        <div class="col-md-6 pull-left">
                            <asp:Button ID="btnBack" Text="<%$ Resources:LocalizedResource, Back %>" CssClass="btn tc-btn-md btn-teal btn-flat" runat="server"
                                OnClientClick="btnBackToCoursePreview_ClientClick(); return false;" />
                            <asp:Button ID="btnPause" Text="<%$ Resources:LocalizedResource, Pause %>" CssClass="btn tc-btn-md btn-teal btn-flat"
                                runat="server" Visible="false" OnClientClick="btnPause_ClientClick(); return false;" />
                        </div>
                        <div class="col-md-6 pull-right">
                            <asp:Button ID="btnTakeAssessment" Text="<%$ Resources:LocalizedResource, TakeAssessment %>" CssClass="btn tc-btn-md btn-teal btn-flat"
                                runat="server" OnClick="btnTakeAssessment_Click" OnClientClick="btnTakeAssessment_ClientClick();" />
                            <asp:Button ID="btnSubmit" Text="<%$ Resources:LocalizedResource, Submit %>" CssClass="btn tc-btn-md btn-teal btn-flat"
                                runat="server" OnClientClick="btnSubmit_ClientClick(); return false;" Visible="false" />
                        </div>
                    </asp:Panel>
                </div>
            </div>
            <div class="col-md-4 no-padding" style="margin-left: -30px;">
                <span class="bold">
                    <asp:Literal ID="ltInstructionLabel" Text="<%$ Resources:LocalizedResource, AssessmentInstruction %>" runat="server" /></span>
                <p runat="server" id="pInstruction">
                   <asp:Label runat="server" ID="tructions" Text= "<%$ Resources:LocalizedResource, Instructions %>"></asp:Label>  
                </p>
                <br />
                <br />
                <span class="bold">
                    <asp:Literal ID="ltDetailsLabel" Text="Assessment Details:" runat="server" /></span>
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                          <asp:Label runat="server" ID="datetimestart" Text= "<%$ Resources:LocalizedResource, StartDatetime %>"></asp:Label>:
                        </td>
                        <td>
                            <asp:Literal ID="ltStartDate" Text="<%$ Resources:LocalizedResource, January %>" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                           <asp:Label runat="server" ID="endtimedate" Text= "<%$ Resources:LocalizedResource, EndDatetime %>"></asp:Label>:
                        </td>
                        <td>
                            <asp:Literal ID="ltEndDate" Text="January 1, 2019" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                          <asp:Label runat="server" ID="dur" Text= "<%$ Resources:LocalizedResource, Duration %>"></asp:Label>:
                        </td>
                        <td>
                            <asp:Literal ID="ltDuration" Text="0" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                           <asp:Label runat="server" ID="questno" Text= "<%$ Resources:LocalizedResource, NoofQuestions %>"></asp:Label> :
                        </td>
                        <td>
                            <asp:Literal ID="ltNumbOfQuestion" Text="99" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" ID="epyt" Text= "<%$ Resources:LocalizedResource, Type %>"></asp:Label> :&nbsp;
                        </td>
                        <td>
                            <asp:Literal ID="ltType" Text="<%$ Resources:LocalizedResource, Assessment %>" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-md-12 display-none course-preview-row-container" id="loadingMessage"
                runat="server" clientidmode="Static">
                <br />
                <br />
                <span class="bold"><asp:Label runat="server" ID="loadi" Text= "<%$ Resources:LocalizedResource, Loading %>"></asp:Label> </span>
                <br />
                <span><asp:Label runat="server" ID="waitpatinet" Text= "<%$ Resources:LocalizedResource, Pleasewaitpatientlywhilewereloadingyour %>"></asp:Label> 
                    <asp:Literal ID="ltLoadingText" Text="exam" runat="server" />
                    .</span>
            </div>
            <asp:Panel runat="server" ID="pnlAllItemsContainer" CssClass="min-height-60vh col-md-12"
                Visible="false">
                <%-- <asp:PlaceHolder ID="holderAllItems" runat="server"></asp:PlaceHolder>
                <ucAllItems:AllItems ID="AllItems1" runat="server"/>--%>
                <asp:Panel ID="pnlTimer" runat="server" HorizontalAlign="Center" Font-Size="12px"
                    Width="200px" CssClass="" Style="width: 200px; z-index: 2; right: 7vw; position: fixed;
                    top: 30vh;">
                    <%-- <asp:UpdatePanel ID="udpTimer" runat="server" UpdateMode="conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="tmrTimeRemaining" EventName="Tick" />
                        </Triggers>
                        <ContentTemplate>
                            <table width="100%" id="examTimer" class="display-nones" runat="server">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblTimeRemaining" runat="server" Text="Time Remaining:" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblActualTimeLeft" runat="server" Visible="false" />
                                        <asp:Label ID="lblTimeRemainingValue" runat="server" />
                                        <asp:Timer ID="tmrTimeRemaining" runat="server" Interval="1000" Enabled="false" OnTick="tmrTimeRemaining_Tick" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                   </asp:UpdatePanel>--%>
                    <div class="text-center" id="starterTimer" runat="server">
                        <h4>
                            <asp:Label ID="lblTimer" Text="<%$ Resources:LocalizedResource, Yourtestwillbeginin %>" runat="server" ViewStateMode="Enabled" />
                        </h4>
                        <asp:Label ID="lblActualStarterTime" runat="server" Visible="false" Text="5" />
                        <asp:Label ID="lblStarterTime" runat="server" Text="5" Font-Size="5em" />
                        <asp:HiddenField ID="hfTimeRemaining" Value="5" runat="server" />
                        <asp:Timer ID="tmrStartTimeRemaining" runat="server" Interval="1000" Enabled="true"
                            OnTick="tmrStartTimeRemaining_Tick" />
                    </div>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlAllItems" CssClass="col-md-12 exam-container borderless">
                    <asp:Panel ID="pnlOverlay" runat="server" CssClass="overlay" Style="width: 100%; background: white;
                        height: 100%; position: absolute; opacity: .2;">
                    </asp:Panel>
                    <table width="100%" cellspacing="0">
                        <tr>
                            <td colspan="3">
                                <br />
                                <asp:Label ID="lblNote" runat="server" Text="" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:GridView ID="gvQuestions" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                    AllowSorting="false" Width="100%">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <table width="100%" cellspacing="0" class="table border-lightgrey">
                                                    <tr>
                                                        <td style="width: 100%">
                                                            <asp:Label ID="lblGridQuestionnaireID" runat="server" Text='<%#Bind("QuestionnaireID") %> '
                                                                Visible="false" />
                                                            <asp:Label ID="lblGridTestCategoryID" runat="server" Text='<%#Bind("TestCategoryID") %>'
                                                                Visible="false" />
                                                            <wus:examquestioncontrol ID="wusQuestion" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <%--<tr>
            <td colspan="3" align="center">
                <asp:UpdateProgress ID="udpProgressLeft" runat="server">
                    <ProgressTemplate>
                        <asp:Panel ID="pnlLeft" runat="server" Width="20px">
                            <table width="100%">
                                <tr>
                                    <td align="center" valign="middle">
                                        <asp:Image ID="imgLeft" runat="server" ImageUrl="~/images/ajax-loader-small-2.gif" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </td>
        </tr>--%>
                        <tr>
                            <%--<td style="width: 15%">
                                <asp:Label ID="lblOnce" runat="server" Text="Please click the buttons only once."
                                    CssClass="smallerText" />
                            </td>--%>
                            <td align="center" style="width: 70%">
                                <asp:Button ID="btnSaveAndQuit" runat="server" Text="<%$ Resources:LocalizedResource, Pause %>" OnClick="btnSaveAndQuit_Click"
                                    CssClass="btn tc-btn-md btn-teal btn-flat" UseSubmitBehavior="false" />
                                <asp:Button ID="Button1" Text="Submit" CssClass="btn tc-btn-md btn-teal btn-flat"
                                    runat="server" OnClientClick="btnSubmit_ClientClick(); return false;" />
                                <asp:Button ID="btnContinue" runat="server" Text="I'm Finished!" OnClick="btnSubmit_Click"
                                    CssClass="display-none" />
                            </td>
                            <td align="right" style="width: 15%">
                            </td>
                        </tr>
                    </table>
                    <ajax:ModalPopupExtender ID="mpeNoReplacement" runat="server" BackgroundCssClass="modalBackground"
                        PopupControlID="pnlNoReplacement" TargetControlID="hidNoReplacement" />
                    <asp:HiddenField ID="hidNoReplacement" runat="server" />
                    <asp:Panel ID="pnlNoReplacement" runat="server" CssClass="modalPopup bg-color-light-gray"
                        Width="300px">
                        <table cellpadding="10" cellspacing="0" style="vertical-align: middle; font-size: large;"
                            width="100%">
                            <tr>
                                <td align="left">
                                    <asp:Label ID="lblNoReplacement" runat="server" Text="<%$ Resources:LocalizedResource, Youarenotallowedtochangeyouranswer %>"
                                        Font-Size="12px" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Button ID="btnNoReplacement" runat="server" Text="<%$ Resources:LocalizedResource, Next %>" OnClick="btnNoReplacement_Click"
                                        Width="80" CssClass="buttons" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <ajax:ModalPopupExtender ID="mpeWarning" runat="server" BackgroundCssClass="modalBackground"
                        TargetControlID="hidWarning" PopupControlID="pnlWarning" />
                    <asp:HiddenField ID="hidWarning" runat="server" />
                    <asp:Panel ID="pnlWarning" runat="server" CssClass="modalPopup bg-color-light-gray"
                        HorizontalAlign="left" Width="300px">
                        <table cellpadding="10" cellspacing="0" style="vertical-align: middle; font-size: large;"
                            width="100%">
                            <tr>
                                <td>
                                    <asp:Label ID="lblWarning" runat="server" Font-Size="12px" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="btnWarning" runat="server" Text="<%$ Resources:LocalizedResource, Return %>" CssClass="buttons" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <%-- <ajax:AlwaysVisibleControlExtender ID="avcTimeRemaining" runat="server" TargetControlID="pnlTimer"
        VerticalSide="Top" HorizontalSide="Right" VerticalOffset="10" HorizontalOffset="10"
        ScrollEffectDuration=".1" />--%>
                    <ajax:ModalPopupExtender ID="mpePnlTimeUp" runat="server" BackgroundCssClass="modalBackground"
                        PopupControlID="pnlTimeUp" TargetControlID="hidTimeUp" />
                    <asp:HiddenField ID="hidTimeUp" runat="server" />
                    <asp:Panel ID="pnlTimeUp" runat="server" HorizontalAlign="left" CssClass="modalPopup"
                        Width="200px">
                        <table width="100%" cellpadding="5">
                            <tr>
                                <td>
                                    <asp:Label ID="lblTimeUp" runat="server" Text="<%$ Resources:LocalizedResource, Timesup %>" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="btnContinuePopup" runat="server" Text="<%$ Resources:LocalizedResource, Continue %>" OnClick="btnSubmit_Click"
                                        Width="80px" CssClass="buttons" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <ajax:ModalPopupExtender ID="mpeMissingAnswer" runat="server" BackgroundCssClass="modalBackground"
                        PopupControlID="pnlMissingAnswer" TargetControlID="hidMissingAnswer" />
                    <asp:HiddenField ID="hidMissingAnswer" runat="server" />
                    <asp:Panel ID="pnlMissingAnswer" runat="server" HorizontalAlign="left" CssClass="modalPopup"
                        Width="300px">
                        <table width="100%" cellpadding="5">
                            <tr>
                                <td align="left">
                                   <asp:Label runat="server" ID="indred" Text= "<%$ Resources:LocalizedResource, Pleasecheckthatyouhaveanswersforitemsinred %>"></asp:Label> 
                                    <br />
                                    <asp:Button ID="btnMissingAnswerOK" runat="server" Text="<%$ Resources:LocalizedResource, Ok %>" CssClass="buttons" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
</asp:Content>
