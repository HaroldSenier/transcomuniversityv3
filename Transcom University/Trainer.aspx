﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TranscomUniversityV3.Master" AutoEventWireup="true"
    CodeFile="Trainer.aspx.cs" Inherits="Trainer" %>

<%@ Register Src="UserControl/Pages/CourseManagement.ascx" TagName="CourseMngtUC"
    TagPrefix="uccm" %>
<%@ Register Src="UserControl/Pages/AdminCoursesForumUserCtrl.ascx" TagName="CourseForumUserCtrl"
    TagPrefix="ucf" %>
<%@ Register Src="UserControl/Pages/AdminForumTopics.ascx" TagName="ForumTopicsUserCtrl"
    TagPrefix="ucft" %>
<%@ Register Src="UserControl/Pages/AdminForumAddNewTopic.ascx" TagName="ForumAddTopic"
    TagPrefix="ucat" %>
<%@ Register Src="UserControl/Pages/ForumThreadUserCtrl.ascx" TagName="ForumThread"
    TagPrefix="uctd" %>
<%@ Register Src="UserControl/Pages/CategoriesUserCtrl.ascx" TagName="Categories"
    TagPrefix="ucct" %>
<%@ Register Src="UserControl/Pages/TrainerCalendar.ascx" TagName="MyCalendar" TagPrefix="MyCalendarUC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="contentPlaceHolderLeftPanel" runat="Server">
    <rad:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Transparency="25"
        IsSticky="true" CssClass="Loading" />
    <rad:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel2" runat="server" MinDisplayTime="1000"
        CssClass="Loading2" Transparency="25" />
    <rad:RadAjaxLoadingPanel ID="noLoadingPanel" runat="server" CssClass="Loading-Empty"
        Transparency="25" />
    <rad:RadAjaxLoadingPanel ID="fullPageLoading" runat="server" Transparency="25" IsSticky="true"
        CssClass="Loading" />
    <rad:RadAjaxManager ID="RadAjaxManager1" runat="server" DefaultLoadingPanelID="RadAjaxLoadingPanel1">
    </rad:RadAjaxManager>
    <div class="sidebar-relative">
        <div class="side-menu-container">
            <div class="overlay">
            </div>
            <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
                <ul class="nav sidebar-nav">
                    <li class="sidebar-brand" style="z-index: 1000; color: #fff;">
                        <a href="#" class="close"><i class="fa fa-times" aria-hidden="true"></i></a> <asp:Label runat="server" ID="dashtrain" Text= "<%$ Resources:LocalizedResource, TrainerDashboard %>"></asp:Label> 
                    </li>
                    <rad:RadTabStrip ID="RadTabStrip1" runat="server" RenderMode="Lightweight" SelectedIndex="0" Skin="Black"> <%--OnClientTabSelected="onClientTabSelected"--%>
                        <Tabs>
                            <rad:RadTab runat="server" Text="My Class" Value="1"  Width="285px" SelectedCssClass="rtsSelected" CssClass="toggle-submenu"  PostBack="false" Visible="false" >
                                 <TabTemplate>
                                  <ul class="nav custom-submenu panel-title" id="menuPageEditor">
		                            <li>
                                        <a class="tab-title" onclick="tabClick(this);" ><asp:Label runat="server" ID="MyClass" Text= "<%$ Resources:LocalizedResource, MyClass %>"></asp:Label>
                                         <i class="fa fa-sort-down pull-right hover-pointer js-collapse-event-2" data-toggle="collapse" data-target="#pageEditorMenu" aria-expanded="false"></i>
                                        </a>
			                            <ul class="nav collapse submenu" id="pageEditorMenu" role="menu">
				                            <li id="tabCourseLogins">
                                                <a id="tabHomepageEditor" href="Admin.aspx?Tab=HomepageEditor" class="submenu-title"><asp:Label runat="server" ID="lisclas" Text= "<%$ Resources:LocalizedResource, ClassList %>"></asp:Label>
                                                </a>
                                            </li>
				                             <li id="Li5">
                                                <a class="submenu-title" data-toggle="collapse" data-target="#courseCatalogMenu" aria-expanded="false" onclick="tabClick(this);">Class Calendar
                                                </a>
                                            </li>
			                            </ul>
		                            </li>
	                            </ul>                                
                                </TabTemplate>
                            </rad:RadTab>
                            <%--<rad:RadTab runat="server" Text="Class Management" Value="2" NavigateUrl="Trainer.aspx?Tab=ClassMgmt" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>--%>
                            <rad:RadTab runat="server" Text="My Class" Value="2" Width="285px" PostBack="false" CssClass="toggle-submenu" >
                                <TabTemplate>
                                  <ul class="nav custom-submenu panel-title" id="menuMyClass">
		                            <li>
                                        <a class="tab-title" data-toggle="collapse" data-target="#myClassMenu" aria-expanded="false" onclick="tabClick(this);">My Class
                                         <i class="fa fa-sort-down pull-right hover-pointer js-collapse-event-2">
                                        </i>
                                        </a>
			                            <ul class="nav collapse submenu" id="myClassMenu" role="menu" aria-labelledby="customSubmenu">
				                            <li id="tabClassList"><a href="Trainer.aspx?Tab=ClassMgmt"><asp:Label runat="server" ID="classl" Text= "<%$ Resources:LocalizedResource, ClassList %>"></asp:Label> </a></li>
				                            <li id="tabClassCalendar"><a href="Trainer.aspx?Tab=ClassCalendar"><asp:Label runat="server" ID="ClassCalendar" Text= "<%$ Resources:LocalizedResource, ClassCalendar %>"></asp:Label> </a></li>
			                            </ul>
		                            </li>
	                            </ul>
                                </TabTemplate>
                            </rad:RadTab>
                            <rad:RadTab runat="server" Text="<%$ Resources:LocalizedResource, ClassManagement %>" Value="6" NavigateUrl="~/ClassManagement.aspx?viewBy=Trainer" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                            <rad:RadTab runat="server" Text="<%$ Resources:LocalizedResource, CourseManagement %>" Value="3" NavigateUrl="Trainer.aspx?Tab=CourseMgmt" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                            
                            <rad:RadTab runat="server" Text="<%$ Resources:LocalizedResource, CourseCategories %>" Value="4" NavigateUrl="Trainer.aspx?Tab=CourseCat" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                            <rad:RadTab runat="server" Text="<%$ Resources:LocalizedResource, Forums %>" Value="5" NavigateUrl="Trainer.aspx?Tab=TrainerForum" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                        </Tabs>
                    </rad:RadTabStrip>
                </ul>
                <div id="divCalendar" style="display: inline-block; position: relative; margin-top: 100%;" >
            <rad:RadCalendar RenderMode="Lightweight" runat="server" ID="RadCalendar1" EnableMultiSelect="false"
                DayNameFormat="FirstTwoLetters" EnableNavigation="true" Visible="false" 
                EnableMonthYearFastNavigation="true" Skin="Metro" CssClass="Calendar1" >                            
                <%--<ClientEvents OnDateSelected="OnCalendar1DateSelected" OnCalendarViewChanged="OnCalendar1ViewChanged" />--%>
                   <SpecialDays>
                        <rad:RadCalendarDay Repeatable="Today">
                            <ItemStyle CssClass="rcToday" />
                        </rad:RadCalendarDay>
                    </SpecialDays>
            </rad:RadCalendar>
            </div>
            </nav>
            <div class="hamburger-container text-center" style="font-size: 20px; color: #fff;
                margin-top: 10px">
                <a href="#" class="open-nav is-closed animated fadeInLeft"><i class="fa fa-bars"
                    aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
    <rad:RadWindowManager ID="RadWindowManager1" RenderMode="Lightweight" EnableShadow="true"
        Skin="Bootstrap" Modal="true" VisibleOnPageLoad="false" Behaviors="Close, Move"
        DestroyOnClose="true" RestrictionZoneID="RestrictionZone" Opacity="99" runat="server"
        Width="800" Height="400px">
    </rad:RadWindowManager>
    <rad:RadScriptBlock ID="rs" runat="server">
        <script type="text/javascript">
            function tabClick(el) {
                var caret = $(el).find("i.js-collapse-event-2");
                if ($("i.js-collapse-event-2").hasClass("fa-sort-up")) {
                    $("i.js-collapse-event-2").removeClass("fa-sort-up").addClass("fa-sort-down");
                    $("#divCalendar").css({ "margin-top": "100%" });

                } else {
                    $("i.js-collapse-event-2").removeClass("fa-sort-down").addClass("fa-sort-up");
                    $("#divCalendar").css({ "margin-top": "120%" });

                    var href = $("#tabClassCalendar a").attr("href");
                    var tz = $("#hfEncryptedOffset").val();
                    $("#tabClassCalendar a").attr("href", href + "&tz=" + tz);
                }
            }
        </script>
    </rad:RadScriptBlock>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolderMain" runat="Server">
    <div class="col-sm-11 col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12 thirty-px-padding">
                        <div class="col-md-12 no-paddings">
                            <ul id="ulBreadcrumb" runat="server" class="breadcrumb">
                                <li><a>
                                    <asp:Label ID="lblTab1" runat="server" Text="<%$ Resources:LocalizedResource, TrainerDashboard %>" /></a></li>
                                <li><a>
                                    <asp:Label ID="lblTab2" runat="server" Text="" /></a></li>
                                <li id="item3" runat="server" visible="false"><a>
                                    <asp:Label ID="lblTab3" runat="server" Text="" ClientIDMode="Static" /></a></li>
                                <li id="item4" runat="server" visible="false"><a>
                                    <asp:Label ID="lblTab4" runat="server" Text="" /></a></li>
                            </ul>
                        </div>
                        <rad:RadMultiPage ID="rmp1" runat="server" RenderMode="Lightweight" SelectedIndex="0"
                            CssClass="outerMultiPage" RenderSelectedPageOnly="true">
                            <rad:RadPageView ID="rdpCourseManagement" runat="server">
                                <uccm:CourseMngtUC ID="CourseMngt1" runat="server" />
                            </rad:RadPageView>
                            <rad:RadPageView ID="rdpCourseCategories" runat="server">
                                <br />
                                <br />
                                <br />
                                <br />
                                <div style="overflow-x: hidden;">
                                    <ucct:Categories ID="Categories1" runat="server" />
                                </div>
                            </rad:RadPageView>
                            <rad:RadPageView ID="rdpForum" runat="server">
                                <br />
                                <br />
                                <br />
                                <br />
                                <ucf:CourseForumUserCtrl ID="coursesForum" runat="server" Visible="false" />
                                <ucft:ForumTopicsUserCtrl ID="forumTopics" runat="server" Visible="false" />
                                <ucat:ForumAddTopic ID="forumAddTopic" runat="server" Visible="false" />
                                <uctd:ForumThread ID="forumThreadView" runat="server" Visible="false" />
                            </rad:RadPageView>
                            <rad:RadPageView ID="rdpMyClass" runat="server">
                                <MyCalendarUC:MyCalendar ID="myCalendar1" runat="server" />
                            </rad:RadPageView>
                        </rad:RadMultiPage>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hfIsTrainerView" runat="server" Value="1" ClientIDMode="Static" />
</asp:Content>
