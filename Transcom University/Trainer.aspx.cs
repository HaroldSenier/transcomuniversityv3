﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Text;
using TranscomUniversityV3Model;

public partial class Trainer : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack) {
            Session["FromPage"] = "Trainer";
        }

        hfIsTrainerView.Value = Utils.Encrypt(Convert.ToInt32(DataHelper.GetCurrentUserCIM()));
        RadTab myCLass = RadTabStrip1.Tabs.FindTabByValue("1");
        RadTab myCalendar = RadTabStrip1.Tabs.FindTabByValue("2");
        RadTab courseManagement = RadTabStrip1.Tabs.FindTabByValue("3");
        RadTab courseCategories = RadTabStrip1.Tabs.FindTabByValue("4");
        RadTab forum = RadTabStrip1.Tabs.FindTabByValue("5");

        lblTab2.Text = "Course Management";
        //CreateCourse
        if (Request.QueryString["Tab"] != null && Request.QueryString["Tab"].ToString() == "CourseMgmt")
        {
            rmp1.SelectedIndex = 0;
            lblTab2.Text = "Course Management";
            courseManagement.Selected = true;
        }
        else if (Request.QueryString["Tab"] != null && Request.QueryString["Tab"].ToString() == "CourseCat")
        {
            rmp1.SelectedIndex = 1;
            lblTab2.Text = "Course Categories";
            courseCategories.Selected = true;
        }
        else if (Request.QueryString["Tab"] != null && Request.QueryString["Tab"].ToString() == "ClassCalendar")
        {
            rmp1.SelectedIndex = 3;
            lblTab2.Text = "Class Calendar";
            myCalendar.Selected = true;
            RadCalendar1.Visible = true;
            RadCalendar1.FocusedDate = DataHelper.serverDate();
        }
        else if (Request.QueryString["Tab"] != null && Request.QueryString["Tab"].ToString() == "TrainerForum")
        {
            rmp1.SelectedIndex = 2;
            lblTab2.Text = "Forum";
            forum.Selected = true;

            if (Request.QueryString["forumaction"] != null && Request.QueryString["forumaction"].ToString() == "AddNewTopic")
            {
                var courseTitle = DataHelper.getCourseTitleByID(Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"].ToString())));
                item3.Visible = true;
                lblTab3.Text = courseTitle;
                item4.Visible = true;
                //lblTab4.Text = DataHelper.getThreadTitleByID(Convert.ToInt32(Utils.Decrypt(Request.QueryString["ThreadID"].ToString())));
                lblTab4.Text = "Add New Topic";
                coursesForum.Visible = false;
                forumTopics.Visible = false;
                forumAddTopic.Visible = true;
                forumThreadView.Visible = false;
            }
            else if (Request.QueryString["CourseID"] != null && Request.QueryString["ThreadID"] == null)
            {
                var courseTitle = DataHelper.getCourseTitleByID(Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"].ToString())));
                item3.Visible = true;
                lblTab3.Text = courseTitle;
                coursesForum.Visible = false;
                forumTopics.Visible = true;
                forumAddTopic.Visible = false;
                forumThreadView.Visible = false;
            }
            else if (Request.QueryString["ThreadID"] != null)
            {
                var courseTitle = DataHelper.getCourseTitleByID(Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"].ToString())));
                var threadTitle = DataHelper.getThreadTitleByID(Convert.ToInt32(Utils.Decrypt(Request.QueryString["ThreadID"].ToString())));
                item3.Visible = true;
                lblTab3.Text = courseTitle;
                item4.Visible = true;
                lblTab4.Text = threadTitle;
                coursesForum.Visible = false;
                forumTopics.Visible = false;
                forumAddTopic.Visible = false;
                forumThreadView.Visible = true;
            }
            else
            {
                item3.Visible = false;
                item4.Visible = false;

                coursesForum.Visible = true;
                forumTopics.Visible = false;
                forumAddTopic.Visible = false;
                forumThreadView.Visible = false;
            }

        }
    }


    #region CourseManagement

    public class CourseDetails
    {
        public string EncryptedCourseID { get; set; }

        public int? CourseID { get; set; }

        public string CourseImage { get; set; }

        public string CourseTitle { get; set; }

        public string CourseType { get; set; }

        public int? CourseDuration { get; set; }

        public string CourseCategory { get; set; }

        public string CourseSubcategory { get; set; }

        public string CourseDescription { get; set; }

        public string DateLastModified { get; set; }

        public string CoursePath { get; set; }
    }


    #region Load CourseLists

    private static IEnumerable<CourseDetails> _mandatoryCourse;
    private static IEnumerable<CourseDetails> _trendingCourse;
    private static IEnumerable<CourseDetails> _recentCourse;

    //--Start Mandatory Course Page Methods
    [System.Web.Services.WebMethod()]
    public static IEnumerable<CourseDetails> GetManadatoryCourseData(int startRowIndex, int maximumRows, string sortExpression)
    {
        return RadGrid.GetBindingData(LoadMandatoryCourse().AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data;
    }

    [System.Web.Services.WebMethod()]
    public static int GetMandatoryCount()
    {
        return LoadMandatoryCourse().Count();

    }

    [System.Web.Services.WebMethod()]
    public static IEnumerable<CourseDetails> LoadMandatoryCourse()
    {
        TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();

        _mandatoryCourse = db.vw_TranscomUniversity_MandatoryCourses
                            .AsEnumerable()
                            .Select(c => new CourseDetails
                            {
                                EncryptedCourseID = Utils.Encrypt(c.CourseID),
                                CourseID = c.CourseID,
                                CourseImage = c.CourseImage,
                                CourseTitle = c.CourseTitle,
                                CourseType = c.CourseType,
                                CourseDuration = c.CourseDuration,
                                CourseCategory = c.CourseCategory,
                                CourseSubcategory = c.CourseSubcategory,
                                CourseDescription = c.CourseDescription,
                                DateLastModified = string.Format("{0: yyyy/MM/dd hh:mm:ss tt}", c.DateLastModified),
                                CoursePath = c.CoursePath
                            })
                            .OrderBy(c => c.CourseTitle)
                            .ToList();
        //string json = new JavaScriptSerializer().Serialize(mandatoryCourse);
        return _mandatoryCourse;

    }

    //--Start Trending Course Page Methods
    [System.Web.Services.WebMethod()]
    public static IEnumerable<CourseDetails> GetTrendingCourseData(int startRowIndex, int maximumRows, string sortExpression)
    {
        return RadGrid.GetBindingData(LoadTrendingCourse().AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data;
    }

    [System.Web.Services.WebMethod()]
    public static int GetTrendingCount()
    {
        return LoadTrendingCourse().Count();

    }

    [System.Web.Services.WebMethod()]
    public static IEnumerable<CourseDetails> LoadTrendingCourse()
    {
        TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();

        _trendingCourse = db.vw_TranscomUniversity_TrendingCourses
                            .AsEnumerable()
                            .Select(c => new CourseDetails
                            {
                                EncryptedCourseID = Utils.Encrypt(c.CourseID),
                                CourseID = c.CourseID,
                                CourseImage = c.CourseImage,
                                CourseTitle = c.CourseTitle,
                                CourseType = c.CourseType,
                                CourseDuration = c.CourseDuration,
                                CourseCategory = c.CourseCategory,
                                CourseSubcategory = c.CourseSubcategory,
                                CourseDescription = c.CourseDescription,
                                DateLastModified = string.Format("{0: yyyy/MM/dd hh:mm:ss tt}", c.DateLastModified),
                                CoursePath = c.CoursePath
                            })
                            .OrderBy(c => c.CourseTitle)
                            .ToList();
        //string json = new JavaScriptSerializer().Serialize(mandatoryCourse);
        return _trendingCourse;

    }

    //--Start Recently Added Course Page Methods
    [System.Web.Services.WebMethod()]
    public static IEnumerable<CourseDetails> GetRecentCourseData(int startRowIndex, int maximumRows, string sortExpression)
    {
        return RadGrid.GetBindingData(LoadRecentCourse().AsQueryable(), startRowIndex, maximumRows, sortExpression, String.Empty).Data;
    }

    [System.Web.Services.WebMethod()]
    public static int GetRecentCount()
    {
        return LoadRecentCourse().Count();

    }

    [System.Web.Services.WebMethod()]
    public static IEnumerable<CourseDetails> LoadRecentCourse()
    {
        TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();

        _recentCourse = db.pr_TranscomUniversity_RecentlyAddedCourse()
                                .AsEnumerable()
                                .Select(c => new CourseDetails
                                {
                                    EncryptedCourseID = Utils.Encrypt(c.CourseID),
                                    CourseID = c.CourseID,
                                    CourseImage = c.CourseImage,
                                    CourseTitle = c.CourseTitle,
                                    CourseType = c.CourseType,
                                    CourseDuration = c.CourseDuration,
                                    CourseCategory = c.CourseCategory,
                                    CourseSubcategory = c.CourseSubcategory,
                                    CourseDescription = c.CourseDescription,
                                    DateLastModified = string.Format("{0: yyyy/MM/dd hh:mm:ss tt}", c.DateLastModified),
                                    CoursePath = c.CoursePath
                                })
                                .ToList();
        //string json = new JavaScriptSerializer().Serialize(mandatoryCourse);
        return _recentCourse;

    }

    #endregion

    #endregion

    #region CalendarEventDetails

    [System.Web.Services.WebMethod()]
    public static dynamic GetCalendarEventDetails(int calendarID, string tz)
    {
        int decryptTz = Convert.ToInt32(Utils.Decrypt(tz));

        var offset = new TimeSpan(decryptTz / 60, decryptTz % 60, 0);

        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            var CIM = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
            var details = db.pr_TranscomUniversity_CalendarEventDetails_Trainer(calendarID, CIM)
                //var details = db.vw_TranscomUniversity_CalendarEventDetails1
                      .AsEnumerable()
                //.Where(e => e.AsEnumerable().CALENDARID == calendarID)
                       .Select(e => new
                       {
                           e.CalendarID,
                           e.Subject,
                           //EventTimeZone = e.StartTime.,
                           Title = e.Title,
                           EventWhen = string.Format("{0: dddd MMMM dd, yyyy}", (e.StartTime - offset)),
                           EventTime = string.Format("{0: hh:mm tt}", (e.StartTime - offset)) + " to " + string.Format("{0: hh:mm tt}", (e.EndTime - offset)),
                           CalendarDay = Convert.ToDateTime((e.StartTime - offset)).Day,
                           CalendarMonth = string.Format("{0: MMMM}", (e.StartTime - offset)).ToUpper(),
                           e.SessionStatusID,
                           e.EventTypeID,
                           e.EventType,
                           e.Location,
                           e.Description,
                           e.RegisteredParticipants,
                           e.AvailableSeats,
                           e.WaitListed,
                           e.TrainerName,
                           e.TrainerCIM,
                           e.CreateDate
                        })
                        .SingleOrDefault();

            if (details != null)
                return details;

            return null;
        }
    }
    #endregion

}