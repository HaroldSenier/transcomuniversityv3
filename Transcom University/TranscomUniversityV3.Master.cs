﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using Telerik.Web.UI;
using Microsoft.AspNet.Membership.OpenAuth;

namespace TranscomUniversityV3
{
    public partial class TranscomUniversityV3 : System.Web.UI.MasterPage
    {
        public string ReturnUrl { get; set; }

        public int SessionLengthMinutes
        {
            get { return Session.Timeout; }
        }

        public string SessionExpireDestinationUrl
        {
            get { return "/SessionExpired.aspx"; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            for (int i = 5; i <= 60; i += 5)
            {
                RadComboBoxItem item = new RadComboBoxItem();
                item.Value = i + "";
                item.Text = i + "";
                rcbDuration.Items.Add(item);
            }
            if(Session["SessionRoles"] == null)
                Session["SessionRoles"] = "Learner";
            if (!Page.IsPostBack)
            {
                masterServerDate.Value = DataHelper.serverDate().ToString();
            }
        }

        protected void btnRequestLogin_Click(object sender, EventArgs e)
        {
            var redirectUrl = "~/ExternalLandingPage.aspx";

            if (!String.IsNullOrEmpty(ReturnUrl))
            {
                var resolvedReturnUrl = ResolveUrl(ReturnUrl);
                redirectUrl += "?ReturnUrl=" + HttpUtility.UrlEncode(resolvedReturnUrl);
            }

            OpenAuth.RequestAuthentication("google", redirectUrl);
        }
    }
}