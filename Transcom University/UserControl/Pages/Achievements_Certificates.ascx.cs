﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Data;
using TranscomUniversityV3Model;


public partial class UserControl_Pages_Achievements_Certificates : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        makePDF();
    }

   void makePDF()
    {
        Response.ContentType = "application/pdf";

        //Response.AddHeader("content-disposition", "attachment;filename=test.pdf");
        Response.AddHeader("content-disposition", "inline;filename=bincha.pdf");

        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        string imageFilePath = string.Empty;

        //query for certificates
        var certType = Request.QueryString["tab"];
        var entCert = Request.QueryString["CertID"];
        DataSet ds = DataHelper.GetCert(Convert.ToInt32(Utils.Decrypt(entCert)));        
        string GoogleID = DataHelper.GetGoogleID();
        var title = string.Empty;

        
        var userInfo = DataHelper.GetUsersInfo();


        
        //string imageFilePath = Server.MapPath(".") + "/Media/Images/certificate-of-completion-29.jpg";
        if (certType == "int")
        {
            DataSet dsCourse = DataHelper.GetCourse(Convert.ToInt32(ds.Tables[0].Rows[0]["CourseID"].ToString()));
            var encCourse = Utils.Encrypt(Convert.ToInt32(ds.Tables[0].Rows[0]["CourseID"].ToString()));
            encCourse = encCourse.Replace("+", "");
            encCourse = encCourse.Replace("/", "");
            imageFilePath = Server.MapPath(".") + "/Media/Uploads/Certs/" + encCourse + "/" + ds.Tables[0].Rows[0]["ImgFile"].ToString();
            title = dsCourse.Tables[0].Rows[0]["Title"].ToString();
        }
        else
        {
            imageFilePath = Server.MapPath(".") +  "/Media/Uploads/Certs/" + GoogleID + "/" + ds.Tables[0].Rows[0]["ImgFile"].ToString(); ;
        }

        iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imageFilePath);

        // Page site and margin left, right, top, bottom is defined
        Document pdfDoc = new Document(null, 0, 0, 0, 0);
        pdfDoc.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());

        //Resize image depend upon your need
        //For give the size to image
        jpg.ScaleToFit(1150, 600);      

        //If you want to choose image as background then,

        jpg.Alignment = iTextSharp.text.Image.UNDERLYING;

        //If you want to give absolute/specified fix position to image.
        jpg.SetAbsolutePosition(0, 0);

        if (certType != "int"){
            jpg.SetAbsolutePosition(0, 0);
        jpg.ScaleAbsoluteHeight(pdfDoc.PageSize.Height);
        jpg.ScaleAbsoluteWidth(pdfDoc.PageSize.Width);
   }

        PdfWriter pW = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);

        pdfDoc.Open();

        pdfDoc.NewPage();
            
        
        Font brown = new Font(Font.FontFamily.HELVETICA, 16f, Font.NORMAL, BaseColor.BLACK);
        Font lightblue = new Font(Font.FontFamily.COURIER, 9f, Font.NORMAL, new BaseColor(43, 145, 175));
        Font courier = new Font(Font.FontFamily.COURIER, 45f);
        Font georgia = FontFactory.GetFont("georgia", 30f);
        georgia.Color = BaseColor.BLACK;



        Font georgiaName = new Font(Font.FontFamily.HELVETICA, 30f, Font.NORMAL,BaseColor.BLACK);

        string FullName = string.Empty;
       string dateTime = string.Empty;

        if (certType == "int")
        {
            FullName = userInfo[0].FirstName + " " + userInfo[0].LastName; 
            dateTime =  string.Format("{0:MMMM dd, yyyy}",ds.Tables[0].Rows[0]["DateAcquired"]);
        }
        else
        {
            FullName = string.Empty;
            title = string.Empty;
            dateTime = string.Empty;
        }


            Chunk c1 = new Chunk("\n\n\n\n\n", georgia);
            Chunk c2 = new Chunk("\n\n\n\n\n\n\n" + FullName + "\n\n\n\n", georgiaName);
            Chunk c3 = new Chunk(title, georgia);
            Chunk c4 = new Chunk("\n\n This " + dateTime, brown);

            // Chunk c4 = new Chunk("\n\n\n\nhas completed\n\n\n\n", brown);       

            Phrase p2 = new Phrase();
            p2.Add(c1);
            p2.Add(c2);
            p2.Add(c3);            
            p2.Add(c4);
            Paragraph p = new Paragraph();
            p.Add(p2);

            p.Alignment = Element.ALIGN_CENTER;
            
            string imageURL = Server.MapPath(".") + "/Media/Images/favicon.png";
            iTextSharp.text.Image jpg2 = iTextSharp.text.Image.GetInstance(imageURL);
            //Resize image depend upon your need
            jpg2.ScaleToFit(50f, 50f);
            //Give space before image
            jpg2.SpacingAfter = -10f;
            //Give some space after the image
            //jpg2.SpacingAfter = 1f;
            jpg2.Alignment = Element.ALIGN_RIGHT;
            jpg2.SetAbsolutePosition(700, 60);

            pdfDoc.Add(p);

            pdfDoc.Add(jpg);
            pdfDoc.Add(jpg2);
        

            //  pdfDoc.Add(rect);

            /*  ColumnText ct = new ColumnText(cb);
            Phrase myText = new Phrase("TEST paragraph\nNewline");
            ct.SetSimpleColumn(myText, 34, 750, 580, 317, 15, Element.ALIGN_LEFT);
            ct.Go();*/

            /* PdfContentByte canvas = pW.DirectContent;
            var rect = new iTextSharp.text.Rectangle(200, 200, 100, 100);
            rect.Border = iTextSharp.text.Rectangle.BOX;
            rect.BorderWidth = 5;
            rect.BorderColor = new BaseColor(0, 0, 0);
            canvas.Rectangle(rect);
            */

            pdfDoc.Close();

            Response.Write(pdfDoc);
              

       

        Response.End();
    }
  
}