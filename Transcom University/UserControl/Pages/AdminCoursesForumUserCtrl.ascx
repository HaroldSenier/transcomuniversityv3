﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AdminCoursesForumUserCtrl.ascx.cs"
    Inherits="UserControl_Pages_AdminCoursesForumUserCtrl" %>
<div class="forum-main-container">
    <h4 class="title">
        <asp:Label runat="server" ID="lblCourseswithForum" Text="<%$ Resources:LocalizedResource, CoursesWithForum %>"></asp:Label>
    </h4>
    <rad:RadGrid ID="gridCourseForum" runat="server" RenderMode="Lightweight" Skin="Bootstrap"
        OnNeedDataSource="gridCourseForum_onNeedDatasource" AllowPaging="true" AllowSorting="true"
        HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" AlternatingItemStyle-HorizontalAlign="Center"
        ClientSettings-EnableAlternatingItems="false" AutoGenerateColumns="false" HeaderStyle-Font-Bold="true">
        <MasterTableView DataKeyNames="courseID">
            <Columns>
                <rad:GridTemplateColumn UniqueName="CourseName" DataField="CourseName" HeaderText="<%$ Resources:LocalizedResource, CourseName %>"
                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="CourseName"
                    ItemStyle-CssClass="pointer">
                    <ItemTemplate>
                        <asp:HyperLink ID="lnkForumThread" runat="server" NavigateUrl='<%# Eval("CourseURL") %>'
                            Text='<%# Eval("CourseName") %>' CssClass="text-elipsis-md">
                        </asp:HyperLink>
                    </ItemTemplate>
                </rad:GridTemplateColumn>
                <rad:GridBoundColumn UniqueName="Topics" DataField="Topics" HeaderText="<%$ Resources:LocalizedResource, Topics %>">
                </rad:GridBoundColumn>
                <rad:GridBoundColumn UniqueName="Replies" DataField="Replies" HeaderText="<%$ Resources:LocalizedResource, Replies %>">
                </rad:GridBoundColumn>
                <rad:GridBoundColumn UniqueName="Views" DataField="Views" HeaderText="<%$ Resources:LocalizedResource, Views %>">
                </rad:GridBoundColumn>
                <rad:GridTemplateColumn UniqueName="LastActivityDate" HeaderText="<%$ Resources:LocalizedResource, LastActivityDate %>"
                    SortExpression="LastActivityDate">
                    <ItemTemplate>
                         <asp:Label ID="lblLastActivityDate" Text='<%# Eval("LastActivityDate") %>' runat="server"
                                    CssClass="convertTimeAt" />
                    </ItemTemplate>
                </rad:GridTemplateColumn>
            </Columns>
        </MasterTableView>
    </rad:RadGrid>
</div>
<rad:RadAjaxLoadingPanel ID="localLoadingPanel" runat="server" CssClass="Loading2"
    Transparency="25" />
<rad:RadAjaxManagerProxy ID="RadAjaxManager1" runat="server">
    <AjaxSettings>
        <rad:AjaxSetting AjaxControlID="gridCourseForum">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="gridCourseForum" LoadingPanelID="localLoadingPanel" />
                <rad:AjaxUpdatedControl ControlID="rdForumsTopics" LoadingPanelID="localLoadingPanel" />
            </UpdatedControls>
        </rad:AjaxSetting>
    </AjaxSettings>
</rad:RadAjaxManagerProxy>
<rad:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        function ConvertTimeToLocal() {
          var convertTimeAt = $(".convertTimeAt");

          $.each(convertTimeAt, function (idx, el) {
              var st = $(el).text().trim();
              debugger;
              if (st != "-") {
                  var hr = st.substring(st.length - 7, st.length - 5).trim();
                  var mm = st.substring(st.length - 4, st.length - 2).trim();
                  var tt = st.substring(st.length - 2, st.length).trim();
                  var nDate = new Date("2019-1-1 " + hr + ":" + mm + " " + tt);
                  var txtThreadDate = convertTimeZone(nDate);

                  $(el).text(st.substring(0, st.length - 7) + moment(txtThreadDate).format("hh:mm A"));
                  $(el).removeClass("convertTimeAt");
              }
          });
                }

//        $(document).ready(function () {
//            var convertTimeAt = $(".convertTimeAt");
//            $.each(convertTimeAt, function (idx, el) {
//                var st = $(el).text().trim();
//                debugger;
//                if (st != "-") {
//                    var hr = st.substring(st.length - 7, st.length - 5).trim();
//                    var mm = st.substring(st.length - 4, st.length - 2).trim();
//                    var tt = st.substring(st.length - 2, st.length).trim();
//                    var nDate = new Date("2019-1-1 " + hr + ":" + mm + " " + tt);
//                    var txtThreadDate = convertTimeZone(nDate);
//                    $(el).text(st.substring(0, st.length - 7) + moment(txtThreadDate).format("hh:mm A"));
//                    $(el).removeClass("convertTimeAt");
//                }
//            });
//        })
    </script>
</rad:RadScriptBlock>
