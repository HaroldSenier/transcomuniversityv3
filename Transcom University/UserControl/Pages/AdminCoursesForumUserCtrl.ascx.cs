﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TranscomUniversityV3Model;

public partial class UserControl_Pages_AdminCoursesForumUserCtrl : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            gridCourseForum.DataBind();
        }
    }

    protected void gridCourseForum_onNeedDatasource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        string page = Request.QueryString["Tab"] == "TrainerForum" ? "~/Trainer.aspx?Tab=TrainerForum&CourseID=" : "~/Admin.aspx?Tab=AdminForum&CourseID=";
        var db = new TranscomUniversityV3ModelContainer();
        var courseForum = db.vw_TranscomUniversity_CourseForum
                            .AsEnumerable()
                            .Select(f => new
                            {
                                EncryptedCourseID = Utils.Encrypt(f.CourseID),
                                CourseURL = ResolveUrl(page + Utils.Encrypt(f.CourseID)),
                                f.CourseID,
                                f.CourseName,
                                f.Topics,
                                f.Replies,
                                f.Views,
                                f.LastActivityDate
                            }).ToList();
        gridCourseForum.DataSource = courseForum;

        ScriptManager.RegisterStartupScript(Page, typeof(Page), "ConvertTimeToLocal", Utils.callClientScript("ConvertTimeToLocal"), true);
        
    }
}