﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AdminForumAddNewTopic.ascx.cs"
    Inherits="UserControl_Pages_AdminForumAddNewTopic" %>
<rad:RadScriptBlock ID="rsbAddTopic" runat="server">
    <script type="text/javascript">
        function fnVerifyTopic() {
            console.log($("#<%= txtTopicTitle.ClientID %>").val());
            var RadConfirm = $("#<%=hdnRadConfirm.ClientID%>").val();
            var RadMessage = $("#<%=hdnRadMessage.ClientID%>").val();


            if ($("#<%= txtTopicTitle.ClientID %>").val() != "")
                radconfirm(RadMessage, confirmAddTopic, 330, 180, "", RadConfirm);
            else
                $("#<%= txtTopicTitle.ClientID %>").focus();
        }

        function confirmAddTopic(arg) {
            if (arg) {
                showLoadingPage();
                $("#<%= btnAddTopic.ClientID %>").click();
            } else {
                return false;
            }
        }

        function callBackOkfn(arg) {
            if (arg) {
                var courseId = '<%= Request.QueryString["CourseID"].ToString() %>';
                var page = '<%= Request.QueryString["Tab"].ToString() %>' == "TrainerForum" ? '/Trainer.aspx?Tab=TrainerForum&CourseID=' : '/Admin.aspx?Tab=AdminForum&CourseID=';              
                windows.location.href(page + courseId);
            } else {
                return false;
            }
        }

        function showLoadingPage() {
            var panel = $find("<%= radLoading.ClientID %>");
            panel.show();
        }

        function successMessage() {
            alert("Successfully Added a new Topic!");
        }

        function goBack() {
            window.history.back();
        }
     
      
    </script>
</rad:RadScriptBlock>
<rad:RadWindowManager ID="rwmForumAddTopic" RenderMode="Lightweight" EnableShadow="true"
    VisibleOnPageLoad="false" Behaviors="Close, Move" DestroyOnClose="true" Modal="true"
    Opacity="99" runat="server" VisibleStatusbar="false" Skin="Bootstrap">
</rad:RadWindowManager>

<asp:Panel ID="addTopicPanel" CssClass="row new-topic-container" runat="server">
<asp:HiddenField runat="server" ID="hdnRadMessage" Value="<%$ Resources:LocalizedResource, AreYouSureYouAddThisTopic %>" />
<asp:HiddenField runat="server" ID="hdnRadConfirm" Value="<%$ Resources:LocalizedResource, ConfirmAdd %>" />
    <h4 class="title">
        <asp:Label runat="server" ID="TopicTitle" Text="<%$ Resources:LocalizedResource, AddNewTopic %>"></asp:Label>
    </h4>
    <asp:TextBox ID="txtTopicTitle" runat="server" CssClass="form-control" placeholder="<%$ Resources:LocalizedResource, TypeTopic %>"
        ClientIDMode="Static">
    </asp:TextBox>
    <asp:RequiredFieldValidator ID="rfvTitle" runat="server" ControlToValidate="txtTopicTitle"
        ErrorMessage="*" ForeColor="Red" Display="Dynamic" ValidationGroup="validateAddTopic"></asp:RequiredFieldValidator>
    <div class="row display-none">
        <div class="col-md-12 display-inline-flex input-container">
            <asp:TextBox ID="txtParticipantCIM" runat="server" CssClass="form-control" placeholder="<%$ Resources:LocalizedResource, TypeCim %>"
                Width="200px" />
            <asp:Button ID="btnAddtoList" Text="<%$ Resources:LocalizedResource, AddToList %>" CssClass=" btn btn-sm btn-teal btn-flat" runat="server" />
        </div>
    </div>
    <div class="row display-none">
        <div class="col-md-12" style="padding: 25px 15px;">
            <p class="bold">
                <asp:Literal Text="<%$ Resources:LocalizedResource, ParticipantList %>" runat="server" />
            </p>
            <rad:RadListBox ID="listParticipants" runat="server" CheckBoxes="true" RenderMode="Lightweight"
                Skin="Bootstrap" AllowTransfer="false" Height="100px" Width="200px">
                <%-- <ItemTemplate>
                    <asp:TextBox runat="server" />
                    <asp:Button Text="Invite" CssClass="btn btn-sm btn-teal" runat="server" />
                    <i class="fa fa-times" style="color:Red;"></i>
                </ItemTemplate>--%>
                <EmptyMessageTemplate>
                    No Participants Added.
                </EmptyMessageTemplate>
            </rad:RadListBox>
        </div>
    </div>
    <div class="row">
        <div class="col-md-1">
            <button id="btnFakeAddTopic" class="btn btn-sm btn-teal btn-flat "
                onclick="fnVerifyTopic();"><asp:label runat="server" ID="lblFakeAddTopic" Text="<%$ Resources:LocalizedResource, AddTopic %>"></asp:label></button>
          <%--  <asp:Button ID="btnFakeAddTopic" Text="Add Topic" CssClass="btn btn-sm btn-teal btn-flat "
                OnClientClick="fnVerifyTopic(); return false;" runat="server" />--%>
            <asp:Button type="submit" ID="btnAddTopic" Text="<%$ Resources:LocalizedResource, AddTopic %>" CssClass="display-none"
                OnClick="btnAddTopic_Click" runat="server" ValidationGroup="validateAddTopic" />
          
        </div>
        <div class="col-md-1">
          <asp:Button ID="btnCancel" Text="<%$ Resources:LocalizedResource, Cancel %>" CssClass="btnbtn btn-sm btn-teal btn-flat"
                OnClientClick="goBack(); return false;" runat="server" />
        </div>
    </div>
</asp:Panel>
<rad:RadAjaxLoadingPanel ID="radLoading" runat="server"  CssClass="Loading"></rad:RadAjaxLoadingPanel>
<rad:RadAjaxManagerProxy ID="radmanagerproxy1" runat="server" >
<AjaxSettings>
         
        </AjaxSettings>
    </rad:RadAjaxManagerProxy>
