﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using TranscomUniversityV3Model;

public partial class UserControl_Pages_AdminForumAddNewTopic : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnAddTopic_Click(object sender, EventArgs e) 
    {

        //if(Page.IsValid)
        //{
            var db = new TranscomUniversityV3ModelContainer();

            string threadTitle;
            int courseID;
            int authorCIM;
        
            threadTitle = txtTopicTitle.Text.Trim();
            string encCourseID = Request.QueryString["CourseID"].ToString();
            courseID = Convert.ToInt32(Utils.Decrypt(encCourseID));
            authorCIM = Convert.ToInt32(DataHelper.GetCurrentUserCIM());

            var ds = db.tbl_TranscomUniversity_Cor_Forum
                    .Where(p => p.ThreadTitle == threadTitle).ToList();

            if(ds.Count <= 0){
            pr_TranscomUniversity_AddForumTopic_Result res = db.pr_TranscomUniversity_AddForumTopic(threadTitle, courseID, authorCIM).SingleOrDefault();

            //if (res.Response == 1)
            //{
                string page = Request.QueryString["Tab"] == "TrainerForum" ? "~/Trainer.aspx?Tab=TrainerForum&CourseID=" : "~/Admin.aspx?Tab=AdminForum&CourseID=";
                //rwmForumAddTopic.RadAlert("Added Successfully.", 330, 180, "Success", "callBackOkfn");
                //ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", Utils.callClientScript("showPageLoading"), true);
                //
                //fullPageLoading.Visible = true;
                //this.NamingContainer.FindControl("forumAddTopic").Visible = false;
                //this.NamingContainer.FindControl("forumTopics").Visible = true;

                UserControl forumAddTopic = (UserControl)this.NamingContainer.FindControl("forumAddTopic");
                UserControl forumTopics = (UserControl)this.NamingContainer.FindControl("forumTopics");

                RadGrid rgCourseThread = this.NamingContainer.FindControl("forumTopics").FindControl("gridCourseThread") as RadGrid;


           
                var courseId = Request.QueryString["CourseID"] != null ? Request.QueryString["CourseID"].ToString() : "";            
                var courseForum = db.vw_TranscomUniversity_CourseForumThreads
                                    .AsEnumerable()
                                    .Select(f => new
                                    {
                                        ThreadURL = page + courseId + "&ThreadID=" + Utils.Encrypt(f.ThreadID),
                                        f.ThreadID,
                                        f.AuthorName,
                                        f.ThreadTitle,
                                        f.DateCreated,
                                        f.LastPostBy,
                                        f.LastPostTime,
                                        f.PosterImage,
                                        f.Replies,
                                        f.Views,
                                        f.CourseID
                                    })
                                    .Where(f => f.CourseID == Convert.ToInt32(Utils.Decrypt(courseId)))
                                    .ToList();
                rgCourseThread.DataSource = courseForum;
                rgCourseThread.DataBind();

                forumAddTopic.Visible = false;
                forumTopics.Visible = true;
            //ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", Utils.callClientScript("goBack"), true);      


            //Response.Redirect(page + encCourseID);
            }
            else
            {
                rwmForumAddTopic.RadAlert(Resources.LocalizedResource.TopicAlreadyExistsTryanotherone.ToString(), 330, 180, Resources.LocalizedResource.DuplicateTopic.ToString(), "");
            }

        }
        
    //}

  
    protected void btnCancel_Click(object sender, EventArgs e)
    { 
    
    }
}