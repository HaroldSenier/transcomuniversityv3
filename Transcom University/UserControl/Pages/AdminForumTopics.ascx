﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AdminForumTopics.ascx.cs"
    Inherits="UserControl_Pages_AdminForumTopics" %>
<rad:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        function gotoAddNewTopic() {
            var courseId = '<%= Request.QueryString["CourseID"].ToString() %>';
            var page = "";
            if ($(location).attr('hostname') == "localhost")
                page = '<%= Request.QueryString["Tab"].ToString() %>' == "TrainerForum" ? '/Trainer.aspx?Tab=TrainerForum&CourseID=' : '/Admin.aspx?Tab=AdminForum&CourseID=';
            else
                page = '<%= Request.QueryString["Tab"].ToString() %>' == "TrainerForum" ? '/TranscomUniversityV3/Trainer.aspx?Tab=TrainerForum&CourseID=' : '/TranscomUniversityV3/Admin.aspx?Tab=AdminForum&CourseID=';



            window.location = page + courseId + '&forumaction=AddNewTopic';
        }

    </script>
    <script type="text/javascript">
        function fnVerifyTopic() {
            console.log($("#<%= txtTopicTitle.ClientID %>").val());

            if ($("#<%= txtTopicTitle.ClientID %>").val() != "") {
                var hdnradconfirmmessage = $("#<%=hdnradconfirmmessage.ClientID%>").val();
                var hdnradaddconfirm = $("#<%=hdnradaddconfirm.ClientID%>").val();
                radconfirm(hdnradconfirmmessage, confirmAddTopic, 330, 180, "", hdnradaddconfirm);
            } else {
                $("#<%= txtTopicTitle.ClientID %>").focus();
                var hdnradalertinputtopicheader = $("#<%=hdnradalertinputtopicheader.ClientID%>").val();
                var hdnradalertinputtopicconfirm = $("#<%=hdnradalertinputtopicconfirm.ClientID%>").val();
                radalert(hdnradalertinputtopicheader, 330, 180, hdnradalertinputtopicconfirm, "");
            }
        }

        function confirmAddTopic(arg) {
            if (arg) {
                $("#<%= btnAddTopic.ClientID %>").click();
            } else {
                return false;
            }
        }

        function callBackOkfn(arg) {
            if (arg) {
                var courseId = '<%= Request.QueryString["CourseID"].ToString() %>';
                var page = '<%= Request.QueryString["Tab"].ToString() %>' == "TrainerForum" ? '/Trainer.aspx?Tab=TrainerForum&CourseID=' : '/Admin.aspx?Tab=AdminForum&CourseID=';
                windows.location.href(page + courseId);
            } else {
                return false;
            }
        }


        function successMessage() {
            var hndradsuccessaddtopic = $("#<%=hndradsuccessaddtopic.ClientID%>").val();
            alert(hndradsuccessaddtopic);
        }

        function goBack() {
            window.history.back();
        }

        function AddNewTopic() {
            $("#txtTopicTitle").val("");
            var radwindow = $find('<%=rwAddnew.ClientID %>');
            radwindow.show();
            return false;
        }

        function closeAddNewTopic() {
            var radwindow = $find('<%=rwAddnew.ClientID %>');
            radwindow.close();
            return false;
        }

        function topicsLocalizedTime() {
            var ctDateCreated = $(".convertTimeZone");
                $.each(ctDateCreated, function (idx, el) {
                    var txtDateCreated = $(el).text();

                    var txtThreadDate = convertTimeZone(txtDateCreated);

                    $(el).text(moment(txtThreadDate).format("MM/DD/YYYY hh:mm A"));
                });

                var convertTimeAt = $(".convertTimeAt");

                $.each(convertTimeAt, function (idx, el) {
                    var st = $(el).text().trim();
                    if (st != "") {
                        var hr = st.substring(st.length - 7, st.length - 5).trim();
                        var mm = st.substring(st.length - 4, st.length - 2).trim();
                        var tt = st.substring(st.length - 2, st.length).trim();
                        var nDate = new Date("2019-1-1 " + hr + ":" + mm + " " + tt);
                        var txtThreadDate = convertTimeZone(nDate);

                        $(el).text(st.substring(0, st.length - 7) + moment(txtThreadDate).format("hh:mm A"));
                    }
                
                });
            }
      
    </script>
</rad:RadScriptBlock>
<rad:RadAjaxManagerProxy ID="RadAjaxManager1" runat="server">
    <AjaxSettings>
        <rad:AjaxSetting AjaxControlID="gridCourseThread">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="gridCourseThread" UpdatePanelRenderMode="Inline"
                    LoadingPanelID="RadAjaxLoadingPanel2" />
            </UpdatedControls>
        </rad:AjaxSetting>
    </AjaxSettings>
</rad:RadAjaxManagerProxy>
<div class="forum-main-container">
    <div class="container title-container">
        <h4 class="title bold pull-left" runat="server" id="lblCourseTitle">
             <asp:Label runat="server" ID="ctile1" Text= "<%$ Resources:LocalizedResource, CourseTitle %>"></asp:Label>
        </h4>
        <%--<asp:LinkButton ID="btnTopicNew" runat="server" CssClass="btn btn-sm btn-teal pull-right"
            Onclick="btnTopicNew_OnClick">--%>
        <button id="btnTopicNew" class="btn btn-sm btn-teal pull-right" onclick="AddNewTopic(); return false;">
            <i class="fa fa-plus" aria-hidden="true"></i><asp:Label runat="server" ID="Label4" Text= "New Topic"></asp:Label>
        </button>
    </div>
    <rad:RadGrid ID="gridCourseThread" runat="server" RenderMode="Lightweight" Skin="Bootstrap"
        AllowPaging="true" AllowSorting="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
        OnNeedDataSource="gridCrouseThread_OnNeedDatasource" AlternatingItemStyle-HorizontalAlign="Center"
        AutoGenerateColumns="false" HeaderStyle-Font-Bold="true" PageSize="10">
        <ClientSettings EnableAlternatingItems="false">
        </ClientSettings>
        <MasterTableView DataKeyNames="courseID">
            <Columns>
                <rad:GridTemplateColumn UniqueName="ThreadName" HeaderText="Threads in this course"
                    HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" SortExpression="ThreadName">
                    <ItemTemplate>
                        <div class="row">
                            <div class="col-md-1" style="text-align: center;">
                                <i class="fa fa-comments"></i>
                            </div>
                            <div class="col-md-11">
                                <%--<asp:HyperLink ID="lnkThread" runat="server" NavigateUrl='<%# Eval("ThreadURL") %>'
                                    Text='<%# Eval("ThreadTitle") %>'>
                                </asp:HyperLink>--%>
                                <asp:LinkButton runat="server" CommandArgument='<%# Eval("ThreadID") %>' OnCommand="lbtnThread_Click">
                                    <%# Eval("ThreadTitle") %>
                                </asp:LinkButton>
                                <br />
                                <asp:Label Text='<%# Eval("AuthorName") %>' runat="server" />
                                <asp:Label ID="lblDateCreated" Text='<%# Eval("DateCreated") %>' runat="server" CssClass="convertTimeZone" />
                            </div>
                        </div>
                    </ItemTemplate>
                </rad:GridTemplateColumn>
                <rad:GridTemplateColumn UniqueName="LastPostBy" HeaderText="Last post by" HeaderStyle-HorizontalAlign="Center"
                    ItemStyle-HorizontalAlign="Left" SortExpression="LastPostBy">
                    <ItemTemplate>
                        <div class="row">
                            <div class="col-md-2">
                                <asp:Image ID="uImg" runat="server" ImageUrl='<%# Eval("PosterImage", "{0}") %>'
                                    CssClass="border-teal" AlternateText="User Image" Width="50px" />
                            </div>
                            <div class="col-md-10">
                                <asp:Label ID="Label3" Text='<%# Eval("LastPostBy") %>' runat="server" />
                                <br />
                                <asp:Label ID="lblLastPostTime" Text='<%# Eval("LastPostTime") %>' runat="server" CssClass="convertTimeAt" />
                            </div>
                        </div>
                    </ItemTemplate>
                </rad:GridTemplateColumn>
                <rad:GridBoundColumn UniqueName="Replies" DataField="Replies" HeaderText="Replies">
                </rad:GridBoundColumn>
                <rad:GridBoundColumn UniqueName="Views" DataField="Views" HeaderText="<%$ Resources:LocalizedResource, Views %>">
                </rad:GridBoundColumn>
            </Columns>
        </MasterTableView>
    </rad:RadGrid>
</div>
<rad:RadWindowManager ID="rwmForum" RenderMode="Lightweight" EnableShadow="true"
    Modal="true" VisibleOnPageLoad="false" Behaviors="Close, Move" Opacity="99" runat="server"
    VisibleStatusbar="false" Skin="Bootstrap">
    <Windows>
        <rad:RadWindow RenderMode="Lightweight" ID="rwAddnew" Behaviors="Close" VisibleOnPageLoad="false"
            runat="server" CssClass="tc-radwindow-1 height-inherit rdGroupList" Title="Add New Topic"
            Width="800" Height="200px">
            <ContentTemplate>
                <asp:Panel ID="addTopicPanel" CssClass="container no-padding" runat="server">
                  <%--  <h4 class="title">
                        Add New Topic
                    </h4>--%>
                    <asp:TextBox ID="txtTopicTitle" runat="server" CssClass="form-control width-fill" placeholder="Type topic..."
                        ClientIDMode="Static" MaxLength="255">
                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvTitle" runat="server" ControlToValidate="txtTopicTitle"
                        ErrorMessage="*" ForeColor="Red" Display="Dynamic" ValidationGroup="validateAddTopic"></asp:RequiredFieldValidator>
                    <div class="row display-none">
                        <div class="col-md-12 display-inline-flex input-container">
                            <asp:TextBox ID="txtParticipantCIM" runat="server" CssClass="form-control" placeholder="-Type CIM-"
                                Width="200px" />
                            <asp:Button ID="Button1" Text="Add to list" CssClass=" btn btn-sm btn-teal btn-flat"
                                runat="server" />
                        </div>
                    </div>
                    <div class="row display-none">
                        <div class="col-md-12" style="padding: 25px 15px;">
                            <p class="bold">
                                <asp:Literal ID="Literal1" Text="Participant List" runat="server" />
                            </p>
                            <rad:RadListBox ID="listParticipants" runat="server" CheckBoxes="true" RenderMode="Lightweight"
                                Skin="Bootstrap" AllowTransfer="false" Height="100px" Width="200px">
                                <%-- <ItemTemplate>
                    <asp:TextBox runat="server" />
                    <asp:Button Text="Invite" CssClass="btn btn-sm btn-teal" runat="server" />
                    <i class="fa fa-times" style="color:Red;"></i>
                </ItemTemplate>--%>
                                <EmptyMessageTemplate>
                                    No Participants Added.
                                </EmptyMessageTemplate>
                            </rad:RadListBox>
                        </div>
                    </div>
                    <div class="container no-padding">
                        <br />
                        <div class="col-md-1 no-padding">
                            <asp:Button ID="btnFakeAddTopic" Text="Add Topic" CssClass="btn btn-sm btn-teal btn-flat "
                                OnClientClick="fnVerifyTopic(); return false;" runat="server" />
                            <asp:Button type="submit" ID="btnAddTopic" Text="Add Topic" CssClass="display-none"
                                OnClick="btnAddTopic_Click" runat="server" ValidationGroup="validateAddTopic" />
                        </div>
                        <div class="col-md-1">
                            <asp:Button ID="btnCancel" Text="<%$ Resources:LocalizedResource, Cancel %>" CssClass="btn btn-sm btn-teal btn-flat"
                                OnClientClick="closeAddNewTopic(); return false;" runat="server" />
                        </div>
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </rad:RadWindow>
    </Windows>
</rad:RadWindowManager>
<asp:HiddenField runat="server" ID ="hdnradalertinputtopicheader" Value="Please Input a Topic." /> 
<asp:HiddenField runat="server" ID ="hdnradalertinputtopicconfirm" Value="Error" /> 
<asp:HiddenField runat="server" ID ="hdnradconfirmmessage" Value="Are you sure you want to add this topic?" /> 
<asp:HiddenField runat="server" ID ="hdnradaddconfirm" Value="Confirm Add" /> 
<asp:HiddenField runat="server" ID ="hndradsuccessaddtopic" Value="Successfully Added a new Topic!" /> 

