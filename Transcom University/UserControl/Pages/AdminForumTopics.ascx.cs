﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TranscomUniversityV3Model;
using Telerik.Web.UI;

public partial class UserControl_Pages_AdminForumTopics : System.Web.UI.UserControl
{
    string courseId;
    string page;

    protected void Page_Load(object sender, EventArgs e)
    {
        page = Request.QueryString["Tab"] == "TrainerForum" ? "~/Trainer.aspx?Tab=TrainerForum&CourseID=" : "~/Admin.aspx?Tab=AdminForum&CourseID=";
        if (!Page.IsPostBack && Request.QueryString["CourseID"] != null)
        {
            courseId = Request.QueryString["CourseID"] != null ? Request.QueryString["CourseID"].ToString() : "";
            lblCourseTitle.InnerText = DataHelper.getCourseTitleByID(Convert.ToInt32(Utils.Decrypt(courseId)));
            gridCourseThread.DataBind();
            
        }
    }

    //protected void btnAddTopic_Click(object sender, EventArgs e)
    //{
    //    Response.Redirect(page + courseId + "&forumaction=AddNewTopic");
    //}

    protected void gridCrouseThread_OnNeedDatasource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        LoadData();
        ScriptManager.RegisterStartupScript(Page, typeof(Page), "topicsLocalizedTime", Utils.callClientScript("topicsLocalizedTime"), true);
    }

    protected void LoadData()
    {
        if (Request.QueryString["CourseID"] != null)
        {
            courseId = Request.QueryString["CourseID"] != null ? Request.QueryString["CourseID"].ToString() : "";
            var db = new TranscomUniversityV3ModelContainer();
            var courseForum = db.vw_TranscomUniversity_CourseForumThreads
                                .AsEnumerable()
                                .Select(f => new
                                {
                                    ThreadURL = page + courseId + "&ThreadID=" + Utils.Encrypt(f.ThreadID),
                                    f.ThreadID,
                                    f.AuthorName,
                                    f.ThreadTitle,
                                    f.DateCreated,
                                    f.LastPostBy,
                                    f.LastPostTime,
                                    f.PosterImage,
                                    f.Replies,
                                    f.Views,
                                    f.CourseID
                                })
                                .Where(f => f.CourseID == Convert.ToInt32(Utils.Decrypt(courseId)))
                                .ToList();
            gridCourseThread.DataSource = courseForum;
        }
    }

    protected void lbtnThread_Click(object sender, CommandEventArgs e)
    {
        string cmd = e.CommandArgument.ToString();
        int courseID = Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"].ToString()));
        int threadID = Convert.ToInt32(e.CommandArgument.ToString());
        string str_courseID = Request.QueryString["CourseID"].ToString();
        string str_threadID = Utils.Encrypt(threadID);

        int enrolleeCIM = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
        var db = new TranscomUniversityV3ModelContainer();
        db.pr_TranscomUniversity_ForumLogin(enrolleeCIM, threadID);

        Response.Redirect(page + str_courseID + "&ThreadID=" + str_threadID);

    }

    protected void btnAddTopic_Click(object sender, EventArgs e)
    {

        //if(Page.IsValid)
        //{
        var db = new TranscomUniversityV3ModelContainer();

        string threadTitle;
        int courseID;
        int authorCIM;

        threadTitle = txtTopicTitle.Text.Trim();
        string encCourseID = Request.QueryString["CourseID"].ToString();
        courseID = Convert.ToInt32(Utils.Decrypt(encCourseID));
        authorCIM = Convert.ToInt32(DataHelper.GetCurrentUserCIM());

        var ds = db.tbl_TranscomUniversity_Cor_Forum
                .Where(p => p.ThreadTitle == threadTitle && p.CourseID == courseID).ToList();

        if (ds.Count <= 0)
        {
            pr_TranscomUniversity_AddForumTopic_Result res = db.pr_TranscomUniversity_AddForumTopic(threadTitle, courseID, authorCIM).SingleOrDefault();

            //if (res.Response == 1)
            //{
            string page = Request.QueryString["Tab"] == "TrainerForum" ? "~/Trainer.aspx?Tab=TrainerForum&CourseID=" : "~/Admin.aspx?Tab=AdminForum&CourseID=";
            //rwmForumAddTopic.RadAlert("Added Successfully.", 330, 180, "Success", "callBackOkfn");
            //ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", Utils.callClientScript("showPageLoading"), true);
            //
            //fullPageLoading.Visible = true;
            //this.NamingContainer.FindControl("forumAddTopic").Visible = false;
            //this.NamingContainer.FindControl("forumTopics").Visible = true;

            //UserControl forumAddTopic = (UserControl)this.NamingContainer.FindControl("forumAddTopic");
            //UserControl forumTopics = (UserControl)this.NamingContainer.FindControl("forumTopics");

            //RadGrid rgCourseThread = this.NamingContainer.FindControl("forumTopics").FindControl("gridCourseThread") as RadGrid;

            LoadData();
            gridCourseThread.DataBind();
           
            txtTopicTitle.Text = string.Empty;
            rwmForum.RadAlert("Adding Topic Successfully!", 330, 180, "Success Message", "");

            ScriptManager.RegisterStartupScript(Page, typeof(Page), "topicsLocalizedTime1", Utils.callClientScript("topicsLocalizedTime"), true);
            //rgCourseThread.DataBind();

            //forumAddTopic.Visible = false;
            //forumTopics.Visible = true;
            //ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", Utils.callClientScript("goBack"), true);      


            //Response.Redirect(page + encCourseID);
        }
        else
        {
            rwmForum.RadAlert("Topic Already Exists. Try another one.", 330, 180, "Duplicate Topic", "AddNewTopic");
        }

        

    }

    protected void btnTopicNew_OnClick(object sender, EventArgs e) {
        //ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", Utils.callClientScript("AddNewTopic"), true);
    }
}