﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AdminReportDashboardUserCtrl.ascx.cs"
    Inherits="UserControl_Pages_AdminReportDashboardUserCtrl" %>
<rad:RadScriptBlock ID="rcbDashboard" runat="server">
    <script type="text/javascript">
        function tabClick(el) {

            $("i.js-collapse-event").removeClass("fa-sort-up").addClass("fa-sort-down");
        }
    </script>
</rad:RadScriptBlock>
<div class="container border-lightgray dashboard-main-container">
    <div class="header">
   
    <h5 class="pull-left">
        <asp:Label runat="server" ID="MDash" Text= "<%$ Resources:LocalizedResource, ManagerDashboard %>"></asp:Label>
    </h5>
    <asp:LinkButton ID="btnToExcel" runat="server">
            <i class="fa fa-file-excel-o pull-right convert-to" aria-hidden="true"></i>
    </asp:LinkButton>
    <asp:LinkButton ID="btnToPdf" runat="server">
            <i class="fa fa-file-pdf-o pull-right convert-to" aria-hidden="true"></i>
    </asp:LinkButton>
     </div>
    <div class="col-md-12 no-paddings" style="border-top: 2px solid lightgray;">
        <div class="button-pager display-inline-flex">
            <asp:Button ID="btn1" Text="1" runat="server" CssClass="btn btn-md btn-teal" />
            <asp:Button ID="btn2" Text="2" runat="server" CssClass="btn btn-md btn-darkgray" />
            <asp:Button ID="btn3" Text="3" runat="server" CssClass="btn btn-md btn-darkgray" />
            <asp:Button ID="btn4" Text="4" runat="server" CssClass="btn btn-md btn-darkgray" />
            <asp:Button ID="btn5" Text="5" runat="server" CssClass="btn btn-md btn-darkgray" />
        </div>
    </div>
    <div class="col-md-12 no-paddings">
        <div class="panel-group" id="reportsAccodion">
            <div class="panel panel-default">
                <div class="panel-heading panel-heading-black display-flex">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" class="js-collapse-event no-underline-hover" data-parent="#reportsAccodion"
                            href="#tab1" onclick="tabClick(this);"><asp:Label runat="server" ID="Label1" Text= "<%$ Resources:LocalizedResource, Category %>"></asp:Label></a> <i class="fa fa-sort-down pull-right hover-pointer js-collapse-event"
                                data-toggle="collapse" data-parent="#reportsAccodion" href="#tab1" onclick="tabClick(this);">
                            </i>
                    </h4>
                </div>
                <div id="tab1" class="panel-collapse collapse">
                    <div class="panel-body">
                        <asp:Label runat="server" ID="ctile1" Text= "<%$ Resources:LocalizedResource, Content %>"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
