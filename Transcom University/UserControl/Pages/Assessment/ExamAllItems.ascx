﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ExamAllItems.ascx.cs"
    Inherits="UserControl_Pages_Assessment_ExamAllItems" %>
<%@ Register TagPrefix="wus" TagName="examquestioncontrol" Src="~/UserControl/Pages/Assessment/examquestioncontrol.ascx" %>
<rad:RadScriptBlock ID="rsb" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            sidebarFull();
        })
    </script>
</rad:RadScriptBlock>
<%--<rad:RadAjaxManagerProxy ID="ramp1" runat="server">
    <AjaxSettings>
        <rad:AjaxSetting AjaxControlID="btnContinue">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="btnContinue" />
            </UpdatedControls>
        </rad:AjaxSetting>
    </AjaxSettings>
</rad:RadAjaxManagerProxy>--%>
<asp:Panel ID="pnlTimer" runat="server" HorizontalAlign="left" BorderWidth="3px"
    Font-Size="12px" Width="200px" CssClass="" Style="width: 200px; z-index: 2; right: 7vw;
    position: fixed; top: 30vh;">
    <asp:UpdatePanel ID="udpTimer" runat="server" UpdateMode="conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="tmrTimeRemaining" EventName="Tick" />
        </Triggers>
        <ContentTemplate>
            <table width="100%" class="">
                <tr>
                    <td>
                        <asp:Label ID="lblTimeRemaining" runat="server" Text="Time Remaining:" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblActualTimeLeft" runat="server" Visible="false" />
                        <asp:Label ID="lblTimeRemainingValue" runat="server" />
                        <asp:Timer ID="tmrTimeRemaining" runat="server" Interval="1000" Enabled="false" OnTick="tmrTimeRemaining_Tick" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<asp:Panel runat="server" ID="pnlAllItems" CssClass="col-md-12 exam-container borderless">
    <table width="100%" cellspacing="0">
        <tr>
            <td colspan="3">
                <br />
                <asp:Label ID="lblNote" runat="server" Text="" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:GridView ID="gvQuestions" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                    AllowSorting="false" Width="100%">
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <table width="100%" cellspacing="0" class="table border-lightgrey">
                                    <tr>
                                        <td style="width: 100%">
                                            <asp:Label ID="lblGridQuestionnaireID" runat="server" Text='<%#Bind("QuestionnaireID") %> '
                                                Visible="false" />
                                            <asp:Label ID="lblGridTestCategoryID" runat="server" Text='<%#Bind("TestCategoryID") %>'
                                                Visible="false" />
                                            <wus:examquestioncontrol ID="wusQuestion" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <%--<tr>
            <td colspan="3" align="center">
                <asp:UpdateProgress ID="udpProgressLeft" runat="server">
                    <ProgressTemplate>
                        <asp:Panel ID="pnlLeft" runat="server" Width="20px">
                            <table width="100%">
                                <tr>
                                    <td align="center" valign="middle">
                                        <asp:Image ID="imgLeft" runat="server" ImageUrl="~/images/ajax-loader-small-2.gif" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </td>
        </tr>--%>
        <tr>
            <td style="width: 15%">
                <asp:Label ID="lblOnce" runat="server" Text="<%$ Resources:LocalizedResource, Pleaseclickthebuttonsonlyonce%>."
                    CssClass="smallerText" />
            </td>
            <td align="left" style="width: 70%">
                <asp:Button ID="btnSaveAndQuit" runat="server" Text="Save and Resume Later" OnClick="btnSaveAndQuit_Click"
                    CssClass="btn tc-btn-md btn-teal btn-flat" Width="180px" UseSubmitBehavior="false" />
                <asp:Button ID="btnContinue" runat="server" Text="<%$ Resources:LocalizedResource, ImFinished%>" OnClick="btnSubmit_Click"
                    Width="180px" CssClass="btn tc-btn-md btn-teal btn-flat" />
                <asp:Button ID="Button1" runat="server" Text="<%$ Resources:LocalizedResource, ImFinished2%>" Width="180px" CssClass="btn tc-btn-md btn-teal btn-flat" />
                <button>Test</button>
            </td>
            <td align="right" style="width: 15%">
            </td>
        </tr>
    </table>
    <ajax:ModalPopupExtender ID="mpeNoReplacement" runat="server" BackgroundCssClass="modalBackground"
        PopupControlID="pnlNoReplacement" TargetControlID="hidNoReplacement" />
    <asp:HiddenField ID="hidNoReplacement" runat="server" />
    <asp:Panel ID="pnlNoReplacement" runat="server" CssClass="modalPopup bg-color-light-gray"
        Width="300px">
        <table cellpadding="10" cellspacing="0" style="vertical-align: middle; font-size: large;"
            width="100%">
            <tr>
                <td align="left">
                    <asp:Label ID="lblNoReplacement" runat="server" Text="<%$ Resources:LocalizedResource, Youarenotallowedtochangeyouranswer%>."
                        Font-Size="12px" />
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Button ID="btnNoReplacement" runat="server" Text="<%$ Resources:LocalizedResource, Next%>" OnClick="btnNoReplacement_Click"
                        Width="80" CssClass="buttons" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajax:ModalPopupExtender ID="mpeWarning" runat="server" BackgroundCssClass="modalBackground"
        TargetControlID="hidWarning" PopupControlID="pnlWarning" />
    <asp:HiddenField ID="hidWarning" runat="server" />
    <asp:Panel ID="pnlWarning" runat="server" CssClass="modalPopup bg-color-light-gray"
        HorizontalAlign="left" Width="300px">
        <table cellpadding="10" cellspacing="0" style="vertical-align: middle; font-size: large;"
            width="100%">
            <tr>
                <td>
                    <asp:Label ID="lblWarning" runat="server" Font-Size="12px" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnWarning" runat="server" Text="<%$ Resources:LocalizedResource, Return%>" CssClass="buttons" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <%-- <ajax:AlwaysVisibleControlExtender ID="avcTimeRemaining" runat="server" TargetControlID="pnlTimer"
        VerticalSide="Top" HorizontalSide="Right" VerticalOffset="10" HorizontalOffset="10"
        ScrollEffectDuration=".1" />--%>
    <ajax:ModalPopupExtender ID="mpePnlTimeUp" runat="server" BackgroundCssClass="modalBackground"
        PopupControlID="pnlTimeUp" TargetControlID="hidTimeUp" />
    <asp:HiddenField ID="hidTimeUp" runat="server" />
    <asp:Panel ID="pnlTimeUp" runat="server" HorizontalAlign="left" CssClass="modalPopup"
        Width="200px">
        <table width="100%" cellpadding="5">
            <tr>
                <td>
                    <asp:Label ID="lblTimeUp" runat="server" Text="Time's up!" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnContinuePopup" runat="server" Text="<%$ Resources:LocalizedResource, Continue %>" OnClick="btnSubmit_Click"
                        Width="80px" CssClass="buttons" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajax:ModalPopupExtender ID="mpeMissingAnswer" runat="server" BackgroundCssClass="modalBackground"
        PopupControlID="pnlMissingAnswer" TargetControlID="hidMissingAnswer" />
    <asp:HiddenField ID="hidMissingAnswer" runat="server" />
    <asp:Panel ID="pnlMissingAnswer" runat="server" HorizontalAlign="left" CssClass="modalPopup"
        Width="300px">
        <table width="100%" cellpadding="5">
            <tr>
                <td align="left">
                    <asp:Label runat="server" ID="lblPleasecheckthatyouhaveanswersforitemsinred" Text="<%$ Resources:LocalizedResource, Pleasecheckthatyouhaveanswersforitemsinred%>" ></asp:Label>
                    <br />
                    <asp:Button ID="btnMissingAnswerOK" runat="server" Text="<%$ Resources:LocalizedResource, Ok %>" CssClass="buttons" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Panel>
