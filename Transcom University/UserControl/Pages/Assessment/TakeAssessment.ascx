﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TakeAssessment.ascx.cs"
    Inherits="UserControl_Pages_Assessment_TakeAssessment" %>
<rad:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">

        var style = window.getComputedStyle(document.getElementById("BgCourse"), false),
        bi = style.backgroundImage.split(',')[0].slice(4, -1).replace(/"/g, "");
        document.getElementById("takeAssessmentCourseImage").setAttribute("src", bi);
        //        $('body,html').animate({
        //            scrollTop: 0
        //        }, 300);
        //debugger;
    </script>
</rad:RadScriptBlock>
<asp:Panel runat="server" ID="pnlAssessmentDetailContainer" CssClass="col-md-12">
    <div class="col-md-4 text-center">
        <div class="col-md-12">
            <image id="takeAssessmentCourseImage" src="#" alt="Course Image" width="350px" height="250px"
                onerror="this.onerror=null;this.src='Media/Uploads/CourseImg/No_image.jpg'" class="margin-bottom-10px"></image>
            <div class="col-md-6 pull-left">
                <asp:Button ID="btnBack" Text="<%$ Resources:LocalizedResource, Back %>" CssClass="btn tc-btn-md btn-teal btn-flat" runat="server"
                    OnClientClick="btnBackToCoursePreview_ClientClick(); return false;" />
            </div>
            <div class="col-md-6 pull-right">
                <asp:Button ID="btnTakeAssessment" Text="<%$ Resources:LocalizedResource, TakeAssessment %>" CssClass="btn tc-btn-md btn-teal btn-flat"
                    runat="server" />
            </div>
        </div>
    </div>
</asp:Panel>
<asp:Panel ID="pnlMain" runat="server">
    <table cellpadding="10" cellspacing="0" width="100%">
        <tr style="height: 80px; vertical-align: middle">
            <td align="center" style="vertical-align: middle; font-size: large; padding: 0px 10px 0px 10px">
                <asp:Label ID="lblExamName" runat="server" CssClass="testQuestion" />
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label ID="lblInstructions" runat="server" Text="<%$ Resources:LocalizedResource, Instructions %>" Font-Bold="true" />:
            </td>
        </tr>
        <tr>
            <td align="left" id="tdInstructions" runat="server">
                <asp:Label ID="lblInstructionValue" runat="server" Width="90%" />
            </td>
        </tr>
        <tr style="height: 30px">
            <td align="center" style="vertical-align: middle; font-size: large;">
                <table width="90%" cellpadding="5" style="vertical-align: middle; font-size: small;">
                    <tr>
                        <td align="center" style="padding: 10px 10px 10px 10px">
                            <asp:Label ID="lblAbout" runat="server" Text="<%$ Resources:LocalizedResource, ClickTheBeginButtonToStartTheExamsurvey %>" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr style="height: 20px">
            <td align="center">
                <asp:Label ID="lblDuration" runat="server" Text="<%$ Resources:LocalizedResource, Duration %>" />
                <asp:Label ID="lblDurationVal" runat="server" />
            </td>
        </tr>
        <tr style="height: 40px">
            <td align="center" valign="middle" style="">
                <asp:Button ID="btnContinue" runat="server" Text="<%$ Resources:LocalizedResource, Begin %>" OnClick="btnContinue_Click"
                    Width="80px" CssClass="buttons" />
            </td>
        </tr>
    </table>
</asp:Panel>
