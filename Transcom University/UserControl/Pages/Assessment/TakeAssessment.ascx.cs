﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;

public partial class UserControl_Pages_Assessment_TakeAssessment : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(SessionManager_Testing.SessionUsername))
            Response.Redirect("~/LandingPage.aspx?" + Request.QueryString.ToString());
        TestCategory category = TestCategory.Select(SessionManager_Testing.SessionTestCategoryID, true);
        if (category != null)
        {
            Questionnaire[] questionnaire = Questionnaire.SelectByCategory(SessionManager_Testing.SessionTestCategoryID);
            this.lblExamName.Text = category.TestName;
            if (category.TimeLimit == 0)
            {
                SessionManager_Testing.SessionTimedExam = false;
                this.lblDuration.Text = Resources.LocalizedResource.NoTimeLimit.ToString();
            }
            else
            {
                SessionManager_Testing.SessionTimedExam = true;
                int trueTime = category.TimeLimit / 60;
                this.lblDurationVal.Text = trueTime.ToString() + " " + Resources.LocalizedResource.minutes.ToString();
            }
            if (!string.IsNullOrEmpty(category.Instructions))
                this.lblInstructions.Text = category.Instructions.Replace(Environment.NewLine, "<br />");
            else
            {
                this.tdInstructions.Align = "center";
                this.lblInstructionValue.Text = Resources.LocalizedResource.None.ToString();
            }
        }
        else
            Response.Redirect("~/testhub.aspx?" + Request.QueryString.ToString());
    }

    protected void btnContinue_Click(object sender, EventArgs e)
    {
        TestCategory category = TestCategory.Select(SessionManager_Testing.SessionTestCategoryID, true);
        int tempQuestionNumber = 0;
        if (category.IsEssay)
            SessionManager_Testing.SessionIsEssay = true;
        else
            SessionManager_Testing.SessionIsEssay = false;
        if (SessionManager_Testing.SessionExamAction == "resumeexam")
        {
            foreach (Questionnaire question in SessionManager_Testing.SessionQuestionnaire)
            {
                SaveTestResponse response = SaveTestResponse.Retrieve(SessionManager_Testing.SessionUsername, SessionManager_Testing.SessionTestTaken.TestTakenID, question.QuestionnaireID);
                if (response != null)
                    tempQuestionNumber++;
            }
            SessionManager_Testing.SessionTestTaken.DateLastSave = DateTime.Now;
            SessionManager_Testing.SessionTestTaken.UpdateTime();
        }
        else if (SessionManager_Testing.SessionExamAction == "newexam")
        {
            TestTaken taken = new TestTaken(SessionManager_Testing.SessionTestCategoryID, SessionManager_Testing.SessionUsername, SessionManager_Testing.SessionCompanySite, DateTime.Now);
            taken.Office = SessionManager_Testing.SessionSite;
            int v = taken.Insert();
            SessionManager_Testing.SessionTestTaken = TestTaken.Select(v);
            SessionManager_Testing.SessionTestTaken.MinutesRemaining = category.TimeLimit;
            SessionManager_Testing.SessionTestTaken.DateLastSave = DateTime.Now;
            SessionManager_Testing.SessionTestTaken.UpdateTime();
            SessionManager_Testing.SessionExamAction = "resumeexam";
        }
        SessionManager_Testing.SessionQuestionNumber = tempQuestionNumber;
        Response.Redirect("~/allitems.aspx?" + Request.QueryString.ToString());
    }
}