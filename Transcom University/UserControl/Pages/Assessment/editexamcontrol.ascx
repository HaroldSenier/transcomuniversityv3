<%@ Control Language="C#" AutoEventWireup="true" CodeFile="editexamcontrol.ascx.cs"
    Inherits="UserControl_Pages_Assessment_editexamcontrol" %>

    <asp:HiddenField runat="server" ID="hfThereAreNoItemsForThisExamYetDoYouWishToContinueSaving" Value="<%$ Resources:LocalizedResource, ThereAreNoItemsForThisExamYetDoYouWishToContinueSaving %>" />
    <asp:HiddenField runat="server" ID="AreYouSureDeleteThisQuestion" Value="<%$ Resources:LocalizedResource, AreYouSureYouWantToDeleteThisQuestion %>" />
    <asp:HiddenField runat="server" ID="hfAreYouSureYouWantToDeleteThisQuestion" Value="<%$ Resources:LocalizedResource, AreYouSureYouWantToDeleteThisQuestion %>"  />
    <asp:HiddenField runat="server" ID="ConfirmCreate" Value="<%$ Resources:LocalizedResource, ConfirmCreate %>" />
    <asp:HiddenField runat="server" ID="ConfirmClose" Value="<%$ Resources:LocalizedResource, ConfirmClose %>" />
     <asp:HiddenField runat="server" ID="ConfirmAdd" Value="<%$ Resources:LocalizedResource, ConfirmAdd %>" />
     <asp:HiddenField runat="server" ID="hfConfirmDelete" Value="<%$ Resources:LocalizedResource, ConfirmDelete %>" />
     <asp:HiddenField runat="server" ID="hfConfirmEdit" Value="<%$ Resources:LocalizedResource, ConfirmAdd %>" />
     <asp:HiddenField runat="server" ID="AreYouSureAddQuestion" Value="<%$ Resources:LocalizedResource, AreYouSureYouWantToAddQuestionToThisItem %>" />
      <asp:HiddenField runat="server" ID="hfAreyousureyouwanttoAddthisItem" Value="<%$ Resources:LocalizedResource, AreyousureyouwanttoAddthisItem %>" />
      <asp:HiddenField runat="server" ID="hfAreyousureyouwanttoClosethisItem" Value="<%$ Resources:LocalizedResource, AreyousureyouwanttoClosethisItem %>" />
      <asp:HiddenField runat="server" ID="hfAreYouSureYouWantToCloseThis" Value="<%$ Resources:LocalizedResource, AreYouSureYouWantToCloseThis %>" />     
      <asp:HiddenField runat="server" ID="hfAreyousureyouwanttoAddQuestiontothis" Value="<%$ Resources:LocalizedResource, AreyousureyouwanttoAddQuestiontothis %>" />   
         <asp:HiddenField runat="server" ID="hfAreyousureyouwantto" Value="<%$ Resources:LocalizedResource, Areyousureyouwantto %>" />   
         
      

<rad:RadScriptBlock ID="rsb" runat="server">
    <script type="text/javascript">
        var dQuestion;
        var rIndex;
        var rcbType;
        var pGrade;
       
        function Confirm(element,proc) {
            var errorMessageDelete = $("#<%= hfAreYouSureYouWantToDeleteThisQuestion.ClientID %>").val();
            var errorMessageEdit = $("#<%= hfAreYouSureYouWantToDeleteThisQuestion.ClientID %>").val();
            var errorTitleEdit = $("#<%= hfConfirmEdit.ClientID %>").val();
            var errorTitleDelete = $("#<%= hfConfirmDelete.ClientID %>").val();
            var index = $(element).closest("tr").find("input:eq(1)").val();
            $("#<%= hidIndex.ClientID %>").val(index);
            //dQuestion = element;
            //rIndex = element.parentNode.parentNode.rowIndex;
            if(proc == "delete")
                radconfirm(errorMessageDelete, confirmDeleteFn, 330, 180, "", errorTitleDelete);
            else
                radconfirm(errorMessageEdit, confirmEditFn, 330, 180, "", "Confirm Edit");
        }

       // function ConfirmEdit(element) {
       //     var id = $(element).closest("tr").find("input:eq(0)").val();
       //     var index = $(element).closest("tr").find("input:eq(1)").val();
       //     console.log(id)
       //     $("#<%= hidEdit.ClientID %>").val(id);
       //     $("#<%= hidIndex.ClientID %>").val(index);
       //    
       //     dQuestion = element;
       //     rIndex = element.parentNode.parentNode.rowIndex;
       //     radconfirm("Are you sure you want to edit this question?", confirmEditFn, 330, 180, "", "Confirm Edit");
       // }


        function confirmDeleteFn(arg) {
            if (arg) {
                //debugger;
                $("#<%= btnConfirmDeleteQuestionItem.ClientID %>").click();

            } else {
                return false;
            }
        }

        function confirmEditFn(arg) {
            if (arg) {
                //debugger;
                //$("#<%= hidEdit.ClientID %>").val(rIndex);
                $("#<%= btnConfirmEditQuestionItem.ClientID %>").click();

            } else {
                return false;
            }
        }

        function confirmCreate() {
            rcbType = $find('<%=rcbCreateType.ClientID %>').get_selectedItem();
            var confirmCreate = $("#<%= ConfirmCreate.ClientID %>").val();
            var errorMessageAdd = $("#<%= hfAreyousureyouwanttoAddthisItem.ClientID %>").val();            
            
            if (rcbType != null) {                
                var procType = $("#<%= hidProcess.ClientID %>").val();
                radconfirm($("#<%= hfAreyousureyouwantto.ClientID %>").val() + procType + " this " + rcbType.get_text() + "?", confirmCreateFn, 330, 180, "", confirmCreate);
            }
            else {
                radconfirm(errorMessageAdd, confirmCreateFn, 330, 180, "", ConfirmCreate);
            }
         }

        function confirmCreateFn(arg) {
            if (arg) {
                showPageLoading();
                $("#<%= btnContinue.ClientID %>").click();
            } else {
                return false;
            }
        }

        function confirmReturn() {
            rcbType = $find('<%=rcbCreateType.ClientID %>').get_selectedItem();
            var ConfirmClose = $("#<%= ConfirmClose.ClientID %>").val();
            var errorMessageCloseThis = $("#<%= hfAreYouSureYouWantToCloseThis.ClientID %>").val();
            var errorMessageCloseThisItem = $("#<%= hfAreyousureyouwanttoClosethisItem.ClientID %>").val();    
            if (rcbType  != null) {
                radconfirm(errorMessageCloseThis  + rcbType.get_text() + "?", confirmReturnFn, 330, 180, "", ConfirmClose);
            }
            else {
                radconfirm( errorMessageCloseThisItem + "?", confirmReturnFn, 330, 180, "", ConfirmClose);
            }
        }

        function confirmReturnFn(arg) {
            if (arg) {
                showPageLoading();
                $("#<%= btnReturnToExamList.ClientID %>").click();
            } else {
                return false;
            }
        }

        function confirmAddQuestion() {
            rcbType = $find('<%=rcbCreateType.ClientID %>').get_selectedItem();
            var areyousureadd = $('#<%=AreYouSureAddQuestion.ClientID %>').val();
            var ConfirmAdd = $('#<%=ConfirmAdd.ClientID %>').val();
            var errorMessageAddQuestion = $('#<%=hfAreyousureyouwanttoAddQuestiontothis.ClientID %>').val();
            if (rcbType != null) {
                radconfirm(errorMessageAddQuestion + " " + rcbType.get_text() + "?", confirmAddQuestionFn, 330, 180, "", ConfirmAdd);
            }
            else
                radconfirm(areyousureadd, confirmAddQuestionFn, 330, 180, "", ConfirmAdd);
        }

        function confirmAddQuestionFn(arg) {
            if (arg) {
                showPageLoading();
                $("#<%= btnAddItem.ClientID %>").click();
            } else {
                return false;
            }
        }

        function rcbCreateType_OnClientSelectedIndexChanged(sender, eventArgs) {
            var item = eventArgs.get_item().get_text();
            var txtCommision = document.getElementById("<%= txtPassingGrade.ClientID %>");
            if (txtCommision.text != "")
                pGrade = txtCommision.text;

            if (item == "Survey") {
                txtCommision.disabled = true;
                txtCommision.text = "";
                }
            else {
                txtCommision.disabled = false;
                txtCommision.text = pGrade;
                }
        }
    </script>
</rad:RadScriptBlock>
<asp:Panel ID="pnlExam" runat="server" Width="100%" HorizontalAlign="center" Font-Size="12px">
    <%--  <asp:UpdatePanel ID="udpSub" runat="server">--%>
    <%-- <ContentTemplate>--%>
    <table cellpadding="10" width="100%" cellspacing="0" style="font-family: Arial">
        <tr>
            <td align="center" style="vertical-align: middle; font-size: large;" colspan="2">
                <table cellpadding="5" cellspacing="0" style="vertical-align: middle; font-family: Arial;
                    font-size: 12px" width="100%" border="1">
                    <tr>
                        <td align="center" colspan="6">
                            <asp:Panel ID="pnlUpdate" runat="server">
                                <asp:UpdateProgress ID="udpProgress" runat="server">
                                    <ProgressTemplate>
                                        <asp:Panel ID="pnlBuffer" runat="server">
                                            <%--<table>
                                                <tr>
                                                    <td align="center" valign="middle">
                                                        <asp:Image ID="imgLoading" runat="server" ImageUrl="~/images/ajax-loader.gif" />
                                                        <br />
                                                        <asp:Label ID="lblPleaseWait" runat="server" Text="Please Wait..." CssClass="smallText" />
                                                    </td>
                                                </tr>
                                            </table>--%>
                                        </asp:Panel>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </asp:Panel>
                            <asp:ValidationSummary ID="vsmMain" runat="server" ValidationGroup="ValExam" DisplayMode="list"
                                CssClass="validatorStyle" ForeColor="Red"/>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center" valign="top" style="width: 32%" class="display-none">
                            <asp:Label ID="lblAccounts" runat="server" Text="Test Category:" />
                            <br />
                            <asp:DropDownList ID="ddlAccounts" runat="server" DataTextField="Campaign" DataValueField="CampaignID"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlAccounts_SelectedIndexChanged" />
                            <br />
                            <br />
                            <asp:Label ID="lblSubCategory" runat="server" Text="Sub Category:" />
                            <br />
                            <asp:DropDownList ID="ddlSubCategory" runat="server" DataTextField="Campaign" DataValueField="CampaignID" />
                        </td>
                        <td colspan="2" align="center" valign="top" style="width: 36%" class="display-none">
                            <asp:Label ID="lblCourseID" runat="server" Text="<%$ Resources:LocalizedResource, CourseId %>" />
                            <br />
                            <asp:TextBox ID="txtCourseID" runat="server" />
                            <ajax:FilteredTextBoxExtender ID="ftbCourseID" runat="server" FilterMode="validchars"
                                FilterType="numbers" TargetControlID="txtCourseID" />
                        </td>
                        <td colspan="2" align="center" valign="middle" style="width: 32%">
                            <asp:CheckBox ID="chkHideGrades" runat="server" Text="<%$ Resources:LocalizedResource, HideExamGradesFromExaminees %>" />
                            <br />
                            <br />
                            <asp:CheckBox ID="chkRetake" runat="server" Text="<%$ Resources:LocalizedResource, RetakeForPassedTest %>" />
                        </td>
                        <td colspan="4" align="center" valign="middle">
                            <div class="col-lg-3">
                                        <asp:Label runat="server" ID="lblTestType" Text="<%$ Resources:LocalizedResource, TestType %>"></asp:Label>
                                        <rad:RadComboBox ID="rcbCreateType" runat="server" DefaultValue="0" RenderMode="Lightweight"
                                            DataValueField="scotypeid" DataTextField="scotype" DataSourceID="dsTestType"
                                            AutoPostBack="True" MaxHeight="150px" EmptyMessage="- Select Test Type -" Enabled="true" 
                                            ToolTip="Select Test Type" OnSelectedIndexChanged="rcbCreateType_OnSelectedIndexChanged"
                                            CssClass="width-fill" OnClientSelectedIndexChanged="rcbCreateType_OnClientSelectedIndexChanged">
                                        </rad:RadComboBox>
                                          <asp:SqlDataSource ID="dsTestType" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
                                            SelectCommand="select ScoTypeID, ScoType from tbl_Scorm_Lkp_ScoType	where ScoTypeID in (2,4) order by ScoTypeID">
                                        </asp:SqlDataSource> 
                                        <asp:CustomValidator ID="cvTestType" runat="server" ErrorMessage="<%$ Resources:LocalizedResource, PleaseSelectATestType %>"
                                Display="none" CssClass="validatorStyle" ValidationGroup="ValExam" />
                                </div>
                            <div class="col-lg-3">
                                <asp:Label runat="server" ID="lblResourceType" Text="<%$ Resources:LocalizedResource, ResourceType %>"></asp:Label>
                                <rad:RadComboBox ID="rcbResourceTypeAssessment" runat="server" DefaultValue="0" RenderMode="Lightweight"
                                    DataSourceID="dsResourceTypeAssessment" DataValueField="scotypeid" DataTextField="scotype"
                                    AutoPostBack="true" MaxHeight="150px" EmptyMessage="<%$ Resources:LocalizedResource, SelectResourceType %>"   
                                    ToolTip="Select Sco Type" CssClass="width-fill" OnSelectedIndexChanged="rcbResourceTypeAssessment_IndexChanged">
                                </rad:RadComboBox>
                                <asp:SqlDataSource ID="dsResourceTypeAssessment" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
                                    SelectCommand="select * from vw_transcomuniversity_ResourceType order by ScoTypeID ASC">
                                </asp:SqlDataSource>

                                <asp:CustomValidator ID="cvResourceType" runat="server" ErrorMessage="Please Select a Resource Type."
                                Display="none" CssClass="validatorStyle" ValidationGroup="ValExam" />
                            </div>
                            <div class="col-lg-3">
                                <asp:Label runat="server" ID="lblSection" Text="<%$ Resources:LocalizedResource, Section %>"></asp:Label>
                                <rad:RadComboBox ID="rcbSectionBrowse" runat="server" DefaultValue="0" RenderMode="Lightweight"
                                    DataValueField="SectionID" DataTextField="SectionName" MaxHeight="150px" EmptyMessage="- Select Section -"
                                    ToolTip="Select Section" CssClass="width-fill">
                                </rad:RadComboBox>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle;" colspan="6" align="center">
                            <asp:Label ID="Label3" runat="server" Visible="false" />
                            <asp:Label ID="lblTestNameEdit" runat="server" Text="<%$ Resources:LocalizedResource, NameOfTest %>" />
                            <br />
                            <asp:TextBox ID="txtExamName" runat="server" MaxLength="200" Width="70%" />
                            <asp:CustomValidator ID="cvExamName" runat="server" ErrorMessage="<%$ Resources:LocalizedResource, EnterAnExamName %>"
                                Display="none" CssClass="validatorStyle" ValidationGroup="ValExam" />
                        </td>

                        
                    </tr>
                    <tr>
                        <td style="vertical-align: middle;" colspan="6" align="center">
                            <asp:CheckBox ID="chkRandomizeQuestions" runat="server" Text="<%$ Resources:LocalizedResource, NotRandomizeQuestions %>" />
                            <asp:CheckBox ID="chkHideExamFromTesting" runat="server" Text="<%$ Resources:LocalizedResource, HidefromTUT %>" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" align="center">
                            <asp:Label ID="lblInstructions" runat="server" Text="<%$ Resources:LocalizedResource, InstructionspleasedonotuseHTML %>" />
                            <br />
                            <asp:TextBox ID="txtInstructions" runat="server" TextMode="multiline" Rows="6" Width="70%" />
                            <ajax:FilteredTextBoxExtender ID="ftbInstructions" runat="server" TargetControlID="txtInstructions"
                                FilterMode="invalidchars" FilterType="custom" InvalidChars="<>" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="width: 50%">
                            <asp:Label ID="lblStartDateEdit" runat="server" Text="<%$ Resources:LocalizedResource, StartDate %>" />
                            <br />
                            <asp:TextBox ID="txtStartDate" runat="server" /><asp:ImageButton ID="imgCalendarStartDate"
                                runat="server" ImageUrl="~/Media/Images/icon-calendar.gif" />
                            <ajax:CalendarExtender ID="calStartDate" runat="server" TargetControlID="txtStartDate"
                                PopupButtonID="imgCalendarStartDate" Format="yyyy/MM/dd" />
                            <asp:DropDownList ID="ddlStartHour" runat="server" />
                            <asp:Label ID="lblStartDateColon" runat="server" Text=":" Font-Bold="true" />
                            <asp:DropDownList ID="ddlStartMinute" runat="server" />
                            <asp:CustomValidator ID="cvStartDate" runat="server" ErrorMessage="<%$ Resources:LocalizedResource, MakeSureTheStartDateFollowsTheFormatStated %>"
                                Display="none" CssClass="validatorStyle" ValidationGroup="ValExam" />
                        </td>
                        <td colspan="3" style="width: 50%">
                            <asp:Label ID="lblEndDateEdit" runat="server" Text="<%$ Resources:LocalizedResource, EndDate %>" />
                            <br />
                            <asp:TextBox ID="txtEndDate" runat="server" /><asp:ImageButton ID="imgCalendarEndDate"
                                runat="server" ImageUrl="~/Media/Images/icon-calendar.gif" />
                            <ajax:CalendarExtender ID="calEndDate" runat="server" TargetControlID="txtEndDate"
                                PopupButtonID="imgCalendarEndDate" Format="yyyy/MM/dd" />
                            <asp:DropDownList ID="ddlEndHour" runat="server" />
                            <asp:Label ID="lblEndDateColon" runat="server" Text=":" Font-Bold="true" />
                            <asp:DropDownList ID="ddlEndMinute" runat="server" />
                            <asp:CustomValidator ID="cvEndDate" runat="server" ErrorMessage="<%$ Resources:LocalizedResource, MakeSureTheEndDateFollowsTheFormatStated %>"
                                Display="none" CssClass="validatorStyle" ValidationGroup="ValExam" />
                           <asp:CustomValidator ID="cvStartgEnd" runat="server" ErrorMessage="<%$ Resources:LocalizedResource, EndDateIsLowerThanStartdate %>"
                                Display="none" CssClass="validatorStyle" ValidationGroup="ValExam" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="width: 33%">
                            <asp:Label ID="lblDurationEdit" runat="server" Text="<%$ Resources:LocalizedResource, Duration %> " ></asp:Label> <br />
                            <asp:Label ID="lblDurationEdit1" runat="server" Text="<%$ Resources:LocalizedResource, MinutesUse0ForNoTimeLimit %>"></asp:Label>
                            <br />
                            <asp:TextBox ID="txtMinutes" runat="server" />
                            <ajax:FilteredTextBoxExtender ID="ftbMinutes" runat="server" FilterMode="validchars"
                                FilterType="numbers" TargetControlID="txtMinutes" />
                            <asp:CustomValidator ID="cvMinutes" runat="server" ErrorMessage="<%$ Resources:LocalizedResource, EnterAWholeNumberForTheDuration %>"
                                Display="none" CssClass="validatorStyle" ValidationGroup="ValExam" />
                        </td>
                        <td colspan="2" style="width: 34%">
                            <asp:Label ID="lblNumberOfTriesEdit" runat="server" Text="<%$ Resources:LocalizedResource, NumberOfAttemptsAllowed %>"></asp:Label> <br />
                            <asp:Label ID="lblNumberOfTriesEdit1" runat="server" Text="<%$ Resources:LocalizedResource, Use0ForUnlimitedTries %>"></asp:Label>
                            <br />
                            <asp:TextBox ID="txtNumTries" runat="server" />
                            <ajax:FilteredTextBoxExtender ID="ftbNumTries" runat="server" FilterMode="validchars"
                                FilterType="numbers" TargetControlID="txtNumTries" /> 
                            <asp:CustomValidator ID="cvNumTries" runat="server" ErrorMessage="<%$ Resources:LocalizedResource, EnterAWholeNumberForTheNumberOfTries %>"
                                Display="none" CssClass="validatorStyle" ValidationGroup="ValExam" />
                        </td>
                        <td colspan="2" style="width: 33%">
                            <asp:Label ID="lblPassingScoreEdit" runat="server" Text="<%$ Resources:LocalizedResource, PassingScore %>"></asp:Label><br />
                            <asp:Label ID="Label1" runat="server" Text="<%$ Resources:LocalizedResource, PercentageIeUse80For80 %>"></asp:Label>
                            <br />
                            <asp:TextBox ID="txtPassingGrade" runat="server" MaxLength="3" ViewStateMode="Enabled" />
                            <ajax:FilteredTextBoxExtender ID="ftbPassingGrade" runat="server" FilterMode="validchars"
                                FilterType="numbers" TargetControlID="txtPassingGrade" /> 
                            <asp:CustomValidator ID="cvPassingGrade" runat="server" ErrorMessage="<%$ Resources:LocalizedResource, EnterAValidWholeNumberFoThePassingGrade %>"
                                Display="none" CssClass="validatorStyle" ValidationGroup="ValExam" />
                        </td>
                    </tr>
                    <%--<tr>
                        <td colspan="6">
                            <ajax:Accordion ID="accordionTestGroups" runat="server" Width="100%" BorderColor="black"
                                BorderWidth="3px" RequireOpenedPane="false" AutoSize="none" SelectedIndex="0">
                                <Panes>
                                    <ajax:AccordionPane ID="paneGroup" runat="server" HeaderCssClass="accordionHeader"
                                        ContentCssClass="accordionContent">
                                        <Header>
                                            <asp:Label ID="lblGroupHeader" runat="server" Text="Choose Test Group/s:" />
                                        </Header>
                                        <Content>
                                            <asp:Panel ID="pnlGroupContent" runat="server" Width="100%" Height="400px">
                                            </asp:Panel>
                                        </Content>
                                    </ajax:AccordionPane>
                                </Panes>
                            </ajax:Accordion>
                        </td>
                    </tr>--%>
                    <tr>
                        <td colspan="6">
                            <asp:Label ID="lblExamItemsEdit" runat="server" Text="Exam Items:" Font-Bold="true" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" align="left">
                            <asp:Button CssClass="buttons" ID="btnFakeAddItem" runat="server" Text="<%$ Resources:LocalizedResource, AddQuestion %>" OnClientClick="confirmAddQuestion(this);return false;" />
                            <asp:Button CssClass="buttons display-none" ID="btnAddItem" runat="server" Text="<%$ Resources:LocalizedResource, AddQuestion %>" OnClick="btnAddItem_Click" /><br />
                            <%--<asp:GridView ID="gvGridItems" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                BorderColor="black" Font-Size="12px" CellPadding="5" BorderWidth="1" OnRowDeleting="gvGridItems_RowDeleting"
                                OnSelectedIndexChanging="gvGridItems_SelectedIndexChanging" EmptyDataText="This test currently has no items."
                                Width="100%" OnRowEditing="gvGridItems_RowEditing" ViewStateMode="Disabled">
                                <PagerStyle CssClass="gridPager" HorizontalAlign="center" ForeColor="black" />
                                <PagerSettings FirstPageText="<<" LastPageText=">>" Mode="NextPreviousFirstLast"
                                    NextPageText=">" Position="Bottom" PreviousPageText="<" />
                                <RowStyle BorderColor="black" ForeColor="black" Font-Size="11px" />
                                <HeaderStyle BorderColor="#C1D7E7" Height="1px" />
                                <Columns>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="center" HeaderText="Question" ItemStyle-HorizontalAlign="left">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="HiddenFieldQuestionnaireID" runat="server" Value='<%# Eval("QuestionnaireID")%>'></asp:HiddenField>
                                            <asp:Label ID="lblQuestionnaireID" runat="server" Text='<%#Bind("QuestionnaireID") %>'
                                                Visible="false" />
                                         <%--   <asp:LinkButton ID="lnkItemQuestion" runat="server" ForeColor="black" Text='<%#Bind("Question") %>'
                                                CommandName="Select" />
                                            <asp:Label ID="lblItemQuestion" runat="server" ForeColor="black" Text='<%#Bind("Question") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField> 
                                     <asp:TemplateField ItemStyle-Width="60px" ItemStyle-HorizontalAlign="center">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEdit"  runat="server" Text="Edit" OnClientClick="ConfirmEdit(this);return false;"
                                                CommandName="Edit" ForeColor="black" Font-Bold="true" />


                                        </ItemTemplate>
                                    </asp:TemplateField>                              
                                    <asp:TemplateField ItemStyle-Width="60px" ItemStyle-HorizontalAlign="center">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtnFakeDelete" OnClientClick="ConfirmDelete(this);return false;"
                                                runat="server" Text="Delete" ForeColor="black" Font-Bold="true"></asp:LinkButton>
                                            <%--<asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" CommandName="Delete"/>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>--%>
                            
                            <rad:RadListView ID="gvGridItems" runat="server" CssClass="GridLess" AllowPaging="false"
                            PageSize="99" Skin="Bootstrap" Width="100%"
                            Font-Size="Small" RenderMode="Lightweight" ItemPlaceholderID="gridmcitem">
                                <LayoutTemplate>
                                    <div id="mandatoryGridView">
                                        <table class="gridMainTable table table-bordered table-striped ">
                                            <tbody>
                                               <asp:PlaceHolder ID="gridmcitem" runat="server">
                   
                                               </asp:PlaceHolder>                   
                                            </tbody>
                                            <tfoot>                    
                                            </tfoot>
                                        </table>

                                    </div>
                                </LayoutTemplate>
                                <ItemTemplate>
                                <tr class="rlvI">
                                    <td>
                                        <asp:HiddenField ID="HiddenFieldQuestionnaireID" runat="server" Value='<%# Eval("QuestionnaireID")%>'></asp:HiddenField>
                                        <asp:HiddenField ID="HiddenFieldIndex" runat="server" Value='<%#DataBinder.Eval(Container, "DataItemIndex")%>'></asp:HiddenField>
                                       
                                        <asp:Label ID="lblQuestionnaireID" runat="server" Text='<%#Bind("QuestionnaireID") %>' Visible="false" />
                                        <asp:Label ID="lblItemQuestion" runat="server" ForeColor="black" Text='<%#Bind("Question") %>' />
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lnkEdit"  runat="server" Text="<%$ Resources:LocalizedResource, Edit %>" OnClientClick="Confirm(this,'edit');return false;"
                                                CommandName="Edit" ForeColor="black" Font-Bold="true" />
                                    </td>

                                    <td>
                                        <asp:LinkButton ID="lbtnFakeDelete" OnClientClick="Confirm(this,'delete');return false;"
                                                runat="server" Text="<%$ Resources:LocalizedResource, Delete %>" ForeColor="black" Font-Bold="true"></asp:LinkButton>
                                    </td>  
                                </tr>       
                            </ItemTemplate>
                            </rad:RadListView>
            
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <asp:Label ID="lblHotlink" runat="server" Text="<%$ Resources:LocalizedResource, ExamHotlink %>" />
                            <br />
                            <a href="#" id="txtHotlink" runat="server" readonly="true" width="70%" target="_blank">
                                Hotlink</a>
                            <%-- <asp:TextBox ID="txtHotlink" runat="server" ReadOnly="true" Width="70%" />--%>
                        </td>
                    </tr>
                    <%--<tr>
            <td colspan="6">
                <asp:LinkButton ID="LinkButton1" runat="server" Text="Click here for a list of users who have taken the exam"
                    Font-Bold="true" OnClick="lnkExamTakers_Click" ForeColor="black" />
            </td>
        </tr>
        <tr>
            <td colspan="3" align="left">
                <asp:Button ID="btnSaveExamEdit" runat="server" Text="Save" CssClass="buttons" Width="80px" />
            </td>
            <td colspan="3" align="right">
                <asp:Button ID="btnCancelExamEdit" runat="server" Text="Cancel" CssClass="buttons"
                    Width="80px" />
            </td>
        </tr>--%>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button CssClass="buttons pull-left" ID="btnFakeReturnToExamList" runat="server"
                    Text="<%$ Resources:LocalizedResource, Return %>" OnClientClick="confirmReturn(this);return false;" />
                <asp:Button CssClass="buttons pull-left display-none" ID="btnReturnToExamList" runat="server"
                    Text="<%$ Resources:LocalizedResource, Return %>" OnClick="btnReturnToExamList_Click" />
                <asp:Button CssClass="buttons pull-right" Text="<%$ Resources:LocalizedResource, Create %>" runat="server" ID="btnFakeContinue" OnClientClick="confirmCreate(this);return false;"/>
                <asp:Button CssClass="buttons pull-right display-none" ID="btnContinue" runat="server" Text="<%$ Resources:LocalizedResource, Create %>"
                    OnClick="btnContinue_Click" />
            </td>
        </tr>
    </table>
    <%-- </ContentTemplate>
    </asp:UpdatePanel>--%>
</asp:Panel>
<asp:HiddenField ID="hidStatus" runat="server" />
<asp:HiddenField ID="hidDelete" runat="server" />
<asp:HiddenField ID="hidEdit" runat="server" />
<asp:HiddenField ID="hidIndex" runat="server" />
<asp:HiddenField ID="hidProcess" runat="server" value="Create"/>
<ajax:ModalPopupExtender ID="mpeStatus" runat="server" TargetControlID="hidStatus"
    PopupControlID="pnlStatus" BackgroundCssClass="modalBackground" />
<asp:Panel ID="pnlStatus" runat="server" CssClass="modalPopup">
    <table cellpadding="10" cellspacing="0" style="border-style: solid; border-color: black;
        font-family: Arial; font-size: 12px; width: 200px">
        <tr>
            <td align="center">
                <asp:Label ID="lblStatus" runat="server" />
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Button CssClass="buttons" ID="btnStatus" runat="server" Text="Return" OnClick="btnStatus_Click" />
            </td>
        </tr>
    </table>
</asp:Panel>
<ajax:ModalPopupExtender ID="mpeDeleteExam" runat="server" TargetControlID="hidDelete"
    PopupControlID="pnlDeleteExam" BackgroundCssClass="modalBackground" />
<asp:Panel ID="pnlDeleteExam" runat="server" CssClass="modalPopup">
    <table cellpadding="10" cellspacing="0" style="border-style: solid; border-color: black;
        font-family: Arial; font-size: 12px; width: 400px">
        <tr>
            <td align="center" colspan="2">
                <asp:Label ID="lblConfirmDelete" runat="server" Text="Delete Item?" />
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:Button CssClass="buttons" ID="btnConfirmDelete" runat="server" Text="Delete"
                    OnClick="btnConfirmDelete_Click" />
            </td>
            <td align="right">
                <asp:Button CssClass="buttons" ID="btnConfirmCancel" runat="server" Text="Cancel" />
            </td>
        </tr>
    </table>
</asp:Panel>
<ajax:ModalPopupExtender ID="mpeNoItems" runat="server" TargetControlID="hidNoItems"
    PopupControlID="pnlNoItems" BackgroundCssClass="modalBackground" />
<asp:HiddenField ID="hidNoItems" runat="server" />
<asp:Panel ID="pnlNoItems" runat="server" CssClass="modalPopup">
    <table cellpadding="10" cellspacing="0" style="border-style: solid; border-color: black;
        font-family: Arial; font-size: 12px; width: 230px">
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblNoItems" runat="server" Text="<%$ Resources:LocalizedResource, ThereAreNoItemsForThisExamYetDoYouWishToContinueSaving %>" />
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:Button CssClass="buttons" ID="btnNoItemsConfirm" runat="server" Text="Save"
                    Width="60px" OnClick="btnNoItemsConfirm_Click" />
            </td>
            <td align="right">
                <asp:Button CssClass="buttons" ID="btnNoItemsCancel" runat="server" Text="<%$ Resources:LocalizedResource, Cancel %>"
                    Width="60px" />
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Button ID="btnConfirmDeleteQuestionItem" runat="server" OnClick="btnConfirmDelete_Click"
    CssClass="display-none" />

<asp:Button ID="btnConfirmEditQuestionItem" runat="server" OnClick="btnConfirmEdit_Click"
    CssClass="display-none" />

<rad:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true" RenderMode="Lightweight"
    Skin="Bootstrap" VisibleStatusbar="false">
</rad:RadWindowManager>
