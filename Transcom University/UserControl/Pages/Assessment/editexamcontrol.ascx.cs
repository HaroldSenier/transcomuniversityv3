/******************************************************************************/
/* Class Filename   -   editexamcontrol.ascx.cs                               */
/* Namespace        -   None                                                  */
/* Description      -   Codefile for creating and editing exams               */
/* Used By          -   NuSkillCheck V2 Manager                               */
/* Created          -   20080702, Anna Aleta P. Oandasan (30988)              */
/* Tandim           -   730367                                                */
/******************************************************************************/
/* ChangeLog                                                                  */
/* 20080702         -   Initial Version                                       */
/******************************************************************************/
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Web;
using NuComm.Security.Encryption;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using NuSkill.Business;
using System.Web.UI;
using TranscomUniversityV3Model;
using System.Linq;
using Telerik.Web.UI;

public partial class UserControl_Pages_Assessment_editexamcontrol : System.Web.UI.UserControl
{
    const string pageFormName = "editexamcontrol.ascx";

    public string ExistingVal = string.Empty;

    private int Deleted
    {
        get { return (int)this.ViewState["delete"]; }
        set { this.ViewState["delete"] = value; }
    }

    //string _Role;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!System.Web.Security.Roles.IsUserInRole(HttpContext.Current.User.Identity.Name.Split('|')[0], "Admin") && !System.Web.Security.Roles.IsUserInRole(HttpContext.Current.User.Identity.Name.Split('|')[0], "Trainer"))
            Response.Redirect("~/Unauthorized.aspx");

        //CheckUserRole();
        this.txtCourseID.Text = Utils.Decrypt(Request.QueryString["CourseID"]);
      
        if (!this.IsPostBack)
        {
            SessionManager.SessionExamAction = "newexam";


            this.PopulateHourDropDownList(this.ddlStartHour);
            this.PopulateHourDropDownList(this.ddlEndHour);
            this.PopulateMinuteDropDownList(this.ddlEndMinute);
            this.PopulateMinuteDropDownList(this.ddlStartMinute);
            //Default to New Exam

            try
            {

                //this.ddlAccounts.DataSource = NonCimCampaign.SelectParents();
                //this.ddlAccounts.DataBind();

                //if (_Role != null)
                //{
                //    this.ddlSubCategory.DataSource = NonCimCampaign.SelectFromParent(Convert.ToInt32(this.ddlAccounts.SelectedValue), true);
                //    this.ddlSubCategory.DataBind();
                //}
                //else
                //{
                //    this.ddlSubCategory.DataSource = NonCimCampaign.SelectCatSubFromParent(Convert.ToInt32(this.ddlAccounts.SelectedValue), Convert.ToInt32(SessionManager.SessionUsername));
                //    this.ddlSubCategory.DataBind();
                //}
                #region Creating a New Exam

                if (SessionManager.SessionExamAction == "newexam")
                {
                    initilizeCreateNewExam();
                }

                #endregion

                //if (!RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(6))
                //    this.btnAddItem.Visible = false;
                
            }
            catch (Exception ex)
            {
                using (ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService())
                {
                    service.WriteError(Config.ApplicationID(), "editexamcontrol.ascx", "Page_Load", ex.Message);
                }
                ///TODO:Catch error when page can't load an exam/load the page for a new exam
            }
        }
        //else
        //{
        //    reloadDetails();
        //}
        //try
        //{
        //    if ((bool)Session["IsNewExam"] == true)
        //    {
        //        btnFakeContinue.Text = "Create";
        //    }else
        //        btnFakeContinue.Text = "Update";
        //}
        //catch (Exception)
        //{
        //    btnFakeContinue.Text = "Update";
        //}
            

    }

    protected void PopulateHourDropDownList(DropDownList list)
    {
        list.Items.Clear();
        for (int i = 0; i < 24; i++)
            list.Items.Add(new ListItem(i.ToString("00"), i.ToString("00")));
    }

    protected void PopulateMinuteDropDownList(DropDownList list)
    {
        list.Items.Clear();
        for (int i = 0; i < 60; i++)
            list.Items.Add(new ListItem(i.ToString("00"), i.ToString("00")));
    }

    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        try
        {
            int tempCategoryID = SessionManager.SessionTestCategoryID;
            OnAddNewQuestion(sender);

            if (tempCategoryID > 0)
            {
                SessionManager.SessionExamAction = "editexam";
                SessionManager.SessionQuestionAction = "newquestion";
                #region Commented
                //TestCategory category = new TestCategory
                //    (
                //        this.txtExamName.Text.Trim(),
                //        string.IsNullOrEmpty(this.txtStartDate.Text.Trim()) ? new DateTime() : new DateTime(Convert.ToInt32(this.txtStartDate.Text.Substring(6, 4)), Convert.ToInt32(this.txtStartDate.Text.Substring(0, 2)), Convert.ToInt32(this.txtStartDate.Text.Substring(3, 2)), Convert.ToInt32(this.ddlStartHour.Text), Convert.ToInt32(this.ddlStartMinute.Text), 0),
                //        string.IsNullOrEmpty(this.txtEndDate.Text.Trim()) ? new DateTime() : new DateTime(Convert.ToInt32(this.txtEndDate.Text.Substring(6, 4)), Convert.ToInt32(this.txtEndDate.Text.Substring(0, 2)), Convert.ToInt32(this.txtEndDate.Text.Substring(3, 2)), Convert.ToInt32(this.ddlEndHour.Text), Convert.ToInt32(this.ddlEndMinute.Text), 0),
                //        this.gvGridItems.Rows.Count,
                //        string.IsNullOrEmpty(this.txtPassingGrade.Text.Trim()) ? 0 : Convert.ToInt32(this.txtPassingGrade.Text.Trim()),
                //        string.IsNullOrEmpty(this.txtNumTries.Text.Trim()) ? 0 : Convert.ToInt32(this.txtNumTries.Text.Trim()),
                //        string.IsNullOrEmpty(this.txtMinutes.Text.Trim()) ? 0 : Convert.ToInt32(this.txtMinutes.Text.Trim()) * 60,
                //        true,
                //        System.DateTime.Now,
                //        !string.IsNullOrEmpty(SessionManager.SessionUsername) ? SessionManager.SessionUsername : "test",
                //        false
                //    );
                //TestCampaignAccount tca = new TestCampaignAccount();
                //AccountList[] accounts = AccountList.SelectAccountsVader2(1);
                //for (int x = 0; x < accounts.Length; x++)
                //{
                //    if (Convert.ToInt32(this.ddlAccounts.SelectedValue) == accounts[x].AccountID)
                //    {
                //        tca.TestCategoryID = 0;
                //        tca.CampaignID = 0;
                //        tca.AccountID = Math.Abs(Convert.ToInt32(this.ddlAccounts.SelectedValue));
                //        tca.IsCampaign = false;
                //        SessionManager.SessionTestCampaignAccount = tca;
                //        break;
                //    }
                //}
                //if (!tca.IsCampaign)
                //{
                //    accounts = AccountList.SelectGenericCampaignsVader2(1);
                //    for (int x = 0; x < accounts.Length; x++)
                //    {
                //        if (Convert.ToInt32(this.ddlAccounts.SelectedValue) == accounts[x].AccountID)
                //        {
                //            tca.TestCategoryID = 0;
                //            tca.CampaignID = Math.Abs(Convert.ToInt32(this.ddlAccounts.SelectedValue));
                //            tca.AccountID = 0;
                //            tca.IsCampaign = true;
                //            SessionManager.SessionTestCampaignAccount = tca;
                //            break;
                //        }
                //    }
                //}
                //SessionManager.SessionTempCategory = category;
                //DataSet set = new DataSet();
                //DataTable table = new DataTable();
                //table.Columns.Add("TestGroupName");
                //table.Columns.Add("IsPartOfGroup");
                //foreach (GridViewRow row in this.gvGroupsEdit.Rows)
                //{
                //    Label lblGroupName = row.FindControl("lblGroupName") as Label;
                //    CheckBox chkGroup = row.FindControl("chkGroup") as CheckBox;
                //    if (lblGroupName != null && chkGroup != null)
                //    {
                //        if (chkGroup.Checked)
                //        {
                //            DataRow inrow = table.NewRow();
                //            inrow.ItemArray.SetValue(lblGroupName.Text, 0);
                //            inrow.ItemArray.SetValue(chkGroup.Checked, 1);
                //            table.Rows.Add(inrow);
                //        }
                //    }
                //}
                //set.Tables.Add(table);
                //SessionManager.SessionTestCategoryGroupMatrixDataSet = set;
                //SessionManager.SessionQuestionAction = "newquestion";
                #endregion

                //Response.Redirect("~/newquestion.aspx?" + Request.QueryString.ToString());
                redirectToQuestionBuilder();
            }
            else
            {
                if (this.CreateExam(false) != 0)
                {

                    SessionManager.SessionExamAction = "editexam";
                    SessionManager.SessionQuestionAction = "newquestion";
                    redirectToQuestionBuilder();
                }
            }

        }
        catch (Exception ex)
        {
            if (!ex.Message.Contains("aborted"))
            {
                using (ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService())
                {
                    service.WriteError(Config.ApplicationID(), "editexamcontrol.ascx", "btnAddItem_Click", ex.Message);
                }
                this.mpeStatus.Show();
            }
        }
    }

    protected void btnContinue_Click(object sender, EventArgs e)
    {
        if (this.CreateExam(false) != 0)
        {
            if (SessionManager.SessionExamAction == "newexam")
            {
                SessionManager.SessionExamAction = "editexam";
                SessionManager.SessionQuestionAction = "newquestion";
                redirectToQuestionBuilder();
            }
            else
            {
                redirectToExamList();
                //redirectToList();
            }
            RadScriptManager.RegisterStartupScript(Page, typeof(Page), "clientRefreshResources", Utils.callClientScript("clientRefreshResources"), true);
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", Utils.callClientScript("clientRefreshResources"), true);
        }
    }

    protected void BindQuestions()
    {
        try
        {
            if (SessionManager.SessionQuestionnaire.Count > 0)
            {
                //this.gvGridItems.DataSource = SessionManager.SessionQuestionnaire;
                //this.gvGridItems.DataBind();
                foreach (RadListViewDataItem row in this.gvGridItems.Items)
                {
                    LinkButton lnkDelete = row.FindControl("lnkDelete") as LinkButton;
                    LinkButton lnkEdit = row.FindControl("lnkEdit") as LinkButton;

                    if (lnkDelete != null && !RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(8))
                        lnkDelete.Visible = false;
                    if (lnkEdit != null && !RightsManagement.ProcessRightsList(SessionManager.SessionUserRights).Contains(7))
                        lnkEdit.Visible = false;
                    LinkButton lnkItemQuestion = row.FindControl("lnkItemQuestion") as LinkButton;
                    Label lblItemQuestion = row.FindControl("lblItemQuestion") as Label;
                    Label lblQuestionnaireID = row.FindControl("lblQuestionnaireID") as Label;
                    if (lblQuestionnaireID.Text == "0")
                    {
                        lnkItemQuestion.Visible = false;
                    }
                    else
                    {
                        if (SessionManager.SessionExamAction == "editexam")
                        {
                            if (lblItemQuestion != null)
                                lblItemQuestion.Visible = false;
                        }
                        else if (SessionManager.SessionExamAction == "newexam")
                        {
                            if (lnkItemQuestion != null)
                                lnkItemQuestion.Visible = false;
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            service.WriteError(Config.ApplicationID(), "editexamcontrol.ascx", "BindQuestions", ex.Message);
        }
    }

    protected int CreateExam(bool isCreateItem)
    {
        try
        {
            int testTypeID = rcbCreateType.SelectedValue == "" ? 0 : Convert.ToInt32(rcbCreateType.SelectedValue);
            int resourceTypeID = rcbResourceTypeAssessment.SelectedValue == "" ? 0 : Convert.ToInt32(rcbResourceTypeAssessment.SelectedValue);
            int sectionID = rcbSectionBrowse.SelectedValue == "" ? 0 : Convert.ToInt32(rcbSectionBrowse.SelectedValue);
            int testCategoryID = 0;
            var db = new TranscomUniversityV3ModelContainer();
            bool isValid = true;
            int tempVal;
            DateTime tempDate;

            if (resourceTypeID <= 0)
            {
                this.cvResourceType.IsValid = false;
                isValid = false;
            }
            if (testTypeID <= 0)
            {
                this.cvTestType.IsValid = false;
                isValid = false;
            }
            if (string.IsNullOrEmpty(this.txtExamName.Text.Trim()))
            {
                this.cvExamName.IsValid = false;
                isValid = false;
            }
            if (!DateTime.TryParse(this.txtStartDate.Text.Trim(), out tempDate))
            {
                this.cvStartDate.IsValid = false;
                isValid = false;
            }
            if (!DateTime.TryParse(this.txtEndDate.Text.Trim(), out tempDate))
            {
                this.cvEndDate.IsValid = false;
                isValid = false;
            }
            if (!int.TryParse(this.txtMinutes.Text.Trim(), out tempVal))
            {
                this.cvMinutes.IsValid = false;
                isValid = false;
            }
            else if (Convert.ToInt32(this.txtMinutes.Text.Trim()) < 0)
            {
                this.cvMinutes.IsValid = false;
                isValid = false;
            }
            if (!int.TryParse(this.txtNumTries.Text.Trim(), out tempVal))
            {
                this.cvNumTries.IsValid = false;
                isValid = false;
            }
            else if (Convert.ToInt32(this.txtNumTries.Text.Trim()) < 0)
            {
                this.cvNumTries.IsValid = false;
                isValid = false;
            }
            if (rcbCreateType.SelectedValue.ToString() == "4")
                this.txtPassingGrade.Text = "0";
            else if (!int.TryParse(this.txtPassingGrade.Text.Trim(), out tempVal))
            {
                this.cvPassingGrade.IsValid = false;
                isValid = false;
            }
            else if (Convert.ToInt32(this.txtPassingGrade.Text.Trim()) > 100)
            {
                this.cvPassingGrade.IsValid = false;
                isValid = false;
            }
            else if (Convert.ToInt32(this.txtPassingGrade.Text.Trim()) < 1)
            {
                this.cvPassingGrade.IsValid = false;
                isValid = false;
            }
            if (cvEndDate.IsValid == true && cvStartDate.IsValid == true)
            {
                var startDate = new DateTime(Convert.ToInt32(this.txtStartDate.Text.Substring(0, 4)), Convert.ToInt32(this.txtStartDate.Text.Substring(5, 2)), Convert.ToInt32(this.txtStartDate.Text.Substring(8, 2)), Convert.ToInt32(this.ddlStartHour.Text), Convert.ToInt32(this.ddlStartMinute.Text), 0);
                var endDate = new DateTime(Convert.ToInt32(this.txtEndDate.Text.Substring(0, 4)), Convert.ToInt32(this.txtEndDate.Text.Substring(5, 2)), Convert.ToInt32(this.txtEndDate.Text.Substring(8, 2)), Convert.ToInt32(this.ddlEndHour.Text), Convert.ToInt32(this.ddlEndMinute.Text), 0);
                if (startDate > endDate)
                {
                    this.cvStartgEnd.IsValid = false;
                    isValid = false;
                }
            }
            if (isValid)
            {
                try
                {
                    SessionManager.SessionExamAction = SessionManager.SessionTestCategoryID == 0 ? "newexam" : "editexam";
                    if (SessionManager.SessionExamAction == "newexam")
                    {
                        if (SessionManager.SessionQuestionnaire != null)
                        {
                            bool isEssay = false;
                            foreach (Questionnaire question in SessionManager.SessionQuestionnaire)
                                if (question.TypeCode == "essay")
                                {
                                    isEssay = true;
                                    break;
                                }
                            TestCategory category = new TestCategory
                            (
                                this.txtExamName.Text.Trim(),
                                new DateTime(Convert.ToInt32(this.txtStartDate.Text.Substring(0, 4)), Convert.ToInt32(this.txtStartDate.Text.Substring(5, 2)), Convert.ToInt32(this.txtStartDate.Text.Substring(8, 2)), Convert.ToInt32(this.ddlStartHour.Text), Convert.ToInt32(this.ddlStartMinute.Text), 0),
                                new DateTime(Convert.ToInt32(this.txtEndDate.Text.Substring(0, 4)), Convert.ToInt32(this.txtEndDate.Text.Substring(5, 2)), Convert.ToInt32(this.txtEndDate.Text.Substring(8, 2)), Convert.ToInt32(this.ddlEndHour.Text), Convert.ToInt32(this.ddlEndMinute.Text), 0),
                                this.gvGridItems.Items.Count,
                                Convert.ToInt32(this.txtPassingGrade.Text.Trim()),
                                Convert.ToInt32(this.txtNumTries.Text.Trim()),
                                Convert.ToInt32(this.txtMinutes.Text.Trim()) * 60,
                                isEssay,
                                System.DateTime.Now,
                                !string.IsNullOrEmpty(SessionManager.SessionUsername) ? SessionManager.SessionUsername : "test",
                                false,
                                this.txtInstructions.Text.Trim(),
                                this.chkHideGrades.Checked,
                                this.chkRetake.Checked
                                //this.chkExternalExam.Checked,
                                //this.chkInternalExam.Checked
                            );

                            if (string.IsNullOrEmpty(this.txtCourseID.Text.Trim()))
                                category.CourseID = 0;
                            else
                                category.CourseID = Convert.ToInt32(this.txtCourseID.Text.Trim());
                            #region commented
                            //foreach (Questionnaire question in SessionManager.SessionQuestionnaire)
                            //{
                            //    question.TestCategoryID = catID;
                            //    question.LastUpdated = !string.IsNullOrEmpty(SessionManager.SessionUsername) ? SessionManager.SessionUsername : "test";
                            //    question.Insert();
                            //}
                            #endregion
                            this.lblStatus.Text = "Exam creation complete.";
                            //TestCampaignAccount tca = new TestCampaignAccount(catID);
                            //AccountList[] accounts = AccountList.SelectAccountsVader2(1);
                            #region 1634854 - switched to categories
                            //AccountList[] accounts = AccountList.SelectAccounts(1);
                            //{
                            //    //accounts = AccountList.SelectGenericCampaignsVader2(1);
                            //    accounts = AccountList.SelectGenericCampaigns(1);
                            //    for (int x = 0; x < accounts.Length; x++)
                            //    {
                            //        if (Convert.ToInt32(this.ddlAccounts.SelectedValue) == accounts[x].AccountID)
                            //        {
                            //            category.CampaignID = Convert.ToInt32(this.ddlAccounts.SelectedValue);
                            //            category.AccountID = 0;
                            //            category.IsCampaign = true;
                            //            //tca.Insert();
                            //            break;
                            //        }
                            //    }
                            //}
                            //if (!category.IsCampaign)
                            //    for (int x = 0; x < accounts.Length; x++)
                            //    {
                            //        if (Convert.ToInt32(this.ddlAccounts.SelectedValue) == accounts[x].AccountID)
                            //        {
                            //            #region commented
                            //            //tca.TestCategoryID = catID;
                            //            //tca.CampaignID = 0;
                            //            //tca.AccountID = Convert.ToInt32(this.ddlAccounts.SelectedValue);
                            //            //tca.IsCampaign = false;
                            //            #endregion
                            //            category.CampaignID = 0;
                            //            category.AccountID = Convert.ToInt32(this.ddlAccounts.SelectedValue);
                            //            category.IsCampaign = false;
                            //            //tca.Insert();
                            //            break;
                            //        }
                            //    }
                            #endregion
                            //category.AccountID = Convert.ToInt32(ddlAccounts.SelectedValue);
                            //category.CampaignID = Convert.ToInt32(ddlSubCategory.SelectedValue);
                            category.AccountID = 0;
                            category.CampaignID = 0;
                            category.IsCampaign = true;
                            category.RandomizeQuestions = !chkRandomizeQuestions.Checked;
                            category.HideExamFromTesting = chkHideExamFromTesting.Checked;
                            int catID = category.Insert();
                            testCategoryID = catID;


                            //set assessment type and section

                            db.pr_transcomuniversity_addAssessmentDetails(testCategoryID, resourceTypeID, sectionID, testTypeID);
                            SessionManager.SessionTestCategoryID = testCategoryID;
                            SessionManager.SessionSingleExam = TestCategory.Select(testCategoryID, true);
                            return testCategoryID;
                        }
                    }
                    else if (SessionManager.SessionExamAction == "editexam")
                    {
                        bool isEssay = false;
                        foreach (Questionnaire question in SessionManager.SessionQuestionnaire)
                            if (question.TypeCode == "essay")
                            {
                                isEssay = true;
                                break;
                            }
                        TestCategory category = SessionManager.SessionSingleExam;

                        category.DateLastUpdated = System.DateTime.Now;
                        category.EndDate = new DateTime(Convert.ToInt32(this.txtEndDate.Text.Substring(0, 4)), Convert.ToInt32(this.txtEndDate.Text.Substring(5, 2)), Convert.ToInt32(this.txtEndDate.Text.Substring(8, 2)), Convert.ToInt32(this.ddlEndHour.Text), Convert.ToInt32(this.ddlEndMinute.Text), 0);
                        category.IsEssay = isEssay;
                        category.LastUpdatedBy = SessionManager.SessionUsername;
                        category.NumberOfQuestions = SessionManager.SessionQuestionnaire.Count;
                        category.PassingGrade = Convert.ToInt32(this.txtPassingGrade.Text.Trim());
                        category.StartDate = new DateTime(Convert.ToInt32(this.txtStartDate.Text.Substring(0, 4)), Convert.ToInt32(this.txtStartDate.Text.Substring(5, 2)), Convert.ToInt32(this.txtStartDate.Text.Substring(8, 2)), Convert.ToInt32(this.ddlStartHour.Text), Convert.ToInt32(this.ddlStartMinute.Text), 0);
                        category.TestLimit = Convert.ToInt32(this.txtNumTries.Text.Trim());
                        category.TestName = this.txtExamName.Text.Trim();
                        category.TimeLimit = Convert.ToInt32(this.txtMinutes.Text.Trim()) * 60;
                        category.Instructions = this.txtInstructions.Text.Trim();
                        if (string.IsNullOrEmpty(this.txtCourseID.Text.Trim()))
                            category.CourseID = 0;
                        else
                            category.CourseID = Convert.ToInt32(this.txtCourseID.Text.Trim());
                        //category.AccountID = Convert.ToInt32(ddlAccounts.SelectedValue);
                        //category.CampaignID = Convert.ToInt32(ddlSubCategory.SelectedValue);
                        category.AccountID = 0;
                        category.CampaignID = 0;
                        category.IsCampaign = true;
                        category.HideScores = this.chkHideGrades.Checked;
                        category.Retake = this.chkRetake.Checked;
                        //category.IsExternal = this.chkExternalExam.Checked;
                        //category.IsInternal = this.chkInternalExam.Checked;
                        category.RandomizeQuestions = !this.chkRandomizeQuestions.Checked;
                        category.HideExamFromTesting = this.chkHideExamFromTesting.Checked;
                        category.Update();
                        this.lblStatus.Text = "Edit exam complete.";
                        foreach (Questionnaire question in SessionManager.SessionQuestionnaire)
                        {
                            Questionnaire dbQuestion = Questionnaire.Select(question.QuestionnaireID, false);
                            if (dbQuestion == null)
                            {
                                question.TestCategoryID = category.TestCategoryID;
                                question.Insert();
                            }
                            else
                                question.Update();
                        }
                        //TestCampaignAccount tca = TestCampaignAccount.SelectByTestCategory(SessionManager.SessionSingleExam.TestCategoryID);
                        //AccountList[] accounts = AccountList.SelectAccountsVader2(1);
                        //AccountList[] accounts = AccountList.SelectAccounts(1);

                        #region 1634854 - switched to categories
                        //AccountList[] accounts = AccountList.SelectAccounts(1);
                        //{
                        //    //accounts = AccountList.SelectGenericCampaignsVader2(1);
                        //    accounts = AccountList.SelectGenericCampaigns(1);
                        //    for (int x = 0; x < accounts.Length; x++)
                        //    {
                        //        if (Convert.ToInt32(this.ddlAccounts.SelectedValue) == accounts[x].AccountID)
                        //        {
                        //            category.CampaignID = Convert.ToInt32(this.ddlAccounts.SelectedValue);
                        //            category.AccountID = 0;
                        //            category.IsCampaign = true;
                        //            //tca.Insert();
                        //            break;
                        //        }
                        //    }
                        //}
                        //if (!category.IsCampaign)
                        //    for (int x = 0; x < accounts.Length; x++)
                        //    {
                        //        if (Convert.ToInt32(this.ddlAccounts.SelectedValue) == accounts[x].AccountID)
                        //        {
                        //            #region commented
                        //            //tca.TestCategoryID = catID;
                        //            //tca.CampaignID = 0;
                        //            //tca.AccountID = Convert.ToInt32(this.ddlAccounts.SelectedValue);
                        //            //tca.IsCampaign = false;
                        //            #endregion
                        //            category.CampaignID = 0;
                        //            category.AccountID = Convert.ToInt32(this.ddlAccounts.SelectedValue);
                        //            category.IsCampaign = false;
                        //            //tca.Insert();
                        //            break;
                        //        }
                        //    }
                        #endregion
                        testCategoryID = category.TestCategoryID;
                        SessionManager.SessionTestCategoryID = testCategoryID;
                        SessionManager.SessionExamAction = "editexam";
                        db.pr_transcomuniversity_updateAssessmentDetails(testCategoryID, resourceTypeID, sectionID, testTypeID);
                        //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", Utils.callClientScript("clientRefreshResources"), true);
                        //reloadDetails();
                        return testCategoryID;
                    }
                }
                catch (Exception ex)
                {
                    using (ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService())
                    {
                        service.WriteError(Config.ApplicationID(), "editexamcontrol.ascx", "CreateExam - Inner", ex.Message);
                    }
                    this.lblStatus.Text = "An error occured.";
                }
                finally
                {
                    if (isCreateItem)
                        this.mpeStatus.Show();
                    SessionManager.SessionTestCampaignAccount = null;
                }

            }
        }
        catch (Exception ex)
        {
            if (!ex.Message.Contains("aborted"))
            {
                using (ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService())
                {
                    service.WriteError(Config.ApplicationID(), "editexamcontrol.ascx", "CreateExam - Outer", ex.Message);
                }
                return 0;
            }
        }
        return 0;
    }

    protected void btnStatus_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/exams.aspx?" + Request.QueryString.ToString());
    }

    //protected void gvGridItems_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    //{
    //    try
    //    {
    //        GridViewRow row = this.gvGridItems.Items[e.NewSelectedIndex];
    //        Label lblQuestionnaireID = row.FindControl("lblQuestionnaireID") as Label;
    //        if (lblQuestionnaireID != null)
    //        {
    //            Questionnaire question = Questionnaire.Select(Convert.ToInt32(lblQuestionnaireID.Text.Trim()), false);
    //            SessionManager.SessionSingleQuestion = question;
    //            SessionManager.SessionSingleQuestionIndex = e.NewSelectedIndex;
    //            SessionManager.SessionQuestionAction = "viewsinglequestion";
    //            Response.Redirect("~/viewsinglequestion.aspx?" + Request.QueryString.ToString());
    //        }
    //        else
    //        {
    //            throw new Exception("No questionnaireID.");
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        using (ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService())
    //        {
    //            service.WriteError(Config.ApplicationID(), "editexamcontrol.ascx", "gvGridItems_SelectedIndexChanging", ex.Message);
    //        }
    //    }
    //}

    protected void gvGridItems_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            redirectToQuestionBuilder();
            SessionManager.SessionSingleQuestion = SessionManager.SessionQuestionnaire[e.NewEditIndex];
            SessionManager.SessionSingleQuestionIndex = e.NewEditIndex;
            SessionManager.SessionQuestionAction = "editquestion";
            //CourseContentRightPanelCtrl rightpanel = new CourseContentRightPanelCtrl();
            //var question = (asd as UserControl_Pages_Assessment_editexamquestioncontrol)rightpanel.FindControl("questionBuilder");
            //question.reloadQuestionDetails();
            OnReloadQuestionDetails(sender);
            
            //Response.Redirect("~/newquestion.aspx?" + Request.QueryString.ToString());
        }
        catch (Exception ex)
        {
            using (ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService())
            {
                service.WriteError(Config.ApplicationID(), "editexamcontrol.ascx", "gvGriditems_RowEditing", ex.Message);
            }
        }
    }

    protected void gvGridItems_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            this.Deleted = e.RowIndex;

            try
            {
                Questionnaire temp = SessionManager.SessionQuestionnaire[this.Deleted];
                if (temp.QuestionnaireID != 0)
                    temp.Delete();

                SessionManager.SessionQuestionnaire.RemoveAt(this.Deleted);

                //mpeDeleteExam.Show();
                //SessionManager.SessionQuestionnaire.RemoveAt(this.Deleted);
                this.BindQuestions();
            }
            catch (Exception ex)
            {
                using (ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService())
                {
                    service.WriteError(Config.ApplicationID(), "editexamcontrol.ascx", "btnConfirmDelete_Click", ex.Message);
                }
            }

            //this.mpeDeleteExam.Show();
        }
        catch (Exception ex)
        {
            using (ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService())
            {
                service.WriteError(Config.ApplicationID(), "editexamcontrol.ascx", "gvGridItems_RowDeleting", ex.Message);
            }
        }
    }

    protected void btnConfirmDelete_Click(object sender, EventArgs e)
    {
        try
        {
            //Deleted = Convert.ToInt32(hidDelete.Value) - 1;
            Deleted = Convert.ToInt32(hidIndex.Value);
            Questionnaire temp = SessionManager.SessionQuestionnaire[this.Deleted];
            if (temp.QuestionnaireID != 0)
                temp.Delete();
            SessionManager.SessionQuestionnaire.RemoveAt(this.Deleted);
            reloadDetails();
        }
        catch (Exception ex)
        {
            using (ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService())
            {
                service.WriteError(Config.ApplicationID(), "editexamcontrol.ascx", "btnConfirmDelete_Click", ex.Message);
            }
        }
    }

    protected void btnConfirmEdit_Click(object sender, EventArgs e)
    {
        try
        {
            //Questionnaire temp = SessionManager.SessionQuestionnaire[this.Deleted];
            var questionaireindex = Convert.ToInt32(hidIndex.Value);
            redirectToQuestionBuilder();
            SessionManager.SessionSingleQuestion = SessionManager.SessionQuestionnaire[questionaireindex];
            SessionManager.SessionSingleQuestionIndex = Convert.ToInt32(questionaireindex);
            SessionManager.SessionQuestionAction = "editquestion";
            //CourseContentRightPanelCtrl rightpanel = new CourseContentRightPanelCtrl();
            //var question = (asd as UserControl_Pages_Assessment_editexamquestioncontrol)rightpanel.FindControl("questionBuilder");
            //question.reloadQuestionDetails();
            OnReloadQuestionDetails(sender);
            
            //Response.Redirect("~/newquestion.aspx?" + Request.QueryString.ToString());

        }
        catch (Exception ex)
        {
           
        }
    }

    #region commented
    //Temporarily commented out, may be delete if determined to be unused by deployment
    //protected void gvGridItems_RowCommand(object sender, GridViewCommandEventArgs e)
    //{
    //    if (e.CommandName == "View")
    //    {
    //        GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
    //        GridView view = (GridView)row.NamingContainer;
    //        if (view != null)
    //        {
    //            ~
    //        }
    //    }
    //}
    #endregion

    protected void btnNoItemsConfirm_Click(object sender, EventArgs e)
    {
        this.CreateExam(true);
    }

    //protected void Populategroups()
    //{
    //    try
    //    {
    //        if (SessionManager.SessionExamAction == "editexam" || SessionManager.SessionTestCategoryGroupMatrixDataSet != null)
    //        {
    //            this.gvGroupsEdit.Visible = true;
    //            this.gvGroupsEdit.DataSource = TestCategoryGroupMatrix.SelectAllGroupByCategory(SessionManager.SessionSingleExam.TestCategoryID);
    //            this.gvGroupsEdit.DataBind();

    //            foreach (GridViewRow row in this.gvGroupsEdit.Rows)
    //            {
    //                Label lblIsPartOfGroup = row.FindControl("lblIsPartOfGroup") as Label;
    //                CheckBox chkGroup = row.FindControl("chkGroup") as CheckBox;
    //                chkGroup.Checked = Convert.ToBoolean(lblIsPartOfGroup.Text);
    //            }
    //        }
    //        else
    //        {
    //            this.gvGroups.Visible = true;
    //            this.gvGroups.DataSource = TestGroup.SelectAll();
    //            this.gvGroups.DataBind();
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        using (ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService())
    //        {
    //            service.WriteError(Config.ApplicationID(), "editexamcontrol.ascx", "Populategroups", ex.Message);
    //        }
    //    }
    //}

    //protected void btnAddNewGroup_Click(object sender, EventArgs e)
    //{
    //    this.pnlAddGroup.Visible = true;
    //    this.btnAddNewGroup.Visible = false;
    //}

    //protected void btnAddGroupCancel_Click(object sender, EventArgs e)
    //{
    //    this.pnlAddGroup.Visible = false;
    //    this.btnAddNewGroup.Visible = true;
    //}

    //protected void btnAddGroupOK_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        if (!string.IsNullOrEmpty(this.txtAddNewGroup.Text.Trim()))
    //        {
    //            TestGroup group = new TestGroup(this.txtAddNewGroup.Text.Trim(), SessionManager.SessionUsername);
    //            group.Insert();
    //            this.Populategroups();
    //            this.txtAddNewGroup.Text = string.Empty;
    //            this.pnlAddGroup.Visible = false;
    //            this.btnAddNewGroup.Visible = true;
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        using (ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService())
    //        {
    //            service.WriteError(Config.ApplicationID(), "editexamcontrol.ascx", "btnAddGroupOK_Click", ex.Message);
    //        }
    //    }
    //}

    protected void ddlAccounts_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (_Role != null)
        //{
        //    this.ddlSubCategory.DataSource = NonCimCampaign.SelectFromParent(Convert.ToInt32(this.ddlAccounts.SelectedValue), true);
        //    this.ddlSubCategory.DataBind();
        //}
        //else
        //{
        //    this.ddlSubCategory.DataSource = NonCimCampaign.SelectCatSubFromParent(Convert.ToInt32(this.ddlAccounts.SelectedValue), Convert.ToInt32(SessionManager.SessionUsername));
        //    this.ddlSubCategory.DataBind();
        //}
    }

    public void reloadDetails()
    {
        TestCategory category = TestCategory.Select(SessionManager.SessionTestCategoryID, true);
        SessionManager.SessionSingleExam = category;

        #region Editing an Existing Exam
        if (SessionManager.SessionTestCategoryID > 0 && SessionManager.SessionExamAction != "newexam")
        {
            btnFakeContinue.Text = "Update";
            hidProcess.Value = "Update";
            string testlink = "";
            if (HttpContext.Current.Request.Url.Host == "localhost")
                testlink = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + ResolveUrl("~/") + "hotlink.aspx?tid=" + HttpUtility.UrlEncode(UTF8.EncryptText(SessionManager.SessionTestCategoryID.ToString()));
            else
                testlink = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + ResolveUrl("~/") + "hotlink.aspx?tid=" + HttpUtility.UrlEncode(UTF8.EncryptText(SessionManager.SessionTestCategoryID.ToString()));
            this.txtHotlink.InnerHtml = testlink;
            this.txtHotlink.HRef = testlink;

            Questionnaire[] questionnaires = Questionnaire.SelectByCategory(category.TestCategoryID);
            SessionManager.SessionQuestionnaire = new List<Questionnaire>();

            this.gvGridItems.DataSource = questionnaires;
            this.gvGridItems.DataBind();

            if (questionnaires.Count() > 0)
                rcbCreateType.Enabled = false;
            else
                rcbCreateType.Enabled = true;


            this.ddlAccounts.SelectedIndex = -1;
            this.ddlStartHour.SelectedIndex = -1;
            this.ddlStartMinute.SelectedIndex = -1;
            this.ddlEndHour.SelectedIndex = -1;
            this.ddlEndMinute.SelectedIndex = -1;

            //this.ddlAccounts.SelectedValue = category.AccountID.ToString();

            var resourceType = DataHelper.GetAssessmentResourceType(SessionManager.SessionTestCategoryID);
            if (resourceType == 5)
                rcbResourceTypeAssessment.SelectedIndex = 1;
            else if (resourceType == 8)
                rcbResourceTypeAssessment.SelectedIndex = 2;
            else
                rcbResourceTypeAssessment.SelectedIndex = 0;
            int sectionIndex = rcbSectionBrowse.FindItemIndexByValue(DataHelper.GetAssessmentSection(SessionManager.SessionTestCategoryID) + "");
            rcbSectionBrowse.SelectedIndex = sectionIndex;

            var TestType = Convert.ToString(DataHelper.GetAssessmentTestType(SessionManager.SessionTestCategoryID));
            TestType = TestType == null ? "2" : TestType;
            if (TestType == "2")
            {
                rcbCreateType.SelectedIndex = 0;
                txtPassingGrade.Enabled = true;
            }
            else
            {
                rcbCreateType.SelectedIndex = 1;
                txtPassingGrade.Enabled = false;

            }

            //if (_Role != null)
            //{
            //    this.ddlSubCategory.DataSource = NonCimCampaign.SelectFromParent(Convert.ToInt32(this.ddlAccounts.SelectedValue), true);
            //    this.ddlSubCategory.DataBind();
            //    this.ddlSubCategory.SelectedValue = category.CampaignID.ToString();
            //}
            //else
            //{
            //    this.ddlSubCategory.DataSource = NonCimCampaign.SelectCatSubFromParent(Convert.ToInt32(this.ddlAccounts.SelectedValue), Convert.ToInt32(SessionManager.SessionUsername));
            //    this.ddlSubCategory.DataBind();
            //    this.ddlSubCategory.SelectedValue = category.CampaignID.ToString();
            //}

            if (!string.IsNullOrEmpty(category.Instructions))
                this.txtInstructions.Text = category.Instructions;

            this.txtExamName.Text = category.TestName;
            this.txtStartDate.Text = category.StartDate.ToString("yyyy/MM/dd");
            this.txtEndDate.Text = category.EndDate.ToString("yyyy/MM/dd");

            foreach (ListItem item in this.ddlStartHour.Items)
                if (Convert.ToInt32(item.Value) == category.StartDate.Hour)
                {
                    item.Selected = true;
                    break;
                }

            foreach (ListItem item in this.ddlEndHour.Items)
                if (Convert.ToInt32(item.Value) == category.EndDate.Hour)
                {
                    item.Selected = true;
                    break;
                }

            foreach (ListItem item in this.ddlStartMinute.Items)
                if (Convert.ToInt32(item.Value) == category.StartDate.Minute)
                {
                    item.Selected = true;
                    break;
                }

            foreach (ListItem item in this.ddlEndMinute.Items)
                if (Convert.ToInt32(item.Value) == category.EndDate.Minute)
                {
                    item.Selected = true;
                    break;
                }

            int timeLimit = category.TimeLimit / 60;
            this.txtMinutes.Text = timeLimit.ToString();
            this.txtNumTries.Text = category.TestLimit.ToString();
            this.txtPassingGrade.Text = category.PassingGrade.ToString();
            this.txtCourseID.Text = category.CourseID.ToString();
            this.chkHideGrades.Checked = category.HideScores;
            this.chkRetake.Checked = category.Retake;

            //tandim #2265866 - to add additional option to allow to randomize question checkbox.
            this.chkRandomizeQuestions.Checked = !category.RandomizeQuestions;
            this.chkHideExamFromTesting.Checked = category.HideExamFromTesting;

            bool found = false;

            if (questionnaires.Length > 0)
                foreach (Questionnaire questionnaire in questionnaires)
                {
                    found = false;
                    foreach (Questionnaire inQuestion in SessionManager.SessionQuestionnaire)
                        if (questionnaire.QuestionnaireID == inQuestion.QuestionnaireID)
                        {
                            found = true;
                            break;
                        }
                    if (!found)
                        SessionManager.SessionQuestionnaire.Add(questionnaire);
                }


            this.btnContinue.Text = "Update";
            btnFakeContinue.Text = "Update";
            hidProcess.Value = "Update";
            gvGridItems.Visible = true;

            //this.BindQuestions();
        }   

        #endregion

        RadScriptManager.RegisterStartupScript(Page, typeof(Page), "hidePageLoading2", Utils.callClientScript("hidePageLoading"), true);


    }

    public void initilizeCreateNewExam()
    {
        //PoPulateCreateType();
        this.ddlStartHour.SelectedIndex = -1;
        this.ddlStartMinute.SelectedIndex = -1;
        this.ddlEndHour.SelectedIndex = -1;
        this.ddlEndMinute.SelectedIndex = -1;
        rcbResourceTypeAssessment.ClearSelection();
        rcbSectionBrowse.Items.Clear();
        this.txtExamName.Text = "";
        this.txtHotlink.InnerText = "";
        this.rcbCreateType.ClearSelection();
        this.rcbCreateType.Enabled = true;
        this.txtInstructions.Text = "";

        this.txtStartDate.Text = "";
        this.txtEndDate.Text = "";
        gvGridItems.Visible = false;
        gvGridItems.DataSource = null;
        gvGridItems.DataBind();
        this.txtMinutes.Text = "";
        this.txtNumTries.Text = "";
        this.txtPassingGrade.Text = "";
        this.txtCourseID.Text = Request.QueryString["CourseID"];
        this.chkHideGrades.Checked = false;
        this.chkRetake.Checked = false;

        //tandim #2265866 - to add additional option to allow to randomize question checkbox.
        this.chkRandomizeQuestions.Checked = false;
        this.chkHideExamFromTesting.Checked = false;

        Questionnaire[] questionnaires = Questionnaire.SelectByCategory(0);
        if (SessionManager.SessionQuestionnaire == null)
            SessionManager.SessionQuestionnaire = new List<Questionnaire>();

        bool found = false;

        if (questionnaires.Length > 0)
            foreach (Questionnaire questionnaire in questionnaires)
            {
                found = false;
                foreach (Questionnaire inQuestion in SessionManager.SessionQuestionnaire)
                    if (questionnaire.QuestionnaireID == inQuestion.QuestionnaireID)
                    {
                        found = true;
                        break;
                    }
                if (!found)
                    SessionManager.SessionQuestionnaire.Add(questionnaire);
            }

        this.btnContinue.Text = "Create";
        btnFakeContinue.Text = "Create";
        hidProcess.Value = "Create";
    }

    protected void redirectToList()
    {
        UserControl testStatus = (UserControl)this.NamingContainer.FindControl("testStatus");
        UserControl assessmentBuilder = (UserControl)this.NamingContainer.FindControl("assessmentBuilder");
        UserControl questionBuilder = (UserControl)this.NamingContainer.FindControl("questionBuilder");
        UserControl examsList = (UserControl)this.NamingContainer.FindControl("examsList");

        //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", Utils.callClientScript("clientRefreshResourcesNoLoading"), true);

        //assessmentBuilder.Visible = false;
        //questionBuilder.Visible = false;
        //testStatus.Visible = false;
        //examsList.Visible = true;
    }

    protected void redirectToQuestionBuilder()
    {
        UserControl testStatus = (UserControl)this.NamingContainer.FindControl("testStatus");
        UserControl assessmentBuilder = (UserControl)this.NamingContainer.FindControl("assessmentBuilder");
        UserControl questionBuilder = (UserControl)this.NamingContainer.FindControl("questionBuilder");
        UserControl examsList = (UserControl)this.NamingContainer.FindControl("examsList");

        
        
        RadComboBox ddlContent = questionBuilder.FindControl("ddlItemtypes") as RadComboBox;
        PopulateDropDownList(ddlContent);
        assessmentBuilder.Visible = false;
        questionBuilder.Visible = true;
        testStatus.Visible = false;
        examsList.Visible = false;

        // Button btnRefresh = this.Parent.FindControl("") as Button;

    }

    public void PopulateDropDownList(RadComboBox ddlItemTypes)
    {
        try
        {
            ddlItemTypes.Items.Clear();
            QuestionType[] questions = QuestionType.SelectAll();
            string TestType = string.Empty;
            if (SessionManager.SessionTestCategoryID > 0)
                TestType = Convert.ToString(DataHelper.GetAssessmentTestType(SessionManager.SessionTestCategoryID));

            //for (int i = 0; i < questions.Length; i++)
            //{
            //    if (SessionManager.SessionQuestionAction == "suggestquestion")
            //    {
            //        if (questions[i].TypeCode != "hotspot")
            //            ddlItemTypes.Items.Add(new ListItem(questions[i].Description, questions[i].TypeCode));
            //    }
            //    else
            //    {
            //        if (TestType != "2")
            //        {
            //            if (questions[i].Description.Contains("Survey"))
            //                ddlItemTypes.Items.Add(new ListItem(questions[i].Description, questions[i].TypeCode));
            //        }
            //        else
            //        {
            //            if (!questions[i].Description.Contains("Survey"))
            //                ddlItemTypes.Items.Add(new ListItem(questions[i].Description, questions[i].TypeCode));
            //        }
            //    }

            //}


            if (SessionManager.SessionQuestionAction == "suggestquestion")
            {
                var ds = questions
                        .Where(c => c.TypeCode != "hotspot").ToList();

                ddlItemTypes.DataSource = ds;
                ddlItemTypes.DataBind();
            }
            else
            {
                if (TestType != "2")
                {
                    var ds = questions
                        .Select(c => new
                        {
                            c.Description,
                            c.TypeCode,
                            c.HideFromList
                        })
                       .Where(c => c.Description.Contains("Survey")).ToList();

                    ddlItemTypes.DataSource = ds;
                    ddlItemTypes.DataBind();

                }
                else
                {
                    var ds = questions
                         .Select(c => new
                         {
                             c.Description,
                             c.TypeCode,
                             c.HideFromList
                         })
                        .Where(c => !c.Description.Contains("Survey")).ToList();

                    ddlItemTypes.DataSource = ds;
                    ddlItemTypes.DataBind();
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
            service.WriteError(Config.ApplicationID(), "editexamquestioncontrol.ascx", "PopulateDropDownList", ex.Message);
        }
    }

    protected void redirectToExamList()
    {
        UserControl testStatus = (UserControl)this.NamingContainer.FindControl("testStatus");
        UserControl assessmentBuilder = (UserControl)this.NamingContainer.FindControl("assessmentBuilder");
        UserControl questionBuilder = (UserControl)this.NamingContainer.FindControl("questionBuilder");
        UserControl examsList = (UserControl)this.NamingContainer.FindControl("examsList");
        RadListView rgExams = examsList.FindControl("rgExams") as RadListView;
        RadWindowManager radWinMng = examsList.FindControl("RadWindowManager1") as RadWindowManager;



        int courseID = Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"]));
        rgExams.DataSource = DataHelper.allExamsByCourse(courseID);
        rgExams.DataBind();

        ClearSession();

        assessmentBuilder.Visible = false;
        questionBuilder.Visible = false;
        testStatus.Visible = false;
        examsList.Visible = true;


        //radWinMng.RadAlert(rcbCreateType.Text + " Operation Successfull!", 330, 180, "Success Message", "");
    }

    //edit question
    public delegate bool customHandler(object sender);
    public event customHandler EventReloadQuestionDetails;

    protected void OnReloadQuestionDetails(object sender)
    {
        EventReloadQuestionDetails(sender);
    }

    //add item
    public event customHandler EventAddQuestion;

    protected void OnAddNewQuestion(object sender)
    {
        EventAddQuestion(sender);
    }

    protected void btnReturnToExamList_Click(object sender, EventArgs e)
    {
        redirectToExamList();
    }

    void ClearSession()
    {
        SessionManager.SessionCreatorComments = string.Empty;
        SessionManager.SessionExamAction = string.Empty;
        SessionManager.SessionExamineeName = string.Empty;
        SessionManager.SessionQuestionAction = string.Empty;
        //SessionManager.SessionQuestionnaire = ;
        SessionManager.SessionQuestionNumber = 0;
        SessionManager.SessionRecTestCategoryID = 0;
        SessionManager.SessionRole = 0;
        //SessionManager.SessionSaveTestResponse = 0;
        SessionManager.SessionSaveTestResponseID = 0;
        //SessionManager.SessionSingleExam = 0;
        //SessionManager.SessionSingleQuestion = string.Empty;
        SessionManager.SessionSingleQuestionIndex = 0;
        //SessionManager.SessionSingleSaveTestResponse = string.Empty;
        //SessionManager.SessionTempCategory = string.Empty;
        //SessionManager.SessionTestCampaignAccount = string.Empty;
        //SessionManager.SessionTestCategoryGroupMatrixDataSet = string.Empty;
        SessionManager.SessionTestCategoryID = 0;
        //SessionManager.SessionTestTaken = string.Empty;
        SessionManager.SessionTestTakenID = 0;
        SessionManager.SessionUsername = string.Empty;
        SessionManager.SessionUserRights = string.Empty;
    }

    protected void rcbResourceTypeAssessment_IndexChanged(object sender, EventArgs e)
    {
        try
        {

            rcbSectionBrowse.ClearSelection();
            rcbSectionBrowse.Items.Clear();
            int resourceTypeID = Convert.ToInt32(rcbResourceTypeAssessment.SelectedValue);
            int courseid = Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseiD"]));
            var db = new TranscomUniversityV3ModelContainer();
            var sections = db.tbl_transcomuniversity_rlt_CourseSections
                                .Where(s => s.CourseID == courseid && s.HideFromList == false && s.ResourceTypeID == resourceTypeID)
                                .ToList();


            rcbSectionBrowse.DataSource = sections;
            rcbSectionBrowse.DataBind();
        }
        catch (ArgumentOutOfRangeException aore)
        {
            RadWindowManager1.RadAlert("Something went wrong. Please reload the page and try again.", 330, 180, "Error", "");
        }
    }

    protected void rcbCreateType_OnSelectedIndexChanged(object sender, EventArgs e)
    {

        if (rcbCreateType.SelectedItem.Value.ToString() == "4")
        {
            txtPassingGrade.Enabled = false;

        }
        else
            txtPassingGrade.Enabled = true;


    }

    private void GetLang()
    {
        #region HEADER
        chkHideGrades.Text = Resources.LocalizedResource.HideExamGradesFromExaminees.ToString();
        chkRetake.Text = Resources.LocalizedResource.RetakeForPassedTest.ToString();
        #endregion



        #region FOOTER
       
        #endregion

       
    }

  
}