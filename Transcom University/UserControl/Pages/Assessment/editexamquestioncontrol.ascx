<%@ Control Language="C#" AutoEventWireup="true" CodeFile="editexamquestioncontrol.ascx.cs"
    Inherits="UserControl_Pages_Assessment_editexamquestioncontrol" %>

<%@ Register TagPrefix="wus" TagName="editexamitemcontrol" Src="~/UserControl/Pages/Assessment/editexamcontrol.ascx" %>
<style type="text/css">
    .style1
    {
        width: 111px;
    }
    .style2
    {
        height: 34px;
        width: 111px;
    }
</style>

<asp:Panel ID="pnlExamQuestion" runat="server" Width="100%" HorizontalAlign="center"
    ForeColor="black">
    <%--border-color: #4682b4; background-color: White;">--%>
    <table cellpadding="5" width="100%" cellspacing="0" style="height: 400px; vertical-align: text-top">
        <tr>
            <td align="center">
                <table cellpadding="5" cellspacing="0" style="vertical-align: middle; font-family: Arial;
                    font-size: 12px;" width="100%">
                    <tr>
                        <td align="left" style="vertical-align: middle; font-size: 12px; font-family: Arial;">
                            <asp:Label ID="lblQuestion" runat="server" Text="<%$ Resources:LocalizedResource, EnterQuestioninstructionsHere %>" />
                            <br />
                            <asp:TextBox ID="txtQuestion" runat="server" MaxLength="2000" Width="600px" />
                        </td>
                    </tr>
                    <tr class="display-none">
                        <td align="left" style="vertical-align: middle; font-size: 12px; font-family: Arial">
                            <asp:Label ID="lblHyperlink" runat="server" Text="Enter a URL for the question here, if applicable:" />
                            <br />
                            <asp:TextBox ID="txtHyperlink" runat="server" MaxLength="2000" Width="600px" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:ValidationSummary ID="vsmItems" runat="server" DisplayMode="List" ValidationGroup="ValItems" ForeColor="Red" />
                            <asp:CustomValidator ID="cvNoQuestion" runat="server" Display="none" ErrorMessage="<%$ Resources:LocalizedResource, Provideaquestion%>"
                                ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvNoOptionsMultipleChoice" runat="server" Display="none"
                                ErrorMessage="<%$ Resources:LocalizedResource, Provideatleasttwochoicesandatleastonecorrectanswer%>" 
                                ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvDuplicateMultipleChoice" runat="server" Display="none"
                                ErrorMessage="<%$ Resources:LocalizedResource, Makesuretherearenoduplicateanswers%>" ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvItemSkipMultipleChoice" runat="server" Display="none"
                                ErrorMessage="<%$ Resources:LocalizedResource, Makesuretherearenoskippedspacesfortheanswers%>" ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvCorrectBlankMultipleChoice" runat="server" Display="none"
                                ErrorMessage="<%$ Resources:LocalizedResource, Makesurenoblanksaremarkedascorrectanswers%>" ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvNoOptionsTrueOrFalse" runat="server" Display="none" ErrorMessage="<%$ Resources:LocalizedResource, Providethecorrectanswer%>"
                                ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvNoOptionsYesOrNo" runat="server" Display="none" ErrorMessage="<%$ Resources:LocalizedResource, Providethecorrectanswer%>"
                                ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvNoOptionsSequencing" runat="server" Display="none" ErrorMessage="<%$ Resources:LocalizedResource, Provideatleasttwochoicesandtheirproperorder%>"
                                ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvDuplicateSequencing" runat="server" Display="none" ErrorMessage="<%$ Resources:LocalizedResource, Makesuretherearenoduplicateanswers%>"
                                ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvOptionMismatchSequencing" runat="server" Display="None"
                                ErrorMessage="<%$ Resources:LocalizedResource, MakesuretheanswersintheRandomOrderandtheCorrectOrderarethesame%>"
                                ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvItemSkipSequencing" runat="server" Display="none" ErrorMessage="<%$ Resources:LocalizedResource, Makesuretherearenoskippedspacesforbothcolumns%>"
                                ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvCountMismatchSequencing" runat="server" Display="none"
                                ErrorMessage="<%$ Resources:LocalizedResource, ProvidethesamenumberofitemsintheRandomOrderandtheCorrectOrdercolumns%> "
                                ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvItemSkipMatching" runat="server" Display="none" ErrorMessage="<%$ Resources:LocalizedResource, Makesuretherearenoskippedspacesforallcolumns%>"
                                ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvNoOptionsMatching" runat="server" Display="none" ErrorMessage="<%$ Resources:LocalizedResource, Provideatleasttwochoicesontherightcolumn%>"
                                ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvNoItemsMatching" runat="server" Display="none" ErrorMessage="<%$ Resources:LocalizedResource, Provideatleasttwoitemsontheleftcolumn%>"
                                ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvInvalidChoiceMatching" runat="server" Display="none" ErrorMessage="<%$ Resources:LocalizedResource, Makesureallanswersarevalidnoblanks%>"
                                ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvDuplicateMatching" runat="server" Display="none" ErrorMessage="<%$ Resources:LocalizedResource, Makesuretherearenoduplicateitemsineachcolumn%>"
                                ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvNoOptionsHotspot" runat="server" Display="none" ErrorMessage="<%$ Resources:LocalizedResource, Provideatleasttwochoicesandatleastonecorrectanswer%>"
                                ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvNoScoreEssay" runat="server" Display="none" ErrorMessage="<%$ Resources:LocalizedResource, Providethemaximumscorefortheessay%>"
                                ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvNoChoiceFillBlanks" runat="server" Display="none" ErrorMessage="<%$ Resources:LocalizedResource, Chooseanonblankcorrectanswer%>"
                                ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvNoSkipFillBlanks" runat="server" Display="none" ErrorMessage="<%$ Resources:LocalizedResource, Makesuretherearenoskippedspacesfortheanswers%>"
                                ValidationGroup="ValItems" />
                            <asp:CustomValidator ID="cvDuplicateFillBlanks" runat="server" Display="none" ErrorMessage="<%$ Resources:LocalizedResource, Makesuretherearenoduplicateanswers%>"
                                ValidationGroup="ValItems" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblItemTypes" runat="server" Text="<%$ Resources:LocalizedResource, ChooseTheItemType %>" />
                            <br />
                            <rad:RadComboBox ID="ddlItemtypes" runat="server" OnSelectedIndexChanged="ddlItemtypes_SelectedIndexChanged" DataTextField="Description" DataValueField="TypeCode" AutoPostBack="true"
                                EmptyMessage="Select a Question Type">
                            </rad:RadComboBox>
                            <%--<asp:DropDownList ID="ddlItemtypes" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlItemtypes_SelectedIndexChanged" >
                            </asp:DropDownList>--%>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Panel runat="server" ID="pnlQuestionTypeContainer" CssClass="container no-padding">
                                <asp:Panel ID="pnlMultipleChoice" runat="server" Visible="false" Width="100%">
                                    <table cellpadding="5" cellspacing="0">
                                        <tr>
                                            <td colspan="2">
                                                <asp:Label ID="lblInsertAnswers" runat="server" Text="<%$ Resources:LocalizedResource, EnterAnswersHereUpToAMaximumOf10ThenMarkTheCorrectAnswer %>" />
                                            </td>
                                        </tr>
                                        <tr id="trCheck1" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtChoice1" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chkChoice1" runat="server" Text="<%$ Resources:LocalizedResource, CorrectAnswers%>" />
                                            </td>
                                        </tr>
                                        <tr id="trCheck2" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtChoice2" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chkChoice2" runat="server" Text="<%$ Resources:LocalizedResource, CorrectAnswers%>" />
                                            </td>
                                        </tr>
                                        <tr id="trCheck3" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtChoice3" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chkChoice3" runat="server" Text="<%$ Resources:LocalizedResource, CorrectAnswers%>" />
                                            </td>
                                        </tr>
                                        <tr id="trCheck4" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtChoice4" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chkChoice4" runat="server" Text="<%$ Resources:LocalizedResource, CorrectAnswers%>" />
                                            </td>
                                        </tr>
                                        <tr id="trCheck5" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtChoice5" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chkChoice5" runat="server" Text="<%$ Resources:LocalizedResource, CorrectAnswers%>" />
                                            </td>
                                        </tr>
                                        <tr id="trCheck6" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtChoice6" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chkChoice6" runat="server" Text="<%$ Resources:LocalizedResource, CorrectAnswers%>" />
                                            </td>
                                        </tr>
                                        <tr id="trCheck7" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtChoice7" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chkChoice7" runat="server" Text="<%$ Resources:LocalizedResource, CorrectAnswers%>" />
                                            </td>
                                        </tr>
                                        <tr id="trCheck8" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtChoice8" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chkChoice8" runat="server" Text="<%$ Resources:LocalizedResource, CorrectAnswers%>" />
                                            </td>
                                        </tr>
                                        <tr id="trCheck9" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtChoice9" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chkChoice9" runat="server" Text="<%$ Resources:LocalizedResource, CorrectAnswers%>" />
                                            </td>
                                        </tr>
                                        <tr id="trCheck10" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtChoice10" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chkChoice10" runat="server" Text="<%$ Resources:LocalizedResource, CorrectAnswers%>" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlTrueOrFalse" runat="server" Visible="false" Width="100%">
                                    <table cellpadding="5">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblTrueOrFalseCorrect" runat="server" Text="<%$ Resources:LocalizedResource, PleaseMarkTheCorrectAnswer %>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RadioButton ID="rdoTrue" runat="server" GroupName="torf" Text="<%$ Resources:LocalizedResource, True %>" TextAlign="right" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RadioButton ID="rdoFalse" runat="server" GroupName="torf" Text="<%$ Resources:LocalizedResource, False %>" TextAlign="right" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlYesOrno" runat="server" Visible="false" Width="100%">
                                    <table cellpadding="5">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblYesOrNoCorrect" runat="server" Text="<%$ Resources:LocalizedResource, PleaseMarkTheCorrectAnswer %>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RadioButton ID="rdoYes" runat="server" GroupName="yorn" Text="<%$ Resources:LocalizedResource, Yes %>" TextAlign="right" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RadioButton ID="rdoNo" runat="server" GroupName="yorn" Text="<%$ Resources:LocalizedResource, No %>" TextAlign="right" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlSequencing" runat="server" Visible="false" Width="100%">
                                    <table cellpadding="5">
                                        <tr>
                                            <td colspan="2">
                                                <asp:Label ID="lblSequencing" runat="server" Text="<%$ Resources:LocalizedResource, EnterTheAnswersInTheOrderTheyWillAppearInTheTestInTheLeftColumn %>" />
                                                <asp:Label ID="lblSequencing1" runat="server" Text="<%$ Resources:LocalizedResource, EnterTheCorrectOrderInTheRightColumn %>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblRandomOrder" runat="server" Text="<%$ Resources:LocalizedResource, RandomizedOrder %>" />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblCorrectOrder" runat="server" Text="<%$ Resources:LocalizedResource, CorrectOrder %>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtItemTextbox1" runat="server" MaxLength="1000" Width="310px" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtAnswerTextbox1" runat="server" MaxLength="1000" Width="310px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtItemTextbox2" runat="server" MaxLength="1000" Width="310px" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtAnswerTextbox2" runat="server" MaxLength="1000" Width="310px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtItemTextbox3" runat="server" MaxLength="1000" Width="310px" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtAnswerTextbox3" runat="server" MaxLength="1000" Width="310px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtItemTextbox4" runat="server" MaxLength="1000" Width="310px" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtAnswerTextbox4" runat="server" Width="310px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtItemTextbox5" runat="server" MaxLength="1000" Width="310px" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtAnswerTextbox5" runat="server" MaxLength="1000" Width="310px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtItemTextbox6" runat="server" MaxLength="1000" Width="310px" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtAnswerTextbox6" runat="server" MaxLength="1000" Width="310px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtItemTextbox7" runat="server" MaxLength="1000" Width="310px" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtAnswerTextbox7" runat="server" MaxLength="1000" Width="310px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtItemTextbox8" runat="server" MaxLength="1000" Width="310px" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtAnswerTextbox8" runat="server" MaxLength="1000" Width="310px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtItemTextbox9" runat="server" MaxLength="1000" Width="310px" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtAnswerTextbox9" runat="server" MaxLength="1000" Width="310px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtItemTextbox10" runat="server" MaxLength="1000" Width="310px" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtAnswerTextbox10" runat="server" MaxLength="1000" Width="310px" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlFillInTheBlanks" runat="server" Visible="false" Width="100%">
                                    <table cellpadding="5">
                                        <tr>
                                            <td colspan="2">
                                                <asp:Label ID="Label1" runat="server" Text="<%$ Resources:LocalizedResource, EnterAnswersHereUpToAMaximumOf10ThenMarkTheCorrectAnswer %>" />
                                            </td>
                                        </tr>
                                        <tr id="tr1" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtFill1" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                            <td class="style1">
                                                <asp:RadioButton ID="rdoFill1" runat="server" GroupName="FillInTheBlanks" Text="<%$ Resources:LocalizedResource, CorrectAnswers%>" />
                                            </td>
                                        </tr>
                                        <tr id="tr2" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtFill2" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                            <td class="style1">
                                                <asp:RadioButton ID="rdoFill2" runat="server" GroupName="FillInTheBlanks" Text="<%$ Resources:LocalizedResource, CorrectAnswers%>" />
                                            </td>
                                        </tr>
                                        <tr id="tr3" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtFill3" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                            <td class="style1">
                                                <asp:RadioButton ID="rdoFill3" runat="server" GroupName="FillInTheBlanks" Text="<%$ Resources:LocalizedResource, CorrectAnswers%>" />
                                            </td>
                                        </tr>
                                        <tr id="tr4" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtFill4" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                            <td class="style1">
                                                <asp:RadioButton ID="rdoFill4" runat="server" GroupName="FillInTheBlanks" Text="<%$ Resources:LocalizedResource, CorrectAnswers%>" />
                                            </td>
                                        </tr>
                                        <tr id="tr5" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtFill5" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                            <td class="style2">
                                                <asp:RadioButton ID="rdoFill5" runat="server" GroupName="FillInTheBlanks" Text="<%$ Resources:LocalizedResource, CorrectAnswers%>" />
                                            </td>
                                        </tr>
                                        <tr id="tr6" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtFill6" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                            <td class="style1">
                                                <asp:RadioButton ID="rdoFill6" runat="server" GroupName="FillInTheBlanks" Text="<%$ Resources:LocalizedResource, CorrectAnswers%>" />
                                            </td>
                                        </tr>
                                        <tr id="tr7" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtFill7" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                            <td class="style1">
                                                <asp:RadioButton ID="rdoFill7" runat="server" GroupName="FillInTheBlanks" Text="<%$ Resources:LocalizedResource, CorrectAnswers%>" />
                                            </td>
                                        </tr>
                                        <tr id="tr8" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtFill8" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                            <td class="style1">
                                                <asp:RadioButton ID="rdoFill8" runat="server" GroupName="FillInTheBlanks" Text="<%$ Resources:LocalizedResource, CorrectAnswers%>" />
                                            </td>
                                        </tr>
                                        <tr id="tr9" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtFill9" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                            <td class="style1">
                                                <asp:RadioButton ID="rdoFill9" runat="server" GroupName="FillInTheBlanks" Text="<%$ Resources:LocalizedResource, CorrectAnswers%>" />
                                            </td>
                                        </tr>
                                        <tr id="tr10" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtFill10" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                            <td class="style1">
                                                <asp:RadioButton ID="rdoFill10" runat="server" GroupName="FillInTheBlanks" Text="<%$ Resources:LocalizedResource, CorrectAnswers%>" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlEssay" runat="server" Visible="false" Width="100%">
                                    <table cellpadding="5">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblEssayScore" runat="server" Text="<%$ Resources:LocalizedResource, IndicateTheMaximumScoreForThisEssay %>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtEssayScore" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlMatchingType" runat="server" Visible="false" Width="100%">
                                    <table cellpadding="5">
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label2" runat="server" Text="<%$ Resources:LocalizedResource, EnterTheItemsInTheOrderTheyWillAppearInTheTestInTheSecondColumnEnterTheListOfChoicesInTheRightColumnEnterTheCorrespondingLetterOfTheCorrectChoiceInTheLeftColumn %>" />
                                            </td>
                                        </tr>
                                    </table>
                                    <table cellpadding="5">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtCorrectMatch1" runat="server" MaxLength="1" Width="20px" />
                                                <ajax:FilteredTextBoxExtender ID="ftbCorrectMatch1" runat="server" FilterMode="validchars"
                                                    FilterType="Custom" TargetControlID="txtCorrectMatch1" ValidChars="ABCDEFGHIJabcdefghij" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtMatchItem1" runat="server" Width="250px" />
                                            </td>
                                            <td valign="middle">
                                                <asp:Label ID="lblMatchLetter1" runat="server" Text="A:" Width="20px" />
                                                <asp:TextBox ID="txtMatchChoice1" runat="server" Width="250px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtCorrectMatch2" runat="server" MaxLength="1" Width="20px" />
                                                <ajax:FilteredTextBoxExtender ID="ftbCorrectMatch2" runat="server" FilterMode="validchars"
                                                    FilterType="Custom" TargetControlID="txtCorrectMatch2" ValidChars="ABCDEFGHIJabcdefghij" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtMatchItem2" runat="server" Width="250px" />
                                            </td>
                                            <td valign="middle">
                                                <asp:Label ID="lblMatchLetter2" runat="server" Text="B:" Width="20px" />
                                                <asp:TextBox ID="txtMatchChoice2" runat="server" Width="250px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtCorrectMatch3" runat="server" MaxLength="1" Width="20px" />
                                                <ajax:FilteredTextBoxExtender ID="ftbCorrectMatch3" runat="server" FilterMode="validchars"
                                                    FilterType="Custom" TargetControlID="txtCorrectMatch3" ValidChars="ABCDEFGHIJabcdefghij" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtMatchItem3" runat="server" Width="250px" />
                                            </td>
                                            <td valign="middle">
                                                <asp:Label ID="lblMatchLetter3" runat="server" Text="C:" Width="20px" />
                                                <asp:TextBox ID="txtMatchChoice3" runat="server" Width="250px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtCorrectMatch4" runat="server" MaxLength="1" Width="20px" />
                                                <ajax:FilteredTextBoxExtender ID="ftbCorrectMatch4" runat="server" FilterMode="validchars"
                                                    FilterType="Custom" TargetControlID="txtCorrectMatch4" ValidChars="ABCDEFGHIJabcdefghij" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtMatchItem4" runat="server" Width="250px" />
                                            </td>
                                            <td valign="middle">
                                                <asp:Label ID="lblMatchLetter4" runat="server" Text="D:" Width="20px" />
                                                <asp:TextBox ID="txtMatchChoice4" runat="server" Width="250px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtCorrectMatch5" runat="server" MaxLength="1" Width="20px" />
                                                <ajax:FilteredTextBoxExtender ID="ftbCorrectMatch5" runat="server" FilterMode="validchars"
                                                    FilterType="Custom" TargetControlID="txtCorrectMatch5" ValidChars="ABCDEFGHIJabcdefghij" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtMatchItem5" runat="server" Width="250px" />
                                            </td>
                                            <td valign="middle">
                                                <asp:Label ID="lblMatchLetter5" runat="server" Text="E:" Width="20px" />
                                                <asp:TextBox ID="txtMatchChoice5" runat="server" Width="250px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtCorrectMatch6" runat="server" MaxLength="1" Width="20px" />
                                                <ajax:FilteredTextBoxExtender ID="ftbCorrectMatch6" runat="server" FilterMode="validchars"
                                                    FilterType="Custom" TargetControlID="txtCorrectMatch6" ValidChars="ABCDEFGHIJabcdefghij" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtMatchItem6" runat="server" Width="250px" />
                                            </td>
                                            <td valign="middle">
                                                <asp:Label ID="lblMatchLetter6" runat="server" Text="F:" Width="20px" />
                                                <asp:TextBox ID="txtMatchChoice6" runat="server" Width="250px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtCorrectMatch7" runat="server" MaxLength="1" Width="20px" />
                                                <ajax:FilteredTextBoxExtender ID="ftbCorrectMatch7" runat="server" FilterMode="validchars"
                                                    FilterType="Custom" TargetControlID="txtCorrectMatch7" ValidChars="ABCDEFGHIJabcdefghij" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtMatchItem7" runat="server" Width="250px" />
                                            </td>
                                            <td valign="middle">
                                                <asp:Label ID="lblMatchLetter7" runat="server" Text="G:" Width="20px" />
                                                <asp:TextBox ID="txtMatchChoice7" runat="server" Width="250px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtCorrectMatch8" runat="server" MaxLength="1" Width="20px" />
                                                <ajax:FilteredTextBoxExtender ID="ftbCorrectMatch8" runat="server" FilterMode="validchars"
                                                    FilterType="Custom" TargetControlID="txtCorrectMatch8" ValidChars="ABCDEFGHIJabcdefghij" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtMatchItem8" runat="server" Width="250px" />
                                            </td>
                                            <td valign="middle">
                                                <asp:Label ID="lblMatchLetter8" runat="server" Text="H:" Width="20px" />
                                                <asp:TextBox ID="txtMatchChoice8" runat="server" Width="250px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtCorrectMatch9" runat="server" MaxLength="1" Width="20px" />
                                                <ajax:FilteredTextBoxExtender ID="ftbCorrectMatch9" runat="server" FilterMode="validchars"
                                                    FilterType="Custom" TargetControlID="txtCorrectMatch9" ValidChars="ABCDEFGHIJabcdefghij" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtMatchItem9" runat="server" Width="250px" />
                                            </td>
                                            <td valign="middle">
                                                <asp:Label ID="lblMatchLetter9" runat="server" Text="I:" Width="20px" />
                                                <asp:TextBox ID="txtMatchChoice9" runat="server" Width="250px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtCorrectMatch10" runat="server" MaxLength="1" Width="20px" />
                                                <ajax:FilteredTextBoxExtender ID="ftbCorrectMatch10" runat="server" FilterMode="validchars"
                                                    FilterType="Custom" TargetControlID="txtCorrectMatch10" ValidChars="ABCDEFGHIJabcdefghij" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtMatchItem10" runat="server" Width="250px" />
                                            </td>
                                            <td valign="middle">
                                                <asp:Label ID="lblMatchLetter10" runat="server" Text="J:" Width="20px" />
                                                <asp:TextBox ID="txtMatchChoice10" runat="server" Width="250px" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlHotspot" runat="server" Visible="false" Width="100%">
                                    <table cellpadding="5" width="100%">
                                        <tr>
                                            <td colspan="5">
                                                <asp:Label ID="lblHotspot" runat="server" Text="<%$ Resources:LocalizedResource, Chooseuptotenimagesandmarkthecorrectanswersbyclickingonthem%>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="5">
                                                <asp:FileUpload ID="fupHotspot" runat="server" />
                                                <br />
                                                <asp:Button ID="btnAdd" runat="server" CssClass="buttons" OnClick="btnAdd_Click"
                                                    Text="<%$ Resources:LocalizedResource, Add%>" Width="60px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="5">
                                                <asp:CustomValidator ID="cvImageExists" runat="server" Display="dynamic" ErrorMessage="<%$ Resources:LocalizedResource, Filealreadyexists%>" />
                                                <asp:CustomValidator ID="cvTooMany" runat="server" Display="dynamic" ErrorMessage="<%$ Resources:LocalizedResource, Therearealreadytenimagesinthisquestion%>" />
                                                <asp:CustomValidator ID="cvNoImage" runat="server" Display="dynamic" ErrorMessage="<%$ Resources:LocalizedResource, BrowseforanimagebeforeclickingtheAddbutton%>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td id="imgCell1" runat="server" align="center" valign="middle" visible="false">
                                                <asp:ImageButton ID="imgChoice1" runat="server" OnClick="imgClick" />
                                                <br />
                                                <asp:Button ID="btnRemove1" runat="server" CssClass="buttons" OnClick="RemoveImage"
                                                    Text="<%$ Resources:LocalizedResource, Remove%>" Width="100px" />
                                            </td>
                                            <td id="imgCell2" runat="server" align="center" valign="middle" visible="false">
                                                <asp:ImageButton ID="imgChoice2" runat="server" OnClick="imgClick" />
                                                <br />
                                                <asp:Button ID="btnRemove2" runat="server" CssClass="buttons" OnClick="RemoveImage"
                                                    Text="<%$ Resources:LocalizedResource, Remove%>" Width="100px" />
                                            </td>
                                            <td id="imgCell3" runat="server" align="center" valign="middle" visible="false">
                                                <asp:ImageButton ID="imgChoice3" runat="server" OnClick="imgClick" />
                                                <br />
                                                <asp:Button ID="btnRemove3" runat="server" CssClass="buttons" OnClick="RemoveImage"
                                                    Text="<%$ Resources:LocalizedResource, Remove%>" Width="100px" />
                                            </td>
                                            <td id="imgCell4" runat="server" align="center" valign="middle" visible="false">
                                                <asp:ImageButton ID="imgChoice4" runat="server" OnClick="imgClick" />
                                                <br />
                                                <asp:Button ID="btnRemove4" runat="server" CssClass="buttons" OnClick="RemoveImage"
                                                    Text="<%$ Resources:LocalizedResource, Remove%>" Width="100px" />
                                            </td>
                                            <td id="imgCell5" runat="server" align="center" valign="middle" visible="false">
                                                <asp:ImageButton ID="imgChoice5" runat="server" OnClick="imgClick" />
                                                <br />
                                                <asp:Button ID="btnRemove5" runat="server" CssClass="buttons" OnClick="RemoveImage"
                                                    Text="<%$ Resources:LocalizedResource, Remove%>" Width="100px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td id="imgCell6" runat="server" align="center" valign="middle" visible="false">
                                                <asp:ImageButton ID="imgChoice6" runat="server" OnClick="imgClick" />
                                                <br />
                                                <asp:Button ID="btnRemove6" runat="server" CssClass="buttons" OnClick="RemoveImage"
                                                    Text="<%$ Resources:LocalizedResource, Remove%>" Width="100px" />
                                            </td>
                                            <td id="imgCell7" runat="server" align="center" valign="middle" visible="false">
                                                <asp:ImageButton ID="imgChoice7" runat="server" OnClick="imgClick" />
                                                <br />
                                                <asp:Button ID="btnRemove7" runat="server" CssClass="buttons" OnClick="RemoveImage"
                                                    Text="<%$ Resources:LocalizedResource, Remove%>" Width="100px" />
                                            </td>
                                            <td id="imgCell8" runat="server" align="center" valign="middle" visible="false">
                                                <asp:ImageButton ID="imgChoice8" runat="server" OnClick="imgClick" />
                                                <br />
                                                <asp:Button ID="btnRemove8" runat="server" CssClass="buttons" OnClick="RemoveImage"
                                                    Text="<%$ Resources:LocalizedResource, Remove%>" Width="100px" />
                                            </td>
                                            <td id="imgCell9" runat="server" align="center" valign="middle" visible="false">
                                                <asp:ImageButton ID="imgChoice9" runat="server" OnClick="imgClick" />
                                                <br />
                                                <asp:Button ID="btnRemove9" runat="server" CssClass="buttons" OnClick="RemoveImage"
                                                    Text="<%$ Resources:LocalizedResource, Remove%>" Width="100px" />
                                            </td>
                                            <td id="imgCell10" runat="server" align="center" valign="middle" visible="false">
                                                <asp:ImageButton ID="imgChoice10" runat="server" OnClick="imgClick" />
                                                <br />
                                                <asp:Button ID="btnRemove10" runat="server" CssClass="buttons" OnClick="RemoveImage"
                                                    Text="<%$ Resources:LocalizedResource, Remove%>" Width="100px" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <%--ADDED BY RAYMARK COSME <11/19/2015>--%>
                                <asp:Panel ID="pnlRate" runat="server" Visible="false" Width="100%">
                                    <table cellpadding="5">
                                        <tr>
                                            <td colspan="2">
                                                <asp:Label ID="Label7" runat="server" Text="<%$ Resources:LocalizedResource, EnterRatingHereUpToAMaximumOf10%>" />
                                            </td>
                                        </tr>
                                        <tr id="tr11" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtFillRate1" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                            <td class="style1">
                                                <asp:RadioButton ID="rdoRate1" runat="server" GroupName="Rate" Text="" />
                                            </td>
                                        </tr>
                                        <tr id="tr12" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtFillRate2" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                            <td class="style1">
                                                <asp:RadioButton ID="rdoRate2" runat="server" GroupName="Rate" Text="" />
                                            </td>
                                        </tr>
                                        <tr id="tr13" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtFillRate3" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                            <td class="style1">
                                                <asp:RadioButton ID="rdoRate3" runat="server" GroupName="Rate" Text="" />
                                            </td>
                                        </tr>
                                        <tr id="tr14" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtFillRate4" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                            <td class="style1">
                                                <asp:RadioButton ID="rdoRate4" runat="server" GroupName="Rate" Text="" />
                                            </td>
                                        </tr>
                                        <tr id="tr15" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtFillRate5" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                            <td class="style1">
                                                <asp:RadioButton ID="rdoRate5" runat="server" GroupName="Rate" Text="" />
                                            </td>
                                        </tr>
                                        <tr id="tr16" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtFillRate6" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                            <td class="style1">
                                                <asp:RadioButton ID="rdoRate6" runat="server" GroupName="Rate" Text="" />
                                            </td>
                                        </tr>
                                        <tr id="tr17" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtFillRate7" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                            <td class="style1">
                                                <asp:RadioButton ID="rdoRate7" runat="server" GroupName="Rate" Text="" />
                                            </td>
                                        </tr>
                                        <tr id="tr18" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtFillRate8" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                            <td class="style1">
                                                <asp:RadioButton ID="rdoRate8" runat="server" GroupName="Rate" Text="" />
                                            </td>
                                        </tr>
                                        <tr id="tr19" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtFillRate9" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                            <td class="style1">
                                                <asp:RadioButton ID="rdoRate9" runat="server" GroupName="Rate" Text="" />
                                            </td>
                                        </tr>
                                        <tr id="tr20" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtFillRate10" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                            <td class="style1">
                                                <asp:RadioButton ID="rdoRate10" runat="server" GroupName="Rate" Text="" />
                                            </td>
                                        </tr>
                                        <tr id="tr41" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtFillRate11" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                            <td class="style1">
                                                <asp:RadioButton ID="rdoRate11" runat="server" GroupName="Rate" Text="" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <%--ADDED BY RAYMARK COSME <11/19/2015>--%>
                                <asp:Panel ID="pnlComments" runat="server" Visible="false" Width="100%">
                                    <table cellpadding="5" width="100%">
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label6" runat="server" Text="<%$ Resources:LocalizedResource, Comments%>" />
                                                <table cellpadding="5" width="100%">
                                                    <tr>
                                                        <td>
                                                            <asp:TextBox ID="txtComments" runat="server" TextMode="MultiLine" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <%--ADDED BY RAYMARK COSME <11/19/2015>--%>
                                <asp:Panel ID="pnlDropDown" runat="server" Visible="false" Width="100%">
                                    <table cellpadding="5">
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label3" runat="server" Text="<%$ Resources:LocalizedResource, EnterValuesForDropDownHereUpToAMaximumOf10%>" />
                                            </td>
                                        </tr>
                                        <tr id="tr21" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtFillDD1" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                        </tr>
                                        <tr id="tr22" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtFillDD2" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                        </tr>
                                        <tr id="tr23" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtFillDD3" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                        </tr>
                                        <tr id="tr24" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtFillDD4" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                        </tr>
                                        <tr id="tr25" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtFillDD5" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                        </tr>
                                        <tr id="tr26" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtFillDD6" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                        </tr>
                                        <tr id="tr27" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtFillDD7" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                        </tr>
                                        <tr id="tr28" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtFillDD8" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                        </tr>
                                        <tr id="tr29" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtFillDD9" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                        </tr>
                                        <tr id="tr30" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtFillDD10" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <%--ADDED BY RAYMARK COSME <11/19/2015>--%>
                                <asp:Panel ID="pnlMultipleAnswer" runat="server" Visible="false" Width="100%">
                                    <table cellpadding="5" cellspacing="0">
                                        <tr>
                                            <td colspan="2">
                                                <asp:Label ID="Label4" runat="server" Text="<%$ Resources:LocalizedResource, Enterchoiceshereuptoamaximumof10%>" />
                                            </td>
                                        </tr>
                                        <tr id="tr31" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtMA1" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chkMA1" runat="server" Text="" />
                                            </td>
                                        </tr>
                                        <tr id="tr32" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtMA2" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chkMA2" runat="server" Text="" />
                                            </td>
                                        </tr>
                                        <tr id="tr33" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtMA3" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chkMA3" runat="server" Text="" />
                                            </td>
                                        </tr>
                                        <tr id="tr34" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtMA4" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chkMA4" runat="server" Text="" />
                                            </td>
                                        </tr>
                                        <tr id="tr35" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtMA5" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chkMA5" runat="server" Text="" />
                                            </td>
                                        </tr>
                                        <tr id="tr36" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtMA6" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chkMA6" runat="server" Text="" />
                                            </td>
                                        </tr>
                                        <tr id="tr37" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtMA7" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chkMA7" runat="server" Text="" />
                                            </td>
                                        </tr>
                                        <tr id="tr38" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtMA8" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chkMA8" runat="server" Text="" />
                                            </td>
                                        </tr>
                                        <tr id="tr39" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtMA9" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chkMA9" runat="server" Text="" />
                                            </td>
                                        </tr>
                                        <tr id="tr40" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtMA10" runat="server" MaxLength="1000" Width="350px" />
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chkMA10" runat="server" Text="" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <%--ADDED BY RAYMARK COSME <11/19/2015>--%>
                                <asp:Panel ID="pnlCalendar" runat="server" Visible="false" Width="100%">
                                    <table cellpadding="5" width="100%">
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label5" runat="server" Text="<%$ Resources:LocalizedResource, Calendar%>" />
                                                <table cellpadding="5" width="100%">
                                                    <tr>
                                                        <td>
                                                            <asp:TextBox ID="txtDate" runat="server" /><asp:ImageButton ID="imgCalendarStartDate"
                                                                runat="server" ImageUrl="~/Media/Images/icon-calendar.gif" />
                                                            <ajax:CalendarExtender ID="calStartDate" runat="server" TargetControlID="txtDate"
                                                                PopupButtonID="imgCalendarStartDate" Format="yyyy/MM/dd" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </asp:Panel>
                        </td>
                    </tr>
                    <%--ADDED BY JAY MILLARE <01/12/2015>--%>
                    <tr>
                        <td align="left" style="vertical-align: middle; font-size: 12px; font-family: Arial">
                            <asp:label runat="server" ID="UploadAReferencePictureHereIfApplicable" Text="<%$ Resources:LocalizedResource, UploadAReferencePictureHereIfApplicable %>">
                                </asp:label><br />
                            <%--<asp:UpdatePanel ID="updatePanel" runat="server" UpdateMode="Conditional">--%>
                            <%--<ContentTemplate>--%>
                            <asp:FileUpload ID="imgUpload" runat="server" />
                            <%--</ContentTemplate>--%>
                            <%--  <Triggers>
                                    <asp:PostBackTrigger ControlID="btnContinue" />
                                </Triggers>--%>
                            <%--</asp:UpdatePanel>--%>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center" valign="middle" style="font-family: Arial;">
                <asp:Button CssClass="buttons pull-left" ID="btnCancelAddQuestion" runat="server" Text="<%$ Resources:LocalizedResource, Return %>" OnClick="btnCancelAddQuestion_Click"  />
                <asp:Button CssClass="buttons pull-right" ID="btnContinueAddQuestion" runat="server" Text="<%$ Resources:LocalizedResource, Continue %>" OnClick="btnContinue_Click" />
            </td>
        </tr>
    </table>
</asp:Panel>

<rad:RadCodeBlock ID="RadCodeBlock1" runat="server">
<script type="text/javascript" language="javascript">
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
    function EndRequestHandler(sender, args) {
        if (args.get_error() != undefined) {
            args.set_errorHandled(true);
        }
    }
</script>
</rad:RadCodeBlock>
