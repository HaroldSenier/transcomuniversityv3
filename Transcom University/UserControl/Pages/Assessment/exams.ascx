﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="exams.ascx.cs" Inherits="UserControl_Pages_Assessment_exams" %>
<%@ Register TagPrefix="ucei" TagName="editexamitemcontrol" Src="~/UserControl/Pages/Assessment/editexamcontrol.ascx" %>
<%@ Register TagPrefix="ucst" TagName="editexamstatuscontrol" Src="~/UserControl/Pages/Assessment/examstatuscontrol.ascx" %>
<rad:RadListView ID="rgExams" runat="server" CssClass="GridLess" AllowPaging="false"
    PageSize="99" OnNeedDataSource="rgExams_NeedDataSource" Skin="Bootstrap" Width="100%"
    Font-Size="Small" RenderMode="Lightweight" ItemPlaceholderID="gridmcitem">
    
    <LayoutTemplate>
        <div id="mandatoryGridView">
            <table class="gridMainTable table table-bordered table-striped ">
                <thead>
                    <tr class="rlvHeader">
                        <th class="btn-teal">
                            <asp:Label runat="server" ID="theadExamName" Text="<%$ Resources:LocalizedResource, ExamName %>"></asp:Label>
                        </th>
                        <th class="btn-teal">
                            <asp:Label runat="server" ID="theadEndDate" Text="<%$ Resources:LocalizedResource, EndDate %>"></asp:Label>
                        </th>
                        <th class="btn-teal">
                            <asp:Label runat="server" ID="theadTimeLimit" Text="<%$ Resources:LocalizedResource, TimeLimit %>"></asp:Label>
                        </th>
                        <th class="btn-teal">
                            <asp:Label runat="server" ID="theadAttemptAllowed" Text="<%$ Resources:LocalizedResource, AttemptAllowed %>"></asp:Label>
                        </th>
                        <th class="btn-teal">
                            <asp:Label runat="server" ID="theadAction" Text="<%$ Resources:LocalizedResource, Action %>"></asp:Label>
                        </th>
                    </tr>
                </thead>
                <tbody>
                   <asp:PlaceHolder ID="gridmcitem" runat="server">
                   
                   </asp:PlaceHolder>                   
                </tbody>
                <tfoot>                    
                </tfoot>
            </table>

        </div>
    </LayoutTemplate>
    <ItemTemplate>
        <tr class="rlvI">
            <td>
                <asp:Label ID="Label2" runat="server"> <%# Eval("ExamName") %></asp:Label>
            </td>
            <td>
                <asp:Label ID="Label3" runat="server"> <%# Eval("EndDate")%></asp:Label>
            </td>
            <td>
                <asp:Label ID="Label4" runat="server"> <%# Eval("TimeLimit")%></asp:Label>
            </td>
            <td>
                <asp:Label ID="Label5" runat="server"> <%# Eval("TestLimit")%></asp:Label>
            </td>
            <td>
                <asp:LinkButton ID="lbtnEditExam" OnClientClick="showPageLoading();" runat="server" OnCommand="EditExam_Click" CommandName="EditCommand"
                        Text="<%$ Resources:LocalizedResource, Edit %>" CommandArgument='<%#Bind("TestCategoryID") %>' CssClass="btn btn-sm btn-flat btn-teal" />
            </td>  
        </tr>       
    </ItemTemplate>
    <EmptyDataTemplate>
        No Exams Added.
    </EmptyDataTemplate>
</rad:RadListView>
 
    <%--<rad:RadGrid ID="rgExams" RenderMode="Lightweight" OnNeedDataSource="rgExams_NeedDataSource" AllowPaging="True" Width="100%"
        runat="server" AutoGenerateColumns="False" AllowSorting="FALSE" PageSize="10" AllowMultiRowSelection="true">
    <MasterTableView AutoGenerateColumns="false" EnableNoRecordsTemplate="True" ShowHeadersWhenNoRecords="True" GridLines="None">              
        <Columns>        
            <rad:GridBoundColumn UniqueName="ExamName" HeaderText="Exam Name" DataField="ExamName">
            </rad:GridBoundColumn>
             <rad:GridBoundColumn UniqueName="TestCategoryID" HeaderText="TestCategoryID" DataField="TestCategoryID" Visible="false">
            </rad:GridBoundColumn>
            <rad:GridBoundColumn UniqueName="EndDate" HeaderText="End Date" DataField="EndDate">
            </rad:GridBoundColumn>
            <rad:GridBoundColumn UniqueName="TimeLimit" HeaderText="Time Limit" DataField="TimeLimit">
            </rad:GridBoundColumn>
            <rad:GridBoundColumn UniqueName="TestLimit" HeaderText="Attempt Allowed" DataField="TestLimit">
            </rad:GridBoundColumn>            
            <rad:GridTemplateColumn HeaderText="Action">
             <ItemTemplate>    
                    <asp:Button ID="lbtnEditExam" OnCommand="EditExam_Click" runat="server" CommandName="Edit" CommandArgument='<%#Bind("TestCategoryID") %>'
                        Text="Edit" CssClass="btn btn-sm btn-flat btn-teal"/> 
             </ItemTemplate>
            </rad:GridTemplateColumn>
        </Columns>
    </MasterTableView> 
</rad:RadGrid>--%>
<asp:LinkButton ID="lbtnAddNewExam" runat="server" CssClass="btn btn-md btn-flat btn-teal pull-right"
    OnClick="lbtnAddNewExam_Click" Text="<%$ Resources:LocalizedResource, AddNewItem %>" OnClientClick="showPageLoading();"/>
<asp:Button runat="server" ID="refreshPage" CssClass="display-none" OnClick="refreshPage_onclick"/>

    <rad:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true" RenderMode="Lightweight"
    Skin="Bootstrap" VisibleStatusbar="false">
</rad:RadWindowManager>

<rad:RadScriptBlock runat="server" ID="radscriptblock1">
<script type="text/javascript">

    function clientRefreshResourcesNoLoadingExam() {
        $("#<%= refreshPage.ClientID %>").click();
    }
</script>
</rad:RadScriptBlock>


    
