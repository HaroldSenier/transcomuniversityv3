﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class UserControl_Pages_Assessment_exams : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["IsNewExam"] = false;
    }



    protected void rgExams_NeedDataSource(object sender, EventArgs e)
    {
        int courseID = Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"]));
        rgExams.DataSource = DataHelper.allExamsByCourse(courseID);

       // ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", Utils.callClientScript("clientRefreshResourcesNoLoading"), true);
        
        
    }   

    protected void EditExam_Click(object sender, CommandEventArgs e)
    {
        int testCategoryID = Convert.ToInt32(e.CommandArgument);       

        SessionManager.SessionTestCategoryID = testCategoryID;
        //controls_examstatuscontrol testStatus = (controls_examstatuscontrol)this.NamingContainer.FindControl("testStatus");
        UserControl testStatus = (UserControl)this.NamingContainer.FindControl("testStatus");
        UserControl_Pages_Assessment_editexamcontrol assessmentBuilder = (UserControl_Pages_Assessment_editexamcontrol)this.NamingContainer.FindControl("assessmentBuilder");
        UserControl questionBuilder = (UserControl)this.NamingContainer.FindControl("questionBuilder");
        UserControl examsList = (UserControl)this.NamingContainer.FindControl("examsList");

        Session["IsNewExam"] = false;
        //testStatus.reloadExamStatusDetails();
        SessionManager.SessionExamAction = "editexam";
        assessmentBuilder.reloadDetails();
        assessmentBuilder.Visible = true;
        questionBuilder.Visible = false;
        testStatus.Visible = false;
        examsList.Visible = false;
    }

    protected void refreshPage_onclick(Object sender, EventArgs e) {
        int courseID = Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"]));
        rgExams.DataSource = DataHelper.allExamsByCourse(courseID);
        rgExams.DataBind();
    }

    protected void lbtnAddNewExam_Click(object sender, EventArgs e)
    {
        UserControl testStatus = (UserControl)this.NamingContainer.FindControl("testStatus");
        UserControl_Pages_Assessment_editexamcontrol assessmentBuilder = (UserControl_Pages_Assessment_editexamcontrol)this.NamingContainer.FindControl("assessmentBuilder");
        UserControl questionBuilder = (UserControl)this.NamingContainer.FindControl("questionBuilder");
        UserControl examsList = (UserControl)this.NamingContainer.FindControl("examsList");

        SessionManager.SessionTestCategoryID = 0;
        SessionManager.SessionTempCategory = null;
        SessionManager.SessionExamAction = "newexam";
        Session["IsNewExam"] = true;
        assessmentBuilder.initilizeCreateNewExam();
        assessmentBuilder.Visible = true;
        questionBuilder.Visible = false;
        testStatus.Visible = false;
        examsList.Visible = false;
    }

}