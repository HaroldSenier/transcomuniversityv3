﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AssessmentReports.ascx.cs" Inherits="UserControl_Pages_AssessmentReports" %>
<rad:RadScriptBlock ID="RadScriptBlock" runat="server">
    <script type="text/javascript">
        function pageLoad() {
            highlightSelectedSubmenu();
        }
    </script>
</rad:RadScriptBlock>
<rad:RadAjaxManagerProxy runat="server" ID="ramAssessment">
    <AjaxSettings>
        <rad:AjaxSetting AjaxControlID="rgAssessment">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlAssessmentContainer" LoadingPanelID="localLoadingPanel" />
            </UpdatedControls>
        </rad:AjaxSetting>
    </AjaxSettings>
</rad:RadAjaxManagerProxy>
<div class="col-md-12">
    <asp:Panel ID="pnlAssessmentContainer" CssClass="container" runat="server" style="overflow-x:scroll;">
        <rad:RadGrid RenderMode="Lightweight" ID="rgAssessment" Width="100%" AllowFilteringByColumn="True"
            DataSourceID="SqlDataSource1" AllowSorting="True" AllowPaging="True" PageSize="10"
            runat="server" AutoGenerateColumns="False" ShowStatusBar="true" EnableLinqExpressions="false" CssClass="GridLess">
            <ExportSettings HideStructureColumns="true" Excel-Format="ExcelML" ExportOnlyData="true" />
            <MasterTableView DataKeyNames="CIMNumber" TableLayout="Fixed" CommandItemDisplay="Top">
                <CommandItemSettings ShowExportToExcelButton="true" ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                <Columns>
                    <rad:GridBoundColumn UniqueName="CIMNumber" DataField="CIMNumber" HeaderText="<%$ Resources:LocalizedResource, CimNumber %>"
                        HeaderStyle-Width="200px" HeaderStyle-ForeColor="Teal">
                        <FilterTemplate>
                            <rad:RadComboBox RenderMode="Lightweight" ID="CIMNumber" DataSourceID="dsCIMNumber"
                                DataTextField="CIMNumber" DataValueField="CIMNumber" Width="180px" AppendDataBoundItems="true"
                                SelectedValue='<%# ((GridItem)Container).OwnerTableView.GetColumn("CIMNumber").CurrentFilterValue %>'
                                runat="server" OnClientSelectedIndexChanged="CIMNumberIndexChanged">
                                <Items>
                                    <rad:RadComboBoxItem Text="<%$ Resources:LocalizedResource, All %>" />
                                </Items>
                            </rad:RadComboBox>
                            <rad:RadScriptBlock ID="RadScriptBlock0" runat="server">
                                <script type="text/javascript">
                                    function CIMNumberIndexChanged(sender, args) {
                                        var tableView = $find("<%# ((GridItem)Container).OwnerTableView.ClientID %>");
                                        tableView.filter("CIMNumber", args.get_item().get_value(), "EqualTo");
                                    }
                                </script>
                            </rad:RadScriptBlock>
                        </FilterTemplate>
                    </rad:GridBoundColumn>
                    <rad:GridBoundColumn UniqueName="FirstName" DataField="FirstName" HeaderText="<%$ Resources:LocalizedResource, FirstName %>"
                        HeaderStyle-Width="200px" AllowFiltering="false" HeaderStyle-ForeColor="Teal">
                    </rad:GridBoundColumn>
                    <rad:GridBoundColumn UniqueName="LastName" DataField="LastName" HeaderText="<%$ Resources:LocalizedResource, LastName %>"
                        HeaderStyle-Width="200px" AllowFiltering="false" HeaderStyle-ForeColor="Teal">
                    </rad:GridBoundColumn>
                    <rad:GridBoundColumn UniqueName="Program" DataField="Program" HeaderText="<%$ Resources:LocalizedResource, Program %>"
                        HeaderStyle-Width="200px" HeaderStyle-ForeColor="Teal">
                        <FilterTemplate>
                            <rad:RadComboBox RenderMode="Lightweight" ID="Program" DataSourceID="dsProgram" DataTextField="Program"
                                DataValueField="Program" Width="180px" AppendDataBoundItems="true" SelectedValue='<%# ((GridItem)Container).OwnerTableView.GetColumn("Program").CurrentFilterValue %>'
                                runat="server" OnClientSelectedIndexChanged="ProgramIndexChanged">
                                <Items>
                                    <rad:RadComboBoxItem Text="<%$ Resources:LocalizedResource, All %>" />
                                </Items>
                            </rad:RadComboBox>
                            <rad:RadScriptBlock ID="RadScriptBlock1" runat="server">
                                <script type="text/javascript">
                                    function ProgramIndexChanged(sender, args) {
                                        var tableView = $find("<%# ((GridItem)Container).OwnerTableView.ClientID %>");
                                        tableView.filter("Program", args.get_item().get_value(), "EqualTo");
                                    }
                                </script>
                            </rad:RadScriptBlock>
                        </FilterTemplate>
                    </rad:GridBoundColumn>
                    <rad:GridBoundColumn UniqueName="ReportsTo" DataField="ReportsTo" HeaderText="<%$ Resources:LocalizedResource, ReportsTo %>"
                        HeaderStyle-Width="200px" HeaderStyle-ForeColor="Teal">
                        <FilterTemplate>
                            <rad:RadComboBox RenderMode="Lightweight" ID="ReportsTo" DataSourceID="dsReportsTo"
                                DataTextField="ReportsTo" DataValueField="ReportsTo" Width="180px" AppendDataBoundItems="true"
                                SelectedValue='<%# ((GridItem)Container).OwnerTableView.GetColumn("ReportsTo").CurrentFilterValue %>'
                                runat="server" OnClientSelectedIndexChanged="ReportsToIndexChanged">
                                <Items>
                                    <rad:RadComboBoxItem Text="<%$ Resources:LocalizedResource, All %>" />
                                </Items>
                            </rad:RadComboBox>
                            <rad:RadScriptBlock ID="RadScriptBlock2" runat="server">
                                <script type="text/javascript">
                                    function ReportsToIndexChanged(sender, args) {
                                        var tableView = $find("<%# ((GridItem)Container).OwnerTableView.ClientID %>");
                                        tableView.filter("ReportsTo", args.get_item().get_value(), "EqualTo");
                                    }
                                </script>
                            </rad:RadScriptBlock>
                        </FilterTemplate>
                    </rad:GridBoundColumn>
                    <rad:GridBoundColumn UniqueName="CourseName" DataField="CourseName" HeaderText="<%$ Resources:LocalizedResource, CourseName %>"
                        HeaderStyle-Width="200px" HeaderStyle-ForeColor="Teal">
                        <FilterTemplate>
                            <rad:RadComboBox RenderMode="Lightweight" ID="CourseName" DataSourceID="dsCourseName"
                                DataTextField="Title" DataValueField="Title" Width="180px" AppendDataBoundItems="true"
                                SelectedValue='<%# ((GridItem)Container).OwnerTableView.GetColumn("CourseName").CurrentFilterValue %>'
                                runat="server" OnClientSelectedIndexChanged="CourseNameIndexChanged">
                                <Items>
                                    <rad:RadComboBoxItem Text="<%$ Resources:LocalizedResource, All %>" />
                                </Items>
                            </rad:RadComboBox>
                            <rad:RadScriptBlock ID="RadScriptBlock3" runat="server">
                                <script type="text/javascript">
                                    function CourseNameIndexChanged(sender, args) {
                                        var tableView = $find("<%# ((GridItem)Container).OwnerTableView.ClientID %>");
                                        tableView.filter("CourseName", args.get_item().get_value(), "EqualTo");
                                    }
                                </script>
                            </rad:RadScriptBlock>
                        </FilterTemplate>
                    </rad:GridBoundColumn>
                    <rad:GridDateTimeColumn FilterControlWidth="135px" DataField="DateTestTaken" HeaderText="<%$ Resources:LocalizedResource, DateTestTaken %>"
                        PickerType="DatePicker" EnableRangeFiltering="true" HeaderStyle-Width="400px" HeaderStyle-ForeColor="Teal">
                    </rad:GridDateTimeColumn>
                    <rad:GridBoundColumn UniqueName="Score" DataField="Score" HeaderText="<%$ Resources:LocalizedResource, Score %>"
                        HeaderStyle-Width="200px" AllowFiltering="false" HeaderStyle-ForeColor="Teal">
                    </rad:GridBoundColumn>
                </Columns>
            </MasterTableView>
        </rad:RadGrid>
    </asp:Panel>
</div>
<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
    ProviderName="System.Data.SqlClient" SelectCommand="pr_TranscomUniversity_Rpt_Assessment" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>
<asp:SqlDataSource ID="dsCIMNumber" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
    ProviderName="System.Data.SqlClient" SelectCommand="SELECT DISTINCT CIMNumber FROM vw_TranscomUniversity_Rpt_Param">
</asp:SqlDataSource>
<asp:SqlDataSource ID="dsProgram" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
    ProviderName="System.Data.SqlClient" SelectCommand="SELECT DISTINCT Program FROM vw_TranscomUniversity_Rpt_Param">
</asp:SqlDataSource>
<asp:SqlDataSource ID="dsReportsTo" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
    ProviderName="System.Data.SqlClient" SelectCommand="SELECT DISTINCT ReportsTo FROM vw_TranscomUniversity_Rpt_Param">
</asp:SqlDataSource>
<asp:SqlDataSource ID="dsCourseName" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
    ProviderName="System.Data.SqlClient" SelectCommand="SELECT DISTINCT Title FROM vw_TranscomUniversity_Rpt_Param">
</asp:SqlDataSource>