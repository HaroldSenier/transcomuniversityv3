﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BadgeUserCtrl.ascx.cs"
    Inherits="UserControl_Pages_BadgeUserCtrl" %>
<asp:Panel ID="pnlForm1" runat="server">
    <div class="row" id="folder_holder">
        <div class="col-md-11">
            <span class="pull-right">
                <table>
                    <tr>
                        <%--OnClientClick="btnSwitchView_Click(); return false;"--%>
                        <td valign="middle" style="padding-left: 15px; padding-right: 15px;">
                            <asp:LinkButton ID="btnSwitchToBadgeListView" runat="server" OnClick="btnSwitchListView_OnClick"
                                Font-Underline="false" aria-hidden="true" ToolTip="Switch to Grid View" Visible="false">
                                <i id="switchClassListView" runat="server" class="fa fa-th  pull-right"></i>
                            </asp:LinkButton>
                            <asp:LinkButton ID="btnSwitchToBadgeGridView" runat="server" OnClick="btnSwitchGridView_OnClick"
                                Font-Underline="false" aria-hidden="true" Visible="true" ToolTip="Switch to List View">
                                <i id="switchClassGridView" runat="server" class="fa fa-th-list pull-right"></i>
                            </asp:LinkButton>
                        </td>
                        <td valign="middle" style="padding-left: 15px; padding-right: 15px;">
                            <asp:LinkButton ID="btnBadgeLib" runat="server" Font-Underline="false" ForeColor="Black"
                                OnClientClick="showMenu(event);">
                                <b style="color: #636363;"><i id="iBadgeLib" runat="server" class="fa fa-pencil-square-o"
                                    style="margin-right: 15px" aria-hidden="true" title="Settings"></i></b>
                            </asp:LinkButton>
                        </td>
                        <td valign="middle">
                            <asp:LinkButton ID="lbtnAddFolder" runat="server" OnClientClick="showMenu1(event);">
                                <asp:Image ID="imgAddFolder" ImageUrl="~/Media/Images/add-folder-icon.png" runat="server"
                                    Width="15px" />
                            </asp:LinkButton>
                        </td>
                        <td valign="middle" style="padding-left: 15px;">
                            <asp:LinkButton ID="LinkBtnChanger3" runat="server" OnClientClick="deleteFolder(); return false;">
                    <b style="font-size: larger; color: #636363;"><i class="fa fa-trash-o"></i></b> 
                            </asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </span>
        </div>
        <div class="col-md-11">
            <h5>
                <asp:Label ID="BadgeMsg" runat="server" Style="color: #09A8A4;">
                </asp:Label></h5>
            <asp:HiddenField runat="server" ID="hidFileType" Value="0" />
            <asp:LinkButton ID="lbtBUCnBack" OnClick="LinkBtnBack_Click" Visible="false" runat="server"
                Style="z-index: 999999;"><h3><i class="fa fa-arrow-circle-left fa-large"></i></h3></asp:LinkButton>
            <asp:Panel runat="server" ID="pnlMainBadge">
                <asp:Panel runat="server" ID="pnlLvResult" CssClass="LearningPlan-view margin-bottom-10px"
                    Visible="true">
                    <rad:RadListView ID="RadLV" runat="server" GroupItemCount="3" OnItemCommand="RadLV_OnItemCommande">
                        <ItemTemplate>
                            <table style="display: inline-block;">
                                <asp:HiddenField ID="radLVHiddenField" runat="server" Value='<%# Eval("ID")%>'></asp:HiddenField>
                                <asp:HiddenField ID="radLvHidFolder" runat="server" Value='<%# Eval("Fname")%>'>
                                </asp:HiddenField>
                                <asp:HiddenField ID="radBName" runat="server" Value='<%# Eval("BName")%>'></asp:HiddenField>
                                <tr>
                                    <td align="center" style="padding: 10px;">
                                        <a id="hlLvUpdateBadge" onclick='SelectFolder(this);'>
                                            <div id="divCatFolder" class="rlvCourseCat" title='<%#Eval("BName")%>'>
                                                <asp:Panel runat="server" ID="pnlImage">
                                                    <div class="photo-container-coursecat badge-image image-selected">
                                                        <asp:Image ID="Image2" runat="server" ImageUrl='<%# "~/Media/Uploads/" + Eval("IconImg")%>'
                                                            Width="150px" Height="100px" />
                                                    </div>
                                                    <div class="category font-bold" align="center">
                                                        <%#Eval("BName")%>
                                                    </div>
                                                </asp:Panel>
                                            </div>
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                        <ClientSettings >
                             <ClientEvents OnItemSelected="courseRowSelected" OnItemDeselected="courseRowDeselected"/>
                        </ClientSettings>
                    </rad:RadListView>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlGridResult" CssClass="LearningPlan-view margin-bottom-10px"
                    Visible="False">
                    <rad:RadGrid ID="RadGV" runat="server" Skin="Bootstrap" CellSpacing="0" GridLines="None"
                        RenderMode="Lightweight" OnNeedDataSource="RadGV_NeedDataSource" OnItemCommand="RadGV_OnItemCommande">
                        <ClientSettings EnableAlternatingItems="true">
                            <Selecting AllowRowSelect="true"></Selecting>
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            <Resizing AllowColumnResize="true" ResizeGridOnColumnResize="true" AllowResizeToFit="true" />
                            <ClientEvents OnRowSelected="courseRowSelected" OnRowDeselected="courseRowDeselected"
                                OnRowCreated="courseRowCreated" />
                        </ClientSettings>
                        <MasterTableView DataKeyNames="ID,Fname,BName" AllowSorting="true" AllowPaging="true"
                            AutoGenerateColumns="false" CommandItemDisplay="None" HeaderStyle-ForeColor="Teal"
                            ClientDataKeyNames="ID,Fname,BName">
                            <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                            <Columns>
                                <rad:GridTemplateColumn HeaderText="#" ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <%# Container.ItemIndex + 1 %>
                                    </ItemTemplate>
                                </rad:GridTemplateColumn>
                                <rad:GridTemplateColumn UniqueName="hidFields" Visible="false">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="radGVHiddenField" runat="server" Value='<%# Eval("ID")%>'></asp:HiddenField>
                                        <asp:HiddenField ID="radGVHidFolder" runat="server" Value='<%# Eval("Fname")%>'>
                                        </asp:HiddenField>
                                        <asp:HiddenField ID="radBName" runat="server" Value='<%# Eval("BName")%>'></asp:HiddenField>
                                    </ItemTemplate>
                                </rad:GridTemplateColumn>
                                <rad:GridTemplateColumn UniqueName="Bname" HeaderText="<%$ Resources:LocalizedResource, Name %>" DataField="Bname"
                                    ItemStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="hlGvCourseCategory" runat="server" BackColor="Transparent" BorderWidth="0px"
                                            BorderColor="Transparent">
                                                                            <%# Eval("BName")%>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </rad:GridTemplateColumn>
                                <rad:GridBoundColumn ItemStyle-Font-Size="Small" UniqueName="ContentCtr" HeaderText="CONTENTS"
                                    DataField="ContentCtr" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="Small">
                                </rad:GridBoundColumn>
                                <rad:GridBoundColumn ItemStyle-Font-Size="Small" UniqueName="UpdatedBy" HeaderText="<%$ Resources:LocalizedResource, UpdatedBy %>"
                                    DataField="UpdatedBy" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="Small">
                                </rad:GridBoundColumn>
                                <rad:GridDateTimeColumn DataField="Updated" DataType="System.DateTime" DataFormatString="{0:dd MMMM yyyy hh:mm tt}"
                                    HeaderText="Updated" UniqueName="Updated">
                                </rad:GridDateTimeColumn>
                            </Columns>
                        </MasterTableView>
                    </rad:RadGrid>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="pnlBadge2nd" runat="server" Visible="false">
                <rad:RadListView ID="rlvBadgeInner" runat="server" GroupItemCount="3">
                    <ItemTemplate>
                        <table style="display: inline-block;">
                            <asp:Label ID="lblInner" runat="server" Text='<%# Eval("ID")%>' Visible="false"></asp:Label>
                            <tr>
                                <td align="center" style="padding: 10px;">
                                    <asp:LinkButton ID="hlLvUpdateBadge" runat="server" ForeColor="Black">
                                        <div id="divCatFolder" class="rlvCourseCat" title='<%#Eval("BadgeName")%>'>
                                            <div class="photo-container-coursecat">
                                                <asp:Image ID="Image2" runat="server" ImageUrl='<%# "~/Media/Uploads/Badges/" + Eval("BadgeImgBig")%>'
                                                    Width="170px" Height="130px" />
                                            </div>
                                            <div class="category font-bold" align="center">
                                                <%#Eval("BadgeName")%>
                                            </div>
                                        </div>
                                    </asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </rad:RadListView>
            </asp:Panel>
        </div>
    </div>
    <rad:RadWindowManager ID="RadWindowManager2" RenderMode="Lightweight" EnableShadow="true"
        Skin="Bootstrap" Modal="true" VisibleOnPageLoad="false" Behaviors="Close, Move"
        DestroyOnClose="true" RestrictionZoneID="RestrictionZone" Opacity="99" runat="server"
        Width="800" Height="400px">
        <Windows>
            <rad:RadWindow ID="rwAddFolder" runat="server" RenderMode="Lightweight" Modal="true" 
                Skin="Bootstrap" Behaviors="Move, Close" CssClass="tc-radwindow-1 height-inherit tc-radwindow-teal"
                Height="200px" Width="450px" VisibleStatusbar="false" Title="New Folder">
                <ContentTemplate>
                    <center>
                        <div>
                            <br />
                            <asp:Panel ID="pnlAddFolder" runat="server" DefaultButton="btnSubmit">
                                <table>
                                    <tr>
                                        <rad:RadTextBox ID="rtbAddFolder" runat="server" Width="100%" CssClass="width-fill"  onkeyup="CheckMaxLength(this,50,'Add');"
                                            EmptyMessage="New Folder" MaxLength="50" ondragstart="return false;" ondrop="return false;">
                                        </rad:RadTextBox>
                                          <label class="text-danger text-right">
                                        <span runat="server" id="txtAddChars">50</span> <asp:Label runat="server" ID="Charremain" Text= "<%$ Resources:LocalizedResource, CharactersRemaining %>"></asp:Label></label> 
                                        <%--<asp:Image ID="Image1" ImageUrl='~/Media/Images/CourseCat.png' runat="server"
                                    CssClass="img-responsive" Width="150" />--%>
                                        <br />
                                        <br />
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="right" style="padding: 10px;">                                       
                                            <rad:RadButton ID="btnSubmit" runat="server" Text="Create" OnClick="btnSubmit_Click" OnClientClicking = "checkingTextboxEmptyAdd" Skin="Bootstrap" RenderMode="Lightweight"
                                               CssClass="btn btn-teal font-white no-border-radius margin-right-10" Width="150px"  />
                                        </td>
                                        <td colspan="2" align="right" style="padding: 10px;">
                                        <rad:RadButton ID="btnCancel" runat="server" Text="Cancel" OnClientClicking = "closePopup" Skin="Bootstrap" RenderMode="Lightweight"
                                               CssClass="btn btn-teal font-white no-border-radius margin-right-10" Width="150px" AutoPostBack="false" />
                                            
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <br />
                        </div>
                    </center>
                </ContentTemplate>
            </rad:RadWindow>
        </Windows>
        <Windows>
            <rad:RadWindow ID="rwEditFolder" runat="server" RenderMode="Lightweight" Modal="true" OnClientShow="setPopupTitle"
                Skin="Bootstrap" Behaviors="Move, Close" CssClass="tc-radwindow-1 height-inherit tc-radwindow-teal"
                Height="200px" Width="450px" VisibleStatusbar="false" Title="Rename Folder">
                <ContentTemplate>
                    <center>
                        <div>
                            <br />
                            <asp:Panel ID="Panel1" runat="server" DefaultButton="btnEditSubmit">
                                <table>
                                    <tr>
                                        <rad:RadTextBox ID="rtbEditFolder" runat="server" Width="100%" CssClass="width-fill" onkeyup="CheckMaxLength(this,50,'Edit');"
                                            EmptyMessage="Rename Folder" MaxLength="50" ondragstart="return false;" ondrop="return false;">
                                        </rad:RadTextBox>
                                           <label class="text-danger text-right">
                                        <span runat="server" id="txtEditChars">50</span> <asp:Label runat="server" ID="Charmain" Text= "<%$ Resources:LocalizedResource, CharactersRemaining %>"></asp:Label></label> 
                                        <%--<asp:Image ID="Image1" ImageUrl='~/Media/Images/CourseCat.png' runat="server"
                                    CssClass="img-responsive" Width="150" />--%>
                                        <br />
                                        <br />
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="right" style="padding: 10px;"> 
                                            <%--<asp:Button ID="btnEditSubmit" runat="server" Text="Edit" OnClick="btnEditSubmit_Click" OnClientClick = "checkingTextboxEmpty('Edit')" 
                                                class="btn btn-teal font-white no-border-radius margin-right-10" Style="width: 150px;" />--%>
                                            <rad:RadButton ID="btnEditSubmit" runat="server" Text="Edit" OnClick="btnEditSubmit_Click" OnClientClicking = "checkingTextboxEmpty" Skin="Bootstrap" RenderMode="Lightweight"
                                                CssClass="btn btn-md btn-teal font-white no-border-radius margin-right-10" Width="150px"/>
                                        </td>
                                        <td colspan="2" align="right" style="padding: 10px;">
                                             <rad:RadButton ID="btnCancelEdit" runat="server" Text="Cancel" OnClientClicking = "closePopup" Skin="Bootstrap" RenderMode="Lightweight"
                                               CssClass="btn btn-teal font-white no-border-radius margin-right-10" Width="150px" AutoPostBack="false" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <br />
                        </div>
                    </center>
                </ContentTemplate>
            </rad:RadWindow>
        </Windows>
        <Windows>
            <rad:RadWindow ID="rwDeleteFolder" runat="server" RenderMode="Lightweight" Width="330px" Height="180px"
                Title="Delete Folder" Behaviors="Close" Modal="true" VisibleStatusbar="false" OnClientShow="setPopupTitleDelete">
                <ContentTemplate>
                    <asp:Panel ID="Panel2" runat="server" DefaultButton="btnDeleteFolderOk">
                        <div class="col-md-6">
                         <asp:Label runat="server" ID="AreYouSureYouWantToDelete" Text= "<%$ Resources:LocalizedResource, AreYouSureYouWantToDelete %>"></asp:Label>
                        </div>
                        <br />
                        <br />
                        <br />
                        <div class="form-group">
                            <asp:Button ID="btnDeleteFolderOk" runat="server" CssClass="btn btn-default" Text=" <%$ Resources:LocalizedResource,Delete %>"
                                OnClick="btnDeleteFolderOk_Click" />
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </rad:RadWindow>
        </Windows>
    </rad:RadWindowManager>
    <rad:RadContextMenu ID="rcmBadgesCertificate" runat="server" RenderMode="Lightweight"
        OnItemClick="rcmBadgesCertificate_Click" EnableShadows="true" CssClass="RadContextMenu1"
        EnableScreenBoundaryDetection="false">
        <Items>
            <rad:RadMenuItem Text="Organize" Value="5" />
            <rad:RadMenuItem Text="Open" Value="2" />
            <rad:RadMenuItem Text="Rename" Value="3" />
        </Items>
    </rad:RadContextMenu>
    <rad:RadContextMenu ID="rcmBadgesCertificateAdd" runat="server" RenderMode="Lightweight"
        OnClientItemClicked="rcmBadgesCertificateAdd_Click" EnableShadows="true" CssClass="RadContextMenu1"
        EnableScreenBoundaryDetection="false">
        <Items>
            <rad:RadMenuItem Text="Folder" Value="Folder" />
            <%-- <rad:RadMenuItem Text="File" Value="File" />  --%>
        </Items>
    </rad:RadContextMenu>
    <div class="display-none">
        <rad:RadAsyncUpload ID="radAddFile" runat="server" MultipleFileSelection="Automatic"
            ViewStateMode="Enabled" ControlObjectsVisibility="None" OnClientFilesUploaded="fileUploaded"
            Width="100%" HideFileInput="false" ToolTip="Select Item to Uploade" CssClass="CourseImage display-none"
            ForeColor="#ddd" OnFileUploaded="upload_FileUploaded" AllowedFileExtensions="jpeg,jpg,gif,png">
        </rad:RadAsyncUpload>
    </div>
    <div>
        <asp:HiddenField ID="hidID" runat="server" Value="" />
        <asp:HiddenField ID="hidFolder" runat="server" />
        <asp:HiddenField ID="hidSelectedFolderName" runat="server" />
        <asp:HiddenField ID="hidImgID" runat="server" />
    </div>
</asp:Panel>
<rad:RadAjaxLoadingPanel ID="localLoadingPanel" runat="server" CssClass="Loading2"
    Transparency="25" />
<rad:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <rad:AjaxSetting AjaxControlID="btnSwitchToBadgeListView">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlForm1" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnSwitchToBadgeGridView">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlForm1" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="LinkBtnChanger1">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="LinkBtnChanger1" />
                <rad:AjaxUpdatedControl ControlID="RadLV" />
                <rad:AjaxUpdatedControl ControlID="RadGV" />
                <rad:AjaxUpdatedControl ControlID="LinkButton1" />
                <rad:AjaxUpdatedControl ControlID="rlvBadgeInner" />
                <rad:AjaxUpdatedControl ControlID="SettingsCBUserCtrl1" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnUploadBadge">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="RadLV" />
                <rad:AjaxUpdatedControl ControlID="RadGV" />
                <rad:AjaxUpdatedControl ControlID="btnUploadBadge" />
                <rad:AjaxUpdatedControl ControlID="rlvBadgeInner" />
                <rad:AjaxUpdatedControl ControlID="BadgeUserCtrl1" />
                <rad:AjaxUpdatedControl ControlID="SettingsCBUserCtrl1" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="rlvBadgeInner">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="RadLV" />
                <rad:AjaxUpdatedControl ControlID="RadGV" />
                <rad:AjaxUpdatedControl ControlID="rlvBadgeInner" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnEditSubmit">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="btnEditSubmit" />
                <rad:AjaxUpdatedControl ControlID="RadLV" />
                <rad:AjaxUpdatedControl ControlID="RadGV" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="lbtBUCnBack">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="lbtBUCnBack" />
                <rad:AjaxUpdatedControl ControlID="RadLV" />
                <rad:AjaxUpdatedControl ControlID="RadGV" />
                <rad:AjaxUpdatedControl ControlID="rlvBadgeInner" />
                <rad:AjaxUpdatedControl ControlID="BadgeUserCtrl1" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnSubmit">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="btnSubmit" />
                <rad:AjaxUpdatedControl ControlID="RadGV" />
                <rad:AjaxUpdatedControl ControlID="RadLV" />
                 <rad:AjaxUpdatedControl ControlID="rtbAddFolder" />
                
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnDeleteFolderOk">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="btnDeleteFolderOk" />
                <rad:AjaxUpdatedControl ControlID="RadGV" />
                <rad:AjaxUpdatedControl ControlID="RadLV" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="RadGV">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="RadGV" />
                <rad:AjaxUpdatedControl ControlID="RadWindowManager2" />
                <rad:AjaxUpdatedControl ControlID="rlvBadgeInner" />
                <rad:AjaxUpdatedControl ControlID="pnlBadge2nd" />
                <rad:AjaxUpdatedControl ControlID="BadgeUserCtrl1" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="RadLV">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="RadLV" />
                <rad:AjaxUpdatedControl ControlID="RadWindowManager2" />
                <rad:AjaxUpdatedControl ControlID="rlvBadgeInner" />
                <rad:AjaxUpdatedControl ControlID="BadgeUserCtrl1" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="RadWindowManager2">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="RadLV" />
                <rad:AjaxUpdatedControl ControlID="RadGV" />
                <rad:AjaxUpdatedControl ControlID="rlvBadgeInner" />
                <rad:AjaxUpdatedControl ControlID="RadWindowManager2" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="rcmBadgesCertificate">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="RadGV" />
                <rad:AjaxUpdatedControl ControlID="RadLV" />
                <rad:AjaxUpdatedControl ControlID="RadWindowManager2" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="pnlGridResult">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="RadLV" />
                <rad:AjaxUpdatedControl ControlID="RadGV" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="pnlLvResult">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="RadLV" />
                <rad:AjaxUpdatedControl ControlID="RadGV" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="RadAjaxManager1">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="radAddFile" LoadingPanelID="noLoadingPanel" />
                <rad:AjaxUpdatedControl ControlID="RadLV" LoadingPanelID="noLoadingPanel" />
                <rad:AjaxUpdatedControl ControlID="RadLV" LoadingPanelID="noLoadingPanel" />
                <rad:AjaxUpdatedControl ControlID="rlvBadgeInner" LoadingPanelID="noLoadingPanel" />
            </UpdatedControls>
        </rad:AjaxSetting>
    </AjaxSettings>
</rad:RadAjaxManagerProxy>
<rad:RadScriptBlock runat="server" ID="rcbBadgesAndCert">
    <script type="text/javascript">
        //$ = $telerik.$;
        //        function btnSwitchView_Click() {
        //            debugger;    
        //            if ($('#<%= pnlGridResult.ClientID %>').hasClass("display-none")) {
        //                $("#<%= pnlLvResult.ClientID %>").addClass("display-none");
        //                $("#<%= pnlGridResult.ClientID %>").removeClass("display-none");
        //                $("#<%= btnSwitchToBadgeGridView.ClientID %>").removeClass("display-none");
        //                $("#<%= btnSwitchToBadgeListView.ClientID %>").addClass("display-none");
        //                //switch to gridview

        //                console.log("to list");
        //            } else {
        //                //switch to list view
        //                $("#<%= pnlLvResult.ClientID %>").removeClass("display-none");
        //                $("#<%= pnlGridResult.ClientID %>").addClass("display-none");
        //                $("#<%= btnSwitchToBadgeListView.ClientID %>").removeClass("display-none");
        //                $("#<%= btnSwitchToBadgeGridView.ClientID %>").addClass("display-none");
        //                console.log("to Grid");
        //            }
        //            return false;
        //        }

        function setPopupTitle(sender) {
            debugger;
            var folder = $('#<%=hidFolder.ClientID %>').val();
            var txtBoxEdit = $find('<%=rtbEditFolder.ClientID %>');
            if (folder == 'FOLDER') {
                sender.set_title("Edit Folder Name");
                txtBoxEdit.set_emptyMessage("Folder Name");
            }
            else {
                sender.set_title("Edit Image File Name");
                txtBoxEdit.set_emptyMessage("File Name");
            }
        }

        function setPopupTitleDelete(sender) {
            debugger;
            var folder = $('#<%=hidFolder.ClientID %>').val();
            if (folder == 'FOLDER')
                sender.set_title("Delete Folder ");
            else
                sender.set_title("Delete Image File");
        }

        function checkingTextboxEmpty(sender, args) { 
                var radtxtbox = $find('<%=rtbEditFolder.ClientID %>');            
            if (radtxtbox.get_value() == "") {
                radalert("Name is required.", 330, 180, "Error", "");
                 args.set_cancel(true);
            }           

        }

        function checkingTextboxEmptyAdd(sender, args) { 
        var radtxtbox = $find('<%=rtbAddFolder.ClientID %>');
            if (radtxtbox.get_value() == "") {
                radalert("Name is required.", 330, 180, "Error", "");
                 args.set_cancel(true);
            }           
        }

        function showMenu(e) {
            var contextMenu = $find("<%= rcmBadgesCertificate.ClientID %>");

            $find("<%= rtbAddFolder.ClientID %>").set_value("");
            $("#<%= txtAddChars.ClientID %>").text("100");
            if ((!e.relatedTarget) || (!$telerik.isDescendantOrSelf(contextMenu.get_element(), e.relatedTarget))) {
                contextMenu.show(e);
            }

            $telerik.cancelRawEvent(e);
        }

        function showMenu1(e) {
            var contextMenu = $find("<%= rcmBadgesCertificateAdd.ClientID %>");
            debugger;

            $find("<%= rtbAddFolder.ClientID %>").set_value("");
            $("#<%= txtAddChars.ClientID %>").text("100");

            if ((!e.relatedTarget) || (!$telerik.isDescendantOrSelf(contextMenu.get_element(), e.relatedTarget))) {
                contextMenu.show(e);
            }

            $telerik.cancelRawEvent(e);
        }

        function addFolder() {
            var radwindow = $find('<%=rwAddFolder.ClientID %>');
            radwindow.show();
        }

        function editFolder() {
            var radwindow = $find('<%=rwEditFolder.ClientID %>');
            radwindow.show();
        }
        function deleteFolder() {
            var radwindow = $find('<%=rwDeleteFolder.ClientID %>');
            radwindow.show();
        }

        function closePopup() {
            var radwindow = $find('<%=rwAddFolder.ClientID %>');
            var radwindowEdit = $find('<%=rwEditFolder.ClientID %>');
            var radwindowDelete = $find('<%=rwDeleteFolder.ClientID %>');
            radwindow.close();
            radwindowEdit.close();
            radwindowDelete.close();
        }

        function rcmBadgesCertificateAdd_Click(sender, args) {
            var itemValue = args.get_item().get_value();
            var radwindow = $find('<%=rwAddFolder.ClientID %>');
            if (itemValue == "Folder") {
                radwindow.show();
            }
            else {
                var uploader = $("#<%= radAddFile.ClientID %> .ruFileInput");
                uploader.click();
            }
        }

        function fileUploadede(sender, args) {
            debugger;
            var upload = $find("<%= radAddFile.ClientID %>");
            var inputs = upload.getUploadedFiles();

            if (!upload.isExtensionValid(inputs[0]))
                alert("extension is invalid!");


            var radManager = $find('<%= RadAjaxManager.GetCurrent(Page).ClientID %>');
            radManager.ajaxRequest();


            setTimeout(function () {
                hideAllLocalLoading();
            }, 3000);

        }

        function hideAllLocalLoading() {
            $(".Loading2").hide();
            console.log("hide loading");
        }

        function SelectFolder(element) {
            $('.rlvCourseCat').css("background-color", "transparent");

            var id = $(element).closest("table").find("input:eq(0)").val();
            var folder = $(element).closest("table").find("input:eq(1)").val();
            var bname = $(element).closest("table").find("input:eq(2)").val();

            $("#" + "<%= hidSelectedFolderName.ClientID %>").val(bname);
            $(element).find("#divCatFolder").css("background-color", "#ff5046");
            var rtbFolder = $find('<%=rtbEditFolder.ClientID %>');
            if (folder.toUpperCase() == 'FOLDER') {
                $('#<%=hidID.ClientID %>').val(id);
                $('#<%=hidFolder.ClientID %>').val(folder);
                $('#<%=hidImgID.ClientID %>').val(0);
            }
            else {
                $('#<%=hidID.ClientID %>').val(0);
                $('#<%=hidFolder.ClientID %>').val(folder);
                $('#<%=hidImgID.ClientID %>').val(id);
            }
            rtbFolder.set_value(bname);
//            var len = 50 - bname.length;
//            $('#<%= txtEditChars.ClientID %>').text(len);

        }

        var selectedCourseItems = [];

        function courseRowSelected(sender, args) {
            var id = args.getDataKeyValue("ID");
            var fname = args.getDataKeyValue("Fname");
            var bname = args.getDataKeyValue("BName");
            if (!selectedCourseItems.includes(id)) {
                selectedCourseItems.push(id);
                selectedCourseItems.push(fname);
                selectedCourseItems.push(bname);
            }
            setSelectedItems();
        }

        function courseRowDeselected(sender, args) {
            var id = args.getDataKeyValue("ID");
            var fname = args.getDataKeyValue("Fname");
            var bname = args.getDataKeyValue("BName");
            if (selectedCourseItems.includes(id)) {
                selectedCourseItems.splice(selectedCourseItems.indexOf(id), 1);
                selectedCourseItems.splice(selectedCourseItems.indexOf(fname), 1)
                selectedCourseItems.splice(selectedCourseItems.indexOf(bname), 1)
            }
            setSelectedItems();
        }

        function courseRowCreated(sender, args) {
            var id = args.getDataKeyValue("ID");
            if (selectedCourseItems.includes(id) && selectedCourseItems.length > 0) {
                args.get_gridDataItem().set_selected(true);
            }
        }

        function setSelectedItems() {
            var elements = selectedCourseItems;
            debugger;
            //            var outputStr = "";
            //            for (var i = 0; i < elements.length; i++) {
            //                outputStr += elements[i] + ",";
            //            }
            var rtbFolder = $find('<%=rtbEditFolder.ClientID %>');
            if (elements.length > 0) {
                if (elements[1].toUpperCase() == 'FOLDER') {
                    $('#<%=hidID.ClientID %>').val(elements[0]);
                    $('#<%=hidFolder.ClientID %>').val(elements[2]);
                    $('#<%=hidImgID.ClientID %>').val(0);
                }
                else {
                    $('#<%=hidID.ClientID %>').val(0);
                    $('#<%=hidFolder.ClientID %>').val(elements[2]);
                    $('#<%=hidImgID.ClientID %>').val(elements[0]);
                }
                rtbFolder.set_value(elements[2]);
//                var len = 50 - elements[2].length;
//                $('#<%= txtEditChars.ClientID %>').text(len);
            }
        }

        //CHECK LENGTH GIVEN BY THE PARAMETER
        function CheckMaxLength(sender, Maxlength,Proc) {
            var length = $(sender).val().length;
            if (sender.value.length > Maxlength) {
                sender.value = sender.value.substr(0, Maxlength);
            }

            var length = Maxlength - length;
            if (Proc == 'Add')
                $('#<%= txtAddChars.ClientID %>').text(length);
            else
                $('#<%= txtEditChars.ClientID %>').text(length);

        }
       

    </script>
</rad:RadScriptBlock>
