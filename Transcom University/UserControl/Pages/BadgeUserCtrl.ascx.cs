﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Telerik.Web.UI;
using System.IO;
using System.Collections.Generic;
using Telerik.Web.UI.Widgets;
using TranscomUniversityV3Model;
using System.Drawing;

public partial class UserControl_Pages_BadgeUserCtrl : System.Web.UI.UserControl
{
    private static int ID = 0, imgID = 0;
    private static string FolderName = string.Empty;

    int strFileType {
        get { return  hidFileType.Value == "" ? 0 : Convert.ToInt16(hidFileType.Value); }
        set { hidFileType.Value = Convert.ToString(value); }
    }
   
    protected void Page_Load(object sender, EventArgs e)    {        
        var BadgeUser = (this.Parent.Parent).FindControl("BadgeUserCtrl1");
        var settings = (this.Parent.Parent).FindControl("SettingsCBUserCtrl1");
        if (BadgeUser.Visible == false)
        {
            settings.Visible = true;        
        }       

        if (strFileType != null && strFileType > 0)
            LoadBadges(strFileType);
            
          

    }

    protected void RadGV_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        var courseID = Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"].ToString()));
        var ds = DataHelper.GetFolderAndBadges(strFileType,courseID);
        RadGV.DataSource = ds;
    }
       
    public void LoadBadges(int fileType)
    {
        var fileName = Request.QueryString["CourseID"].ToString();
        fileName = fileName.Replace(" ", "");
        fileName = fileName.Replace("/", "");
        var courseID = Convert.ToInt32(Utils.Decrypt(fileName));
        var ds = DataHelper.GetFolderAndBadges(fileType, courseID);
        //var path = fileType == 1 ? "Badges" : "Certs";
        var path = string.Empty;        
        foreach (DataRow dr in ds.Tables[0].Rows)
        {
            if (dr["Fname"].ToString() == "FOLDER")
                path = "Badges";
            else
            { path = fileType == 1 ? "Badges" : "Certs";
            path = path + "/" + fileName;
            }
            dr["IconImg"] = path + "/" + dr["IconImg"].ToString();
        }
                
        RadLV.DataSource = ds;
        RadLV.DataBind();
        RadGV.DataSource = ds;
        RadGV.DataBind();

        BadgeMsg.Text = "There are " + ds.Tables[0].Rows.Count.ToString() + " items in this library";
    }    

    protected void LinkBtnChanger3_Click(object sender, EventArgs e)    {        
    }

    protected void btnSwitchListView_OnClick(object sender, EventArgs e) {
        pnlLvResult.Visible = true;
        pnlGridResult.Visible = false;
        btnSwitchToBadgeGridView.Visible = true;
        btnSwitchToBadgeListView.Visible = false;

    }

    protected void btnSwitchGridView_OnClick(object sender, EventArgs e) {
        pnlLvResult.Visible = false;
        pnlGridResult.Visible = true;
        btnSwitchToBadgeGridView.Visible = false;
        btnSwitchToBadgeListView.Visible = true;
    }

    protected void RadLV_OnItemCommande(object sender, RadListViewCommandEventArgs e)
    {
        //var Fname = (e.ListViewItem.FindControl("radLvHidFolder") as HiddenField).Value;
        //var Bname = (e.ListViewItem.FindControl("radBName") as HiddenField).Value;
        //var img = e.ListViewItem.FindControl("pnlImage") as Panel;
        //var newColor = Color.FromArgb(255, 64, 64);
        //img.BackColor = newColor;
        //if (Fname.ToUpper() == "FOLDER")
        //{
        //    ID = Convert.ToInt32((e.ListViewItem.FindControl("radLVHiddenField") as HiddenField).Value);
        //    imgID = 0;
        //}
        //else
        //{
        //    ID = 0;
        //    imgID = Convert.ToInt32((e.ListViewItem.FindControl("radLVHiddenField") as HiddenField).Value);
        //}

        //if (ID != 0 || imgID != 0)
        //{
        //    FolderName = Bname;
        //    rtbEditFolder.Text = FolderName;
        //}
        
    }

    protected void RadGV_OnItemCommande(object sender, GridCommandEventArgs e) {
        try
        {
            GridDataItem item = e.Item as GridDataItem;
            var Fname = (item.FindControl("radGVHidFolder") as HiddenField).Value;
            FolderName = Fname;
            var Bname = (item.FindControl("radBName") as HiddenField).Value;
            if (Fname.ToUpper() == "FOLDER")
            {
                ID = Convert.ToInt32((item.FindControl("radGVHiddenField") as HiddenField).Value);
                imgID = 0;
            }
            else
            {
                ID = 0;
                imgID = Convert.ToInt32((item.FindControl("radGVHiddenField") as HiddenField).Value);
            }

            if (ID != 0)
            {
                OpenFolder();
            }
        }
        catch { }

    }

    protected void btnDeleteFolderOk_Click(object sender, EventArgs e)
    {   
        var currentCIM = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
        int r = 0;
        ID = Convert.ToInt32(Int32.TryParse(hidID.Value, out r) == true ? r : 0);
        imgID = Convert.ToInt32(Int32.TryParse(hidImgID.Value, out r) == true ? r : 0);
        var folderType = ID == 0 ? "Image File" : "Folder";
        if (ID == 0 && imgID == 0)
        {
            RadWindowManager2.RadAlert("Please Select a File First before Deleting" ,330, 180, "Message", "");
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "closePopup1", DataHelper.callClientScript("closePopup"), true);
            return;
        }
        else{
        if(ID != 0)
        {   
            DataHelper.deleteBadgeFolder(currentCIM, ID);            
        }
        else if (imgID != 0)
        {
            DataHelper.deleteBadgesCertificate(currentCIM, imgID);
            
        }

        rtbAddFolder.Text = string.Empty;
        RadWindowManager2.RadAlert(folderType +" has been Deleted.", 330, 180, "Message", "");
        LoadBadges(strFileType);
        ID = 0;
        imgID = 0;
        ScriptManager.RegisterStartupScript(Page, typeof(Page), "closePopup1", DataHelper.callClientScript("closePopup"), true);        
        }
    }

    protected void OpenFolder() {

        lbtBUCnBack.Visible = true;
        rlvBadgeInner.Items.Clear();
        rlvBadgeInner.DataBind();

        pnlMainBadge.Visible = false;
        pnlBadge2nd.Visible = true;

        TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();

        var ds = db.tbl_transcomuniversity_Lkp_Badge
                .Select(a => new
                {
                    a.BadgeName,
                    a.BadgeImgSmall,
                    a.BadgeImgBig,
                    a.ID,
                    a.FolderID,
                    a.FileType
                })
                .Where(a => a.FolderID == ID && a.FileType == strFileType)
                .ToList();
        rlvBadgeInner.DataSource = ds;
        rlvBadgeInner.DataBind();
        rlvBadgeInner.Visible = true;

        BadgeMsg.Text = "There are " + ds.Count.ToString() + " items in " + FolderName;

        btnSwitchToBadgeGridView.Enabled = false;
        btnSwitchToBadgeListView.Enabled = false;
    }

    protected void rcmBadgesCertificate_Click(object sender, RadMenuEventArgs e)
    {
        ID = Convert.ToInt32(hidID.Value);
        imgID = Convert.ToInt32(hidImgID.Value);

        FolderName = hidSelectedFolderName.Value;
        var itemVal = e.Item.Value;
        if (itemVal != "1" && ID == 0 && imgID  == 0) {
            RadWindowManager2.RadAlert("Please Select a File First before " + e.Item.Text, 330, 180, "Error Message","");
        }
        else
        {           
             if (itemVal == "2")
            {             
                 if(ID != 0)     
                    OpenFolder();    
                 else
                     RadWindowManager2.RadAlert("Please Select a Folder before " + e.Item.Text, 330, 180, "Error Message", "");

            }
            else if (itemVal == "3")
            {
                var len = 50 - Convert.ToInt32(rtbEditFolder.Text.Length);
                txtEditChars.InnerText = len.ToString() ;
                    ScriptManager.RegisterStartupScript(Page, typeof(Page), "Deletekey", DataHelper.callClientScript("editFolder"), true);
                    LoadBadges(strFileType);                
            }             
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        var folderName = rtbAddFolder.Text;
        var currentCIM = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
        var courseID = Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"].ToString()));

        if (folderName.Trim() == "")
        {
            RadWindowManager2.RadAlert("Please Input Folder Name.", 330, 180, "Error Message", "");
        }
        else{

            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {

                if (DataHelper.isFolderName(folderName, courseID, strFileType,"Folder"))
                {
                    var ans = DataHelper.insertBadgeFolder(courseID,folderName.Trim(), currentCIM, strFileType);

                    if (ans)
                    {
                        RadWindowManager2.RadAlert("Folder Created.", 330, 180, "Message", "");
                        LoadBadges(strFileType);
                        rtbAddFolder.Text = string.Empty;
                        ScriptManager.RegisterStartupScript(Page, typeof(Page), "Createdkey", DataHelper.callClientScript("closePopup"), true);
                    }
                    else
                    {
                        RadWindowManager2.RadAlert("Failed to Create Folder. Contact Admin for further questions.", 330, 180, "Error Message", "");
                    }
                }
                else
                {
                    RadWindowManager2.RadAlert("Duplicate Folder Name.", 330, 180, "Error Message", "");
                }
            }
        }
    }

    //private bool isFolderName (string folderName, int courseid){
    //    using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
    //    {
    //        var ds = db.tbl_TranscomUniversity_Lkp_FolderBadge
    //                         .Select(a => new
    //                         {
    //                             a.FolderName,
    //                             a.FileType,
    //                             a.CourseID,
    //                             a.HideFromList
    //                         })
    //                         .Where(a => a.FolderName == folderName && a.FileType == strFileType && a.CourseID == courseid && a.HideFromList == 0 )
    //                         .ToList();


    //        if (ds.Count == 0)
    //            return true;
    //        else
    //            return false;
    //    }
    //}

    protected void btnEditSubmit_Click(object sender, EventArgs e)
    {
        var folderName = rtbEditFolder.Text;
        var currentCIM = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
        var courseID = Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"].ToString()));
        var folderType = ID == 0 ? "File Name" : "Folder Name";
        var fileType = ID == 0 ? "image" : "Folder";
        //if (folderName.Trim() == "")
        //{
        //    RadWindowManager2.RadAlert("Please Input Folder Name.", 330, 180, "Error Message", "");
        //}
        //else
        //{
            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                if (DataHelper.isFolderName(folderName, courseID, strFileType,fileType))
                {
                    var ans = true;
                    if (ID == 0)
                        ans = DataHelper.updateBadgesAndCertificate(folderName.Trim(), currentCIM, imgID);
                    else
                        ans = DataHelper.updateBadgeFolder(folderName.Trim(), currentCIM, ID);                    

                    if (ans)
                    {
                       
                        RadLV.SelectedItems.Clear();
                        RadGV.SelectedIndexes.Clear();
                        RadWindowManager2.RadAlert(folderType + " Updated.", 330, 180, "Message", "");
                        LoadBadges(strFileType);
                        rtbEditFolder.Text = string.Empty;
                        ScriptManager.RegisterStartupScript(Page, typeof(Page), "Updatedkey", DataHelper.callClientScript("closePopup"), true);
                        ID = 0;
                        imgID = 0;
                    }
                    else
                    {
                        RadWindowManager2.RadAlert("Failed to Update " + folderType + ". Contact Admin for further questions.", 330, 180, "Error Message", "");
                    }
                }
                else
                {
                    RadWindowManager2.RadAlert("Duplicate " + folderType , 330, 180, "Error Message", "");
                }
            //}
        }
    }

    protected void LinkBtnBack_Click(object sender, EventArgs e)
    {
        pnlMainBadge.Visible = true;
        pnlBadge2nd.Visible = false;
        lbtBUCnBack.Visible = false;

        btnSwitchToBadgeGridView.Enabled = true;
        btnSwitchToBadgeListView.Enabled = true;
        ID = 0;
        imgID = 0;
    }

    protected void upload_FileUploaded(object sender, FileUploadedEventArgs e)
    {

        if (e.IsValid)
        {
            if (e.File.ContentLength > 0) {
                var courseID = Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"].ToString()));
                var currentCIM = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
                foreach (UploadedFile file in radAddFile.UploadedFiles)
                {
                    DataHelper.insertBadges(courseID,file.FileName, file.FileName, file.FileName, ID, currentCIM, strFileType);
                }
                radAddFile.UploadedFiles.Clear();
                RadWindowManager2.RadAlert("  Items Successfully Uploaded", 330, 180, "Message", "");     
            }
                                  
        }
        else
        {
            radAddFile.UploadedFiles.Clear();
            RadWindowManager2.RadAlert("Invalid File. Allowed Files (jpeg,jpg,gif,png)", null, null, "Error Message", "");
        }

    }

}