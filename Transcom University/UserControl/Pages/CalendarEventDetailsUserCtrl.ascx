﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CalendarEventDetailsUserCtrl.ascx.cs"
    Inherits="UserControl_Pages_CalendarEventDetailsUserCtrl" %>

<rad:RadCodeBlock ID="rcbEventDetails" runat="server" >
    <script type="text/javascript">
        function loadEventDetails() {
            var calendarID;
            PageMethods.GetCalendarEventDetails(calendarID,
                function (result) {
                    console.log(result);
                    $("#subjectTitle").html(result.Subject);
                },
                function (error) {
                    var code = error._statusCode;
                    if (code == 401)
                        location.reload();
                    else if (code == 500)
                        var hdnradalerterrormess = $("#<%=hdnradalerterrormess.ClientID%>").val();
                         var hdnraderror = $("#<%=hdnraderror.ClientID%>").val();
                        radalert(hdnradalerterrormess, 330, 180, "hdnraderror", "");
                });
        }
    </script>
</rad:RadCodeBlock>
<asp:Panel ID="pnlDetails" runat="server">
    <h3 class="text-center bold" id="subjectTitle">
        <asp:Label runat="server" ID="Label7" Text= "<%$ Resources:LocalizedResource, EffectiveCoaching %>"></asp:Label>
    </h3>
    <br />
    <br />
    <br />
    <br />
    <br />
    <div class="row">
        <div class="col-md-3">
            <asp:Image ImageUrl="default-user.png" runat="server" />
        </div>
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-3">
                    <asp:Label runat="server" ID="ctile1" Text= "<%$ Resources:LocalizedResource, When %>"></asp:Label>:
                </div>
                <div class="col-md-9">
                    <label id="lblWhen"></label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                   <asp:Label runat="server" ID="Label1" Text= "<%$ Resources:LocalizedResource, Time %>"></asp:Label>:
                </div>
                <div class="col-md-9">
                    <label id="lblTime"></label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <asp:Label runat="server" ID="Label2" Text= "<%$ Resources:LocalizedResource, Location %>"></asp:Label>:
                </div>
                <div class="col-md-9">
                    <label id="lblLocation"></label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <asp:Label runat="server" ID="Label3" Text= "<%$ Resources:LocalizedResource, Trainer %>"></asp:Label>:
                </div>
                <div class="col-md-9">
                    <label id="lblTrainer"></label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <asp:Label runat="server" ID="Label4" Text= "<%$ Resources:LocalizedResource, CourseDescription %>"></asp:Label>:
                </div>
                <div class="col-md-9">
                     <label id="lblDesc"></label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <asp:Label runat="server" ID="Label5" Text= "<%$ Resources:LocalizedResource, AvailableSeats %>"></asp:Label>:
                </div>
                <div class="col-md-9">
                    <label id="lblAvailableSeats"></label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                   <asp:Label runat="server" ID="Label6" Text= "<%$ Resources:LocalizedResource, WaitListed %>"></asp:Label>:
                </div>
                <div class="col-md-9">
                    <label id="lblWaitListed"></label>
                </div>
            </div>
        </div>
    </div>
    <hr />
</asp:Panel>
<asp:HiddenField runat="server" ID ="hdnradalerterrormess" Value="Something went while submitting your report. Please try again" />
<asp:HiddenField runat="server" ID ="hdnraderror" Value="Error Message" /> 