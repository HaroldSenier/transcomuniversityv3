﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CategoriesUserCtrl.ascx.cs"
    Inherits="UserControl_Pages_CategoriesUserCtrl" %>
<rad:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <rad:AjaxSetting AjaxControlID="btnSwitchCatListView">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="rmp1" />
                <rad:AjaxUpdatedControl ControlID="ulBreadcrumb" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnSwitchCatGridView">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="rmp1" />
                <rad:AjaxUpdatedControl ControlID="ulBreadcrumb" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="rdpCourseCategory">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="gvCourseCategory">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="gvCourseCategory" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="rcmCourseCategory">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnAddCategory">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlAddCategory" LoadingPanelID="RadAjaxLoadingPanel2" />
                <rad:AjaxUpdatedControl ControlID="lvCourseCategory" />
                <rad:AjaxUpdatedControl ControlID="gvCourseCategory" />
                <rad:AjaxUpdatedControl ControlID="ltCourseCategory" UpdatePanelRenderMode="Inline" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnCatDelete">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="gvDeleteCategory">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlDelCategory" LoadingPanelID="RadAjaxLoadingPanel2" />
                <rad:AjaxUpdatedControl ControlID="lvCourseCategory" />
                <rad:AjaxUpdatedControl ControlID="gvCourseCategory" />
                <rad:AjaxUpdatedControl ControlID="ltCourseCategory" UpdatePanelRenderMode="Inline" />
            </UpdatedControls>
        </rad:AjaxSetting>
    </AjaxSettings>
</rad:RadAjaxManagerProxy>
<rad:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        function showMenu(e) {
            var contextMenu = $find("<%= rcmCourseCategory.ClientID %>");

            if ((!e.relatedTarget) || (!$telerik.isDescendantOrSelf(contextMenu.get_element(), e.relatedTarget))) {
                contextMenu.show(e);
            }

            $telerik.cancelRawEvent(e);
        }

        function addCategories() {
            var radwindow = $find('<%=rwAddCategories.ClientID %>');

            radwindow.show();
        }

        function deleteCategories() {
            var radwindow = $find('<%=rwDeleteCategories.ClientID %>');

            radwindow.show();
        }
    </script>
</rad:RadScriptBlock>
<rad:RadWindowManager ID="RadWindowManager1" RenderMode="Lightweight" EnableShadow="true"
    Skin="Bootstrap" Modal="true" VisibleOnPageLoad="false" Behaviors="Close, Move"
    DestroyOnClose="true" RestrictionZoneID="RestrictionZone" Opacity="99" runat="server"
    Width="800" Height="400px">
    <Windows>
        <rad:RadWindow ID="rwAddCategories" runat="server" RenderMode="Lightweight" Modal="true"
            Skin="Bootstrap" Behaviors="Move, Close" CssClass="tc-radwindow-1 height-inherit"
            Height="250px" Width="450px" VisibleStatusbar="false" Title="<%$ Resources:LocalizedResource, AddCategory%>">
            <ContentTemplate>
                <br />
                <asp:Panel ID="pnlAddCategory" runat="server">
                    <table width="100%">
                        <tbody>
                            <tr>
                                <td>
                                   <asp:Label runat="server" ID="lblCategoryName" Text="<%$ Resources:LocalizedResource, CategoryName %>"></asp:Label>
                                </td>
                                <td>
                                    <rad:RadTextBox ID="txtCategoryName" runat="server" RenderMode="Lightweight" EmptyMessage="<%$ Resources:LocalizedResource, CategoryName %>"
                                        MaxLength="50">
                                    </rad:RadTextBox>
                                    <asp:RequiredFieldValidator ID="rfvCategoryName" runat="server" ErrorMessage="*"
                                        ControlToValidate="txtCategoryName" SetFocusOnError="True" Display="Dynamic"
                                        ValidationGroup="addCategory" ForeColor="Red" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   <asp:Label runat="server" ID="lblDescription" Text="<%$ Resources:LocalizedResource, Description %>"></asp:Label>
                                </td>
                                <td>
                                    <rad:RadTextBox ID="txtCategoryDesc" runat="server" RenderMode="Lightweight" EmptyMessage="<%$ Resources:LocalizedResource, Description %>"
                                        TextMode="MultiLine">
                                    </rad:RadTextBox>
                                </td>
                            </tr>
                        </tbody>
                        <tfoot class="footer-bottom">
                            <tr>
                                <td style="padding-top: 10px;">
                                    <asp:LinkButton ID="btnAddCategory" runat="server" OnClick="btnAddCategory_Click"
                                        ToolTip="Add Category" Text="<%$ Resources:LocalizedResource, Add %>" CssClass="btn tc-btn-md btn-teal btn-flat pull-right"
                                        ValidationGroup="addCategory">
                                    </asp:LinkButton>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </asp:Panel>
                <br />
            </ContentTemplate>
        </rad:RadWindow>
    </Windows>
    <Windows>
        <rad:RadWindow ID="rwDeleteCategories" runat="server" RenderMode="Lightweight" Modal="true"
            Skin="Bootstrap" Behaviors="Move, Close" CssClass="tc-radwindow-1 height-inherit"
            Height="450px" Width="850px" VisibleStatusbar="false" Title="<%$ Resources:LocalizedResource, DeleteCategory%>">
            <ContentTemplate>
                <asp:Panel ID="pnlDelCategory" runat="server">
                    <rad:RadGrid ID="gvDeleteCategory" runat="server" RenderMode="Lightweight" OnNeedDataSource="gvDeleteCategory_NeedDataSource"
                        OnItemCommand="gvDeleteCategory_ItemCommand" AutoGenerateColumns="false" Height="370px"
                        PageSize="5" CssClass="GridLess">
                        <PagerStyle Mode="NextPrev" AlwaysVisible="true" />
                        <ClientSettings EnableAlternatingItems="true" AllowColumnsReorder="true" EnableRowHoverStyle="true">
                            <Selecting AllowRowSelect="true"></Selecting>
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            <Resizing AllowColumnResize="true" ResizeGridOnColumnResize="true" AllowResizeToFit="true" />
                        </ClientSettings>
                        <ItemStyle Wrap="false"></ItemStyle>
                        <MasterTableView DataKeyNames="CategoryID" AllowSorting="true" AllowPaging="true"
                            CommandItemDisplay="None" HeaderStyle-ForeColor="Teal">
                            <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                            <Columns>
                                <rad:GridButtonColumn ConfirmDialogType="RadWindow" ConfirmTitle="<%$ Resources:LocalizedResource, DeleteRecord%>"
                                    ButtonType="ImageButton" CommandName="Delete" Text="<%$ Resources:LocalizedResource, DeleteRecord%>" UniqueName="DeleteColumn"
                                    ConfirmText="<%$ Resources:LocalizedResource, Areyousureyouwanttodeletethisrecord%>" ImageUrl="~/Media/Images/deletesmall.png"
                                    ConfirmDialogHeight="180" ConfirmDialogWidth="330" HeaderStyle-Width="30">
                                </rad:GridButtonColumn>
                                <rad:GridBoundColumn UniqueName="Category" HeaderText="<%$ Resources:LocalizedResource, Category %>" DataField="Category"
                                    ReadOnly="true">
                                </rad:GridBoundColumn>
                                <rad:GridBoundColumn UniqueName="Description" HeaderText="<%$ Resources:LocalizedResource, Description %>" DataField="Description"
                                    ReadOnly="true">
                                </rad:GridBoundColumn>
                                <rad:GridBoundColumn UniqueName="Subcategories" HeaderText="<%$ Resources:LocalizedResource, Subcategories %>" DataField="Subcategories"
                                    ReadOnly="true">
                                </rad:GridBoundColumn>
                                <rad:GridBoundColumn UniqueName="Courses" HeaderText="<%$ Resources:LocalizedResource, Courses %>" DataField="Courses"
                                    ReadOnly="true">
                                </rad:GridBoundColumn>
                                <rad:GridBoundColumn UniqueName="CreatedBy" HeaderText="<%$ Resources:LocalizedResource, UpdatedBy %>" DataField="CreatedBy"
                                    ReadOnly="true">
                                </rad:GridBoundColumn>
                                <rad:GridBoundColumn UniqueName="DateTimeCreated" HeaderText="<%$ Resources:LocalizedResource, DateUpdated %>" DataField="DateTimeCreated"
                                    ReadOnly="true">
                                </rad:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                    </rad:RadGrid>
                </asp:Panel>
            </ContentTemplate>
        </rad:RadWindow>
    </Windows>
</rad:RadWindowManager>
<div class="row">
    <div class="container" style="text-align: right;">
        <asp:LinkButton ID="btnSwitchCatListView" runat="server" OnClick="btnSwitchCatListView_Click"
            Font-Underline="false" ForeColor="Black">
            <i id="iCatListView" runat="server" class="fa fa-th-list" style="margin-right: 15px"
                aria-hidden="true" title="<%$ Resources:LocalizedResource, ListView %>"></i>
        </asp:LinkButton>
        <asp:LinkButton ID="btnSwitchCatGridView" runat="server" OnClick="btnSwitchCatGridView_Click"
            Font-Underline="false" ForeColor="Black" Visible="false">
            <i id="iCatGridView" runat="server" class="fa fa-th" style="margin-right: 15px" aria-hidden="true"
                title="<%$ Resources:LocalizedResource, GridView %>"></i>
        </asp:LinkButton>
        <asp:LinkButton ID="btnCatSettings" runat="server" Font-Underline="false" ForeColor="Black"
            OnClientClick="showMenu(event);" Visible="false">
            <i id="iCatSettings" runat="server" class="fa fa-ellipsis-v" style="margin-right: 15px"
                aria-hidden="true" title="<%$ Resources:LocalizedResource, Settings%>"></i>
        </asp:LinkButton>
        <asp:LinkButton ID="btnCatDelete" runat="server" Font-Underline="false" ForeColor="Black"
            OnClick="btnCatDelete_Click" Visible="false">
            <i id="iCatDelete" runat="server" class="fa fa-trash-o" aria-hidden="true" title="<%$ Resources:LocalizedResource, Delete%>">
            </i>
        </asp:LinkButton>
    </div>
    <div class="container">
        <asp:Label runat="server" ID="lblNumberOfCourseCat" ForeColor="Teal" Font-Bold="true"
            Font-Size="Small">
            <asp:Label runat="server" ID="Label1" ForeColor="Teal" Font-Bold="true"
            Font-Size="Small" Text="<%$ Resources:LocalizedResource, ThereAre %>"></asp:Label>
            <asp:Literal ID="ltCourseCategory" runat="server"></asp:Literal>
            <asp:Label runat="server" ID="Label2" ForeColor="Teal" Font-Bold="true"
            Font-Size="Small" Text="<%$ Resources:LocalizedResource, CourseCategories %>">
            </asp:Label></asp:Label>
    </div>
</div>
<asp:Panel ID="pnlCatListView" runat="server">
    <div class="col-md-14" style="margin-left: 1%; margin-right: 1%">
        <div class="col-md-12">
            <asp:Panel ID="pnlCatLv" runat="server">
                <div id="divCatLv" class="demo-container col-md-14">
                    <rad:RadListView ID="lvCourseCategory" runat="server" ItemPlaceholderID="ListViewContainer1"
                        AllowPaging="true" OnNeedDataSource="lvCourseCategory_NeedDataSource">
                        <ItemTemplate>
                            <asp:HyperLink ID="hlLvCourseCategory" runat="server" NavigateUrl='<%# string.Format("~/Subcategories.aspx?CategoryID={0}", HttpUtility.UrlEncode(Encrypt(Eval("CategoryID").ToString()))) %>'
                                ForeColor="Black">
                                <div id="divCatFolder" class="rlvCourseCat" title='<%#Eval("Category")%>'>
                                    <div class="photo-container-coursecat">
                                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Media/Images/CourseCat.png" Width="170px"
                                            Height="130px" />
                                    </div>
                                    <div class="category font-bold" align="center">
                                        <%#Eval("Category")%>
                                    </div>
                                </div>
                            </asp:HyperLink>
                        </ItemTemplate>
                        <LayoutTemplate>
                            <asp:PlaceHolder ID="ListViewContainer1" runat="server" />
                            <rad:RadDataPager ID="rdpCourseCategory" runat="server" RenderMode="Lightweight"
                                PagedControlID="lvCourseCategory" PageSize="5" BackColor="White" BorderStyle="None"
                                CssClass="RadDataPagerx">
                                <Fields>
                                    <rad:RadDataPagerButtonField FieldType="Prev" PrevButtonText="Prev"></rad:RadDataPagerButtonField>
                                    <%--<rad:RadDataPagerButtonField FieldType="Numeric" PageButtonCount="10" />--%>
                                    <rad:RadDataPagerButtonField FieldType="Next" NextButtonText="Next"></rad:RadDataPagerButtonField>
                                </Fields>
                            </rad:RadDataPager>
                        </LayoutTemplate>
                    </rad:RadListView>
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Panel>
<asp:Panel ID="pnlCatGridView" runat="server" Visible="false">
    <div class="col-md-14" style="margin-left: 1%; margin-right: 1%">
        <div class="col-md-12">
            <asp:Panel ID="pnlCatGv" runat="server">
                <div id="divCatGv" class="demo-container col-md-14">
                    <rad:RadGrid ID="gvCourseCategory" runat="server" RenderMode="Lightweight" OnNeedDataSource="gvCourseCategory_NeedDataSource"
                        AutoGenerateColumns="false" CssClass="GridLess" Height="370px" PageSize="5">
                        <PagerStyle Mode="NextPrev" AlwaysVisible="true" />
                        <ClientSettings EnableAlternatingItems="true" AllowColumnsReorder="true">
                            <Selecting AllowRowSelect="true"></Selecting>
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            <Resizing AllowColumnResize="true" ResizeGridOnColumnResize="true" AllowResizeToFit="true" />
                        </ClientSettings>
                        <ItemStyle Wrap="false"></ItemStyle>
                        <MasterTableView DataKeyNames="CategoryID" AllowSorting="true" AllowPaging="true"
                            HeaderStyle-ForeColor="Teal">
                            <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                            <Columns>
                                <rad:GridTemplateColumn SortExpression="Category" UniqueName="Category" HeaderText="CATEGORY"
                                    DataField="Category">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hlGvCourseCategory" runat="server" NavigateUrl='<%# string.Format("Subcategories.aspx?CategoryID={0}", HttpUtility.UrlEncode(Encrypt(Eval("CategoryID").ToString()))) %>'>
                                                                            <%#Eval("Category")%>
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                </rad:GridTemplateColumn>
                                <rad:GridBoundColumn UniqueName="Description" HeaderText="<%$ Resources:LocalizedResource, Description%>" DataField="Description"
                                    ReadOnly="true">
                                </rad:GridBoundColumn>
                                <rad:GridBoundColumn UniqueName="Subcategories" HeaderText="<%$ Resources:LocalizedResource, Subcategories%>" DataField="Subcategories"
                                    ReadOnly="true">
                                </rad:GridBoundColumn>
                                <rad:GridBoundColumn UniqueName="Courses" HeaderText="<%$ Resources:LocalizedResource, Course%>" DataField="Courses"
                                    ReadOnly="true">
                                </rad:GridBoundColumn>
                                <rad:GridBoundColumn UniqueName="CreatedBy" HeaderText="<%$ Resources:LocalizedResource, UpdatedBy%>" DataField="CreatedBy"
                                    ReadOnly="true">
                                </rad:GridBoundColumn>
                                <rad:GridBoundColumn UniqueName="DateTimeCreated" HeaderText="<%$ Resources:LocalizedResource, DateUpdated %>" DataField="DateTimeCreated"
                                    ReadOnly="true">
                                </rad:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                    </rad:RadGrid>
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Panel>
<rad:RadContextMenu ID="rcmCourseCategory" runat="server" RenderMode="Lightweight"
    OnItemClick="rcmCourseCategory_Click" EnableShadows="true" CssClass="RadContextMenu1"
    EnableScreenBoundaryDetection="false">
    <Items>
        <rad:RadMenuItem Text="<%$ Resources:LocalizedResource, AddCategory%>" Value="1" />
    </Items>
</rad:RadContextMenu>
