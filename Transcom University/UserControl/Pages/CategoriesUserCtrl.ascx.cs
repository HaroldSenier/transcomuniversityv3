﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using TranscomUniversityV3Model;
using System.Text;
using System.Security.Cryptography;
using System.IO;

public partial class UserControl_Pages_CategoriesUserCtrl : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {//for testing purpose
            var CIM = DataHelper.GetCurrentUserCIM();
            if (!System.Web.Security.Roles.IsUserInRole(HttpContext.Current.User.Identity.Name.Split('|')[0], "Trainer") && (CIM != "10157009" && CIM != "10146063" && CIM != "10168588" && CIM != "10163298"))
                    Response.Redirect("~/Unauthorized.aspx");

            //set Session
            Session["SessionRoles"] = "Trainer";
        }
        GetCourseCatCount();
    }

    #region CourseCategories

    public void GetCourseCatCount()
    {
        var db = new TranscomUniversityV3ModelContainer();

        var countCourseCategory = (from c in db.vw_TranscomUniversity_Category
                                   select c).Count().ToString();

        ltCourseCategory.Text = countCourseCategory;
    }

    protected void lvCourseCategory_NeedDataSource(object source, RadListViewNeedDataSourceEventArgs e)
    {
        if (e.RebindReason == Telerik.Web.UI.RadListViewRebindReason.ExplicitRebind || e.RebindReason == Telerik.Web.UI.RadListViewRebindReason.PostBackEvent)
        {
            var courseCategory = DataHelper.GetCourseCategory();

            if (courseCategory != null)
            {
                lvCourseCategory.DataSource = courseCategory;
            }
        }
    }

    protected void gvCourseCategory_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        var courseCategory = DataHelper.GetCourseCategory();

        if (courseCategory != null)
            gvCourseCategory.DataSource = courseCategory;
    }

    protected void btnSwitchCatListView_Click(object sender, EventArgs e)
    {
        btnSwitchCatGridView.Visible = true;
        pnlCatGridView.Visible = true;

        gvCourseCategory.Rebind();

        btnSwitchCatListView.Visible = false;
        pnlCatListView.Visible = false;

        //lblTab3.Text = "List View";
    }

    protected void btnSwitchCatGridView_Click(object sender, EventArgs e)
    {
        btnSwitchCatListView.Visible = true;
        pnlCatListView.Visible = true;

        lvCourseCategory.Rebind();

        btnSwitchCatGridView.Visible = false;
        pnlCatGridView.Visible = false;

        //lblTab3.Text = "Grid View";
    }

    protected void rcmCourseCategory_Click(object sender, RadMenuEventArgs e)
    {
        var itemVal = e.Item.Value;

        if (itemVal == "1")
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "addCategories", DataHelper.callClientScript("addCategories"), true);
    }

    protected void btnAddCategory_Click(object sender, EventArgs e)
    {
        try
        {

            var createdBy = DataHelper.GetCurrentUserCIM();
            var db = new TranscomUniversityV3ModelContainer();

            //Logger variables Start
            string _userIP = DataHelper.GetIPAddress();
            string _userID = DataHelper.GetCurrentUserCIM();
            double _duration = 0;

            DateTime startTime;
            DateTime endTime;

            var dts = db.CreateQuery<DateTime>("CurrentDateTime() ");
            startTime = dts.AsEnumerable().First();
            //Logger variables End

            pr_TranscomUniversity_InsertCategory_Result retVal = db.pr_TranscomUniversity_InsertCategory(txtCategoryName.Text.Trim(), txtCategoryDesc.Text, Convert.ToInt32(createdBy)).SingleOrDefault();


            //logger start transaction
            string _action = "Category " + txtCategoryName.Text.Trim() + " was created by " + _userID;

            var dte = db.CreateQuery<DateTime>("CurrentDateTime()");

            endTime = dte.AsEnumerable().First();
            _duration = (endTime - startTime).TotalMilliseconds;

            int l_actionID = DataHelper.getLastLogID() + 1;
            DataHelper.logCategory(Convert.ToInt32(retVal.CategoryID), l_actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);
            //logger end transaction

            if (retVal.CategoryID.ToString() != "0")
            {
                RadWindowManager1.RadAlert("Category successfully added.", 330, 180, "System Message", "");

                lvCourseCategory.Rebind();
                gvCourseCategory.Rebind();
                GetCourseCatCount();
            }
            else
                RadWindowManager1.RadAlert("Category name already exists. Please try another name.", 330, 180, "System Message", "");

            txtCategoryName.Text = "";
            txtCategoryDesc.Text = "";
        }
        catch
        {
            RadWindowManager1.RadAlert("Error adding category. Please contact your System Administrator.", 330, 180, "Error Message", "");
        }
    }

    protected void btnCatDelete_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(Page, typeof(Page), "deleteCategories", DataHelper.callClientScript("deleteCategories"), true);
    }

    protected void gvDeleteCategory_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        var courseCategory = DataHelper.GetCourseCategory();

        if (courseCategory != null)
            gvDeleteCategory.DataSource = courseCategory;
    }

    protected void gvDeleteCategory_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == RadGrid.DeleteCommandName)
        {
            var deletedItem = e.Item as GridEditableItem;

            try
            {
                var catId = deletedItem.GetDataKeyValue("CategoryID");
                var db = new TranscomUniversityV3ModelContainer();


                //Logger variables Start
                string _userIP = DataHelper.GetIPAddress();
                string _userID = DataHelper.GetCurrentUserCIM();
                double _duration = 0;

                DateTime startTime;
                DateTime endTime;

                var dts = db.CreateQuery<DateTime>("CurrentDateTime() ");
                startTime = dts.AsEnumerable().First();
                //Logger variables End

                db.pr_TranscomUniversity_DeleteCategory(Convert.ToInt32(catId));

                //logger start transaction
                string _action = "Category " + txtCategoryName.Text.Trim() + " was deleted by " + _userID;

                var dte = db.CreateQuery<DateTime>("CurrentDateTime()");

                endTime = dte.AsEnumerable().First();
                _duration = (endTime - startTime).TotalMilliseconds;

                int l_actionID = DataHelper.getLastLogID() + 1;
                DataHelper.logCategory(Convert.ToInt32(catId), l_actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);
                //logger end transaction

                gvDeleteCategory.Rebind();
                lvCourseCategory.Rebind();
                gvCourseCategory.Rebind();
                GetCourseCatCount();
            }
            catch
            {
                RadWindowManager1.RadAlert("Error deleting category. Please contact your System Administrator.", 330, 180, "Error Message", "");
            }
        }
    }

    #endregion

    protected string Encrypt(string clearText)
    {
        string EncryptionKey = "TRNSCMV32017111";
        byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);

        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(clearBytes, 0, clearBytes.Length);
                    cs.Close();
                }
                clearText = Convert.ToBase64String(ms.ToArray());
            }
        }
        return clearText;
    }
}