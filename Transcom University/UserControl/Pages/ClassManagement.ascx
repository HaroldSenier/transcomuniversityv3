﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ClassManagement.ascx.cs"
    Inherits="UserControl_Pages_ClassManagement" %>
<asp:Panel runat="server" class="region">
    <div class="wrapper-main-content">
        <div class="row">
            <div class="col-md-12">
                <ul class="flex-container row-reverse">
                    <li><asp:Label runat="server" ID="lblAddDataFields" Text="<%$ Resources:LocalizedResource, AddDataFields %>"></asp:Label>
                        <rad:RadDropDownList ID="ddlAddFields" DefaultMessage="<%$ Resources:LocalizedResource, Select %>" runat="server">
                        </rad:RadDropDownList>
                    </li>
                    <li style="width: 20%;"><asp:Label runat="server" ID="lblStatus" Text="<%$ Resources:LocalizedResource, Status %>"></asp:Label>
                        <rad:RadDropDownList ID="ddlStatus" DefaultMessage="<%$ Resources:LocalizedResource, Select %>" runat="server">
                        </rad:RadDropDownList>
                    </li>
                    <li style="width: 25%;"><asp:Label runat="server" ID="lblTrainingType" Text="<%$ Resources:LocalizedResource, TrainingType %>"></asp:Label>
                        <rad:RadDropDownList ID="ddlTrainingType" DefaultMessage="<%$ Resources:LocalizedResource, Select %>" runat="server">
                        </rad:RadDropDownList>
                    </li>
                </ul>
            </div>
        </div>
        <asp:Panel runat="server" ID="pnlNew">
        </asp:Panel>
        <div class="widgets black-header">
            <div class="col-md-6 font-bold ">
                <p class="">
                   <asp:Label runat="server" ID="lblNewHire" Text="<%$ Resources:LocalizedResource, NewHire %>"></asp:Label>
                </p>
            </div>
           <div id="mc-container" class="mc-container col-md-12 no-paddings">
         
        </div>
        </div>
    </div>
</asp:Panel>
