﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ClassManagement_ClassDetails.ascx.cs"
    Inherits="UserControl_Pages_ClassManagement_ClassDetails" %>
<rad:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <rad:AjaxSetting AjaxControlID="btnEditClassDetails">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlClassDetails" LoadingPanelID="localLoading" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnHiddenSaveDetails">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlClassDetails" LoadingPanelID="localLoading" />
                <rad:AjaxUpdatedControl ControlID="pnlClassPreview" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnCancelEdit">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlClassDetails" LoadingPanelID="localLoading" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <%--<rad:AjaxSetting AjaxControlID="btnSaveChanges">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlClassDetails" LoadingPanelID="fullPageLoading" />
            </UpdatedControls>
        </rad:AjaxSetting>--%>
        <rad:AjaxSetting AjaxControlID="rcbSite">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="rcbSite" LoadingPanelID="noLoadingPanel" />
                <rad:AjaxUpdatedControl ControlID="rcbVenue" LoadingPanelID="localLoading" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="rcbProgramAccount">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="rcbLOBCampaign" LoadingPanelID="localLoading" />
            </UpdatedControls>
        </rad:AjaxSetting>
      <%--  <rad:AjaxSetting AjaxControlID="rdpPstEndtDate">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="txtNoTrainingDays" LoadingPanelID="localLoading" />
                <rad:AjaxUpdatedControl ControlID="txtTotalTrainingHours" LoadingPanelID="localLoading" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="rdpPSTStartDate">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="txtNoTrainingDays" LoadingPanelID="localLoading" />
                <rad:AjaxUpdatedControl ControlID="txtTotalTrainingHours" LoadingPanelID="localLoading" />
            </UpdatedControls>
        </rad:AjaxSetting>--%>
        <rad:AjaxSetting AjaxControlID="btnLoadTrainerDetails">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="txtSupName" LoadingPanelID="noLoadingPanel" />
                <rad:AjaxUpdatedControl ControlID="txtTrainerName" LoadingPanelID="noLoadingPanel" />
            </UpdatedControls>
        </rad:AjaxSetting>
    </AjaxSettings>
</rad:RadAjaxManagerProxy>
<rad:RadAjaxLoadingPanel ID="localLoadingPanel" runat="server" CssClass="Loading2"
    Transparency="25" />
<rad:RadAjaxLoadingPanel ID="noLoadingPanel" runat="server" CssClass="Loading-Empty" />
<asp:Panel ID="pnlClassDetails" runat="server" CssClass="container">
    <asp:LinkButton ID="btnEditClassDetails" runat="server" Font-Underline="false" CssClass="fa fa-pencil-square-o pull-right fa-gray fa-md"
        aria-hidden="true" ToolTip="Edit Class Details" OnClick="btnEditClassDetails_Click"
        Style="margin-top: 5px;"></asp:LinkButton>
    <asp:LinkButton ID="btnSaveDetails" runat="server" Font-Underline="false" CssClass="fa fa-floppy-o pull-right fa-gray fa-md d"
        aria-hidden="true" ToolTip="<%$ Resources:LocalizedResource, SaveChanges%>" OnClientClick="ConfirmUpdateClass(); return false;"
        Visible="false" Style="margin-top: 5px;"></asp:LinkButton>
    <rad:RadButton ID="btnHiddenSaveDetails" runat="server" ToolTip="Save" ValidationGroup="addClass"
        CausesValidation="true" OnClick="btnSaveChanges_Click" ButtonType="SkinnedButton" CssClass="display-none"
        RenderMode="Lightweight" Skin="Bootstrap">
    </rad:RadButton>
    <br />
    <br />
    <br />
    <asp:Panel ID="pnlDetailView" runat="server">
        <div class="col-md-12">
            <div class="row border-bottom-dash">
                <div class="col-md-6">
                    <h5 class="bold black">                   
                        <asp:Label runat="server" ID="lblProgramAccountInformation" Text="<%$ Resources:LocalizedResource, ProgramAccountInformation %>"></asp:Label>
                    </h5>
                </div>
                <div class="col-md-6">
                    <h5 class="bold black">
                        New Hire Training Details
                       <%-- <asp:Label runat="server" ID="Label1" Text="<%$ Resources:LocalizedResource, ProgramAccountInformation %>"></asp:Label>--%>
                    </h5>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="col-md-5 no-padding padding-tb">
                        <asp:Label runat="server" ID="Label1" Text="<%$ Resources:LocalizedResource, Accountprogram %>"></asp:Label>:
                    </div>
                    <div class="col-md-6 padding-tb">
                        <asp:Label ID="lblAccount" Text="<%$ Resources:LocalizedResource, Account %>" runat="server" />:
                    </div>
                    <div class="col-md-5 no-padding padding-tb">
                        <asp:Label runat="server" ID="lblRecommendedLeadershipCoursesForYou" Text=" <%$ Resources:LocalizedResource, Course%>" ></asp:Label>:
                    </div>
                    <div class="col-md-6 padding-tb">
                        <asp:Label ID="lblCourse" Text="<%$ Resources:LocalizedResource, Account %>" runat="server" />:
                    </div>
                    <div class="col-md-5 no-padding padding-tb">
                        <asp:Label runat="server" ID="Label2" Text=" <%$ Resources:LocalizedResource, Class%>" ></asp:Label>:
                    </div>
                    <div class="col-md-6 padding-tb">
                        <asp:Label ID="lblClass" Text="<%$ Resources:LocalizedResource, Account %>" runat="server" />
                    </div>
                    <div class="col-md-5 no-padding padding-tb">
                        <asp:Label runat="server" ID="Label3" Text=" <%$ Resources:LocalizedResource, Audience%>" ></asp:Label>:
                    </div>
                    <div class="col-md-6 padding-tb">
                        <asp:Label ID="lblAudience" Text="<%$ Resources:LocalizedResource, Account %>" runat="server" />
                    </div>
                    <div class="col-md-5 no-padding padding-tb">
                        Site:
                    </div>
                    <div class="col-md-6 padding-tb">
                        <asp:Label ID="lblSite" Text="<%$ Resources:LocalizedResource, Account %>" runat="server" />
                    </div>
                    <div class="col-md-5 no-padding padding-tb">
                        <asp:Label runat="server" ID="Label4" Text=" <%$ Resources:LocalizedResource, Venue%>" ></asp:Label>:
                    </div>
                    <div class="col-md-6 padding-tb">
                        <asp:Label ID="lblVenue" Text="<%$ Resources:LocalizedResource, Account %>" runat="server" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-5 no-padding padding-tb">
                         <asp:Label ID="lblTotalNoOfSessions" Text="<%$ Resources:LocalizedResource, TotalNoOfSessions %>" runat="server" />
                    </div>
                    <div class="col-md-6 padding-tb">
                        <asp:Label ID="lblSession" Text="<%$ Resources:LocalizedResource, Account %>" runat="server" />
                    </div>
                    <div class="col-md-5 no-padding padding-tb">
                    <asp:Label ID="lblStartDatetime" Text="<%$ Resources:LocalizedResource, StartDatetime %>" runat="server" />                        
                    </div>
                    <div class="col-md-6 padding-tb">
                        <asp:Label ID="lblStartDate" Text="Account" runat="server" />
                    </div>
                    <div class="col-md-5 no-padding padding-tb">
                     <asp:Label ID="lblEndDatetime" Text="<%$ Resources:LocalizedResource, EndDatetime %>" runat="server" />     
                        
                    </div>
                    <div class="col-md-6 padding-tb">
                        <asp:Label ID="lblEndDate" Text="Account" runat="server" />
                    </div>
                    <div class="col-md-5 no-padding padding-tb">                        
                        <asp:Label ID="lblStartingHeadcount" Text="<%$ Resources:LocalizedResource, StartingHeadcount %>" runat="server" />  
                    </div>
                    <div class="col-md-6 padding-tb">
                        <asp:Label ID="lblHeadCount" Text="Account" runat="server" />
                    </div>
                    <div class="col-md-5 no-padding padding-tb">                        
                        <asp:Label ID="lblNoOfTrainingDays" Text="<%$ Resources:LocalizedResource, NoOfTrainingDays %>" runat="server" />  
                    </div>
                    <div class="col-md-6 padding-tb">
                        <asp:Label ID="lblTrainingDays" Text="Account" runat="server" />
                    </div>
                    <div class="col-md-5 no-padding padding-tb">                        
                        <asp:Label ID="lblTotalTrainingHours" Text="<%$ Resources:LocalizedResource, TotalTrainingHours %>" runat="server" />  
                    </div>
                    <div class="col-md-6 padding-tb">
                        <asp:Label ID="lblTrainingHours" Text="Account" runat="server" />
                    </div>
                </div>
            </div>
            <br />
            <%-- Start 3rd row details --%>
            <div class="row border-bottom-dash">
                <div class="col-md-6">
                    <h5 class="bold black">                       
                        
                        <asp:Label ID="lblClassNameAndDescription" Text="<%$ Resources:LocalizedResource, ClassNameAndDescription %>" runat="server" />  
                    </h5>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="col-md-5 no-padding padding-tb">
                        Class Name:
                    </div>
                    <div class="col-md-7 padding-tb">
                        <asp:Label ID="lblClassName" runat="server" CssClass="break-all">
                            Class Name
                        </asp:Label>
                    </div>
                    <div class="col-md-5 no-padding padding-tb">                       
                       <asp:Label ID="lblClassDescription1" Text="<%$ Resources:LocalizedResource, ClassDescription %>" runat="server" />  
                    </div>
                    <div class="col-md-7 padding-tb overflow-scroll" style="height:150px;">
                        <p>
                       
                        <asp:Label ID="lblClassDescription" runat="server" CssClass="break-all " Text="<%$ Resources:LocalizedResource, Description %>">                           
                           
                        </asp:Label>
                         </p>
                        <%--<rad:RadTextBox ID="lblClassDescription" TextMode="MultiLine" runat="server" CssClass="form-control MultiLineTextBox btn-flat"
                            RenderMode="Lightweight" Skin="Bootstrap" ViewStateMode="Enabled"
                            Width="100%" ClientIDMode="Static" placeholder="What is this class about?" ReadOnly="true" >
                        </rad:RadTextBox>--%>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-5 no-padding padding-tb">
                        <asp:Label runat="server" ID="Label5" Text=" <%$ Resources:LocalizedResource, Trainer%>" ></asp:Label> CIM:
                    </div>
                    <div class="col-md-5 padding-tb">
                        <asp:Label ID="lblTrainerCIM" runat="server">
                            Class Name
                        </asp:Label>
                    </div>
                    <div class="col-md-5 no-padding padding-tb">
                        <asp:Label runat="server" ID="Label6" Text=" <%$ Resources:LocalizedResource, Trainer%>" ></asp:Label> <asp:Label runat="server" ID="Label7" Text=" <%$ Resources:LocalizedResource, Name%>" ></asp:Label>:
                    </div>
                    <div class="col-md-5 padding-tb">
                        <asp:Label ID="lblTrainerName" runat="server">
                            Trainer Name
                        </asp:Label>
                    </div>
                    <div class="col-md-5 no-padding padding-tb">             
                        <asp:Label ID="lblTrainerSupervisorName" Text="<%$ Resources:LocalizedResource, TrainerSupervisorName %>" runat="server" />  
                    </div>
                    <div class="col-md-5 padding-tb">
                        <asp:Label ID="lblTrainerSup" runat="server" Text="<%$ Resources:LocalizedResource, Supervisor %>"></asp:Label>
                    </div>
                </div>
                <%-- <div class="col-md-6">
                    <asp:Image ID="classImageContainer" runat="server" Width="100px" Height="100px" />
                </div>--%>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlDetailsEdit" runat="server" Visible="false" CssClass="classEditor">
        <asp:Label ID="lblReqFields" runat="server" Tex="" ForeColor="Red" ClientIDMode="Static"></asp:Label>
        <div class="row">
            <div class="col-md-6">
                <div class="font-bold">                
                    <asp:Label ID="lblProgramAccountInformation1" runat="server" Text="<%$ Resources:LocalizedResource, ProgramAccountInformation %>"></asp:Label>
                </div>
            </div>
            <div class="col-md-5">
                <div class="font-bold">
                <asp:Label ID="lblTrainingDetails" runat="server" Text="<%$ Resources:LocalizedResource, TrainingDetails %>"></asp:Label>
                    
                </div>
            </div>
            <hr style="border-top: dotted 1px;" />
        </div>
        <div class="row">
            <div class="col-lg-6 col-sm-12" style="font-weight: normal;">
                <div class="row">
                    <div class="col-md-4">
                        
                        <asp:Label ID="lblProgramAccount" runat="server" Text="<%$ Resources:LocalizedResource, ProgramAccount %>"></asp:Label>
                    </div>
                    <div class="col-md-6 pull-left form-group no-padding">
                        <rad:RadComboBox ID="rcbProgramAccount" runat="server" RenderMode="Lightweight" DataTextField="Client"
                            DataValueField="ClientID" EmptyMessage="<%$ Resources:LocalizedResource, Select %>" EnableVirtualScrolling="true"
                            ToolTip="<%$ Resources:LocalizedResource, SelectProgram%>" CssClass="width-fill" MarkFirstMatch="true" AllowCustomText="false"
                            EnableItemCaching="true" Height="250px" AppendDataBoundItems="true">
                        </rad:RadComboBox>
                    </div>
                    <div class="col-md-1 pull-left no-padding">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                            ForeColor="Red" ControlToValidate="rcbProgramAccount" SetFocusOnError="True"
                            Display="Dynamic" CssClass="displayerror pull-left" ValidationGroup="addClass"
                            Text="*" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        Course
                    </div>
                    <div class="col-md-6 pull-left form-group no-padding">
                        <rad:RadComboBox ID="rcbCourse" runat="server" DefaultValue="0" RenderMode="Lightweight"
                            DataValueField="ClassCourseID" EmptyMessage="<%$ Resources:LocalizedResource, Select %>" ToolTip="<%$ Resources:LocalizedResource, SelectClassCourse%>"
                            CssClass="width-fill" DataTextField="ClassCourseName">
                        </rad:RadComboBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        Class
                    </div>
                    <div class="col-md-6 pull-left form-group no-padding">
                        <rad:RadComboBox ID="rcbClass" runat="server" DefaultValue="0" RenderMode="Lightweight"
                            DataValueField="ClassID" EmptyMessage="<%$ Resources:LocalizedResource, Select %>" ToolTip="Select Class" CssClass="width-fill"
                            DataTextField="ClassName">
                        </rad:RadComboBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        Audience
                    </div>
                    <div class="col-md-6 pull-left form-group no-padding">
                        <rad:RadComboBox ID="rcbAudience" runat="server" DefaultValue="0" RenderMode="Lightweight"
                            EmptyMessage="<%$ Resources:LocalizedResource, Select %>" ToolTip="Select Audience" CssClass="width-fill" DataValueField="AudienceID"
                            DataTextField="Audience">
                        </rad:RadComboBox>
                    </div>
                    <div class="col-md-1 pull-left no-padding">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                            ForeColor="Red" ControlToValidate="rcbAudience" SetFocusOnError="True" Display="Dynamic"
                            CssClass="displayerror pull-left" ValidationGroup="addClass" Text="*" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        Site
                    </div>
                    <div class="col-md-6 pull-left form-group no-padding">
                        <rad:RadComboBox ID="rcbSite" runat="server" DefaultValue="0" RenderMode="Lightweight"
                            DataValueField="CompanySiteID" DataTextField="CompanySite" EmptyMessage="<%$ Resources:LocalizedResource, Select %>"
                            AutoPostBack="true" OnSelectedIndexChanged="rcbSite_SelectedIndexChanged" ToolTip="Select Site"
                            CssClass="width-fill">
                        </rad:RadComboBox>
                    </div>
                    <div class="col-md-1 pull-left no-padding">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                            ForeColor="Red" ControlToValidate="rcbSite" SetFocusOnError="True" Display="Dynamic"
                            CssClass="displayerror pull-left" ValidationGroup="addClass" Text="*" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        Venue
                    </div>
                    <div class="col-md-6 pull-left form-group no-padding">
                        <rad:RadComboBox ID="rcbVenue" runat="server" DefaultValue="0" RenderMode="Lightweight"
                            DataValueField="VenueID" DataTextField="Venue" EmptyMessage="<%$ Resources:LocalizedResource, Select %>" ToolTip="Select Venue"
                            CssClass="width-fill">
                        </rad:RadComboBox>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-sm-12" style="font-weight: normal;">
                <div class="row">
                    <div class="col-md-6 col-md-offset-4 no-padding">
                        <asp:CustomValidator ID="CustomValidator2" runat="server" ErrorMessage="<%$ Resources:LocalizedResource, Sessionshouldbegreaterthan0%>"
                            SetFocusOnError="true" Display="Dynamic" ForeColor="Red" ControlToValidate="txtNoOfSessions"
                            ClientValidationFunction="validateGreaterOne" ValidationGroup="minimumValidation"
                            ValidateEmptyText="true"></asp:CustomValidator>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                         <asp:Label ID="lblTotalNoOfSessions1" runat="server" Text="<%$ Resources:LocalizedResource, TotalNoOfSessions %>"></asp:Label>
                        
                    </div>
                    <div class="col-md-6 pull-left form-group no-padding">
                        <asp:TextBox ID="txtNoOfSessions" runat="server" CssClass="form-control no-paddings js-number-filter"
                            Text="" ToolTip="Number of Sessions" ClientIDMode="Static" Style="padding: 0 0 0 5px;"
                            onkeypress='filterNumberOnly(event)' onpaste="filterNumberOnly(event)" placeholder="<%$ Resources:LocalizedResource, Sessionshouldbegreaterthan0%>"
                            TextMode="SingleLine" MaxLength="4"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <%--<div class="col-md-4">
                        Start Date / Time
                    </div>
                    <div class="col-md-6 pull-left form-group no-padding">
                        <rad:RadDateTimePicker ID="rdpStartDate" runat="server" RenderMode="Lightweight"
                            ToolTip="Set Start Date" CssClass="left-calendar-icon width-fill js-startdate"
                            AutoPostBackControl="Both" OnSelectedDateChanged="rdpStartDate_SelectedDateChanged">
                            <TimeView ID="TimeView1" runat="server" StartTime="00:00" EndTime="23:59" OnClientTimeSelecting="timeSelecting" />
                            <ClientEvents OnDateSelected="dateSelected" />
                            <DateInput ID="DateInput2" runat="server">
                            </DateInput>
                            <Calendar ID="Calendar1" runat="server">
                                <SpecialDays>
                                    <rad:RadCalendarDay Repeatable="Today" ItemStyle-BackColor="Gray">
                                    </rad:RadCalendarDay>
                                </SpecialDays>
                            </Calendar>
                        </rad:RadDateTimePicker>
                    </div>
                    <div class="col-md-1 pull-left no-padding">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                            ForeColor="Red" ControlToValidate="rdpStartDate" SetFocusOnError="True" Display="Dynamic"
                            CssClass="displayerror pull-left" ValidationGroup="addClass" Text="*" />
                    </div>--%>
                    <div class="col-md-4">
                         <asp:Label ID="lblStartDatetime1" runat="server" Text="<%$ Resources:LocalizedResource, StartDatetime %>"></asp:Label>                        
                    </div>
                    <%--OnSelectedDateChanged="rdpStartDate_SelectedDateChanged"--%>
                    <div class="col-md-6 pull-left form-group no-padding" id="rdpStartDatePanel">
                        <rad:RadDateTimePicker ID="rdpStartDate" runat="server" RenderMode="Lightweight"
                            ToolTip="Set Start Date" CssClass="left-calendar-icon width-fill js-startdate"
                            EnableTyping="false">
                            <%--<ClientEvents OnDateSelected="dateSelected" />--%>
                            <TimeView ID="TimeView1" runat="server" StartTime="00:00" EndTime="23:59" OnClientTimeSelecting="timeSelecting" />
                            <DateInput ID="DateInput2" runat="server" OnClientDateChanged="startDateSelected">
                            </DateInput>
                            <Calendar ID="Calendar1" runat="server">
                                <SpecialDays>
                                    <rad:RadCalendarDay Repeatable="Today" ItemStyle-BackColor="Coral">
                                    </rad:RadCalendarDay>
                                </SpecialDays>
                            </Calendar>
                        </rad:RadDateTimePicker>
                    </div>
                    <div class="col-md-1 pull-left no-padding">
                        <%--  <asp:CustomValidator ID="cvDate" runat="server" ErrorMessage="Start Date should be less than End Date"
                                                SetFocusOnError="true" Display="Dynamic" ForeColor="Red" ControlToValidate="rdpStartDate"
                                                ClientValidationFunction="validateStartEndDate" ValidationGroup="addClass"></asp:CustomValidator>--%>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                            ForeColor="Red" ControlToValidate="rdpStartDate" SetFocusOnError="True" Display="Dynamic"
                            CssClass="displayerror pull-left" ValidationGroup="addClass" Text="*" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-md-offset-4 no-padding">
                        <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="" SetFocusOnError="true"
                            Display="Dynamic" ForeColor="Red" ControlToValidate="rdpEndDate" ClientValidationFunction="validateStartEndDate"
                            ValidationGroup="dateValid" ValidateEmptyText="true"></asp:CustomValidator>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <asp:Label ID="lblEndDatetime1" runat="server" Text="<%$ Resources:LocalizedResource, EndDatetime %>"></asp:Label>  
                    </div>
                    <%--OnSelectedDateChanged="rdpEndDate_SelectedDateChanged"--%>
                    <div class="col-md-6 pull-left form-group no-padding" id="rdpEndDatePanel">
                        <rad:RadDateTimePicker ID="rdpEndDate" runat="server" RenderMode="Lightweight" ToolTip="<%$ Resources:LocalizedResource, SetEndDate%>"
                            CssClass="left-calendar-icon width-fill" EnableTyping="false">
                            <%--<ClientEvents OnDateSelected="EndDateSelected" />--%>
                            <TimeView ID="TimeView2" runat="server" StartTime="00:00" EndTime="23:59" OnClientTimeSelecting="timeSelecting" />
                            <%--<DateInput ID="DateInput2" runat="server">--%>
                            <DateInput ID="DateInput1" runat="server" OnClientDateChanged="EndDateSelected">
                            </DateInput>
                            <Calendar ID="Calendar2" runat="server">
                                <SpecialDays>
                                    <rad:RadCalendarDay Repeatable="Today" ItemStyle-BackColor="Coral">
                                    </rad:RadCalendarDay>
                                </SpecialDays>
                            </Calendar>
                        </rad:RadDateTimePicker>
                    </div>
                    <div class="col-md-1 pull-left no-padding">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="*"
                            ForeColor="Red" ControlToValidate="rdpEndDate" SetFocusOnError="True" Display="Dynamic"
                            CssClass="displayerror pull-left" ValidationGroup="addClass" Text="*" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-md-offset-4 no-padding">
                        <asp:CustomValidator ID="CustomValidator3" runat="server" ErrorMessage="Starting head count should be greater than 0."
                            SetFocusOnError="true" Display="Dynamic" ForeColor="Red" ControlToValidate="txtHeadCount"
                            ClientValidationFunction="validateGreaterOne" ValidationGroup="minimumValidation"
                            ValidateEmptyText="true"></asp:CustomValidator>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                         <asp:Label ID="lblStartingHeadcount1" Text="<%$ Resources:LocalizedResource, StartingHeadcount %>" runat="server" />  
                    </div>
                    <div class="col-md-6 pull-left form-group no-padding">
                        <asp:TextBox ID="txtHeadCount" runat="server" CssClass="form-control no-paddings"
                            Text="" ToolTip="<%$ Resources:LocalizedResource, StartingHeadcount %>" ClientIDMode="Static" Style="padding: 0 0 0 5px;"
                            onkeypress='filterNumberOnly(event)' onpaste="filterNumberOnly(event)" placeholder="Starting head count should be greater than 0"
                            TextMode="SingleLine" MaxLength="4"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <asp:Label ID="lblNoOfTrainingDays1" Text="<%$ Resources:LocalizedResource, NoOfTrainingDays %>" runat="server" />  
                    </div>
                    <div class="col-md-6 pull-left form-group no-padding">
                        <asp:TextBox ID="txtNoTrainingDays" runat="server" CssClass="form-control no-paddings"
                            ToolTip="<%$ Resources:LocalizedResource, NoOfTrainingDays %>" ClientIDMode="Static" Style="padding: 0 0 0 5px;"
                            placeholder="Select a valid Start date and End Date To Fill this" ReadOnly></asp:TextBox>
                    </div>
                    <div class="col-md-1 pull-left no-padding">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="*"
                            ForeColor="Red" ControlToValidate="txtNoTrainingDays" SetFocusOnError="True"
                            Display="Dynamic" CssClass="displayerror pull-left" ValidationGroup="addClass"
                            Text="*" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">                        
                         <asp:Label ID="lblTotalTrainingHours1" Text="<%$ Resources:LocalizedResource, TotalTrainingHours %>" runat="server" /> 
                    </div>
                    <div class="col-md-6 pull-left form-group no-padding">
                        <asp:TextBox ID="txtTotalTrainingHours" runat="server" CssClass="form-control no-paddings"
                            ToolTip="<%$ Resources:LocalizedResource, TotalTrainingHours %>" ClientIDMode="Static" Style="padding: 0 0 0 5px;"
                            placeholder="Select a validStart date and End Date To Fill this" ReadOnly></asp:TextBox>
                    </div>
                    <div class="col-md-1 pull-left no-padding">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="*"
                            ForeColor="Red" ControlToValidate="txtTotalTrainingHours" SetFocusOnError="True"
                            Display="Dynamic" CssClass="displayerror pull-left" ValidationGroup="addClass"
                            Text="*" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="font-bold">
                     <asp:Label ID="lblClassNameAndDescription1" Text="<%$ Resources:LocalizedResource, ClassNameAndDescription %>" runat="server" />  
                </div>
            </div>
            <div class="col-md-5">
                <div class="font-bold">
                    Trainer Details
                </div>
            </div>
            <hr style="border-top: dotted 1px;" />
        </div>
        <div class="row">
            <div class="col-lg-6 col-sm-12" style="font-weight: normal;">
                <div class="row">
                    <div class="col-md-4">
                        Class Name
                    </div>
                    <div class="col-md-6 pull-left form-group no-padding">
                        <asp:TextBox ID="txtClassName" runat="server" CssClass="form-control " ToolTip="Class Name"
                            ViewStateMode="Enabled" ClientIDMode="Static" MaxLength="100" onkeyup="CheckMaxLength(this,100);"></asp:TextBox>
                        <label class="text-danger text-right js-char-counter">
                            <span id="txtClassNameChars" runat="server">100</span> characters remaining</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        
                        <asp:Label ID="lblClassDescription2" Text="<%$ Resources:LocalizedResource, ClassDescription %>" runat="server" />  
                    </div>
                    <div class="col-md-6 pull-left form-group no-padding">
                        <rad:RadTextBox ID="txtClassDesc" TextMode="MultiLine" runat="server" CssClass="form-control MultiLineTextBox btn-flat"
                            RenderMode="Lightweight" Skin="Bootstrap" ViewStateMode="Enabled" Height="120px"
                            Width="100%" MaxLength="1500" ClientIDMode="Static" placeholder="What is this class about?"
                            onkeyup="CheckMaxLength(this,1500);">
                        </rad:RadTextBox>
                        <%-- <asp:TextBox ID="txtClassDesc" runat="server" CssClass="form-control MultiLineTextBox"
                            ToolTip="Class Description" ViewStateMode="Enabled" Height="120px" TextMode="MultiLine"
                            ClientIDMode="Static" placeholder="What is this class about?"></asp:TextBox>--%>
                        <label class="text-danger text-right text-right js-char-counter">
                            <span id="txtClassDescChars" runat="server">1500</span> characters remaining</label>
                    </div>
                </div>
                <br />
                <div class="col-md-6 col-md-offset-4">
                    <div class="row img-dropzone-md" style="margin: 0; width: 100% !important; height: 25vh;">
                        <div class="dropzone-empty" style="padding-top: 2em;">
                            <asp:Image ID="Image4" ImageUrl="~/Media/Images/icon-upload.png" runat="server" AlternateText="Upload Image"
                                Height="2.875em" Widt h="2.875em" />
                            <br />
                            <p>                                
                                <asp:Label runat="server" ID="lblDragImageFileHere" Text="<%$ Resources:LocalizedResource, DragImageFileHere %>" ></asp:Label>
                            </p>
                        </div>
                        <rad:RadAsyncUpload ID="ClassImage" runat="server" MultipleFileSelection="Disabled"
                            ViewStateMode="Enabled" MaxFileInputsCount="1" ControlObjectsVisibility="None"
                            Width="100%" HideFileInput="false" ToolTip="Select Class Image" CssClass="ClassImage display-none"
                            DropZones=".img-dropzone-md" ForeColor="#ddd" OnClientFilesUploaded="fileUploaded"
                            OnClientFileSelected="fileSelected" OnFileUploaded="upload_FileUploaded" AllowedFileExtensions="jpeg,jpg,gif,png">
                            <%-- <FileFilters>
                                                                <rad:FileFilter Description="Images(jpeg;jpg;gif;png)" Extensions="jpeg,jpg,gif,png" />
                                                            </FileFilters>--%>
                        </rad:RadAsyncUpload>
                        <div id="DropzonePreview" class="dropzone-preview" onmouseover="dropzoneEnter()"
                            onmouseout="dropzoneExit()" runat="server">
                            <rad:RadBinaryImage ID="classImagePreview" runat="server" CssClass="box-fluid" ClientIDMode="Static" />
                            <div class="box-fluid tc-overlay">
                                <asp:LinkButton ID="btnRemovePreviewImg" runat="server" ToolTip="Remove Image" OnClick="btnRemovePreviewImg_Click"
                                    OnClientClick="showImageDropzonePreviewLoading();">
                                                                    <i class="fa fa-times-circle-o previewClose" style="color: white; position: absolute;
                                                                    top: 50%; left: 50%; transform: translate(-50%,-50%); font-size: 5em; cursor: pointer;">
                                                                    </i>
                                </asp:LinkButton>
                            </div>
                        </div>
                    </div>
                    <br />
                    <asp:Button ID="Button1" Text="<%$ Resources:LocalizedResource, SelectFile %>" runat="server" CssClass="btn btn-default border-lightgray btn-sm btn-flat btn-thin relative-hr-center "
                        OnClientClick="openFileUploader(); return false;" />
                </div>
            </div>
            <div class="col-lg-6 col-sm-12" style="font-weight: normal;">
                <div class="row">
                    <div class="col-md-4">
                        CIM
                    </div>
                    <div class="col-md-6 pull-left form-group no-padding">
                        <asp:TextBox ID="txtTrainerCIM" runat="server" CssClass="form-control no-paddings"
                            TextMode="SingleLine" MaxLength="8" ToolTip="Trainer CIM" ViewStateMode="Enabled"
                            ClientIDMode="Static" Style="padding: 0 0 0 5px;" placeholder="Enter the Trainer CIM"
                            onkeypress='filterNumberOnly(event)'></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <asp:Label runat="server" ID="lblName" Text="<%$ Resources:LocalizedResource, Name %>" ></asp:Label>
                    </div>
                    <div class="col-md-6 pull-left form-group no-padding" id="pnlTrainerName">
                        <asp:TextBox ID="txtTrainerName" runat="server" CssClass="form-control no-paddings"
                            ReadOnly="true" Enabled="false" ToolTip="Trainer Name" ViewStateMode="Enabled"
                            ClientIDMode="Static" Style="padding: 0 0 0 5px;"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">                        
                        <asp:Label runat="server" ID="lblTrainerSupervisorName1" Text="<%$ Resources:LocalizedResource, TrainerSupervisorName %>" ></asp:Label>
                    </div>
                    <div class="col-md-6 pull-left form-group no-padding" id="pnlTrainerSup">
                        <asp:TextBox ID="txtSupName" runat="server" CssClass="form-control no-paddings" ReadOnly="true"
                            Enabled="false" ToolTip="<%$ Resources:LocalizedResource, TrainerSupervisorName %>" ViewStateMode="Enabled" ClientIDMode="Static"
                            Style="padding: 0 0 0 5px;"></asp:TextBox>
                    </div>
                    <asp:Button ID="btnLoadTrainerDetails" runat="server" CssClass="display-none" OnClick="btnLoadTrainerDetails_Click" />
                </div>
            </div>
        </div>
        <div class="row btn-group-center">
            <%--<rad:RadButton ID="btnCancelEdit" runat="server" ToolTip="Cancel" OnClick="btnCancelEditDetails_Click"
                CssClass="btn btn-sm btn-teal btn-flat" Text="Cancel" ButtonType="SkinnedButton"
                RenderMode="Lightweight" Skin="Bootstrap">
            </rad:RadButton>--%>
            <asp:LinkButton ID="btnCancelEdit" runat="server" Font-Underline="false" CssClass="btn btn-sm btn-teal btn-flat"
                aria-hidden="true" ToolTip="<%$ Resources:LocalizedResource, Cancel %>" OnClick="btnCancelEditDetails_Click" Text="<%$ Resources:LocalizedResource, Cancel %>"></asp:LinkButton>
            <asp:LinkButton ID="btnSaveChanges" runat="server" Font-Underline="false" CssClass="btn btn-sm btn-teal btn-flat"
                aria-hidden="true" ToolTip="Save Changes" OnClientClick="ConfirmUpdateClass(); return false;"
                Text="Save Changes"></asp:LinkButton>
            <%--   <rad:RadButton ID="btnSaveChanges" runat="server" ToolTip="Save Changes" ValidationGroup="addClass"
                CausesValidation="true" CssClass="btn btn-sm btn-teal btn-flat" Text="Save Changes"
                ButtonType="SkinnedButton" RenderMode="Lightweight" Skin="Bootstrap" OnClientClicking="ConfirmUpdateClass">
            </rad:RadButton>--%>
            <%--ConfirmUpdateClass--%>
        </div>
        <asp:HiddenField ID="hfIsTrainerCIMValid" runat="server" />
        <asp:HiddenField ID="hfErrorMessage" runat="server" Value="<%$ Resources:LocalizedResource, ErrorMessage %>"/>

    </asp:Panel>
</asp:Panel>
<asp:Button CssClass="display-none" runat="server" ID="btnFakeAjax" />
<rad:RadWindowManager ID="RadWindowManager1_cd" RenderMode="Lightweight" EnableShadow="true"
    Skin="Bootstrap" Modal="true" VisibleOnPageLoad="false" Behaviors="Close, Move"
    RestrictionZoneID="RestrictionZone" Opacity="99" runat="server">
    <Windows>
    </Windows>
</rad:RadWindowManager>
<rad:RadScriptBlock ID="RadScriptBlock" runat="server">
    <script type="text/javascript">
        var trainerCIMValid = false;
        var ErrorMessage = "";        

        function initializeTrainerDetailLoader() {
            trainerCIMValid = $("#<%= hfIsTrainerCIMValid.ClientID %>").val() == "1" ? true : false;
            $("#txtTrainerCIM").blur(function (e) {
                e.stopImmediatePropagation();
                if ($("#txtTrainerCIM").val() != "") {
                    hideTrainerDetailLoading();
                    showTrainerDetailLoading();

                    setTrainerSupDetails();
                    //$("#<%= btnLoadTrainerDetails.ClientID %>").click();
                } else {
                    trainerCIMValid = true;
                    $("#txtTrainerName").val("");
                    $("#txtSupName").val("");
                }
            })

            $("#txtTrainerCIM").keypress(function (e) {
                e.stopImmediatePropagation();
                if (e.which == 13) {
                    $(this).blur();
                    //                        hideTrainerDetailLoading();
                    //                        showTrainerDetailLoading();

                    //                        setTrainerSupDetails();
                    //$("#<%= btnLoadTrainerDetails.ClientID %>").click();
                }

            });
        }

        function setTrainerSupDetails() {
            trainerCIMValid = false;
            var trainerCIM = $("#txtTrainerCIM").val().trim();
            ErrorMessage = $("#<%= hfErrorMessage.ClientID %>").val().trim();
            if (trainerCIM.length > 0) {
                $.ajax({
                    type: "POST",
                    data: "{userCIM: " + trainerCIM + "}",
                    contentType: "application/json; charset=utf-8",
                    url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "GetSupervisorDetails",
                    dataType: 'json',
                    success: function (res) {

                        if (res.d == null) {

                            radalert("Trainer CIM is not valid or the user is not enrolled in TU.", 330, 180, ErrorMessage);
                            //$("#txtTrainerCIM").select();
                            $("#txtTrainerName").val("");
                            $("#txtSupName").val("");
                            //$("#txtTrainerCIM").focus();
                        }
                        else {
                            trainerCIMValid = true;
                            var trainername = res.d.Name;
                            var supname = res.d.SupervisorName;
                            $("#txtTrainerName").val(trainername);
                            $("#txtSupName").val(supname);
                        }

                        hideAllLocalLoading();



                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        var code = XMLHttpRequest.status;
                        if (code == "401")
                            window.location.reload();
                        hideAllLocalLoading();
                        //$("#txtTrainerCIM").focus();
                    }
                });
            }

        }

        function fileUploaded(sender, args) {
            var upload = $find("<%= ClassImage.ClientID %>");
            var inputs = upload.getUploadedFiles();

            if (!upload.isExtensionValid(inputs[0]))
                alert("extension is invalid!");


            var radManager = $find('<%= RadAjaxManager.GetCurrent(Page).ClientID %>');
            radManager.ajaxRequest();


            setTimeout(function () {
                hideAllLocalLoading();
            }, 3000);

        }

        function fileSelected(sender, args) {

            showImageDropzonePreviewLoading();
            var fileExtention = args.get_fileName().substring(args.get_fileName().lastIndexOf('.') + 1, args.get_fileName().length);
            if (args.get_fileName().lastIndexOf('.') != -1) {//this checks if the extension is correct
                if (sender.get_allowedFileExtensions().toLowerCase().indexOf(fileExtention.toLowerCase()) == -1) {
                    $("#" + "<%= classImagePreview.ClientID%>").attr('src', "").hide();
                    removePreviewImage();
                    radalert("Invalid File Type.<br> Allowed Files are JPG, GIF, PNG.", 330, 180, ErrorMessage, "");
                }
                else {
                    console.log("File Supported.");
                }
            }
            else {
                console.log("not correct extension.");

            }

        }

        function showImageDropzonePreviewLoading() {
            var panel = $find("<%= localLoadingPanel.ClientID %>");
            panel.show("classImagePreview");
        }

        function hideAllLocalLoading() {
            $(".Loading2").hide();
            console.log("hide loading");
        }

        function dropzoneEnter() {

            if ($("#" + "<%= classImagePreview.ClientID %>").is('[src]')) {
                $(".tc-overlay").css({ "display": "block", "opacity": "1" });
            }
        }

        function dropzoneExit() {
            var a = $("#" + "<%= classImagePreview.ClientID %>").attr('src');
            $(".tc-overlay").css({ "display": "none", "opacity": "0" });

        }

        function removePreviewImage() {
            try {
                var upload = $find("<%= ClassImage.ClientID %>");
                upload.deleteAllFileInputs();
                hideAllLocalLoading();
            } catch (e) {

            }
        }

        function openFileUploader() {
            $telerik.$(".ruFileInput").click();
        }

        function showTrainerDetailLoading() {
            var panel = $find("<%= localLoadingPanel.ClientID %>");
            panel.show("pnlTrainerName");
            panel.show("pnlTrainerSup");

        }
        function hideTrainerDetailLoading() {
            var panel = $find("<%= localLoadingPanel.ClientID %>");
            panel.hide("pnlTrainerName");
            panel.hide("pnlTrainerSup");

        }
    </script>
    <%--additional for time picker--%>
    <script type="text/javascript">
        // using a variable to prevent infinite loop
        var isTimeSet = false;
        function timeSelecting(sender, args) {
            isTimeSet = true;
        }

        function startDateSelected(sender, args) {

            if (args.get_newDate() && !isTimeSet) {
                //isTimeSet = true;
                $find("<%= rdpStartDate.ClientID %>").get_timeView().setTime(00, 0, 0, 0);
            }
            if (isTimeSet) {
                isTimeSet = false;
            }

            var StartDate = $find("<%= rdpStartDate.ClientID %>")
            var EndDate = $find("<%= rdpEndDate.ClientID %>");

            if (StartDate.get_selectedDate() == null || EndDate.get_selectedDate() == null) {
                $("#txtNoTrainingDays").val("");
                $("#txtTotalTrainingHours").val("");
            } else {
                var totalWorkingDays = workingDaysBetweenDates(new Date(StartDate.get_selectedDate()), new Date(EndDate.get_selectedDate()));
                var totalHours = totalWorkingDays * 8;
                //workingHoursBetweenDates(startDate, endDate)
                $("#txtNoTrainingDays").val(totalWorkingDays == 0 ? "" : totalWorkingDays);
                $("#txtTotalTrainingHours").val(totalHours == 0 ? "" : totalHours);
            }

            ////////////
            var _EndDate = EndDate.get_selectedDate();
            var _StartDate = StartDate.get_selectedDate();
            _EndDate = _EndDate == null ? new Date(2099, 12, 30, 23, 0, 0) : _EndDate;
            _StartDate = new Date(_StartDate);
            _EndDate = new Date(_EndDate);
            var valid = (_EndDate > _StartDate);

            if (!valid) {
                radalert("The Start date should be less than End Date", 330, 180, ErrorMessage);
                // $find("<%= rdpStartDate.ClientID %>").get_textBox().focus();
            }
        }


        function EndDateSelected(sender, args) {

            if (args.get_newDate() && !isTimeSet) {
                isTimeSet = true;
                $find("<%= rdpEndDate.ClientID %>").get_timeView().setTime(23, 0, 0, 0);
                args.set_cancel(true);
            }
            if (isTimeSet) {
                isTimeSet = false;
            }
            var StartDate = $find("<%= rdpStartDate.ClientID %>")
            var EndDate = $find("<%= rdpEndDate.ClientID %>");

            if (StartDate.get_selectedDate() == null || EndDate.get_selectedDate() == null) {
                $("#txtNoTrainingDays").val("");
                $("#txtTotalTrainingHours").val("");
            } else {
                var totalWorkingDays = workingDaysBetweenDates(new Date(StartDate.get_selectedDate()), new Date(EndDate.get_selectedDate()));
                var totalHours = totalWorkingDays * 8;
                //workingHoursBetweenDates(startDate, endDate)
                $("#txtNoTrainingDays").val(totalWorkingDays == 0 ? "" : totalWorkingDays);
                $("#txtTotalTrainingHours").val(totalHours == 0 ? "" : totalHours);
            }
        }
        function valueChanging(sender, args) {
            if (args.get_newValue().indexOf(":") > 0) {
                isTimeSet = true;
            }
        }

        //CHECK LENGTH GIVEN BY THE PARAMETER
        function CheckMaxLength(sender, Maxlength) {

            var length = $(sender).val().length;
            if (sender.value.length > Maxlength) {

                sender.value = sender.value.substr(0, Maxlength);
            }

            var length = Maxlength - length;
            if (!$(sender).hasClass("MultiLineTextBox"))
                $(sender).siblings(".js-char-counter").find("span").text(length);
            else
                $(sender).parent().siblings(".js-char-counter").find("span").text(length);

        }

        function workingDaysBetweenDates(startDate, endDate) {

            // Validate input
            if (endDate < startDate)
                return 0;

            // Calculate days between dates
            var millisecondsPerDay = 86400 * 1000; // Day in milliseconds
            startDate.setHours(0, 0, 0, 1);  // Start just after midnight
            endDate.setHours(23, 59, 59, 999);  // End just before midnight
            var diff = endDate - startDate;  // Milliseconds between datetime objects    
            var days = Math.ceil(diff / millisecondsPerDay);

            // Subtract two weekend days for every week in between
            var weeks = Math.floor(days / 7);
            days = days - (weeks * 2);

            // Handle special cases
            var startDay = startDate.getDay();
            var endDay = endDate.getDay();

            // Remove weekend not previously removed.   
            if (startDay - endDay > 1)
                days = days - 2;

            // Remove start day if span starts on Sunday but ends before Saturday
            if (startDay == 0 && endDay != 6)
                days = days - 1

            // Remove end day if span ends on Saturday but starts after Sunday
            if (endDay == 6 && startDay != 0)
                days = days - 1

            return days;
        }

        function workingHoursBetweenDates(startDate, endDate) {

            // Validate input
            if (endDate < startDate)
                return 0;

            // Calculate days between dates
            var millisecondsPerDay = 86400 * 1000; // Day in milliseconds
            var millisecondsPerHour = 3600 * 1000; // Day in milliseconds
            startDate.setHours(0, 0, 0, 1);  // Start just after midnight
            endDate.setHours(23, 59, 59, 999);  // End just before midnight
            var diff = endDate - startDate;  // Milliseconds between datetime objects    
            var days = Math.ceil(diff / millisecondsPerDay);
            var hours = Math.ceil(diff / millisecondsPerHour);

            // Subtract two weekend days for every week in between
            var weeks = Math.floor(hours / 168);
            hours = hours - (weeks * 48);

            // Handle special cases
            var startDay = startDate.getDay();
            var endDay = endDate.getDay();

            // Remove weekend not previously removed.   
            if (startDay - endDay > 1)
                hours = hours - 48;

            // Remove start day if span starts on Sunday but ends before Saturday
            if (startDay == 0 && endDay != 6)
                hours = hours - 24

            // Remove end day if span ends on Saturday but starts after Sunday
            if (endDay == 6 && startDay != 0)
                hours = hours - 24

            //return hours - (workingDaysBetweenDates );
        }

        function validateStartEndDate(sender, args) {
            debugger;
            var startDate = $find("<%= rdpStartDate.ClientID %>").get_selectedDate();
            var endDate = $find("<%= rdpEndDate.ClientID %>").get_selectedDate();

            args.IsValid = false;
            if (startDate != null && endDate != null) {
                endDate = endDate == null ? new Date(2099, 12, 30, 23, 0, 0) : endDate;
                //                console.log(startDate);
                //                console.log(endDate);
                //                console.log(endDate > startDate);
                var valid = (endDate > startDate);

                args.IsValid = valid;
                if ((!valid && !isTimeSet)) {
                    radalert("The End date should be greater than Start Date", 330, 180, ErrorMessage);
                }
            } else {
                if (startDate == null)
                    $find("<%= rdpStartDate.ClientID %>").get_dateInput().focus();
                else if (endDate == null)
                    $find("<%= rdpEndDate.ClientID %>").get_dateInput().focus();
            }
        }

        function filterNumberOnly(evt) {
            var theEvent = evt || window.event;

            // Handle paste
            if (theEvent.type === 'paste') {
                key = event.clipboardData.getData('text/plain');
            } else {
                // Handle key press
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
            }
            var regex = /[0-9]/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }


        function setInputFilter(textbox, inputFilter) {
            ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function (event) {
                textbox.addEventListener(event, function () {
                    if (inputFilter(this.value)) {
                        this.oldValue = this.value;
                        this.oldSelectionStart = this.selectionStart;
                        this.oldSelectionEnd = this.selectionEnd;
                    } else if (this.hasOwnProperty("oldValue")) {
                        this.value = this.oldValue;
                        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                    }
                });
            });
        }

        function validateGreaterOne(sender, args) {
            debugger;
            try {
                var numberInput = parseInt(args.Value.trim() == "" ? "0" : args.Value.trim());
                var valid = numberInput >= 1
                args.IsValid = valid;
                if (!valid) {
                    $("#" + sender.controltovalidate).focus();
                }
            } catch (e) {
                alert("The input is not a valid number.");
            }


        }


        function ConfirmUpdateClass() {
            debugger;
            trainerCIMValid = $("#txtTrainerCIM").val().trim() == "" ? true : trainerCIMValid;
            var callBackFunction = function (shouldSubmit) {
                if (shouldSubmit) {
                    $find("<%= btnHiddenSaveDetails.ClientID %>").get_element().click();
                }
            };



            if (Page_ClientValidate('addClass')) {

                if (Page_ClientValidate('dateValid')) {

                    if (Page_ClientValidate('minimumValidation')) {
                        if (trainerCIMValid) {
                            var text = "Are you sure you want save your changes in this class?";
                            radconfirm(text, callBackFunction, 330, 180, null, "Confirm");
                        } else
                            radalert("Trainer CIM is not valid or the user is not enrolled in TU.", 330, 180, ErrorMessage);

                    }

                }


            } else {
                $('body,html').animate({
                    scrollTop: $("#txtNoOfSessions").offset().top - 65
                }, 300);
                return;
            }
            return false;
        }

    </script>
</rad:RadScriptBlock>
