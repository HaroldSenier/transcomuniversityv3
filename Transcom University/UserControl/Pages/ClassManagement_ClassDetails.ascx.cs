﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TranscomUniversityV3Model;
using Telerik.Web.UI;
using Telerik.Web.UI.Calendar;
using System.IO;

public partial class UserControl_Pages_ClassManagement_ClassDetails : System.Web.UI.UserControl
{
    private static int CLASSID;

    private static string global_classImage;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Request.QueryString["ClassId"] != null)
        {
            CLASSID = Convert.ToInt32(Utils.Decrypt(Request.QueryString["ClassId"]));
            if (!Page.IsPostBack)
                loadClassDetails();

        }

       

    }

    private void loadClassDetails()
    {
        try
        {
            var db = new TranscomUniversityV3ModelContainer();
            var d = DataHelper.GetClassDetails(CLASSID);

            lblAccount.Text = d.Account;
            lblCourse.Text = d.ClassCourseName;
            lblClass.Text = d.ClassTrainingName;
            lblAudience.Text = d.Audience;
            lblSite.Text = d.CompanySite == null ? "-" : d.CompanySite;
            lblVenue.Text = d.Venue == null ? "-" : d.Venue;

            lblSession.Text = d.NumOfSessions + "";
            lblStartDate.Text = String.Format("{0:MM/dd/yyyy hh:mm tt}", d.StartDate);
            lblEndDate.Text = String.Format("{0:MM/dd/yyyy hh:mm tt}", d.EndDate);
            lblHeadCount.Text = d.StartingHeadCount + "";
            lblTrainingDays.Text = d.NumOfTrainingDays + "";
            lblTrainingHours.Text = d.TotalTrainingHours + "";

            lblClassName.Text = d.ClassName;
            lblClassDescription.Text = d.ClassDescription;

            lblTrainerCIM.Text = (d.TrainerCIM != null && d.TrainerCIM != 0) ? d.TrainerCIM + "" : "-";
            lblTrainerName.Text = d.TrainerName != null ? d.TrainerName : "-";

            try
            {
                string supName = DataHelper.GetSupervisorDetails(d.TrainerCIM).SupervisorName;
                lblTrainerSup.Text = supName == null ? "-" : supName;
            }
            catch (Exception)
            {

                lblTrainerSup.Text = "-";
            }
           
           
  

            // classImageContainer.ImageUrl = "~/Media/Uploads/ClassImg/" + CLASSID + "/" + d.ClassImageUrl;
            string className =  d.ClassName.Length > 100 ? Utils.ClipStringTo100(d.ClassName) :d.ClassName;
            string classDesc = d.ClassDescription.Length > 500 ? Utils.ClipStringTo500(d.ClassDescription) :d.ClassDescription;
            string classImage = d.ClassImageUrl != null ? "Media/Uploads/ClassImg/" + CLASSID + "/" + d.ClassImageUrl : "Media/Uploads/ClassImg/No_image.jpg";
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", Utils.callClientScript("refreshClassPreview", className, classDesc, HttpUtility.UrlPathEncode(classImage)), true);


        }
        catch (Exception)
        {

            throw;
        }
    }

    private void loadEditDetails()
    {
        try
        {
            var db = new TranscomUniversityV3ModelContainer();
            var d = DataHelper.GetClassDetails(CLASSID);

            rcbProgramAccount.DataSource = db.tbl_TranscomUniversity_Lkp_Client.Where(c => c.HideFromList == false).ToList();
            rcbProgramAccount.DataBind();


            rcbCourse.DataSource = db.tbl_transcomuniversity_lkp_classCourse.Where(c => c.HideFromList == false).ToList();
            rcbCourse.DataBind();

            rcbClass.DataSource = db.tbl_transcomuniversity_lkp_classes.Where(c => c.HideFromList == false).ToList();
            rcbClass.DataBind();

            rcbAudience.DataSource = db.tbl_TranscomUniversity_lkp_Audience.Where(c => c.HideFromList == false).ToList();
            rcbAudience.DataBind();

            int siteid = d.SiteID;
            rcbSite.DataSource = db.pr_TranscomUniversity_Lkp_Sites(siteid);
            rcbSite.DataBind();
            rcbVenue.DataSource = db.tbl_TranscomUniversity_Lkp_Venue.Where(v => v.SiteID == siteid).ToList();
            rcbVenue.DataBind();


            rcbProgramAccount.SelectedIndex = rcbProgramAccount.FindItemIndexByText(d.Account);
            rcbCourse.SelectedIndex = rcbCourse.FindItemIndexByText(d.ClassCourseName);
            rcbClass.SelectedIndex = rcbClass.FindItemIndexByText(d.ClassTrainingName);
            rcbAudience.SelectedIndex = rcbAudience.FindItemIndexByText(d.Audience);

            rcbSite.SelectedIndex = rcbSite.FindItemIndexByText(d.CompanySite);
            rcbVenue.SelectedIndex = rcbVenue.FindItemIndexByText(d.Venue);


            rdpStartDate.SelectedDate = d.StartDate;
            rdpEndDate.SelectedDate = d.EndDate;
            txtNoOfSessions.Text = d.NumOfSessions + "";
            txtNoTrainingDays.Text = d.NumOfTrainingDays + "";
            txtTotalTrainingHours.Text = d.TotalTrainingHours + "";
            txtHeadCount.Text = d.StartingHeadCount + "";




            txtTrainerCIM.Text = (d.TrainerCIM != null && d.TrainerCIM != 0) ? d.TrainerCIM + "" : "";
            txtTrainerName.Text = d.TrainerName != null ? d.TrainerName : "";
            hfIsTrainerCIMValid.Value = (d.TrainerCIM != null && d.TrainerCIM != 0) ? "1" : "0";
                
            //txtManagerName.Text = d.TrainerManagerName;
            try
            {
                txtSupName.Text = (d.TrainerCIM != null && d.TrainerCIM != 0) ? DataHelper.GetSupervisorDetails(d.TrainerCIM).SupervisorName : "-";
            }
            catch (Exception)
            {

                txtSupName.Text = ""; 
            }
            txtClassName.Text = d.ClassName;
            txtClassNameChars.InnerText = (100 - txtClassName.Text.Length).ToString();
            txtClassDesc.Text = d.ClassDescription;
            txtClassDescChars.InnerText = (1500 - txtClassDesc.Text.Length).ToString();
            global_classImage = d.ClassImageUrl != null ? "~/Media/Uploads/ClassImg/" + CLASSID + "/" + d.ClassImageUrl : "~/Media/Uploads/ClassImg/No_image.jpg";
            classImagePreview.ImageUrl = global_classImage;

        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void btnEditClassDetails_Click(object sender, EventArgs e)
    {
        btnSaveDetails.Visible = true;
        btnEditClassDetails.Visible = false;
        pnlDetailView.Visible = false;
        pnlDetailsEdit.Visible = true;
        loadEditDetails();

    }

    protected void btnCancelEditDetails_Click(object sender, EventArgs e)
    {
        btnSaveDetails.Visible = false;
        btnEditClassDetails.Visible = true;
        pnlDetailView.Visible = true;
        pnlDetailsEdit.Visible = false;
    }

    protected void btnSaveChanges_Click(object sender, EventArgs e)
    {
        if (rcbProgramAccount.SelectedValue == "" || rcbSite.SelectedValue == "" || rdpStartDate.SelectedDate == null || rdpEndDate.SelectedDate == null)
        {
            rcbProgramAccount.Focus();
            RadWindowManager1_cd.RadAlert("Please fill all required fields", 330, 180, "Error Message", "");
        }
        else
        {
            try
            {
                DateTime firstDay = Convert.ToDateTime(rdpStartDate.SelectedDate);
                DateTime lastDay = Convert.ToDateTime(rdpEndDate.SelectedDate);

                int trainingDays = BusinessDaysUntil(firstDay, lastDay);
                int trainingHours = Convert.ToInt32(trainingDays) * 8;
                string classImage = global_classImage.Split('/')[global_classImage.Split('/').Count() - 1];

                var db = new TranscomUniversityV3ModelContainer();

                //Logger variables Start
                string _userIP = DataHelper.GetIPAddress();
                string _userID = DataHelper.GetCurrentUserCIM();
                double _duration = 0;

                DateTime startTime;
                DateTime endTime;

                var dts = db.CreateQuery<DateTime>("CurrentDateTime() ");
                startTime = dts.AsEnumerable().First();
                //Logger variables End
                int? trainerCIM;
                if (txtTrainerCIM.Text.Trim() == "")
                    trainerCIM = null;
                else if (DataHelper.GetSupervisorDetails(Convert.ToInt32(txtTrainerCIM.Text.Trim())) == null)
                {
                    RadWindowManager1_cd.RadAlert("Trainer CIM is not Valid.", 330, 180, "Invalid Trainer CIM", "");
                    return;
                }

                else
                    trainerCIM = Convert.ToInt32(txtTrainerCIM.Text.Trim());
                db.pr_TranscomUniversity_UpdateClass(
                    Convert.ToInt32(rcbProgramAccount.SelectedValue),
                    CLASSID,
                    txtClassName.Text.Trim(),
                     txtClassDesc.Text.Trim(),
                     trainerCIM,
                     Convert.ToInt32(rcbCourse.SelectedValue),
                     Convert.ToInt32(rcbClass.SelectedValue),
                     Convert.ToInt32(rcbAudience.SelectedValue),
                     Convert.ToInt32(rcbSite.SelectedValue),
                     Convert.ToInt32(rcbVenue.SelectedValue),
                     Convert.ToInt32(txtNoOfSessions.Text.Trim()),
                     rdpStartDate.SelectedDate,
                     rdpEndDate.SelectedDate,
                     Convert.ToInt32(txtHeadCount.Text.Trim()),
                     trainingDays,
                     trainingHours,
                     classImage);

                btnSaveDetails.Visible = false;
                btnEditClassDetails.Visible = true;
                pnlDetailView.Visible = true;
                pnlDetailsEdit.Visible = false;

                //logger start transaction
                string _action = "Class Details was updated by " + _userID;

                var dte = db.CreateQuery<DateTime>("CurrentDateTime() ");

                endTime = dte.AsEnumerable().First();
                _duration = (endTime - startTime).TotalMilliseconds;

                int l_actionID = DataHelper.getLastLogID() + 1;
                DataHelper.logClass(CLASSID, l_actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);
                //logger end transaction

                RadWindowManager1_cd.RadAlert("Class was successfully updated.", 330, 180, "Success Message", "");
                loadClassDetails();
            }
            catch (Exception)
            {
                RadWindowManager1_cd.RadAlert("Error updating class. Please contact your System Administrator.", 330, 180, "Error Message", "");
                throw;
            }
        }
    }

    private static int BusinessDaysUntil(DateTime firstDay, DateTime lastDay)
    {
        firstDay = firstDay.Date;
        lastDay = lastDay.Date;
        if (firstDay > lastDay)
            throw new ArgumentException("Incorrect last day " + lastDay);

        TimeSpan span = lastDay - firstDay;
        int businessDays = span.Days + 1;
        int fullWeekCount = businessDays / 7;
        // find out if there are weekends during the time exceedng the full weeks
        if (businessDays > fullWeekCount * 7)
        {
            // we are here to find out if there is a 1-day or 2-days weekend
            // in the time interval remaining after subtracting the complete weeks
            int firstDayOfWeek = firstDay.DayOfWeek == DayOfWeek.Sunday ? 7 : (int)firstDay.DayOfWeek;
            int lastDayOfWeek = lastDay.DayOfWeek == DayOfWeek.Sunday ? 7 : (int)lastDay.DayOfWeek;
            if (lastDayOfWeek < firstDayOfWeek)
                lastDayOfWeek += 7;
            if (firstDayOfWeek <= 6)
            {
                if (lastDayOfWeek >= 7)// Both Saturday and Sunday are in the remaining time interval
                    businessDays -= 2;
                else if (lastDayOfWeek >= 6)// Only Saturday is in the remaining time interval
                    businessDays -= 1;
            }
            else if (firstDayOfWeek <= 7 && lastDayOfWeek >= 7)// Only Sunday is in the remaining time interval
                businessDays -= 1;
        }

        // subtract the weekends during the full weeks in the interval
        businessDays -= fullWeekCount + fullWeekCount;

        //// subtract the number of bank holidays during the time interval
        //foreach (DateTime bankHoliday in bankHolidays)
        //{
        //    DateTime bh = bankHoliday.Date;
        //    if (firstDay <= bh && bh <= lastDay)
        //        --businessDays;
        //}

        return businessDays;

    }

    #region EditClassDetails

    protected void rcbSite_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        try
        {
            int siteId = Convert.ToInt32(rcbSite.SelectedValue);

            var db = new TranscomUniversityV3ModelContainer();
            var venue = db.tbl_TranscomUniversity_Lkp_Venue
                            .Where(v => v.SiteID == siteId)
                            .ToList();

            rcbVenue.DataSource = venue;
            rcbVenue.DataBind();
        }
        catch (Exception)
        {
            RadWindowManager1_cd.RadAlert("Invalid input. Please try again", 330, 180, "Error Message", "");
            throw;
        }
    }

    protected void rdpStartDate_SelectedDateChanged(object sender, SelectedDateChangedEventArgs e)
    {
        DateTime firstDay = Convert.ToDateTime(rdpStartDate.SelectedDate);
        DateTime lastDay = Convert.ToDateTime(rdpEndDate.SelectedDate);

        if (firstDay > lastDay)
        {
            txtNoTrainingDays.Text = "";
            txtTotalTrainingHours.Text = "";
        }
        else
        {
            double trainingDays = Math.Round(1 + ((lastDay - firstDay).TotalDays * 5 - (firstDay.DayOfWeek - lastDay.DayOfWeek) * 2) / 7);
            int trainingHours = Convert.ToInt32(trainingDays) * 8;

            txtNoTrainingDays.Text = trainingDays.ToString();
            txtTotalTrainingHours.Text = trainingHours.ToString();
        }

    }

    protected void rdpEndDate_SelectedDateChanged(object sender, SelectedDateChangedEventArgs e)
    {
        DateTime firstDay = Convert.ToDateTime(rdpStartDate.SelectedDate);
        DateTime lastDay = Convert.ToDateTime(rdpEndDate.SelectedDate);

        if (firstDay > lastDay)
        {
            txtNoTrainingDays.Text = "";
            txtTotalTrainingHours.Text = "";
            RadWindowManager1_cd.RadAlert("TBAY Start Date should be greater than PST Start Date", 330, 180, "Error Date", "");
        }
        else
        {
            double trainingDays = Math.Round(1 + ((lastDay - firstDay).TotalDays * 5 - (firstDay.DayOfWeek - lastDay.DayOfWeek) * 2) / 7);
            int trainingHours = Convert.ToInt32(trainingDays) * 8;

            txtNoTrainingDays.Text = trainingDays.ToString();
            txtTotalTrainingHours.Text = trainingHours.ToString();
        }

    }

    protected string imageClass(int classId, UploadedFile f)
    {

        string error = null;

        if (string.IsNullOrWhiteSpace(error))
        {
            string uploadFolder;
            var userCim = DataHelper.GetCurrentUserCIM();

            if (HttpContext.Current.Request.Url.Host == "localhost")
                uploadFolder = Request.PhysicalApplicationPath + "Media\\Uploads\\ClassImg\\" + classId + "\\";
            else
                uploadFolder = HttpRuntime.AppDomainAppPath + "Media\\Uploads\\ClassImg\\" + classId + "\\";

            string directoryPath = Server.MapPath(string.Format("~/{0}/", "Media/Uploads/ClassImg/" + classId + "\\"));

            if (!Directory.Exists(directoryPath))
                Directory.CreateDirectory(directoryPath);

            if (f.ContentType == "image/jpeg" || f.ContentType == "image/png" || f.ContentType == "image/jpg" || f.ContentType == "image/gif")
            {
                if (f.ContentLength < 1024000000)
                {
                    Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                    string extension = Path.GetExtension(f.FileName);
                    f.SaveAs(uploadFolder + f.FileName, true);
                    //f.SaveAs(uploadFolder + unixTimestamp.ToString() + extension, true);
                    //global_classImage = unixTimestamp.ToString() + extension;
                    global_classImage = f.FileName;
                }
                else
                {
                    global_classImage = "error";
                    RadWindowManager1_cd.RadAlert("  File size is too large, please select another one!", 330, 180, "Image too Large", "");
                }
            }
            else
            {
                global_classImage = "error";
                RadWindowManager1_cd.RadAlert("Error file format please select another one!", 330, 180, "Invalid File Format", "");
            }
        }

        return global_classImage;
    }

    private static bool IsImage(HttpPostedFile file)
    {
        return ((file != null) && System.Text.RegularExpressions.Regex.IsMatch(file.ContentType, "image/\\S+") && (file.ContentLength > 0));
    }

    protected void upload_FileUploaded(object sender, FileUploadedEventArgs e)
    {

        if (e.IsValid)
        {
            imageClass(CLASSID, e.File);
            classImagePreview.ImageUrl = "~/Media/Uploads/ClassImg/" + CLASSID + "/" + global_classImage;
        }
        else
        {
            ClassImage.UploadedFiles.Clear();
            RadWindowManager1_cd.RadAlert("Invalid File. Allowed Files (jpeg,jpg,gif,png)", null, null, "Error Message", "");

        }

    }

    protected void btnRemovePreviewImg_Click(object sender, EventArgs e)
    {

        //courseImagePreview.DataValue = null;
        global_classImage = "No_image.jpg";
        classImagePreview.ImageUrl = "";
        //deleteImageInServer(CLASSID);
        ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", Utils.callClientScript("hideAllLocalLoading"), true);

    }

    private void deleteImageInServer(int courseID)
    {
        try
        {
            var folderPath = Server.MapPath("Media/Uploads/ClassImg/" + courseID + "\\");
            System.IO.DirectoryInfo folderInfo = new DirectoryInfo(folderPath);

            foreach (FileInfo file in folderInfo.GetFiles())
            {
                file.Delete();
            }
        }
        catch { }

    }

    protected void btnLoadTrainerDetails_Click(object sender, EventArgs e)
    {
        try
        {
            int trainerCIM = Convert.ToInt32(txtTrainerCIM.Text);
            var d = DataHelper.GetSupervisorDetails(trainerCIM);
            txtTrainerName.Text = d == null ? "" : d.Name;
            txtSupName.Text = d == null ? "" : d.SupervisorName;
        }
        catch
        {
            txtTrainerName.Text = "";
            txtSupName.Text = "";
        }

        ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", Utils.callClientScript("hideTrainerDetailLoading"), true);

    }

    #endregion
}