﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ClassManagement_History.ascx.cs"
    Inherits="UserControl_Pages_ClassManagement_History" %>
<rad:RadAjaxManager ID="RadAjaxManager1" runat="server">
    <AjaxSettings>
        <rad:AjaxSetting AjaxControlID="lbtnRefreshHistory">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="rgHistory" LoadingPanelID="localLoading" />
            </UpdatedControls>
        </rad:AjaxSetting>
    </AjaxSettings>
</rad:RadAjaxManager>
<div class="container">
    <br />
    <br />
    <asp:Label runat="server" ID="lblNumberOfLogs" ForeColor="Teal" Font-Bold="true"
        Font-Size="Medium">
        <asp:Label runat="server" ID="lblNumberOfLogsShowing" Text="<%$ Resources:LocalizedResource, Showing %>" ></asp:Label>
        <asp:Literal ID="ltLogCount" runat="server"></asp:Literal>
        <asp:Label runat="server" ID="lblNumberOfLogsActivities" Text="<%$ Resources:LocalizedResource, Activities %>" ></asp:Label>
    </asp:Label>
    <asp:LinkButton ID="lbtnRefreshHistory" runat="server" OnClick="lbtnRefreshHistory_Click" CssClass="pull-right"><i class="fa fa-refresh" aria-hidden="true"></i></asp:LinkButton>
    <rad:RadGrid RenderMode="Lightweight" ID="rgHistory" runat="server" CssClass="GridLess"
        Font-Size="12px" OnNeedDataSource="rgHistory_NeedDataSource" PageSize="20">
        <PagerStyle Mode="NextPrev" AlwaysVisible="true" />
        <ItemStyle Wrap="true" />
        <ClientSettings EnableAlternatingItems="true">
            <Selecting AllowRowSelect="true"></Selecting>
            <Scrolling UseStaticHeaders="true" />
            <Resizing AllowResizeToFit="true" ResizeGridOnColumnResize="false" />
        </ClientSettings>
        <ItemStyle Wrap="true"></ItemStyle>
        <MasterTableView AutoGenerateColumns="false" HeaderStyle-ForeColor="Teal" AllowPaging="true"
            TableLayout="Auto">
            <Columns>
                <rad:GridTemplateColumn HeaderText="#">
                    <ItemTemplate>
                        <%#Container.ItemIndex+1 %></ItemTemplate>
                </rad:GridTemplateColumn>
                <rad:GridBoundColumn DataField="LogDate" HeaderText="<%$ Resources:LocalizedResource, Date %>" UniqueName="LogDate">
                </rad:GridBoundColumn>
                <rad:GridBoundColumn DataField="UserID" HeaderText="<%$ Resources:LocalizedResource, Cim %>" UniqueName="UserID">
                </rad:GridBoundColumn>
                <rad:GridBoundColumn DataField="IpAddress" HeaderText="<%$ Resources:LocalizedResource, IpAddress %>" UniqueName="Ip">
                </rad:GridBoundColumn>
                <rad:GridBoundColumn DataField="Action" HeaderText="<%$ Resources:LocalizedResource, Action %>" UniqueName="Action">
                </rad:GridBoundColumn>
                <rad:GridBoundColumn DataField="Duration" HeaderText="<%$ Resources:LocalizedResource, Duration %>" UniqueName="Duration">
                </rad:GridBoundColumn>
            </Columns>
        </MasterTableView>
    </rad:RadGrid>
</div>
