﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TranscomUniversityV3Model;

public partial class UserControl_Pages_ClassManagement_History : System.Web.UI.UserControl
{
    
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void rgHistory_NeedDataSource(object sender, EventArgs e)
    {
        int classid = Convert.ToInt32(Utils.Decrypt(Request.QueryString["ClassId"]));
        var db = new TranscomUniversityV3ModelContainer();
        var res = db.vw_TranscomUniversity_ClassLog
                    .Where(c => c.ClassID == classid)
                    .ToList();
        rgHistory.DataSource = res;
        ltLogCount.Text = res.Count.ToString();
    }

    protected void lbtnRefreshHistory_Click(object sender, EventArgs e)
    {
        rgHistory.Rebind();
    }
}