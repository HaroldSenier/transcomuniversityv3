﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ClassManagement_Participants.ascx.cs"
    Inherits="UserControl_Pages_ClassManagement_Participants" %>
<rad:RadAjaxManager ID="RadAjaxManager1" runat="server">
    <AjaxSettings>
        <%--<rad:AjaxSetting AjaxControlID="RadTabStrip1">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="gridClassParticipants" LoadingPanelID="localLoading" />
            </UpdatedControls>
        </rad:AjaxSetting>--%>
        <rad:AjaxSetting AjaxControlID="btnViewUsersList">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="btnViewUsersList" />
                <rad:AjaxUpdatedControl ControlID="gridViewUsersList" LoadingPanelID="fullPageLoading" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnAddParticipant">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="gridClassParticipants" LoadingPanelID="fullPageLoading" />
                <rad:AjaxUpdatedControl ControlID="lblParticipantCount" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="gridViewUsersList">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="gridViewUsersList" LoadingPanelID="localLoading" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnAssignMultipleUsers">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlViewUsersList" LoadingPanelID="fullPageLoading" />
                <rad:AjaxUpdatedControl ControlID="gridClassParticipants" />
                <rad:AjaxUpdatedControl ControlID="urrgTransactionSummary" />
                <rad:AjaxUpdatedControl ControlID="lblParticipantCount" />
                <rad:AjaxUpdatedControl ControlID="btnAssignMultipleUsers" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnUploadBulkParticipant">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlViewUsersList" LoadingPanelID="fullPageLoading" />
                <rad:AjaxUpdatedControl ControlID="gridClassParticipants" />
                <rad:AjaxUpdatedControl ControlID="lblParticipantCount" />
                <rad:AjaxUpdatedControl ControlID="btnUploadBulkParticipant" />
                <rad:AjaxUpdatedControl ControlID="fileUploaderParticipant" />
                <rad:AjaxUpdatedControl ControlID="urrgTransactionSummary" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="gridClassParticipants">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="gridClassParticipants" LoadingPanelID="fullPageLoading" />
            </UpdatedControls>
        </rad:AjaxSetting>
    </AjaxSettings>
</rad:RadAjaxManager>
<div class="col-md-9 margin-bottom-5vh" style="overflow: auto;">
    <div class="margin-top-10px" style="display: inline-flex">
        <asp:Label runat="server" ID="lblParticipantShowing" Text="<%$ Resources:LocalizedResource, Showing %>"></asp:Label>&nbsp;<asp:Label
            Text="0" runat="server" ID="lblParticipantCount" Style="border: 1px solid gray;
            padding: 0 10px;" />&nbsp;<asp:Label runat="server" ID="lblParticipantAdded" Text="<%$ Resources:LocalizedResource, ParticipantsAddedToTheClass %>"></asp:Label>
    </div>
    <rad:RadGrid ID="gridClassParticipants" runat="server" RenderMode="Lightweight" AutoGenerateColumns="false"
        PageSize="20" OnNeedDataSource="gridClassParticipants_NeedDataSource" CssClass="GridLess">
        <PagerStyle Mode="NextPrev" AlwaysVisible="true" />
        <ClientSettings EnableAlternatingItems="false">
            <Scrolling UseStaticHeaders="true" AllowScroll="true" />
            <Resizing AllowColumnResize="true" AllowResizeToFit="true" ResizeGridOnColumnResize="false" />
        </ClientSettings>
        <ItemStyle Wrap="false"></ItemStyle>
        <MasterTableView AllowSorting="true" AllowPaging="true" CommandItemDisplay="None"
            HeaderStyle-ForeColor="Teal" TableLayout="Auto">
            <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
            <Columns>
                <rad:GridTemplateColumn HeaderText="#&nbsp;&nbsp;&nbsp;" HeaderStyle-Width="5%">
                    <ItemTemplate>
                        <%# Container.ItemIndex+1 %>
                    </ItemTemplate>
                </rad:GridTemplateColumn>
                <rad:GridBoundColumn DataField="CIM" HeaderText="<%$ Resources:LocalizedResource, CimNumber %>" UniqueName="CIM"
                    ItemStyle-Width="15%">
                </rad:GridBoundColumn>
                <rad:GridTemplateColumn HeaderText="<%$ Resources:LocalizedResource, Name %>">
                    <ItemTemplate>
                        <asp:Image ID="userImage" ImageUrl='<%# Eval("UserImageUrl", "{0}") %>' runat="server"
                            Height="50px" Width="50px" onerror="this.src='Media/Uploads/UserImage/No_Image.png'" />
                        <asp:Label ID="Label1" Text='<%# Eval("Name", "{0}") %>' runat="server" />
                    </ItemTemplate>
                </rad:GridTemplateColumn>
                <rad:GridBoundColumn DataField="Position" HeaderText="<%$ Resources:LocalizedResource, Position %>"
                    UniqueName="Position">
                </rad:GridBoundColumn>
                <rad:GridBoundColumn DataField="Account" HeaderText="<%$ Resources:LocalizedResource, Accountprogram %>"
                    UniqueName="Account">
                </rad:GridBoundColumn>
                <rad:GridBoundColumn DataField="Campaign" HeaderText="<%$ Resources:LocalizedResource, Lobcampaign %>"
                    UniqueName="Campaign">
                </rad:GridBoundColumn>
            </Columns>
        </MasterTableView>
    </rad:RadGrid>
</div>
<div class="col-md-3" style="position: absolute; top: 2.99%; right: 2.29%; height: -webkit-fill-available;
    height: 100%;">
    <div id="pnlUsers" class="groupSidebarContainer bg-darkgray white" style="height: 100%;">
        <div class="row" style="text-align: center; font-size: inherit;">
            <h5 style="font-weight: bold;">
                <asp:Label runat="server" ID="lblAddParticipants" Text="<%$ Resources:LocalizedResource, AddParticipants %>"></asp:Label>
            </h5>
            <br />
            <div class="input-group">
                <rad:RadTextBox ID="txtParticipantCIM" runat="server" Skin="Bootstrap" MaxLength="10"
                    EmptyMessage="<%$ Resources:LocalizedResource, TypeCim %>" Width="200px" RenderMode="Lightweight" >
                    <ClientEvents OnKeyPress="keyPress" />
                </rad:RadTextBox>
                <div class="input-group-btn">
                    <asp:LinkButton ID="btnAddParticipant" runat="server" OnClick="btnAddParticipant_Click"
                        Font-Underline="false" ValidationGroup="addParticipant">
                        <i id="i1" runat="server" class="icon fa fa-plus-square" style="color: Black; font-size: 25px;">
                        </i>
                    </asp:LinkButton>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtParticipantCIM"
                        ValidationGroup="addParticipant" Display="Dynamic" ErrorMessage="*" CssClass="error-message"></asp:RequiredFieldValidator>
                </div>
            </div>
            <br />
            <h5 style="font-weight: bold;">
                or
            </h5>
            <br />
            <h5 style="font-weight: bold;">
                <asp:Label runat="server" ID="lblSelectMultiplePar" Text="<%$ Resources:LocalizedResource, SelectMultipleParticipants %>"></asp:Label>
            </h5>
            <br />
            <asp:LinkButton ID="btnViewUsersList" runat="server" Text="<%$ Resources:LocalizedResource, ViewList %>"
                OnClick="btnViewUsersList_Click" CssClass="btn btn-md btn-teal button-flat"></asp:LinkButton>
            <br />
            <br />
            <h5 style="font-weight: bold;">
                <asp:Label runat="server" ID="lblOr" Text="<%$ Resources:LocalizedResource, Or %>"></asp:Label>
            </h5>
            <br />
            <h5 style="font-weight: bold;">
                <asp:Label runat="server" ID="lblUploadList" Text="<%$ Resources:LocalizedResource, UploadList %>"></asp:Label>
            </h5>
            <br />
            <div class="row file-dropzone-md-trainer" style="border: 1px solid gray; padding: 20px;
                color: lightgray;">
                <div class="dropzone-empty" style="padding-top: 2em;">
                    <asp:Image ID="Image4" ImageUrl="~/Media/Images/icon-upload.png" runat="server" AlternateText="Upload Image"
                        Height="2.875em" Widt h="2.875em" Style="filter: invert(1);" />
                    <br />
                    <p>
                        <asp:Label runat="server" ID="pDragFile" Text="<%$ Resources:LocalizedResource, DragFileHere %>"></asp:Label>
                    </p>
                </div>
                <rad:RadAsyncUpload ID="fileUploaderParticipant" runat="server" DropZones=".file-dropzone-md-trainer"
                    OnClientFileUploaded="fileUploaderParticipant_OnClientFileDropped" MultipleFileSelection="Disabled"
                    MaxFileInputsCount="1" ControlObjectsVisibility="None" CssClass="display-none"
                    OnClientFileSelected="showPageLoading">
                </rad:RadAsyncUpload>
                <asp:Button ID="btnUploadBulkParticipant" runat="server" Text="<%$ Resources:LocalizedResource, UploadBulkUsers %>"
                    CssClass="display-none" OnClick="btnUploadBulkUsers_Click" />
            </div>
        </div>
    </div>
</div>
<rad:RadWindowManager ID="RadWindowManager1_pt" runat="server" EnableShadow="true"
    RenderMode="Lightweight" Skin="Bootstrap" Modal="true" VisibleStatusbar="false">
    <Windows>
        <rad:RadWindow ID="rwViewUsersList" runat="server" Title="Select multiple Users"
            Behaviors="Close,Move" CssClass="tc-radwindow-1 height-inherit" Height="600px"
            Width="950px" OnClientBeforeShow="clearSelectedItems">
            <ContentTemplate>
                <asp:Panel ID="pnlViewUsersList" runat="server">
                    <rad:RadGrid ID="gridViewUsersList" runat="server" RenderMode="Lightweight" AutoGenerateColumns="false"
                        PageSize="10" DataSourceID="dsViewUsersList" CssClass="GridLess" AllowMultiRowSelection="true"
                        AllowFilteringByColumn="true">
                        <GroupingSettings CaseSensitive="false" />
                        <PagerStyle Mode="NextPrev" AlwaysVisible="true" />
                        <ClientSettings EnableAlternatingItems="true">
                            <Selecting AllowRowSelect="true"></Selecting>
                            <Resizing AllowColumnResize="true" ResizeGridOnColumnResize="true" AllowResizeToFit="true" />
                            <ClientEvents OnRowSelected="userRowSelected" OnRowDeselected="userRowDeselected"
                                OnRowCreated="userRowCreated" />
                        </ClientSettings>
                        <ItemStyle Wrap="false"></ItemStyle>
                        <MasterTableView DataKeyNames="CIMNumber" ClientDataKeyNames="CIMNumber" AllowSorting="true"
                            AllowPaging="true" CommandItemDisplay="None" HeaderStyle-ForeColor="Teal" AllowFilteringByColumn="true">
                            <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                            <Columns>
                                <rad:GridClientSelectColumn UniqueName="chkMultipleUsers" HeaderText="<%$ Resources:LocalizedResource, Select %>">
                                </rad:GridClientSelectColumn>
                                <rad:GridBoundColumn UniqueName="CIMNumber" HeaderText="<%$ Resources:LocalizedResource, CimNumber %>"
                                    DataField="CIMNumber" AllowFiltering="true" DataType="System.String" FilterControlWidth="120px"
                                    CurrentFilterFunction="Contains" ShowFilterIcon="false" AutoPostBackOnFilter="True">
                                </rad:GridBoundColumn>
                                <rad:GridBoundColumn UniqueName="Name" HeaderText="<%$ Resources:LocalizedResource, Name %>"
                                    DataField="Name" AllowFiltering="true" FilterControlWidth="250px" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false" AutoPostBackOnFilter="True">
                                </rad:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                    </rad:RadGrid>
                    <asp:SqlDataSource ID="dsViewUsersList" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
                        SelectCommand="pr_TranscomUniversity_AllEmployees" SelectCommandType="StoredProcedure" />
                    <br />
                    <div style="margin-left: 75%;">
                        <asp:Button ID="btnAssignMultipleUsers" runat="server" OnClick="btnAssignMultipleUsers_Click"
                            Text="<%$ Resources:LocalizedResource, AssignUserSToThisClass %>" CssClass="btn btn-md btn-teal button-flat" />
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </rad:RadWindow>
        <rad:RadWindow RenderMode="Lightweight" ID="pTransactionSummary" Behaviors="Close,Move"
            VisibleOnPageLoad="false" runat="server" CssClass="tc-radwindow-1" Height="330px"
            Title="<%$ Resources:LocalizedResource, ResultSummary %>" Width="500px">
            <ContentTemplate>
                <asp:Panel ID="pnlTransactionList" runat="server">
                    <rad:RadGrid RenderMode="Lightweight" ID="urrgTransactionSummary" runat="server"
                        AllowSorting="True" CssClass="GridLess" AllowMultiRowSelection="true" AllowPaging="true"
                        OnItemDataBound="rgTransactionSummary_OnItemDataBound">
                        <MasterTableView AutoGenerateColumns="false" TableLayout="Auto" HeaderStyle-ForeColor="Teal"
                            DataKeyNames="Id" ClientDataKeyNames="Id">
                            <Columns>
                                <rad:GridTemplateColumn HeaderText="#">
                                    <ItemTemplate>
                                        <%#Container.ItemIndex+1 %></ItemTemplate>
                                </rad:GridTemplateColumn>
                                <rad:GridBoundColumn DataField="Name" HeaderText="<%$ Resources:LocalizedResource, Name %>"
                                    UniqueName="Name">
                                </rad:GridBoundColumn>
                                <rad:GridBoundColumn DataField="Status" HeaderText="<%$ Resources:LocalizedResource, Status %>"
                                    UniqueName="Status">
                                </rad:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                    </rad:RadGrid>
                </asp:Panel>
                <button id="urbtnTransactionsList" onclick="urbtnTransactionsList_Click(); return false;"
                    runat="server" title="<%$ Resources:LocalizedResource, Close %>" class="btn tc-btn-md btn-teal button-flat bottom-center">
                    <asp:Label runat="server" ID="lblurbtnTransactionsListClose" Text="<%$ Resources:LocalizedResource, Close %>"></asp:Label>
                </button>
                <%-- <asp:LinkButton ID="gtbtnTransactionsList" OnClientClick="gtbtnTransactionsList_Click(); return false;"
                    runat="server" Text="Close" ToolTip="Close" CssClass="btn tc-btn-md btn-teal button-flat bottom-center"></asp:LinkButton>--%>
            </ContentTemplate>
        </rad:RadWindow>
    </Windows>
</rad:RadWindowManager>
<rad:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        function clearSelectedItems() {
            selectedUserItems = [];
            var grid = $find("<%= gridViewUsersList.ClientID %>");
            grid.clearSelectedItems();
        }

        function viewUsersList() {
            debugger;
            var radwindow = $find('<%=rwViewUsersList.ClientID %>');
            radwindow.show();
        }
        var selectedUserItems = [];

        function userRowSelected(sender, args) {
            var id = args.getDataKeyValue("CIMNumber");

            if (!selectedUserItems.includes(id)) {
                selectedUserItems.push(id);
            }
            setSelectedItems();
        }

        function userRowDeselected(sender, args) {
            var id = args.getDataKeyValue("CIMNumber");

            if (selectedUserItems.includes(id)) {
                selectedUserItems.splice(selectedUserItems.indexOf(id), 1);
            }
            setSelectedItems();
        }

        function userRowCreated(sender, args) {
            var id = args.getDataKeyValue("CIMNumber");
            if (selectedUserItems.includes(id) && selectedUserItems.length > 0) {
                args.get_gridDataItem().set_selected(true);
            }
        }

        function setSelectedItems() {
            var elements = selectedUserItems;
            var outputStr = "";
            for (var i = 0; i < elements.length; i++) {
                outputStr += elements[i] + ",";
            }
            $("#" + "<%= hfSelectedUser.ClientID %>").val(outputStr);
        }

        function pshowTransaction() {
            hidePageLoading();
            var users = $find('<%= rwViewUsersList.ClientID %>');
            users.close();
            var radwindow = $find('<%= pTransactionSummary.ClientID %>');
            radwindow.show();
        }

        function urbtnTransactionsList_Click() {
            var radwindow = $find('<%= pTransactionSummary.ClientID %>');
            radwindow.close();
        }

        function fileUploaderParticipant_OnClientFileDropped() {
            showPageLoading();
            $("#<%= btnUploadBulkParticipant.ClientID %>").click();
        }
        function keyPress(sender, args) {
            var text = sender.get_value() + args.get_keyCharacter();
            if (!text.match('^[0-9]+$'))
                args.set_cancel(true);
        }

        function resizeParticipantGrid() {
            var grid = $find("<%= gridClassParticipants.ClientID %>");
            //showParticipantGrid();
            var columns = grid.get_masterTableView().get_columns();
            for (var i = 0; i < columns.length; i++) {
                columns[i].resizeToFit();
            }

        }

        function showParticipantGrid() {
            $("#<%= gridClassParticipants.ClientID %>").removeClass("display-none");
        }

    </script>
</rad:RadScriptBlock>
<rad:RadAjaxLoadingPanel ID="fullPageLoading2" runat="server" Transparency="25" IsSticky="true"
    CssClass="Loading" />
<asp:HiddenField ID="hfSelectedUser" runat="server" />
