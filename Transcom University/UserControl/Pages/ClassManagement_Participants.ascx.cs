﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TranscomUniversityV3Model;
using TransactionNames;
using Telerik.Web.UI;
using System.Drawing;
using System.IO;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

public partial class UserControl_Pages_ClassManagement_Participants : System.Web.UI.UserControl
{
    private static int CLASSID;

    protected void Page_Load(object sender, EventArgs e)
    {

        CLASSID = Convert.ToInt32(Utils.Decrypt(Request.QueryString["ClassId"]));

    }

    protected void gridClassParticipants_NeedDataSource(object sender, EventArgs e)
    {
      

        var db = new TranscomUniversityV3ModelContainer();
        var res = db.vw_TranscomUniversity_ClassParticipants
                    .Where(c => c.ClassID == CLASSID)
                    .ToList();

        gridClassParticipants.DataSource = res;
        //if (gridClassParticipants.Items.Count <= 0)
        //    gridClassParticipants.CssClass = "GridLess display-none";
        //else
        //    gridClassParticipants.CssClass = "GridLess";

        lblParticipantCount.Text = res.Count.ToString();
    }

    protected void btnAddParticipant_Click(object sender, EventArgs e)
    {
        try
        {
            if (Page.IsValid)
            {

                int CIM = Convert.ToInt32(txtParticipantCIM.Text);
                int addedBy = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
                var db = new TranscomUniversityV3ModelContainer();

                string _userIP = DataHelper.GetIPAddress();
                string _userID = DataHelper.GetCurrentUserCIM();
                double _duration = 0;

                DateTime startTime;
                DateTime endTime;


                if (isUser(CIM))
                {
                    var dts = db.CreateQuery<DateTime>("CurrentDateTime() ");
                    startTime = dts.AsEnumerable().First();

                    db.pr_TranscomUniversity_InsertUpdateParticipant(CLASSID, CIM, addedBy);



                    string _action = "Added Participant " + DataHelper.getUserName(CIM);
                    var dte = db.CreateQuery<DateTime>("CurrentDateTime()");

                    endTime = dte.AsEnumerable().First();
                    _duration = (endTime - startTime).TotalMilliseconds;

                    int l_actionID = DataHelper.getLastLogID() + 1;
                    DataHelper.logClass(CLASSID, l_actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);

                    gridClassParticipants.Rebind();
                    RadWindowManager1_pt.RadAlert(Resources.LocalizedResource.SuccessfullyAddedNewParticipant.ToString(), 330, 180, Resources.LocalizedResource.Successfull.ToString(), "");

                }
                else
                {
                    RadWindowManager1_pt.RadAlert(Resources.LocalizedResource.TheCIMyouenteredisnotvalid.ToString(), 330, 180, Resources.LocalizedResource.ErrorCIM.ToString(), "");
                }
                txtParticipantCIM.Text = "";

            }

        }
        catch (FormatException ex)
        {
            RadWindowManager1_pt.RadAlert(Resources.LocalizedResource.TheCIMyouenteredisnotvalid.ToString(), 330, 180, Resources.LocalizedResource.ErrorCIM.ToString(), "");
            throw;
        }
        catch (Exception ex)
        {
            RadWindowManager1_pt.RadAlert(ex.Message, 330, 180, "Error", "");
            throw;
        }
    }

    protected void btnAssignMultipleUsers_Click(object sender, EventArgs e)
    {
        try
        {
            string _userIP = DataHelper.GetIPAddress();
            string _userID = DataHelper.GetCurrentUserCIM();
            double _duration = 0;

            DateTime startTime;
            DateTime endTime;

            string stat = "";
            int successCount = 0;

            List<Transaction> UserList = new List<Transaction>();

            var db = new TranscomUniversityV3ModelContainer();

            string userList = hfSelectedUser.Value;
            string[] data = userList.Split(',');

            var classId = CLASSID;
            var delegator = Convert.ToInt32(_userID);
            int participantCIM = -1;

            if (data.Length > 1)
            {
                var dts = db.CreateQuery<DateTime>("CurrentDateTime() ");
                startTime = dts.AsEnumerable().First();

                for (int i = 0; i < (data.Length - 1); i++)
                {
                    participantCIM = Convert.ToInt32(data[i]);

                    pr_TranscomUniversity_InsertUpdateParticipant_Result _res = db.pr_TranscomUniversity_InsertUpdateParticipant(classId, participantCIM, delegator).SingleOrDefault();

                    if (_res.Msg == "1")
                    {
                        stat = "Successful";
                        successCount++;
                    }
                    else if (_res.Msg == "2")
                    {
                        stat = "Duplicate";
                    }
                    else
                    {
                        stat = "Fail";
                    }

                    Transaction participant = new Transaction
                    {
                        Id = Convert.ToInt32(data[i]),
                        Name = DataHelper.getUserName(participantCIM),
                        Status = stat
                    };

                    UserList.Add(participant);
                }

                if (successCount > 0)
                {
                    string _action = successCount > 1 ? "Added " + successCount + " Participants" : "Added Participant " + DataHelper.getUserName(participantCIM);
                    var dte = db.CreateQuery<DateTime>("CurrentDateTime() ");

                    endTime = dte.AsEnumerable().First();
                    _duration = (endTime - startTime).TotalMilliseconds;

                    int l_actionID = DataHelper.getLastLogID() + 1;
                    DataHelper.logClass(classId, l_actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);
                }



                urrgTransactionSummary.DataSource = UserList;
                urrgTransactionSummary.DataBind();
                gridClassParticipants.Rebind();
                ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", Utils.callClientScript("pshowTransaction"), true);
            }
            else
            {
                RadWindowManager1_pt.RadAlert(Resources.LocalizedResource.Pleaseselectatleastone1User.ToString(), 330, 180, Resources.LocalizedResource.SystemMessage.ToString(), "");
            }
        }
        catch
        {
            RadWindowManager1_pt.RadAlert(Resources.LocalizedResource.ErrorassigningtoanothercoursesPleasecontactyourSystemAdministrator.ToString(), 330, 180, Resources.LocalizedResource.ErrorMessage.ToString(), "");
        }
    }

    protected void rgTransactionSummary_OnItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            GridDataItem dataBoundItem = e.Item as GridDataItem;

            if (dataBoundItem["Status"].Text == "Duplicate")
            {
                dataBoundItem["Status"].ForeColor = Color.DarkGray;
            }
            else if (dataBoundItem["Status"].Text == "Fail")
            {
                dataBoundItem["Status"].ForeColor = Color.Red;
            }
            else
            {
                dataBoundItem["Status"].ForeColor = Color.YellowGreen;
            }
        }

    }

    protected void btnUploadBulkUsers_Click(object sender, EventArgs e)
    {
        string _userIP = DataHelper.GetIPAddress();
        string _userID = DataHelper.GetCurrentUserCIM();
        double _duration = 0;

        DateTime startTime;
        DateTime endTime;

        try
        {
            if (fileUploaderParticipant.UploadedFiles.Count == 0 || fileUploaderParticipant.UploadedFiles[0].GetExtension() != ".csv")
            {
                fileUploaderParticipant.UploadedFiles.Clear();

                RadWindowManager1_pt.RadAlert(Resources.LocalizedResource.PleaseSelectACsvFileToUpload.ToString(), 330, 180, Resources.LocalizedResource.SystemMessage.ToString(), "");
                return;
            }
            ///////////
            UploadedFile f = fileUploaderParticipant.UploadedFiles[0];
            var db = new TranscomUniversityV3ModelContainer();
            var dts = db.CreateQuery<DateTime>("CurrentDateTime() ");
            startTime = dts.AsEnumerable().First();

            string csvPath = Server.MapPath("~/Files/BulkUsers/") + f.FileName;
            f.SaveAs(csvPath);

            string csvData = File.ReadAllText(csvPath);
            List<Transaction> UserList = new List<Transaction>();
            int ui = 0;
            int successCount = 0;
            int CourseID = Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"]));


            foreach (string row in csvData.Split('\n'))
            {
                string stat = "";
                if (ui == 0 && !string.IsNullOrEmpty(row))
                {
                    string[] filecolum = row.Split(',');
                    if (filecolum[0].Trim().ToLower() != "participantcim")
                    {
                        RadWindowManager1_pt.RadAlert(Resources.LocalizedResource.CSVFiledoesnotcontaincorrectColumnsPleaseselectanother.ToString(), 330, 180, Resources.LocalizedResource.SystemMessage.ToString(), "");
                        return;
                    }
                }
                else if (!string.IsNullOrEmpty(row) && ui > 0)
                {

                    string[] cells = row.Split(',');
                    int participantCIM = Convert.ToInt32(cells[0]);
                    int delegator = Convert.ToInt32(_userID);
                    pr_TranscomUniversity_InsertUpdateParticipant_Result _res = db.pr_TranscomUniversity_InsertUpdateParticipant(CLASSID, participantCIM, delegator).SingleOrDefault();

                    if (_res.Msg == "1")
                    {
                        stat = "Successful";
                        successCount++;
                    }
                    else if (_res.Msg == "2")
                    {
                        stat = "Duplicate";
                    }
                    else
                    {
                        stat = "Fail";
                    }

                    Transaction participant = new Transaction
                    {
                        Id = ui,
                        Name = DataHelper.getUserName(participantCIM),
                        Status = stat
                    };

                    UserList.Add(participant);

                }
                ui++;
            }

            if (successCount > 0)
            {
                string _action = "Uploaded " + f.FileName + " Participant List";
                var dte = db.CreateQuery<DateTime>("CurrentDateTime() ");

                endTime = dte.AsEnumerable().First();
                _duration = (endTime - startTime).TotalMilliseconds;

                int l_actionID = DataHelper.getLastLogID() + 1;
                DataHelper.logClass(CLASSID, l_actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);
            }


            fileUploaderParticipant.UploadedFiles.Clear();
            urrgTransactionSummary.DataSource = UserList;
            urrgTransactionSummary.DataBind();
            gridClassParticipants.Rebind();

            ScriptManager.RegisterStartupScript(Page, typeof(Page), "pshowTransaction2", Utils.callClientScript("pshowTransaction"), true);
            /////////////////////////

        }
        catch
        {
            RadWindowManager1_pt.RadAlert(Resources.LocalizedResource.ErroronaddingparticipantssPleasecontactyourSystemAdministrator.ToString(), 330, 180, Resources.LocalizedResource.ErrorMessage.ToString(), "");
        }
        finally
        {
            fileUploaderParticipant.UploadedFiles.Clear();
        }
    }

    private bool isUser(int cim)
    {
        var db = new TranscomUniversityV3ModelContainer();
        var u = db.tbl_TranscomUniversity_Cor_UsersInfo
                    .Where(c => c.CIMNumber == cim)
                    .Select(c => c.CIMNumber)
                    .SingleOrDefault();
        return u != null ? true : false;
    }

    protected void btnViewUsersList_Click(object sender, EventArgs e)
    {

        foreach (GridColumn column in gridViewUsersList.MasterTableView.OwnerGrid.Columns)
        {
            column.CurrentFilterFunction = GridKnownFunction.NoFilter;
            column.CurrentFilterValue = string.Empty;
        }
        gridViewUsersList.MasterTableView.FilterExpression = string.Empty;
        gridViewUsersList.Rebind();

        ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", DataHelper.callClientScript("viewUsersList"), true);
    }

}