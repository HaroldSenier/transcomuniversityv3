﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ClassManagement_Trainer.ascx.cs"
    Inherits="UserControl_Pages_ClassManagement_Trainer" %>
<rad:RadAjaxManager ID="RadAjaxManager1" runat="server">
    <AjaxSettings>
        <rad:AjaxSetting AjaxControlID="btnAddTrainer">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="gridClassTrainers" LoadingPanelID="fullPageLoading" />
                <rad:AjaxUpdatedControl ControlID="lblTrainerCount" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="gridViewTrainersList">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="gridViewTrainersList" LoadingPanelID="localLoading" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnAssignMultipleTrainers">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlviewTrainersList" LoadingPanelID="fullPageLoading" />
                <rad:AjaxUpdatedControl ControlID="gridClassTrainers" />
                <rad:AjaxUpdatedControl ControlID="urrgTransactionSummaryTrainer" />
                <rad:AjaxUpdatedControl ControlID="lblTrainerCount" />
                <rad:AjaxUpdatedControl ControlID="fileUploaderTrainer" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnUploadBulkTrainer">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlviewTrainersList" LoadingPanelID="fullPageLoading" />
                <rad:AjaxUpdatedControl ControlID="gridClassTrainers" />
                <rad:AjaxUpdatedControl ControlID="lblTrainerCount" />
                <rad:AjaxUpdatedControl ControlID="urrgTransactionSummaryTrainer" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnEdit">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlClassTrainerContainer" LoadingPanelID="fullPageLoading" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnCancel">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlClassTrainerContainer" LoadingPanelID="fullPageLoading" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnSaveChanges">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlClassTrainerContainer" LoadingPanelID="fullPageLoading" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnTransfer">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlClassTrainerContainer" LoadingPanelID="fullPageLoading" />
                <rad:AjaxUpdatedControl ControlID="btnTransfer" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="gridClassTrainers">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="gridClassTrainers" LoadingPanelID="localLoading" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnViewTrainersList">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="btnViewTrainersList" />
                <rad:AjaxUpdatedControl ControlID="gridViewTrainersList" LoadingPanelID="fullPageLoading" />
            </UpdatedControls>
        </rad:AjaxSetting>
    </AjaxSettings>
</rad:RadAjaxManager>
<asp:Panel ID="pnlClassTrainerContainer" runat="server" CssClass="col-md-9 margin-bottom-5vh">
    <div class="margin-top-10px" style="display: inline-flex">
        <asp:Label runat="server" ID="lblParticipantShowing" Text="<%$ Resources:LocalizedResource, Showing %>"></asp:Label>&nbsp;<asp:Label
            Text="0" runat="server" ID="lblTrainerCount" Style="border: 1px solid gray; padding: 0 10px;" />&nbsp;<asp:Label
                runat="server" ID="lblParticipantAdded" Text="<%$ Resources:LocalizedResource, TrainersAddedToTheClass %>"></asp:Label>
    </div>
    <rad:RadGrid ID="gridClassTrainer_EditMode" runat="server" RenderMode="Lightweight"
        AutoGenerateColumns="false" PageSize="20" OnNeedDataSource="gridClassTrainers_EditMode_NeedDataSource"
        CssClass="GridLess" Visible="false" OnItemDataBound="gridClassTrainers_EditMode_ItemDataBound"
        Width="1000px">
        <PagerStyle Mode="NextPrev" AlwaysVisible="true" />
        <ClientSettings EnableAlternatingItems="false">
            <Scrolling AllowScroll="True" UseStaticHeaders="True"></Scrolling>
            <Resizing AllowColumnResize="true" AllowResizeToFit="true" ResizeGridOnColumnResize="true" />
        </ClientSettings>
        <ItemStyle Wrap="false"></ItemStyle>
        <MasterTableView AllowSorting="true" AllowPaging="true" CommandItemDisplay="None"
            HeaderStyle-ForeColor="Teal" TableLayout="Auto" DataKeyNames="ClassTrainerID,AssignState">
            <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
            <Columns>
                <rad:GridTemplateColumn HeaderText="#" HeaderStyle-Width="50px">
                    <ItemTemplate>
                        <%# Container.ItemIndex+1 %>
                    </ItemTemplate>
                </rad:GridTemplateColumn>
                <rad:GridBoundColumn DataField="TrainerCIM" HeaderText="<%$ Resources:LocalizedResource, Cim %>#" UniqueName="TrainerCIM"
                    ItemStyle-Width="150px">
                </rad:GridBoundColumn>
                <rad:GridTemplateColumn HeaderText="<%$ Resources:LocalizedResource, Name %>" ItemStyle-Width="300px">
                    <ItemTemplate>
                        <asp:Image ID="userImage" ImageUrl='<%# Eval("UserImageUrl", "{0}") %>' runat="server"
                            Height="50px" Width="50px" onerror="this.src='Media/Uploads/UserImage/No_Image.png'" />
                        <asp:Label ID="Label1" Text='<%# Eval("Name", "{0}") %>' runat="server" />
                    </ItemTemplate>
                </rad:GridTemplateColumn>
                <rad:GridBoundColumn DataField="Campaign" HeaderText="<%$ Resources:LocalizedResource, Lobcampaign %>"
                    UniqueName="Campaign" HeaderStyle-Width="50px">
                </rad:GridBoundColumn>
                <rad:GridBoundColumn DataField="Division" HeaderText="<%$ Resources:LocalizedResource, Division %>" UniqueName="Division">
                </rad:GridBoundColumn>
                <rad:GridBoundColumn DataField="ReportingManager" HeaderText="<%$ Resources:LocalizedResource, ReportingManagerName %>"
                    UniqueName="ReportingManager">
                </rad:GridBoundColumn>
                <rad:GridTemplateColumn HeaderText="<%$ Resources:LocalizedResource, AssignState %>"
                    UniqueName="AssignState">
                    <ItemTemplate>
                        <rad:RadToggleButton ID="btnToggleAssignState" runat="server" AutoPostBack="false"
                            RenderMode="Lightweight" OnClientToggleStateChanged="btnToggleAssignState_ClientToggleStateChanged"
                            CommandArgument='<%# Eval("ClassTrainerID") %>'>
                            <ToggleStates>
                                <rad:ButtonToggleState Text="<%$ Resources:LocalizedResource, ToggleAssignState %>" Selected="true" CssClass="btn btn-sm toggle-btn toggle-btn-gray"
                                    Value="0" />
                                <rad:ButtonToggleState Text="<%$ Resources:LocalizedResource, LeadTrainer %>" CssClass="btn btn-sm toggle-btn toggle-btn-teal "
                                    Value="1" />
                                <rad:ButtonToggleState Text="<%$ Resources:LocalizedResource, BackupTrainer %>" CssClass="btn btn-sm toggle-btn toggle-btn-blue"
                                    Value="2" />
                                <rad:ButtonToggleState Text="<%$ Resources:LocalizedResource, TransferTrainer %>" CssClass="btn btn-sm toggle-btn toggle-btn-green"
                                    Value="3" />
                            </ToggleStates>
                        </rad:RadToggleButton>
                    </ItemTemplate>
                </rad:GridTemplateColumn>
                <rad:GridBoundColumn DataField="AssignState" HeaderText="<%$ Resources:LocalizedResource, AssignState %>"
                    UniqueName="AssignState" Visible="false">
                </rad:GridBoundColumn>
            </Columns>
        </MasterTableView>
    </rad:RadGrid>
    <rad:RadGrid ID="gridClassTrainers" runat="server" RenderMode="Lightweight" AutoGenerateColumns="false"
        PageSize="20" OnNeedDataSource="gridClassTrainers_NeedDataSource" CssClass="GridLess">
        <PagerStyle Mode="NextPrev" AlwaysVisible="true" />
        <ClientSettings EnableAlternatingItems="false">
            <Scrolling UseStaticHeaders="true" AllowScroll="true" />
            <Resizing AllowColumnResize="true" AllowResizeToFit="true" ResizeGridOnColumnResize="false" />
        </ClientSettings>
        <ItemStyle Wrap="false"></ItemStyle>
        <MasterTableView AllowSorting="true" AllowPaging="true" CommandItemDisplay="None"
            HeaderStyle-ForeColor="Teal" TableLayout="Auto">
            <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
            <Columns>
                <rad:GridTemplateColumn HeaderText="#">
                    <ItemTemplate>
                        <%# Container.ItemIndex+1 %>
                    </ItemTemplate>
                </rad:GridTemplateColumn>
                <rad:GridBoundColumn DataField="TrainerCIM" HeaderText="CIM#" UniqueName="TrainerCIM">
                </rad:GridBoundColumn>
                <rad:GridTemplateColumn HeaderText="<%$ Resources:LocalizedResource, Name %>">
                    <ItemTemplate>
                        <asp:Image ID="userImage" ImageUrl='<%# Eval("UserImageUrl", "{0}") %>' runat="server"
                            Height="50px" Width="50px" onerror="this.src='Media/Uploads/UserImage/No_Image.png'" />
                        <asp:Label ID="Label1" Text='<%# Eval("Name", "{0}") %>' runat="server" />
                    </ItemTemplate>
                </rad:GridTemplateColumn>
                <rad:GridBoundColumn DataField="Campaign" HeaderText="<%$ Resources:LocalizedResource, Lobcampaign %>"
                    UniqueName="Campaign">
                </rad:GridBoundColumn>
                <rad:GridBoundColumn DataField="Division" HeaderText="<%$ Resources:LocalizedResource, Division %>" UniqueName="Division">
                </rad:GridBoundColumn>
                <rad:GridBoundColumn DataField="ReportingManager" HeaderText="<%$ Resources:LocalizedResource, ReportingManagerName %>"
                    UniqueName="ReportingManager">
                </rad:GridBoundColumn>
                <rad:GridBoundColumn DataField="AssignState" HeaderText="<%$ Resources:LocalizedResource, AssignState %>"
                    UniqueName="AssignState">
                </rad:GridBoundColumn>
            </Columns>
        </MasterTableView>
    </rad:RadGrid>
    <div class="text-center margin-top-10px ">
        <asp:Button ID="btnEdit" Text="<%$ Resources:LocalizedResource, Edit %>" runat="server" CssClass="btn btn-md btn-flat btn-teal"
            OnClick="btnEdit_Click" />
        <asp:Button ID="btnCancel" Text="<%$ Resources:LocalizedResource, Cancel %>" runat="server"
            CssClass="btn btn-md btn-flat btn-teal" OnClick="btnCancel_Click" Visible="false" />
        <asp:Button ID="btnSaveChanges" Text="<%$ Resources:LocalizedResource, SaveChanges %>" runat="server" CssClass="btn btn-md btn-flat btn-teal"
            OnClick="btnSaveChanges_Click" Visible="false" />
    </div>
</asp:Panel>
<div class="col-md-3" style="position: absolute; top: 2.99%; right: 2.29%; height: -webkit-fill-available;
    height: 100%;">
    <div id="pnlUsers" class="groupSidebarContainer bg-darkgray white" style="height: 100%;">
        <div class="row" style="text-align: center; font-size: inherit;">
            <h5 style="font-weight: bold;">
                <asp:Label runat="server" ID="lblAddTrainers" Text="<%$ Resources:LocalizedResource, AddTrainers %>"></asp:Label>
            </h5>
            <br />
            <div class="input-group">
                <rad:RadTextBox ID="txtTrainerCIM" runat="server" Skin="Bootstrap" MaxLength="10"
                    EmptyMessage="Type CIM" Width="200px" placeholder="<%$ Resources:LocalizedResource, TypeCim %>" RenderMode="Lightweight" >
                    <ClientEvents OnKeyPress="keyPress" />
                </rad:RadTextBox>
                <div class="input-group-btn">
                    <asp:LinkButton ID="btnAddTrainer" runat="server" Font-Underline="false" OnClick="btnAddTrainer_Click"
                        ValidationGroup="addTrainer">
                        <i id="i1" runat="server" class="icon fa fa-plus-square" style="color: Black; font-size: 25px;">
                        </i>
                    </asp:LinkButton>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtTrainerCIM"
                        ValidationGroup="addTrainer" Display="Dynamic" ErrorMessage="*" CssClass="error-message"></asp:RequiredFieldValidator>
                </div>
            </div>
            <br />
            <h5 style="font-weight: bold;">
                or
            </h5>
            <br />
            <h5 style="font-weight: bold;">
                <asp:Label runat="server" ID="lblSelectMultipleTrainers" Text="<%$ Resources:LocalizedResource, SelectMultipleTrainers %>"></asp:Label>
            </h5>
            <br />
            <%--<button class="btn btn-md btn-teal button-flat" onclick="viewTrainersList(); return false;">
                View List</button>--%>
            <asp:LinkButton ID="btnViewTrainersList" runat="server" Text="<%$ Resources:LocalizedResource, ViewList %>"
                OnClick="btnViewTrainerList_Click" CssClass="btn btn-md btn-teal button-flat"></asp:LinkButton>
            <br />
            <br />
            <h5 style="font-weight: bold;">
                <asp:Label runat="server" ID="lblOR" Text="<%$ Resources:LocalizedResource, Or %>"></asp:Label>
            </h5>
            <br />
            <h5 style="font-weight: bold;">
                <asp:Label runat="server" ID="lblUploadList" Text="<%$ Resources:LocalizedResource, UploadList %>"></asp:Label>
            </h5>
            <br />
            <div class="row file-dropzone-md" style="border: 1px solid gray; padding: 20px; color: lightgray;">
                <div class="dropzone-empty" style="padding-top: 2em;">
                    <asp:Image ID="Image4" ImageUrl="~/Media/Images/icon-upload.png" runat="server" AlternateText="Upload Image"
                        Height="2.875em" Widt h="2.875em" Style="filter: invert(1);" />
                    <br />
                    <p>
                        <asp:Label runat="server" ID="lblDragFile" Text="<%$ Resources:LocalizedResource, DragFileHere %>"></asp:Label>
                    </p>
                </div>
                <rad:RadAsyncUpload ID="fileUploaderTrainer" runat="server" DropZones=".file-dropzone-md"
                    OnClientFileUploaded="fileUploaderTrainerTrainer_OnClientFileDropped" MultipleFileSelection="Disabled"
                    MaxFileInputsCount="1" ControlObjectsVisibility="None" CssClass="display-none"
                    OnClientFileSelected="showPageLoading">
                </rad:RadAsyncUpload>
                <asp:Button ID="btnUploadBulkTrainer" runat="server" Text="<%$ Resources:LocalizedResource, UploadBulkUsers %>"
                    CssClass="display-none" OnClick="btnUploadBulkUsers_Click" />
            </div>
            <%--  <asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:FileUpload ID="fuUsers" runat="server" ToolTip="Upload Bulk Users" CssClass="btn btn-default btn-file"
                        Width="220px" />
                    <br />
                    <asp:LinkButton ID="btnUploadBulkUsers" runat="server" Text="Upload Bulk Users" CssClass="btn btn-md btn-teal button-flat" OnClick="btnUploadBulkUsers_Click"></asp:LinkButton>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnUploadBulkUsers" />
                </Triggers>
            </asp:UpdatePanel>--%>
        </div>
    </div>
</div>
<rad:RadWindowManager ID="RadWindowManager1_pt" runat="server" EnableShadow="true"
    RenderMode="Lightweight" Skin="Bootstrap" Modal="true" VisibleStatusbar="false">
    <Windows>
        <rad:RadWindow ID="rwViewTrainerList" runat="server" Title="<%$ Resources:LocalizedResource, SelectMultipleUsers %>"
            Behaviors="Close" CssClass="tc-radwindow-1 height-inherit" Height="600px" Width="950px"
            OnClientBeforeShow="clearSelectedItemsTrainer">
            <ContentTemplate>
                <asp:Panel ID="pnlviewTrainersList" runat="server">
                    <rad:RadGrid ID="gridViewTrainersList" runat="server" RenderMode="Lightweight" AutoGenerateColumns="false"
                        PageSize="10" DataSourceID="dsviewTrainersList" CssClass="GridLess" AllowMultiRowSelection="true"
                        AllowFilteringByColumn="true">
                        <PagerStyle Mode="NextPrev" AlwaysVisible="true" />
                        <GroupingSettings CaseSensitive="false" />
                        <ClientSettings EnableAlternatingItems="true">
                            <Selecting AllowRowSelect="true"></Selecting>
                            <Resizing AllowColumnResize="true" ResizeGridOnColumnResize="true" AllowResizeToFit="true" />
                            <ClientEvents OnRowSelected="trainerRowSelected" OnRowDeselected="trainerRowDeselected"
                                OnRowCreated="trainerRowCreated" />
                        </ClientSettings>
                        <ItemStyle Wrap="false"></ItemStyle>
                        <MasterTableView DataKeyNames="CIMNumber" ClientDataKeyNames="CIMNumber" AllowSorting="true"
                            AllowFilteringByColumn="true" AllowPaging="true" CommandItemDisplay="None" HeaderStyle-ForeColor="Teal">
                            <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                            <Columns>
                                <rad:GridClientSelectColumn UniqueName="chkMultipleUsers" HeaderText="<%$ Resources:LocalizedResource, Select %>">
                                </rad:GridClientSelectColumn>
                                <rad:GridBoundColumn UniqueName="CIMNumber" HeaderText="<%$ Resources:LocalizedResource, CimNumber %>" DataField="CIMNumber"
                                    AllowFiltering="true" DataType="System.String" FilterControlWidth="120px" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false" AutoPostBackOnFilter="True">
                                </rad:GridBoundColumn>
                                <rad:GridBoundColumn UniqueName="Name" HeaderText="<%$ Resources:LocalizedResource, Name %>" DataField="Name" AllowFiltering="true"
                                    FilterControlWidth="250px" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                                    AutoPostBackOnFilter="True">
                                </rad:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                    </rad:RadGrid>
                    <asp:SqlDataSource ID="dsviewTrainersList" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
                        SelectCommand="pr_TranscomUniversity_AllEmployees" SelectCommandType="StoredProcedure" />
                    <br />
                    <div style="margin-left: 75%;">
                        <asp:Button ID="btnAssignMultipleTrainers" runat="server" OnClick="btnAssignMultipleTrainers_Click"
                            Text="<%$ Resources:LocalizedResource, AssignUserSToThisClass %>" CssClass="btn btn-md btn-teal button-flat" />
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </rad:RadWindow>
        <rad:RadWindow RenderMode="Lightweight" ID="pTransactionSummaryTrainer" Behaviors="Close,Move"
            VisibleOnPageLoad="false" runat="server" CssClass="tc-radwindow-1" Height="330px"
            Title="Result Summary" Width="500px">
            <ContentTemplate>
                <asp:Panel ID="pnlTransactionListTrainer" runat="server">
                    <rad:RadGrid RenderMode="Lightweight" ID="urrgTransactionSummaryTrainer" runat="server"
                        AllowSorting="True" CssClass="GridLess" AllowMultiRowSelection="true" AllowPaging="true"
                        OnItemDataBound="rgTransactionSummary_OnItemDataBound">
                        <MasterTableView AutoGenerateColumns="false" TableLayout="Auto" HeaderStyle-ForeColor="Teal"
                            DataKeyNames="Id" ClientDataKeyNames="Id">
                            <Columns>
                                <rad:GridTemplateColumn HeaderText="#">
                                    <ItemTemplate>
                                        <%#Container.ItemIndex+1 %></ItemTemplate>
                                </rad:GridTemplateColumn>
                                <rad:GridBoundColumn DataField="Name" HeaderText="<%$ Resources:LocalizedResource, Name %>" UniqueName="Name">
                                </rad:GridBoundColumn>
                                <rad:GridBoundColumn DataField="Status" HeaderText="<%$ Resources:LocalizedResource, Status %>" UniqueName="Status">
                                </rad:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                    </rad:RadGrid>
                </asp:Panel>
                <button id="urbtnTransactionsList" onclick="urbtnTransactionsListTrainer_Click(); return false;"
                    runat="server" title="<%$ Resources:LocalizedResource, Close %>" class="btn tc-btn-md btn-teal button-flat bottom-center">
                    <asp:Label runat="server" ID="lblurbtnTransactionsListClose" Text="<%$ Resources:LocalizedResource, Close %>"></asp:Label></button>
                <%-- <asp:LinkButton ID="gtbtnTransactionsList" OnClientClick="gtbtnTransactionsList_Click(); return false;"
                    runat="server" Text="Close" ToolTip="Close" CssClass="btn tc-btn-md btn-teal button-flat bottom-center"></asp:LinkButton>--%>
            </ContentTemplate>
        </rad:RadWindow>
        <rad:RadWindow RenderMode="Lightweight" ID="rwClassList" Behaviors="Close,Move" VisibleOnPageLoad="false"
            runat="server" CssClass="tc-radwindow-1" Title="Class List" Width="500px" OnClientBeforeShow="clearSelectedClassItems">
            <ContentTemplate>
                <asp:Panel ID="Panel1" runat="server">
                    <rad:RadGrid ID="rgClasses" runat="server" RenderMode="Lightweight" AutoGenerateColumns="false"
                        PageSize="20" OnNeedDataSource="rgClasses_NeedDataSource" CssClass="GridLess"
                        AllowMultiRowSelection="false">
                        <MasterTableView AllowSorting="true" AllowPaging="true" CommandItemDisplay="None"
                            HeaderStyle-ForeColor="Teal" TableLayout="Auto" DataKeyNames="ClassID" ClientDataKeyNames="ClassID">
                            <Columns>
                                <rad:GridClientSelectColumn UniqueName="cbClass" ItemStyle-Width="5px">
                                </rad:GridClientSelectColumn>
                                <rad:GridBoundColumn HeaderText="<%$ Resources:LocalizedResource, ClassName %>" UniqueName="ClassName" DataField="ClassName"
                                    ItemStyle-Width="90px">
                                </rad:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                        <ClientSettings>
                            <Selecting AllowRowSelect="true" />
                        </ClientSettings>
                    </rad:RadGrid>
                    <div class="text-center">
                        <asp:LinkButton ID="btnTransfer" Text="<%$ Resources:LocalizedResource, Transfer %>" runat="server" CssClass="btn btn-md btn-teal btn-flat"
                            OnClick="btnTransfer_Click"></asp:LinkButton>
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </rad:RadWindow>
    </Windows>
</rad:RadWindowManager>
<rad:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        function clearSelectedItemsTrainer() {
            selectedTrainerItems = [];
            var grid = $find("<%= gridViewTrainersList.ClientID %>");
            grid.clearSelectedItems();
        }

        function viewTrainersList() {
            var radwindow = $find('<%=rwViewTrainerList.ClientID %>');
            radwindow.show();
        }
        var selectedTrainerItems = [];

        function trainerRowSelected(sender, args) {
            var id = args.getDataKeyValue("CIMNumber");

            if (!selectedTrainerItems.includes(id)) {
                selectedTrainerItems.push(id);
            }
            setSelectedItemsTrainer();
        }

        function trainerRowDeselected(sender, args) {
            var id = args.getDataKeyValue("CIMNumber");

            if (selectedTrainerItems.includes(id)) {
                selectedTrainerItems.splice(selectedTrainerItems.indexOf(id), 1);
            }
            setSelectedItemsTrainer();
        }

        function trainerRowCreated(sender, args) {
            var id = args.getDataKeyValue("CIMNumber");
            if (selectedTrainerItems.includes(id) && selectedTrainerItems.length > 0) {
                args.get_gridDataItem().set_selected(true);
            }
        }

        function setSelectedItemsTrainer() {
            var elements = selectedTrainerItems;
            var outputStr = "";
            for (var i = 0; i < elements.length; i++) {
                outputStr += elements[i] + ",";
            }
            $("#" + "<%= hfSelectedTrainer.ClientID %>").val(outputStr);
        }

        function pshowTransactionTrainer() {
            hidePageLoading();
            var users = $find('<%= rwViewTrainerList.ClientID %>');
            users.close();
            var classlist = $find('<%= rwClassList.ClientID %>');
            classlist.close();

            var radwindow = $find('<%= pTransactionSummaryTrainer.ClientID %>');
            radwindow.show();
        }

        function urbtnTransactionsListTrainer_Click() {
            var radwindow = $find('<%= pTransactionSummaryTrainer.ClientID %>');
            radwindow.close();
        }

        function fileUploaderTrainerTrainer_OnClientFileDropped() {
            showPageLoading();
            $("#<%= btnUploadBulkTrainer.ClientID %>").click();
        }

        function btnToggleAssignState_ClientToggleStateChanged(sender, args) {
            var state = args.get_currentToggleState().get_value();
            var classtrainerid = args.get_commandArgument();

            $("#<%= hfSelectedClassTrainerID.ClientID %>").val(classtrainerid);

            if (state == 3) {
                var radwindow = $find('<%= rwClassList.ClientID %>');
                radwindow.show();
            }
            console.log(state + "" + classtrainerid)
        }

        function clearSelectedClassItems() {
            selectedTrainerItems = [];
            var grid = $find("<%= rgClasses.ClientID %>");
            grid.clearSelectedItems();
        }

        function closeClassListWindow() {
            var radwindow = $find('<%= rwClassList.ClientID %>');
            radwindow.close();
        }

        function keyPress(sender, args) {
            var text = sender.get_value() + args.get_keyCharacter();
            if (!text.match('^[0-9]+$'))
                args.set_cancel(true);
        }

        function resizeTrainerGrid() {
            var grid = $find("<%= gridClassTrainers.ClientID %>");
            //showTrainerGrid();
            var columns = grid.get_masterTableView().get_columns();
            for (var i = 0; i < columns.length; i++) {
                columns[i].resizeToFit();
            }
        }

        function showTrainerGrid() {
            $("#<%= gridClassTrainers.ClientID %>").removeClass("display-none");
        }

    </script>
</rad:RadScriptBlock>
<asp:HiddenField ID="hfSelectedTrainer" runat="server" />
<asp:HiddenField ID="hfSelectedClassTrainerID" runat="server" />
