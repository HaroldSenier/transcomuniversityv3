﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TranscomUniversityV3Model;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using Telerik.Web.UI;
using TransactionNames;
using System.IO;
using System.Drawing;

public partial class UserControl_Pages_ClassManagement_Trainer : System.Web.UI.UserControl
{
    private static int CLASSID;

    protected void Page_Load(object sender, EventArgs e)
    {
        CLASSID = Convert.ToInt32(Utils.Decrypt(Request.QueryString["ClassId"]));
    }

    protected void gridClassTrainers_NeedDataSource(object sender, EventArgs e)
    {
        //if (gridClassTrainers.Items.Count <= 0)
        //    gridClassTrainers.CssClass = "GridLess display-none";
        //else
        //    gridClassTrainers.CssClass = "GridLess";

        var db = new TranscomUniversityV3ModelContainer();

        var res = db.vw_TranscomUniversity_ClassTrainers
                    .AsEnumerable()
                    .Where(c => c.ClassID == CLASSID)
                    .Select(c => new
                    {
                        c.ClassID,
                        c.TrainerCIM,
                        c.Name,
                        c.UserImageUrl,
                        c.Campaign,
                        c.Division,
                        c.ReportingManager,
                        AssignState = GetAssignStateValue((int)c.AssignState)
                    })
                    .ToList();

        gridClassTrainers.DataSource = res;

        lblTrainerCount.Text = res.Count.ToString();

    }

    private static string GetAssignStateValue(int assignState)
    {
        if (assignState == 0)
            return "Not Assigned";
        if (assignState == 1)
            return "Lead Trainer";
        if (assignState == 2)
            return "Backup Trainer";
        else
            return "Unidentified";
    }

    protected void gridClassTrainers_EditMode_NeedDataSource(object sender, EventArgs e)
    {
        var db = new TranscomUniversityV3ModelContainer();

        var res = db.vw_TranscomUniversity_ClassTrainers
                    .Where(c => c.ClassID == CLASSID)
                    .ToList();
        gridClassTrainer_EditMode.DataSource = res;

        lblTrainerCount.Text = res.Count.ToString();

    }

    protected void btnAddTrainer_Click(object sender, EventArgs e)
    {
        try
        {
            if (Page.IsValid)
            {

                int CIM = Convert.ToInt32(txtTrainerCIM.Text);
                int addedBy = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
                var db = new TranscomUniversityV3ModelContainer();

                string _userIP = DataHelper.GetIPAddress();
                string _userID = DataHelper.GetCurrentUserCIM();
                double _duration = 0;

                DateTime startTime;
                DateTime endTime;


                if (isUser(CIM))
                {
                    var dts = db.CreateQuery<DateTime>("CurrentDateTime() ");
                    startTime = dts.AsEnumerable().First();

                    db.pr_TranscomUniversity_InsertUpdateClassTrainer(CLASSID, CIM, 0, addedBy);



                    string _action = "Added Trainer " + DataHelper.getUserName(CIM);
                    var dte = db.CreateQuery<DateTime>("CurrentDateTime()");

                    endTime = dte.AsEnumerable().First();
                    _duration = (endTime - startTime).TotalMilliseconds;

                    int l_actionID = DataHelper.getLastLogID() + 1;
                    DataHelper.logClass(CLASSID, l_actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);

                    gridClassTrainers.Rebind();
                    
                    
                    RadWindowManager1_pt.RadAlert(Resources.LocalizedResource.SuccessfullyAddedNewTrainer.ToString(), 330, 180, Resources.LocalizedResource.Successfull.ToString(), "");

                }
                else
                {
                    RadWindowManager1_pt.RadAlert(Resources.LocalizedResource.TheCIMyouenteredisnotvalid.ToString(), 330, 180, Resources.LocalizedResource.ErrorCIM.ToString(), "");
                }
                txtTrainerCIM.Text = "";

            }

        }
        catch (FormatException ex)
        {
            RadWindowManager1_pt.RadAlert(Resources.LocalizedResource.TheCIMyouenteredisnotvalid.ToString(), 330, 180, Resources.LocalizedResource.ErrorCIM.ToString(), "");
            throw;
        }
        catch (Exception ex)
        {

            RadWindowManager1_pt.RadAlert(ex.Message, 330, 180, Resources.LocalizedResource.Error.ToString(), "");
            throw;
        }
    }

    protected void btnAssignMultipleTrainers_Click(object sender, EventArgs e)
    {
        try
        {
            string _userIP = DataHelper.GetIPAddress();
            string _userID = DataHelper.GetCurrentUserCIM();
            double _duration = 0;

            DateTime startTime;
            DateTime endTime;

            string stat = "";
            int successCount = 0;

            List<Transaction> UserList = new List<Transaction>();

            var db = new TranscomUniversityV3ModelContainer();

            string userList = hfSelectedTrainer.Value;
            string[] data = userList.Split(',');

            var classId = CLASSID;
            var delegator = Convert.ToInt32(_userID);
            int TrainerCIM = -1;

            if (data.Length > 1)
            {
                var dts = db.CreateQuery<DateTime>("CurrentDateTime() ");
                startTime = dts.AsEnumerable().First();

                for (int i = 0; i < (data.Length - 1); i++)
                {
                    TrainerCIM = Convert.ToInt32(data[i]);

                    pr_TranscomUniversity_InsertUpdateClassTrainer_Result _res = db.pr_TranscomUniversity_InsertUpdateClassTrainer(classId, TrainerCIM, 0, delegator).SingleOrDefault();

                    if (_res.Msg == "1")
                    {
                        stat = "Successful";
                        successCount++;
                    }
                    else if (_res.Msg == "2")
                    {
                        stat = "Duplicate";
                    }
                    else
                    {
                        stat = "Fail";
                    }

                    Transaction Trainer = new Transaction
                    {
                        Id = Convert.ToInt32(data[i]),
                        Name = DataHelper.getUserName(TrainerCIM),
                        Status = stat
                    };

                    UserList.Add(Trainer);
                }

                if (successCount > 0)
                {
                    string _action = successCount > 1 ? "Added " + successCount + " Trainers" : "Added Trainer " + DataHelper.getUserName(TrainerCIM);
                    var dte = db.CreateQuery<DateTime>("CurrentDateTime() ");

                    endTime = dte.AsEnumerable().First();
                    _duration = (endTime - startTime).TotalMilliseconds;

                    int l_actionID = DataHelper.getLastLogID() + 1;
                    DataHelper.logClass(classId, l_actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);
                }



                pTransactionSummaryTrainer.Title = Resources.LocalizedResource.AddTrainerResult.ToString();
                urrgTransactionSummaryTrainer.DataSource = UserList;
                urrgTransactionSummaryTrainer.DataBind();
                gridClassTrainers.Rebind();
                ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", Utils.callClientScript("pshowTransactionTrainer"), true);
            }
            else
            {
                RadWindowManager1_pt.RadAlert(Resources.LocalizedResource.Pleaseselectatleastone1User.ToString(), 330, 180, Resources.LocalizedResource.SystemMessage.ToString(), "");
            }
        }
        catch
        {
            RadWindowManager1_pt.RadAlert(Resources.LocalizedResource.ErrorassigningtoanothercoursesPleasecontactyourSystemAdministrator.ToString(), 330, 180, Resources.LocalizedResource.ErrorMessage.ToString(), "");
        }
    }

    protected void rgTransactionSummary_OnItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            GridDataItem dataBoundItem = e.Item as GridDataItem;

            if (dataBoundItem["Status"].Text == "Duplicate")
            {
                dataBoundItem["Status"].ForeColor = Color.DarkGray;
            }
            else if (dataBoundItem["Status"].Text == "Fail")
            {
                dataBoundItem["Status"].ForeColor = Color.Red;
            }
            else
            {
                dataBoundItem["Status"].ForeColor = Color.YellowGreen;
            }
        }

    }

    protected void btnUploadBulkUsers_Click(object sender, EventArgs e)
    {
        string _userIP = DataHelper.GetIPAddress();
        string _userID = DataHelper.GetCurrentUserCIM();
        double _duration = 0;

        DateTime startTime;
        DateTime endTime;

        try
        {
            if (fileUploaderTrainer.UploadedFiles.Count == 0 || fileUploaderTrainer.UploadedFiles[0].GetExtension() != ".csv")
            {
                fileUploaderTrainer.UploadedFiles.Clear();
                RadWindowManager1_pt.RadAlert(Resources.LocalizedResource.PleaseSelectACsvFileToUpload.ToString(), 330, 180, Resources.LocalizedResource.SystemMessage.ToString(), "");
                return;
            }
            //////////
            UploadedFile f = fileUploaderTrainer.UploadedFiles[0];
            var db = new TranscomUniversityV3ModelContainer();
            var dts = db.CreateQuery<DateTime>("CurrentDateTime() ");
            startTime = dts.AsEnumerable().First();

            string csvPath = Server.MapPath("~/Files/BulkTrainers/") + f.FileName;
            f.SaveAs(csvPath);

            string csvData = File.ReadAllText(csvPath);
            List<Transaction> TrainerList = new List<Transaction>();
            int ui = 0;
            int successCount = 0;
            int CourseID = Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"]));


            foreach (string row in csvData.Split('\n'))
            {
                string stat = "";
                if (ui == 0 && !string.IsNullOrEmpty(row))
                {
                    string[] filecolum = row.Split(',');
                    if (filecolum[0].Trim().ToLower() != "trainercim" || filecolum[1].Trim().ToLower() != "assignstateid" || filecolum[2].Trim().ToLower() != "assignstate")
                    {
                        RadWindowManager1_pt.RadAlert(Resources.LocalizedResource.CSVFiledoesnotcontaincorrectColumnsPleaseselectanother.ToString(), 330, 180, Resources.LocalizedResource.SystemMessage.ToString(), "");
                        return;
                    }
                }
                else if (!string.IsNullOrEmpty(row) && ui > 0)
                {
                    string[] cells = row.Split(',');
                    int TrainerCIM = Convert.ToInt32(cells[0]);
                    int AssignStateID = Convert.ToInt32(cells[1]);
                    int delegator = Convert.ToInt32(_userID);
                    pr_TranscomUniversity_InsertUpdateClassTrainer_Result _res = db.pr_TranscomUniversity_InsertUpdateClassTrainer(CLASSID, TrainerCIM, AssignStateID, delegator).SingleOrDefault();

                    if (_res.Msg == "1")
                    {
                        stat = "Successful";
                        successCount++;
                    }
                    else if (_res.Msg == "2")
                    {
                        stat = "Duplicate";
                    }
                    else
                    {
                        stat = "Fail";
                    }

                    Transaction Trainer = new Transaction
                    {
                        Id = ui,
                        Name = DataHelper.getUserName(TrainerCIM),
                        Status = stat
                    };

                    TrainerList.Add(Trainer);
                }
                ui++;
            }

            if (successCount > 0)
            {
                string _action = "Uploaded " + f.FileName + " Trainer List";
                var dte = db.CreateQuery<DateTime>("CurrentDateTime() ");

                endTime = dte.AsEnumerable().First();
                _duration = (endTime - startTime).TotalMilliseconds;

                int l_actionID = DataHelper.getLastLogID() + 1;
                DataHelper.logClass(CLASSID, l_actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);

                gridClassTrainers.Rebind();
            }
            fileUploaderTrainer.UploadedFiles.Clear();

            pTransactionSummaryTrainer.Title = Resources.LocalizedResource.AddTrainerResult.ToString();
            urrgTransactionSummaryTrainer.DataSource = TrainerList;
            urrgTransactionSummaryTrainer.DataBind();
            gridClassTrainers.Rebind();
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "pshowTransactionTrainer1", Utils.callClientScript("pshowTransactionTrainer"), true);
            //////////////////
           
            
        }
        catch
        {
            RadWindowManager1_pt.RadAlert(Resources.LocalizedResource.ErroronaddingparticipantssPleasecontactyourSystemAdministrator.ToString(), 330, 180, Resources.LocalizedResource.ErrorMessage.ToString(), "");
            
        }
        finally {
            fileUploaderTrainer.UploadedFiles.Clear();
        }
    }

    private bool isUser(int cim)
    {
        var db = new TranscomUniversityV3ModelContainer();
        var u = db.tbl_TranscomUniversity_Cor_UsersInfo
                    .Where(c => c.CIMNumber == cim)
                    .Select(c => c.CIMNumber)
                    .SingleOrDefault();
        return u != null ? true : false;
    }

    protected void btnEdit_Click(object sender, EventArgs e)
    {
        btnEdit.Visible = false;
        btnCancel.Visible = true;
        btnSaveChanges.Visible = true;
        gridClassTrainer_EditMode.Visible = true;
        gridClassTrainer_EditMode.Rebind();
        gridClassTrainers.Visible = false;
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        btnEdit.Visible = true;
        btnCancel.Visible = false;
        btnSaveChanges.Visible = false;
        gridClassTrainer_EditMode.Visible = false;
        gridClassTrainers.Visible = true;
    }

    protected void btnSaveChanges_Click(object sender, EventArgs e)
    {
        try
        {
            foreach (GridDataItem item in gridClassTrainer_EditMode.Items)
            {
                var db = new TranscomUniversityV3ModelContainer();

                //Logger variables Start
                string _userIP = DataHelper.GetIPAddress();
                string _userID = DataHelper.GetCurrentUserCIM();
                double _duration = 0;

                DateTime startTime;
                DateTime endTime;

                var dts = db.CreateQuery<DateTime>("CurrentDateTime() ");
                startTime = dts.AsEnumerable().First();
                //Logger variables End

                RadToggleButton btnToggleAssignState = item.Cells[8].FindControl("btnToggleAssignState") as RadToggleButton;
                int assignState = btnToggleAssignState.SelectedToggleStateIndex == 3 ? 0 : btnToggleAssignState.SelectedToggleStateIndex;
                int classTrainerId = Convert.ToInt32(item.GetDataKeyValue("ClassTrainerID"));
                int modifiedBy = Convert.ToInt32(DataHelper.GetCurrentUserCIM());

                var tDetails = db.vw_TranscomUniversity_ClassTrainers.Select(c => new { c.ClassTrainerID, c.Name, c.AssignState }).Where(c => c.ClassTrainerID == classTrainerId).SingleOrDefault();
                string tname = tDetails.Name;
                int tstate = (int)tDetails.AssignState;

                if (assignState != tstate)
                {
                    db.pr_TranscomUniversity_UpdateClassTrainerState(classTrainerId, modifiedBy, assignState);

                    //logger start transaction
                    string _action = "Changed State of " + tname + " from " + GetAssignStateValue(tstate) + " to " + GetAssignStateValue(assignState);
                    var dte = db.CreateQuery<DateTime>("CurrentDateTime() ");

                    endTime = dte.AsEnumerable().First();
                    _duration = (endTime - startTime).TotalMilliseconds;

                    int l_actionID = DataHelper.getLastLogID() + 1;
                    DataHelper.logClass(CLASSID, l_actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);
                    //logger end transaction
                }


            }

            btnEdit.Visible = true;
            btnCancel.Visible = false;
            btnSaveChanges.Visible = false;
            gridClassTrainer_EditMode.Visible = false;
            gridClassTrainers.Visible = true;
            gridClassTrainers.Rebind();
            RadWindowManager1_pt.RadAlert(Resources.LocalizedResource.TrainerStateSuccessfullyupdated.ToString(), 330, 180, Resources.LocalizedResource.UpdateSuccessfull.ToString(), "");
        }
        catch (Exception ex)
        {
            RadWindowManager1_pt.RadAlert(Resources.LocalizedResource.ErrorinupdatingTrainerStatePleasecontactyouradministrator.ToString(), 330, 180, Resources.LocalizedResource.ErrorUpdating.ToString(), "");
            throw;
        }

    }

    protected void gridClassTrainers_EditMode_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            GridDataItem item = e.Item as GridDataItem;

            RadToggleButton btnToggleAssignState = e.Item.FindControl("btnToggleAssignState") as RadToggleButton;
            btnToggleAssignState.SetSelectedToggleStateByValue(item.GetDataKeyValue("AssignState").ToString());

        }
    }

    protected void rgClasses_NeedDataSource(object sender, EventArgs e)
    {
        var db = new TranscomUniversityV3ModelContainer();
        var res = db.vw_TranscomUniversity_ClassDetails.Select(c => new
        {
            c.ClassName,
            c.ClassID
        })
                    .ToList();

        rgClasses.DataSource = res;
    }

    protected void btnTransfer_Click(object sender, EventArgs e)
    {
        //List<Transaction> ClassList = new List<Transaction>();
        foreach (GridItem item in rgClasses.MasterTableView.Items)
        {
            GridDataItem dataitem = (GridDataItem)item;
            int ClassTrainerID = Convert.ToInt32(hfSelectedClassTrainerID.Value);
            int modifiedBy = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
            if (dataitem.Selected)
            {
                var db = new TranscomUniversityV3ModelContainer();

                //Logger variables Start
                string _userIP = DataHelper.GetIPAddress();
                string _userID = DataHelper.GetCurrentUserCIM();
                double _duration = 0;

                DateTime startTime;
                DateTime endTime;

                var dts = db.CreateQuery<DateTime>("CurrentDateTime() ");
                startTime = dts.AsEnumerable().First();
                //Logger variables End

                int destinationClassID = Convert.ToInt32(dataitem.GetDataKeyValue("ClassID"));

                string fClass = db.vw_TranscomUniversity_ClassDetails.Select(c => new
                {
                    c.ClassName,
                    c.ClassID
                }).Where(c => c.ClassID == CLASSID).SingleOrDefault().ClassName;

                pr_TranscomUniversity_TransferClassTrainer_Result res = db.pr_TranscomUniversity_TransferClassTrainer(ClassTrainerID, destinationClassID, modifiedBy).SingleOrDefault();

                string tClass = db.vw_TranscomUniversity_ClassDetails.Select(c => new
                                {
                                    c.ClassName,
                                    c.ClassID
                                }).Where(c => c.ClassID == destinationClassID)
                                                    .SingleOrDefault().ClassName;



                var tDetails = db.vw_TranscomUniversity_ClassTrainers.Select(c => new { c.ClassTrainerID, c.Name, c.AssignState }).Where(c => c.ClassTrainerID == ClassTrainerID).SingleOrDefault();
                string tname = tDetails.Name;

                //logger start transaction
                string _action = "Transfered Trainer " + tname + " to " + tClass + " from " + fClass;

                var dte = db.CreateQuery<DateTime>("CurrentDateTime() ");

                endTime = dte.AsEnumerable().First();
                _duration = (endTime - startTime).TotalMilliseconds;

                int l_actionID = DataHelper.getLastLogID() + 1;
                DataHelper.logClass(CLASSID, l_actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);

                _action = "Transfered Trainer " + tname + " from " + fClass;
                DataHelper.logClass(destinationClassID, l_actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);
                //logger end transaction

                if (res.Msg == 1)
                {
                    gridClassTrainer_EditMode.Rebind();
                    
                    RadWindowManager1_pt.RadAlert("Trainer already exists in " + tClass, 330, 180, Resources.LocalizedResource.ErrorTransferring.ToString(), "");
                }
                else
                {
                    btnEdit.Visible = true;
                    btnCancel.Visible = false;
                    btnSaveChanges.Visible = false;
                    gridClassTrainer_EditMode.Visible = false;
                    gridClassTrainers.Visible = true;

                    gridClassTrainers.Rebind();
                    RadWindowManager1_pt.RadAlert(Resources.LocalizedResource.TrainerSuccessfullytransferred.ToString(), 330, 180, Resources.LocalizedResource.SuccessfullTransferring.ToString(), "");
                }
            }
        }

        //gridClassTrainer_EditMode.Rebind();




        //pTransactionSummaryTrainer.Title = "Trainer Transfer Result";
        //urrgTransactionSummaryTrainer.DataSource = ClassList;
        //urrgTransactionSummaryTrainer.DataBind();
        ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", Utils.callClientScript("closeClassListWindow"), true);
    }

    protected void btnViewTrainerList_Click(object sender, EventArgs e) 
    {
        foreach (GridColumn column in gridViewTrainersList.MasterTableView.OwnerGrid.Columns)
        {
            column.CurrentFilterFunction = GridKnownFunction.NoFilter;
            column.CurrentFilterValue = string.Empty;
        }
        gridViewTrainersList.MasterTableView.FilterExpression = string.Empty;
        gridViewTrainersList.Rebind();

        ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", DataHelper.callClientScript("viewTrainersList"), true);
    }
   
}