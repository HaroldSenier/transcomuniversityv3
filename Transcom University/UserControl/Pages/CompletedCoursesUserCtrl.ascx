﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CompletedCoursesUserCtrl.ascx.cs" Inherits="UserControl_Pages_CompletedCoursesUserCtrl" %>

<rad:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel2" runat="server" Transparency="25" IsSticky="true" CssClass="Loading myLoading" />
<rad:RadWindowManager ID="rwmCompletedResult" RenderMode="Lightweight" EnableShadow="true"
    VisibleOnPageLoad="false" Behaviors="Close, Move" DestroyOnClose="true" Modal="true"
    Opacity="99" runat="server" VisibleStatusbar="false" Skin="Bootstrap" Width="620px"
    Height="400px">
</rad:RadWindowManager>
<rad:RadCodeBlock ID="rcbCompletedCourses" runat="server" Visible="false">
    <script type="text/javascript">
        //listview variables
        var completedListView;

        var completedGridView;
        var arrcompleted;

        var virtualCount;
        var pageSize;
        var pageIndex;
        var defaultPageSize = 12;
        var oldPageSize;
        var isViewAll = false;

        function pageLoad() {
            initializeCompletedCourse();
        }

        //--Start completedResult Binding

        //initialize completed Course
        function initializeCompletedCourse() {
            $ = $telerik.$;
            //this will fire the OnCommand event
            completedListView = $find("<%= lvCompletedResult.ClientID%>");
            completedListView.rebind();
            pageSize = defaultPageSize;
            completedListView.set_pageSize(pageSize);
            completedListView.page(0);

            completedGridView = $find("<%= gridCompletedResult.ClientID%>");

            $(".mcPager .pagePrev").click(function (e) {
                completedListView.page(completedListView.get_currentPageIndex() - 1);
            });

            $(".mcPager .pageNext").click(function (e) {
                completedListView.page(completedListView.get_currentPageIndex() + 1);
            });

            $("#completedListView").on("click", ".mc-item", function (e) {
                completedListView.toggleSelection($(this).index());
            });

        }

        function bindCompletedCourseList() {
            completedListView = $find("<%= lvCompletedResult.ClientID %>");

            var startRowIndex = completedListView.get_currentPageIndex() * completedListView.get_pageSize(),
                maximumRows = completedListView.get_pageSize(),
                sortExpression = completedListView.get_sortExpressions().toLinq();

            //make a call to get the data
            PageMethods.GetCompletedCoursesData(startRowIndex, maximumRows, sortExpression,
                function (result) {
                    var count = result.count;
                    var data = result.data;
                        console.log("hey");
                    
                    completedListView.set_virtualItemCount(count);
                    if (count > 0) {
                        $(".mcPager .pagePrev").removeClass("display-none");
                        $(".mcPager .pageNext").removeClass("display-none");
                        $(".mcViews").removeClass("display-none");
                        $(".mcPager").removeClass("display-none");
                        $(".switchView").removeClass("display-none");
                        $(".ddlPageSize-container").removeClass("display-none");
                    }

                    pageSize = completedListView.get_pageSize();
                    virtualCount = completedListView.get_virtualItemCount();
                    completedListView.set_dataSource(data);

                    console.log(virtualCount);
                    $(".js-mc-loader").hide();
                    completedListView.dataBind();

                    completedGridView.set_dataSource(data);
                    completedGridView.dataBind();

                    if (isViewAll == true) {
                        $("#<%=btnViewAllListEC.ClientID%>").addClass("display-none");
                        $("#<%=btnBackListEC.ClientID%>").removeClass("display-none");

                        $("#btnViewAllGridTLC").addClass("display-none");
                        $("#btnBackGridTLC").removeClass("display-none");

                    }
                     else {
                        $("#<%=btnViewAllListEC.ClientID%>").removeClass("display-none");
                        $("#<%=btnBackListEC.ClientID%>").addClass("display-none");

                        $("#btnViewAllGridTLC").removeClass("display-none");
                        $("#btnBackGridTLC").addClass("display-none");
                    }

                    $(".myLoading").hide();
                    pageNumberGenerator();
                    console.log("just rebind");
                },
                function (error) {
                    var code = error._statusCode;
                    var RadAlert = $("#<%=hdnRadAlert.ClientID%>").val();
                    var RadAlertErrorMessage = $("#<%=hdnRadAlertErrorMessage.ClientID%>").val();
                    if (code == 401)
                        location.reload();
                    else if (code == 500)

                        radalert(RadAlert, 330, 180, RadAlertErrorMessage, "");
                });
        }

        //Client Events completedCourses

        function ccOnListViewDataBinding(sender, args) {
            sender.set_selectedIndexes([]);
        }

        function ccOnListViewCommand(sender, args) {
            args.set_cancel(true);
            bindCompletedCourseList();
        }

        function btnSwitchView_Click() {
            if ($('#<%= pnllvCompletedResult.ClientID %>').hasClass("display-none")) {

                $("#<%= pnllvCompletedResult.ClientID %>").removeClass("display-none");
                $("#<%= pnlgridCompletedResult.ClientID %>").addClass("display-none");
                $("#<%= btnSwitchToGridView.ClientID %>").addClass("display-none");
                $("#<%= btnSwitchToListView.ClientID %>").removeClass("display-none");
                //switch to gridview

                console.log("to list");
            }
            else {
                //switch to list view

                $("#<%= pnllvCompletedResult.ClientID %>").addClass("display-none");
                $("#<%= pnlgridCompletedResult.ClientID %>").removeClass("display-none");
                $("#<%= btnSwitchToListView.ClientID %>").addClass("display-none");
                $("#<%= btnSwitchToGridView.ClientID %>").removeClass("display-none");
                console.log("to grid");
                var stored = sessionStorage.getItem('completedResult');
                console.log("tolist" + stored);

                completedGridView.set_dataSource(arrcompleted);
                completedGridView.dataBind();
            }

            return false;
        }

        function inGridView() {
            if ($('#<%= pnllvCompletedResult.ClientID %>').hasClass("display-none"))
                return false;
            else
                return true;
        }

        function sortCompleted() {
            completedListView.get_sortExpressions().clear();
            completedListView.get_sortExpressions().add('CourseTitle', "DESC");
            completedListView.rebind();
        }

        function generatePageNumbers() {
            var maxPage = Math.ceil(virtualCount / pageSize);
            var pageVirtualCount = virtualCount;
            var pages = $(".js-page-numbers ul").html('');

            for (var i = 1; i <= maxPage; i++) {
                var pages = $(".js-page-numbers ul").html();
                var rp = i - 1;
                $(".js-page-numbers ul").html(pages + '<li class="page-number"> <a href="javascript:void(0);" onclick="gotoPage(' + rp + ');">' + i + '</a></li>');
            }
        }

        function gotoPage(pageNum) {
            $(".js-page-numbers ul li").removeClass("selected-page");
            completedListView.page(pageNum);
            completedGridView.page(pageNum);
        }

        function setPage() {
            pageIndex = completedListView.get_currentPageIndex();
            var myPage = pageIndex + 1;
            $(".js-page-numbers ul li").removeClass("selected-page");
            $(".js-page-numbers ul li:nth-child(" + myPage + ")").addClass("selected-page");
            var itemCount = pageSize * (pageIndex + 1) <= virtualCount ? (pageSize * (pageIndex + 1)) : ((pageIndex * pageSize) + (virtualCount % pageSize));
            $(".lblShowNumber").html("Showing " + itemCount + " of " + virtualCount + " Completed Courses");
        }

        function setDdlPagerValue(selectedPageSize) {
            if (selectedPageSize >= virtualCount) {
                $("#ddlpageSize").val(virtualCount);
            } else {
                $("#ddlpageSize").val(selectedPageSize);
            }
        }

        function changePageSize(pageSizer) {
            showLoading();
            isViewAll = false;
            var pageSizerID = pageSizer.id;
            pageSize = $("#" + pageSizerID).val();

            if (!$.isNumeric(pageSize)) {
                pageSize = defaultPageSize;
            }

            rebindPageSize(pageSize)
        }

        function pageNumberGenerator() {
            generatePageNumbers(); //generate page numbers depending on virtual count and pagesize
            setPage(); //set the numbers displayed in "n of n Take Later Courses"
        }

        function btnViewAllListEC_Click() {
            showLoading();
            console.log(virtualCount);
            isViewAll = true;
            oldPageSize = completedListView.get_pageSize();
            rebindPageSize(virtualCount);

            return false;
        }

        function btnBackListEC_Click() {
            showLoading();
            isViewAll = false;
            rebindPageSize(oldPageSize);

            return false;
        }

        function rebindPageSize(ps) {
            completedListView.set_pageSize(ps);
            completedListView.page(0);
            completedListView.rebind();

            completedGridView.set_pageSize(ps);
            completedGridView.page(0);
        }

        function showLoading() {
            var loading = $find("<%= RadAjaxLoadingPanel2.ClientID %>");
            loading.show("<%= pnllvCompletedResult.ClientID %>");
        }

        function setStatusFontColor() {
           $('.statusContainer').each(function (i, obj) {
               var status = jQuery('[id=hidStatus]', this).val();

               if (status == "Pending Approval")
                   jQuery('[id=status]', this).css({ "color": "#0000CC" });

               if (status == "Approved")
                   jQuery('[id=status]', this).css({ "color": "#d8c91e" });
               else if (status == "Declined" || status == "Cancelled (By Learner)" || status == "Overdue" || status == "Cancelled (By Assigner)")
                   jQuery('[id=status]', this).css({ "color": "#E51C23" });
               else if (status == "Completed")
                   jQuery('[id=status]', this).css({ "color": "#259B24" });
               else if (status == "In Progress")
                   jQuery('[id=status]', this).css({ "color": "#C9AE1C" });
               else
                   jQuery('[id=status]', this).css({ "color": "#000" });
           });
       }
    </script>
</rad:RadCodeBlock>
<asp:HiddenField runat="server" ID="hdnRadAlert" Value="<%$ Resources:LocalizedResource, SomethingWentWhileSubmittingYourReportPleaseTryAgain %>" />
<asp:HiddenField runat="server" ID="hdnRadAlertErrorMessage" Value="<%$ Resources:LocalizedResource, ErrorMessage %>" />
<asp:Panel ID="pnlCompletedResultContainer" runat="server" CssClass="enrolled-container search-container" Style="width: 100%; min-height: calc(100vh - 200px);">
    <div class="row">
        <div class="switchView display-none pull-left col-md-6 " style="line-height: 34px; left: 5%; padding-top: 0.5%;">
            <asp:LinkButton ID="btnSwitchToListView" runat="server" OnClientClick="btnSwitchView_Click(); return false;"
                Font-Underline="false" aria-hidden="true" ForeColor="Black" ToolTip="Switch to List View">
                <i id="switchClassListView" runat="server" class="fa fa-th-list "></i>
            </asp:LinkButton>
            <asp:LinkButton ID="btnSwitchToGridView" runat="server" OnClientClick="btnSwitchView_Click(); return false;"
                Font-Underline="false" aria-hidden="true" CssClass="display-none" ToolTip="Switch to Grid View">
                <i id="switchClassGridView" runat="server" class="fa fa-th "></i>
            </asp:LinkButton>
        </div>
        <div class="ddlPageSize-container display-none col-md-6" style="left: -5%; padding-top: 1%;">
            <div class="display-inline-flex pull-right">
                <div class="col-md-2">
                    <p style="line-height: 34px;">
                       <asp:Label runat="server" ID="lblShow" Text="<%$ Resources:LocalizedResource, Show %>"></asp:Label>
                    </p>
                </div>
                <div class="col-md-4">
                    <asp:DropDownList ID="ddlpageSize" runat="server" CssClass="form-control rounded-corner"
                        onchange="changePageSize(this); return false;" ClientIDMode="Static">
                    </asp:DropDownList>
                </div>
                <div class="col-md-6 no-padding">
                    <p style="line-height: 34px;">
                        <asp:Label runat="server" ID="lblItemPerPage" Text="<%$ Resources:LocalizedResource, ItemsPerPage %>"></asp:Label>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <asp:Panel ID="pnllvCompletedResult" runat="server" CssClass="enrolled-view">
        <div id="ec-containerListView" class="mc-container col-md-11" style="left: 3.4%">
            <div class="mcPager container display-none">
                <p class="lblShowNumber" class="pull-left" style="color: Teal;">
                </p>
                <div class="row-fluid mcViews">
                    <div class="pull-right">
                        <button id="btnViewAllListEC" onclick="btnViewAllListEC_Click(); return false;" class="btn-transparent"
                            style="line-height: 32px; color: Blue; text-decoration: underline;" title="View All" runat="server">
                            <asp:Label runat="server" ID="lblViewAllListECViewall" Text="<%$ Resources:LocalizedResource, ViewAll %>"></asp:Label>
                        </button>
                        <button runat="server" id="btnBackListEC" onclick="btnBackListEC_Click(); return false;" class="display-none btn-transparent"
                            style="line-height: 32px;" title="<%$ Resources:LocalizedResource, Back %>">
                            <i class="fa fa-long-arrow-left "></i>
                        </button>
                    </div>
                </div>
            </div>
            <div class="loader js-mc-loader">
            </div>
            <rad:RadListView ID="lvCompletedResult" runat="server" AllowPaging="true" PageSize="12"
                AllowMultiFieldSorting="true">
                <LayoutTemplate>
                    <div id="completedListView" style="min-height: 950px;">
                        <div id="ec-items" class="display-flow">
                        </div>
                        <div class="mcPager container display-none">
                            <div class="js-page-numbers display-inline-flex takeLaterCourse-page" style="padding-top: 5%;">
                                <a class="pagePrev black-arrow fa fa-caret-left no-underline-hover" href="javascript:void(0);"
                                    title="#= Resources:LocalizedResource, GoToPreviousPage #"></a>
                                <ul class="nostyle display-inline-flex black-font">
                                </ul>
                                <a class="pageNext black-arrow fa fa-caret-right no-underline-hover" href="javascript:void(0);"
                                    title="#= Resources:LocalizedResource, GoToNextPage #"></a>
                            </div>
                        </div>
                    </div>
                </LayoutTemplate>
                <ClientSettings>
                    <DataBinding ItemPlaceHolderID="ec-items">
                        <ItemTemplate>
                            <div class="col-md-3">
                                <a id="hllvCompletedResult" onclick='confirmLaunchCourse("#= EncryptedCourseID #");' style="color:Black;" title=" #= CourseTitle #">
                                    <div id="divCompletedResult" runat="server" class="mc-item lv rlvI">
                                        <div class="photo-container">
                                            <image src='Media/Uploads/CourseImg/#= CourseID #/#= CourseImage #' onerror="this.src='Media/Uploads/CourseImg/No_image.jpg'" style="height:130px; width:190px; filter: grayscale(80%) brightness(.8);">
                                            <div class="courseStatus">
                                                Completed
                                            </div>
                                            <div class="transparent_btn">
                                                View
                                            </div>
                                            </image>
                                        </div>
                                        <div class="category font-bold">
                                            #= CourseTitle #
                                        </div>
                                            <div class="category">
                                            #= CourseType #
                                        </div>
                                        <div class="category">
                                            #= CourseDuration #
                                        </div>
                                        <div class="category">
                                            #= CourseCategory #
                                        </div>
                                        <div class="category">
                                            #= CourseSubcategory #
                                        </div>
                                        <div class="clearfix">
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </ItemTemplate>
                        <EmptyDataTemplate>
                        <p style="margin-left: 10px;">
                         No Result Found...
                        </p>
                        </EmptyDataTemplate>
                        <DataService EnableCaching="true" />
                    </DataBinding>
                    <ClientEvents OnCommand="ccOnListViewCommand" OnDataBinding="ccOnListViewDataBinding">
                    </ClientEvents>
                </ClientSettings>
            </rad:RadListView>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlgridCompletedResult" runat="server" CssClass="display-none enrolled-view">
        <div id="ec-containerGridView" class="mc-container col-md-11" style="left: 3.4%">
            <div class="loader js-mc-loader">
            </div>
            <div class="mcPager container display-none">
                <p class="lblShowNumber" class="pull-left" style="color: Teal;">
                </p>
                <div class="row-fluid mcViews">
                    <div class="pull-right">
                        <button runat="server" id="btnViewAllGridEC" onclick="btnViewAllListEC_Click(); return false;" class="btn-transparent"
                            style="line-height: 32px; color: Blue; text-decoration: underline;" title="<%$ Resources:LocalizedResource, ViewAll %>">
                            <asp:Label runat="server" ID="lblViewAllGridECViewAll" Text="<%$ Resources:LocalizedResource, ViewAll %>"></asp:Label>
                        </button>
                        <button id="btnBackGridEC" onclick="btnBackListEC_Click(); return false;" class="display-none btn-transparent"
                            style="line-height: 32px;" title="#= Resources:LocalizedResource, Back #">
                            <i class="fa fa-long-arrow-left "></i>
                        </button>
                    </div>
                </div>
            </div>
            <rad:RadListView ID="gridCompletedResult" runat="server" AllowPaging="true" PageSize="8">
                <LayoutTemplate>
                    <div id="completedGridView">
                        <table class="gridMainTable table table-bordered table-striped course-grid">
                            <thead>
                                <tr class="rlvHeader">
                                    <th class="btn-teal">
                                        <asp:Label runat="server" ID="lblCourseTitle" Text="<%$ Resources:LocalizedResource, CourseTitle %>"></asp:Label>
                                    </th>
                                    <th class="btn-teal">
                                        <asp:Label runat="server" ID="lblDesc" Text="<%$ Resources:LocalizedResource, Description %>"></asp:Label>
                                    </th>
                                    <th class="btn-teal"> 
                                        <asp:Label runat="server" ID="lblCourseType" Text="<%$ Resources:LocalizedResource, CourseType %>"></asp:Label>
                                    </th>
                                    <th class="btn-teal">
                                        <asp:Label runat="server" ID="lblDuration" Text="<%$ Resources:LocalizedResource, Duration %>"></asp:Label>
                                    </th>
                                    <th class="btn-teal">
                                        <asp:Label runat="server" ID="lblStatus" Text="<%$ Resources:LocalizedResource, Status %>"></asp:Label>
                                    </th>
                                    <th class="btn-teal">
                                        <asp:Label runat="server" ID="lblCoursePath" Text="<%$ Resources:LocalizedResource, CoursePath %>"></asp:Label>
                                    </th>
                                </tr>
                            </thead>
                            <tbody id="gridmcitem">
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                        <div class="mcPager container display-none">
                            <div class="js-page-numbers display-inline-flex takeLaterCourse-page">
                                <a class="pagePrev black-arrow fa fa-caret-left no-underline-hover" href="javascript:void(0);" title="Go to previous page">
                                </a>
                                <ul class="nostyle display-inline-flex black-font">
                                </ul>
                                <a class="pageNext black-arrow fa fa-caret-right no-underline-hover" href="javascript:void(0);" title="Go to next page">
                                </a>
                            </div>
                        </div>
                    </div>
                </LayoutTemplate>
                <ClientSettings>
                    <DataBinding ItemPlaceHolderID="gridmcitem">
                        <ItemTemplate>
                        <tr class="rlvI">
                            <td>
                                <p class="clamp-line-2"><a id="hlgridCompletedResult" onclick='confirmLaunchCourse("#= EncryptedCourseID #");' title=" #= CourseTitle #" class="hover-pointer"> #= CourseTitle #</a></p>
                            </td>
                            <td>
                                <p class="clamp-line-2"><asp:Label ID="Label1" runat="server"> #= CourseDescription #</asp:Label></p>
                            </td>
                            <td>
                                <asp:Label ID="Label2" runat="server"> #= CourseType #</asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="Label3" runat="server"> #= CourseDuration #</asp:Label>
                            </td>
                            <td>
                                <div class="statusContainer">
                                    <p id="status" class="bold">#= Status #</p>
                                    <input type="hidden" id="hidStatus" value="#= Status #" class="display-none"/>
                                </div>
                                <script type="text/javascript">
                                    setStatusFontColor();
                                </script>
                            </td>
                            <td>
                                <asp:Label ID="Label5" runat="server"> #= CourseCategory #/ #= CourseSubcategory #</asp:Label>
                            </td>
                        </tr>
                        </ItemTemplate>
                        <EmptyDataTemplate>
                            <p style="margin-left: 10px;">
                                No Result Found
                            </p>
                        </EmptyDataTemplate>
                        <DataService EnableCaching="true" />
                    </DataBinding>
                </ClientSettings>
            </rad:RadListView>
        </div>
    </asp:Panel>
</asp:Panel>
