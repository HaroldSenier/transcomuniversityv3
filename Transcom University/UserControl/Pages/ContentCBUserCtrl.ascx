﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ContentCBUserCtrl.ascx.cs"
    Inherits="UserControl_Pages_ContentCBUserCtrl" %>
<asp:Panel ID="pnlMainContentUC" runat="server" ClientIDMode="Static">
    <rad:RadScriptBlock ID="rcbContent" runat="server">
        <script type="text/javascript">
            Sys.Application.add_load(function () {
                //               fixNoSection();
            });


            function initializeContentAccordion() {

                if ($(".ContentAccordion").length != 0) {
                    $(".ContentAccordion")
                        .accordion({
                            header: "> div > div"
                        })
                        .sortable({
                            handle: "div",
                            stop: function (event, ui) {
                                ui.item.children("div").triggerHandler("focusout");
                                var i = 1;
                                console.log($(this).parent().attr("id"));
                                $(".ContentAccordion").find('input[name*=hfIndex]').each(function (index) {
                                    var newIndex = $(this).val(i);
                                    i++;
                                });
                            }
                        });
                }

            }

            function tabClick(el) {
                if ($('.NotSection').length > 0) {
                    fixNoSection();
                }
                //$(el).removeClass("fa-sort-up").addClass("fa-sort-down");
                var panelId = $(el).parent().find("a").attr("href");

                console.log(panelId);
                //                $(panelId).addClass("display-none");
                $(el).parent().find("i").removeClass("fa-sort-up").addClass("fa-sort-down");
                if (!$(panelId).hasClass("in")) {
                    //console.log(el);

                    $(el).parent().find("i").removeClass("fa-sort-down").addClass("fa-sort-up");

                    // $(panelId).removeClass("display-none");
                } else {

                }
            }

            function fixNoSection() {
                $('.NotSection').each(function (i, obj) {
                    var x = $(this).find(".panel-body").find("#ContentAccordion").prependTo($(this).parent().parent());
                    $(this).parent().remove();
                    if (x.text().trim() == "This section has no item to display.")
                        x.text("");

                });


            }
        </script>
        <script type="text/javascript">
            var btnRemove;

            function showRadWindow(btn) {
                btnRemove = btn;
                var RadMessageConfirmRemove = $("#<%=hdnRadMessageConfirmRemove.ClientID%>").val();
                var RadConfirmRemove = $("#<%=hdnRadConfirmRemove.ClientID%>").val();
                radconfirm(RadMessageConfirmRemove, callBackConfirmRemoveFn, 350, 180, null, RadConfirmRemove);
            }

            function callBackConfirmRemoveFn(arg) {
                if (arg) {
                    var loading = $find("<%= RadAjaxLoadingPanel1.ClientID %>");

                    loading.show("pnlCourseOrderTrue");
                    $(btnRemove).parent().find(".JSbtnDelSCOTrue").click();
                }
                else {
                    return false;
                }
            }

            function clientRefreshResources() {
                //                var loading = $find("<%= RadAjaxLoadingPanel1.ClientID %>");
                //                loading.show("pnlCourseOrderTrue");
                console.log("refresh content");
                showPageLoading();
                deleteCourseUploaderFiles();
                $("#<%= btnRefreshResources.ClientID %>").click();
            }

            function clientRefreshResourcesAndSuccessUpload(filename) {
                debugger;
                sessionStorage.setItem('lastFileName', filename);
                console.log("refresh content");
                showPageLoading();
                deleteCourseUploaderFiles();
                $("#<%= btnRefreshResourcesUpload.ClientID %>").click();
            }

            function clientRefreshResourcesNoLoading() {
                //                var loading = $find("<%= RadAjaxLoadingPanel1.ClientID %>");
                //                loading.show("pnlCourseOrderTrue");
                console.log("refresh content");
                deleteCourseUploaderFiles();
                //showPageLoading();
                $("#<%= btnRefreshResources.ClientID %>").click();
            }

            function hidePageLoadingAndShowAlert() {
                debugger;
                hidePageLoading();
                var RadAlertMessage = $("#<%=hdnRadAlertMessage.ClientID%>").val();
                var RadAlertConfirm = $("#<%=hdnRadAlertConfirm.ClientID%>").val();
                radalert(sessionStorage.getItem('lastFileName') + RadAlertMessage, 330, 180, RadAlertConfirm, "")
            }


        </script>
    </rad:RadScriptBlock>
    <rad:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <rad:AjaxSetting AjaxControlID="btnReorderSequence">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage2" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnCancel">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage2" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnSave">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlMainContentUC" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnRefreshResources">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlMainContentUC" LoadingPanelID="RadAjaxLoadingPanel2"/>
                </UpdatedControls>
            </rad:AjaxSetting>
           <%-- <rad:AjaxSetting AjaxControlID="UploadPackageButton">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlMainContentUC" LoadingPanelID="RadAjaxLoadingPanel2"/>
                </UpdatedControls>
            </rad:AjaxSetting>--%>
            
        </AjaxSettings>
    </rad:RadAjaxManagerProxy>
    <%--  <ucUC:UploadContent ID="PreviewScormCourse" ClientIDMode="Static" runat="server"
        Visible="false" />--%>
    <rad:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Transparency="25"
        IsSticky="true" CssClass="Loading" />
    <rad:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true" RenderMode="Lightweight"
        Skin="Bootstrap" VisibleStatusbar="false">
    </rad:RadWindowManager>
    <asp:HiddenField runat="server" ID="hdnRadMessageConfirmRemove" Value="<%$ Resources:LocalizedResource, AreYouSureYouWantToRemoveThisItem %>" />
    <asp:HiddenField runat="server" ID="hdnRadConfirmRemove" Value="<%$ Resources:LocalizedResource, AreYouSureYouWantToRemoveThisItem %>" />
    <asp:HiddenField runat="server" ID="hdnRadAlertMessage" Value="<%$ Resources:LocalizedResource, WasSuccessfullyUploaded %>" />
    <asp:HiddenField runat="server" ID="hdnRadAlertConfirm" Value="<%$ Resources:LocalizedResource, UploadedSuccessfuly %>" />
    <asp:Panel ID="pnlCourseOrderTrue" runat="server" ClientIDMode="Static">
        <div class="col-md-12" style="width: 98%">
            <div id="divCourseReorderTrueLv" class="demo-container">
                <%--OnItemDataBound="lvCourseDefault_Learner_OnDataBinding"--%>
                <asp:Panel ID="pnlLearner" runat="server" CssClass="panel panel-default">
                    <div class="panel-heading panel-heading-black display-flex">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" class="js-collapse-event no-underline-hover" data-parent="#CourseAccordion"
                                href="#learnerTab" onclick="tabClick(this);"><asp:Label runat="server" ID="lblLearnerResources" Text="<%$ Resources:LocalizedResource, LearnerResources %>"></asp:Label></a> <i class="fa fa-sort-down pull-right hover-pointer js-collapse-event"
                                    data-toggle="collapse" data-parent="#CourseAccordion" href="#learnerTab" onclick="tabClick(this);">
                                </i>
                        </h4>
                    </div>
                    <div id="learnerTab" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="col-md-14" style="margin-left: 1%; margin-right: 1%">
                                <div class="col-md-12">
                                    <asp:Panel ID="pnlLearnerResourceLv" runat="server">
                                        <div id="divLearnerResourceLv" class="demo-container col-md-14">
                                            <rad:RadListView ID="lvCourseDefault_Learner" runat="server" ItemPlaceholderID="SectionHolder"
                                                AllowPaging="false" OnItemDataBound="lvCourseDefault_Learner_SectionDataBound"
                                                OnNeedDataSource="lvCourseDefault_Learner_NeedDataSource" DataKeyNames="SectionID">
                                                <LayoutTemplate>
                                                    <div id="SectionHolder" runat="server">
                                                    </div>
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <asp:Panel ID="pnlSection" runat="server" CssClass="panel panel-default">
                                                        <div class="panel-heading panel-heading-black display-flex">
                                                            <h4 class="panel-title">
                                                                <a id="A1" data-toggle="collapse" class="js-collapse-event no-underline-hover" data-parent="#CourseAccordion"
                                                                    href='<%# String.Format("#_{0}", Eval("SectionID")) %>' onclick="tabClick(this);">
                                                                    <%# Eval("SectionName") %>
                                                                </a><i id="I10" class="fa fa-sort-down pull-right hover-pointer js-collapse-event"
                                                                    runat="server" data-toggle="collapse" data-parent="#CourseAccordion" href='<%#String.Format("#_{0}", Eval("SectionID")) %>'
                                                                    onclick="tabClick(this);"></i>
                                                            </h4>
                                                        </div>
                                                        <div id='<%#String.Format("_{0}", Eval("SectionID")) %>' class='panel-collapse collapse <%# Eval("SectionName").ToString().Replace(" ", "") %>'>
                                                            <div class="panel-body">
                                                                <div id="ContentAccordion">
                                                                    <rad:RadListView ID="lvCourseDefault_Learner_Item" runat="server" ItemPlaceholderID="ItemHolder"
                                                                        AllowPaging="false" DataKeyNames="dripEnable" OnItemDataBound="lvCourseDefault_Learner_Item_OnItemDataBound">
                                                                        <LayoutTemplate>
                                                                            <div id="ItemHolder" runat="server">
                                                                            </div>
                                                                        </LayoutTemplate>
                                                                        <ItemTemplate>
                                                                            <div class="group">
                                                                                <div class="row panel-gray panel-course-category">
                                                                                    <div class="col-xs-11" title='<%#Eval("ScoTitle")%>'>
                                                                                        <i id="I1" runat="server" class="fa fa-file pull-left" visible='<%# isFile(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I2" runat="server" class="fa fa-file-pdf-o pull-left" visible='<%# isPdf(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I3" runat="server" class="fa fa-file-word-o pull-left" visible='<%# isWord(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I4" runat="server" class="fa fa-file-excel-o pull-left" visible='<%# isExcel(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I5" runat="server" class="fa fa-file-powerpoint-o pull-left" visible='<%# isPpt(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I6" runat="server" class="fa fa-file-image-o pull-left" visible='<%# isImg(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I7" runat="server" class="fa fa-file-archive-o pull-left" visible='<%# isZip(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I8" runat="server" class="fa fa-file-audio-o pull-left" visible='<%# isMp3(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I9" runat="server" class="fa fa-file-video-o pull-left" visible='<%# isMp4(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i>
                                                                                        <asp:Label ID="lblTitle" runat="server" CssClass="course-title">
                                                                                            <%# Eval("ScoTitle")%>
                                                                                        </asp:Label>
                                                                                        <asp:HiddenField ID="hfIndex" runat="server" Value='<%#Eval("CourseOrder")%>' />
                                                                                        <asp:HiddenField ID="hfScoID" runat="server" Value='<%#Eval("ScoID")%>' />
                                                                                    </div>
                                                                                    <div class="col-xs-1" title="Remove SCO">
                                                                                        <asp:LinkButton ID="btnDelSCOFake" runat="server" Font-Underline="false" OnClientClick="showRadWindow(this);">
                                                                                            <i class="fa fa-window-close-o"></i>
                                                                                        </asp:LinkButton>
                                                                                        <asp:Button ID="btnDelSCOTrue" runat="server" CommandArgument='<%# Convert.ToInt32(Eval("TestCategoryID")) == 0 ?  Eval("ScoID") : Eval("TestCategoryID") %>'
                                                                                            CommandName='<%# Convert.ToInt32(Eval("TestCategoryID")) == 0 ? "Resource" : "Assessment" %>'
                                                                                            OnClick="btnDelSCOTrue_Click" CssClass='display-none JSbtnDelSCOTrue ' />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                        <EmptyDataTemplate>
                                                                            This section has no item to display.
                                                                        </EmptyDataTemplate>
                                                                    </rad:RadListView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </asp:Panel>
                                                </ItemTemplate>
                                                <EmptyDataTemplate>
                                                    No Resource to display.
                                                </EmptyDataTemplate>
                                                <EmptyItemTemplate>
                                                    No Resource to display.
                                                </EmptyItemTemplate>
                                            </rad:RadListView>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlAdditional" runat="server" CssClass="panel panel-default">
                    <div class="panel-heading panel-heading-black display-flex">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" class="js-collapse-event no-underline-hover" data-parent="#CourseAccordion"
                                href="#additionalTab" onclick="tabClick(this);"><asp:Label runat="server" ID="lblAdditionalResources" Text="<%$ Resources:LocalizedResource, AdditionalResources %>"></asp:Label></a> <i class="fa fa-sort-down pull-right hover-pointer js-collapse-event"
                                    data-toggle="collapse" data-parent="#CourseAccordion" href="#additionalTab" onclick="tabClick(this);">
                                </i>
                        </h4>
                    </div>
                    <div id="additionalTab" class="panel-collapse collapse ">
                        <div class="panel-body">
                            <div class="col-md-14" style="margin-left: 1%; margin-right: 1%">
                                <div class="col-md-12">
                                    <asp:Panel ID="Panel2" runat="server">
                                        <div id="div2" class="demo-container col-md-14">
                                            <rad:RadListView ID="lvCourseDefault_Additional" runat="server" ItemPlaceholderID="SectionHolder"
                                                AllowPaging="false" OnItemDataBound="lvCourseDefault_Additional_SectionDataBound"
                                                OnNeedDataSource="lvCourseDefault_Additional_NeedDataSource" DataKeyNames="SectionID">
                                                <LayoutTemplate>
                                                    <div id="SectionHolder" runat="server">
                                                    </div>
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <asp:Panel ID="pnlSection" runat="server" CssClass="panel panel-default">
                                                        <div class="panel-heading panel-heading-black display-flex">
                                                            <h4 class="panel-title">
                                                                <a id="A1" data-toggle="collapse" class="js-collapse-event no-underline-hover" data-parent="#CourseAccordion"
                                                                    href='<%# String.Format("#_{0}", Eval("SectionID")) %>' onclick="tabClick(this);">
                                                                    <%# Eval("SectionName") %>
                                                                </a><i id="I10" class="fa fa-sort-down pull-right hover-pointer js-collapse-event"
                                                                    runat="server" data-toggle="collapse" data-parent="#CourseAccordion" href='<%#String.Format("#_{0}", Eval("SectionID")) %>'
                                                                    onclick="tabClick(this);"></i>
                                                            </h4>
                                                        </div>
                                                        <div id='<%#String.Format("_{0}", Eval("SectionID")) %>' class='panel-collapse collapse <%# Eval("SectionName").ToString().Replace(" ", "") %>'>
                                                            <div class="panel-body">
                                                                <div id="ContentAccordion">
                                                                    <rad:RadListView ID="lvCourseDefault_Additional_Item" runat="server" ItemPlaceholderID="ItemHolder"
                                                                        AllowPaging="false" DataKeyNames="dripEnable" OnItemDataBound="lvCourseDefault_Additional_Item_OnItemDataBound">
                                                                        <LayoutTemplate>
                                                                            <div id="ItemHolder" runat="server">
                                                                            </div>
                                                                        </LayoutTemplate>
                                                                        <ItemTemplate>
                                                                            <div class="group">
                                                                                <div class="row panel-gray panel-course-category">
                                                                                    <div class="col-xs-11" title='<%#Eval("ScoTitle")%>'>
                                                                                        <i id="I1" runat="server" class="fa fa-file pull-left" visible='<%# isFile(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I2" runat="server" class="fa fa-file-pdf-o pull-left" visible='<%# isPdf(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I3" runat="server" class="fa fa-file-word-o pull-left" visible='<%# isWord(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I4" runat="server" class="fa fa-file-excel-o pull-left" visible='<%# isExcel(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I5" runat="server" class="fa fa-file-powerpoint-o pull-left" visible='<%# isPpt(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I6" runat="server" class="fa fa-file-image-o pull-left" visible='<%# isImg(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I7" runat="server" class="fa fa-file-archive-o pull-left" visible='<%# isZip(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I8" runat="server" class="fa fa-file-audio-o pull-left" visible='<%# isMp3(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I9" runat="server" class="fa fa-file-video-o pull-left" visible='<%# isMp4(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i>
                                                                                        <asp:Label ID="lblTitle" runat="server" CssClass="course-title">
                                                                                            <%#Eval("ScoTitle")%>
                                                                                        </asp:Label>
                                                                                        <asp:HiddenField ID="hfIndex" runat="server" Value='<%#Eval("CourseOrder")%>' />
                                                                                        <asp:HiddenField ID="hfScoID" runat="server" Value='<%#Eval("ScoID")%>' />
                                                                                    </div>
                                                                                    <div class="col-xs-1" title="Remove SCO">
                                                                                        <asp:LinkButton ID="btnDelSCOFake" runat="server" Font-Underline="false" OnClientClick="showRadWindow(this);">
                                                                                        <i class="fa fa-window-close-o"></i>
                                                                                        </asp:LinkButton>
                                                                                        <asp:Button ID="btnDelSCOTrue" runat="server" CommandArgument='<%# Convert.ToInt32(Eval("TestCategoryID")) == 0 ?  Eval("ScoID") : Eval("TestCategoryID") %>'
                                                                                            CommandName='<%# Convert.ToInt32(Eval("TestCategoryID")) == 0 ? "Resource" : "Assessment" %>'
                                                                                            OnClick="btnDelSCOTrue_Click" CssClass="display-none JSbtnDelSCOTrue"></asp:Button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                        <EmptyDataTemplate>
                                                                            This section has no item to display.
                                                                        </EmptyDataTemplate>
                                                                    </rad:RadListView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </asp:Panel>
                                                </ItemTemplate>
                                                <EmptyDataTemplate>
                                                    No Resource to display.
                                                </EmptyDataTemplate>
                                            </rad:RadListView>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnTrainer" runat="server" CssClass="panel panel-default">
                    <div class="panel-heading panel-heading-black display-flex">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" class="js-collapse-event no-underline-hover" data-parent="#CourseAccordion"
                                href="#trainerTab" onclick="tabClick(this);"><asp:Label runat="server" ID="lblTrainerResources" Text="<%$ Resources:LocalizedResource, TrainerResources %>"></asp:Label></a> <i class="fa fa-sort-down pull-right hover-pointer js-collapse-event"
                                    data-toggle="collapse" data-parent="#CourseAccordion" href="#trainerTab" onclick="tabClick(this);">
                                </i>
                        </h4>
                    </div>
                    <div id="trainerTab" class="panel-collapse collapse ">
                        <div class="panel-body">
                            <div class="col-md-14" style="margin-left: 1%; margin-right: 1%">
                                <div class="col-md-12">
                                    <asp:Panel ID="Panel3" runat="server">
                                        <div id="div3" class="demo-container col-md-14">
                                            <rad:RadListView ID="lvCourseDefault_Trainer" runat="server" ItemPlaceholderID="SectionHolder"
                                                AllowPaging="false" OnItemDataBound="lvCourseDefault_Trainer_SectionDataBound"
                                                OnNeedDataSource="lvCourseDefault_Trainer_NeedDataSource" DataKeyNames="SectionID">
                                                <LayoutTemplate>
                                                    <div id="SectionHolder" runat="server">
                                                    </div>
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <asp:Panel ID="pnlSection" runat="server" CssClass="panel panel-default">
                                                        <div class="panel-heading panel-heading-black display-flex">
                                                            <h4 class="panel-title">
                                                                <a id="A1" data-toggle="collapse" class="js-collapse-event no-underline-hover" data-parent="#CourseAccordion"
                                                                    href='<%# String.Format("#_{0}", Eval("SectionID")) %>' onclick="tabClick(this);">
                                                                    <%# Eval("SectionName") %>
                                                                </a><i id="I10" class="fa fa-sort-down pull-right hover-pointer js-collapse-event"
                                                                    runat="server" data-toggle="collapse" data-parent="#CourseAccordion" href='<%#String.Format("#_{0}", Eval("SectionID")) %>'
                                                                    onclick="tabClick(this);"></i>
                                                            </h4>
                                                        </div>
                                                        <div id='<%#String.Format("_{0}", Eval("SectionID")) %>' class='panel-collapse collapse <%# Eval("SectionName").ToString().Replace(" ", "") %>'>
                                                            <div class="panel-body">
                                                                <div id="ContentAccordion">
                                                                    <rad:RadListView ID="lvCourseDefault_Trainer_Item" runat="server" ItemPlaceholderID="ItemHolder"
                                                                        AllowPaging="false" DataKeyNames="dripEnable" OnItemDataBound="lvCourseDefault_Trainer_Item_OnItemDataBound">
                                                                        <LayoutTemplate>
                                                                            <div id="ItemHolder" runat="server">
                                                                            </div>
                                                                        </LayoutTemplate>
                                                                        <ItemTemplate>
                                                                            <div class="group">
                                                                                <div class="row panel-gray panel-course-category">
                                                                                    <div class="col-xs-11" title='<%#Eval("ScoTitle")%>'>
                                                                                        <i id="I1" runat="server" class="fa fa-file pull-left" visible='<%# isFile(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I2" runat="server" class="fa fa-file-pdf-o pull-left" visible='<%# isPdf(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I3" runat="server" class="fa fa-file-word-o pull-left" visible='<%# isWord(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I4" runat="server" class="fa fa-file-excel-o pull-left" visible='<%# isExcel(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I5" runat="server" class="fa fa-file-powerpoint-o pull-left" visible='<%# isPpt(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I6" runat="server" class="fa fa-file-image-o pull-left" visible='<%# isImg(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I7" runat="server" class="fa fa-file-archive-o pull-left" visible='<%# isZip(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I8" runat="server" class="fa fa-file-audio-o pull-left" visible='<%# isMp3(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I9" runat="server" class="fa fa-file-video-o pull-left" visible='<%# isMp4(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i>
                                                                                        <asp:Label ID="lblTitle" runat="server" CssClass="course-title">
                                                                                            <%#Eval("ScoTitle")%>
                                                                                        </asp:Label>
                                                                                        <asp:HiddenField ID="hfIndex" runat="server" Value='<%#Eval("CourseOrder")%>' />
                                                                                        <asp:HiddenField ID="hfScoID" runat="server" Value='<%#Eval("ScoID")%>' />
                                                                                    </div>
                                                                                    <div class="col-xs-1" title="Remove SCO">
                                                                                        <asp:LinkButton ID="btnDelSCOFake" runat="server" Font-Underline="false" OnClientClick="showRadWindow(this);">
                                                                                        <i class="fa fa-window-close-o"></i>
                                                                                        </asp:LinkButton>
                                                                                        <asp:Button ID="btnDelSCOTrue" runat="server" CommandArgument='<%# Convert.ToInt32(Eval("TestCategoryID")) == 0 ?  Eval("ScoID") : Eval("TestCategoryID") %>'
                                                                                            CommandName='<%# Convert.ToInt32(Eval("TestCategoryID")) == 0 ? "Resource" : "Assessment" %>'
                                                                                            OnClick="btnDelSCOTrue_Click" CssClass="display-none JSbtnDelSCOTrue"></asp:Button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </rad:RadListView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </asp:Panel>
                                                </ItemTemplate>
                                                <EmptyDataTemplate>
                                                    No Resource to display.
                                                </EmptyDataTemplate>
                                            </rad:RadListView>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <br />
                <div style="margin-left: 40%;">
                    <asp:Button ID="btnReorderSequence" runat="server" Text="<%$ Resources:LocalizedResource, ReorderSequence %>" ToolTip="Re-Order Sequence"
                        OnClick="btnReorderSequence_Click" CssClass="btn btn-md btn-teal btn-flat" />
                </div>
            </div>
            <br />
            <br />
            <br />
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlCourseOrderFalse" runat="server" Visible="false">
        <div class="col-md-12" style="width: 98%" id="CourseAccordion">
            <div id="divCourseReorderFalseLv" class="demo-container">
                <asp:Panel ID="pnlLearner_isOrder" runat="server" CssClass="panel panel-default">
                    <div class="panel-heading panel-heading-black display-flex">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" class="js-collapse-event no-underline-hover" data-parent="#CourseAccordion"
                                href="#learnerTab_isOrder" onclick="tabClick(this);"><asp:Label runat="server" ID="lblLearnerResourcesTab" Text="<%$ Resources:LocalizedResource, LearnerResources %>"></asp:Label></a> <i class="fa fa-sort-down pull-right hover-pointer js-collapse-event"
                                    data-toggle="collapse" data-parent="#CourseAccordion" href="#learnerTab_isOrder"
                                    onclick="tabClick(this);"></i>
                        </h4>
                    </div>
                    <div id="learnerTab_isOrder" class="panel-collapse collapse ">
                        <div class="panel-body">
                            <div class="col-md-14" style="margin-left: 1%; margin-right: 1%">
                                <div class="col-md-12">
                                    <asp:Panel ID="pnlLearner_isOrder_lv" runat="server">
                                        <div id="divLearner_isOrder_lv" class="demo-container col-md-14">
                                            <rad:RadListView ID="lvCourseOrder_Learner" runat="server" ItemPlaceholderID="SectionHolder"
                                                AllowPaging="false" OnItemDataBound="lvCourseOrder_Learner_SectionDataBound"
                                                OnNeedDataSource="lvCourseOrder_Learner_NeedDataSource" DataKeyNames="SectionID">
                                                <LayoutTemplate>
                                                    <div id="SectionHolder" runat="server">
                                                    </div>
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <asp:Panel ID="pnlSection" runat="server" CssClass="panel panel-default">
                                                        <div class="panel-heading panel-heading-black display-flex">
                                                            <h4 class="panel-title">
                                                                <a id="A1" data-toggle="collapse" class="js-collapse-event no-underline-hover" data-parent="#CourseAccordion"
                                                                    href='<%# String.Format("#_{0}", Eval("SectionID")) %>' onclick="tabClick(this);">
                                                                    <%# Eval("SectionName") %>
                                                                </a><i id="I10" class="fa fa-sort-down pull-right hover-pointer js-collapse-event"
                                                                    runat="server" data-toggle="collapse" data-parent="#CourseAccordion" href='<%#String.Format("#_{0}", Eval("SectionID")) %>'
                                                                    onclick="tabClick(this);"></i>
                                                            </h4>
                                                        </div>
                                                        <div id='<%#String.Format("_{0}", Eval("SectionID")) %>' class='panel-collapse collapse <%# Eval("SectionName").ToString().Replace(" ", "") %>'>
                                                            <div class="panel-body">
                                                                <div id="ContentAccordion" class="ContentAccordion">
                                                                    <rad:RadListView ID="lvCourseOrder_Learner_Item" runat="server" ItemPlaceholderID="ItemHolder"
                                                                        AllowPaging="false" DataKeyNames="dripEnable" OnItemDataBound="lvCourseOrder_Learner_Item_OnItemDataBound">
                                                                        <LayoutTemplate>
                                                                            <div id="ItemHolder" runat="server">
                                                                            </div>
                                                                        </LayoutTemplate>
                                                                        <ItemTemplate>
                                                                            <div class="group pointer">
                                                                                <div class="row panel-gray panel-course-category">
                                                                                    <div class="col-xs-11" title='<%#Eval("ScoTitle")%>'>
                                                                                        <i id="I1" runat="server" class="fa fa-file pull-left" visible='<%# isFile(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I2" runat="server" class="fa fa-file-pdf-o pull-left" visible='<%# isPdf(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I3" runat="server" class="fa fa-file-word-o pull-left" visible='<%# isWord(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I4" runat="server" class="fa fa-file-excel-o pull-left" visible='<%# isExcel(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I5" runat="server" class="fa fa-file-powerpoint-o pull-left" visible='<%# isPpt(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I6" runat="server" class="fa fa-file-image-o pull-left" visible='<%# isImg(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I7" runat="server" class="fa fa-file-archive-o pull-left" visible='<%# isZip(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I8" runat="server" class="fa fa-file-audio-o pull-left" visible='<%# isMp3(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I9" runat="server" class="fa fa-file-video-o pull-left" visible='<%# isMp4(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i>
                                                                                        <asp:Label ID="lblTitle" runat="server" CssClass="course-title">
                                                                                        <%#Eval("ScoTitle")%>
                                                                                        </asp:Label>
                                                                                        <asp:HiddenField ID="hfIndex" runat="server" Value='<%#Eval("CourseOrder")%>' />
                                                                                        <asp:HiddenField ID="hfIsAssessment" runat="server" Value='<%# Convert.ToInt32(Eval("TestCategoryID")) == 0 ? "0" : "1" %>' />
                                                                                        <asp:HiddenField ID="hfScoID" runat="server" Value='<%# Convert.ToInt32(Eval("TestCategoryID")) == 0 ? Eval("ScoID") : Eval("TestCategoryID") %>' />
                                                                                    </div>
                                                                                    <div class="col-xs-1" title="Remove SCO">
                                                                                        <asp:LinkButton ID="btnDelSCOFake" runat="server" Font-Underline="false" OnClientClick="showRadWindow(this);"
                                                                                            CssClass="display-none">
                                                                                        <i class="fa fa-window-close-o"></i>
                                                                                        </asp:LinkButton>
                                                                                        <asp:Button ID="btnDelSCOTrue" runat="server" CommandArgument='<%# Convert.ToInt32(Eval("TestCategoryID")) == 0 ?  Eval("ScoID") : Eval("TestCategoryID") %>'
                                                                                            CommandName='<%# Convert.ToInt32(Eval("TestCategoryID")) == 0 ? "Resource" : "Assessment" %>'
                                                                                            OnClick="btnDelSCOTrue_Click" CssClass="display-none JSbtnDelSCOTrue"></asp:Button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                        <EmptyDataTemplate>
                                                                            This section has no item to display.
                                                                        </EmptyDataTemplate>
                                                                    </rad:RadListView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </asp:Panel>
                                                </ItemTemplate>
                                                <EmptyDataTemplate>
                                                    No Resource to display.
                                                </EmptyDataTemplate>
                                                <EmptyItemTemplate>
                                                    No Resource to display.
                                                </EmptyItemTemplate>
                                            </rad:RadListView>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlAdditional_isOrder" runat="server" CssClass="panel panel-default">
                    <div class="panel-heading panel-heading-black display-flex">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" class="js-collapse-event no-underline-hover" data-parent="#CourseAccordion"
                                href="#additionalTab_isOrder" onclick="tabClick(this);"><asp:Label runat="server" ID="lblAdditionalResourcesTab" Text="<%$ Resources:LocalizedResource, AdditionalResources %>"></asp:Label></a>
                            <i class="fa fa-sort-down pull-right hover-pointer js-collapse-event" data-toggle="collapse"
                                data-parent="#CourseAccordion" href="#additionalTab_isOrder" onclick="tabClick(this);">
                            </i>
                        </h4>
                    </div>
                    <div id="additionalTab_isOrder" class="panel-collapse collapse ">
                        <div class="panel-body">
                            <div class="col-md-14" style="margin-left: 1%; margin-right: 1%">
                                <div class="col-md-12">
                                    <asp:Panel ID="Panel6" runat="server">
                                        <div id="div6" class="demo-container col-md-14">
                                            <rad:RadListView ID="lvCourseOrder_Additional" runat="server" ItemPlaceholderID="SectionHolder"
                                                AllowPaging="false" OnItemDataBound="lvCourseOrder_Additional_SectionDataBound"
                                                OnNeedDataSource="lvCourseOrder_Additional_NeedDataSource" DataKeyNames="SectionID">
                                                <LayoutTemplate>
                                                    <div id="SectionHolder" runat="server">
                                                    </div>
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <asp:Panel ID="pnlSection" runat="server" CssClass="panel panel-default">
                                                        <div class="panel-heading panel-heading-black display-flex">
                                                            <h4 class="panel-title">
                                                                <a id="A1" data-toggle="collapse" class="js-collapse-event no-underline-hover" data-parent="#CourseAccordion"
                                                                    href='<%# String.Format("#_{0}", Eval("SectionID")) %>' onclick="tabClick(this);">
                                                                    <%# Eval("SectionName") %>
                                                                </a><i id="I10" class="fa fa-sort-down pull-right hover-pointer js-collapse-event"
                                                                    runat="server" data-toggle="collapse" data-parent="#CourseAccordion" href='<%#String.Format("#_{0}", Eval("SectionID")) %>'
                                                                    onclick="tabClick(this);"></i>
                                                            </h4>
                                                        </div>
                                                        <div id='<%#String.Format("_{0}", Eval("SectionID")) %>' class='panel-collapse collapse <%# Eval("SectionName").ToString().Replace(" ", "") %>'>
                                                            <div class="panel-body">
                                                                <div id="ContentAccordion" class="ContentAccordion">
                                                                    <rad:RadListView ID="lvCourseOrder_Additional_Item" runat="server" ItemPlaceholderID="ItemHolder"
                                                                        AllowPaging="false" DataKeyNames="dripEnable" OnItemDataBound="lvCourseOrder_Additional_Item_OnItemDataBound">
                                                                        <LayoutTemplate>
                                                                            <div id="ItemHolder" runat="server">
                                                                            </div>
                                                                        </LayoutTemplate>
                                                                        <ItemTemplate>
                                                                            <div class="group pointer">
                                                                                <div class="row panel-gray panel-course-category">
                                                                                    <div class="col-xs-11" title='<%#Eval("ScoTitle")%>'>
                                                                                        <i id="I1" runat="server" class="fa fa-file pull-left" visible='<%# isFile(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I2" runat="server" class="fa fa-file-pdf-o pull-left" visible='<%# isPdf(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I3" runat="server" class="fa fa-file-word-o pull-left" visible='<%# isWord(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I4" runat="server" class="fa fa-file-excel-o pull-left" visible='<%# isExcel(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I5" runat="server" class="fa fa-file-powerpoint-o pull-left" visible='<%# isPpt(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I6" runat="server" class="fa fa-file-image-o pull-left" visible='<%# isImg(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I7" runat="server" class="fa fa-file-archive-o pull-left" visible='<%# isZip(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I8" runat="server" class="fa fa-file-audio-o pull-left" visible='<%# isMp3(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I9" runat="server" class="fa fa-file-video-o pull-left" visible='<%# isMp4(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i>
                                                                                        <asp:Label ID="lblTitle" runat="server" CssClass="course-title">
                                                                                        <%#Eval("ScoTitle")%>
                                                                                        </asp:Label>
                                                                                        <asp:HiddenField ID="hfIndex" runat="server" Value='<%#Eval("CourseOrder")%>' />
                                                                                        <asp:HiddenField ID="hfIsAssessment" runat="server" Value='<%#Eval("TestCategoryID")%>' />
                                                                                        <asp:HiddenField ID="hfScoID" runat="server" Value='<%#Eval("ScoID")%>' />
                                                                                    </div>
                                                                                    <div class="col-xs-1" title="Remove SCO">
                                                                                        <asp:LinkButton ID="btnDelSCOFake" runat="server" Font-Underline="false" OnClientClick="showRadWindow(this);"
                                                                                            CssClass="display-none">
                                                                                        <i class="fa fa-window-close-o"></i>
                                                                                        </asp:LinkButton>
                                                                                        <asp:Button ID="btnDelSCOTrue" runat="server" CommandArgument='<%# Convert.ToInt32(Eval("TestCategoryID")) == 0 ?  Eval("ScoID") : Eval("TestCategoryID") %>'
                                                                                            CommandName='<%# Convert.ToInt32(Eval("TestCategoryID")) == 0 ? "Resource" : "Assessment" %>'
                                                                                            OnClick="btnDelSCOTrue_Click" CssClass="display-none JSbtnDelSCOTrue"></asp:Button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                        <EmptyDataTemplate>
                                                                            This section has no item to display.
                                                                        </EmptyDataTemplate>
                                                                    </rad:RadListView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </asp:Panel>
                                                </ItemTemplate>
                                                <EmptyDataTemplate>
                                                    No Resource to display.
                                                </EmptyDataTemplate>
                                            </rad:RadListView>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlTrainerTab_isOrder" runat="server" CssClass="panel panel-default">
                    <div class="panel-heading panel-heading-black display-flex">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" class="js-collapse-event no-underline-hover" data-parent="#CourseAccordion"
                                href="#trainerTab_isOrder" onclick="tabClick(this);"><asp:Label runat="server" ID="lblTrainerResourcesTab" Text="<%$ Resources:LocalizedResource, TrainerResources %>"></asp:Label></a> <i class="fa fa-sort-down pull-right hover-pointer js-collapse-event"
                                    data-toggle="collapse" data-parent="#CourseAccordion" href="#trainerTab_isOrder"
                                    onclick="tabClick(this);"></i>
                        </h4>
                    </div>
                    <div id="trainerTab_isOrder" class="panel-collapse collapse ">
                        <div class="panel-body">
                            <div class="col-md-14" style="margin-left: 1%; margin-right: 1%">
                                <div class="col-md-12">
                                    <asp:Panel ID="Panel8" runat="server">
                                        <div id="div8" class="demo-container col-md-14">
                                            <rad:RadListView ID="lvCourseOrder_Trainer" runat="server" ItemPlaceholderID="SectionHolder"
                                                AllowPaging="false" OnItemDataBound="lvCourseOrder_Trainer_SectionDataBound"
                                                OnNeedDataSource="lvCourseOrder_Trainer_NeedDataSource" DataKeyNames="SectionID">
                                                <LayoutTemplate>
                                                    <div id="SectionHolder" runat="server">
                                                    </div>
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <asp:Panel ID="pnlSection" runat="server" CssClass="panel panel-default">
                                                        <div class="panel-heading panel-heading-black display-flex">
                                                            <h4 class="panel-title">
                                                                <a id="A1" data-toggle="collapse" class="js-collapse-event no-underline-hover" data-parent="#CourseAccordion"
                                                                    href='<%# String.Format("#_{0}", Eval("SectionID")) %>' onclick="tabClick(this);">
                                                                    <%# Eval("SectionName") %>
                                                                </a><i id="I10" class="fa fa-sort-down pull-right hover-pointer js-collapse-event"
                                                                    runat="server" data-toggle="collapse" data-parent="#CourseAccordion" href='<%#String.Format("#_{0}", Eval("SectionID")) %>'
                                                                    onclick="tabClick(this);"></i>
                                                            </h4>
                                                        </div>
                                                        <div id='<%#String.Format("_{0}", Eval("SectionID")) %>' class='panel-collapse collapse <%# Eval("SectionName").ToString().Replace(" ", "") %>'>
                                                            <div class="panel-body">
                                                                <div id="ContentAccordion" class="ContentAccordion">
                                                                    <rad:RadListView ID="lvCourseOrder_Trainer_Item" runat="server" ItemPlaceholderID="ItemHolder"
                                                                        AllowPaging="false" DataKeyNames="dripEnable" OnItemDataBound="lvCourseOrder_Trainer_Item_OnItemDataBound">
                                                                        <LayoutTemplate>
                                                                            <div id="ItemHolder" runat="server">
                                                                            </div>
                                                                        </LayoutTemplate>
                                                                        <ItemTemplate>
                                                                            <div class="group pointer">
                                                                                <div class="row panel-gray panel-course-category">
                                                                                    <div class="col-xs-11" title='<%#Eval("ScoTitle")%>'>
                                                                                        <i id="I1" runat="server" class="fa fa-file pull-left" visible='<%# isFile(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I2" runat="server" class="fa fa-file-pdf-o pull-left" visible='<%# isPdf(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I3" runat="server" class="fa fa-file-word-o pull-left" visible='<%# isWord(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I4" runat="server" class="fa fa-file-excel-o pull-left" visible='<%# isExcel(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I5" runat="server" class="fa fa-file-powerpoint-o pull-left" visible='<%# isPpt(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I6" runat="server" class="fa fa-file-image-o pull-left" visible='<%# isImg(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I7" runat="server" class="fa fa-file-archive-o pull-left" visible='<%# isZip(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I8" runat="server" class="fa fa-file-audio-o pull-left" visible='<%# isMp3(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I9" runat="server" class="fa fa-file-video-o pull-left" visible='<%# isMp4(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i>
                                                                                        <asp:Label ID="lblTitle" runat="server" CssClass="course-title">
                                                                                        <%#Eval("ScoTitle")%>
                                                                                        </asp:Label>
                                                                                        <asp:HiddenField ID="hfIndex" runat="server" Value='<%#Eval("CourseOrder")%>' />
                                                                                        <asp:HiddenField ID="hfIsAssessment" runat="server" Value='<%# Convert.ToInt32(Eval("TestCategoryID")) == 0 ? "0" : "1" %>' />
                                                                                        <asp:HiddenField ID="hfScoID" runat="server" Value='<%# Convert.ToInt32(Eval("TestCategoryID")) == 0 ? Eval("ScoID") : Eval("TestCategoryID") %>' />
                                                                                    </div>
                                                                                    <div class="col-xs-1" title="Remove SCO">
                                                                                        <asp:LinkButton ID="btnDelSCOFake" runat="server" Font-Underline="false" OnClientClick="showRadWindow(this);"
                                                                                            CssClass="display-none">
                                                                                        <i class="fa fa-window-close-o"></i>
                                                                                        </asp:LinkButton>
                                                                                        <asp:Button ID="btnDelSCOTrue" runat="server" CommandArgument='<%# Convert.ToInt32(Eval("TestCategoryID")) == 0 ?  Eval("ScoID") : Eval("TestCategoryID") %>'
                                                                                            CommandName='<%# Convert.ToInt32(Eval("TestCategoryID")) == 0 ? "Resource" : "Assessment" %>'
                                                                                            OnClick="btnDelSCOTrue_Click" CssClass="display-none JSbtnDelSCOTrue"></asp:Button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </rad:RadListView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </asp:Panel>
                                                </ItemTemplate>
                                                <EmptyDataTemplate>
                                                    No Resource to display.
                                                </EmptyDataTemplate>
                                            </rad:RadListView>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <br />
                <div style="margin-left: 40%;">
                    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:LocalizedResource, Cancel %>" ToolTip="<%$ Resources:LocalizedResource, Cancel %>" OnClick="btnCancel_Click"
                        CssClass="btn btn-md btn-teal btn-flat" />
                    <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:LocalizedResource, SaveOnly %>" ToolTip="<%$ Resources:LocalizedResource, SaveOnly %>" OnClick="btnSave_Click"
                        CssClass="btn btn-md btn-teal btn-flat" />
                </div>
            </div>
            <br />
            <br />
            <br />
        </div>
    </asp:Panel>
</asp:Panel>
<asp:Button ID="btnRefreshResources" Text="Refresh Resources" runat="server" OnClick="btnRefreshResources_Click"
    CssClass="display-none" />
<asp:Button ID="btnRefreshResourcesUpload" Text="Refresh Resources" runat="server"
    OnClick="btnRefreshResourcesUpload_Click" CssClass="display-none" />