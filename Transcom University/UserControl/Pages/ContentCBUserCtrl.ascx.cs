﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using Telerik.Web.UI;
using TranscomUniversityV3Model;
using System.Configuration;
using NuSkill.Business;

public partial class UserControl_Pages_ContentCBUserCtrl : System.Web.UI.UserControl
{
    private DataHelper _db;
    private static SqlConnection _oconn;
    private static SqlCommand _ocmd;
    private static string itemID;
    private static int COURSEID;
    private static List<int> CURRCOURSEID;
    private static IEnumerable<TranscomUniversityV3Model.pr_Scorm_Cor_GetCurriculumPackages_Result> resources;

    public DataTable DtOverAllBundle
    {
        get
        {
            return ViewState["DtOverAllBundle"] as DataTable;
        }
        set
        {
            ViewState["DtOverAllBundle"] = value;
        }
    }

    private static void Dbconn(string connStr)
    {
        _oconn = new SqlConnection
        {
            ConnectionString = ConfigurationManager.ConnectionStrings[connStr].ConnectionString
        };
        _oconn.Open();
    }

    protected string Decrypt(string cipherText)
    {
        string EncryptionKey = "TRNSCMV32017111";
        cipherText = cipherText.Replace(" ", "+");
        byte[] cipherBytes = Convert.FromBase64String(cipherText);

        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(cipherBytes, 0, cipherBytes.Length);
                    cs.Close();
                }
                cipherText = Encoding.Unicode.GetString(ms.ToArray());
            }
        }
        return cipherText;
    }

    public void CreateOverAllPackageDataTableColumns()
    {
        DtOverAllBundle = new DataTable();
        var column = new DataColumn
        {
            DataType = Type.GetType("System.Int32"),
            ColumnName = "ScoId",
            AutoIncrement = true,
            AutoIncrementSeed = 1,
            AutoIncrementStep = 1
        };
        DtOverAllBundle.Columns.Add(column);
        DtOverAllBundle.Columns.Add("ScoPackageId", typeof(int));
        DtOverAllBundle.Columns.Add("ScoTitle", typeof(String));
        DtOverAllBundle.Columns.Add("ScoTypeId", typeof(int));
        DtOverAllBundle.Columns.Add("ScoType", typeof(String));
        DtOverAllBundle.Columns.Add("CourseTypeID", typeof(int));
        DtOverAllBundle.Columns.Add("curriculumcourseID", typeof(int));

    }

    private DataSet GetCurriculumPackages(int CurriculumID, int CurriculumCourseID)
    {
        _db = new DataHelper("DefaultConnection");
        var ds = _db.GetCurriculumPackages(CurriculumID, CurriculumCourseID);
        return ds;
    }

    public void GetDBPackages()
    {
        try
        {
            if (CURRCOURSEID == null || CURRCOURSEID.Count == 0)
            {
                CURRCOURSEID = new List<int>();
                Dbconn("DefaultConnection");
                _ocmd = new SqlCommand("pr_Scorm_Cor_GetCurriculumPackages", _oconn)
                {
                    CommandType = CommandType.StoredProcedure
                };

                _ocmd.Parameters.AddWithValue("@CurriculumID", COURSEID);
                _ocmd.Parameters.AddWithValue("@isBundle", 1);
                SqlDataReader oDataReader = _ocmd.ExecuteReader();
                if (oDataReader.Read())
                {
                    do
                    {
                        CURRCOURSEID.Add(Convert.ToInt32(oDataReader["curriculumcourseID"].ToString()));
                    } while (oDataReader.Read());
                }

                rcbContent.Visible = true;

                //GetDBPackages();
            }

            foreach (var currid in CURRCOURSEID)
            {
                var db = new TranscomUniversityV3ModelContainer();
                resources = db.pr_Scorm_Cor_GetCurriculumPackages(COURSEID, currid, 0).ToList();

                var dtLearnerResources = DataHelper.GetCourseSections(COURSEID, 1);
                var dtAdditionalResources = DataHelper.GetCourseSections(COURSEID, 5);
                var dtTrainerResources = DataHelper.GetCourseSections(COURSEID, 8);



                lvCourseDefault_Learner.DataSource = dtLearnerResources;
                lvCourseDefault_Additional.DataSource = dtAdditionalResources;
                lvCourseDefault_Trainer.DataSource = dtTrainerResources;
                lvCourseDefault_Learner.DataBind();
                lvCourseDefault_Trainer.DataBind();
                lvCourseDefault_Additional.DataBind();

            }

            LoadDripDDL();
            if (resources == null)
                btnReorderSequence.Visible = false;
            else if (resources.Count() == 0)
                btnReorderSequence.Visible = false;
            else
                btnReorderSequence.Visible = true;
        }
        catch (Exception ex)
        {
            RadWindowManager1.RadAlert("6" + ex.Message, 330, 180, Resources.LocalizedResource.ErrorMessage.ToString(), "");
        }
        finally
        {
            
            RadScriptManager.RegisterStartupScript(Page, Page.GetType(), "hidePageLoadingContent", Utils.callClientScript("hidePageLoading"), true);
        }

    }

    public void GetDBPackagesWithAlert()
    {
        try
        {
            if (CURRCOURSEID == null || CURRCOURSEID.Count == 0)
            {
                CURRCOURSEID = new List<int>();
                Dbconn("DefaultConnection");
                _ocmd = new SqlCommand("pr_Scorm_Cor_GetCurriculumPackages", _oconn)
                {
                    CommandType = CommandType.StoredProcedure
                };

                _ocmd.Parameters.AddWithValue("@CurriculumID", COURSEID);
                _ocmd.Parameters.AddWithValue("@isBundle", 1);
                SqlDataReader oDataReader = _ocmd.ExecuteReader();
                if (oDataReader.Read())
                {
                    do
                    {
                        CURRCOURSEID.Add(Convert.ToInt32(oDataReader["curriculumcourseID"].ToString()));
                    } while (oDataReader.Read());
                }

                rcbContent.Visible = true;

                //GetDBPackages();
            }

            foreach (var currid in CURRCOURSEID)
            {
                var db = new TranscomUniversityV3ModelContainer();
                resources = db.pr_Scorm_Cor_GetCurriculumPackages(COURSEID, currid, 0).ToList();

                var dtLearnerResources = DataHelper.GetCourseSections(COURSEID, 1);
                var dtAdditionalResources = DataHelper.GetCourseSections(COURSEID, 5);
                var dtTrainerResources = DataHelper.GetCourseSections(COURSEID, 8);



                lvCourseDefault_Learner.DataSource = dtLearnerResources;
                lvCourseDefault_Additional.DataSource = dtAdditionalResources;
                lvCourseDefault_Trainer.DataSource = dtTrainerResources;
                lvCourseDefault_Learner.DataBind();
                lvCourseDefault_Trainer.DataBind();
                lvCourseDefault_Additional.DataBind();

            }

            LoadDripDDL();
            if (resources == null)
                btnReorderSequence.Visible = false;
            else if (resources.Count() == 0)
                btnReorderSequence.Visible = false;
            else
                btnReorderSequence.Visible = true;
        }
        catch (Exception ex)
        {
            RadWindowManager1.RadAlert("7" + ex.Message, 330, 180, Resources.LocalizedResource.ErrorMessage.ToString(), "");
        }
        finally
        {
            RadScriptManager.RegisterStartupScript(Page, Page.GetType(), "hidePageLoadingAndShowAlert", Utils.callClientScript("hidePageLoadingAndShowAlert"), true);
        }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        RadTabStrip RadTabStrip2 = (RadTabStrip)this.Parent.FindControl("RadTabStrip2");
        if (RadTabStrip2.SelectedIndex == 0)
        {
            if (Request.QueryString["CourseID"] != null)
                COURSEID = Convert.ToInt32(Decrypt(Request.QueryString["CourseID"]));
            else
                Response.Redirect("CourseCatalog.aspx");

            if (!Page.IsPostBack)
            {
                resources = null;
                CURRCOURSEID = new List<int>();

            }
            GetDBPackages();
        }



    }

    protected void btnReorderSequence_Click(object sender, EventArgs e)
    {
        pnlCourseOrderFalse.Visible = true;
        pnlCourseOrderTrue.Visible = false;
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        pnlCourseOrderTrue.Visible = true;
        pnlCourseOrderFalse.Visible = false;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        var courseId = Convert.ToInt32(Decrypt(Request.QueryString["CourseID"]));

        try
        {
            foreach (RadListViewItem section in lvCourseOrder_Learner.Items)
            {
                RadListView lvSection = section.FindControl("lvCourseOrder_Learner_Item") as RadListView;
                foreach (RadListViewItem item in lvSection.Items)
                {
                    HiddenField hfIndex = item.FindControl("hfIndex") as HiddenField;
                    HiddenField hfScoID = item.FindControl("hfScoID") as HiddenField;
                    HiddenField hfIsAssessment = item.FindControl("hfIsAssessment") as HiddenField;

                    int newCourseOrder = Convert.ToInt32(hfIndex.Value);
                    int _hfScoID = Convert.ToInt32(hfScoID.Value);
                    int isAssessment = Convert.ToInt32(hfIsAssessment.Value);

                    if (isAssessment > 0)
                    {
                        var db = new TranscomUniversityV3ModelContainer();
                        db.pr_TranscomUniversity_CourseOrder(newCourseOrder, courseId, _hfScoID, true);
                    }
                    else
                    {
                        var db = new TranscomUniversityV3ModelContainer();
                        db.pr_TranscomUniversity_CourseOrder(newCourseOrder, courseId, _hfScoID, false);
                    }
                }
            }
            foreach (RadListViewItem section in lvCourseOrder_Additional.Items)
            {
                RadListView lvSection = section.FindControl("lvCourseOrder_Additional_Item") as RadListView;
                foreach (RadListViewItem item in lvSection.Items)
                {
                    HiddenField hfIndex = item.FindControl("hfIndex") as HiddenField;
                    HiddenField hfScoID = item.FindControl("hfScoID") as HiddenField;
                    HiddenField hfIsAssessment = item.FindControl("hfIsAssessment") as HiddenField;

                    int newCourseOrder = Convert.ToInt32(hfIndex.Value);
                    int _hfScoID = Convert.ToInt32(hfScoID.Value);
                    int isAssessment = Convert.ToInt32(hfIsAssessment.Value);

                    if (isAssessment > 0)
                    {
                        var db = new TranscomUniversityV3ModelContainer();
                        db.pr_TranscomUniversity_CourseOrder(newCourseOrder, courseId, isAssessment, true);
                    }
                    else
                    {
                        var db = new TranscomUniversityV3ModelContainer();
                        db.pr_TranscomUniversity_CourseOrder(newCourseOrder, courseId, _hfScoID, false);
                    }
                }
            }
            foreach (RadListViewItem section in lvCourseOrder_Trainer.Items)
            {
                RadListView lvSection = section.FindControl("lvCourseOrder_Trainer_Item") as RadListView;
                foreach (RadListViewItem item in lvSection.Items)
                {
                    HiddenField hfIndex = item.FindControl("hfIndex") as HiddenField;
                    HiddenField hfScoID = item.FindControl("hfScoID") as HiddenField;
                    HiddenField hfIsAssessment = item.FindControl("hfIsAssessment") as HiddenField;

                    int newCourseOrder = Convert.ToInt32(hfIndex.Value);
                    int _hfScoID = Convert.ToInt32(hfScoID.Value);
                    int isAssessment = Convert.ToInt32(hfIsAssessment.Value);

                    if (isAssessment > 0)
                    {
                        var db = new TranscomUniversityV3ModelContainer();
                        db.pr_TranscomUniversity_CourseOrder(newCourseOrder, courseId, isAssessment, true);
                    }
                    else
                    {
                        var db = new TranscomUniversityV3ModelContainer();
                        db.pr_TranscomUniversity_CourseOrder(newCourseOrder, courseId, _hfScoID, false);
                    }
                }
            }
        }
        catch (Exception)
        {

            RadWindowManager1.RadAlert(Resources.LocalizedResource.ErrorarrangingthecoursesequencePleasecontactyourSystemAdministrator.ToString(), 330, 180, Resources.LocalizedResource.ErrorMessage.ToString(), "");
        }

        GetDBPackages();

        pnlCourseOrderTrue.Visible = true;
        pnlCourseOrderFalse.Visible = false;
    }

    protected void btnDelSCOTrue_Click(object sender, EventArgs e)
    {

        Button btn = (Button)(sender);
        itemID = btn.CommandArgument;
        string cm = btn.CommandName.ToString();
        if (cm == "Assessment")
        {
            TestCategory category = TestCategory.Select(Convert.ToInt32(itemID), true);
            category.DeleteByHide();
            GetDBPackages();


            //RadListView rgExams = (this.Parent).FindControl("contentPanel").FindControl("CreateAssessmentWindow").FindControl("C").FindControl("pnlCreateAssessment").FindControl("pnlExamsContainer").FindControl("examsList").FindControl("rgExams") as RadListView;
            //rgExams.Rebind();


            RadWindowManager1.RadAlert(Resources.LocalizedResource.ResourceSuccesfullyRemove.ToString(), 330, 180, Resources.LocalizedResource.SuccessfulMessage.ToString(), "clientRefreshResourcesNoLoadingExam");

        }
        else
        {
            try
            {
                var courseId = Convert.ToInt32(Decrypt(Request.QueryString["CourseID"]));
                var db = new TranscomUniversityV3ModelContainer();

                db.pr_TranscomUniversity_DeleteSco(Convert.ToInt32(itemID));
                GetDBPackages();
                RadWindowManager1.RadAlert(Resources.LocalizedResource.ResourceSuccesfullyRemove.ToString(), 330, 180, Resources.LocalizedResource.SuccessfulMessage.ToString(), "");
            }
            catch (Exception ex)
            {

                RadWindowManager1.RadAlert(Resources.LocalizedResource.ErrorremovingbundlePleasecontactyourSystemAdministrator.ToString(), 330, 180, Resources.LocalizedResource.ErrorMessage.ToString(), "");
            }
        }

    }

    protected void btnRefreshResources_Click(object sender, EventArgs e)
    {
        try
        {
            GetDBPackages();

        }
        catch (Exception ex)
        {

            RadWindowManager1.RadAlert(Resources.LocalizedResource.SomethingwentwronginrefreshingSCOResourcesPleaserefreshthepage.ToString(), 330, 180, Resources.LocalizedResource.ErrorMessage.ToString(), "");

        }
        //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", Utils.callClientScript("hidePageLoading"), true);
    }

    protected void btnRefreshResourcesUpload_Click(object sender, EventArgs e)
    {
        try
        {
            //GetDBPackagesWithAlert();
            GetDBPackages();

        }
        catch (Exception ex)
        {
            RadWindowManager1.RadAlert(Resources.LocalizedResource.SomethingwentwronginrefreshingSCOResourcesPleaserefreshthepage.ToString(), 330, 180, Resources.LocalizedResource.ErrorMessage.ToString(), "");

        }
        RadScriptManager.RegisterStartupScript(Page, Page.GetType(), "hidePageLoadingContent", Utils.callClientScript("hidePageLoading"), true);
    }

    protected void lvCourseDefault_Learner_NeedDataSource(object sender, EventArgs e)
    {
        try
        {
            //foreach (var currid in CURRCOURSEID)
            //{
            var dtResources = DataHelper.GetCourseSections(COURSEID, 1);
            lvCourseDefault_Learner.DataSource = dtResources;
            //}

            //    if (lvCourseDefault_Learner.Items.Count == 0)
            //{
            //    btnReorderSequence.Visible = false;
            //}
        }
        catch (Exception ex)
        {
            RadWindowManager1.RadAlert("Error Loading Learner Resources" + ex.Message, 330, 180, Resources.LocalizedResource.ErrorMessage.ToString(), "");
        }
    }

    protected void lvCourseDefault_Additional_NeedDataSource(object sender, EventArgs e)
    {
        try
        {
            //foreach (var currid in CURRCOURSEID)
            //{
            var dtResources = DataHelper.GetCourseSections(COURSEID, 5);
            lvCourseDefault_Additional.DataSource = dtResources;
            //}

            //if (CURRCOURSEID.Count == 0)
            //{
            //    btnReorderSequence.Visible = false;
            //}
        }
        catch (Exception ex)
        {
            RadWindowManager1.RadAlert("Error Loading Additional Resources" + ex.Message, 330, 180, Resources.LocalizedResource.ErrorMessage.ToString(), "");
        }
    }

    protected void lvCourseDefault_Trainer_NeedDataSource(object sender, EventArgs e)
    {
        try
        {
            //foreach (var currid in CURRCOURSEID)
            //{
            var dtResources = DataHelper.GetCourseSections(COURSEID, 8);
            lvCourseDefault_Trainer.DataSource = dtResources;
            //}

            //if (CURRCOURSEID.Count == 0)
            //{
            //    btnReorderSequence.Visible = false;
            //}
        }
        catch (Exception ex)
        {
            RadWindowManager1.RadAlert("Error loading trainer resources" + ex.Message, 330, 180, Resources.LocalizedResource.ErrorMessage.ToString(), "");
        }
    }

    protected void lvCourseDefault_Learner_SectionDataBound(object sender, RadListViewItemEventArgs e)
    {
        var dataItem = e.Item as RadListViewDataItem;
        if (dataItem != null)
        {
            RadListView lvItem = dataItem.FindControl("lvCourseDefault_Learner_Item") as RadListView;
            int sectionID = Convert.ToInt32(lvCourseDefault_Learner.DataKeyValues[dataItem.DataItemIndex]["SectionID"].ToString());

            if (resources != null)
            {
                var item = resources
                           .AsQueryable()
                           .Where(p => p.SectionID == sectionID && (p.ScoTypeId >= 1 && p.ScoTypeId <= 4))
                             .Select(p => new
                             {
                                 ScoId = p.scoid,
                                 ScoPackageId = p.ScoPackageId,
                                 ScoTitle = p.ScoTitle,
                                 ScoTypeId = p.ScoTypeId,
                                 ScoType = p.ScoType,
                                 CourseTypeID = p.CourseTypeID,
                                 curriculumcourseID = p.curriculumcourseID,
                                 CourseOrder = p.CourseOrder,
                                 p.SectionID,
                                 p.dripEnable,
                                 SectionName = p.SectionName == null ? "NotSection" : p.SectionName,
                                 p.TestCategoryID
                             })
                            .ToList();

                lvItem.DataSource = item;
                lvItem.DataBind();
            }

            //foreach (var currid in CURRCOURSEID)
            //{
            //    var db = new TranscomUniversityV3ModelContainer();
            //    var dt1 = db.pr_Scorm_Cor_GetCurriculumPackages(COURSEID, currid, 0)
            //                .AsQueryable()
            //                .Where(p => p.SectionID == sectionID && (p.ScoTypeId >= 1 && p.ScoTypeId <= 4))
            //                .Select(p => new
            //                {
            //                    ScoId = p.scoid,
            //                    ScoPackageId = p.ScoPackageId,
            //                    ScoTitle = p.ScoTitle,
            //                    ScoTypeId = p.ScoTypeId,
            //                    ScoType = p.ScoType,
            //                    CourseTypeID = p.CourseTypeID,
            //                    curriculumcourseID = p.curriculumcourseID,
            //                    CourseOrder = p.CourseOrder,
            //                    p.SectionID,
            //                    p.dripEnable,
            //                    SectionName = p.SectionName == null ? "NotSection" : p.SectionName,
            //                    p.TestCategoryID
            //                })
            //                .ToList();

            //    lvItem.DataSource = dt1;
            //    lvItem.DataBind();
            //}
        }
        //var dripEnable = lvCourseDefault_Learner.DataKeyValues[itemIndex]["dripEnable"].ToString();
        //LinkButton btnClose = e.Item.FindControl("btnDelSCOFake") as LinkButton;
        //Label lblTitle = e.Item.FindControl("lblTitle") as Label;
        //if (dripEnable == "1")
        //{
        //    lblTitle.ForeColor = System.Drawing.Color.Gray;
        //    btnClose.Enabled = false;
        //    btnClose.OnClientClick = "";
        //}
        //else
        //{
        //    btnClose.Enabled = true;
        //    btnClose.OnClientClick = "showRadWindow(this);";
        //}
        //itemIndex++;
    }

    protected void LoadDripDDL()
    {
        var ddlContent = (RadComboBox)this.NamingContainer.FindControl("CourseBuilderDripContent1").FindControl("modalPopup").FindControl("C").FindControl("ddContent") as RadComboBox;

        ddlContent.DataTextField = "ScoTitle";
        ddlContent.DataValueField = "scoid";
        ddlContent.DataSource = resources;
        ddlContent.DataBind();
    }

    protected void lvCourseDefault_Additional_SectionDataBound(object sender, RadListViewItemEventArgs e)
    {
        var dataItem = e.Item as RadListViewDataItem;
        if (dataItem != null)
        {
            RadListView lvItem = dataItem.FindControl("lvCourseDefault_Additional_Item") as RadListView;
            int sectionID = Convert.ToInt32(lvCourseDefault_Additional.DataKeyValues[dataItem.DataItemIndex]["SectionID"].ToString());


            if (resources != null)
            {
                var item = resources
                            .AsQueryable()
                            .Where(p => p.SectionID == sectionID && p.ScoTypeId == 5)
                             .Select(p => new
                             {
                                 ScoId = p.scoid,
                                 ScoPackageId = p.ScoPackageId,
                                 ScoTitle = p.ScoTitle,
                                 ScoTypeId = p.ScoTypeId,
                                 ScoType = p.ScoType,
                                 CourseTypeID = p.CourseTypeID,
                                 curriculumcourseID = p.curriculumcourseID,
                                 CourseOrder = p.CourseOrder,
                                 p.SectionID,
                                 p.dripEnable,
                                 SectionName = p.SectionName == null ? "NotSection" : p.SectionName,
                                 p.TestCategoryID
                             })
                            .ToList();

                lvItem.DataSource = item;
                lvItem.DataBind();
            }
            //foreach (var currid in CURRCOURSEID)
            //{
            //    var db = new TranscomUniversityV3ModelContainer();             
            //    var dt1 = db.pr_Scorm_Cor_GetCurriculumPackages(COURSEID, currid, 0)
            //                .AsQueryable()
            //                .Where(p => p.SectionID == sectionID && p.ScoTypeId == 5)
            //                .Select(p => new 
            //                {
            //                    ScoId = p.scoid,
            //                    ScoPackageId = p.ScoPackageId,
            //                    ScoTitle = p.ScoTitle,
            //                    ScoTypeId = p.ScoTypeId,
            //                    ScoType = p.ScoType,
            //                    CourseTypeID = p.CourseTypeID,
            //                    curriculumcourseID = p.curriculumcourseID,
            //                    CourseOrder = p.CourseOrder,
            //                    p.SectionID,
            //                    p.dripEnable,
            //                    SectionName = p.SectionName == null ? "NotSection" : p.SectionName,
            //                    p.TestCategoryID
            //                })
            //                .ToList();

            //    lvItem.DataSource = dt1;
            //    lvItem.DataBind();

            //}



        }
    }

    protected void lvCourseDefault_Trainer_SectionDataBound(object sender, RadListViewItemEventArgs e)
    {
        var dataItem = e.Item as RadListViewDataItem;
        if (dataItem != null)
        {
            RadListView lvItem = dataItem.FindControl("lvCourseDefault_Trainer_Item") as RadListView;
            int sectionID = Convert.ToInt32(lvCourseDefault_Trainer.DataKeyValues[dataItem.DataItemIndex]["SectionID"].ToString());


            if (resources != null)
            {
                var item = resources
                           .AsQueryable()
                           .Where(p => p.SectionID == sectionID && p.ScoTypeId == 8)
                             .Select(p => new
                             {
                                 ScoId = p.scoid,
                                 ScoPackageId = p.ScoPackageId,
                                 ScoTitle = p.ScoTitle,
                                 ScoTypeId = p.ScoTypeId,
                                 ScoType = p.ScoType,
                                 CourseTypeID = p.CourseTypeID,
                                 curriculumcourseID = p.curriculumcourseID,
                                 CourseOrder = p.CourseOrder,
                                 p.SectionID,
                                 p.dripEnable,
                                 SectionName = p.SectionName == null ? "NotSection" : p.SectionName,
                                 p.TestCategoryID
                             })
                            .ToList();

                lvItem.DataSource = item;
                lvItem.DataBind();
            }
            //foreach (var currid in CURRCOURSEID)
            //{
            //    var db = new TranscomUniversityV3ModelContainer();
            //    var dt1 = db.pr_Scorm_Cor_GetCurriculumPackages(COURSEID, currid, 0)
            //                .AsQueryable()
            //                .Where(p => p.SectionID == sectionID && p.ScoTypeId == 8)
            //                .Select(p => new
            //                {
            //                    ScoId = p.scoid,
            //                    ScoPackageId = p.ScoPackageId,
            //                    ScoTitle = p.ScoTitle,
            //                    ScoTypeId = p.ScoTypeId,
            //                    ScoType = p.ScoType,
            //                    CourseTypeID = p.CourseTypeID,
            //                    curriculumcourseID = p.curriculumcourseID,
            //                    CourseOrder = p.CourseOrder,
            //                    p.SectionID,
            //                    p.dripEnable,
            //                    SectionName = p.SectionName == null ? "NotSection" : p.SectionName,
            //                    p.TestCategoryID
            //                })
            //                .ToList();

            //    lvItem.DataSource = dt1;
            //    lvItem.DataBind();
            //}
        }
    }

    protected void lvCourseDefault_Learner_Item_OnItemDataBound(object sender, RadListViewItemEventArgs e)
    {
        var dataItem = e.Item as RadListViewDataItem;
        if (dataItem != null)
        {
            var dripEnable = dataItem.GetDataKeyValue("dripEnable").ToString();
            LinkButton btnClose = e.Item.FindControl("btnDelSCOFake") as LinkButton;
            Label lblTitle = e.Item.FindControl("lblTitle") as Label;
            if (dripEnable == "1")
            {
                lblTitle.ForeColor = System.Drawing.Color.Gray;
                btnClose.Enabled = false;
                btnClose.OnClientClick = "";
            }
            else
            {
                btnClose.Enabled = true;
                btnClose.OnClientClick = "showRadWindow(this);";
            }
        }
    }

    protected void lvCourseDefault_Additional_Item_OnItemDataBound(object sender, RadListViewItemEventArgs e)
    {
        var dataItem = e.Item as RadListViewDataItem;
        if (dataItem != null)
        {
            var dripEnable = dataItem.GetDataKeyValue("dripEnable").ToString();
            LinkButton btnClose = e.Item.FindControl("btnDelSCOFake") as LinkButton;
            Label lblTitle = e.Item.FindControl("lblTitle") as Label;
            if (dripEnable == "1")
            {
                lblTitle.ForeColor = System.Drawing.Color.Gray;
                btnClose.Enabled = false;
                btnClose.OnClientClick = "";
            }
            else
            {
                btnClose.Enabled = true;
                btnClose.OnClientClick = "showRadWindow(this);";
            }
        }
    }

    protected void lvCourseDefault_Trainer_Item_OnItemDataBound(object sender, RadListViewItemEventArgs e)
    {
        var dataItem = e.Item as RadListViewDataItem;
        if (dataItem != null)
        {
            var dripEnable = dataItem.GetDataKeyValue("dripEnable").ToString();
            LinkButton btnClose = e.Item.FindControl("btnDelSCOFake") as LinkButton;
            Label lblTitle = e.Item.FindControl("lblTitle") as Label;
            if (dripEnable == "1")
            {
                lblTitle.ForeColor = System.Drawing.Color.Gray;
                btnClose.Enabled = false;
                btnClose.OnClientClick = "";
            }
            else
            {
                btnClose.Enabled = true;
                btnClose.OnClientClick = "showRadWindow(this);";
            }
        }
    }

    //Order
    protected void lvCourseOrder_Learner_NeedDataSource(object sender, EventArgs e)
    {
        try
        {
            foreach (var currid in CURRCOURSEID)
            {
                var dtResources = DataHelper.GetCourseSections(COURSEID, 1);
                lvCourseOrder_Learner.DataSource = dtResources;
            }
        }
        catch (Exception ex)
        {
            RadWindowManager1.RadAlert("1" + ex.Message, 330, 180, Resources.LocalizedResource.ErrorMessage.ToString(), "");
        }
    }

    protected void lvCourseOrder_Additional_NeedDataSource(object sender, EventArgs e)
    {
        try
        {
            foreach (var currid in CURRCOURSEID)
            {
                var dtResources = DataHelper.GetCourseSections(COURSEID, 5);
                lvCourseOrder_Additional.DataSource = dtResources;
            }
        }
        catch (Exception ex)
        {
            RadWindowManager1.RadAlert("2" + ex.Message, 330, 180, Resources.LocalizedResource.ErrorMessage.ToString(), "");
        }
    }

    protected void lvCourseOrder_Trainer_NeedDataSource(object sender, EventArgs e)
    {
        try
        {
            foreach (var currid in CURRCOURSEID)
            {
                var dtResources = DataHelper.GetCourseSections(COURSEID, 8);
                lvCourseOrder_Trainer.DataSource = dtResources;
            }
        }
        catch (Exception ex)
        {
            RadWindowManager1.RadAlert("4" + ex.Message, 330, 180, Resources.LocalizedResource.ErrorMessage.ToString(), "");
        }
    }

    protected void lvCourseOrder_Learner_SectionDataBound(object sender, RadListViewItemEventArgs e)
    {
        var dataItem = e.Item as RadListViewDataItem;
        if (dataItem != null)
        {
            RadListView lvItem = dataItem.FindControl("lvCourseOrder_Learner_Item") as RadListView;
            int sectionID = Convert.ToInt32(lvCourseOrder_Learner.DataKeyValues[dataItem.DataItemIndex]["SectionID"].ToString());

            foreach (var currid in CURRCOURSEID)
            {
                var db = new TranscomUniversityV3ModelContainer();
                var dt1 = db.pr_Scorm_Cor_GetCurriculumPackages(COURSEID, currid, 0)
                            .AsQueryable()
                            .Where(p => p.SectionID == sectionID && (p.ScoTypeId >= 1 && p.ScoTypeId <= 4))
                            .Select(p => new
                            {
                                ScoId = p.scoid,
                                ScoPackageId = p.ScoPackageId,
                                ScoTitle = p.ScoTitle,
                                ScoTypeId = p.ScoTypeId,
                                ScoType = p.ScoType,
                                CourseTypeID = p.CourseTypeID,
                                curriculumcourseID = p.curriculumcourseID,
                                CourseOrder = p.CourseOrder,
                                p.SectionID,
                                p.dripEnable,
                                SectionName = p.SectionName == null ? "NotSection" : p.SectionName,
                                p.TestCategoryID
                            })
                            .ToList();

                lvItem.DataSource = dt1;
                lvItem.DataBind();
            }
        }
    }

    protected void lvCourseOrder_Additional_SectionDataBound(object sender, RadListViewItemEventArgs e)
    {
        var dataItem = e.Item as RadListViewDataItem;
        if (dataItem != null)
        {
            RadListView lvItem = dataItem.FindControl("lvCourseOrder_Additional_Item") as RadListView;
            int sectionID = Convert.ToInt32(lvCourseOrder_Additional.DataKeyValues[dataItem.DataItemIndex]["SectionID"].ToString());

            foreach (var currid in CURRCOURSEID)
            {
                var db = new TranscomUniversityV3ModelContainer();
                var dt1 = db.pr_Scorm_Cor_GetCurriculumPackages(COURSEID, currid, 0)
                            .AsQueryable()
                            .Where(p => p.SectionID == sectionID && p.ScoTypeId == 5)
                            .Select(p => new
                            {
                                ScoId = p.scoid,
                                ScoPackageId = p.ScoPackageId,
                                ScoTitle = p.ScoTitle,
                                ScoTypeId = p.ScoTypeId,
                                ScoType = p.ScoType,
                                CourseTypeID = p.CourseTypeID,
                                curriculumcourseID = p.curriculumcourseID,
                                CourseOrder = p.CourseOrder,
                                p.SectionID,
                                p.dripEnable,
                                SectionName = p.SectionName == null ? "NotSection" : p.SectionName,
                                p.TestCategoryID
                            })
                            .ToList();

                lvItem.DataSource = dt1;
                lvItem.DataBind();
            }
        }
    }

    protected void lvCourseOrder_Trainer_SectionDataBound(object sender, RadListViewItemEventArgs e)
    {
        var dataItem = e.Item as RadListViewDataItem;
        if (dataItem != null)
        {
            RadListView lvItem = dataItem.FindControl("lvCourseOrder_Trainer_Item") as RadListView;
            int sectionID = Convert.ToInt32(lvCourseOrder_Trainer.DataKeyValues[dataItem.DataItemIndex]["SectionID"].ToString());

            foreach (var currid in CURRCOURSEID)
            {
                var db = new TranscomUniversityV3ModelContainer();
                var dt1 = db.pr_Scorm_Cor_GetCurriculumPackages(COURSEID, currid, 0)
                            .AsQueryable()
                            .Where(p => p.SectionID == sectionID && p.ScoTypeId == 8)
                            .Select(p => new
                            {
                                ScoId = p.scoid,
                                ScoPackageId = p.ScoPackageId,
                                ScoTitle = p.ScoTitle,
                                ScoTypeId = p.ScoTypeId,
                                ScoType = p.ScoType,
                                CourseTypeID = p.CourseTypeID,
                                curriculumcourseID = p.curriculumcourseID,
                                CourseOrder = p.CourseOrder,
                                p.SectionID,
                                p.dripEnable,
                                SectionName = p.SectionName == null ? "NotSection" : p.SectionName,
                                p.TestCategoryID
                            })
                            .ToList();

                lvItem.DataSource = dt1;
                lvItem.DataBind();
            }
        }
    }

    protected void lvCourseOrder_Learner_Item_OnItemDataBound(object sender, RadListViewItemEventArgs e)
    {
        var dataItem = e.Item as RadListViewDataItem;
        if (dataItem != null)
        {
            var dripEnable = dataItem.GetDataKeyValue("dripEnable").ToString();
            Label lblTitle = e.Item.FindControl("lblTitle") as Label;
            if (dripEnable == "1")
            {
                lblTitle.ForeColor = System.Drawing.Color.Gray;
            }

        }
    }

    protected void lvCourseOrder_Additional_Item_OnItemDataBound(object sender, RadListViewItemEventArgs e)
    {
        var dataItem = e.Item as RadListViewDataItem;
        if (dataItem != null)
        {
            var dripEnable = dataItem.GetDataKeyValue("dripEnable").ToString();
            Label lblTitle = e.Item.FindControl("lblTitle") as Label;
            if (dripEnable == "1")
            {
                lblTitle.ForeColor = System.Drawing.Color.Gray;
            }
        }
    }

    protected void lvCourseOrder_Trainer_Item_OnItemDataBound(object sender, RadListViewItemEventArgs e)
    {
        var dataItem = e.Item as RadListViewDataItem;
        if (dataItem != null)
        {
            var dripEnable = dataItem.GetDataKeyValue("dripEnable").ToString();
            Label lblTitle = e.Item.FindControl("lblTitle") as Label;
            if (dripEnable == "1")
            {
                lblTitle.ForeColor = System.Drawing.Color.Gray;
            }
        }
    }


    #region FileExtension

    public static bool isFile(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (!isPdf(filename) && !isWord(filename) && !isExcel(filename) && !isPpt(filename) && !isImg(filename) && !isZip(filename) && !isMp3(filename) && !isMp4(filename))
            return true;
        else
            return false;
    }

    public static bool isPdf(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (ext == "pdf")
            return true;
        else
            return false;
    }

    public static bool isWord(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (ext == "doc" || ext == "docx")
            return true;
        else
            return false;
    }

    public static bool isExcel(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (ext == "xls" || ext == "xlsx")
            return true;
        else
            return false;
    }

    public static bool isPpt(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (ext == "ppt" || ext == "pptx")
            return true;
        else
            return false;
    }

    public static bool isImg(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (ext == "jpg" || ext == "jpeg" || ext == "png")
            return true;
        else
            return false;
    }

    public static bool isZip(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (ext == "rar" || ext == "zip")
            return true;
        else
            return false;
    }

    public static bool isMp3(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (ext == "mp3")
            return true;
        else
            return false;
    }

    public static bool isMp4(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (ext == "mp4")
            return true;
        else
            return false;
    }

    #endregion

}