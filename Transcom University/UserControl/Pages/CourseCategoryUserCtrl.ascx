﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CourseCategoryUserCtrl.ascx.cs"
    Inherits="UserControl_Pages_CourseCategoryUserCtrl" %>
<rad:RadCodeBlock runat="server" ID="rcbCategory">
    <script type="text/javascript">

        var selectedSubcatId;
        var selectedRadGrid;
        var selectedRadList;
        var selectecCategoryId;
        var arrGrid = new Array();
        var arrList = new Array();
        var myRecommended;

        function mcOnListViewDataBinding(sender, args) {
            sender.set_selectedIndexes([]);
          
        }
        function mcOnListViewCommand(sender, args) {
            debugger;
            args.set_cancel(true);
        }

        function initializeGridSubcategoryCourse(sender, args, subcategoryid) {
            $ = $telerik.$;
            //this will fire the OnCommand event
            //sender.rebind();
            var view = '<%= Request.QueryString["View"] %>';
            var mand = '<%= Request.QueryString["Mand"] %>';
          
            var pageSize = 4;
            if(view == "SeeAll" && mand == subcategoryid)
                pageSize = 99;
            sender.set_selectedIndexes([]);
            sender.set_pageSize(pageSize);
            sender.page(0);

            sender.set_currentPageIndex(0);
            var id = sender._element.id;
            if('<%= Request.QueryString["view"] %>' == "MyRecommended"  && '<%= Request.QueryString["Tab"] %>' == "Leadership")
                myRecommended =  1;
            else
                myRecommended = 0;
            arrGrid.push({ radlist: id, id: subcategoryid});
            bindSubcategories(sender, args, subcategoryid);

        }

         function initializeListSubcategoryCourse(sender, args, subcategoryid) {
            $ = $telerik.$;
            //this will fire the OnCommand event

            //sender.rebind();
            var view = '<%= Request.QueryString["View"] %>';
            var pageSize = 4;
            if(view == "SeeAll")
                pageSize = 99;

            sender.set_selectedIndexes([]);
            sender.set_pageSize(pageSize);
            sender.page(0);

            sender.set_currentPageIndex(0);
            var id = sender._element.id;

            arrList.push({ radlist: id, id: subcategoryid});
            bindSubcategories(sender, args, subcategoryid);

        }


        function bindSubcategories(sender, args, subcategoryid) {
            //sender.set_selectedIndexes([]);
            selectecCategoryId = $("#<%= hfCurrentCategoryId.ClientID %>").val();
            
            var startRowIndex = sender.get_currentPageIndex() * sender.get_pageSize(),
                maximumRows = sender.get_pageSize(),
                sortExpression = sender.get_sortExpressions().toLinq();

          

            var s = sender.UniqueID;
            var c;
            //make a call to get the data
            PageMethods.GetSubCategoryCoursesData(startRowIndex, maximumRows, sortExpression, subcategoryid, parseInt(selectecCategoryId), myRecommended,

                function (result) {
                    c = result.count;
                    sender.set_virtualItemCount(c);
                    
                    if (c > 4) {
                        $("#list" +subcategoryid+ " .mcPager .pagePrev").removeClass("display-none");
                        $("#list" +subcategoryid+ " .mcPager .pageNext").removeClass("display-none");
                        $(".mcViews" + subcategoryid).removeClass("display-none");

                        $("#<%= btnSwitchToListView.ClientID %>").removeClass("display-none");
                    }else{
                        $("#list" +subcategoryid+ " .mcPager .pagePrev").addClass("display-none");
                        $("#list" +subcategoryid+ " .mcPager .pageNext").addClass("display-none");
                        $(".mcViews" + subcategoryid).addClass("display-none");
                    }

                    sender.set_dataSource(result.data);
                    //console.log("outdata = subcategoryid -" + subcategoryid + " - count - " + c + " - sender - " + s + "- cat " + selectecCategoryId);
                    $(".js-mc-loader-" + subcategoryid).hide();
                    sender.dataBind();
                    //console.log("lazyload");
                    //lazyload();
                    //let lazy = 
                     lazyload();

                },
                function (error) {
                    var code = error._statusCode;
                    if (code == 401)
                        location.reload();
                    else if (code == 500){
                        console.log(error)
                         var hdnradmessage = $("#<%=hdnradmessage.ClientID%>").val();
                        var hndraderrormessage = $("#<%=hndraderrormessage.ClientID%>").val();
                        radalert(hdnradmessage, 330, 180, hndraderrormessage, "");
                    }
                    
                });
        }

        function bindCourses(grid, list, subcategoryid) {
            var startRowIndex = grid.get_currentPageIndex() * grid.get_pageSize(),
                maximumRows = grid.get_pageSize(),
                sortExpression = grid.get_sortExpressions().toLinq();

            //make a call to get the data
            PageMethods.GetSubCategoryCoursesData(startRowIndex, maximumRows, sortExpression, subcategoryid, parseInt(selectecCategoryId), myRecommended,

                function (result) {
                    PageMethods.GetSubCategoryCoursesCount(function (count) {
                        grid.set_virtualItemCount(count);

                    });


                    grid.set_dataSource(result.data);
                    grid.dataBind();

                    list.set_dataSource(result.data);
                    list.dataBind();

                    $(".js-mc-loader-" + subcategoryid).hide();

                    debugger;
                    if (grid.get_pageSize() == 4) {
                        $("#btnViewAllListMC" + subcategoryid).removeClass("display-none");
                        $("#btnBackListMC" + subcategoryid).addClass("display-none");
                    }
                    else if (grid.get_pageSize() > 4) {
                        $("#btnViewAllListMC" + subcategoryid).addClass("display-none");
                        $("#btnBackListMC" + subcategoryid).removeClass("display-none");
                     
                    }
                    else {
                        $("#btnViewAllListMC" + subcategoryid).addClass("display-none");
                        $("#btnBackListMC" + subcategoryid).addClass("display-none");
                    }
                    lazyload();


                },
                function (error) {
                    var code = error._statusCode;
                    if (code == 401)
                        location.reload();
                    else if (code == 500)
                        var hdnradmessage = $("#<%=hdnradmessage.ClientID%>").val();
                    var hndraderrormessage = $("#<%=hndraderrormessage.ClientID%>").val();
                    radalert(hdnradmessage, 330, 180, hndraderrormessage, "");
                });
        }

        function pageNext(sender) {
            selectedSubcatId = $(sender).parent().parent().parent().parent().find('input[name="hfMySubcatId"]').val();
//            var listId = arrList.find(x => x.id == selectedSubcatId).radlist;
//            var gridId = arrGrid.find(x => x.id == selectedSubcatId).radlist;
                var listId;
                var gridId;
                arrList.map(function(obj, index) {
                    if(obj.id == selectedSubcatId) {
                        listId = obj.radlist;
                    }
                });
                arrGrid.map(function(obj, index) {
                    if(obj.id == selectedSubcatId) {
                        gridId = obj.radlist;
                    }
                });

            selectedRadGrid = $find(gridId);
            selectedRadList = $find(listId);
         
            var page = Math.ceil(selectedRadGrid.get_virtualItemCount()/selectedRadGrid.get_pageSize());

            if((selectedRadGrid.get_currentPageIndex() + 1) < page){
                selectedRadGrid.set_selectedIndexes([]);
                selectedRadGrid.page(selectedRadGrid.get_currentPageIndex() + 1);

                selectedRadList.set_selectedIndexes([]);
                selectedRadList.page(selectedRadList.get_currentPageIndex() + 1);

                bindCourses(selectedRadGrid, selectedRadList, selectedSubcatId);
            }
        }

        function pagePrev(sender) {

            selectedSubcatId = $(sender).parent().parent().parent().parent().find('input[name="hfMySubcatId"]').val();
//            var listId = arrList.find(x => x.id == selectedSubcatId).radlist;
//            var gridId = arrGrid.find(x => x.id == selectedSubcatId).radlist;
                var listId;
                var gridId;
                arrList.map(function(obj, index) {
                    if(obj.id == selectedSubcatId) {
                        listId = obj.radlist;
                    }
                });
                arrGrid.map(function(obj, index) {
                    if(obj.id == selectedSubcatId) {
                        gridId = obj.radlist;
                    }
                });
            selectedRadGrid = $find(gridId);
            selectedRadList = $find(listId);

            if(selectedRadGrid.get_currentPageIndex() > 0){
                selectedRadGrid.set_selectedIndexes([]);
                selectedRadGrid.page(selectedRadGrid.get_currentPageIndex() - 1);

                selectedRadList.set_selectedIndexes([]);
                selectedRadList.page(selectedRadList.get_currentPageIndex() - 1);

                bindCourses(selectedRadGrid, selectedRadList, selectedSubcatId);
                
            }

        }

        function btnViewAllListMC_Click(sender) {
                
            selectedSubcatId = sender;
//            var listId = arrList.find(x => x.id == selectedSubcatId).radlist;
//            var gridId = arrGrid.find(x => x.id == selectedSubcatId).radlist;
             var listId;
            var gridId;
                arrList.map(function(obj, index) {
                    if(obj.id == selectedSubcatId) {
                        listId = obj.radlist;
                    }
                });
                arrGrid.map(function(obj, index) {
                    if(obj.id == selectedSubcatId) {
                        gridId = obj.radlist;
                    }
                });
            selectedRadGrid = $find(gridId);
            selectedRadList = $find(listId);

            selectedRadGrid.set_pageSize(12);
            selectedRadGrid.page(0);

            selectedRadList.set_pageSize(12);
            selectedRadList.page(0);

           

            bindCourses(selectedRadGrid, selectedRadList, selectedSubcatId);
        }

        function btnBackListMC_Click(sender) {

            selectedSubcatId = sender;

//            var listId = arrList.find(x => x.id == selectedSubcatId).radlist;
//            var gridId = arrGrid.find(x => x.id == selectedSubcatId).radlist;
            var listId;
            var gridId;
            arrList.map(function (obj, index) {
                if (obj.id == selectedSubcatId) {
                    listId = obj.radlist;
                }
            });
            arrGrid.map(function (obj, index) {
                if (obj.id == selectedSubcatId) {
                    gridId = obj.radlist;
                }
            });

            selectedRadGrid = $find(gridId);
            selectedRadList = $find(listId);

            selectedRadGrid.set_pageSize(4);
            selectedRadGrid.page(0);
            
            selectedRadList.set_pageSize(4);
            selectedRadList.page(0);

            bindCourses(selectedRadGrid, selectedRadList, selectedSubcatId);
        }

    </script>
</rad:RadCodeBlock>
<rad:RadAjaxManagerProxy ID="rampCategoryCourse" runat="server">
    <AjaxSettings>
        <rad:AjaxSetting AjaxControlID="SubcategoryPager">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="SubcategoryPager" />
                <rad:AjaxUpdatedControl ControlID="lvSubcategoryCourses" />
            </UpdatedControls>
        </rad:AjaxSetting>
    </AjaxSettings>
</rad:RadAjaxManagerProxy>
<asp:Panel ID="pnlListView" runat="server">
    <asp:Panel ID="pnlLvSubcategoryCourses" runat="server">
        <div class="th" style="color: Black;">
            <asp:LinkButton ID="btnSwitchToListView" runat="server" OnClientClick="btnSwitchView_Click(); return false;"
                Font-Underline="false" aria-hidden="true" CssClass="display-none">
                <i id="switchClassListView" runat="server" class="fa fa-th-list "></i>
            </asp:LinkButton>
            <asp:LinkButton ID="btnSwitchToGridView" runat="server" OnClientClick="btnSwitchView_Click(); return false;"
                Font-Underline="false" aria-hidden="true" CssClass="display-none">
                <i id="switchClassGridView" runat="server" class="fa fa-th "></i>
            </asp:LinkButton>
        </div>
        <div id="mc-container" class="mc-container col-md-12 no-paddings">
            <rad:RadListView ID="lvSubcategoryCourses" runat="server" AllowPaging="true" AllowMultiFieldSorting="true"
                OnNeedDataSource="lvSubcategoryCourses_NeedDataSource" DataKeyNames="SubcategoryID"
                PageSize="100" ItemPlaceholderID="SubcategoryHolder" ClientSettings-DataBinding-DataService-EnableCaching="true">
                <LayoutTemplate>
                    <div id="SubcategoryHolder" runat="server">
                    </div>
                </LayoutTemplate>
                <ItemTemplate>
                    <div id="courseContainer<%# Eval("SubcategoryID") %>" class="col-md-12 margin-bottom-10px gray-border-no-pads">
                        <div class="row widgets black-header">
                            <div class="col-md-6 font-bold wrap">
                                <%# Eval("Subcategory") %>
                            </div>
                            <div class="col-md-6 font-bold wrap">
                                <div class="row-fluid mcViews<%# Eval("SubcategoryID") %> display-none">
                                    <div class="pull-right">
                                        <button id="btnViewAllListMC<%# Eval("SubcategoryID") %>" onclick="btnViewAllListMC_Click(<%# Eval("SubcategoryID") %>); return false;"
                                            class="btn-transparent gold">
                                            View All</button>
                                        <button id="btnBackListMC<%# Eval("SubcategoryID") %>" onclick="btnBackListMC_Click(<%# Eval("SubcategoryID") %>); return false;"
                                            class="btn-transparent gold display-none ">
                                            <i class="fa fa-long-arrow-left "></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="loader js-mc-loader-<%# Eval("SubcategoryID") %>">
                        </div>
                        <div id="list<%# Eval("SubcategoryID") %>" class="row">
                            <div class="row">
                                <input name="hfMySubcatId" value='<%# Eval("SubcategoryID") %>' class="display-none" />
                                <div class="js-gridViewContainer col-md-12">
                                    <rad:RadListView ID="gridSubcategoryCourses" runat="server" AllowPaging="true" PageSize="4"
                                        AllowMultiFieldSorting="true" ClientSettings-ClientEvents-OnListViewCreating='<%# "function (s,a){initializeGridSubcategoryCourse(s,a,"+Eval("SubcategoryID")+");}" %>'
                                        ItemPlaceholderID="mcItems" CssClass="lvCourse">
                                        <LayoutTemplate>
                                            <div class="col-md-12" style="position: relative;">
                                                <div class="mcPager">
                                                    <a class="pagePrev fa fa-chevron-left pull-left relative-vr-left-center gray-arrow no-underline-hover display-none"
                                                        href="javascript:void(0);" title="Go to previous page" onclick="pagePrev(this);">
                                                    </a><a class="pageNext fa fa-chevron-right pull-right relative-vr-right-center gray-arrow no-underline-hover display-none"
                                                        href="javascript:void(0);" title="Go to next page" onclick="pageNext(this);">
                                                    </a>
                                                </div>
                                                <asp:Panel runat="server" ID="mcItems" CssClass="container">
                                                </asp:Panel>
                                            </div>
                                        </LayoutTemplate>
                                        <ClientSettings>
                                            <DataBinding>
                                                <ItemTemplate>  
                                                <div class="col-md-3">                         
                                                    <a id="hllvSubcategoryCourses" onclick='confirmLaunchCourse("#= EncryptedCourseID #");' style="color:Black;">
                                                        <div id="divSubcategoryCourses" runat="server" class="mc-item lv rlvI">
                                                            <div class="photo-container">
                                                                <img class="lazyload" src='Media/Images/empty.png' data-src='Media/Uploads/CourseImg/#= CourseID #/#= CourseImage #' onerror="this.src='Media/Uploads/CourseImg/No_image.jpg'" style="height:130px;width:190px;"></img>
                                                            </div>
                                                          
                                                            <div class="category font-bold text-trim">
                                                                <p class="clamp-line-1">
                                                                    #= CourseTitle #
                                                                </p>
                                                            </div>
                                                            <div class="category">
                                                                <p class="clamp-line-1 font-desc">
                                                                    #= CourseDuration # 
                                                                </p>
                                                            </div>
                                                            <div class="category">
                                                                <p class="clamp-line-3 font-desc" style="display: -webkit-box;"> 
                                                                    #= CourseDescription #
                                                                </p>
                                                           
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                                </ItemTemplate>
                                                <EmptyDataTemplate>
                                                    No Course to Display.
                                                </EmptyDataTemplate>
                                                <DataService EnableCaching="true" />
                                            </DataBinding>
                                            <ClientEvents OnCommand="mcOnListViewCommand"></ClientEvents>
                                        </ClientSettings>
                                    </rad:RadListView>
                                </div>
                                <div class="js-listViewContainer col-md-12 display-none">
                                    <rad:RadListView ID="listSubcategoryCourses" runat="server" AllowPaging="true" PageSize="4"
                                        ClientSettings-ClientEvents-OnListViewCreating='<%# "function (s,a){initializeListSubcategoryCourse(s,a,"+Eval("SubcategoryID")+");}" %>'
                                        ItemPlaceholderID="listCoursesItems">
                                        <LayoutTemplate>
                                            <table class="gridMainTable table table-bordered table-striped course-grid">
                                                <thead>
                                                    <tr class="rlvHeader">
                                                        <th class="btn-teal">
                                                            <asp:Label runat="server" ID="Title" Text="<%$ Resources:LocalizedResource, Title %>"></asp:Label>
                                                        </th>
                                                        <th class="btn-teal">
                                                            <asp:Label runat="server" ID="descripts" Text="<%$ Resources:LocalizedResource, Description %>"></asp:Label>
                                                        </th>
                                                        <th class="btn-teal">
                                                            <asp:Label runat="server" ID="Durations" Text="<%$ Resources:LocalizedResource, Duration %>"></asp:Label>
                                                        </th>
                                                        <th class="btn-teal">
                                                            <asp:Label runat="server" ID="Lastupdates" Text="<%$ Resources:LocalizedResource, LastUpdated %>"> </asp:Label>
                                                        </th>
                                                        <th class="btn-teal">
                                                            <asp:Label runat="server" ID="pathcourse" Text="<%$ Resources:LocalizedResource,  CoursePath %>"> </asp:Label>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody id="listCoursesItems" runat="server">
                                                </tbody>
                                                <tfoot>
                                                </tfoot>
                                            </table>
                                            <div class="mcPager margin-bottom-10px">
                                                <a class="pagePrev fa fa-caret-left gray-arrow no-underline-hover display-none left-arrow-pager"
                                                    href="javascript:void(0);" title="Go to previous page" onclick="pagePrev(this);">
                                                </a><a class="pageNext fa fa-caret-right gray-arrow no-underline-hover display-none right-arrow-pager"
                                                    href="javascript:void(0);" title="Go to next page" onclick="pageNext(this);">
                                                </a>
                                            </div>
                                        </LayoutTemplate>
                                        <ClientSettings>
                                            <DataBinding>
                                                <ItemTemplate>
                                                <tr class="rlvI">
                                                    <td>
                                                        <p class="">
                                                            <a id="hlgridSubcategoryCourses" onclick='confirmLaunchCourse("#= EncryptedCourseID #");' class="hover-pointer"> #= CourseTitle #</a>
                                                        </p>
                                                    </td>
                                                    <td>
                                                        <p class="" style="display: -webkit-box;">
                                                            <asp:Label ID="Label2" runat="server"> #= CourseDescription #</asp:Label>
                                                        </p>
                                                    </td>
                                                    <td>
                                                        <p class="clamp-line-2" style="display: -webkit-box;">
                                                            <asp:Label ID="Label3" runat="server"> #= CourseDuration #</asp:Label>
                                                        </p>
                                                    </td>
                                                    <td>
                                                        <p class="clamp-line-2" style="display: -webkit-box;">
                                                            <asp:Label ID="Label4" runat="server"> #= DateLastModified #</asp:Label>
                                                        </p>
                                                    </td>
                                                    <td>
                                                        <p class="clamp-line-2" style="display: -webkit-box;">
                                                            <asp:Label ID="Label5" runat="server"> #= CoursePath #</asp:Label>
                                                        </p>
                                                    </td>
                                                </tr>
                                                </ItemTemplate>
                                                <EmptyDataTemplate>
                                                No Course.
                                                </EmptyDataTemplate>
                                                <DataService EnableCaching="true" />
                                            </DataBinding>
                                            <ClientEvents OnCommand="mcOnListViewCommand"></ClientEvents>
                                        </ClientSettings>
                                    </rad:RadListView>
                                </div>
                                <script type="text/javascript">
                                    function btnSwitchView_Click() {
                                        if ($('.js-gridViewContainer').hasClass("display-none")) {

                                            $(".js-gridViewContainer").removeClass("display-none");
                                            $(".js-listViewContainer").addClass("display-none");
                                            $("#<%= btnSwitchToGridView.ClientID %>").addClass("display-none");
                                            $("#<%= btnSwitchToListView.ClientID %>").removeClass("display-none");
                                            //switch to gridview

                                        } else {
                                            //switch to list view

                                            $(".js-gridViewContainer").addClass("display-none");
                                            $(".js-listViewContainer").removeClass("display-none");
                                            $("#<%= btnSwitchToListView.ClientID %>").addClass("display-none");
                                            $("#<%= btnSwitchToGridView.ClientID %>").removeClass("display-none");


                                        }
                                        return false;
                                    }
                                </script>
                            </div>
                        </div>
                    </div>
                </ItemTemplate>
                <EmptyDataTemplate>
                    No Course.
                </EmptyDataTemplate>
            </rad:RadListView>
        </div>
        <asp:HiddenField ID="hfCurrentCategoryId" runat="server" />
    </asp:Panel>
</asp:Panel>
<asp:HiddenField runat="server" ID="hdnradmessage" Value="Something went while loading courses. Please try again." />
<asp:HiddenField runat="server" ID="hndraderrormessage" Value="Error Message" />
