﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TranscomUniversityV3Model;
using Telerik.Web.UI;

public partial class UserControl_Pages_CourseCategoryUserCtrl : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    protected void lvSubcategoryCourses_NeedDataSource(object sender, EventArgs e)
    {
        TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();
        int id = Convert.ToInt32(Utils.Decrypt(Request.QueryString["Id"].ToString()));
        var _subcategory = db.tbl_TranscomUniversity_Lkp_Subcategory
                            .AsEnumerable()
                            .Where(c =>
                                c.CategoryID == id
                            )
                            .OrderByDescending(c => c.Subcategory)
                            .ToList();
        hfCurrentCategoryId.Value = id.ToString();
        lvSubcategoryCourses.DataSource = _subcategory;
    }

  

    public class CourseDetails
    {
        public string EncryptedCourseID { get; set; }

        public int CourseID { get; set; }

        public string CourseImage { get; set; }

        public string CourseTitle { get; set; }

        public string CourseType { get; set; }

        public int? CourseDuration { get; set; }

        public string CourseCategory { get; set; }

        public string CourseSubcategory { get; set; }

        public string CourseDescription { get; set; }

        public string DateLastModified { get; set; }

        public string CoursePath { get; set; }
    }
}
