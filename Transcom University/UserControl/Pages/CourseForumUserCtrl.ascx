﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CourseForumUserCtrl.ascx.cs"
    Inherits="UserControl_Pages_CourseForumUserCtrl"  %>
<rad:RadScriptBlock ID="rsbForum" runat="server">
    <script type="text/javascript">
        var currUploadedFileSize;

        function initializeForum() {
            //            $('html, body').animate({
            //                scrollTop: $("#forumAccordion").offset().top - 30
            //            }, 200);

            //open the thread again on postback
            openForumThread($('#hfSelectedThread').val());
          
            $(".JSDecodeComment").each(function(){
                  var comment =  $(this).html();
                  try {
                          comment = decodeURI(comment);
                          //comment = imagify(comment);
                          comment = linkify(comment);
                            $(this).html(comment);
                    } catch (e) {
                        $(this).html("Invalid Comment");
                    }
               
            });

            currUploadedFileSize = 0;

            var ctDateCreated = $(".lblDateCreated");
//            var ctTimeCreated = $(".lblTimeCreated");

            $.each(ctDateCreated, function(idx, el) {                
                var txtDateCreated = $(el).text();
                var txtTimeCreated = $(el).next().text();

                var txtThreadDate = txtDateCreated + " " + txtTimeCreated;
                txtThreadDate = convertTimeZone(txtThreadDate);

                $(el).text(moment(txtThreadDate).format("MM/DD/YYYY"));
                $(el).next().text(moment(txtThreadDate).format("hh:mm A"));
            });

            localizeDatePosted();
               
           
            initializeImageModal();

        }

        function localizeDatePosted() {
             var ctdatePosted = $(".datePosted");
                    $.each(ctdatePosted, function (idx, el) {
                        var txtDateCreated = $(el).text();

                        var txtThreadDate = convertTimeZone(txtDateCreated);

                        $(el).text(moment(txtThreadDate).format("MM/DD/YYYY hh:mm A"));
                    });

        }

        function initializeImageModal(){
            var modal = $('#imageModal');
            // Get the image and insert it inside the modal - use its "alt" text as a caption
            var img = $(".js-img-zoom");
            var modalImg = $("#img01");
            img.unbind().click(function(){
                modal.css({ "display" : "block"});
                modalImg.attr("src", $(this).attr("src"));
                disableBodyScroll();
            });
           
            // Get the <span> element that closes the modal
            var span = $("#imageModal span")

            // When the user clicks on <span> (x), close the modal
            span.unbind().click(function() { 
                 modal.css({ "display" : "none"});
                 enableBodyScroll();
            });

        }

        function openForumThread(threadid) {
            if (threadid != "") {
                //$("#lnkShow" + threadid).click();
                $("#lnkShow" + threadid).html("Hide");
                $("#tab" + threadid).addClass("in");
            }
        }

        function forumTabClick(el, threadid) {
            //var panelId = $(el).attr("href");
            //            $("#forumAccordion .panel-collapse").removeClass("in");
            //            $(panelId).addClass("in");
            //$('#forumAccordion .js-collapse-event').html("Show");
            hfShow = $("#<%= hfShow.ClientID %>").val();
            hfHide = $("#<%= hfHide.ClientID %>").val();

           
            $("#tab" + threadid).toggle("collapse");
            
            if ($(el).hasClass("isHidden")) {
                $(el).html(hfHide);
                $(el).removeClass("isHidden");
                $(el).addClass("isShowing");
                $('#hfSelectedThread').val(threadid);
            } else {
                $(el).html(hfShow);
                $(el).removeClass("isShowing");
                $(el).addClass("isHidden");
                $('#hfSelectedThread').val('');
            }
            clearRadUploader();



        }

        function showReport(e, postid) {
            hideReplyPanel();
            var id = $(e).attr("id");
            $("#" + '<%= hfSelectedPostId.ClientID %>').val(postid);
            $("#<%= ReportMessage.ClientID %>").val('');
            $('#panelReport').removeClass("display-none");
            $("#panelReport").insertAfter($("#" + id).parent().parent());

        }

        function showReply(e, postid) {
            hideReportPanel();
            var id = $(e).attr("id");
            $("#" + '<%= hfSelectedPostId.ClientID %>').val(postid);
            var name = $("#name" + postid).html();
            var comment = $("#comment" + postid).html();
            $("#<%= txtReplyMessage.ClientID %>").val('');
            //var date = $("#date" + postid).html();
            $('#panelReply .talktext').html(
                '<p>' +
                '<b>-' + name
                + '<br>'
                + '<i> ' + comment + ' </i>' +
                '</p>'
            );
            $('#panelReply').removeClass("display-none");
            $("#panelReply").insertAfter($("#" + id).parent().parent());

        }

        function hideReportPanel() {
            $("#" + "<%= ReportMessage.ClientID %>").val("");
            $('#panelReport').addClass("display-none");
            return false;
        }
        function hideReplyPanel() {
            $("#" + "<%= ReportMessage.ClientID %>").val("");
            $('#panelReply').addClass("display-none");
            return false;
        }

        function fakeClickBtnHelpful(el, postid) {
            var icon = $(el).find(".fa");
            icon.removeClass("fa-thumbs-up");
            icon.addClass("fa-thumbs-o-up");

            $.ajax({
                type: "POST",
                url: "CourseLauncher.aspx/btnHelpfulRes",
                data: '{postId:"' + postid + '"}',  
                contentType: "application/json; charset=utf-8",
                success: function (res) {
                    
                    var isInserted = res.d;
                    if(isInserted == 1){
                        icon.removeClass("fa-thumbs-up");
                        icon.addClass("fa-thumbs-o-up");
                        var c = parseInt($(el).parent().find(".jsLikeCount").html().trim());
                        $(el).parent().find(".jsLikeCount").html( c + 1);
                    }else if(isInserted = 4){
                        icon.addClass("fa-thumbs-up");
                        icon.removeClass("fa-thumbs-o-up");
                        var c = parseInt($(el).parent().find(".jsLikeCount").html().trim());
                        $(el).parent().find(".jsLikeCount").html( c - 1);
                    }else if(isInserted = 2){
                    var hndradalertnotmember = $("#<%=hndradalertnotmember.ClientID%>").val();
                    var hndradlaerterror = $("#<%=hndradlaerterror.ClientID%>").val();
                        radalert(hndradalertnotmember, 330, 180,hndradlaerterror, "");
                    }else{
                     var hndradalertsomethingwentwrong = $("#<%=hndradalertsomethingwentwrong.ClientID%>").val();
                    var hndradlaertunknownerror = $("#<%=hndradlaertunknownerror.ClientID%>").val();
                        radalert(hndradalertsomethingwentwrong, 330, 180, hndradlaertunknownerror, "");
                    }
                },
                error: function (res) { 
                    icon.addClass("fa-thumbs-up");
                    icon.removeClass("fa-thumbs-o-up");
                    if(res.status == 401)
                       location.reload();
                },
            });
        }

        function btnReportSubmit(el) {
            if (Page_ClientValidate("vgReport")) {
                var panel = $find("<%= localLoadingPanel.ClientID %>");
                panel.show("pnlCourseForum");
                var postid = $('#<%= hfSelectedPostId.ClientID %>').val();
                var msg = ($('#<%= ReportMessage.ClientID %>').val()).trim();

                $('#panelReport').addClass("display-none");
                $("#panelReport").insertAfter($("#forumMainContainer"));
                 $.ajax({
                    type: "POST",
                    url: "CourseLauncher.aspx/btnSubmitReport",
                    data: '{postId : "' +  postid + '",txtReportMessage : "' + msg + '"}',  
                    contentType: "application/json; charset=utf-8",
                    success: function (response) {
                        var res = response.d;
                        console.log(res);
                       $(".Loading2").hide();
                        hideReplyPanel();
                        hideReportPanel();
                        if (res == 1) {
                        var hndradalertreportsuccess = $("#<%=hndradalertreportsuccess.ClientID%>").val();
                        var hndradalertsuccessmess = $("#<%=hndradalertsuccessmess.ClientID%>").val();
                            radalert(hndradalertreportsuccess, 330, 180, hndradalertsuccessmess, "");
                      }  else if (res == 2) {
                        var hndradalertnotmember = $("#<%=hndradalertnotmember.ClientID%>").val();
                        var hndradalertsuccessmess = $("#<%=hndradalertsuccessmess.ClientID%>").val();
                            radalert(hndradalertnotmember, 330, 180, hndradalertsuccessmess, "");
                        }else 
                        var hndradalerterrorsubreport = $("#<%=hndradalerterrorsubreport.ClientID%>").val();
                        var hndradalertsuccessmess = $("#<%=hndradalertsuccessmess.ClientID%>").val();
                            radalert(hndradalerterrorsubreport, 330, 180,hndradalertsuccessmess, "");
                    },
                    error: function (res) { 
                        if(res.status == 401)
                           location.reload();
                        else if(res.status == 500)
                         var hndradalerttryagain = $("#<%=hndradalerttryagain.ClientID%>").val();
                        var hndradalerterrormess = $("#<%=hndradalerterrormess.ClientID%>").val();
                            radalert(hndradalerttryagain, 330, 180,hndradalerterrormess, "");
                    },
                });
            }

        }

        function btnReplySubmit(el) {

            if (Page_ClientValidate("vgReply")) {
                var panel = $find("<%= localLoadingPanel.ClientID %>");
                panel.show("pnlCourseForum");
                var threadid = $('#hfSelectedThread').val();
                var postid = $('#<%= hfSelectedPostId.ClientID %>').val();
                var msg = ($('#<%= txtReplyMessage.ClientID %>').val()).trim();
                
                                                        
                $('#panelReply').addClass("display-none");
                $("#panelReply").insertAfter($("#forumMainContainer"));
                $.ajax({
                    type: "POST",
                    url: "CourseLauncher.aspx/btnSubmitReply",
                    data: '{postId: "' + postid +'" ,threadId: "' + threadid + '", txtMessage:"' + msg + '"}',  
                    contentType: "application/json; charset=utf-8",
                    success: function (response) {
                        var res = response.d;
                        // $(".Loading2").hide();
                            console.log(res);
                        if (res == 1){
                              $find("<%= rdForumsTopics.ClientID %>").rebind();
                        }
                        else if (res == 2) {
                        var hndradalertnotmember = $("#<%=hndradalertnotmember.ClientID%>").val();
                        var hndradalertsuccessmess = $("#<%=hndradalertsuccessmess.ClientID%>").val();
                            radalert(hndradalertnotmember, 330, 180, hndradalertsuccessmess, "");
                      }  else 
                        var hndradalerterrorsubreport = $("#<%=hndradalerterrorsubreport.ClientID%>").val();
                        var hndradalertsuccessmess = $("#<%=hndradalertsuccessmess.ClientID%>").val();
                            radalert(hndradalerterrorsubreport, 330, 180, hndradalertsuccessmess, "");

                        //hidePanels();
                    },
                    error: function (res) { 
                        if(res.status == 401)
                           location.reload();
                    },
                });
            }
          
        }

          function hidePanels() {
                $(".Loading2").hide();
                hideReplyPanel();
                hideReportPanel();
            }

        var ELEM;
         function btnSendMessage(el) {
         try {
            ELEM = el;  
                debugger;
                var msg = $(el).parent().parent().find(".JSNewPostRadEditor").get(0).control.get_text();
                var html = $(el).parent().parent().find(".JSNewPostRadEditor").get(0).control.get_html();
            
         
                //editor has a default content of break so <br> = empty
                if(msg.trim() == "<br>")
                    msg = "";
                html = encodeURI(html.trim());

                if(msg.trim() != "" || html.indexOf("img") > 0){
                    debugger;
                    var panel = $find("<%= localLoadingPanel.ClientID %>");
                    panel.show("pnlCourseForum");
                    $("#<%= hfMessage.ClientID %>").val(html);
                    $("#<%= hfMessageShort.ClientID %>").val(msg);
                    $(el).parent().parent().find(".btn-send").click();
                      console.log(html  );
                
                }
                else{
                    $(el).parent().parent().parent().find(".errorMessage").removeClass("display-none");
                    return false;
                }
                //closeNewPostWindow();
            } catch (e) {
                console.log(e);
            }
        
        }

        function OnClientCommandExecuting(editor, args)
        {
            debugger;
            var name = args.get_name();
            var val = args.get_value();
            if (name == "Emoticons")
            {
                if(editor.get_contentArea().innerHTML.trim() == "" || editor.get_contentArea().innerHTML.trim() == "<br>"){
                    editor.get_contentArea().innerText = "";
                }
//                if(editor.get_contentArea().innerHTML.trim().length > 0){
//                    setCursorLast(editor.get_contentArea().getAttribute("id"));
//                }
                
                editor.pasteHtml("<img src='" + val + "'" + ' contenteditable="false" class="">&nbsp;');      

                 setCursorLast(editor.get_contentArea().getAttribute("id"));
                //Cancel the further execution of the command as such a command does not exist in the editor command list     
                
                args.set_cancel(true);
            }
            if(name == "Upload"){
                openUploadManager(editor);
                args.set_cancel(true);
            }
        }

         function disableBodyScroll(){
            //$("body").addClass("noscroll");
            var scrollTop = ($('html').scrollTop()) ? $('html').scrollTop() : $('body').scrollTop(); // Works for Chrome, Firefox, IE...
            $('html').addClass('noscroll').css('top',-scrollTop);     
        }

        function enableBodyScroll(){
            //$("body").removeClass("noscroll");
            var scrollTop = parseInt($('html').css('top'));
            $('html').removeClass('noscroll');
            $('html,body').scrollTop(-scrollTop);
        }

        function linkify(text) {
            debugger;
            var urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
            return text.replace(urlRegex, function(url) {
                return '<a href="' + url + '" target="_blank">' + url + '</a>';
            });
        }

        function imagify(text){
        
            //var urlRegex = /(http)?s?:?(\/\/[^"']*\.(?:png|jpg|jpeg|gif|png|svg))/ig;
            var urlRegex = /(Media\/Uploads\/Thread\/)(\[^"']*\.(?:png|jpg|jpeg|gif|png|svg))/ig;
            return text.replace(urlRegex, function(url) {
                return '<img src="' + url + '" class="img-forum js-img-zoom" />';
            });
        }

        function openUploader(){
            var uploader = $("#<%= RadUploader.ClientID %> .ruFileInput");
            uploader.click();
        }

        function RadUploader_fileSelected(sender, args) {
            
            var fileExtention = args.get_fileName().substring(args.get_fileName().lastIndexOf('.') + 1, args.get_fileName().length);
            if (args.get_fileName().lastIndexOf('.') != -1) {//this checks if the extension is correct
                if (sender.get_allowedFileExtensions().toLowerCase().indexOf(fileExtention.toLowerCase()) == -1) {
                var hndradalertinvafile = $("#<%=hndradalertinvafile.ClientID%>").val();
                var hndradlaerterror = $("#<%=hndradlaerterror.ClientID%>").val();
                    radalert(hndradalertinvafile, 330, 180, hndradlaerterror, "");
                }
                else {
                    console.log("File Supported.");
                }
            }
            else {
             var hndradalertinvafile = $("#<%=hndradalertinvafile.ClientID%>").val();
                var hndradlaerterror = $("#<%=hndradlaerterror.ClientID%>").val();
                radalert(hndradalertinvafile, 330, 180, hndradlaerterror, "");
            }
        }

        function RadUploader_ClientFilesUploaded(sender, args){
//            console.log(sender);
//            console.log(args);
//            console.log(args.get_fileInfo().ContentType);
            $("#panelUploadManager #EmptyMessage").hide();
            //$("#<%= RadUploader.ClientID %>").insertAfter($("#forumMainContainer"));
            console.log("file uploaded");

        }

        function showForumLoading(){
             var panel = $find("<%= localLoadingPanel.ClientID %>");
            panel.show("pnlCourseForum");
        }

        function hideForumLoading(){
            clearRadUploader();
            $(".Loading2").hide();
        }

        function clearRadUploader(){
            var uploader = $find("<%= RadUploader.ClientID %>");
            uploader.deleteAllFileInputs();
        }

        function openUploadManager(sender){
            
            //clearRadUploader();
            
            selectedUploadList = $(sender).parent().find(".fakeUploaderList").attr("id");
            $("#panelUploadManager #EmptyMessage").show();
            disableBodyScroll();
            //console.log(selectedUploadList);
            //clearRadUploader();
            
            //var uploader = $find("<%= radUploadManager.ClientID %>");
            //uploader.show();
            $("#panelUploadManager").show();
        }

        function closeUploadManager(){
//            var uploader = $find("<%= radUploadManager.ClientID %>");
//            uploader.hide();
            showUploadedImages();
            enableBodyScroll();
            $("#panelUploadManager").hide();
            
        }
        function saveUploadManager(){
            showUploadedImages();
            enableBodyScroll();
            $("#panelUploadManager").hide();
            $("#<%= btnSaveUploader.ClientID %>").click();
            loadUploadedImages();
        }

        function loadUploadedImages(){
            console.log("load uploaded images");
             $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "GetUploadedForumImages",
                data: '{optional : "1"}' ,
                dataType: 'json',
                success: function (res) {
                    var data = res.d;
                    console.log(data);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var code = XMLHttpRequest.status;
                    if (code == "401")
                        window.location.reload();
                    else
                        alert(XMLHttpRequest.responseText);
                }
            });
        }

        var selectedUploadList;
        function showUploadedImages(){
            
            var uploader = $find(<%= RadUploader.ClientID %>);
            try {    
                var uploaderImages = $find(<%= RadUploader.ClientID %>).getUploadedFiles();
                var count = uploader.get_maxFileCount();
                var id = "#" + selectedUploadList;
                for(var i = 0;i<count; i++){
                    $(id).append("<li> " + uploaderImages[i].get_fileName() + " </li>")
                }

                $(id).removeClass("display-none");
            } catch (e) {
                
            }
        }

        function hideModal(sender){
            $(sender).modal("hide");
        }

        function showYouDontHavePermission() {
            radalert("You don\'t have permission to post in this forum", 330, 180, "Error", "");
            hideForumLoading();
        }

        function setCursorLast(elID) {
            var el = $("#" + elID).get(0);
            var range = document.createRange();
            var sel = window.getSelection();
            range.setStart(el.lastChild, 0);
            range.collapse(true);
            sel.removeAllRanges();
            sel.addRange(range);
            el.focus();
        }
    </script>
</rad:RadScriptBlock>
<rad:RadAjaxLoadingPanel ID="localLoadingPanel" runat="server" CssClass="Loading2"
    Transparency="25" />
<rad:RadAjaxManagerProxy ID="RadAjaxManager1" runat="server">
    <AjaxSettings>
        <rad:AjaxSetting AjaxControlID="btnSend">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="btnSend" />
                <rad:AjaxUpdatedControl ControlID="rdForumsTopics" />
                <rad:AjaxUpdatedControl ControlID="RadUploader"  />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="rdForumsTopics">
            <UpdatedControls>
            <rad:AjaxUpdatedControl ControlID="rdpForums" LoadingPanelID="localLoadingPanel" />   
                <rad:AjaxUpdatedControl ControlID="rdForumsTopics" LoadingPanelID="localLoadingPanel" />                
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnSaveUploader">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="btnSaveUploader" LoadingPanelID="localLoadingPanel" />
            </UpdatedControls>
        </rad:AjaxSetting>
    </AjaxSettings>
</rad:RadAjaxManagerProxy>
<rad:RadWindowManager ID="rwmForum" runat="server" RenderMode="Lightweight" EnableShadow="true"
    Modal="true" VisibleOnPageLoad="false" Behaviors="Close, Move" DestroyOnClose="true"
    Opacity="99" VisibleStatusbar="false" Skin="Bootstrap">
    <Windows>
        <rad:RadWindow ID="radUploadManager" runat="server" Title="Image Upload Manager"
            Width="330px" Height="250px" CssClass="tc-radwindow-1 tc-radwindow-teal title-left overflow-hidden padding-all-md rw-uploadmanager"
            Behaviors="Move">
            <ContentTemplate>
            </ContentTemplate>
        </rad:RadWindow>
    </Windows>
</rad:RadWindowManager>
<div class="row forum-row" runat="server" id="forumMainContainer" clientidmode="Static">
    <div class="panel-group accordion" id="forumAccordion">
        <rad:RadListView ID="rdForumsTopics" runat="server" OnNeedDataSource="rdForumsTopics_NeedDataSource"
            OnItemDataBound="rdForumsTopics_OnItemDataBound">
            <ItemTemplate>
                <div class="panel panel-default">
                    <div class="panel-heading panel-heading-black display-flex btn-flat">
                        <h4 class="panel-title">
                            <p>
                                <asp:Literal ID="ltFocus" runat="server"></asp:Literal>
                                "<%# Eval("ThreadTitle") %>" by
                                <%# Eval("AuthorName") %>
                                on
                                <br />
                                <label class="lblDateCreated"><%# Eval("DateCreated", "{0:MM/dd/yyyy}")%></label>
                                at
                                <label class="lblTimeCreated"><%# Eval("DateCreated", "{0:hh:mm tt}")%></label>
                            </p>
                            <asp:Panel ID="pnlLinkShow" runat="server" CssClass="row">
                                <%-- <div class="col-xs-6">
                                    <a class="pull-left" href="#tab1" style="color: Yellow">Join</a>
                                </div>--%>
                                <div class="col-xs-6 pull-right">
                                    <a id='lnkShow<%# Eval("ThreadId")%>' class="js-collapse-event pull-right pointer isHidden"
                                        data-parent="#forumAccordion" onclick='<%# "forumTabClick(this," +  Eval("ThreadId") + ")"%>'
                                        style="color: Yellow"> <asp:Label runat="server" ID="show1" Text= "<%$ Resources:LocalizedResource, Show %>"></asp:Label></a>
                                    <br />
                                </div>
                                <br />
                            </asp:Panel>
                        </h4>
                        <asp:HiddenField ID="hfThreadId" runat="server" Value='<%# Eval("ThreadId")%>' />
                    </div>
                    <asp:Panel ID="pnlPostContent" runat="server">
                        <div id='tab<%# Eval("ThreadId")%>' class="panel-collapse panel-gray collapse">
                            <div class="panel-body panel-post" style="padding-top: 5px;">
                                <div class="authorDetail">
                                    <asp:Image ID="imgCurrentUserImage" ImageUrl="default-user.png"
                                        runat="server" onerror='this.src="default-user.png"'
                                        CssClass="img-circle img-sm" AlternateText="User" />
                                    <asp:Label ID="lblUserFullName" Text="text" runat="server" CssClass="bold" />
                                    <asp:Literal ID="ltDate" Text="01/01/1991" runat="server" />
                                    <br />
                                    <asp:Label ID="Label1" Text="Enter you message" runat="server" CssClass="errorMessage display-none"
                                        ForeColor="Red" />
                                    <div class="display-inline-flex margin-all-10 width-fill">
                                        <rad:RadEditor runat="server" ID="NewPostRadEditor" OnClientCommandExecuting="OnClientCommandExecuting"
                                            CssClass="JSNewPostRadEditor smile-icon radeditor-as-textbox" EnableResize="false" 
                                            Height="14px" RenderMode="Lightweight" AutoResizeHeight="true" EditModes="Design"
                                            ContentAreaMode="Div" >
                                            <Tools>
                                                <rad:EditorToolGroup dockingzone="Left">
                                                    <rad:EditorDropDown Name="Emoticons" Text="Emoticons" ItemsPerRow="3" PopupWidth="60px"
                                                        PopupHeight="117px">
                                                        <rad:EditorDropDownItem Name="<img src='Media/Emoticons/smil1.gif' />" Value="Media/Emoticons/smil1.gif" />
                                                        <rad:EditorDropDownItem Name="<img src='Media/Emoticons/smil2.gif' />" Value="Media/Emoticons/smil2.gif" />
                                                        <rad:EditorDropDownItem Name="<img src='Media/Emoticons/smil3.gif' />" Value="Media/Emoticons/smil3.gif" />
                                                        <rad:EditorDropDownItem Name="<img src='Media/Emoticons/smil4.gif' />" Value="Media/Emoticons/smil4.gif" />
                                                        <rad:EditorDropDownItem Name="<img src='Media/Emoticons/smil8.gif' />" Value="Media/Emoticons/smil8.gif" />
                                                        <rad:EditorDropDownItem Name="<img src='Media/Emoticons/smil6.gif' />" Value="Media/Emoticons/smil6.gif" />
                                                        <rad:EditorDropDownItem Name="<img src='Media/Emoticons/smil7.gif' />" Value="Media/Emoticons/smil7.gif" />
                                                        <rad:EditorDropDownItem Name="<img src='Media/Emoticons/smil9.gif' />" Value="Media/Emoticons/smil9.gif" />
                                                        <rad:EditorDropDownItem Name="<img src='Media/Emoticons/smil11.gif' />" Value="Media/Emoticons/smil11.gif" />
                                                    </rad:EditorDropDown>
                                                </rad:EditorToolGroup>
                                                <rad:EditorToolGroup dockingzone="Right">
                                                    <rad:EditorTool Name="Upload" />
                                                </rad:EditorToolGroup>
                                            </Tools>
                                        </rad:RadEditor>
                                    </div>
                                    <br />
                                    <asp:Button ID="btnSend" runat="server" Text="Send" CssClass="btn btn-teal btn-sm btn-flat pull-right"
                                        OnClientClick="btnSendMessage(this); return false" />
                                    <asp:Button ID="btnSendReal" runat="server" Text="Send" CssClass="btn-send display-none"
                                        OnClick="btnSend_Click" />
                                    <br />
                                    <br />
                                    <div class="horizontal-dotted-gray" style="margin-left: -15px; margin-right: -25px;">
                                        <ul class="fakeUploaderList display-none" id='fakeUploaderList<%# Eval("ThreadId")%>'>
                                            <li><span class="filename"></span></li>
                                        </ul>
                                    </div>
                                    <br />
                                </div>
                                <rad:RadListView ID="rdThreadPost" runat="server" ItemPlaceholderID="phThreads" CssClass='rdPost<%# Eval("ThreadId")%>'>
                                    <ItemTemplate>
                                        <asp:Panel ID="pnlPostItem" runat="server" CssClass="margin-bottom-10px">
                                            <div class="row">
                                                <div class="col-xs-2">
                                                    <asp:Image ID="uImg" runat="server" ImageUrl='<%# Eval("UserImageUrl", "{0}") %>'
                                                        CssClass="img-circle img-sm" AlternateText="User Image" onerror='this.src="default-user.png"' />
                                                </div>
                                                <div class="col-xs-4">
                                                    <div class="row">
                                                        <div class="col-xs-offset-1 col-xs-11 no-paddings">
                                                            <asp:Label ID="lblName" runat="server" CssClass="cr-name bold"><i class="text-normal" id='name<%# Eval("PostId")%>'><%# Eval("AuthorName") %></i></asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6">
                                                    <p class="pull-right smaller cr-name">
                                                        <i id='date<%# Eval("PostId")%>'>
                                                            <span class="datePosted">
                                                            <%# Eval("DatePosted", "{0:MM/dd/yyy HH:mm:ss tt}")%></span></i></p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class='talk-bubble tri-right left-top <%# Convert.ToInt32(Eval("QuotePostID")) != 0 ? "" : "display-none" %>'
                                                        style="display: block; width: fit-content; padding: 5px 10px;">
                                                        <div>
                                                            <p>
                                                                <b>-<%# Eval("OriginalPostAuthor")%></b><br>
                                                                <i>
                                                                    <p class="JSDecodeComment">
                                                                        <%# Eval("OriginalPostComment")%>
                                                                    </p>
                                                                </i>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <p id='comment<%# Eval("PostId")%>' class="JSDecodeComment">
                                                        <%# Eval("PostComment").ToString() %>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="postContainer margin-top-10px">
                                                <div class="row">
                                                    <div class="display-inline-flex">
                                                        <asp:LinkButton ID="btnQuote" OnClientClick='<%# "showReply(this," + Eval("PostId") + "); return false;" %>'
                                                            runat="server" CssClass="margin-side-10">
                                                            <i class="fa fa-reply teal medium"></i>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnReport" runat="server" OnClientClick='<%# "showReport(this," + Eval("PostId") + "); return false;" %>'
                                                            Text="Report" CssClass="margin-side-10">
                                                              <i class="fa fa-flag-o teal medium"></i>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnHelpful" runat="server" OnClientClick='<%# "fakeClickBtnHelpful(this," + Eval("PostId") + "); return false;" %>'
                                                            CssClass='margin-side-10'>
                                                           <i class='fa medium teal <%# Convert.ToInt32(Eval("Helpful")) != 0 ? "fa-thumbs-o-up" : "fa-thumbs-up "%>' ></i>
                                                        </asp:LinkButton>
                                                        <asp:Label ID="lblLikeCount" Text='<%# Eval("HelpfulCount") %>' runat="server" CssClass="jsLikeCount"></asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </ItemTemplate>
                                    <LayoutTemplate>
                                        <asp:Panel ID="forumsPanel" runat="server">
                                            <div class="thread">
                                                <asp:PlaceHolder ID="phThreads" runat="server"></asp:PlaceHolder>
                                            </div>
                                            <div>
                                                <%-- <asp:LinkButton ID="btnViewAll" runat="server" Text="View all" CssClass="pull-right link-default"></asp:LinkButton>--%>
                                            </div>
                                        </asp:Panel>
                                    </LayoutTemplate>
                                    <EmptyDataTemplate>
                                        <p>
                                           <asp:Label runat="server" ID="NPTdisp" Text= "<%$ Resources:LocalizedResource, NoPostToDisplay %>"></asp:Label>
                                        </p>
                                    </EmptyDataTemplate>
                                </rad:RadListView>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </ItemTemplate>
            <EmptyDataTemplate>
                <p class="margin-side-10">
                  <asp:Label runat="server" ID="Nothread" Text= "<%$ Resources:LocalizedResource, ThisCourseHasNoThreads %>"></asp:Label>  
                </p>
            </EmptyDataTemplate>
        </rad:RadListView>
    </div>
</div>
<asp:HiddenField ID="hfSelectedPostId" runat="server" Value="0" />
<asp:HiddenField ID="hfCurrentCIM" runat="server" />
<asp:HiddenField ID="hfMessage" runat="server" Value="" />
<asp:HiddenField ID="hfMessageShort" runat="server" Value="" />
<div class="panel panel-default display-none" id="panelReport" runat="server" clientidmode="Static">
    <div class="panel-heading panel-heading-medium">
        <asp:Label runat="server" ID="Repor" Text= "<%$ Resources:LocalizedResource, Report %>"></asp:Label>
        <button id="btnCloseReport" class="btn-transparent pull-right bold" onclick="hideReportPanel(); return false;"
            style="padding-right: 10px">
            x</button>
    </div>
    <div class="panel-body">
        <asp:TextBox ID="ReportMessage" runat="server" TextMode="MultiLine" Rows="4" Columns="4"
            ValidationGroup="vgReport" CssClass="form-control" placeholder="Message...">
        </asp:TextBox>
        <asp:RequiredFieldValidator ID="rfvReport" runat="server" ControlToValidate="ReportMessage"
            Display="Dynamic" SetFocusOnError="true" ForeColor="Red" ValidationGroup="vgReport"><asp:Label runat="server" ID="MessEnter" Text= "<%$ Resources:LocalizedResource, EnterYourMessage %>"></asp:Label></asp:RequiredFieldValidator>
        <br />
        <asp:LinkButton ID="btnSubmitReport" ValidationGroup="vgReport" runat="server" Text="Submit"
            OnClientClick="btnReportSubmit(this); return false;" CssClass="btn btn-teal btn-thin btn-sm btn-flat pull-right margin-side-5"></asp:LinkButton>
        <button id="btnCancelReport" onclick="hideReportPanel(); return false;" class="btn btn-gray btn-thin btn-sm btn-flat pull-right margin-side-5">
            <asp:Label runat="server" ID="cancel" Text= "<%$ Resources:LocalizedResource, Cancel %>"></asp:Label> </button>
    </div>
</div>
<div class="panel panel-default display-none" id="panelReply">
    <div class="panel-heading panel-heading-medium">
        <asp:Label runat="server" ID="Plyre" Text= "<%$ Resources:LocalizedResource, Reply %>"></asp:Label>
        <button id="btnCloseReply" class="btn-transparent pull-right bold" onclick="hideReplyPanel(); return false;"
            style="padding-right: 10px">
            x</button>
    </div>
    <div class="panel-body">
        <div class="talk-bubble tri-right left-top">
            <div class="talktext">
                <p>
                </p>
            </div>
        </div>
        <asp:TextBox ID="txtReplyMessage" runat="server" TextMode="MultiLine" Rows="4" Columns="4"
            CssClass="form-control" placeholder="Message..." ValidationGroup="vgReply">
        </asp:TextBox>
        <asp:RequiredFieldValidator ID="rfvReply" runat="server" ControlToValidate="txtReplyMessage"
            Display="Dynamic" SetFocusOnError="true" ForeColor="Red" ValidationGroup="vgReply"><asp:Label runat="server" ID="Entmessa" Text= "<%$ Resources:LocalizedResource, EnterYourMessage %>"></asp:Label></asp:RequiredFieldValidator>
        <br />
        <%--OnClick="btnReplySubmit"--%>
        <asp:LinkButton ID="btnSubmitReply" runat="server" Text="Submit" OnClientClick="btnReplySubmit(this); return false;"
            CssClass="btn btn-teal btn-thin btn-sm btn-flat pull-right margin-side-5" ValidationGroup="vgReply"></asp:LinkButton>
        <button id="Button2" onclick="hideReplyPanel(); return false;" class="btn btn-gray btn-thin btn-sm btn-flat pull-right margin-side-5">
            Cancel</button>
    </div>
</div>
<div id="imageModal" class="modal top-9999">
    <span class="close">&times;</span>
    <img class="modal-content img-large" id="img01" />
</div>
<div class="modal top-9999" id="panelUploadManager">
    <div class="modal-dialog">
        <div class="modal-content" style="height: 35vh;">
            <div class="modal-header">
               <asp:Label runat="server" ID="Upmanager" Text= "<%$ Resources:LocalizedResource, UploadManager %>"></asp:Label> 
                <button id="Button1" class="btn-transparent pull-right bold" onclick="closeUploadManager(); return false;"
                    style="padding-right: 10px">
                    x</button>
            </div>
            <div class="modal-body">
                <div id="uploaderContent" class="padding-md">
                    <p id="EmptyMessage">
                     <asp:Label runat="server" ID="Noim" Text= "<%$ Resources:LocalizedResource, NoImageUploaded %>"></asp:Label>
                    </p>
                    <rad:RadAsyncUpload ID="RadUploader" runat="server" MultipleFileSelection="Disabled"
                       ViewStateMode="Enabled" MaxFileInputsCount="10"
                        ControlObjectsVisibility="None" HideFileInput="false" CssClass="hide-select center"
                        MaxFileSize="20000000" AllowedFileExtensions="jpeg,jpg,gif,png" TemporaryFileExpiration="00:10:00"
                        OnClientFileSelected="RadUploader_fileSelected" OnClientFilesUploaded="RadUploader_ClientFilesUploaded" TemporaryFolder="Media/Uploads/Thread/Temporary" >
                    </rad:RadAsyncUpload>
                </div>
            </div>
            <div class="modal-footer">
                <div id="uploaderButtons">
                    <asp:Button ID="btnOpenUploader" Text="Select" runat="server" CssClass="btn btn-md btn-teal pull-left btn-flat abs-bottom"
                        OnClientClick="openUploader(); return false;" />
                    <asp:Button ID="btnCloseFake" Text="Save" runat="server" CssClass="btn btn-md btn-teal pull-right btn-flat abs-bottom"
                        OnClientClick="saveUploadManager(); return false;"/>
                    <asp:Button ID="btnSaveUploader" Text="Save" runat="server" CssClass="display-none"
                       OnClick="btnUploaderSave_Click"/>
                </div>
            </div>
        </div>
    </div>
</div>
<asp:HiddenField runat="server" ID ="hndradalertnotmember" Value="You're not a member of this thread." /> 
<asp:HiddenField runat="server" ID ="hndradlaerterror" Value="error" /> 
<asp:HiddenField runat="server" ID ="hndradalertsomethingwentwrong" Value="Something went wrong. Please try again." /> 
<asp:HiddenField runat="server" ID ="hndradlaertunknownerror" Value="Unknown Error" />
<asp:HiddenField runat="server" ID ="hndradalertreportsuccess" Value="Report was successfully submitted" />
<asp:HiddenField runat="server" ID ="hndradalerterrorsubreport" Value="Error in submitting your report. Please contact your administrator." />
<asp:HiddenField runat="server" ID ="hndradalertsuccessmess" Value="Success Message" /> 
<asp:HiddenField runat="server" ID ="hndradalerttryagain" Value="Something went while submitting your report. Please try again." /> 
<asp:HiddenField runat="server" ID ="hndradalerterrormess" Value="Error Message" /> 
<asp:HiddenField runat="server" ID ="hndradalertinvafile" Value="Invalid File Type Allowed Files are JPG, GIF, PNG." /> 
<asp:HiddenField runat="server" ID ="hfShow" Value="Show" /> 
<asp:HiddenField runat="server" ID ="hfHide" Value="Hide" /> 