﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using TranscomUniversityV3Model;
using Telerik.Web.UI;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.IO;
using System.Collections;

public partial class UserControl_Pages_CourseForumUserCtrl : System.Web.UI.UserControl
{
    //protected void Page_Init(object sender, EventArgs e)
    //{
    //    btnSubmitReply.Click += new System.EventHandler(this.btnReplySubmit);
    //}

    protected void Page_Load(object sender, EventArgs e)
    {
        //        ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", @"
        //            $('.Loading2').hide();
        //        ", true);
        if(!IsPostBack)
         hfCurrentCIM.Value = DataHelper.GetCurrentUserCIM();
    }

    protected void rdForumsTopics_NeedDataSource(object sender, EventArgs e)
    {
        int CourseID = Convert.ToInt16(Utils.Decrypt(Request.QueryString["CourseID"]));
        rdForumsTopics.DataSource = DataHelper.courseForumTopics(CourseID);
        RadScriptManager.RegisterStartupScript(Page, Page.GetType(), "hidePanels", Utils.callClientScript("hidePanels"), true);

    }

    protected void btnReplySubmit(object sender, EventArgs e)
    {
        int _postid = Convert.ToInt32(hfSelectedPostId.Value.Trim());
        string _txtReportMessage = txtReplyMessage.Text.Trim();
        int _threadid = Convert.ToInt32(((HiddenField)(this.Parent.Parent).FindControl("hfSelectedThread")).Value.Trim());
        int usercim = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
        string reportmessage = _txtReportMessage;
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            pr_TranscomUniversity_InsertPost_Result res = db.pr_TranscomUniversity_InsertPost(_threadid, _postid, usercim, _txtReportMessage).SingleOrDefault();

            if (res.Response == 1)
            {
                RadListView rdPosts = (RadListView)(this).FindControl("rdThreadPost");
                rdPosts.Rebind();
            }

        }
    }

    protected void rdForumsTopics_OnItemDataBound(object sender, RadListViewItemEventArgs e)
    {
        var dataItem = ((RadListViewDataItem)e.Item).DataItem;
        //DataRowView rowView = (DataRowView)dataItem;
        //String thread = rowView["ThreadId"].ToString();
        if (e.Item is RadListViewDataItem)
        {
            RadListViewDataItem item = e.Item as RadListViewDataItem;

            int threadId = Convert.ToInt32(DataBinder.Eval(item.DataItem, "ThreadId").ToString());
            int usercim = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
            int courseID = Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"]));
            string name = DataHelper.getUserName(usercim);

            RadListView posts = e.Item.FindControl("rdThreadPost") as RadListView;
            Panel pnlLinkSHow = e.Item.FindControl("pnlLinkShow") as Panel;
            Panel pnlPostContent = e.Item.FindControl("pnlPostContent") as Panel;
            Image imgCurrentUserImage = e.Item.FindControl("imgCurrentUserImage") as Image;
            Label lblUserFullName = e.Item.FindControl("lblUserFullName") as Label;
            Literal ltDate = e.Item.FindControl("ltDate") as Literal;

            imgCurrentUserImage.ImageUrl = "" + DataHelper.getCurrentUserImg();
            lblUserFullName.Text = name;
            ltDate.Text = string.Format("{0:MM/dd/yyyy}", DataHelper.serverDate());

            if (DataHelper.isThreadMember(usercim, courseID))
            {
                pnlLinkSHow.Visible = true;
                pnlPostContent.Visible = true;
            }
            else
            {
                pnlLinkSHow.Visible = false;
                pnlPostContent.Visible = false;
            }

            posts.DataSource = DataHelper.threadPosts(threadId, usercim);
            posts.DataBind();
        }
    }

    string imageMessage;

    protected void btnSend_Click(object sender, EventArgs e)
    {

        int isMessageValid = uploadedFileIsValid(hfMessageShort.Value);
        if (isMessageValid == 0)
        {

            saveToTragetFolder();

            int userCim = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
            pr_TranscomUniversity_InsertPost_Result res = null;
            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                HiddenField selectedThreadID = (HiddenField)this.Parent.FindControl("hfSelectedThread");
                int? threadId = Convert.ToInt32(selectedThreadID.Value);
                int? postId = Convert.ToInt32(hfSelectedPostId.Value);
                string message = hfMessage.Value + imageMessage;


                res = db.pr_TranscomUniversity_InsertPost(threadId, postId, userCim, message).SingleOrDefault();
            }

            if (res.Response == 2)
            {
                string directoryPath = Server.MapPath(string.Format("~/{0}/", "Media/Uploads/Thread/" + userCim + "/"));

                if (Directory.Exists(directoryPath))
                    Directory.Delete(directoryPath, true);

                //rwmForum.RadAlert("You don\\'t have permission to post in this forum.", 330, 180, "Error", "");
                RadScriptManager.RegisterStartupScript(Page, Page.GetType(), "showYouDontHavePermission", Utils.callClientScript("showYouDontHavePermission"), true);
                return;
            }
            else {
                
                RadUploader.UploadedFiles.Clear();
                rdForumsTopics.Rebind();
            }
            


        }
        else if (isMessageValid == 1)
        {
            //uploaded files too large
            rwmForum.RadAlert("Uploaded Files are too large.", 330, 180, "Files too large", "");
        }
        else if (isMessageValid == 2)
        {
            rwmForum.RadAlert("Message too long", 330, 180, "Message too long", "");
        }
        else if (isMessageValid == 3)
        {
            rwmForum.RadAlert("Something went wrong please try again.", 330, 180, "Error", "");
        }

        RadScriptManager.RegisterStartupScript(Page, Page.GetType(), "hideForumLoading", Utils.callClientScript("hideForumLoading"), true);
    }

    protected int uploadedFileIsValid(string message)
    {
        long maxFileSize;
        long maxCharacter;
        //0 valid
        //1 file too large
        //2 message too long
        //3 error
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            int CourseID = Convert.ToInt16(Utils.Decrypt(Request.QueryString["CourseID"]));
            var settingsId = db.tbl_TranscomUniversity_Rlt_CourseSetting.Where(s => s.CourseID == CourseID).Select(s => s.CourseSettingID).SingleOrDefault();
            var forumSettings = db.tbl_TranscomUniversity_Rlt_CourseSettingForumWidget.Where(f => f.CourseSettingID == settingsId).SingleOrDefault();
            maxCharacter = (long)forumSettings.MaxChars;
            maxFileSize = (long)forumSettings.MaxUploadSize;
        }

        if (message.Length > maxCharacter)
        {
            return 2;
        }
        else
        {
            long currFilesSize = 0;
            //if with image then save the temporary image to traget folder
            for (int i = 0; i < RadUploader.UploadedFiles.Count; i++)
            {
                try
                {

                    foreach (UploadedFile file in RadUploader.UploadedFiles)
                    {
                        currFilesSize += file.ContentLength;
                        if (currFilesSize > maxFileSize)
                        {
                            return 1;//files too large
                        }

                    }
                }
                catch (Exception ex)
                {
                    string err = ex.Message;
                    return 3;
                }
            }
        }
        return 0;
    }

    protected void saveToTragetFolder()
    {
        // Loops through all files and saves to a specified folder path
        try
        {
            string userCIM = DataHelper.GetCurrentUserCIM();
            string uploadFolder;
            if (HttpContext.Current.Request.Url.Host == "localhost")
                uploadFolder = Request.PhysicalApplicationPath + "Media\\Uploads\\Thread\\" + userCIM + "\\";
            else
                uploadFolder = HttpRuntime.AppDomainAppPath + "Media\\Uploads\\Thread\\" + userCIM + "\\";

            string directoryPath = Server.MapPath(string.Format("~/{0}/", "Media/Uploads/Thread/" + userCIM + "/"));


            if (!Directory.Exists(directoryPath))
                Directory.CreateDirectory(directoryPath);

            //imageMessage += "<div class=\"forum-img-group\">";
            imageMessage += "<br>";
            foreach (UploadedFile file in RadUploader.UploadedFiles)
            {
                Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                string filename = unixTimestamp + file.FileName;
                file.SaveAs(uploadFolder + filename, true);

                imageMessage += "<img src=\"Media/Uploads/Thread/"+ userCIM + "/" + filename + "\" class=\"img-forum js-img-zoom\" />";
                //imageMessage += ("Media/Uploads/Thread/" + filename);
            }
            //imageMessage += "</div>";

        }
        catch (Exception ex)
        {
            string err = ex.Message;
        }
    }

    protected void btnUploaderSave_Click(object sender, EventArgs e)
    {

        try {
            ArrayList UploadedForumImages = new ArrayList();
            Session["ForumUploadedImages"] = null;
            string uploadFolder;
            if (HttpContext.Current.Request.Url.Host == "localhost")
                //uploadFolder = Request.PhysicalApplicationPath + "Media\\Uploads\\Thread\\Temporary\\" + DataHelper.GetCurrentUserCIM() + "\\";
                uploadFolder = Request.PhysicalApplicationPath + "Media\\Uploads\\Thread\\Temporary\\";
            else
                //uploadFolder = HttpRuntime.AppDomainAppPath + "Media\\Uploads\\Thread\\Temporary\\" + DataHelper.GetCurrentUserCIM()+ "\\";
                uploadFolder = HttpRuntime.AppDomainAppPath + "Media\\Uploads\\Thread\\Temporary\\";


            //string directoryPath = Server.MapPath(string.Format("~/{0}/", "Media/Uploads/Thread/Temporary/" + DataHelper.GetCurrentUserCIM())) + "/";
            string directoryPath = Server.MapPath(string.Format("~/{0}/", "Media/Uploads/Thread/Temporary/"));


            if (!Directory.Exists(directoryPath))
                Directory.CreateDirectory(directoryPath);
            //    Directory.Delete(directoryPath, true);

            //Directory.CreateDirectory(directoryPath);


            foreach (UploadedFile file in RadUploader.UploadedFiles)
            {
                string tempImage = uploadFolder + file.FileName;
                //file.SaveAs(tempImage, true);
                UploadedForumImages.Add(tempImage);
            }

            //RadUploader.UploadedFiles.Clear();
            Session["ForumUploadedImages"] = UploadedForumImages;

            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", Utils.callClientScript("loadUploadedImages"), true);
            
        }
        catch (FileNotFoundException fnfe)
        {
            
        }
        
    }

    //public string decodeUri(string text) {
    //    return HttpUtility.UrlDecode(text);
    //}
}