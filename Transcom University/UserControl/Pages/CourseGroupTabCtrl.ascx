﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CourseGroupTabCtrl.ascx.cs"
    Inherits="CourseGroupTabCtrl" %>
<asp:Panel ID="pnlMainGroupUC" runat="server" ClientIDMode="Static">
    <rad:RadScriptBlock ID="gtRadScriptBlock1" runat="server">
        <script type="text/javascript">

            function pageLoad() {
                if ($("#" + "<%= rgCourseGroup.ClientID %>").length > 0) {
                    var grid = $find("<%= rgCourseGroup.ClientID %>");
                    var columns = grid.get_masterTableView().get_columns();
                    for (var i = 0; i < columns.length; i++) {
                        columns[i].resizeToFit();
                    }
                }
            }

            function toggleConfirmRemoveGroup() {
                var groupname = $("#" + '<%= hfGroupName.ClientID %>').val();
                $("#modalGroupName").html(groupname);
                $("#confirmationModalGroup").modal("toggle");

            }

            function toggleConfirmRemoveUser() {
                var username = $("#" + '<%= hfName.ClientID %>').val();
                $("#modalUserName").html(username);
                $("#confirmationModalUser").modal("toggle");
            }

            function openMembersList() {

                var radwindow = $find('<%=radMembersWindow.ClientID %>');
                radwindow.show();

                $('#' + '<%=rgMembers.ClientID %>').hide();
            }

            function btnCloseMembersList_Click() {
                var radwindow = $find('<%=radMembersWindow.ClientID %>');
                radwindow.close();
                hideMembersListLoad();
            }

            function openCourseSelectAssign() {
                selectedItems = [];
                var radwindow = $find('<%=rdCourseListAssign.ClientID %>');
                radwindow.show();
            }

            function showGroupContextMenu(e, gid, groupname) {

                $("#" + "<%= hfGroupID.ClientID %>").val(gid);
                $("#" + "<%= hfGroupName.ClientID %>").val(groupname);

                var contextMenu = $find("<%= rcmGroupMenu.ClientID %>");

                $telerik.cancelRawEvent(e);

                if ((!e.relatedTarget) || (!$telerik.isDescendantOrSelf(contextMenu.get_element(), e.relatedTarget))) {
                    contextMenu.show(e);
                }
            }

            function showUserContextMenu(e, gid, name) {

                $("#" + "<%= hfCIM.ClientID %>").val(gid);
                $("#" + "<%= hfName.ClientID %>").val(name);

                var contextMenu = $find("<%= rcmUserMenu.ClientID %>");

                if ((!e.relatedTarget) || (!$telerik.isDescendantOrSelf(contextMenu.get_element(), e.relatedTarget))) {
                    contextMenu.show(e);
                }

                $telerik.cancelRawEvent(e);

            }

            function setMembersList(sender) {
                var hfValue = $("#" + "<%= hfGroupName.ClientID %>").val();

                if (hfValue) {
                    sender.set_title(hfValue + " - View Members");
                }
                else {
                    sender.set_title("Group Name");

                }
                var panel = $find('<%= localLoadingPanel.ClientID  %>');
                panel.show("<%= pnlMembersList.ClientID%>");

                $("#" + '<%= btnLoadMembers.ClientID %>').click();

            }

            function hideMembersListLoad() {
                $(".Loading2").hide();
                clearSelectedItems();
            }

            function confirmationModal(title, message) {
                $("#confirmationTitle").html(title);
                $("#confirmationBody").html(message);
                $("#confirmationModal").modal("toggle");
            }

            var selectedItems = [];

            function gtCourseRowSelected1(sender, args) {
                var id = args.getDataKeyValue("CourseID");

                if (!selectedItems.includes(id)) {
                    selectedItems.push(id);
                }
                setSelectedItems1();

            }

            function gtCourseRowDeselected1(sender, args) {
                var id = args.getDataKeyValue("CourseID");

                if (selectedItems.includes(id)) {
                    selectedItems.splice(selectedItems.indexOf(id), 1);
                }
                setSelectedItems1();
            }

            function gtCourseRowCreated1(sender, args) {
                var id = args.getDataKeyValue("CourseID");
                if (selectedItems.includes(id) && selectedItems.length > 0) {
                    args.get_gridDataItem().set_selected(true);
                }
            }

            function setSelectedItems1() {
                var elements = selectedItems;
                var outputStr = "";
                for (var i = 0; i < elements.length; i++) {
                    outputStr += elements[i] + ",";

                }
                console.log(outputStr);
                $("#" + "<%= hfGtSelectedCourse.ClientID %>").val(outputStr);
            }

            function clearSelectedItems1() {
                selectedItems = [];
                $("#" + "<%= hfGtSelectedCourse.ClientID %>").val("");
                var grid = $find("<%= rgCourseListAssign.ClientID %>");
                grid.clearSelectedItems();
            }

            function refreshCount(sender, args) {

                var grid = $find("<%= rgCourseGroup.ClientID %>");
                var count = grid.get_masterTableView().get_dataItems().length;

                if (count > 1) {
                    $("#" + "<%= lblNumberOfGroup.ClientID %>").html("There are " + count + " group enrolled to this course");
                } else {
                    $("#" + "<%= lblNumberOfGroup.ClientID %>").html("There is " + count + " group enrolled to this course");
                }
            }

            function gtshowTransaction() {
                var radwindow = $find('<%=gtrdTransactionSummary.ClientID %>');
                radwindow.show();
            }

            function gtbtnTransactionsList_Click() {
                var radwindow = $find('<%=gtrdTransactionSummary.ClientID %>');
                radwindow.close();
            }

        </script>
    </rad:RadScriptBlock>
    <rad:RadAjaxLoadingPanel ID="localLoadingPanel" runat="server" CssClass="Loading2"
        MinDisplayTime="1000" Transparency="25" />
    <rad:RadAjaxLoadingPanel ID="noLoadingPanel" runat="server" CssClass="Loading-Empty"
        Transparency="25" />
    <rad:RadAjaxManagerProxy ID="gtAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <rad:AjaxSetting AjaxControlID="rcmGroupMenu">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="rcmGroupMenu" />
                    <rad:AjaxUpdatedControl ControlID="btnAssignToNew" />
                    <rad:AjaxUpdatedControl ControlID="btnAssignToAnother" />
                    <rad:AjaxUpdatedControl ControlID="rdCourseListAssign" LoadingPanelID="noLoadingPanel" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="rcmUserMenu">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="rcmUserMenu" LoadingPanelID="noLoadingPanel" />
                    <rad:AjaxUpdatedControl ControlID="rgMembers" LoadingPanelID="localLoadingPanel" />
                    <rad:AjaxUpdatedControl ControlID="rgCourseListAssign" LoadingPanelID="localLoadingPanel" />
                    <rad:AjaxUpdatedControl ControlID="rdCourseListAssign" LoadingPanelID="noLoadingPanel" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="rcbCategory">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="rcbCategory" LoadingPanelID="noLoadingPanel" />
                    <rad:AjaxUpdatedControl ControlID="ddtSubcategory" LoadingPanelID="localLoadingPanel" />
                    <rad:AjaxUpdatedControl ControlID="rcbCourse" LoadingPanelID="localLoadingPanel" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="ddtSubcategory">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="ddtSubcategory" LoadingPanelID="noLoadingPanel" />
                    <rad:AjaxUpdatedControl ControlID="rcbCourse" LoadingPanelID="localLoadingPanel" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnLoadMembers">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="btnLoadMembers" LoadingPanelID="noLoadingPanel" />
                    <rad:AjaxUpdatedControl ControlID="pnlMembersList" LoadingPanelID="localLoadingPanel" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnRemoveGroup">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="btnRemoveGroup" LoadingPanelID="noLoadingPanel" />
                    <rad:AjaxUpdatedControl ControlID="pnlGroups" LoadingPanelID="localLoadingPanel" />
                    <rad:AjaxUpdatedControl ControlID="lblNumberOfGroup" LoadingPanelID="noLoadingPanel" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="rgCourseListAssign">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="rgCourseListAssign" LoadingPanelID="localLoadingPanel" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnCloseMembersList">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="btnCloseMembersList" LoadingPanelID="noLoadingPanel" />
                    <rad:AjaxUpdatedControl ControlID="radMembersWindow" LoadingPanelID="noLoadingPanel" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnAssignGroup">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="btnAssignGroup" LoadingPanelID="noLoadingPanel" />
                    <rad:AjaxUpdatedControl ControlID="rgCourseListAssign" LoadingPanelID="localLoadingPanel" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="gtbtnTransactionsList">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="gtbtnTransactionsList" LoadingPanelID="noLoadingPanel" />
                </UpdatedControls>
            </rad:AjaxSetting>
        </AjaxSettings>
    </rad:RadAjaxManagerProxy>
    <rad:RadWindowManager ID="rwmGroup" RenderMode="Lightweight" EnableShadow="true"
        Modal="true" VisibleOnPageLoad="false" Behaviors="Close, Move" DestroyOnClose="true"
        Opacity="99" runat="server" VisibleStatusbar="false">
        <Windows>
            <rad:RadWindow RenderMode="Lightweight" ID="rdCourseListAssign" Behaviors="Close,Move"
                CssClass="tc-radwindow-1" VisibleOnPageLoad="false" runat="server" Width="800px"
                Height="550px" OnClientClose="hideMembersListLoad" OnClientBeforeShow="clearSelectedItems1">
                <ContentTemplate>
                    <asp:Panel ID="Panel2" runat="server">
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td>
                                        <rad:RadGrid RenderMode="Lightweight" ID="rgCourseListAssign" runat="server" AllowSorting="True"
                                            CssClass="GridLess" AllowMultiRowSelection="false" AllowPaging="true" OnNeedDataSource="rgCourseListAssign_OnNeedDataSource">
                                            <MasterTableView AutoGenerateColumns="false" TableLayout="Auto" HeaderStyle-ForeColor="Teal"
                                                DataKeyNames="CourseID" ClientDataKeyNames="CourseID">
                                                <Columns>
                                                    <rad:GridClientSelectColumn UniqueName="cboxCourse">
                                                    </rad:GridClientSelectColumn>
                                                    <rad:GridBoundColumn DataField="CourseID" HeaderText="<%$ Resources:LocalizedResource, CourseId %>" UniqueName="CourseID">
                                                    </rad:GridBoundColumn>
                                                    <rad:GridBoundColumn DataField="Title" HeaderText="<%$ Resources:LocalizedResource, Title %>" UniqueName="Title">
                                                    </rad:GridBoundColumn>
                                                    <rad:GridBoundColumn DataField="Description" HeaderText="<%$ Resources:LocalizedResource, Description %>" UniqueName="Description">
                                                    </rad:GridBoundColumn>
                                                </Columns>
                                            </MasterTableView>
                                            <ClientSettings>
                                                <Selecting AllowRowSelect="true" />
                                                <Resizing AllowResizeToFit="true" ResizeGridOnColumnResize="false" />
                                                <Scrolling AllowScroll="true" UseStaticHeaders="false" ScrollHeight="" />
                                                <ClientEvents OnRowSelected="gtCourseRowSelected1" OnRowDeselected="gtCourseRowDeselected1"
                                                    OnRowCreated="gtCourseRowCreated1" />
                                            </ClientSettings>
                                        </rad:RadGrid>
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot class="footer-bottom">
                                <tr>
                                    <td style="padding-top: 10px;">
                                        <asp:LinkButton ID="btnAssignGroup" runat="server" OnCommand="btnAssignGroup_Click"
                                            ToolTip="Assign Group" CommandArgument="none" Text="Assign" CssClass="btn tc-btn-md btn-teal btn-flat pull-right"></asp:LinkButton>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </asp:Panel>
                </ContentTemplate>
            </rad:RadWindow>
            <rad:RadWindow RenderMode="Lightweight" ID="radMembersWindow" Behaviors="Close,Move"
                CssClass="tc-radwindow-1 top-9999 rw-members" OnClientShow="setMembersList" VisibleOnPageLoad="false"
                runat="server" Width="800px" Height="300px" OnClientClose="hideMembersListLoad">
                <ContentTemplate>
                    <asp:Button ID="btnLoadMembers" runat="server" OnClick="btnLoadMembers_Click" CssClass="display-none" />
                    <asp:Panel ID="pnlMembersList" runat="server" Style="margin-bottom: 45px; height: 250px">
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td>
                                        <rad:RadGrid RenderMode="Lightweight" ID="rgMembers" runat="server" AllowSorting="True"
                                            CssClass="GridLess" AllowMultiRowSelection="true" AllowPaging="true" OnNeedDataSource="rgMembers_OnNeedDataSource">
                                            <MasterTableView AutoGenerateColumns="false" TableLayout="Auto" HeaderStyle-ForeColor="Teal"
                                                DataKeyNames="EnrolleeCIM">
                                                <Columns>
                                                    <rad:GridTemplateColumn HeaderText="#">
                                                        <ItemTemplate>
                                                            <%#Container.ItemIndex+1 %></ItemTemplate>
                                                    </rad:GridTemplateColumn>
                                                    <rad:GridBoundColumn DataField="EnrolleeCIM" HeaderText="<%$ Resources:LocalizedResource, CimNumber %>" UniqueName="EnrolleeCIM">
                                                    </rad:GridBoundColumn>
                                                    <rad:GridBoundColumn DataField="Name" HeaderText="<%$ Resources:LocalizedResource, Name %>" UniqueName="Name">
                                                    </rad:GridBoundColumn>
                                                    <rad:GridBoundColumn DataField="Campaign" HeaderText="<%$ Resources:LocalizedResource, Account %>" UniqueName="Campaign">
                                                    </rad:GridBoundColumn>
                                                    <rad:GridBoundColumn DataField="Supervisor" HeaderText="<%$ Resources:LocalizedResource, Supervisor %>" UniqueName="Supervisor">
                                                    </rad:GridBoundColumn>
                                                    <rad:GridTemplateColumn>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnUserDetails" runat="server" ToolTip="Select Action" OnClientClick='<%# "showUserContextMenu(event," + Eval("EnrolleeCIM") + ",\""+  Eval("Name") + "\")" %>'>
                                        <i class="fa fa-tasks icon-tasks" aria-hidden="true"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </rad:GridTemplateColumn>
                                                </Columns>
                                            </MasterTableView>
                                            <ClientSettings>
                                                <Selecting AllowRowSelect="true" />
                                                <Resizing AllowResizeToFit="true" ResizeGridOnColumnResize="false" />
                                                <Scrolling AllowScroll="true" UseStaticHeaders="false" ScrollHeight="" />
                                            </ClientSettings>
                                        </rad:RadGrid>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </asp:Panel>
                    <asp:LinkButton ID="btnCloseMembersList" OnClientClick="btnCloseMembersList_Click();"
                        runat="server" Text="<%$ Resources:LocalizedResource, Close %>" ToolTip="Close" CssClass="btn tc-btn-md btn-teal btn-flat bottom-center"></asp:LinkButton>
                </ContentTemplate>
            </rad:RadWindow>
            <rad:RadWindow RenderMode="Lightweight" ID="gtrdTransactionSummary" Behaviors="Close,Move"
                VisibleOnPageLoad="false" runat="server" CssClass="tc-radwindow-1 window-summary"
                Height="330px" Title="Result Summary">
                <ContentTemplate>
                    <asp:Panel ID="pnlTransactionList" runat="server" Height="250px">
                        <rad:RadGrid RenderMode="Lightweight" ID="gtrgTransactionSummary" runat="server"
                            AllowSorting="True" CssClass="GridLess" AllowMultiRowSelection="true" AllowPaging="true"
                            OnItemDataBound="rgTransactionSummary_OnItemDataBound">
                            <MasterTableView AutoGenerateColumns="false" TableLayout="Auto" HeaderStyle-ForeColor="Teal"
                                DataKeyNames="Id" ClientDataKeyNames="Id">
                                <Columns>
                                    <rad:GridTemplateColumn HeaderText="#">
                                        <ItemTemplate>
                                            <%#Container.ItemIndex+1 %></ItemTemplate>
                                    </rad:GridTemplateColumn>
                                    <rad:GridBoundColumn DataField="Name" HeaderText="<%$ Resources:LocalizedResource, CourseTitle %>" UniqueName="GroupName">
                                    </rad:GridBoundColumn>
                                    <rad:GridBoundColumn DataField="Status" HeaderText="<%$ Resources:LocalizedResource, Status %>" UniqueName="Status">
                                    </rad:GridBoundColumn>
                                </Columns>
                            </MasterTableView>
                        </rad:RadGrid>
                    </asp:Panel>
                    <button id="gtbtnTransactionsList" onclick="gtbtnTransactionsList_Click(); return false;"
                        runat="server" title="Close" class="btn tc-btn-md btn-teal btn-flat bottom-center">
                        <asp:Label runat="server" ID="Closed" Text= "<%$ Resources:LocalizedResource, Close %>"></asp:Label></button>
                    <%-- <asp:LinkButton ID="gtbtnTransactionsList" OnClientClick="gtbtnTransactionsList_Click(); return false;"
                    runat="server" Text="Close" ToolTip="Close" CssClass="btn tc-btn-md btn-teal btn-flat bottom-center"></asp:LinkButton>--%>
                </ContentTemplate>
            </rad:RadWindow>
        </Windows>
    </rad:RadWindowManager>
    <asp:Panel ID="pnlGroups" runat="server">
        <div class="container">
            <asp:Label runat="server" ID="lblNumberOfGroup" ForeColor="Teal" Font-Bold="true"
                Font-Size="Medium"><asp:Label runat="server" ID="Thereiszero" Text= "<%$ Resources:LocalizedResource, ThereIs0GroupEnrolledToThisCourse %>"></asp:Label>
            </asp:Label>
        </div>
        <rad:RadGrid RenderMode="Lightweight" ID="rgCourseGroup" runat="server" AllowSorting="True"
            OnNeedDataSource="rgCourseGroup_OnNeedDataSource" CssClass="GridLess" Height="500px">
            <MasterTableView AutoGenerateColumns="false" HeaderStyle-ForeColor="Teal">
                <Columns>
                    <rad:GridTemplateColumn HeaderText="#">
                        <ItemTemplate>
                            <%#Container.ItemIndex+1 %></ItemTemplate>
                    </rad:GridTemplateColumn>
                    <rad:GridBoundColumn DataField="GroupName" HeaderText="GROUP / BATCH NAME" UniqueName="GroupName">
                    </rad:GridBoundColumn>
                    <rad:GridBoundColumn DataField="Account" HeaderText="<%$ Resources:LocalizedResource, Account %>" UniqueName="Account">
                    </rad:GridBoundColumn>
                    <rad:GridBoundColumn DataField="Site" HeaderText="<%$ Resources:LocalizedResource, Site %>" UniqueName="Site">
                    </rad:GridBoundColumn>
                    <rad:GridBoundColumn DataField="Members" HeaderText="<%$ Resources:LocalizedResource, Members %>" UniqueName="Members">
                    </rad:GridBoundColumn>
                    <rad:GridTemplateColumn>
                        <ItemTemplate>
                            <asp:LinkButton ID="btnDetails" runat="server" ToolTip="Select Action" OnClientClick='<%# "showGroupContextMenu(event," + Eval("GroupID") + ",\""+  Eval("GroupName") + "\")" %>'>
                        <i class="fa fa-tasks icon-tasks" aria-hidden="true"></i>
                            </asp:LinkButton>
                        </ItemTemplate>
                    </rad:GridTemplateColumn>
                </Columns>
            </MasterTableView>
            <PagerStyle Mode="NextPrev" AlwaysVisible="true" />
            <ClientSettings>
                <Resizing AllowResizeToFit="true" ResizeGridOnColumnResize="false" />
                <Scrolling AllowScroll="true" UseStaticHeaders="false" ScrollHeight="" />
                <ClientEvents OnGridCreated="refreshCount" />
            </ClientSettings>
        </rad:RadGrid>
    </asp:Panel>
    <rad:RadContextMenu ID="rcmGroupMenu" runat="server" OnItemClick="rcmGroupMenu_Click"
        EnableShadows="true" RenderMode="Lightweight" EnableScreenBoundaryDetection="false">
        <Items>
            <rad:RadMenuItem Text="View Members" Value="view" />
            <rad:RadMenuItem Text="Remove Group" Value="remove" />
            <rad:RadMenuItem Text="Assign to another Course" Value="another" />
            <rad:RadMenuItem Text="Assign a new Course" Value="new" />
        </Items>
    </rad:RadContextMenu>
    <rad:RadContextMenu ID="rcmUserMenu" runat="server" EnableShadows="true" RenderMode="Lightweight"
        EnableScreenBoundaryDetection="false" OnItemClick="rcmUserMenu_Click">
        <Items>
            <rad:RadMenuItem Text="Remove User" Value="remove" />
            <rad:RadMenuItem Text="Assign to another Course" Value="another" />
            <rad:RadMenuItem Text="Assign a new Course" Value="new" />
        </Items>
    </rad:RadContextMenu>
    <asp:HiddenField ID="hfGroupID" runat="server" />
    <asp:HiddenField ID="hfGroupName" runat="server" />
    <asp:HiddenField ID="hfCIM" runat="server" />
    <asp:HiddenField ID="hfName" runat="server" />
    <asp:HiddenField ID="hfGtSelectedCourse" runat="server" />
    <div class="modal fade success-popup tc-modal-1 top-9999" id="confirmationModalUser"
        tabindex="-1">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="confirmationTitle">
                       <asp:Label runat="server" ID="userremove" Text= "<%$ Resources:LocalizedResource,  RemoveUser %>"></asp:Label>
                    </h4>
                </div>
                <div class="modal-body text-center">
                    <div class="row">
                        <div class="col-md-2">
                            <i class="fa fa-exclamation-triangle warning-icon"></i>
                        </div>
                        <div class="col-md-10">
                            <p class="confirmationMsg" style="text-align: left;">
                                <asp:Label runat="server" ID="Surewantto" Text= "<%$ Resources:LocalizedResource, Areyousureyouwantto %>"></asp:Label>
                                <br />
                                <asp:Label runat="server" ID="ctile1" Text= "<%$ Resources:LocalizedResource, Remove %>"></asp:label><i id="modalUserName" class="bold-only"></i> <asp:Label runat="server" ID="inthisgroup" Text= "<%$ Resources:LocalizedResource, inthisgroup %>"></asp:Label>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <i data-toggle="modal" data-target="#confirmationModalUser" style="font-style: normal;
                        font-weight: bold;" class="pull-right ">
                        <asp:LinkButton ID="btnRemoveUser" runat="server" Text="YES" runat="server" OnClick="btnRemoveUser_Click"
                            CssClass="btn tc-btn-md btn-teal tc-btn-flat"></asp:LinkButton></i> <a class="btn tc-btn-md btn-default pull-right tc-btn-flat"
                                data-toggle="modal" data-target="#confirmationModalUser"> <asp:Label runat="server" ID="TProfile" Text= "<%$ Resources:LocalizedResource, No %>"></asp:Label></a>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade success-popup tc-modal-1 top-9999" id="confirmationModalGroup"
        tabindex="-1">
        <div class="modal-dialog modal-sm" role="document" style="margin-top:10%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="H1">
                        
                    </h4>
                </div>
                <div class="modal-body text-center">
                    <div class="row">
                        <div class="col-md-2">
                            <i class="fa fa-exclamation-triangle warning-icon"></i>
                        </div>
                        <div class="col-md-10">
                            <p class="confirmationMsg" style="text-align: left;">
                                <asp:Label runat="server" ID="suretowanto" Text= "<%$ Resources:LocalizedResource, Areyousureyouwantto %>"></asp:Label>
                                <br />
                                <asp:Label runat="server" ID="suretowantremove" Text= "<%$ Resources:LocalizedResource, Remove %>"></asp:Label> <i id="modalGroupName" class="bold-only"></i><asp:Label runat="server" ID="inthisCourse" Text= "<%$ Resources:LocalizedResource, inthisCourse %>"></asp:Label>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <i data-toggle="modal" data-target="#confirmationModalGroup" style="font-style: normal;
                        font-weight: bold;" class="pull-right ">
                        <asp:LinkButton ID="btnRemoveGroup" runat="server" Text="YES" runat="server" OnClick="btnRemoveGroup_Click"
                            CssClass="btn tc-btn-md btn-teal tc-btn-flat"></asp:LinkButton></i> <a class="btn tc-btn-md btn-default pull-right tc-btn-flat"
                                data-toggle="modal" data-target="#confirmationModalGroup"> <asp:Label runat="server" ID="nooo" Text= "<%$ Resources:LocalizedResource, No %>"></asp:Label></a>
                </div>
            </div>
        </div>
    </div>
</asp:Panel>
