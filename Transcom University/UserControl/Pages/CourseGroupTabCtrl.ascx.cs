﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Web.UI.HtmlControls;
using TranscomUniversityV3Model;
using System.Data.Objects;
using System.Collections;
using System.Drawing;
using TransactionNames;

public partial class CourseGroupTabCtrl : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            rgCourseGroup.Rebind();
           
        }
       
    }

    protected void rgCourseGroup_OnNeedDataSource(object sender, EventArgs e)
    {
        rgCourseGroup.DataSource = DataHelper.getCourseGroup(Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"])));
        int groupCount = rgCourseGroup.Items.Count;
        if (groupCount > 1)
        {
            lblNumberOfGroup.Text = "There are " + groupCount + " group enrolled to this course";
        }
        else
        {
            lblNumberOfGroup.Text = "There is " + groupCount + " group enrolled to this course";
        }

    }

    protected void rcmGroupMenu_Click(object sender, RadMenuEventArgs e)
    {
        var itemVal = e.Item.Value;
        var groupName = hfGroupName.Value.ToString();

        if (itemVal == "view")
        {
            rgMembers.Visible = false;
            RadScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", Utils.callClientScript("openMembersList"), true);
        }
        else if (itemVal == "remove")
        {
            RadScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", Utils.callClientScript("toggleConfirmRemoveGroup"), true);
        }
        else if (itemVal == "another")
        {
            btnAssignGroup.CommandArgument = "g1";
            rgCourseListAssign.SelectedIndexes.Clear();

            rdCourseListAssign.Title = groupName + " - Assign to Another Course";
            rgCourseListAssign.AllowMultiRowSelection = true;
            rgCourseListAssign.CurrentPageIndex = 0;
            rgCourseListAssign.Rebind();
            RadScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", Utils.callClientScript("openCourseSelectAssign"), true);
        }
        else
        {
            btnAssignGroup.CommandArgument = "g2";
            rgCourseListAssign.SelectedIndexes.Clear();

            rdCourseListAssign.Title = groupName + "  - Assign to New Course";
            rgCourseListAssign.AllowMultiRowSelection = false;
            rgCourseListAssign.CurrentPageIndex = 0;
            rgCourseListAssign.Rebind();
            RadScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", Utils.callClientScript("openCourseSelectAssign"), true);
        }
    }

    protected void rcmUserMenu_Click(object sender, RadMenuEventArgs e)
    {
        var itemVal = e.Item.Value;
        var name = hfName.Value.ToString();

        if (itemVal == "remove")
        {
            RadScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", Utils.callClientScript("toggleConfirmRemoveUser"), true);
        }
        if (itemVal == "another")
        {
            btnAssignGroup.CommandArgument = "u1";
            rdCourseListAssign.Title = name + " - Assign to Another Course";
            rgCourseListAssign.SelectedIndexes.Clear();
            rgCourseListAssign.AllowMultiRowSelection = true;
            rgCourseListAssign.CurrentPageIndex = 0;
            rgCourseListAssign.Rebind();
            RadScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", Utils.callClientScript("openCourseSelectAssign"), true);
        }
        if (itemVal == "new")
        {
            btnAssignGroup.CommandArgument = "u2";
            rdCourseListAssign.Title = name + " - Assign to a New Course";
            rgCourseListAssign.SelectedIndexes.Clear();
            rgCourseListAssign.AllowMultiRowSelection = false;
            rgCourseListAssign.CurrentPageIndex = 0;
            rgCourseListAssign.Rebind();
            RadScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", Utils.callClientScript("openCourseSelectAssign"), true);
        }
    }

    protected void rgCourseListAssign_OnNeedDataSource(object sender, EventArgs e)
    {
        var allActiveCourses = DataHelper.GetAllActiveCourses();

        if (allActiveCourses != null)
            rgCourseListAssign.DataSource = allActiveCourses;
    }

    protected void rgMembers_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        try
        {
            int groupid = Convert.ToInt32(hfGroupID.Value);
            rgMembers.DataSource = DataHelper.getGroupMembers(groupid);
        }
        catch (Exception)
        {

        }
    }

    protected void btnLoadMembers_Click(object sender, EventArgs e)
    {
        rgMembers.Visible = true;
        rgMembers.Rebind();
        RadScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", Utils.callClientScript("hideMembersListLoad"), true);
    }

    protected void btnRemoveUser_Click(object sender, EventArgs e)
    {
        try
        {
            int _actionID = DataHelper.getLastLogID() + 1;
            string _userIP = DataHelper.GetIPAddress();
            string _userID = DataHelper.GetCurrentUserCIM();
            double _duration = 0;

            DateTime startTime;
            DateTime endTime;

            int groupID = Convert.ToInt32(hfGroupID.Value);
            int courseID = Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"]));
            int userCIM = Convert.ToInt32(hfCIM.Value);

            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                var dts = db.CreateQuery<DateTime>("CurrentDateTime() ");
                startTime = dts.AsEnumerable().First();
                int successCount = 0;

                db.pr_TranscomUniversity_UpdateEnrollmentInactivity(courseID, userCIM);
                db.SaveChanges();

                successCount++;

                if (successCount > 0)
                {
                    var dte = db.CreateQuery<DateTime>("CurrentDateTime() ");
                    endTime = dte.AsEnumerable().First();

                    _duration = (endTime - startTime).TotalMilliseconds;

                    string _action = "Removed " + DataHelper.getUserName(userCIM) + " from (Group) " + DataHelper.getGroupName(groupID);
                    DataHelper.logCourse(courseID, _actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);
                    rgCourseGroup.Rebind();

                    rwmGroup.RadAlert("User was successfully removed", 330, 180, "Success Message", "");
                }
            }
        }
        catch (Exception)
        {
            rwmGroup.RadAlert("Error removing User. Please contact your System Administrator.", 330, 180, "Error Message", "");
        }

        rgCourseGroup.Rebind();
        rgMembers.Rebind();
    }

    protected void btnRemoveGroup_Click(object sender, EventArgs e)
    {
        try
        {
            int _actionID = DataHelper.getLastLogID() + 1;
            string _userIP = DataHelper.GetIPAddress();
            string _userID = DataHelper.GetCurrentUserCIM();
            double _duration = 0;

            DateTime startTime;
            DateTime endTime;

            int _groupID = Convert.ToInt32(hfGroupID.Value);
            int courseID = Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"]));

            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                var dts = db.CreateQuery<DateTime>("CurrentDateTime() ");
                startTime = dts.AsEnumerable().First();
                int successCount = 0;

                db.pr_TranscomUniversity_RemoveGroup(_groupID, courseID);
                db.SaveChanges();
                successCount++;

                if (successCount > 0)
                {
                    var dte = db.CreateQuery<DateTime>("CurrentDateTime() ");
                    endTime = dte.AsEnumerable().First();

                    _duration = (endTime - startTime).TotalMilliseconds;

                    string _action = "Removed " + DataHelper.getGroupName(_groupID);
                    DataHelper.logCourse(courseID, _actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);

                    rgCourseGroup.Rebind();
                }
            }
        }
        catch (Exception)
        {
            rwmGroup.RadAlert("Error removing group. Please contact your System Administrator.", 330, 180, "Error Message", "");
        }
    }

    protected void btnAssignGroup_Click(object sender, CommandEventArgs e)
    {
        string cmd = e.CommandArgument.ToString();

        if (cmd == "g1") //group another
        {
            int courseID_Origin = Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"]));
            int _groupID = Convert.ToInt32(hfGroupID.Value);
            int _courseID = -1;

            string courseList = hfGtSelectedCourse.Value;
            string[] data = courseList.Split(',');

            int _actionID = DataHelper.getLastLogID() + 1;
            string _userIP = DataHelper.GetIPAddress();
            string _userID = DataHelper.GetCurrentUserCIM();
            double _duration = 0;

            DateTime startTime;
            DateTime endTime;

            int successCount = 0;
            string stat;

            List<Transaction> CourseList = new List<Transaction>();

            if (data.Length > 1)
            {
                using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
                {
                    var dts = db.CreateQuery<DateTime>("CurrentDateTime() ");
                    startTime = dts.AsEnumerable().First();

                    for (int i = 0; i < (data.Length - 1); i++)
                    {
                        _courseID = Convert.ToInt32(data[i]);
                        try
                        {
                            pr_TranscomUniversity_InsertGroup_Result _res = db.pr_TranscomUniversity_InsertGroup(_courseID, _groupID).SingleOrDefault();

                            if (_res.Msg == "1")
                            {
                                stat = "Successful";
                                successCount++;

                                string _action = "Added " + DataHelper.getGroupName(_groupID);
                                var dte = db.CreateQuery<DateTime>("CurrentDateTime() ");

                                endTime = dte.AsEnumerable().First();
                                _duration = (endTime - startTime).TotalMilliseconds;

                                int l_actionID = DataHelper.getLastLogID() + 1;
                                DataHelper.logCourse(_courseID, l_actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);
                            }
                            else if (_res.Msg == "2")
                            {
                                stat = "Duplicate";
                            }
                            else
                            {
                                stat = "Fail";
                            }

                            Transaction course = new Transaction
                            {
                                Id = Convert.ToInt32(data[i]),
                                Name = _res.Title,
                                Status = stat
                            };

                            CourseList.Add(course);
                        }
                        catch (Exception)
                        {
                            rwmGroup.RadAlert("Error Assigning Group. Please contact your System Administrator.", 330, 180, "Error Message", "");
                        }
                    }
                    if (successCount > 0)
                    {
                        string _action = successCount > 1 ? "Assigned " + DataHelper.getGroupName(_groupID) + " to " + successCount + " Courses" : "Assigned " + DataHelper.getGroupName(_groupID) + " to " + DataHelper.getCourseName(_courseID);

                        if (successCount > 0)
                        {
                            var dte = db.CreateQuery<DateTime>("CurrentDateTime() ");
                            endTime = dte.AsEnumerable().First();
                            _duration = (endTime - startTime).TotalMilliseconds;
                            _actionID = DataHelper.getLastLogID() + 1;
                            DataHelper.logCourse(courseID_Origin, _actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);
                        }
                    }
                    if (data.Length > 1)
                    {
                        gtrgTransactionSummary.DataSource = CourseList;
                        gtrgTransactionSummary.DataBind();
                        RadScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", Utils.callClientScript("gtshowTransaction"), true);
                    }
                }
            }
            else
            {
                rwmGroup.RadAlert("Please Select at least one (1) Course.", 330, 180, "Error Message", "");
            }
            rgCourseGroup.Rebind();
        }
        else if (cmd == "g2") //group new
        {
            int courseID_Origin = Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"]));
            int _groupID = Convert.ToInt32(hfGroupID.Value);
            int _courseID = -1;

            string courseList = hfGtSelectedCourse.Value;
            string[] data = courseList.Split(',');

            int _actionID = DataHelper.getLastLogID() + 1;
            string _userIP = DataHelper.GetIPAddress();
            string _userID = DataHelper.GetCurrentUserCIM();
            double _duration = 0;

            DateTime startTime;
            DateTime endTime;

            if (data.Length > 1)
            {
                using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
                {
                    var dts = db.CreateQuery<DateTime>("CurrentDateTime() ");
                    startTime = dts.AsEnumerable().First();

                    for (int i = 0; i < (data.Length - 1); i++)
                    {
                        _courseID = Convert.ToInt32(data[i]);
                        try
                        {
                            pr_TranscomUniversity_TransferGroup_Result _res = db.pr_TranscomUniversity_TransferGroup(_groupID, courseID_Origin, _courseID).SingleOrDefault();

                            if (_res.Msg == "1")
                            {
                                //Log Assigned Group
                                string _action = "Assigned " + DataHelper.getGroupName(_groupID) + " to new Course " + DataHelper.getCourseName(_courseID);

                                var dte = db.CreateQuery<DateTime>("CurrentDateTime() ");
                                endTime = dte.AsEnumerable().First();
                                _duration = (endTime - startTime).TotalMilliseconds;

                                DataHelper.logCourse(courseID_Origin, _actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);

                                //Log Added Group
                                _action = "Added " + DataHelper.getGroupName(_groupID);
                                dte = db.CreateQuery<DateTime>("CurrentDateTime() ");

                                endTime = dte.AsEnumerable().First();
                                _duration = (endTime - startTime).TotalMilliseconds;

                                _actionID = DataHelper.getLastLogID() + 1;
                                DataHelper.logCourse(_courseID, _actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);

                                rwmGroup.RadAlert("The group was successfully assigned to another course.", 330, 180, "Successful", "");
                            }
                            else if (_res.Msg == "2")
                            {
                                rwmGroup.RadAlert("The group already exists in this course.", 330, 180, "Duplicate", "");
                            }
                            else
                            {
                                rwmGroup.RadAlert("Something went wrong! Please try again.", 330, 180, "Error Message", "");
                            }
                        }
                        catch (Exception)
                        {
                            rwmGroup.RadAlert("Error In Assigning Group. Please contact your System Administrator.", 330, 180, "Error Message", "");
                        }
                    }
                }
            }
            else
            {
                rwmGroup.RadAlert("Please Select at least one (1) Course.", 330, 180, "Error Message", "");
            }
            rgCourseGroup.Rebind();
        }
        else if (cmd == "u1")
        {
            try
            {
                int courseID_Origin = Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"]));
                var enrolleeCim = Convert.ToInt32(hfCIM.Value);
                var delegator = DataHelper.GetCurrentUserCIM();

                string courseList = hfGtSelectedCourse.Value;
                string[] data = courseList.Split(',');

                int _courseID = -1;

                int _actionID = DataHelper.getLastLogID() + 1;
                string _userIP = DataHelper.GetIPAddress();
                string _userID = DataHelper.GetCurrentUserCIM();
                double _duration = 0;

                DateTime startTime;
                DateTime endTime;

                int successCount = 0;
                string stat;

                List<Transaction> CourseList = new List<Transaction>();

                if (data.Length > 1)
                {
                    using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
                    {
                        var dts = db.CreateQuery<DateTime>("CurrentDateTime() ");
                        startTime = dts.AsEnumerable().First();

                        for (int i = 0; i < (data.Length - 1); i++)
                        {
                            _courseID = Convert.ToInt32(data[i]);

                            pr_TranscomUniversity_UpdateEnrollmentUsers_Result _res = db.pr_TranscomUniversity_UpdateEnrollmentUsers(Convert.ToInt32(_courseID), Convert.ToInt32(enrolleeCim), Convert.ToInt32(delegator)).SingleOrDefault();
                            if (_res.Msg == "1")
                            {
                                stat = "Successful";
                                successCount++;
                                string _action = "Added " + DataHelper.getUserName(enrolleeCim);
                                var dte = db.CreateQuery<DateTime>("CurrentDateTime() ");

                                endTime = dte.AsEnumerable().First();
                                _duration = (endTime - startTime).TotalMilliseconds;

                                int l_actionID = DataHelper.getLastLogID() + 1;
                                DataHelper.logCourse(_courseID, l_actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);
                            }
                            else if (_res.Msg == "2")
                            {
                                stat = "Duplicate";
                            }
                            else
                            {
                                stat = "Fail";
                            }

                            Transaction course = new Transaction
                            {
                                Id = Convert.ToInt32(data[i]),
                                Name = _res.Title,
                                Status = stat
                            };

                            CourseList.Add(course);
                        }

                        if (successCount > 0)
                        {
                            string _action = successCount > 1 ? "Assigned " + DataHelper.getUserName(enrolleeCim) + " to " + successCount + " Courses" : "Assigned " + DataHelper.getUserName(enrolleeCim) + " to " + DataHelper.getCourseName(_courseID);

                            if (successCount > 0)
                            {
                                var dte = db.CreateQuery<DateTime>("CurrentDateTime() ");
                                endTime = dte.AsEnumerable().First();

                                _duration = (endTime - startTime).TotalMilliseconds;
                                _actionID = DataHelper.getLastLogID() + 1;

                                DataHelper.logCourse(courseID_Origin, _actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);
                            }
                        }
                    }

                    gtrgTransactionSummary.DataSource = CourseList;
                    gtrgTransactionSummary.DataBind();
                    RadScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", Utils.callClientScript("gtshowTransaction"), true);
                }
                else
                {
                    rwmGroup.RadAlert("Please select atleast one(1) course to assign.", 330, 180, "Error Message", "");
                }
                rgCourseGroup.Rebind();
            }
            catch
            {
                rwmGroup.RadAlert("Error assigning to another course(s). Please contact your System Administrator.", 330, 180, "Error Message", "");
            }

        }
        else if (cmd == "u2")
        {
            try
            {
                var enrolleeCim = Convert.ToInt32(hfCIM.Value);
                var currentCourseId = Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"]));
                int groupID = Convert.ToInt32(hfGroupID.Value);
                int delegator = Convert.ToInt32(DataHelper.GetCurrentUserCIM());

                string courseList = hfGtSelectedCourse.Value;
                string[] data = courseList.Split(',');

                int _courseID = -1;

                int _actionID = DataHelper.getLastLogID() + 1;
                string _userIP = DataHelper.GetIPAddress();
                string _userID = DataHelper.GetCurrentUserCIM();
                double _duration = 0;

                DateTime startTime;
                DateTime endTime;

                if (data.Length > 1)
                {
                    for (int i = 0; i < (data.Length - 1); i++)
                    {
                        var db = new TranscomUniversityV3ModelContainer();
                        var courseId = data[i];

                        var dts = db.CreateQuery<DateTime>("CurrentDateTime() ");
                        startTime = dts.AsEnumerable().First();

                        pr_TranscomUniversity_UpdateGroupTransferUser_Result retVal = db.pr_TranscomUniversity_UpdateGroupTransferUser(groupID, Convert.ToInt32(enrolleeCim), Convert.ToInt32(courseId), delegator).SingleOrDefault();
                        if (retVal.Msg != "0")
                        {
                            var dte = db.CreateQuery<DateTime>("CurrentDateTime() ");
                            endTime = dte.AsEnumerable().First();
                            _duration = (endTime - startTime).TotalMilliseconds;

                            string _action = "Assigned " + DataHelper.getUserName(enrolleeCim) + " to new Course " + DataHelper.getCourseName(Convert.ToInt32(courseId));
                            DataHelper.logCourse(currentCourseId, _actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);

                            _actionID = DataHelper.getLastLogID() + 1;

                            endTime = dte.AsEnumerable().First();
                            _duration = (endTime - startTime).TotalMilliseconds;

                            _action = "Added " + DataHelper.getUserName(enrolleeCim);
                            DataHelper.logCourse(_courseID, _actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);

                            rwmGroup.RadAlert("User was successfully assigned.", 330, 180, "Success Message", "");
                        }
                        else
                            rwmGroup.RadAlert("User is already assigned to this course.", 330, 180, "Error Message", "");
                    }
                }
                else
                {
                    rwmGroup.RadAlert("Please select one(1) course to assign.", 330, 180, "Error Message", "");
                }

                rgCourseGroup.Rebind();
            }
            catch
            {
                rwmGroup.RadAlert("Error assigning to new course. Please contact your System Administrator.", 330, 180, "Error Message", "");
            }
        }
    }

    protected void rgTransactionSummary_OnItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            GridDataItem dataBoundItem = e.Item as GridDataItem;

            if (dataBoundItem["Status"].Text == "Duplicate")
            {
                dataBoundItem["Status"].ForeColor = Color.DarkGray;
            }
            else if (dataBoundItem["Status"].Text == "Fail")
            {
                dataBoundItem["Status"].ForeColor = Color.Red;
            }
            else
            {
                dataBoundItem["Status"].ForeColor = Color.YellowGreen;
            }
        }
    }
}