﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CourseHistoryUserCtrl.ascx.cs"
    Inherits="CourseHistoryUserCtrl" %>
<asp:Panel ID="pnlMainHistoryUC" runat="server" ClientIDMode="Static">
    <rad:RadScriptBlock ID="gtRadScriptBlock1" runat="server">
        <script type="text/javascript">
        </script>
    </rad:RadScriptBlock>
    <div class="container">
        <asp:Label runat="server" ID="lblNumberOfLogs" ForeColor="Teal" Font-Bold="true"
            Font-Size="Medium">
            <asp:Literal ID="ltLogCount" runat="server"></asp:Literal>
             <asp:Label runat="server" ID="TProfile" Text= "<%$ Resources:LocalizedResource, EventsLogged %>"></asp:Label></asp:Label>
    </div>
    <rad:RadGrid RenderMode="Lightweight" ID="rgHistory" runat="server" CssClass="GridLess" Visible="true"
        OnItemEvent="rgHistory_OnItemEvent" Height="500px" Font-Size="12px" OnNeedDataSource="rgHistory_NeedDataSource">
        <PagerStyle Mode="NextPrev" AlwaysVisible="true" />
        <ItemStyle Wrap="true" />
        <ClientSettings EnableAlternatingItems="true">
            <Selecting AllowRowSelect="true"></Selecting>
            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
            <Resizing AllowResizeToFit="true" ResizeGridOnColumnResize="false" />
        </ClientSettings>
        <ItemStyle Wrap="true"></ItemStyle>
        <MasterTableView AutoGenerateColumns="false" HeaderStyle-ForeColor="Teal" AllowPaging="true"
            TableLayout="Auto" NoMasterRecordsText="No History Logs for this Course">
            <Columns>
                <rad:GridTemplateColumn HeaderText="#">
                    <ItemTemplate>
                        <%#Container.ItemIndex+1 %></ItemTemplate>
                </rad:GridTemplateColumn>
                <rad:GridBoundColumn DataField="LogDate" HeaderText="<%$ Resources:LocalizedResource, Date %>" UniqueName="LogDate">
                </rad:GridBoundColumn>
                <rad:GridBoundColumn DataField="UserID" HeaderText="<%$ Resources:LocalizedResource, CimNumber %>" UniqueName="UserID">
                </rad:GridBoundColumn>
                <rad:GridBoundColumn DataField="UserRole" HeaderText="<%$ Resources:LocalizedResource, Role %>" UniqueName="Role">
                </rad:GridBoundColumn>
                <rad:GridBoundColumn DataField="EmailAddress" HeaderText="<%$ Resources:LocalizedResource, EmailAddress %>" UniqueName="EmailAddress">
                </rad:GridBoundColumn>
                <rad:GridBoundColumn DataField="UserIpAddress" HeaderText="<%$ Resources:LocalizedResource, IpAddress %>" UniqueName="Ip">
                </rad:GridBoundColumn>
                <rad:GridBoundColumn DataField="Action" HeaderText="<%$ Resources:LocalizedResource, Action %>" UniqueName="Action">
                </rad:GridBoundColumn>
                <rad:GridBoundColumn DataField="Duration" HeaderText="<%$ Resources:LocalizedResource, Duration %>" UniqueName="Duration">
                </rad:GridBoundColumn>
            </Columns>
        </MasterTableView>
    </rad:RadGrid>
    <rad:RadAjaxManagerProxy ID="chAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <rad:AjaxSetting AjaxControlID="rgHistory">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="rgHistory" LoadingPanelID="localLoadingPanel" />
                </UpdatedControls>
            </rad:AjaxSetting>
        </AjaxSettings>
    </rad:RadAjaxManagerProxy>
</asp:Panel>
