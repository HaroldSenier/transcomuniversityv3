﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;

public partial class CourseHistoryUserCtrl : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["CourseID"] != null)
        rgHistory.Rebind();
    }

    protected void rgHistory_OnItemEvent(object sender, GridItemEventArgs e)
    {
        if (e.EventInfo is GridInitializePagerItem)
        {
            int rowCount = (e.EventInfo as GridInitializePagerItem).PagingManager.DataSourceCount;
            ltLogCount.Text = rowCount.ToString();
        }

    }
    protected void rgHistory_NeedDataSource(object sender, EventArgs e)
    {
        //rgHistory.DataSource = DataHelper.getClassLogHistory(Convert.ToInt32(Utils.Decrypt(Request.QueryString["ClassID"])));
        var ds = DataHelper.getCourseLogHistory(Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"])));
        rgHistory.DataSource = ds;        

    }

}