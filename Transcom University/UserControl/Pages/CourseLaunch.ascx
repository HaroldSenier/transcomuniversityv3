﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CourseLaunch.ascx.cs"
    Inherits="UserControl_Pages_CourseLaunch" %>
<rad:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript" language="javascript">
        //SCORM
        function OpenTraining(strOrgOrAtt, scoID, scoTypeID, cim) {
            LogResourceLaunch(cim, scoID, scoTypeID).done(function () {
                // open training content; <strOrgOrAtt> is either of the form "Org:<organizationId>"
                // (for content that has not been launched yet) or "Att:<attemptId>" for content that's
                // previously been launched -- in the former case we need to create an attempt for the
                // content...
                var a;
                if ((a = strOrgOrAtt.match(/^Pkg:([0-9]+)$/)) != null) {
                    // display the dialog to create an attempt on this organization; if the attempt is
                    // successfully created, OnAttemptCreated() will be called from the the dialog to
                    // update TrainingGrid and display the training
                    var args = new Object;
                    args.OrganizationId = a[1];
                    var courseID = '<%= Request.QueryString["CourseID"] %>';

                    ShowDialog("SCORM/CreateAttempt.aspx?PkgID=" + a[1] + "&CourseID=" + courseID, args, 800, 600, false);

                    //window.open("SCORM/CreateAttempt.aspx?PkgID=" + a[1] + "&CourseID=" + courseID, "myWindow", "width=800,height=600,location=no,menubar=no,scrollbars=yes,status=no,toolbar=no,resizable=yes");
                }
                else if ((a = strOrgOrAtt.match(/^Att:([0-9]+)$/)) != null) {
                    // open training in a new window
                    OpenFrameset(a[1]);
                }
                //window.location.reload();
            });
        }

        function OnAttemptCreated(strOrganizationId, strAttemptId) {
            OpenFrameset(strAttemptId);
            // called after CreateAttempt.aspx has successfully created an attempt; update the
            // anchor tag to include the attempt number, then open the frameset
            //                var anchor = document.all["Org_" + strOrganizationId];
            //                anchor.href = "javascript:OpenTraining('Att:" + strAttemptId + "')";
            //                anchor.title = "Continue training";
            //                anchor.parentElement.parentElement.cells[3].innerHTML =
            //            	"<A href=\"javascript:ShowLog(" + strAttemptId + ")\" title=\"Show Log\">Active</A>";
            OpenFrameset(strAttemptId);
        }

        function ShowDialog(strUrl, args, cx, cy, fScroll) {
            // display a dialog box with URL <strUrl>, arguments <args>, width <cx>, height <cy>,
            // scrollbars if <fScroll>; this can be done using either showModalDialog() or
            // window.open(): the former has better modal behavior; the latter allows selection
            // within the window

            var useShowModalDialog = false;
            var strScroll = fScroll ? "yes" : "no";
            if (useShowModalDialog) {
                showModalDialog(strUrl, args,
				    "dialogWidth: " + cx + "px; dialogHeight: " + cy +
					"px; center: yes; resizable: yes; scroll: " + strScroll + ";");
            }
            else {
                dialogArguments = args; // global variable accessed by dialog
                var x = Math.max(0, (screen.width - cx) / 2);
                var y = Math.max(0, (screen.height - cy) / 2);
                GLOBALFRAMESETPOPUP = window.open(strUrl, "myWindow", "left=" + x + ",top=" + y +
					",width=" + cx + ",height=" + cy +
					",location=no,menubar=no,scrollbars=" + strScroll +
					",status=no,toolbar=no,resizable=yes");
                debugger;
                sessionStorage.setItem('GLOBALFRAMESETPOPUP', GLOBALFRAMESETPOPUP);
                GLOBALFRAMESETPOPUP.onbeforeunload = function () {
                    GLOBALFRAMESETPOPUP = null;
                }

                if (GLOBALFRAMESETPOPUP == null || typeof (GLOBALFRAMESETPOPUP) == 'undefined') {
                   var hndalerclick = $("#<%=hndalerclick.ClientID%>").val();
                    alert(hndalerclick);
                    return;
                }
                else {
                    GLOBALFRAMESETPOPUP.focus();
                }
            }
        }

        function OpenFrameset(strAttemptId) {
            debugger;
            var courseID = '<%= Request.QueryString["CourseID"] %>';
            GLOBALFRAMESETPOPUP = window.open('<%=Page.ResolveClientUrl("~/Frameset/Frameset.aspx?View=0&AttemptId=")%>' + strAttemptId + "&CourseID=" + courseID, "myWindow", "width=800,height=600,location=no,menubar=no,scrollbars=yes,status=no,toolbar=no,resizable=yes")

            sessionStorage.setItem('GLOBALFRAMESETPOPUP', GLOBALFRAMESETPOPUP);
            GLOBALFRAMESETPOPUP.onbeforeunload = function () {
                GLOBALFRAMESETPOPUP = null;
            }
            if (GLOBALFRAMESETPOPUP == null || typeof (GLOBALFRAMESETPOPUP) == 'undefined') {
                var hndalerclick = $("#<%=hndalerclick.ClientID%>").val();
                alert(hndalerclick);
                return;
            }
            else {
                GLOBALFRAMESETPOPUP.focus();
            }
            //rebindBundle();
        }

        function showLoading() {
            //console.log("show loading");
            var panel = $find("<%= localLoadingPanel.ClientID %>");
            panel.show("pnlMain");
        }

        function hideLoading() {
            //console.log("hide");
            $(".Loading2").hide();
        }

        //END SCORM
    </script>
</rad:RadCodeBlock>
<rad:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server" CssClass="Loading">
</rad:RadAjaxLoadingPanel>
<rad:RadAjaxLoadingPanel ID="localLoadingPanel" runat="server" CssClass="Loading2"
    Transparency="25" />
<asp:Panel ID="pnlMain" runat="server" ClientIDMode="Static">
    <div id="wrapper">
        <div id="Header">
        </div>
        <div id="Body">
            <div class="col-md-14" style="margin-left: 1%; margin-right: 1%">
                <div class="col-md-12">
                    <asp:Panel ID="pnlLearnerResourceLv" runat="server">
                        <div id="divLearnerResourceLv" class="demo-container col-md-14">
                            <rad:RadListView ID="lvCourseDefault_Learner" runat="server" ItemPlaceholderID="SectionHolder"
                                AllowPaging="false" OnItemDataBound="lvCourseDefault_Learner_SectionDataBound"
                                OnNeedDataSource="lvCourseDefault_Learner_NeedDataSource" DataKeyNames="SectionID">
                                <LayoutTemplate>
                                    <div id="SectionHolder" runat="server">
                                    </div>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <asp:Panel ID="pnlSection" runat="server" CssClass="panel panel-default">
                                        <div class="panel-heading panel-heading-black display-flex">
                                            <h4 class="panel-title">
                                                <a id="A1" data-toggle="collapse" class="js-collapse-event no-underline-hover" data-parent="#learnerTab"
                                                    href='<%# String.Format("#_{0}", Eval("SectionID")) %>' onclick="tabClick(this);">
                                                    <%# Eval("SectionName") %>
                                                </a><i id="I10" class="fa fa-sort-down pull-right hover-pointer js-collapse-event"
                                                    runat="server" data-toggle="collapse" data-parent="#learnerTab" href='<%#String.Format("#_{0}", Eval("SectionID")) %>'
                                                    onclick="tabClick(this);"></i>
                                            </h4>
                                        </div>
                                        <div id='<%#String.Format("_{0}", Eval("SectionID")) %>' class='panel-collapse collapse in <%# Eval("SectionName").ToString().Replace(" ", "") %>'>
                                            <div class="panel-body">
                                                <div id="ContentAccordion">
                                                    <rad:RadListView ID="lvCourseDefault_Learner_Item" runat="server" ItemPlaceholderID="ItemHolder"
                                                        AllowPaging="false" DataKeyNames="ScoID" OnItemDataBound="lvCourseDefault_Learner_Item_OnItemDataBound">
                                                        <LayoutTemplate>
                                                            <div id="ItemHolder" runat="server">
                                                            </div>
                                                        </LayoutTemplate>
                                                        <ItemTemplate>
                                                            <div class="group">
                                                                <div class="row panel-gray panel-course-category m-course-launcher-item">
                                                                    <div class="col-xs-11 m-item" title='<%#Eval("ScoTitle")%>' style="line-height: 50px;">
                                                                        <div class="col-xs-6 col-md-4 col-lg-4 m-item-icon-title display-inline-flex">
                                                                            <asp:Image ID="ImgELearning" runat="server" ImageUrl="~/Media/Images/elearning.png" />
                                                                            <asp:Button runat="server" ID="btnLaunch" ClientIDMode="Static" Text="Launch" CssClass="btn font-bold btn-transparent linkBtn wrap-visible"
                                                                                ToolTip="Launch Course" />
                                                                        </div>
                                                                        <asp:Label ID="lblTitle" runat="server" CssClass="course-title  m-item-scotitle">
                                                                <%#Eval("ScoTitle")%>
                                                                        </asp:Label>
                                                                        <div class="col-xs-6 col-md-4 m-item-score-details">
                                                                            <div class="col-md-12">
                                                                                <asp:Label runat="server" ID="lblStatusMarker" Text="<%$ Resources:LocalizedResource, Status%>" CssClass="lblTUTorquise font-bold m-display-none" />
                                                                                &nbsp
                                                                                <asp:Label runat="server" ID="lblStatus" Text="<%$ Resources:LocalizedResource, NotTaken %>" CssClass="m-lbl-status" />
                                                                                <br />
                                                                            </div>
                                                                            <asp:Panel runat="server" ID="pnlScoreContainer" runat="server" CssClass="display-none col-md-12">
                                                                                <asp:Label runat="server" ID="lblScoreMarker" Text="Score:" CssClass="lblTUTorquise font-bold" />
                                                                                &nbsp
                                                                                <asp:Label runat="server" ID="lblScore" Text="N/A" />
                                                                                <asp:HiddenField ID="hfScoTypeID" runat="server" Value='<%#Eval("SCOTYPEID")%>' />
                                                                                <asp:HiddenField ID="hfScoID" runat="server" Value='<%#Eval("ScoID")%>' />
                                                                                <asp:HiddenField ID="hfFileName" runat="server" Value='<%#Eval("FileName")%>' />
                                                                                <asp:HiddenField ID="hfAttemptID" runat="server" Value='<%#Eval("AttemptID")%>' />
                                                                                <asp:HiddenField ID="hfRedirectID" runat="server" Value='<%#Eval("RedirectID")%>' />
                                                                                <asp:HiddenField ID="hfAttemptStatus" runat="server" Value='<%#Eval("AttemptStatus")%>' />
                                                                                <asp:HiddenField ID="hfSuccessStatus" runat="server" Value='<%#Eval("SuccessStatus")%>' />
                                                                                <asp:HiddenField ID="hfScoPackageID" runat="server" Value='<%#Eval("ScoPackageID")%>' />
                                                                                <asp:HiddenField ID="hfcoursetypeID" runat="server" Value='<%#Eval("coursetypeID")%>' />
                                                                                <asp:HiddenField ID="hfScore" runat="server" Value='<%#Eval("Score")%>' />
                                                                                <asp:HiddenField ID="hfLessonStatus" runat="server" Value='<%#Eval("LessonStatus")%>' />
                                                                                <asp:HiddenField ID="hfPackageFormat" runat="server" Value='<%#Eval("PackageFormat")%>' />
                                                                                <asp:HiddenField ID="hfDripEnable" runat="server" Value='<%#Eval("dripEnable")%>' />
                                                                            </asp:Panel>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </ItemTemplate>
                                                        <EmptyDataTemplate>
                                                             <asp:Label runat="server" ID="thissection" Text= "<%$ Resources:LocalizedResource, ThisSectionHasNoItemToDisplay %>"></asp:Label>
                                                        </EmptyDataTemplate>
                                                    </rad:RadListView>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </ItemTemplate>
                                <EmptyDataTemplate>
                                  <asp:Label runat="server" ID="nodisp" Text= "<%$ Resources:LocalizedResource, NoResourcetodisplay %>"></asp:Label>
                                </EmptyDataTemplate>
                                <EmptyItemTemplate>
                                 <asp:Label runat="server" ID="sourcenodisp" Text= "<%$ Resources:LocalizedResource, NoResourcetodisplay %>"></asp:Label>
                                </EmptyItemTemplate>
                            </rad:RadListView>
                        </div>
                    </asp:Panel>
                </div>
            </div>
        </div>
    </div>
</asp:Panel>

<asp:HiddenField runat="server" ID ="hndalerclick" Value="Please disable your pop-up blocker or allow Popup to load the course resource and click the link again." /> 
