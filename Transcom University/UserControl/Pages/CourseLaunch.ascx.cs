﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Telerik.Web.UI;
using System.Web.Security;
using TranscomUniversityV3Model;
using DotNetSCORM;
using NuComm.Security.Encryption;
using System.Drawing;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using NuSkill.Business;
using System.Collections;
using System.Net;
using System.Xml.Linq;



public partial class UserControl_Pages_CourseLaunch : System.Web.UI.UserControl
{
    private DataHelper _db;
    private static SqlConnection _oconn;
    private static SqlCommand _ocmd;
    public static string EncCourseId
    {
        get
        {
            var value = HttpContext.Current.Session["EncCourseIdSession"];
            return value.ToString();
        }
        set { HttpContext.Current.Session["EncCourseIdSession"] = value; }
    }
    static int global_CurriculumCourseID; //identity of sco
    static int global_CourseID; //also called CurriculumId;
    static int global_UserCIM;
    static string global_userEmail;
    private static List<int> CURRCOURSEID;
    private static int totalResources;
    private static int totalCompletedResources;
    private static bool isFirstLR;
    private static bool isValid;

    private static int global_TimezoneOffset;
    

    bool donePrevResource;

    private static IEnumerable<TranscomUniversityV3Model.pr_TranscomUniversity_lkp_PackageDetails_Result> resources;

    private static void Dbconn(string connStr)
    {
        _oconn = new SqlConnection
        {
            ConnectionString = ConfigurationManager.ConnectionStrings[connStr].ConnectionString
        };
        _oconn.Open();
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            CURRCOURSEID = new List<int>();
            totalResources = 0;
            totalCompletedResources = 0;

            if (global_TimezoneOffset == 0)
            {
                var tz = "";

                tz = Utils.Decrypt(Request.QueryString["tz"]);
                global_TimezoneOffset = Convert.ToInt32(tz);

            }
            
        }


        //setProgress();

        global_CourseID = Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"]));
        isValid = DataHelper.isCourseValid(global_CourseID, global_TimezoneOffset);

        if (DataHelper.GetCurrentUserCIM() != "")
        {
            global_UserCIM = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
            global_userEmail = DataHelper.getCurrentUserEmail();

            if (!Page.IsPostBack)
            {
                donePrevResource = false;
            }

            //lvCourseDefault_Learner.Rebind();
            //gridBundle.Rebind();

        }

        //for learner resource
        //always true on load to enable access to first item in learner resources
        isFirstLR = true;

        //clear test session every course load
        SessionManager_Testing.SessionQuestionnaire = null;
        //SessionManager_Testing.SessionTestTaken = null;

    }

    protected void lvCourseDefault_Learner_NeedDataSource(object sender, EventArgs e)
    {
        try
        {
            //if (DataHelper.GetCourseType(global_CourseID) == 2)
            //{

            if (CURRCOURSEID == null || CURRCOURSEID.Count == 0)
            {
                Dbconn("DefaultConnection");
                _ocmd = new SqlCommand("pr_Scorm_Cor_GetCurriculumPackages", _oconn)
                {
                    CommandType = CommandType.StoredProcedure
                };

                _ocmd.Parameters.AddWithValue("@CurriculumID", global_CourseID);
                _ocmd.Parameters.AddWithValue("@isBundle", 1);
                SqlDataReader oDataReader = _ocmd.ExecuteReader();
                if (oDataReader.Read())
                {
                    do
                    {
                        CURRCOURSEID.Add(Convert.ToInt32(oDataReader["curriculumcourseID"].ToString()));
                        //global_CurriculumCourseID = Convert.ToInt32(oDataReader["curriculumcourseID"].ToString());
                    } while (oDataReader.Read());
                }

            }
            //}

            foreach (var currid in CURRCOURSEID)
            {
                var db = new TranscomUniversityV3ModelContainer();

                resources = db.pr_TranscomUniversity_lkp_PackageDetails(global_userEmail, global_CourseID, currid).ToList();
                //global_CurriculumCourseID = currid;
                totalResources = resources.ToList().Count();
                totalCompletedResources = resources.ToList().Count(c => c.SuccessStatus > 0);

                resources = resources
                            .Where(p => p.SCOTYPEID >= 1 && p.SCOTYPEID <= 4)
                            .AsQueryable();
                global_CurriculumCourseID = currid;
                var dtResources = DataHelper.GetCourseSections(global_CourseID, 1);
                lvCourseDefault_Learner.DataSource = dtResources;

                setProgress();

            }

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void lvCourseDefault_Learner_Item_OnItemDataBound(object sender, RadListViewItemEventArgs e)
    {
        if (e.Item is RadListViewItem)
        {
            var dataItem = (RadListViewItem)e.Item;

            var FileName = (dataItem.FindControl("hfFileName") as HiddenField).Value.ToString();
            var scoTypeID = Convert.ToInt32((dataItem.FindControl("hfScoTypeID") as HiddenField).Value.ToString());
            var scoID = Convert.ToInt32((dataItem.FindControl("hfScoID") as HiddenField).Value.ToString());
            var ScoTitle = (dataItem.FindControl("lblTitle") as Label).Text;
            var AttemptID = (dataItem.FindControl("hfAttemptID") as HiddenField).Value.ToString() == null ? "0" : (dataItem.FindControl("hfAttemptID") as HiddenField).Value.ToString();
            var RedirectID = (dataItem.FindControl("hfRedirectID") as HiddenField).Value.ToString() ?? "0";
            var CompletionStatus = (dataItem.FindControl("hfAttemptStatus") as HiddenField).Value.ToString() == null ? "0" : (dataItem.FindControl("hfAttemptStatus") as HiddenField).Value.ToString();
            var SuccessStatus = (dataItem.FindControl("hfSuccessStatus") as HiddenField).Value.ToString();
            var ScoPackageID = (dataItem.FindControl("hfScoPackageID") as HiddenField).Value.ToString() ?? "0";
            var encCourseId = Server.UrlEncode(UTF8.EncryptText(ScoPackageID));

            var coursetypeID = (dataItem.FindControl("hfcoursetypeID") as HiddenField).Value.ToString() ?? "0";
            var Score = (dataItem.FindControl("hfScore") as HiddenField).Value.ToString() == null ? "0" : (dataItem.FindControl("hfScore") as HiddenField).Value.ToString();
            var LessonStatus = (dataItem.FindControl("hfLessonStatus") as HiddenField).Value.ToString();
            int? packageFormat = (dataItem.FindControl("hfPackageFormat") as HiddenField).Value.ToString() == null ? Convert.ToInt32((dataItem.FindControl("hfPackageFormat") as HiddenField).Value.ToString()) : 2;

            var linkTitle = dataItem.FindControl("btnLaunch") as Button;
            var lblScore = dataItem.FindControl("lblScore") as Label;
            var lblScoreMarker = dataItem.FindControl("lblScoreMarker") as Label;
            var lblStatus = dataItem.FindControl("lblStatus") as Label;
            var lblStatusMarker = dataItem.FindControl("lblStatusMarker") as Label;

            Panel pnlScoreContainer = dataItem.FindControl("pnlScoreContainer") as Panel;
            Session["Launch"] = linkTitle;
            linkTitle.Text = FileName;

            if (DataHelper.GetCurrentUserCIM() == "")
            {
                //linkTitle.Enabled = false;
                linkTitle.ForeColor = Color.LightGray;
                linkTitle.Attributes["onclick"] = "showPleaseSignin(3); return false;";
                //linkTitle.OnClientClick = "showPleaseSignin2(); return false;";
                lblScore.Visible = false;
                lblScoreMarker.Visible = false;
                lblStatus.Visible = false;
                lblStatusMarker.Visible = false;
                pnlScoreContainer.CssClass = "col-md-12 display-none";
            }
            else
            {
                if (coursetypeID == "1") //NON SCORM
                {

                    if (packageFormat == 1)
                        ScoTitle = "eLearning Course";
                    else if (packageFormat == 2)
                        ScoTitle = "Assessment";
                    else if (packageFormat == 4)
                        ScoTitle = "Survey";
                    else
                        ScoTitle = "";

                    string directoryPath;
                    //string isAssessment = "false";

                    directoryPath = getHotLink(scoID, scoTypeID);
                    //isAssessment = "true";

                    //string jsFunction = "";
                    //if (scoTypeID == 2)
                    //    linkTitle.Attributes["onclick"] = string.Format("openNonScormAssessment('{0}', '{1}', {2});return false;", HttpUtility.HtmlEncode(directoryPath), HttpUtility.UrlEncode(UTF8.EncryptText(scoID.ToString())), global_UserCIM);
                    //else


                    if (scoTypeID == 1)
                    {
                        //directoryPath = string.Format("{0}", "NONSCORM/FileUploads/" + global_CourseID + "/" + FileName + "/story.html");
                        directoryPath = string.Format("{0}", "NONSCORM/FileUploads/" + global_CourseID + "/" + FileName + "/");
                        var ext = new List<string> { ".html", ".htm" };
                        try
                        {


                            if (File.Exists(Server.MapPath(directoryPath + "story.html")))
                                directoryPath = string.Format("{0}", "NONSCORM/FileUploads/" + global_CourseID + "/" + FileName + "/story.html");
                            else if (File.Exists(Server.MapPath(directoryPath + "story_html5.html")))
                                directoryPath = string.Format("{0}", "NONSCORM/FileUploads/" + global_CourseID + "/" + FileName + "/story_html5.html");
                            else if (File.Exists(Server.MapPath(directoryPath + "index.html")))
                                directoryPath = string.Format("{0}", "NONSCORM/FileUploads/" + global_CourseID + "/" + FileName + "/index.html");
                            else if (File.Exists(Server.MapPath(directoryPath + "index.htm")))
                                directoryPath = string.Format("{0}", "NONSCORM/FileUploads/" + global_CourseID + "/" + FileName + "/index.htm");
                            else
                            {
                                var indexFile = Directory.GetFiles(Server.MapPath(directoryPath), "*.*", SearchOption.TopDirectoryOnly)
                                .Where(s => ext.Contains(Path.GetExtension(s)))
                                .Select(f => Path.GetFileName(f))
                                .First();

                                directoryPath = string.Format("{0}", "NONSCORM/FileUploads/" + global_CourseID + "/" + FileName + "/" + indexFile);
                            }

                            directoryPath = string.Format("openNonScormNewTab('{0}', {1}, '{2}', {3});return false;", directoryPath, global_UserCIM, Utils.Encrypt(scoID), scoTypeID);
                        }
                        catch (DirectoryNotFoundException)
                        {
                            
                            //throw;
                        }

                    }
                    if (scoTypeID == 2)
                    {
                        lblScore.Visible = true;
                        lblScoreMarker.Visible = true;

                        pnlScoreContainer.CssClass = "col-md-12";
                        lblScore.Text = Score;

                    }

                    if (Convert.ToInt32(SuccessStatus) > 0)
                        lblStatus.Text = "Completed";
                    else if (Convert.ToInt32(CompletionStatus) > 0)
                        lblStatus.Text = "In Progress";
                    else
                        lblStatus.Text = "Not Taken";

                    if (DataHelper.GetCurrentUserCIM() != "")
                    {
                        linkTitle.Attributes["onclick"] = directoryPath; //string.Format("openNonScorm('{0}', '{1}', {2}, '{3}');return false;", directoryPath, global_UserCIM, isAssessment, Utils.Encrypt(global_CourseID));
                    }

                }
                else
                { //SCORM
                    if (Convert.ToInt32(AttemptID == "" ? "0" : AttemptID) == 0)
                    {
                        lblStatus.Text = "Not Started";
                    }
                    else
                    {
                        if (packageFormat == 2) // SCORM 2004
                        {
                            if (Convert.ToInt32(CompletionStatus) == 0 && Convert.ToInt32(Score) > 0)
                            {
                                //gridBundle.Rebind();
                                //lvCourseDefault_Learner.Rebind();
                                TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();
                                if (Convert.ToInt32(CompletionStatus) == 0 && Convert.ToInt32(Score) > 0)
                                {

                                    var package = db.pr_TranscomUniversity_lkp_PackageDetails(global_userEmail, global_CourseID, global_CurriculumCourseID)
                                        .Where(p => p.AttemptID == Convert.ToInt32(AttemptID)).SingleOrDefault();
                                    CompletionStatus = package.AttemptStatus == null ? "0" : package.AttemptStatus.ToString();
                                }

                            }
                            else
                            {
                                switch (Convert.ToInt32(CompletionStatus))
                                {
                                    case 0:
                                        lblStatus.Text = "In Progress";
                                        linkTitle.ToolTip = "Continue Training";
                                        break;
                                    case 1:
                                        if (scoTypeID == 1 || scoTypeID == 3)
                                        {
                                            lblStatus.Text = "Completed";
                                        }
                                        else if (scoTypeID == 2)
                                        {
                                            if (Convert.ToInt32(SuccessStatus) > 0)
                                                lblStatus.Text = "Completed";
                                            else if (Convert.ToInt32(CompletionStatus) > 0)
                                                lblStatus.Text = "In Progress";
                                            else
                                                lblStatus.Text = "Not Taken";
                                        }
                                        else
                                        {
                                            lblStatus.Text = "Completed";
                                        }

                                        linkTitle.ToolTip = "Training Already Completed";
                                        break;
                                    case 2:
                                        lblStatus.Text = "In Progress";
                                        linkTitle.ToolTip = "Continue Training";
                                        break;
                                    case 3:
                                        lblStatus.Text = "Not Attempted";
                                        linkTitle.ToolTip = "Launch Course";
                                        break;
                                }
                            }
                        }
                        else if (packageFormat == 1) // SCORM 1.2
                        {
                            switch (LessonStatus)
                            {
                                case "Incomplete":
                                    lblStatus.Text = "In Progress";
                                    linkTitle.ToolTip = "Continue Training";
                                    break;
                                case "Completed":
                                    lblStatus.Text = "Completed";
                                    linkTitle.ToolTip = "Review Training";
                                    break;
                                case "Passed":
                                    lblStatus.Text = "Passed";
                                    linkTitle.ToolTip = "Review Training";
                                    break;
                                case "Failed":
                                    lblStatus.Text = "Failed";
                                    linkTitle.ToolTip = "Review Training";
                                    break;
                                default:
                                    lblStatus.Text = "In Progress";
                                    linkTitle.ToolTip = "Continue Training";
                                    break;
                            }
                        }
                    }

                    if (linkTitle != null)
                    {
                        if (scoTypeID != 5)
                        {

                            if (Convert.ToInt32(AttemptID == "" ? "0" : AttemptID) == 0)
                            {
                                //this.value = 'Launching...'; this.disabled = true; this.disabled = 'disabled'; 
                                linkTitle.Attributes["onclick"] =
                                string.Format("OpenTraining('Pkg:{0}','{1}',{2},{3} );return false;", RedirectID, Utils.Encrypt(scoID), scoTypeID, global_UserCIM);
                            }
                            else
                            {
                                //this.value = 'Launching...'; this.disabled = true; this.disabled = 'disabled'; 
                                linkTitle.Attributes["onclick"] =
                                string.Format("OpenTraining('Att:{0}','{1}',{2},{3} );return false;", AttemptID, Utils.Encrypt(scoID), scoTypeID, global_UserCIM);
                            }
                        }
                    }

                    if (scoTypeID != 2)
                    {
                        lblScore.Visible = false;
                        lblScoreMarker.Visible = false;
                    }
                    else
                    {
                        pnlScoreContainer.CssClass = "col-md-12";
                        lblScore.Text = Score;
                    }
                    if (scoTypeID == 3 || scoTypeID == 5 || scoTypeID == 6 || scoTypeID == 7 || scoTypeID == 8)
                    {
                        lblStatusMarker.Visible = false;
                        lblStatus.Visible = false;

                        //Raymark - Launch other resources(Learner)
                        //string asiaPath = HttpContext.Current.Session["AsiaCountry"].ToString();

                        //if ("Philippines" == asiaPath || "Australia" == asiaPath)
                        //{
                        //    var targetAsiaFolder = ConfigurationManager.AppSettings["OtherAsiaLaunchResourcesDirectoryPath"].ToString();
                        //    string completeAsiaPath = string.Format(targetAsiaFolder + "/{0}", FileName.Replace(" ", string.Empty));
                        //    linkTitle.Attributes["onclick"] = string.Format("javascript: this.value = 'Launching...'; this.disabled = true; this.disabled = 'disabled'; return window.open('{0}', '_blank');", completeAsiaPath);
                        //}
                        //else
                        //{
                        //var targetFolder = ConfigurationManager.AppSettings["OtherResourcesDirectoryPath"].ToString();
                        //string completePath = Page.ResolveClientUrl(targetFolder.Replace("\\", "//") + "/" + FileName);
                        linkTitle.Attributes["onclick"] = completePath(FileName, scoID, scoTypeID);
                        //    string.Format("javascript: this.value = 'Launching...'; this.disabled = true; this.disabled = 'disabled'; return window.open('{0}', '_blank'); ", completePath);
                        //}
                    }


                    //if (((DataHelper.isOverDue(global_CourseID) || (!DataHelper.isEnrolled(global_UserCIM, global_CourseID) && DataHelper.isCourseEnrollmentWithApproval(global_CourseID) == true) || !DataHelper.hasCourseStarted(global_CourseID)) && DataHelper.isCourseOpen(global_CourseID) == false || DataHelper.isEnrollmentAllowed(global_CourseID) == false) || !DataHelper.isLoggedIn())

                }

                linkTitle.Enabled = true;

                if ((DataHelper.isOverDue(global_CourseID, global_TimezoneOffset) && !DataHelper.isEnrolled(global_UserCIM, global_CourseID)) || !DataHelper.isEnrolled(global_UserCIM, global_CourseID) || !DataHelper.isLoggedIn() || !isValid)
                {
                    linkTitle.Enabled = false;
                }
                else if (DataHelper.isEnrolled(global_UserCIM, global_CourseID))
                {
                    linkTitle.Enabled = true;

                    //if (scoTypeID == 1)
                    if (isFirstLR)
                    {
                        linkTitle.Enabled = true;
                        isFirstLR = false;
                    }else{
                        if (scoTypeID == 2)
                        {
                            if (donePrevResource == true)
                                linkTitle.Enabled = true;
                            else
                                linkTitle.Enabled = false;
                        }
                    }

                    if (CompletionStatus == "1")
                        donePrevResource = true;
                    else
                    {
                        donePrevResource = false;
                    }

                }
                else
                {
                    linkTitle.Enabled = false;
                }

            }

        }
    }

    protected void lvCourseDefault_Learner_SectionDataBound(object sender, RadListViewItemEventArgs e)
    {
        var dataItem = e.Item as RadListViewDataItem;
        if (dataItem != null)
        {
            RadListView lvItem = dataItem.FindControl("lvCourseDefault_Learner_Item") as RadListView;
            int sectionID = Convert.ToInt32(lvCourseDefault_Learner.DataKeyValues[dataItem.DataItemIndex]["SectionID"].ToString());


            foreach (var currid in CURRCOURSEID)
            {
                var db = new TranscomUniversityV3ModelContainer();
                //var dt1 = db.pr_TranscomUniversity_lkp_PackageDetails(global_userEmail, global_CourseID, currid)
                //         .Where(p => p.SectionID == sectionID && (p.SCOTYPEID >= 1 && p.SCOTYPEID <= 4))
                //         .AsEnumerable();
                var dt1 = resources.Where(p => p.SectionID == sectionID).ToList();
                if (dt1 != null)
                {
                    lvItem.DataSource = dt1;
                    lvItem.DataBind();
                }
                else
                {
                    lvItem.DataSource = null;
                    lvItem.DataBind();
                }

                //var dt1 = DataHelper.scoPackage   (global_userEmail, currid, global_CourseID)
                //            .AsQueryable()
                //            .Where(p => p.SectionID == sectionID && (p.ScoTypeId >= 1 && p.ScoTypeId <= 4))
                //            .Select(p => new
                //            {
                //                ScoId = p.scoid,
                //                ScoPackageId = p.ScoPackageId,
                //                ScoTitle = p.ScoTitle,
                //                ScoTypeId = p.ScoTypeId,
                //                ScoType = p.ScoType,
                //                CourseTypeID = p.CourseTypeID,
                //                curriculumcourseID = p.curriculumcourseID,
                //                CourseOrder = p.CourseOrder,
                //                p.SectionID,
                //                p.dripEnable,
                //                SectionName = p.SectionName == null ? "NotSection" : p.SectionName,
                //                p.TestCategoryID
                //            })
                //.ToList();


            }
        }
    }

    //protected void gridBundle_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    //{
    //    var db = new TranscomUniversityV3ModelContainer();
    //    var scormCourses = ScormDataHelper.Get_ScormPackage_ByCurriculumId(global_CourseID, global_userEmail);       

    //    gridBundle.DataSource = scormCourses;
    //    setProgress();
    //}

    private void setProgress()
    {
        //set progressbar in client

        var db = new TranscomUniversityV3ModelContainer();


        //if (DataHelper.GetCourseType(global_CourseID) == 2)
        //{
        //var overProgress = ScormDataHelper.GetOverallProgressByCourseID(global_userEmail, global_CourseID);
        //var courseStatus = db.pr_TranscomUniversity_getCourseProgressStatus(global_CourseID, global_userEmail).SingleOrDefault();
        //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", Utils.callClientScript("setProgressAndStatus", overProgress.ToString(), courseStatus.Status), true);
        string stat = "";
        int statID = 0;
        int? isInProgress = db.tbl_TranscomUniversity_Cor_CourseLogin
                                .Where(c => c.CourseID == global_CourseID && c.EnrolleeCIM == global_UserCIM)
                                .Select(c => c.CourseID)
                                .FirstOrDefault();
        if (totalCompletedResources == totalResources && totalCompletedResources != 0)
        {
            statID = 1;
            stat = "Completed";
            InsertCertificates(global_CourseID);
        }
        else if (totalCompletedResources > 0 || isInProgress > 0)
        {
            statID = 2;
            stat = "In Progress";
        }
        else
            stat = "Not yet Started";

        decimal userRating = db.vw_TranscomUniversity_CourseReview.Where(u => u.EnrolleeCIM == global_UserCIM && u.CourseID == global_CourseID).Select(u => (decimal)u.Rating).SingleOrDefault();
        string comment = db.vw_TranscomUniversity_CourseReview.Where(u => u.EnrolleeCIM == global_UserCIM && u.CourseID == global_CourseID).Select(u => u.Comment).SingleOrDefault();
        string showRating = "false";
        if ((userRating == 0 || comment.Trim().Length == 0) && statID == 1)
            showRating = "true";


        ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", Utils.callClientScript("setStatus", stat, statID.ToString(), showRating), true);


        //}
        //else
        //{
        //    var overProgress = db.pr_TranscomUniversity_GetNonScormProgress(global_UserCIM, global_CourseID).SingleOrDefault().NonScormProgress;
        //    var courseStatus = (int)overProgress > 0 ? "In Progress" : "Not Started";
        //    ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", Utils.callClientScript("setProgressAndStatus", overProgress.ToString(), courseStatus), true);

        //}


    }

    //protected void GridBundlePreRender(object sender, EventArgs e)
    //{
    //    gridBundle.MasterTableView.ExpandCollapseColumn.Display = false;
    //    if (Page.IsPostBack)
    //    {
    //        foreach (GridDataItem item in gridBundle.Items)
    //        {
    //            if (!item.Expanded || item.Selected) continue;
    //            item.Expanded = true;
    //            item.Selected = true;
    //        }
    //    }
    //}

    //protected void gridPackageScosItemDataBound(object sender, GridItemEventArgs e)
    //{
    //    if (e.Item is GridDataItem)
    //    {
    //        var dataItem = (GridDataItem)e.Item;

    //        var FileName = dataItem.GetDataKeyValue("FileName").ToString();
    //        var scoTypeID = Convert.ToInt32(dataItem.GetDataKeyValue("SCOTYPEID"));
    //        var ScoTitle = dataItem.GetDataKeyValue("ScoTitle").ToString();
    //        var AttemptID = dataItem.GetDataKeyValue("AttemptID") == null ? "0" : dataItem.GetDataKeyValue("AttemptID").ToString();
    //        var RedirectID = dataItem.GetDataKeyValue("RedirectID") ?? 0;
    //        var CompletionStatus = dataItem.GetDataKeyValue("AttemptStatus") == null ? "0" : dataItem.GetDataKeyValue("AttemptStatus").ToString();
    //        var SuccessStatus = dataItem.GetDataKeyValue("SuccessStatus").ToString();
    //        var ScoPackageID = dataItem.GetDataKeyValue("ScoPackageID").ToString() ?? "0";
    //        var encCourseId = Server.UrlEncode(UTF8.EncryptText(ScoPackageID));
    //        var coursetypeID = dataItem.GetDataKeyValue("coursetypeID").ToString() ?? "0";
    //        var EncryptedOrManifest = dataItem.GetDataKeyValue("EncryptedOrManifest") == null ? string.Empty : dataItem.GetDataKeyValue("EncryptedOrManifest").ToString();
    //        var Score = dataItem.GetDataKeyValue("Score") == null ? "0" : dataItem.GetDataKeyValue("Score").ToString();
    //        var LessonStatus = dataItem.GetDataKeyValue("LessonStatus").ToString();
    //        int? packageFormat = dataItem.GetDataKeyValue("PackageFormat") != DBNull.Value ? Convert.ToInt32(dataItem.GetDataKeyValue("PackageFormat")) : 2;

    //        var linkTitle = dataItem["Launch"].FindControl("btnLaunch") as Button;
    //        var lblScore = dataItem["Progress"].FindControl("lblScore") as Label;
    //        var lblScoreMarker = dataItem["Progress"].FindControl("lblScoreMarker") as Label;
    //        var lblStatus = dataItem["Progress"].FindControl("lblStatus") as Label;
    //        var lblStatusMarker = dataItem["Progress"].FindControl("lblStatusMarker") as Label;

    //        Session["Launch"] = linkTitle;
    //        linkTitle.Text = FileName;

    //        if (DataHelper.GetCurrentUserCIM() == "")
    //        {
    //            //linkTitle.Enabled = false;
    //            linkTitle.ForeColor = Color.LightGray;
    //            linkTitle.Attributes["onclick"] = "showPleaseSignin(3); return false;";
    //            //linkTitle.OnClientClick = "showPleaseSignin2(); return false;";
    //            lblScore.Visible = false;
    //            lblScoreMarker.Visible = false;
    //            lblStatus.Visible = false;
    //            lblStatusMarker.Visible = false;
    //        }
    //        else
    //        {
    //            if (Convert.ToInt32(AttemptID == "" ? "0" : AttemptID) == 0)
    //            {
    //                lblStatus.Text = "Not Started";
    //            }
    //            else
    //            {
    //                if (packageFormat == 2) // SCORM 2004
    //                {
    //                    if (Convert.ToInt32(CompletionStatus) == 0 && Convert.ToInt32(Score) > 0)
    //                    {
    //                        //gridBundle.Rebind();
    //                        TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();
    //                        while (Convert.ToInt32(CompletionStatus) == 0 && Convert.ToInt32(Score) > 0)
    //                        {

    //                            var package = db.pr_TranscomUniversity_lkp_PackageDetails(global_userEmail, global_CourseID, global_CurriculumCourseID)
    //                                .Where(p => p.AttemptID == Convert.ToInt32(AttemptID)).SingleOrDefault();
    //                            CompletionStatus = package.AttemptStatus == null ? "0" : package.AttemptStatus.ToString();
    //                        }

    //                    }
    //                    else
    //                    {
    //                        switch (Convert.ToInt32(CompletionStatus))
    //                        {
    //                            case 0:
    //                                lblStatus.Text = "In Progress";
    //                                linkTitle.ToolTip = "Continue Training";
    //                                break;
    //                            case 1:
    //                                if (scoTypeID == 1 || scoTypeID == 3)
    //                                {
    //                                    lblStatus.Text = "Completed";
    //                                }
    //                                else if (scoTypeID == 2)
    //                                {
    //                                    lblStatus.Text = SuccessStatus;
    //                                }
    //                                else
    //                                {
    //                                    lblStatus.Text = "Completed";
    //                                }

    //                                linkTitle.ToolTip = "Training Already Completed";
    //                                break;
    //                            case 2:
    //                                lblStatus.Text = "In Progress";
    //                                linkTitle.ToolTip = "Continue Training";
    //                                break;
    //                            case 3:
    //                                lblStatus.Text = "Not Attempted";
    //                                linkTitle.ToolTip = "Launch Course";
    //                                break;
    //                        }
    //                    }
    //                }
    //                else if (packageFormat == 1) // SCORM 1.2
    //                {
    //                    switch (LessonStatus)
    //                    {
    //                        case "Incomplete":
    //                            lblStatus.Text = "In Progress";
    //                            linkTitle.ToolTip = "Continue Training";
    //                            break;
    //                        case "Completed":
    //                            lblStatus.Text = "Completed";
    //                            linkTitle.ToolTip = "Review Training";
    //                            break;
    //                        case "Passed":
    //                            lblStatus.Text = "Passed";
    //                            linkTitle.ToolTip = "Review Training";
    //                            break;
    //                        case "Failed":
    //                            lblStatus.Text = "Failed";
    //                            linkTitle.ToolTip = "Review Training";
    //                            break;
    //                        default:
    //                            lblStatus.Text = "In Progress";
    //                            linkTitle.ToolTip = "Continue Training";
    //                            break;
    //                    }
    //                }
    //            }

    //            if (linkTitle != null)
    //            {
    //                if (scoTypeID != 5)
    //                {
    //                    if (Convert.ToInt32(coursetypeID) == 1)
    //                    {
    //                        var urlWithParams = string.Format("CourseRedirect.aspx?CourseID={0}&TestCategoryID={1}", encCourseId,
    //                                             EncryptedOrManifest);
    //                        linkTitle.Attributes["onclick"] = string.Format("javascript: this.value = 'Launching...'; this.disabled = true; this.disabled = 'disabled'; return window.open('{0}', '_blank');", urlWithParams);
    //                    }
    //                    else
    //                    {
    //                        if (Convert.ToInt32(AttemptID == "" ? "0" : AttemptID) == 0)
    //                        {
    //                            linkTitle.Attributes["onclick"] =
    //                            string.Format("javascript: this.value = 'Launching...'; this.disabled = true; this.disabled = 'disabled'; return OpenTraining('Pkg:{0}');", RedirectID);
    //                        }
    //                        else
    //                        {
    //                            linkTitle.Attributes["onclick"] =
    //                            string.Format("javascript: this.value = 'Launching...'; this.disabled = true; this.disabled = 'disabled'; return OpenTraining('Att:{0}');", AttemptID);
    //                        }
    //                    }
    //                }
    //            }

    //            if (scoTypeID != 2)
    //            {
    //                lblScore.Visible = false;
    //                lblScoreMarker.Visible = false;
    //            }
    //            else
    //            {
    //                lblScore.Text = Score;
    //            }
    //            if (scoTypeID == 3 || scoTypeID == 5 || scoTypeID == 6 || scoTypeID == 7 || scoTypeID == 8)
    //            {
    //                lblStatusMarker.Visible = false;
    //                lblStatus.Visible = false;

    //                //Raymark - Launch other resources(Learner)
    //                //string asiaPath = HttpContext.Current.Session["AsiaCountry"].ToString();

    //                //if ("Philippines" == asiaPath || "Australia" == asiaPath)
    //                //{
    //                //    var targetAsiaFolder = ConfigurationManager.AppSettings["OtherAsiaLaunchResourcesDirectoryPath"].ToString();
    //                //    string completeAsiaPath = string.Format(targetAsiaFolder + "/{0}", FileName.Replace(" ", string.Empty));
    //                //    linkTitle.Attributes["onclick"] = string.Format("javascript: this.value = 'Launching...'; this.disabled = true; this.disabled = 'disabled'; return window.open('{0}', '_blank');", completeAsiaPath);
    //                //}
    //                //else
    //                //{
    //                var targetFolder = ConfigurationManager.AppSettings["OtherResourcesDirectoryPath"].ToString();
    //                string completePath = Page.ResolveClientUrl(targetFolder.Replace("\\", "//") + "/" + FileName);
    //                linkTitle.Attributes["onclick"] = string.Format("javascript: this.value = 'Launching...'; this.disabled = true; this.disabled = 'disabled'; return window.open('{0}', '_blank');", completePath);
    //                //}
    //            }


    //            if (((DataHelper.isOverDue(global_CourseID) || (!DataHelper.isEnrolled(global_UserCIM, global_CourseID) && DataHelper.isCourseEnrollmentWithApproval(global_CourseID) == true) || !DataHelper.hasCourseStarted(global_CourseID)) && DataHelper.isCourseOpen(global_CourseID) == false || DataHelper.isEnrollmentAllowed(global_CourseID) == false) || !DataHelper.isLoggedIn())
    //            {
    //                linkTitle.Enabled = false;
    //            }
    //            else if ((DataHelper.isEnrolled(global_UserCIM, global_CourseID) || !DataHelper.isOverDue(global_CourseID) || DataHelper.hasCourseStarted(global_CourseID)))
    //            {
    //                linkTitle.Enabled = true;

    //                if (scoTypeID == 1)
    //                {
    //                    linkTitle.Enabled = true;
    //                    if (CompletionStatus == "1")
    //                        donePrevResource = true;
    //                }
    //                else
    //                {
    //                    if (donePrevResource == true)
    //                        linkTitle.Enabled = true;
    //                    else
    //                        linkTitle.Enabled = false;

    //                    if (CompletionStatus == "1")
    //                        donePrevResource = true;
    //                    else
    //                    {
    //                        donePrevResource = false;
    //                    }
    //                }
    //            }
    //        }

    //    }
    //}

    protected void GridBundleItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridNestedViewItem)
        {
            //var db = new SCORMDBDataContext();
            //var dataItem = (GridNestedViewItem)e.Item;
            //var dataItem2 = (pr_TranscomUniversity_lkp_GetScormPackage_Result)dataItem.DataItem;
            //var lblDesc1 = (Label)dataItem.FindControl("lblDesc1");
            //var lblTitle1 = (Label)dataItem.FindControl("lblTitle1");

            //lblDesc1.Text = dataItem2.Description;
            //lblTitle1.Text = dataItem2.Title;

            GridNestedViewItem item = e.Item as GridNestedViewItem;
            RadGrid scos = item.FindControl("gridPackageScos") as RadGrid;
            global_CurriculumCourseID = Convert.ToInt32(DataBinder.Eval(item.DataItem, "curriculumcourseID").ToString());
            global_CourseID = Convert.ToInt32(DataBinder.Eval(item.DataItem, "CurriculumId").ToString());
            var ds = DataHelper.scoPackage(global_userEmail, global_CurriculumCourseID, global_CourseID);
            scos.DataSource = ds;
            scos.DataBind();
        }
    }

    protected void dsPackageScosr_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@Email"].Value = global_userEmail;
    }

    public string completePath(string filename, int scoID, int scoTypeID)
    {
        var targetFolder = ConfigurationManager.AppSettings["OtherResourcesDirectoryPath"].ToString();
        string path = Page.ResolveClientUrl(targetFolder.Replace("\\", "//") + "/" + filename);
        //return string.Format("javascript: this.value = 'Launching...'; this.disabled = true; this.disabled = 'disabled'; window.open('{0}', '_blank');return false;", completePath);
        //return string.Format("javascript: this.value = 'Launching...'; this.disabled = true; this.disabled = 'disabled'; openNonScorm('{0}', '{1}', {2}, '{3}');return false;", completePath, enrolleeCIM, "false", Utils.Encrypt(CourseID));
        //return string.Format("openNonScorm('{0}', '{1}', {2}, '{3}');return false;", completePath, enrolleeCIM, null, Utils.Encrypt(CourseID));
        return string.Format("openNonScormNewTab('{0}', {1}, '{2}', {3});return false;", path, global_UserCIM, Utils.Encrypt(scoID), scoTypeID);
    }

    public string getHotLink(int? TestCategoryID, int scoTypeID)
    {
        string testlink = "";
        if (HttpContext.Current.Request.Url.Host == "localhost")
            testlink = ResolveUrl("~/") + "TakeAssessment.aspx?CourseID=" + Utils.Encrypt(global_CourseID) + "&tid=" + HttpUtility.UrlEncode(UTF8.EncryptText(TestCategoryID.ToString()));
        //testlink = ResolveUrl("~/") + "hotlink.aspx?CourseID=" + WebUtility.HtmlEncode(Utils.Encrypt(global_CourseID)) + "&tid=" + WebUtility.HtmlEncode(UTF8.EncryptText(TestCategoryID.ToString()));
        else
            testlink = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + ResolveUrl("~/") + "TakeAssessment.aspx?tid=" + HttpUtility.UrlEncode(UTF8.EncryptText(TestCategoryID.ToString())) + "&CourseID=" + Utils.Encrypt(global_CourseID);
        //testlink = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + ResolveUrl("~/") + "hotlink.aspx?tid=" + HttpUtility.UrlEncode(UTF8.EncryptText(TestCategoryID.ToString())) + "&CourseID=" + Utils.Encrypt(global_CourseID);

        //return string.Format(testlink);
        return string.Format("openNonScormSamePage('{0}', {1}, '{2}', {3});return false;", testlink, global_UserCIM, Utils.Encrypt(TestCategoryID), scoTypeID);
    }

    protected void InsertCertificates(int? courseID)
    {
        var googleId = DataHelper.GetGoogleID();
        TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();
        db.pr_TranscomUniversity_InsertInternalCertificates(googleId, courseID);

    }

    #region FileExtension

    public static bool isFile(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (!isPdf(filename) && !isWord(filename) && !isExcel(filename) && !isPpt(filename) && !isImg(filename) && !isZip(filename) && !isMp3(filename) && !isMp4(filename))
            return true;
        else
            return false;
    }

    public static bool isPdf(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (ext == "pdf")
            return true;
        else
            return false;
    }

    public static bool isWord(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (ext == "doc" || ext == "docx")
            return true;
        else
            return false;
    }

    public static bool isExcel(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (ext == "xls" || ext == "xlsx")
            return true;
        else
            return false;
    }

    public static bool isPpt(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (ext == "ppt" || ext == "pptx")
            return true;
        else
            return false;
    }

    public static bool isImg(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (ext == "jpg" || ext == "jpeg" || ext == "png")
            return true;
        else
            return false;
    }

    public static bool isZip(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (ext == "rar" || ext == "zip")
            return true;
        else
            return false;
    }

    public static bool isMp3(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (ext == "mp3")
            return true;
        else
            return false;
    }

    public static bool isMp4(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (ext == "mp4")
            return true;
        else
            return false;
    }

    #endregion
}