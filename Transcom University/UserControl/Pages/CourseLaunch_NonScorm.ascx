﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CourseLaunch_NonScorm.ascx.cs"
    Inherits="UserControl_Pages_CourseLaunch_NonScorm" %>
<asp:Panel ID="pnlMain" runat="server" ClientIDMode="Static">
    <div id="wrapper">
        <div id="Header">
        </div>
        <div id="Body">
            <div class="col-md-14" style="margin-left: 1%; margin-right: 1%">
                <div class="col-md-12">
                    <asp:Panel ID="pnlLearnerResourceLv" runat="server">
                        <div id="divLearnerResourceLv" class="demo-container col-md-14">
                            <rad:RadListView ID="lvCourseDefault_Learner_NS" runat="server" ItemPlaceholderID="SectionHolder"
                                AllowPaging="false" OnItemDataBound="lvCourseDefault_Learner_NS_SectionDataBound"
                                OnNeedDataSource="lvCourseDefault_Learner_NS_NeedDataSource" DataKeyNames="SectionID">
                                <LayoutTemplate>
                                    <div id="SectionHolder" runat="server">
                                    </div>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <asp:Panel ID="pnlSection" runat="server" CssClass="panel panel-default">
                                        <div class="panel-heading panel-heading-black display-flex">
                                            <h4 class="panel-title">
                                                <a id="A1" data-toggle="collapse" class="js-collapse-event no-underline-hover" data-parent="#CourseAccordion"
                                                    href='<%# String.Format("#_{0}", Eval("SectionID")) %>' onclick="tabClick(this);">
                                                    <%# Eval("SectionName") %>
                                                </a><i id="I10" class="fa fa-sort-down pull-right hover-pointer js-collapse-event"
                                                    runat="server" data-toggle="collapse" data-parent="#CourseAccordion" href='<%#String.Format("#_{0}", Eval("SectionID")) %>'
                                                    onclick="tabClick(this);"></i>
                                            </h4>
                                        </div>
                                        <div id='<%#String.Format("_{0}", Eval("SectionID")) %>' class='panel-collapse collapse in <%# Eval("SectionName").ToString().Replace(" ", "") %>'>
                                            <div class="panel-body">
                                                <div id="ContentAccordion">
                                                    <rad:RadListView ID="lvCourseDefault_Learner_NS_Item" runat="server" ItemPlaceholderID="ItemHolder"
                                                        AllowPaging="false" DataKeyNames="ScoID" OnItemDataBound="lvCourseDefault_Learner_NS_Item_OnItemDataBound">
                                                        <LayoutTemplate>
                                                            <div id="ItemHolder" runat="server">
                                                            </div>
                                                        </LayoutTemplate>
                                                        <ItemTemplate>
                                                            <div class="group">
                                                                <div class="row panel-gray panel-course-category">
                                                                    <div class="col-xs-11" title='<%#Eval("displayTitle")%>'>
                                                                        <div class="vertical-align col-xs-6 col-md-4 col-lg-4">
                                                                            <asp:Image ID="ImgELearning" runat="server" ImageUrl="~/Media/Images/elearning.png" />
                                                                            <asp:Button runat="server" ID="btnLaunch" ClientIDMode="Static" Text='<%#Eval("FileName")%>'
                                                                                CssClass="btn font-bold btn-transparent linkBtn" ToolTip="Launch Course" />
                                                                        </div>
                                                                        <asp:Label ID="lblScoTitle" runat="server" CssClass="vertical-align"></asp:Label>
                                                                        <asp:HiddenField ID="hfScoID" runat="server" Value='<%#Eval("ScoID")%>' />
                                                                        <asp:HiddenField ID="hfTitle" runat="server" Value='<%#Eval("Title")%>' />
                                                                        <asp:HiddenField ID="hfscoTypeID" runat="server" Value='<%#Eval("FileName")%>' />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </ItemTemplate>
                                                        <EmptyDataTemplate>                                                            
                                                            <asp:Label runat="server" ID="lblThisSectionHasNoItemToDisplay" Text="<%$ Resources:LocalizedResource, ThisSectionHasNoItemToDisplay %>" ></asp:Label>
                                                        </EmptyDataTemplate>
                                                    </rad:RadListView>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </ItemTemplate>
                                <EmptyDataTemplate>
                                <asp:Label runat="server" ID="lblRecommendedLeadershipCoursesForYou" Text=" <%$ Resources:LocalizedResource, NoResourcetodisplay%>" ></asp:Label>
                                </EmptyDataTemplate>
                                <EmptyItemTemplate>
                                   <asp:Label runat="server" ID="lblRecommendedLeadershipCoursesForYou" Text=" <%$ Resources:LocalizedResource, NoResourcetodisplay%>" ></asp:Label>
                                </EmptyItemTemplate>
                            </rad:RadListView>
                        </div>
                    </asp:Panel>
                </div>
            </div>
        </div>
    </div>
</asp:Panel>
