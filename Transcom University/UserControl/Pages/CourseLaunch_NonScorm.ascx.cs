﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Telerik.Web.UI;
using System.Web.Security;
using TranscomUniversityV3Model;
using DotNetSCORM;
using NuComm.Security.Encryption;
using System.Drawing;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using NuSkill.Business;
using System.Net;



public partial class UserControl_Pages_CourseLaunch_NonScorm : System.Web.UI.UserControl
{
    private DataHelper _db;
    private static SqlConnection _oconn;
    private static SqlCommand _ocmd;
    public static string EncCourseId
    {
        get
        {
            var value = HttpContext.Current.Session["EncCourseIdSession"];
            return value.ToString();
        }
        set { HttpContext.Current.Session["EncCourseIdSession"] = value; }
    }
    static int global_CurriculumCourseID; //identity of sco
    static int global_CourseID; //also called CurriculumId;
    static int global_UserCIM;
    static string global_userEmail;
    static int global_TestID;
    static int global_courseType;
    private static List<int> CURRCOURSEID;
    bool donePrevResource;
    private static int totalResources;
    private static int totalCompletedResources;

    private static IEnumerable<TranscomUniversityV3Model.pr_TranscomUniversity_lkp_PackageDetails_Result> resources;

    private static void Dbconn(string connStr)
    {
        _oconn = new SqlConnection
        {
            ConnectionString = ConfigurationManager.ConnectionStrings[connStr].ConnectionString
        };
        _oconn.Open();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            totalResources = 0;
            totalCompletedResources = 0;

            //if logged in
            if (Request.QueryString["CourseID"] != null)
            {

                global_CourseID = Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"]));
                global_courseType = (int)DataHelper.GetCourseType(global_CourseID);

                CURRCOURSEID = new List<int>();

                if (DataHelper.GetCurrentUserCIM() != "")
                {
                    global_UserCIM = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
                    global_userEmail = DataHelper.getCurrentUserEmail();

                }

                if (DataHelper.GetCourseType(global_CourseID) == 1)
                {
                    if (CURRCOURSEID == null || CURRCOURSEID.Count == 0)
                    {
                        Dbconn("DefaultConnection");
                        _ocmd = new SqlCommand("pr_Scorm_Cor_GetCurriculumPackages", _oconn)
                        {
                            CommandType = CommandType.StoredProcedure
                        };

                        _ocmd.Parameters.AddWithValue("@CurriculumID", global_CourseID);
                        _ocmd.Parameters.AddWithValue("@isBundle", 1);
                        SqlDataReader oDataReader = _ocmd.ExecuteReader();
                        if (oDataReader.Read())
                        {
                            do
                            {
                                CURRCOURSEID.Add(Convert.ToInt32(oDataReader["curriculumcourseID"].ToString()));
                            } while (oDataReader.Read());
                        }

                    }

                    //EncCourseId = Request.QueryString["CourseID"] ?? string.Empty;
                    //global_CourseID = Convert.ToInt32(Utils.Decrypt(EncCourseId));
                    donePrevResource = false;
                    lvCourseDefault_Learner_NS.Rebind();
                }

               


            }
            //if logged out


        }
        setProgress();
    }

    protected void lvCourseDefault_Learner_NS_NeedDataSource(object sender, EventArgs e)
    {
        try
        {
            foreach (var currid in CURRCOURSEID)
            {
                var dtResources = DataHelper.GetCourseSections(global_CourseID, 1);
                lvCourseDefault_Learner_NS.DataSource = dtResources;

                var db = new TranscomUniversityV3ModelContainer();
                //resources = db.pr_TranscomUniversity_NonScormPackageDetails(global_UserCIM, global_CourseID, currid)
                resources = db.pr_TranscomUniversity_lkp_PackageDetails(global_userEmail, global_CourseID, currid)
                            .ToList();
                    //.Where(p => p.SectionID == sectionID && (p.SCOTypeID >= 1 && p.SCOTypeID <= 4))

                totalResources = resources.ToList().Count();
                totalCompletedResources = resources.ToList().Count(c => c.SuccessStatus > 0);

                resources = resources
                            .Where(p => p.SCOTYPEID >= 1 && p.SCOTYPEID <= 4)
                            .AsQueryable();

                //totalResources = resources.ToList().Count();
                //totalCompletedResources = resources.ToList().Count(c => c.SuccessStatus > 0);

            }

        }
        catch (Exception ex)
        {
            throw;
        }
    }

    protected void lvCourseDefault_Learner_NS_Item_OnItemDataBound(object sender, RadListViewItemEventArgs e)
    {
        if (e.Item is RadListViewItem)
        {
            var dataItem = (RadListViewItem)e.Item;

            //    var FileName = (dataItem.FindControl("hfFileName") as HiddenField).Value.ToString();
            var scoID = Convert.ToInt32((dataItem.FindControl("hfScoID") as HiddenField).Value.ToString());
            var scoTypeID = Convert.ToInt32((dataItem.FindControl("hfscoTypeID") as HiddenField).Value.ToString());
            var foldername = (dataItem.FindControl("hfTitle") as HiddenField).Value.ToString();

            Label scoTitle = dataItem.FindControl("lblScoTitle") as Label;
            var linkTitle = dataItem.FindControl("btnLaunch") as Button;
            if (scoTypeID == 1)
                scoTitle.Text = "eLearning Course";
            else if (scoTypeID == 2)
                scoTitle.Text = "Assessment";
            else if (scoTypeID == 3)
                scoTitle.Text = "Survey";
            else
                scoTitle.Text = "";

            string directoryPath;
           // string isAssessment = "false";

            if (scoTypeID == 1)
            {
                directoryPath = string.Format("{0}", "NONSCORM/FileUploads/" + global_CourseID + "/" + foldername + "/story.html");
                if (File.Exists(Server.MapPath(directoryPath)))
                    directoryPath = string.Format("{0}", "NONSCORM/FileUploads/" + global_CourseID + "/" + foldername + "/story.html");
                else
                    directoryPath = string.Format("{0}", "NONSCORM/FileUploads/" + global_CourseID + "/" + foldername + "/index.html");

                 directoryPath = string.Format("openNonScorm('{0}', {1}, '{2}', {3});return false;", directoryPath, global_UserCIM, Utils.Encrypt(scoID), scoTypeID); 
            }
            else if (scoTypeID == 2 || scoTypeID == 4)
            {
                directoryPath = getHotLink(scoID, scoTypeID);
                //isAssessment = "true";
            }
            else
            {
                directoryPath = completePath(foldername, scoID, scoTypeID); //string.Format("{0}", "SCORM/OtherResources/" + foldername);
            }

            //string jsFunction = "";
            //if (scoTypeID == 2)
            //    linkTitle.Attributes["onclick"] = string.Format("openNonScormAssessment('{0}', '{1}', {2});return false;", HttpUtility.HtmlEncode(directoryPath), HttpUtility.UrlEncode(UTF8.EncryptText(scoID.ToString())), global_UserCIM);
            //else
            if (DataHelper.GetCurrentUserCIM() != "")
            {
                linkTitle.Attributes["onclick"] = directoryPath; //string.Format("openNonScorm('{0}', '{1}', {2}, '{3}');return false;", directoryPath, global_UserCIM, isAssessment, Utils.Encrypt(global_CourseID));


                if (!DataHelper.isEnrolled(global_UserCIM, global_CourseID))
                    linkTitle.Enabled = false;
            }
            else
            {
                linkTitle.Enabled = false;
            }
        }
    }

    protected void lvCourseDefault_Learner_NS_SectionDataBound(object sender, RadListViewItemEventArgs e)
    {
        try
        {
            var dataItem = e.Item as RadListViewDataItem;
            if (dataItem != null)
            {
                RadListView lvItem = dataItem.FindControl("lvCourseDefault_Learner_NS_Item") as RadListView;
                int sectionID = Convert.ToInt32(lvCourseDefault_Learner_NS.DataKeyValues[dataItem.DataItemIndex]["SectionID"].ToString());

                foreach (var currid in CURRCOURSEID)
                {

                    //var dt1 = db.pr_TranscomUniversity_lkp_PackageDetails(global_userEmail, global_CourseID,currid )
                    //          .Where(p => p.SectionID == sectionID && (p.SCOTYPEID >= 1 && p.SCOTYPEID <= 4))
                    //          .ToList();

                    var dt1 = resources
                              .Where(p => p.SectionID == sectionID && (p.SCOTYPEID >= 1 && p.SCOTYPEID <= 4))
                              .ToList();

                    //var dt1 = DataHelper.scoPackage(global_userEmail, currid, global_CourseID)
                    //            .AsQueryable()
                    //            .Where(p => p.SectionID == sectionID && (p.ScoTypeId >= 1 && p.ScoTypeId <= 4))
                    //            .Select(p => new
                    //            {
                    //                ScoId = p.scoid,
                    //                ScoPackageId = p.ScoPackageId,
                    //                ScoTitle = p.ScoTitle,
                    //                ScoTypeId = p.ScoTypeId,
                    //                ScoType = p.ScoType,
                    //                CourseTypeID = p.CourseTypeID,
                    //                curriculumcourseID = p.curriculumcourseID,
                    //                CourseOrder = p.CourseOrder,
                    //                p.SectionID,
                    //                p.dripEnable,
                    //                SectionName = p.SectionName == null ? "NotSection" : p.SectionName,
                    //                p.TestCategoryID
                    //            })
                    //.ToList();

                    lvItem.DataSource = dt1;
                    lvItem.DataBind();
                }
            }
        }
        catch (Exception)
        {

            throw;
        }

    }

    //protected void gridBundle_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    //{
    //    var db = new TranscomUniversityV3ModelContainer();
    //    var scormCourses = ScormDataHelper.Get_ScormPackage_ByCurriculumId(global_CourseID, global_userEmail);       

    //    gridBundle.DataSource = scormCourses;
    //    setProgress();
    //}

    private void setProgress()
    {
        var db = new TranscomUniversityV3ModelContainer();


        if (DataHelper.GetCourseType(global_CourseID) == 1)
        {
            //var overProgress = db.pr_TranscomUniversity_GetNonScormProgress(global_UserCIM, global_CourseID).SingleOrDefault().NonScormProgress;
            //string courseStatus = "";
            //if ((int)overProgress == 100)
            //{
            //    courseStatus = "Completed";
            //}
            //else if ((int)overProgress > 0)
            //{
            //    courseStatus = "In Progress";
            //}
            //else
            //{
            //    courseStatus = "Not Started";
            //}
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", Utils.callClientScript("setProgressAndStatus", overProgress.ToString(), courseStatus), true);

            string stat = "";
            int statID = 0;
            if (totalCompletedResources == totalResources && totalResources != 0)
            {
                statID = 1;
                stat = "Completed";
            }
            else if (totalCompletedResources > 0)
            {
                statID = 2;
                stat = "In Progress";
            }
            else
                stat = "Not yet Started";

            ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", Utils.callClientScript("setStatus", stat, statID.ToString()), true);

        }
        //else
        //{

        //}
    }

    //protected void GridBundlePreRender(object sender, EventArgs e)
    //{
    //    gridBundle.MasterTableView.ExpandCollapseColumn.Display = false;
    //    if (Page.IsPostBack)
    //    {
    //        foreach (GridDataItem item in gridBundle.Items)
    //        {
    //            if (!item.Expanded || item.Selected) continue;
    //            item.Expanded = true;
    //            item.Selected = true;
    //        }
    //    }
    //}

    //protected void gridPackageScosItemDataBound(object sender, GridItemEventArgs e)
    //{
    //    if (e.Item is GridDataItem)
    //    {
    //        var dataItem = (GridDataItem)e.Item;

    //        var FileName = dataItem.GetDataKeyValue("FileName").ToString();
    //        var scoTypeID = Convert.ToInt32(dataItem.GetDataKeyValue("SCOTYPEID"));
    //        var ScoTitle = dataItem.GetDataKeyValue("ScoTitle").ToString();
    //        var AttemptID = dataItem.GetDataKeyValue("AttemptID") == null ? "0" : dataItem.GetDataKeyValue("AttemptID").ToString();
    //        var RedirectID = dataItem.GetDataKeyValue("RedirectID") ?? 0;
    //        var CompletionStatus = dataItem.GetDataKeyValue("AttemptStatus") == null ? "0" : dataItem.GetDataKeyValue("AttemptStatus").ToString();
    //        var SuccessStatus = dataItem.GetDataKeyValue("SuccessStatus").ToString();
    //        var ScoPackageID = dataItem.GetDataKeyValue("ScoPackageID").ToString() ?? "0";
    //        var encCourseId = Server.UrlEncode(UTF8.EncryptText(ScoPackageID));
    //        var coursetypeID = dataItem.GetDataKeyValue("coursetypeID").ToString() ?? "0";
    //        var EncryptedOrManifest = dataItem.GetDataKeyValue("EncryptedOrManifest") == null ? string.Empty : dataItem.GetDataKeyValue("EncryptedOrManifest").ToString();
    //        var Score = dataItem.GetDataKeyValue("Score") == null ? "0" : dataItem.GetDataKeyValue("Score").ToString();
    //        var LessonStatus = dataItem.GetDataKeyValue("LessonStatus").ToString();
    //        int? packageFormat = dataItem.GetDataKeyValue("PackageFormat") != DBNull.Value ? Convert.ToInt32(dataItem.GetDataKeyValue("PackageFormat")) : 2;

    //        var linkTitle = dataItem["Launch"].FindControl("btnLaunch") as Button;
    //        var lblScore = dataItem["Progress"].FindControl("lblScore") as Label;
    //        var lblScoreMarker = dataItem["Progress"].FindControl("lblScoreMarker") as Label;
    //        var lblStatus = dataItem["Progress"].FindControl("lblStatus") as Label;
    //        var lblStatusMarker = dataItem["Progress"].FindControl("lblStatusMarker") as Label;

    //        Session["Launch"] = linkTitle;
    //        linkTitle.Text = FileName;

    //        if (DataHelper.GetCurrentUserCIM() == "")
    //        {
    //            //linkTitle.Enabled = false;
    //            linkTitle.ForeColor = Color.LightGray;
    //            linkTitle.Attributes["onclick"] = "showPleaseSignin(3); return false;";
    //            //linkTitle.OnClientClick = "showPleaseSignin2(); return false;";
    //            lblScore.Visible = false;
    //            lblScoreMarker.Visible = false;
    //            lblStatus.Visible = false;
    //            lblStatusMarker.Visible = false;
    //        }
    //        else
    //        {
    //            if (Convert.ToInt32(AttemptID == "" ? "0" : AttemptID) == 0)
    //            {
    //                lblStatus.Text = "Not Started";
    //            }
    //            else
    //            {
    //                if (packageFormat == 2) // SCORM 2004
    //                {
    //                    if (Convert.ToInt32(CompletionStatus) == 0 && Convert.ToInt32(Score) > 0)
    //                    {
    //                        //gridBundle.Rebind();
    //                        TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();
    //                        while (Convert.ToInt32(CompletionStatus) == 0 && Convert.ToInt32(Score) > 0)
    //                        {

    //                            var package = db.pr_TranscomUniversity_lkp_PackageDetails(global_userEmail, global_CourseID, global_CurriculumCourseID)
    //                                .Where(p => p.AttemptID == Convert.ToInt32(AttemptID)).SingleOrDefault();
    //                            CompletionStatus = package.AttemptStatus == null ? "0" : package.AttemptStatus.ToString();
    //                        }

    //                    }
    //                    else
    //                    {
    //                        switch (Convert.ToInt32(CompletionStatus))
    //                        {
    //                            case 0:
    //                                lblStatus.Text = "In Progress";
    //                                linkTitle.ToolTip = "Continue Training";
    //                                break;
    //                            case 1:
    //                                if (scoTypeID == 1 || scoTypeID == 3)
    //                                {
    //                                    lblStatus.Text = "Completed";
    //                                }
    //                                else if (scoTypeID == 2)
    //                                {
    //                                    lblStatus.Text = SuccessStatus;
    //                                }
    //                                else
    //                                {
    //                                    lblStatus.Text = "Completed";
    //                                }

    //                                linkTitle.ToolTip = "Training Already Completed";
    //                                break;
    //                            case 2:
    //                                lblStatus.Text = "In Progress";
    //                                linkTitle.ToolTip = "Continue Training";
    //                                break;
    //                            case 3:
    //                                lblStatus.Text = "Not Attempted";
    //                                linkTitle.ToolTip = "Launch Course";
    //                                break;
    //                        }
    //                    }
    //                }
    //                else if (packageFormat == 1) // SCORM 1.2
    //                {
    //                    switch (LessonStatus)
    //                    {
    //                        case "Incomplete":
    //                            lblStatus.Text = "In Progress";
    //                            linkTitle.ToolTip = "Continue Training";
    //                            break;
    //                        case "Completed":
    //                            lblStatus.Text = "Completed";
    //                            linkTitle.ToolTip = "Review Training";
    //                            break;
    //                        case "Passed":
    //                            lblStatus.Text = "Passed";
    //                            linkTitle.ToolTip = "Review Training";
    //                            break;
    //                        case "Failed":
    //                            lblStatus.Text = "Failed";
    //                            linkTitle.ToolTip = "Review Training";
    //                            break;
    //                        default:
    //                            lblStatus.Text = "In Progress";
    //                            linkTitle.ToolTip = "Continue Training";
    //                            break;
    //                    }
    //                }
    //            }

    //            if (linkTitle != null)
    //            {
    //                if (scoTypeID != 5)
    //                {
    //                    if (Convert.ToInt32(coursetypeID) == 1)
    //                    {
    //                        var urlWithParams = string.Format("CourseRedirect.aspx?CourseID={0}&TestCategoryID={1}", encCourseId,
    //                                             EncryptedOrManifest);
    //                        linkTitle.Attributes["onclick"] = string.Format("javascript: this.value = 'Launching...'; this.disabled = true; this.disabled = 'disabled'; return window.open('{0}', '_blank');", urlWithParams);
    //                    }
    //                    else
    //                    {
    //                        if (Convert.ToInt32(AttemptID == "" ? "0" : AttemptID) == 0)
    //                        {
    //                            linkTitle.Attributes["onclick"] =
    //                            string.Format("javascript: this.value = 'Launching...'; this.disabled = true; this.disabled = 'disabled'; return OpenTraining('Pkg:{0}');", RedirectID);
    //                        }
    //                        else
    //                        {
    //                            linkTitle.Attributes["onclick"] =
    //                            string.Format("javascript: this.value = 'Launching...'; this.disabled = true; this.disabled = 'disabled'; return OpenTraining('Att:{0}');", AttemptID);
    //                        }
    //                    }
    //                }
    //            }

    //            if (scoTypeID != 2)
    //            {
    //                lblScore.Visible = false;
    //                lblScoreMarker.Visible = false;
    //            }
    //            else
    //            {
    //                lblScore.Text = Score;
    //            }
    //            if (scoTypeID == 3 || scoTypeID == 5 || scoTypeID == 6 || scoTypeID == 7 || scoTypeID == 8)
    //            {
    //                lblStatusMarker.Visible = false;
    //                lblStatus.Visible = false;

    //                //Raymark - Launch other resources(Learner)
    //                //string asiaPath = HttpContext.Current.Session["AsiaCountry"].ToString();

    //                //if ("Philippines" == asiaPath || "Australia" == asiaPath)
    //                //{
    //                //    var targetAsiaFolder = ConfigurationManager.AppSettings["OtherAsiaLaunchResourcesDirectoryPath"].ToString();
    //                //    string completeAsiaPath = string.Format(targetAsiaFolder + "/{0}", FileName.Replace(" ", string.Empty));
    //                //    linkTitle.Attributes["onclick"] = string.Format("javascript: this.value = 'Launching...'; this.disabled = true; this.disabled = 'disabled'; return window.open('{0}', '_blank');", completeAsiaPath);
    //                //}
    //                //else
    //                //{
    //                var targetFolder = ConfigurationManager.AppSettings["OtherResourcesDirectoryPath"].ToString();
    //                string completePath = Page.ResolveClientUrl(targetFolder.Replace("\\", "//") + "/" + FileName);
    //                linkTitle.Attributes["onclick"] = string.Format("javascript: this.value = 'Launching...'; this.disabled = true; this.disabled = 'disabled'; return window.open('{0}', '_blank');", completePath);
    //                //}
    //            }


    //            if (((DataHelper.isOverDue(global_CourseID) || (!DataHelper.isEnrolled(global_UserCIM, global_CourseID) && DataHelper.isCourseEnrollmentWithApproval(global_CourseID) == true) || !DataHelper.hasCourseStarted(global_CourseID)) && DataHelper.isCourseOpen(global_CourseID) == false || DataHelper.isEnrollmentAllowed(global_CourseID) == false) || !DataHelper.isLoggedIn())
    //            {
    //                linkTitle.Enabled = false;
    //            }
    //            else if ((DataHelper.isEnrolled(global_UserCIM, global_CourseID) || !DataHelper.isOverDue(global_CourseID) || DataHelper.hasCourseStarted(global_CourseID)))
    //            {
    //                linkTitle.Enabled = true;

    //                if (scoTypeID == 1)
    //                {
    //                    linkTitle.Enabled = true;
    //                    if (CompletionStatus == "1")
    //                        donePrevResource = true;
    //                }
    //                else
    //                {
    //                    if (donePrevResource == true)
    //                        linkTitle.Enabled = true;
    //                    else
    //                        linkTitle.Enabled = false;

    //                    if (CompletionStatus == "1")
    //                        donePrevResource = true;
    //                    else
    //                    {
    //                        donePrevResource = false;
    //                    }
    //                }
    //            }
    //        }

    //    }
    //}

    protected void GridBundleItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridNestedViewItem)
        {
            //var db = new SCORMDBDataContext();
            //var dataItem = (GridNestedViewItem)e.Item;
            //var dataItem2 = (pr_TranscomUniversity_lkp_GetScormPackage_Result)dataItem.DataItem;
            //var lblDesc1 = (Label)dataItem.FindControl("lblDesc1");
            //var lblTitle1 = (Label)dataItem.FindControl("lblTitle1");

            //lblDesc1.Text = dataItem2.Description;
            //lblTitle1.Text = dataItem2.Title;

            GridNestedViewItem item = e.Item as GridNestedViewItem;
            RadGrid scos = item.FindControl("gridPackageScos") as RadGrid;
            global_CurriculumCourseID = Convert.ToInt32(DataBinder.Eval(item.DataItem, "curriculumcourseID").ToString());
            global_CourseID = Convert.ToInt32(DataBinder.Eval(item.DataItem, "CurriculumId").ToString());
            var ds = DataHelper.scoPackage(global_userEmail, global_CurriculumCourseID, global_CourseID);
            scos.DataSource = ds;
            scos.DataBind();
        }
    }

    protected void dsPackageScosr_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@Email"].Value = global_userEmail;
    }


    #region FileExtension

    public static bool isFile(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (!isPdf(filename) && !isWord(filename) && !isExcel(filename) && !isPpt(filename) && !isImg(filename) && !isZip(filename) && !isMp3(filename) && !isMp4(filename))
            return true;
        else
            return false;
    }

    public static bool isPdf(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (ext == "pdf")
            return true;
        else
            return false;
    }

    public static bool isWord(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (ext == "doc" || ext == "docx")
            return true;
        else
            return false;
    }

    public static bool isExcel(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (ext == "xls" || ext == "xlsx")
            return true;
        else
            return false;
    }

    public static bool isPpt(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (ext == "ppt" || ext == "pptx")
            return true;
        else
            return false;
    }

    public static bool isImg(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (ext == "jpg" || ext == "jpeg" || ext == "png")
            return true;
        else
            return false;
    }

    public static bool isZip(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (ext == "rar" || ext == "zip")
            return true;
        else
            return false;
    }

    public static bool isMp3(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (ext == "mp3")
            return true;
        else
            return false;
    }

    public static bool isMp4(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (ext == "mp4")
            return true;
        else
            return false;
    }

    #endregion

    public string completePath(string filename, int scoID, int scoTypeID)
    {
        var targetFolder = ConfigurationManager.AppSettings["OtherResourcesDirectoryPath"].ToString();
        string path = Page.ResolveClientUrl(targetFolder.Replace("\\", "//") + "/" + filename);
        //return string.Format("javascript: this.value = 'Launching...'; this.disabled = true; this.disabled = 'disabled'; window.open('{0}', '_blank');return false;", completePath);
        //return string.Format("javascript: this.value = 'Launching...'; this.disabled = true; this.disabled = 'disabled'; openNonScorm('{0}', '{1}', {2}, '{3}');return false;", completePath, enrolleeCIM, "false", Utils.Encrypt(CourseID));
        //return string.Format("openNonScorm('{0}', '{1}', {2}, '{3}');return false;", completePath, enrolleeCIM, null, Utils.Encrypt(CourseID));
        return string.Format("openNonScorm('{0}', {1}, '{2}', {3});return false;", path, global_UserCIM, Utils.Encrypt(scoID), scoTypeID);
    }

    public string getHotLink(int? TestCategoryID, int scoTypeID)
    {
        string testlink = "";
        if (HttpContext.Current.Request.Url.Host == "localhost")
            testlink = ResolveUrl("~/") + "TakeAssessment.aspx?CourseID=" + Utils.Encrypt(global_CourseID) + "&tid=" + HttpUtility.UrlEncode(UTF8.EncryptText(TestCategoryID.ToString()));
        //testlink = ResolveUrl("~/") + "hotlink.aspx?CourseID=" + WebUtility.HtmlEncode(Utils.Encrypt(global_CourseID)) + "&tid=" + WebUtility.HtmlEncode(UTF8.EncryptText(TestCategoryID.ToString()));
        else
            testlink = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + ResolveUrl("~/") + "TakeAssessment.aspx?tid=" + HttpUtility.UrlEncode(UTF8.EncryptText(TestCategoryID.ToString())) + "&CourseID=" + Utils.Encrypt(global_CourseID);
        //testlink = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + ResolveUrl("~/") + "hotlink.aspx?tid=" + HttpUtility.UrlEncode(UTF8.EncryptText(TestCategoryID.ToString())) + "&CourseID=" + Utils.Encrypt(global_CourseID);

        //return string.Format(testlink);
        return string.Format("openNonScorm('{0}', {1}, '{2}', {3});return false;", testlink, global_UserCIM, Utils.Encrypt(TestCategoryID), scoTypeID);
    }

}