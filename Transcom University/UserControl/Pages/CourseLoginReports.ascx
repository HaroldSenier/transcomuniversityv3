﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CourseLoginReports.ascx.cs" Inherits="UserControl_Pages_CourseLoginReports" %>
<rad:RadScriptBlock ID="RadScriptBlock" runat="server">
    <script type="text/javascript">
        function pageLoad() {
            highlightSelectedSubmenu();
        }
    </script>
</rad:RadScriptBlock>
<rad:RadAjaxManagerProxy runat="server" ID="ramLoginReports">
    <AjaxSettings>
        <rad:AjaxSetting AjaxControlID="rgCourseLogins">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlCourseLoginContainer" LoadingPanelID="localLoadingPanel" />
            </UpdatedControls>
        </rad:AjaxSetting>
    </AjaxSettings>
</rad:RadAjaxManagerProxy>
<div class="col-md-12">
    <asp:Panel ID="pnlCourseLoginContainer" CssClass="container" runat="server" style="overflow-x:scroll;">
        <rad:RadGrid RenderMode="Lightweight" ID="rgCourseLogins" Width="100%" AllowFilteringByColumn="True"
            DataSourceID="SqlDataSource1" AllowSorting="True" AllowPaging="True" PageSize="10"
            runat="server" AutoGenerateColumns="False" ShowStatusBar="true" EnableLinqExpressions="false" CssClass="GridLess">
            <ExportSettings HideStructureColumns="true" Excel-Format="ExcelML" ExportOnlyData="true" />
            <MasterTableView DataKeyNames="CIMNumber" TableLayout="Fixed" CommandItemDisplay="Top">
                <CommandItemSettings ShowExportToExcelButton="true" ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                <Columns>
                    <rad:GridBoundColumn UniqueName="CIMNumber" DataField="CIMNumber" HeaderText="<%$ Resources:LocalizedResource, CimNumber %>"
                        HeaderStyle-Width="200px" HeaderStyle-ForeColor="Teal">
                        <FilterTemplate>
                            <rad:RadComboBox RenderMode="Lightweight" ID="CIMNumber" DataSourceID="dsCIMNumber"
                                DataTextField="CIMNumber" DataValueField="CIMNumber" Width="180px" AppendDataBoundItems="true"
                                SelectedValue='<%# ((GridItem)Container).OwnerTableView.GetColumn("CIMNumber").CurrentFilterValue %>'
                                runat="server" OnClientSelectedIndexChanged="CIMNumberIndexChanged">
                                <Items>
                                    <rad:RadComboBoxItem Text="All" />
                                </Items>
                            </rad:RadComboBox>
                            <rad:RadScriptBlock ID="RadScriptBlock0" runat="server">
                                <script type="text/javascript">
                                    function CIMNumberIndexChanged(sender, args) {
                                        var tableView = $find("<%# ((GridItem)Container).OwnerTableView.ClientID %>");
                                        tableView.filter("CIMNumber", args.get_item().get_value(), "EqualTo");
                                    }
                                </script>
                            </rad:RadScriptBlock>
                        </FilterTemplate>
                    </rad:GridBoundColumn>
                    <rad:GridBoundColumn UniqueName="FirstName" DataField="FirstName" HeaderText="<%$ Resources:LocalizedResource, FirstName%>"
                        HeaderStyle-Width="200px" AllowFiltering="false" HeaderStyle-ForeColor="Teal">
                    </rad:GridBoundColumn>
                    <rad:GridBoundColumn UniqueName="LastName" DataField="LastName" HeaderText="<%$ Resources:LocalizedResource, LastName%>"
                        HeaderStyle-Width="200px" AllowFiltering="false" HeaderStyle-ForeColor="Teal">
                    </rad:GridBoundColumn>
                    <rad:GridBoundColumn UniqueName="Program" DataField="Program" HeaderText="<%$ Resources:LocalizedResource, Program%>"
                        HeaderStyle-Width="200px" HeaderStyle-ForeColor="Teal">
                        <FilterTemplate>
                            <rad:RadComboBox RenderMode="Lightweight" ID="Program" DataSourceID="dsProgram" DataTextField="Program"
                                DataValueField="Program" Width="180px" AppendDataBoundItems="true" SelectedValue='<%# ((GridItem)Container).OwnerTableView.GetColumn("Program").CurrentFilterValue %>'
                                runat="server" OnClientSelectedIndexChanged="ProgramIndexChanged">
                                <Items>
                                    <rad:RadComboBoxItem Text="All" />
                                </Items>
                            </rad:RadComboBox>
                            <rad:RadScriptBlock ID="RadScriptBlock1" runat="server">
                                <script type="text/javascript">
                                    function ProgramIndexChanged(sender, args) {
                                        var tableView = $find("<%# ((GridItem)Container).OwnerTableView.ClientID %>");
                                        tableView.filter("Program", args.get_item().get_value(), "EqualTo");
                                    }
                                </script>
                            </rad:RadScriptBlock>
                        </FilterTemplate>
                    </rad:GridBoundColumn>
                    <rad:GridBoundColumn UniqueName="ReportsTo" DataField="ReportsTo" HeaderText="<%$ Resources:LocalizedResource, ReportsTo%>"
                        HeaderStyle-Width="200px" HeaderStyle-ForeColor="Teal">
                        <FilterTemplate>
                            <rad:RadComboBox RenderMode="Lightweight" ID="ReportsTo" DataSourceID="dsReportsTo"
                                DataTextField="ReportsTo" DataValueField="ReportsTo" Width="180px" AppendDataBoundItems="true"
                                SelectedValue='<%# ((GridItem)Container).OwnerTableView.GetColumn("ReportsTo").CurrentFilterValue %>'
                                runat="server" OnClientSelectedIndexChanged="ReportsToIndexChanged">
                                <Items>
                                    <rad:RadComboBoxItem Text="All" />
                                </Items>
                            </rad:RadComboBox>
                            <rad:RadScriptBlock ID="RadScriptBlock2" runat="server">
                                <script type="text/javascript">
                                    function ReportsToIndexChanged(sender, args) {
                                        var tableView = $find("<%# ((GridItem)Container).OwnerTableView.ClientID %>");
                                        tableView.filter("ReportsTo", args.get_item().get_value(), "EqualTo");
                                    }
                                </script>
                            </rad:RadScriptBlock>
                        </FilterTemplate>
                    </rad:GridBoundColumn>
                    <rad:GridBoundColumn UniqueName="CourseName" DataField="CourseName" HeaderText="<%$ Resources:LocalizedResource, CourseName%>"
                        HeaderStyle-Width="200px" HeaderStyle-ForeColor="Teal">
                        <FilterTemplate>
                            <rad:RadComboBox RenderMode="Lightweight" ID="CourseName" DataSourceID="dsCourseName"
                                DataTextField="CourseName" DataValueField="CourseName" Width="180px" AppendDataBoundItems="true"
                                SelectedValue='<%# ((GridItem)Container).OwnerTableView.GetColumn("CourseName").CurrentFilterValue %>'
                                runat="server" OnClientSelectedIndexChanged="CourseNameIndexChanged">
                                <Items>
                                    <rad:RadComboBoxItem Text="All" />
                                </Items>
                            </rad:RadComboBox>
                            <rad:RadScriptBlock ID="RadScriptBlock3" runat="server">
                                <script type="text/javascript">
                                    function CourseNameIndexChanged(sender, args) {
                                        var tableView = $find("<%# ((GridItem)Container).OwnerTableView.ClientID %>");
                                        tableView.filter("CourseName", args.get_item().get_value(), "EqualTo");
                                    }
                                </script>
                            </rad:RadScriptBlock>
                        </FilterTemplate>
                    </rad:GridBoundColumn>
                    <rad:GridDateTimeColumn FilterControlWidth="135px" DataField="Login" HeaderText="<%$ Resources:LocalizedResource, Login%>"
                        PickerType="DatePicker" EnableRangeFiltering="true" HeaderStyle-Width="400px" HeaderStyle-ForeColor="Teal">
                    </rad:GridDateTimeColumn>
                </Columns>
            </MasterTableView>
        </rad:RadGrid>
    </asp:Panel>
</div>
<asp:SqlDataSource ID="SqlDataSource1" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
    ProviderName="System.Data.SqlClient" SelectCommand="SELECT * FROM vw_TranscomUniversity_Rpt_CourseLogins"
    runat="server"></asp:SqlDataSource>
<asp:SqlDataSource runat="server" ID="dsCIMNumber" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
    ProviderName="System.Data.SqlClient" SelectCommand="SELECT DISTINCT CIMNumber FROM vw_TranscomUniversity_Rpt_CourseLogins">
</asp:SqlDataSource>
<asp:SqlDataSource runat="server" ID="dsProgram" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
    ProviderName="System.Data.SqlClient" SelectCommand="SELECT DISTINCT Program FROM vw_TranscomUniversity_Rpt_CourseLogins">
</asp:SqlDataSource>
<asp:SqlDataSource runat="server" ID="dsReportsTo" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
    ProviderName="System.Data.SqlClient" SelectCommand="SELECT DISTINCT ReportsTo FROM vw_TranscomUniversity_Rpt_CourseLogins">
</asp:SqlDataSource>
<asp:SqlDataSource runat="server" ID="dsCourseName" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
    ProviderName="System.Data.SqlClient" SelectCommand="SELECT DISTINCT CourseName FROM vw_TranscomUniversity_Rpt_CourseLogins">
</asp:SqlDataSource>
