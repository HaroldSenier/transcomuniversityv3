﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControl_Pages_CourseManagement : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!Page.IsPostBack)
        {
            int leadershipCatID = 17;
            int skillDevCatID = 18;
            int mandaytoryCatID = 19;
            hfLeadershipCatID.Value = Utils.Encrypt(leadershipCatID);
            hfSkillDevCatID.Value = Utils.Encrypt(skillDevCatID);
            hfMandatoryCatID.Value = Utils.Encrypt(mandaytoryCatID);
            
        }
        
    }
}