﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CoursePreviewCommentSectionCtrl.ascx.cs"
    Inherits="UserControl_Pages_CoursePreviewCommentSectionCtrl" %>
<rad:RadScriptBlock ID="rsbComments" runat="server">
    <script type="text/javascript">
        function commentSetRating(rating, obj) {
            $(obj).val(rating);

        }
    </script>
</rad:RadScriptBlock>
<rad:RadListView ID="rdComments" runat="server" OnNeedDataSource="rdComments_NeedDataSource">
    <ItemTemplate>
        <div class="cr-comment-container container">
            <div class="row">
                <div class="col-xs-1">
                    <asp:Image ID="uImg" runat="server" ImageUrl='<%# Eval("UserImageUrl", "{0}") %>' onerror='this.src="default-user.png"'
                        CssClass="border-teal" AlternateText="User Image" />
                </div>
                <div class="col-xs-8">
                    <div class="row">
                        <div class="col-xs-12 display-inline-flex">
                            <asp:Label ID="lblName" runat="server" CssClass="cr-name"><%#Eval("Name")%></asp:Label>
                            <rad:RadRating RenderMode="Lightweight" ID="rrCommentRating" runat="server" ItemCount="5"
                                SelectionMode="Continuous" Precision="Item" Orientation="Horizontal" Enabled="false"
                                DbValue='<%# DataBinder.Eval(Container.DataItem, "Rating") %>' CssClass="cr-rating">
                            </rad:RadRating>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <p>
                                <%#Eval("Comment")%></p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-3">
                    <p class="pull-right">
                        <i>Date:
                            <%#Eval("DateModified")%>
                        </i>
                    </p>
                </div>
            </div>
        </div>
    </ItemTemplate>
</rad:RadListView>
