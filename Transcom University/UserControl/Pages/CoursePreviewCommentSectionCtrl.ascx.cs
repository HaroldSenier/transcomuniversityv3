﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class UserControl_Pages_CoursePreviewCommentSectionCtrl : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void rdComments_NeedDataSource(object sender, RadListViewNeedDataSourceEventArgs e)
    {
        int CourseID = Convert.ToInt16(Utils.Decrypt(Request.QueryString["CourseID"]));


        var courseComments = DataHelper.courseComments(CourseID);

        if (courseComments != null)
            rdComments.DataSource = courseComments;

    }
}