﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CoursePreviewUserCtrl.ascx.cs"
    Inherits="CoursePreviewUserCtrl" %>
<%--<%@ Register Src="~/UserControl/Pages/CourseForumUserCtrl.ascx" TagName="courseForum"
    TagPrefix="ucCourseForum" %>--%>
<%@ Register Src="~/UserControl/Pages/CourseLaunch.ascx" TagName="courseLaunch" TagPrefix="ucCourseLaunch" %>
<%@ Register Src="~/UserControl/Pages/CourseLaunch_NonScorm.ascx" TagName="courseLaunch_ns"
    TagPrefix="ucCourseLaunch_ns" %>
<rad:RadAjaxLoadingPanel ID="localLoadingPanel" runat="server" CssClass="Loading2"
    Transparency="25" />
<rad:RadAjaxLoadingPanel ID="defaultLoadingPanel" runat="server" Transparency="25"
    IsSticky="true" CssClass="Loading" />
<rad:RadScriptBlock runat="server" ID="rsbPreviewCourse">
    <script type="text/javascript">
        var ratingOldVal;

        function pageLoad() {
            $("#<%= BtnEnrolled.ClientID %>").unbind().click(function () {
                if ($(this).attr("href") == null)
                    radalert($('#<%= hfThiscourseiscurrentlyclose.ClientID %>').val(), 330, 180, $('#<%= hfNotOpen.ClientID %>').val(), "");
            });

            debugger;

            var uploadDate = convertTimeZone($("#<%= LblUploadDate.ClientID %>").text());
            var validDate = convertTimeZone($("#<%= LblValidDate.ClientID %>").text());
            var valid = convertTimeZone($("#<%= lblValid.ClientID %>").text());

            $("#<%= LblUploadDate.ClientID %>").text(moment(uploadDate).format("MMMM DD, YYYY"));
            $("#<%= LblValidDate.ClientID %>").text(moment(validDate).format("MMMM DD, YYYY"));
            
            if (valid != "" && valid != undefined && valid != null)
                $("#<%= lblValid.ClientID %>").text(moment(valid).format("MMMM DD, YYYY"));

            initializeDisplayedRating();
            
            if($("#forumMainContainer").length >0)
                initializeForum();
        }

        function resetRatingValue() {
            //alert("resetRatingValue");
            var oldVal = $("#" + "<%= hfOldValue.ClientID %>").val();
            $find("<%= rrCourse.ClientID %>").set_value(parseInt(oldVal));
            $("#" + "<%= hfRating.ClientID %>").val(oldVal);
            initializeDisplayedRating();
            //$('.star-rating').removeClass("rrtOver");

        }

        function clientRated(sender, args) {
            //alert("clientRated");
            var rating = args.get_newValue();
            $("#" + "<%= hfRating.ClientID %>").val(rating);
        }

        function confirmRating() {
            //alert("confirmRating");
            var wndw = $find("<%= radCommentWindow.ClientID %>");

            if (!wndw.isVisible()) {
                //alert("wndowisVisible");

                //                                var courseBG = $("#<%= BgCourse.ClientID %>");
                //                                var offset = courseBG.offset();
                //                                var width = courseBG.width();
                //                                var height = courseBG.height();
                //                                var commentWindow = $("#<%= radCommentWindow.ClientID %>");
                //                                var wdWidth = commentWindow.width() / 2;
                //                                var wdHeight = commentWindow.height() / 2;
                //                                var centerX = (offset.left + width / 2) - wdWidth;
                //                                var centerY = (offset.top + height / 2) - wdHeight;
                //                                wndw.moveTo(centerX, centerY);

                //                  var showRating = $("#hfShowRating").val();
                //                  if (showRating == "true" && statusID == 1) {
                wndw.show();
                //}
                //}
            }
        }

        function showCancelIfPending() {
            //$(".btn-hover-forAprroval").html("Cancel Enrollment").css({"background-color": "#bc8373 !important;"});
            $(".btn-hover-forAprroval").html("Cancel Enrollment").css("cssText", "background: #bc8373 !important;");
        }

        function hideCancelIfPending() {
            //$(".btn-hover-forAprroval").html("Pending Approval").css({"background-color": "lightgray !important;"});
            $(".btn-hover-forAprroval").html("Pending Enrollment").css("cssText", "background: lightgray !important;");
        }

        function openTakeCoursesWindow() {

            try {
                var tlcWindow = $find("<%= radTakeLaterCourses.ClientID %>");
                tlcWindow.set_behaviors(Telerik.Web.UI.WindowBehaviors.Close + Telerik.Web.UI.WindowBehaviors.Move);
                if (!($("#" + "<%= BtnTakeLater.ClientID %>").hasClass("aspNetDisabled"))) {
                    if ($("#" + "<%= lblFolderError.ClientID %>").length) {
                        $("#" + "<%= lblFolderError.ClientID %>").hide();
                    }
                    tlcWindow.show();
                }
            } catch (e) {
                showPleaseSignin(2);
            }

        }

        function openAddFolder() {
            $("#" + "<%= txtFolderName.ClientID %>").val("");
            $("#<%= txtFolderNameChars.ClientID %>").text("100");
            $find("<%= radAddFolder.ClientID %>").show();
        }

        function rdtTakeLaterCourses_Clicked(sender, args) {
            var node = args.get_node();
            if (node.get_level() > 1) {
                var parentNode = node.get_parent();
                $("#" + "<%= hfSelectedFolderName.ClientID %>").val(parentNode.get_text());
                $("#" + "<%= hfSelectedFolderID.ClientID %>").val(parentNode.get_value());
                parentNode.set_selected(true);
                //console.log(parentNode.get_value() + " " + selectedNode.get_text());
            } else if (node.get_level() == 1) {
                var tree = $find("<%= rdtTakeLaterCourses.ClientID %>");
                var selectedNode = tree.get_selectedNode();
                $("#" + "<%= hfSelectedFolderName.ClientID %>").val(selectedNode.get_text());
                $("#" + "<%= hfSelectedFolderID.ClientID %>").val(selectedNode.get_value());
                //console.log(selectedNode.get_value() + " " + selectedNode.get_text());
            }
        }

        function activateRadTakeCourses() {
            //debugger;
            $find("<%= radAddFolder.ClientID %>").close();
            $find("<%= radTakeLaterCourses.ClientID %>").setActive(true);
            hideLoading();
            radalert("Folder " + $("#txtFolderName").val().trim() + " "+ $('#<%= hfwassuccessfullycreated.ClientID %>').val(), 330 , 180, $('#<%= hfSuccess.ClientID %>').val());
        }

        function closeAddFolder() {
            $find("<%= radAddFolder.ClientID %>").close();
        }

        function closeTakeLaterCourses() {
            $find("<%= radTakeLaterCourses.ClientID %>").close();
        }

        function addRootNode(sender, args) {
            var nodes = sender.get_allNodes();
            nodes[0].set_imageUrl("Media/Images/tree-item-2.png");
            nodes[0].set_text("Take Later Courses");
            nodes[0].set_selected(true);
            nodes[0].set_expanded(true);
        }


        function BtnJoinForum_Click() {
            $find("<%= frGuideLine.ClientID %>").show();
            $('#frmIncludes').load('Includes/ForumGuideline.htm');
        }

        function hideAddFolder() {
            //disable close
            if($("#txtFolderName").val().trim() == ""){
                 radalert("Folder name is not valid.", 330 , 180, "Invalid Folder Name");
                 return false;
            }else{
                var addWindow = $find("<%= radAddFolder.ClientID %>");
            //$telerik.$(closeButton, sender.get_popupElement()).attr('disabled', 'disabled');

                addWindow.set_behaviors(Telerik.Web.UI.WindowBehaviors.None);
                var loading = $find("<%= localLoadingPanel.ClientID %>");
                loading.show("<%= pnlLaterCourses.ClientID %>");
                $find("<%= radAddFolder.ClientID%>").close();
            }
            
        }

        function confirmAddToTakeLater(sender, args) {
              var folderName = $("#<%= hfSelectedFolderName.ClientID %>").val().trim();
            //var courseId = QueryString("CourseID");
            var parentFileName = $("#<%= hfSelectedFolderName.ClientID %>").val().trim();
            var selectedFolderId = $("#<%= hfSelectedFolderID.ClientID %>").val().trim();
            var dpNotify = $find("<%= dpNotifyMeDate.ClientID %>").get_selectedDate();
           
               var callBackFunction = function (shouldSubmit) {
                    if (shouldSubmit) {
                        //initiate the original postback again
//                        sender.click();
//                            if (Telerik.Web.Browser.ff) { //work around a FireFox issue with form submission, see http://www.telerik.com/support/kb/aspnet-ajax/window/details/form-will-not-submit-after-radconfirm-in-firefox
//                                sender.get_element().click();
//                            }
                        debugger;
                        //var serverTime = convertTimeZone($("#masterServerDate").val());
                        var courseEnd = convertTimeZone($("#<%= LblValidDate.ClientID %>").text());

                        var dateNotify = new Date(dpNotify);

                        //var dateServer = new Date(moment(serverTime).format("MMMM DD, YYYY"));
                        var dateNow = new Date();
                        var dateEnd = new Date(moment(courseEnd).format("MMMM DD, YYYY"))

                        if (invalidNotifyDate) {
                            radalert("Invalid Date", 330, 180, $('#<%= hfErrorDate.ClientID %>').val(), "");
                        } else if (dateNotify <= dateNow) {
                            radalert($('#<%= hfNotifyDateshouldbegreaterthancurrentdate.ClientID %>').val(), 330, 180, $('#<%= hfErrorDate.ClientID %>').val(), "");
                        } else if (dateNotify > dateEnd) {
                            radalert($('#<%= hfNotifyDateshouldbelessthanexpirationdate.ClientID %>').val(), 330, 180, $('#<%= hfErrorDate.ClientID %>').val(), "");
                        } else {
                            sender.click();
                            if (Telerik.Web.Browser.ff) { //work around a FireFox issue with form submission, see http://www.telerik.com/support/kb/aspnet-ajax/window/details/form-will-not-submit-after-radconfirm-in-firefox
                                sender.get_element().click();
                            }
                        }
                    }
                };
                
                if(dpNotify == null || invalidNotifyDate != false){
                      radalert($('#<%= hfPleaseselectavalidDate.ClientID %>').val(), 330, 180, $('#<%= hfError.ClientID %>').val(), "");
                }
                else{
                var text = $('#<%= hfAreyousureyouwanttoaddthiscoursetothisfolder.ClientID %>').val() + " <b>" + folderName + "</b> ?";
                radconfirm(text, callBackFunction, 300, 200, null, "Confirm");
                 }
                //always prevent the original postback so the RadConfirm can work, it will be initiated again with code in the callback function
                args.set_cancel(true);

           
        }

        var invalidNotifyDate = true;
        function ValidateNotifyDate(sender, args) {
            
            if (invalidNotifyDate){
                var addToTakeLaterBtn = $find("<%= btnAddToTakeLater %>");
                addToTakeLaterBtn.set_enabled(false);
                 args.IsValid = false;
            }
        }

        function onDateError() {
            invalidNotifyDate = true;
        }

        function onDateBlur() {
            invalidNotifyDate = false;
        }

        function OnNotifyDateSelected() {

            invalidNotifyDate = false;
        }

        function AcceptForumGuide_Click() {
        debugger;
            $('#<%= btnAccept.ClientID %>').click();
            $("body").css({ "display": "none" });
            var loading = $find("<%= localLoadingPanel.ClientID %>");
            loading.show("<%= pnlTransactionList.ClientID %>");
            return false;

        }
        function CancelForumGuide_Click() {
            $find("<%= frGuideLine.ClientID %>").close();
        }
        function pageReload() {
            window.location.reload();
        }

        //similar course List initialization
        var similarCourseListView;

        function initializeSimilarCourse() {
            //$ = $telerik.$;
            //this will fire the OnCommand event
            similarCourseListView = $find("<%= lvSimilarCourses.ClientID%>");
            similarCourseListView.rebind();
            similarCourseListView.set_pageSize(6);
            similarCourseListView.page(0);

            $(".scPager .pagePrev").click(function (e) {

                similarCourseListView.page(similarCourseListView.get_currentPageIndex() - 1);

            });
            $(".scPager .pageNext").click(function (e) {

                similarCourseListView.page(similarCourseListView.get_currentPageIndex() + 1);

            });

//            $("#similarCourseListView").on("click", ".mc-item", function (e) {

//                similarCourseListView.toggleSelection($(this).index());

//            });

        }

        function bindSimilarCourseList() {
            similarCourseListView = $find("<%= lvSimilarCourses.ClientID %>");
            var startRowIndex = similarCourseListView.get_currentPageIndex() * similarCourseListView.get_pageSize(),
                maximumRows = similarCourseListView.get_pageSize(),
                sortExpression = similarCourseListView.get_sortExpressions().toLinq();
            //make a call to get the data
            var courseID = '<%= Request.QueryString["CourseID"] %>';
            PageMethods.GetSimilarCourseData(startRowIndex, maximumRows, sortExpression, courseID,

                function (result) {
                    PageMethods.GetSimilarCount(function (count) {
                        similarCourseListView.set_virtualItemCount(count);

                        if (count > 0) {
                            $(".scPager .pagePrev").removeClass("display-none");
                            $(".scPager .pageNext").removeClass("display-none");
                        }
                    });

                    similarCourseListView.set_dataSource(result);
                    //console.log(similarCourseListView);
                    $(".js-sc-loader").hide();
                    similarCourseListView.dataBind();

                },
                function (error) {
                    var code = error._statusCode;
                    if (code == 401)
                        location.reload();
                    else if (code == 500)
                        radalert($('<%= hfSomethingWentWhileSubmittingYourReportPleaseTryAgain.ClientID %>').val(), 330, 180, $('<%= hfErrorMessage.ClientID %>').val(), "");
                });
        }

        //Client Events similarCourse

        function scOnListViewDataBinding(sender, args) {
            sender.set_selectedIndexes([]);
        }

        function scOnListViewCommand(sender, args) {
            args.set_cancel(true);
            bindSimilarCourseList();
        }

        function hideLoading() {
            $(".Loading2").hide();
            $(".Loading").hide();
        }

        function confirmLeaveForumCallBackFn(arg) {
            var courseID = '<%= Request.QueryString["CourseID"] %>';
            if (arg) {
                PageMethods.btnLeaveForum_Click(courseID, function (result) {
                    //                    if (result == 1)
                    //                        location.reload();
                    //                    else
                    //                        radalert("Cannot join forum.", 330, 180, "Error Message", "");
                    $("body").css({ "display": "none" });
                    location.reload();
                },
                function (error) {
                    var code = error._statusCode;
                    if (code == 401)
                        location.reload();
                    else if (code == 500)
                        radalert($('<%= hfSomethingWentWhileSubmittingYourReportPleaseTryAgain.ClientID %>').val(), 330, 180, $('<%= hfErrorMessage.ClientID %>').val(), "");
                });
            } else {

                return false;
            }
        }

        function back() {
            window.history.go(-1);
        }

        function btnAddToTakeLater_OnClientClick() {
            $("#RadWindowWrapper_<%= radTakeLaterCourses.ClientID %>").find(".rwTitleBar").css({ "cursor": "not-allowed", "pointer-events": "none" });
            var tlcWindow = $find("<%= radTakeLaterCourses.ClientID %>");
            tlcWindow.set_behaviors(Telerik.Web.UI.WindowBehaviors.Close);
            $("#RadWindowWrapper_<%= radTakeLaterCourses.ClientID %>").find(".rwTitleBar").css({ "cursor": "move", "pointer-events": "all" });
            var tlcWindow = $find("<%= radTakeLaterCourses.ClientID %>");
            tlcWindow.set_behaviors(Telerik.Web.UI.WindowBehaviors.Close + Telerik.Web.UI.WindowBehaviors.Move);



        }

        function enableAddTakeLaterBehavior() {
            $("#RadWindowWrapper_<%= radTakeLaterCourses.ClientID %>").find(".rwTitleBar").css({ "cursor": "move", "pointer-events": "all" });
            var tlcWindow = $find("<%= radTakeLaterCourses.ClientID %>");
            tlcWindow.set_behaviors(Telerik.Web.UI.WindowBehaviors.Close + Telerik.Web.UI.WindowBehaviors.Move);

            tlcWindow.close();
            
            $("#<%= BtnTakeLater.ClientID %>").attr("onclick", "");
            $("#<%= BtnTakeLater.ClientID %>").attr("href", "javascript:void(0); return false;");
            $("#<%= BtnTakeLater.ClientID %>").removeClass('btn-teal');
            $("#<%= BtnTakeLater.ClientID %>").css({"background-color": "lightgray", "color": "white"});
            $("#<%= BtnTakeLater.ClientID %>").removeClass('btn-teal');
            //$("#<%= BtnTakeLater.ClientID %>").attr("disabled", "disabled");


        }

        function showLeaveForum() {
            var wndw = $find("<%= radLeaveForum.ClientID %>");
            wndw.show();
            var courseBG = $("#<%= BgCourse.ClientID %>");
            var offset = courseBG.offset();
            var width = courseBG.width();
            var height = courseBG.height();
            var commentWindow = $("#RadWindowWrapper_<%= radLeaveForum.ClientID %>");
            var wdWidth = commentWindow.width() / 2;
            var wdHeight = commentWindow.height() / 2;
            var centerX = (offset.left + width / 2) - wdWidth;
            var centerY = (offset.top + height / 2) - wdHeight;
            wndw.moveTo(centerX, centerY);

        }

        function closeLeaveForum() {
            var wndw = $find("<%= radLeaveForum.ClientID %>");
            wndw.close();
        }
        function setProgressAndStatus(hidValue, status) {
            //debugger;
            $('.progressBarContainer').each(function (i, obj) {
                $('#hdnOverall').val(hidValue);
                // var oldValue = $('#hfOldValue').val();
                jQuery('[id=spanval]', this).text(parseFloat(hidValue) + '%');

                var progressBarWidth = jQuery('[id=progressBar]', this).width() * (hidValue / 100);
                jQuery('[id=progress]', this).animate({ width: hidValue + "%" }, 3000).html("&nbsp;");
                //jQuery('[id=progress]', this).css({ "width": hidValue + "%" });

                jQuery('[id=spanval]', this).css({ "left": (progressBarWidth * .8) + 10 });


                if (hidValue >= 0 && hidValue <= 50) {
                    jQuery('[id=progress]', this).addClass('red');
                }
                if (hidValue >= 50 && hidValue <= 75) {
                    jQuery('[id=progress]', this).addClass('yellow');
                }
                if (hidValue >= 75 && hidValue <= 100) {
                    jQuery('[id=progress]', this).addClass('green');
                }
            });


            $("#lblCourseStatus").html(status);
            if ($("#hfIsDue").val() == 1 && $("#hdnOverall").val() != 100) {
                $("#lblCourseStatus").css({ "color": "red" })
            } else {
                if ($("#hdnOverall").val() == 100)
                    $("#lblCourseStatus").css({ "color": "green" }) //completed
                else if ($("#hdnOverall").val() < 100 && $("#hfIsInProgress").val() > 0)
                    $("#lblCourseStatus").css({ "color": "yellow" }); //in progress
                else
                    $("#lblCourseStatus").css({ "color": "white" }); //not started
            }
            hideLoading();
            console.log("rebind progress" + hidValue);
            // && parseInt(oldValue) == 0
            //&& $("#hfOldValue").val() == "0"
            //debugger;
            var showRating = $("#hfShowRating").val();
            if (showRating == "true") {
                //$("#displayedRating").removeClass("display-none");
                confirmRating();
                //console.log($("#hdnOverall").val());
            }
            //confirmRating();

        }

        function setStatus(status, statusID, showRating) {
            //debugger;
            //0 - Not Started
            //1 - completed
            //2 - In Progress
            //3 - Overdue
            //debugger;
            $("#lblCourseStatus").html(status);
            if (statusID == 3) {
                $("#lblCourseStatus").css({ "color": "red" })
            } else if (statusID == 2)
                $("#lblCourseStatus").css({ "color": "yellow" }); //in progress
            else if (statusID == 1)
                $("#lblCourseStatus").css({ "color": "green" }) //completed
            else
                $("#lblCourseStatus").css({ "color": "white" }); //not started

            //1 if completed show rate window
            //            debugger;
            //            if (statusID == 1) {
            //                confirmRating();
            //            }
            //debugger';
            if(statusID == 1)
                 $("#displayedRating").removeClass("display-none");
            if (showRating == "true") {
               
                confirmRating();
            }
              
            //console.log($("#hdnOverall").val());

        }

        function LogResourceLaunch(CIM, scoID, scoTypeID) {
            debugger;
            return $.ajax({
                type: "POST",
                //data: "{CIM: " + CIM + ",CourseID:'" + CourseID + "'}",
                data: "{CIM: " + CIM + ",scoID:'" + scoID + "',scoTypeID: " + scoTypeID + ",courseID: " + <%= Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"])) %> + "}",
                contentType: "application/json; charset=utf-8",
                url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "LogResourceLaunch",
                dataType: 'json',
                success: function (res) {
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var code = XMLHttpRequest.status;
                    if (code == "401")
                        window.location.reload();
                }
            });
        }

        function closeRating(res) {
            //debugger;
            $find("<%= radCommentWindow.ClientID %>").close();
            if (res == "1") {
                radalert($('#<%= hfYourreviewhasbeenrecordedThankyou.ClientID %>').val(), 330, 180, $('#<%= hfSuccess.ClientID %>').val(), callBackReload);
            } else {
                radalert($('#<%= hfFailedtosaveyourreviewPleasecontacttheadministrator.ClientID %>').val(), 330, 180, $('#<%= hfErrorMessage.ClientID %>').val(), callBackReload);
            }


        }
        function callBackReload(arg) {
            if (arg)
                pageReload();
        }


        function isChecked(checkbox, sub1, sub2) {
            document.getElementById(sub1).disabled = !checkbox.checked;
            document.getElementById(sub2).disabled = !checkbox.checked;
        }
        var similarCourseLoaded = false;

        function tabClick(el) {
            if ($('.NotSection').length > 0) {
                fixNoSection()
            }
            //debugger;
            //$(el).removeClass("fa-sort-up").addClass("fa-sort-down");
            var panelId = $(el).parent().find("a").attr("href");

            if(panelId == "#similarCourseTab" && similarCourseLoaded != true){
                similarCourseLoaded = true;
                initializeSimilarCourse();
            }
            

            console.log(panelId);
            //                $(panelId).addClass("display-none");
            $(el).parent().find("i").removeClass("fa-sort-up").addClass("fa-sort-down");
            if (!$(panelId).hasClass("in")) {
                //console.log(el);

                $(el).parent().find("i").removeClass("fa-sort-down").addClass("fa-sort-up");

                // $(panelId).removeClass("display-none");
            }


        }

        function fixNoSection() {
            $('.NotSection').each(function (i, obj) {
                var x = $(this).find(".panel-body").find("#ContentAccordion").prependTo($(this).parent().parent());
                //$(this).parent().remove();
                $(this).parent().addClass("display-none");
                if (x.text().trim() == $('#<%= hfThisSectionHasNoItemToDisplay.ClientID %>').val())
                    x.text("");

            });


        }

        //        function openNonScorm(url, cim, isAssessment, courseid) {

        //            LogResourceLaunch(cim, courseid).done(function () {
        //                if (isAssessment)
        //                    window.location.href = url;
        //                else
        //                    window.open(url, '_blank');
        //                //window.open(url);

        //                sessionStorage.setItem("cimg", $("#hfCourseImage").val());

        //            });
        //        }

        function openNonScormNewTab(url, cim, scoIDorTestID, scoTypeID) {
            //debugger;
            LogResourceLaunch(cim, scoIDorTestID, scoTypeID).done(function () {
                //                if (scoTypeID <= 4)
                //                    window.location.href = url;
                //                    //window.open(url, '_blank');
                //                else
                //window.open(url);
                window.location.reload();
                var popUp = window.open(url);
                if (popUp == null || typeof(popUp)=='undefined') {  
                     alert($('#<%= hfPleasedisableyourpopupblockerorallowPopuptoloadthecourseresourceandclickthelinkagain.ClientID %>').val());
                     return;
                } 
                else {  
                    popUp.focus();
                }


                //     
                sessionStorage.setItem("cimg", $("#hfCourseImage").val());

            });
        }

        function openNonScormSamePage(url, cim, scoIDorTestID, scoTypeID, courseTypeID) {
            //debugger;
            LogResourceLaunch(cim, scoIDorTestID, scoTypeID).done(function () {
                window.location.href = url;
            });
        }

        function initializeDisplayedRating() {
            var $star_rating = $('.star-rating .fa');

            var SetRatingStar = function () {
                return $star_rating.each(function () {
                    if (parseInt($("#hfRating").val()) >= parseInt($(this).data('rating'))) {
                        return $(this).removeClass('fa-star-o').addClass('fa-star');
                    } else {
                        return $(this).removeClass('fa-star').addClass('fa-star-o');
                    }
                });
            };

            $star_rating.on('click', function () {
                $("#hfRating").val($(this).data('rating'));
                //$star_rating.siblings('input.rating-value').val($(this).data('rating'));
                return SetRatingStar();
            });

            SetRatingStar();

        }

        function confirmNewRating(starRating) {

            $("#hfRating").val(starRating);
            $find("<%= rrCourse.ClientID %>").set_value(starRating);
            confirmRating();
        }

        

        //CHECK LENGTH GIVEN BY THE PARAMETER
        function CheckMaxLength(sender, Maxlength) {
            var length = $(sender).val().length;
            if (sender.value.length > Maxlength) {
                sender.value = sender.value.substr(0, Maxlength);
            }

            var length = Maxlength - length;
            $('#<%= txtFolderNameChars.ClientID %>').text(length);
            

        }

        function confirmEnrollToCourse() {
            debugger;
            var btnenroll =  $("#<%= BtnEnrolled.ClientID %>");
            var courseName = $("#hfCourseName").val();
            var msg = "";
            if(btnenroll[0].text == "Enroll")
                msg = "Are you sure you want to enroll to <b>\"" + courseName + "\"</b>?";
            else
                msg = "Are you sure you want to cancel your enrollment to <b>\"" + courseName + "\"</b>?";
            radconfirm(msg, confirmEnrollFN, 370, 180, "", "Confirm");

            function confirmEnrollFN(arg) {
                if(arg)
                    $("#<%= btnRealEnroll.ClientID %>").click();
                else
                    return false;
            }
        }

    </script>
</rad:RadScriptBlock>
<rad:RadAjaxManagerProxy ID="rampCoursePreview" runat="server">
    <AjaxSettings>
        <rad:AjaxSetting AjaxControlID="btnLeaveComment">
            <UpdatedControls>
                <%--<rad:AjaxUpdatedControl ControlID="btnLeaveComment" />--%>
                <rad:AjaxUpdatedControl ControlID="radCommentWindow" />
                <rad:AjaxUpdatedControl ControlID="pnlLeaveComment" LoadingPanelID="localLoadingPanel" />
                <rad:AjaxUpdatedControl ControlID="pnlReview" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnRealEnroll">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="BtnEnrolled" LoadingPanelID="localLoadingPanel" />
                <rad:AjaxUpdatedControl ControlID="pnlRating" />
                <rad:AjaxUpdatedControl ControlID="btnRealEnroll" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnConfirmFolder">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="radAddFolder" />
                <rad:AjaxUpdatedControl ControlID="lblFolderError" />
                <rad:AjaxUpdatedControl ControlID="pnlAddFolder" LoadingPanelID="localLoadingPanel" />
                <rad:AjaxUpdatedControl ControlID="rdtTakeLaterCourses" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnAddToTakeLater">
            <UpdatedControls>
                <%--            <rad:AjaxUpdatedControl ControlID="btnAddToTakeLater" />
                <rad:AjaxUpdatedControl ControlID="radTakeLaterCourses" />--%>
                <rad:AjaxUpdatedControl ControlID="pnlLaterCourses" LoadingPanelID="localLoadingPanel" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <%-- <rad:AjaxSetting AjaxControlID="cbAcceptForumGuide">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="cbAcceptForumGuide" />
                <rad:AjaxUpdatedControl ControlID="forumMainContainer" LoadingPanelID="localLoadingPanel" />
            </UpdatedControls>
        </rad:AjaxSetting>--%>
        <rad:AjaxSetting AjaxControlID="btnSubmitReply">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="btnSubmitReply" />
                <rad:AjaxUpdatedControl ControlID="coursePreviewContainer" LoadingPanelID="localLoadingPanel" />
            </UpdatedControls>
        </rad:AjaxSetting>
    </AjaxSettings>
</rad:RadAjaxManagerProxy>
<rad:RadWindowManager ID="rwmCoursePrev" RenderMode="Lightweight" VisibleOnPageLoad="false"
    Behaviors="Close, Move" DestroyOnClose="true" Modal="true" Opacity="99" runat="server"
    VisibleStatusbar="false" Skin="Bootstrap" RestrictionZoneID="BgCourse">
    <Windows>
        <rad:RadWindow ID="radCommentWindow" Title="Leave a Comment" runat="server" CssClass="tc-radwindow-1 title-left tc-radwindow-teal bg-gray comment titlebar-teal white-shadow soft m-window-review"
            Width="510px" OnClientClose="resetRatingValue" Height="250px">
            <ContentTemplate>
                <asp:Panel ID="pnlLeaveComment" runat="server">
                    <table width="100%">
                        <tbody>
                            <tr>
                                <td>
                                    <div class="selectCourseContainer">
                                        <p class="text-center">
                                            <asp:Label runat="server" ID="label17" Text=" <%$ Resources:LocalizedResource, Beforeyougohelpusimprovethiscoursebyleavingyourcomment%>"></asp:Label>
                                        </p>
                                        <%--  <p class="text-center">
                                            Please note that your comment will be posted on the "Comments" section of the Course
                                            Launch page.
                                        </p>--%>
                                        <br />
                                        <div class="user-details">
                                            <img id="imgPicStatus" runat="server" class="img-circle img-sm" alt="User" />
                                            <div style="display: inline-flex;">
                                                <asp:Label ID="lblName" runat="server"></asp:Label>
                                                <rad:RadRating RenderMode="Lightweight" ID="rrCourse" runat="server" ItemCount="5"
                                                    Value="0" SelectionMode="Continuous" Precision="Item" Orientation="Horizontal"
                                                    CssClass="absolute-horizontal-center user-course-rating" OnClientRating="clientRated">
                                                </rad:RadRating>
                                                <%----%>
                                                <%--<asp:Panel ID="pnlRating" runat="server" CssClass="course-rating absolute-horizontal-center">--%>
                                                <asp:HiddenField ID="hfRating" runat="server" ClientIDMode="Static" />
                                                <asp:HiddenField ID="hfOldValue" runat="server" ClientIDMode="Static" />
                                                <%--</asp:Panel>--%>
                                            </div>
                                            <asp:Label ID="lblCurrentDate" runat="server" CssClass="pull-right" Style="padding-top: 15px;
                                                padding-right: 10px;"></asp:Label>
                                            <%-- <asp:Label ID="lblRemaingChar" runat="server" Text="100/100"  CssClass="pull-right" Style="padding-right: 10px;"></asp:Label>--%>
                                            <br />
                                            <asp:TextBox ID="txtComment" runat="server" placeholder=" -Leave Comment-" CssClass="form-control width-fill"
                                                MaxLength="100" ToolTip="Enter your comment"></asp:TextBox>
                                            <%--onchange="textChange(this)"--%>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td style="padding-top: 10px;">
                                    <asp:LinkButton ID="btnLeaveComment" runat="server" Text="<%$ Resources:LocalizedResource, LeaveComment%>"
                                        CssClass="btn btn-teal btn-md absolute-horizontal-center btn-soft" OnClick="btnLeaveComment_Click"> </asp:LinkButton>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </asp:Panel>
            </ContentTemplate>
        </rad:RadWindow>
        <rad:RadWindow ID="radTakeLaterCourses" RenderMode="Lightweight" Title="<%$ Resources:LocalizedResource, AddtotakeLaterCourses%>"
            runat="server" CssClass="tc-radwindow-1 tc-radwindow-teal" Width="500px" Height="555px"
            ReloadOnShow="true">
            <ContentTemplate>
                <asp:Panel ID="pnlLaterCourses" runat="server" CssClass="padding-side-10">
                    <p class="teal bold">
                        <asp:Label runat="server" ID="lblRecommendedLeadershipCoursesForYou" Text=" <%$ Resources:LocalizedResource, ThereAre%>"></asp:Label>
                        &nbsp;<asp:Label ID="lblItemCount" runat="server" Text="0"></asp:Label>&nbsp;
                        <asp:Label runat="server" ID="label18" Text=" <%$ Resources:LocalizedResource, itemsinTakeLaterCourses%>"></asp:Label>
                    </p>
                    <br />
                    <p>
                        Move To
                    </p>
                    <rad:RadTreeView RenderMode="Lightweight" ID="rdtTakeLaterCourses" runat="server"
                        OnClientNodeClicked="rdtTakeLaterCourses_Clicked" Height="250px" BorderWidth="2px"
                        BorderColor="Teal" BorderStyle="Solid" OnDataBound="rdtTakeLaterCourses_DataBound"
                        OnClientLoad="addRootNode" DataTextField="Title" DataFieldID="FileID" DataFieldParentID="ParentID"
                        DataValueField="OFileID" Visible="false">
                        <NodeTemplate>
                            <asp:Image ID="icon" ImageUrl='<%# Eval("FileType", "~/Media/Images/tree-item-{0}.png") %>'
                                runat="server" />
                            <%# Eval("Title") %>
                        </NodeTemplate>
                        <DataBindings>
                            <rad:RadTreeNodeBinding Depth="0"></rad:RadTreeNodeBinding>
                        </DataBindings>
                    </rad:RadTreeView>
                    <asp:HiddenField ID="hfSelectedFolderID" runat="server" Value="0" />
                    <asp:HiddenField ID="hfSelectedFolderName" runat="server" Value="Take Later Courses Root" />
                    <asp:LinkButton ID="btnAddNewFolder" runat="server" CssClass="btn btn-sm black" OnClientClick="openAddFolder(); return false;">
                        <asp:Image ID="imgAddFolder" ImageUrl="~/Media/Images/add-folder-icon.png" runat="server"
                            Width="30px" />
                        Create new Folder
                    </asp:LinkButton>
                    <asp:Label ID="lblFolderError" runat="server" ForeColor="Red" Visible="false"></asp:Label>
                    <i>
                        <p class="teal bold">
                            <asp:Label runat="server" ID="label19" Text=" <%$ Resources:LocalizedResource, ThisCourseisvaliduntil%>"></asp:Label>
                            <asp:Label ID="lblValid" runat="server"></asp:Label>
                        </p>
                    </i>
                    <br />
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="label20" Text=" <%$ Resources:LocalizedResource, Notifymeon%>"></asp:Label>
                            </td>
                            <td>
                                <rad:RadDatePicker ID="dpNotifyMeDate" runat="server" ShowPopupOnFocus="true" RenderMode="Lightweight"
                                    DatePopupButton-Visible="true" Width="250px" ToolTip="Set Date" placeholder="<%$ Resources:LocalizedResource, Select%>">
                                    <ClientEvents OnDateSelected="OnNotifyDateSelected" />
                                    <DateInput ID="DateInput1" DateFormat="MM/dd/yyyy" DisplayDateFormat="MM/dd/yyyy"
                                        ClientEvents-OnError="onDateError" ClientEvents-OnBlur="onDateBlur" runat="server"
                                        ValidationGroup="vgNotifyDate">
                                    </DateInput>
                                </rad:RadDatePicker>
                                <asp:CustomValidator runat="server" ID="cvNotifyDate" ClientValidationFunction="ValidateNotifyDate"
                                    ErrorMessage="*" ControlToValidate="dpNotifyMeDate" ValidationGroup="vgNotifyDate" />
                            </td>
                        </tr>
                    </table>
                    <div class="row relative-hr-center display-inline-flex margin-all-10 padding-all-10">
                        <div class="col-xs-5 pull-right">
                            <rad:RadButton ID="btnAddToTakeLater" Text="Add Course" runat="server" CssClass="btn btn-md btn-teal btn-flat"
                                OnClientClicking="confirmAddToTakeLater" OnClick="btnAddToTakeLater_OnClick"
                                ValidationGroup="vgNotifyDate" RenderMode="Lightweight" Skin="Bootstrap">
                            </rad:RadButton>
                            <%--OnClientClick="btnAddToTakeLater_OnClick(); return false;" --%>
                        </div>
                        <div class="col-xs-5 pull-left">
                            <asp:Button type="button" class="btn btn-md btn-teal btn-flat" OnClientClick="closeTakeLaterCourses(); return false;"
                                runat="server" Text="<%$Resources:LocalizedResource, Cancel%>"></asp:Button>
                        </div>
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </rad:RadWindow>
        <rad:RadWindow ID="radAddFolder" Title="Add Folder" runat="server" CssClass="tc-radwindow-1 tc-radwindow-teal"
            Width="300px" Height="210px" Behaviors="Close">
            <ContentTemplate>
                <asp:Panel ID="pnlAddFolder" runat="server" Style="padding-right: 30px; padding-top: 10px;
                    height: -webkit-fill-available;">
                    Folder Name:
                    <asp:RequiredFieldValidator ID="rfvTxtFolderName" runat="server" ErrorMessage="*"
                        ControlToValidate="txtFolderName" SetFocusOnError="True" Display="Dynamic" ValidationGroup="addFolder"
                        ForeColor="Red" />
                    <asp:TextBox ID="txtFolderName" runat="server" placeholder="e.g Favorite Courses"
                        MaxLength="100" onkeyup="CheckMaxLength(this,100);" CssClass="form-control" ToolTip="Enter Folder Name"
                        ValidationGroup="addFolder" ondragstart="return false;" ondrop="return false;"
                        ClientIDMode="Static"></asp:TextBox>
                    <label class="text-danger text-right">
                        <span runat="server" id="txtFolderNameChars">100</span>
                        <asp:Label runat="server" ID="Label2" Text="<%$Resources:LocalizedResource,  CharactersRemaining%>"></asp:Label></label>
                    <asp:Label ID="Label1" Visible="false" Text="*" ForeColor="Red" runat="server"></asp:Label>
                    <div class="row absolute-horizontal-center display-inline-flex" style="margin-top: 20px;">
                        <div class="col-xs-5 pull-right">
                            <asp:LinkButton ID="btnConfirmFolder" Text="Add Folder" runat="server" CssClass="btn btn-md btn-teal btn-flat"
                                OnClientClick="hideAddFolder();" OnClick="btnAddNewFolder_Click" ValidationGroup="addFolder" />
                        </div>
                        <div class="col-xs-5 pull-left">
                            <asp:Button type="button" class="btn btn-md btn-teal btn-flat" OnClientClick="closeAddFolder()"
                                runat="server" Text="<%$Resources:LocalizedResource, Cancel%>"></asp:Button>
                        </div>
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </rad:RadWindow>
        <rad:RadWindow RenderMode="Lightweight" ID="frGuideLine" Behaviors="Close,Move" VisibleOnPageLoad="false"
            Modal="true" runat="server" CssClass="tc-radwindow-1 tc-radwindow-teal" Title="Transcom University's Forum Usage Guide"
            Width="1000px" Height="640" OnClientBeforeClose="hideLoading">
            <ContentTemplate>
                <asp:Panel ID="pnlTransactionList" runat="server">
                    <div id="forumGuidelineContainer">
                        <%-- <iframe frameborder="0" width="100%" height="550px" src="Includes/ForumGuideline.htm">
                        </iframe>--%>
                        <div id="frmIncludes">
                        </div>
                        <p>
                            <input type="checkbox" class="cbMargin" id="termsChkbx" onchange="isChecked(this, 'btnFakeAccept','btnCancel')" />
                            <asp:Label runat="server" ID="label21" Text=" <%$ Resources:LocalizedResource, IhavereadandaccepttheTermsandConditionsoftheForumsUsageGuide%>"></asp:Label>
                        </p>
                        <p>
                            <asp:Button runat="server" OnClick="cbAcceptForumGuide_CheckedChanged" ID="btnAccept"
                                Style="display: none;" />
                            <button class="btn btn-md btn-flat btn-teal" id="btnFakeAccept" onclick="AcceptForumGuide_Click(); return false;"
                                disabled="disabled">
                                <asp:Label runat="server" ID="label22" Text=" <%$ Resources:LocalizedResource, Accept%>"></asp:Label></button>
                            <button class="btn btn-md btn-flat btn-teal" id="btnCancel" onclick="CancelForumGuide_Click()"
                                disabled="disabled">
                                <asp:Label runat="server" ID="label23" Text=" <%$ Resources:LocalizedResource, Cancel%>"></asp:Label></button>
                        </p>
                    </div>
                    <%-- <frameset id="framesetOuter" name="framesetOuter" rows="100%" framespacing="0" frameborder="0"
                        border="0">
                        <frame class="ShellFrame" id="frameLearnTask" name="frameLearnTask" src='<% ~/Includes/ForumGuideline.htm %>'
                            scrolling="no" marginwidth="0" marginheight="0" framespacing="0" frameborder="0" />
                    </frameset>--%>
                </asp:Panel>
            </ContentTemplate>
        </rad:RadWindow>
        <rad:RadWindow ID="radLeaveForum" runat="server" Behaviors="None" VisibleOnPageLoad="false"
            Modal="false" Title="Confirm action to leave forum" Width="500px" CssClass="tc-radwindow-1 leaveforum title-left tc-radwindow-teal bg-gray titlebar-teal white-shadow soft center nomove">
            <ContentTemplate>
                <asp:Label runat="server" ID="label24" Text=" <%$ Resources:LocalizedResource, Youareabouttoleave%>"></asp:Label>
                <asp:Literal ID="ltCourseName" Text="CourseName" runat="server" />
                <asp:Label runat="server" ID="label25" Text=" <%$ Resources:LocalizedResource, Forum%>"></asp:Label>
                <br />
                <asp:Label runat="server" ID="label26" Text=" <%$ Resources:LocalizedResource, Pleasenotethatyouwillnolongerseethediscussionsontheforumthreadsonceyouconfirmtoleavetheforum%>"></asp:Label>
                <br />
                <asp:Label runat="server" ID="label27" Text=" <%$ Resources:LocalizedResource, Torejointheforumyouwillneedtoreviewandacceptthe%>"></asp:Label>
                <br />
                <asp:Label runat="server" ID="label28" Text=" <%$ Resources:LocalizedResource, ForumsUsageandGuidelines%>"></asp:Label>
                <br />
                <asp:Label runat="server" ID="label29" Text=" <%$ Resources:LocalizedResource, PleaseclicktheLeaveforumbuttontoprocess%>"></asp:Label>
                <br />
                <asp:Label runat="server" ID="label30" Text=" <%$ Resources:LocalizedResource, OtherwiseclickCanceltostayontheforum%>"></asp:Label>
                <div class="row absolute-horizontal-center display-inline-flex" style="margin-top: 8%;">
                    <div class="col-xs-5 pull-right">
                        <asp:Button ID="btnCancelLeave" Text="<%$ Resources:LocalizedResource, Cancel%>"
                            runat="server" CssClass="btn btn-md btn-teal pull-left" OnClientClick="closeLeaveForum(); return false;" />
                    </div>
                    <div class="col-xs-5 pull-left">
                        <asp:Button ID="btnConfirmLeave" Text="<%$ Resources:LocalizedResource, Leaveforum%>"
                            runat="server" CssClass="btn btn-md btn-teal pull-right" OnClick="BtnLeaveForum_Clicked" />
                    </div>
                </div>
            </ContentTemplate>
        </rad:RadWindow>
    </Windows>
</rad:RadWindowManager>
<div class="row black-header ten-px-padding vertical-align m-launcher-nav">
    <div class="col-md-6 col-xs-2 font-bold m-launcher-back">
        <%--        <a href="CourseCatalog.aspx" class="fa fa-arrow-circle-left link-no-hover" ></a>--%>
        <button onclick="back(); return false;" style="background: transparent;">
            <i class="fa fa-chevron-circle-left link-no-hover" style="font-size: 30px; color: White;
                text-decoration: none;"></i>
        </button>
    </div>
    <div class="col-md-6 col-xs-8 col-xs-offset-2 font-bold m-progress">
        <%--      <asp:Panel ID="pnlOverallProgress" runat="server">
            <div class="row">
                <div class="col-md-6 col-xs-12 pull-right">
                    <div class="progressBarContainer">
                        <div id="progressBar" style="width: 100%; border: 1px solid #fff;">
                            <asp:HiddenField ID="hdnOverall" runat="server" Value="0" ClientIDMode="Static" />
                            <span id="spanval" style="line-height: 22px; font-size: 12px;"></span>
                            <div id="progress" style="width: 0;">
                            </div>
                        </div>
                    </div>
                </div>
                <div id="extra_text" runat="server" class="col-md-6 col-xs-8 text-right m-overalltext">
                    Overall progress
                </div>
            </div>
        </asp:Panel>--%>
    </div>
</div>
<div class="row">
    <div class="col-md-12 course-prev-cover-photo" id="BgCourse" runat="server" clientidmode="Static">
        <div class="layer">
        </div>
        <div class="course-preview-title text-center">
            <h1>
                <asp:Label ID="LblCourseTitle" runat="server" Text="Sample course 2017" />
            </h1>
        </div>
        <div class="row margin-bottom-10 text-center display-none" id="displayedRating" runat="server"
            clientidmode="Static">
            <div class="col-lg-12">
                <div class="star-rating">
                    <span class="fa fa-star-o pointer" data-rating="1" onclick="confirmNewRating(1);">
                    </span><span class="fa fa-star-o pointer" data-rating="2" onclick="confirmNewRating(2);">
                    </span><span class="fa fa-star-o pointer" data-rating="3" onclick="confirmNewRating(3);">
                    </span><span class="fa fa-star-o pointer" data-rating="4" onclick="confirmNewRating(4);">
                    </span><span class="fa fa-star-o pointer" data-rating="5" onclick="confirmNewRating(5);">
                    </span>
                </div>
            </div>
        </div>
        <div class="row m-preview-description">
            <div class="absolute-horizontal-center padding-side-10 color-white border-bottom-white ">
                <h4>
                    <asp:Label runat="server" ID="Label3" Text=" <%$ Resources:LocalizedResource, Description%>"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="row m-preview-description">
            <div class="col-md-10 col-md-offset-1 text-center color-white course-preview-description scrollbox">
                <div class="scrollbox-content">
                    <asp:Label ID="LblDesc" runat="server" Text="" />
                </div>
            </div>
        </div>
        <%--      <div class="row " style="margin-top: 80px;">
           
        </div>--%>
        <div class="row">
            <div class="right-side-content text-right">
                <b>
                    <asp:Label ID="LblType" runat="server" Text="OLT"  CssClass="font-capitalize"/></b>
                <br />
                <b>
                    <asp:Label runat="server" ID="Label4" Text=" <%$ Resources:LocalizedResource, Status%>" CssClass="font-capitalize"></asp:Label>:</b>
                <asp:Label ID="lblCourseStatus" runat="server" Text="Not yet started" ClientIDMode="Static" />
                <br />
                <b>
                    <asp:Label runat="server" ID="Label5" Text=" <%$ Resources:LocalizedResource, UploadDate%>" CssClass="font-capitalize"></asp:Label>:</b>
                <asp:Label ID="LblUploadDate" runat="server" Text="Nov. 24, 2017" />
                <br />
                <b>
                    <asp:Label runat="server" ID="Label6" Text=" <%$ Resources:LocalizedResource, ValidUntil%>" CssClass="font-capitalize"></asp:Label>:</b>
                <asp:Label ID="LblValidDate" runat="server" Text="Nov. 24, 2017" />
                <br />
                <b>
                    <asp:Label runat="server" ID="Label7" Text=" <%$ Resources:LocalizedResource, Category%>" CssClass="font-capitalize"></asp:Label>:</b>
                <asp:Label ID="lblCategory" runat="server" Text="Transcom" />
                <br />
                <b>
                    <asp:Label runat="server" ID="Label8" Text=" <%$ Resources:LocalizedResource, Subcategory%>" CssClass="font-capitalize"></asp:Label>:</b>
                <asp:Label ID="lblSubcategory" runat="server" Text="Transcom" />
                <asp:HiddenField ID="hfIsDue" runat="server" Value="" ClientIDMode="Static" />
                <asp:HiddenField ID="hfIsInProgress" runat="server" Value="" ClientIDMode="Static" />
            </div>
        </div>
    </div>
</div>
<asp:Panel ID="CourseDetails" runat="server">
    <div class="display-inline-flex relative-hr-center">
        <asp:LinkButton ID="BtnTakeLater" runat="server" CssClass="btn btn-lg btn-teal margin-side-10 font-uppercase pointer"
            OnClientClick="openTakeCoursesWindow(); return false;">
            <i class="fa fa-heart-o"></i>
            <asp:Label runat="server" ID="lblTakeLater" Text="<%$ Resources:LocalizedResource, AddtotakeLaterCourses%>"></asp:Label></asp:LinkButton>
        <button type="button" id="BtnJoinForumFake" class="btn btn-lg btn-teal margin-side-10 font-uppercase"
            onclick="BtnJoinForum_Click();" runat="server">
            <i class="fa fa-comments"></i>Join Forum</button>
        <asp:LinkButton ID="BtnJoinForum" runat="server" CssClass="display-none" OnCommand="BtnJoinForum_Clicked"> </asp:LinkButton>
        <button type="button" id="BtnLeaveForumFake" class="btn btn-lg btn-teal margin-side-10 font-uppercase"
            onclick="showLeaveForum(); return false;" runat="server">
            <i class="fa fa-comments"></i>
            <asp:Label runat="server" ID="label31" Text=" <%$ Resources:LocalizedResource, Leaveforum%>"></asp:Label>
        </button>
        <asp:LinkButton ID="BtnLeaveForum" runat="server" class="btn btn-lg btn-teal margin-side-10 display-none font-uppercase"
            OnClick="BtnLeaveForum_Clicked"> <i class="fa fa-comments"></i>Leave Forum </asp:LinkButton>
        <asp:LinkButton ID="BtnEnrolled" runat="server" CssClass="btn btn-lg btn-teal margin-side-10 pointer font-uppercase"
            CommandArgument="none" onmouseover="showCancelIfPending();" OnClientClick="confirmEnrollToCourse(); return false;"
            onmouseleave="hideCancelIfPending();"> Enroll </asp:LinkButton>
        <%-- OnCommand="BtnEnrolled_Click"--%>
        <asp:Button ID="btnRealEnroll" Text="Enroll" runat="server" OnClick="BtnEnrolled_Click"
            CssClass="display-none" />
    </div>
    <asp:Panel ID="pnlReview" runat="server">
        <div class="row">
            <div class="col-sm-12 border-top-light-dark-gray">
            </div>
        </div>
        <div class="row text-center">
            <div class="col-sm-offset-3 col-sm-6">
                <div class="row">
                    <div class="col-sm-4 col-xs-4">
                        <span class="text-center">
                            <asp:Image ImageUrl="~/Media/Images/star-outline-in-a-circle-line.png" ID="ImgStar"
                                runat="server" /></span></div>
                    <div class="col-sm-4 col-xs-4">
                        <asp:Image ImageUrl="~/Media/Images/search-with-magnifier-in-circular-button.png"
                            ID="Image1" runat="server" /></div>
                    <div class="col-sm-4 col-xs-4">
                        <span class="text-center">
                            <asp:Image ImageUrl="~/Media/Images/chat-circular-button.png" ID="Image2" runat="server" /></span></div>
                </div>
            </div>
            <div class="col-sm-offset-3 col-sm-6">
                <div class="row">
                    <div class="col-sm-4 col-xs-4 text-center">
                        <asp:Label ID="lblStarRating" runat="server"> 5.5 </asp:Label>
                        <asp:Label runat="server" ID="Label10" Text=" <%$ Resources:LocalizedResource, StarRating%>"></asp:Label></div>
                    <div class="col-sm-4 col-xs-4 text-center">
                        <asp:Label ID="lblViews" runat="server"> 9,999 </asp:Label>
                        <asp:Label runat="server" ID="Label11" Text=" <%$ Resources:LocalizedResource, View%>"></asp:Label></div>
                    <div class="col-sm-4 col-xs-4 text-center">
                        <asp:Label ID="lblReviews" runat="server"> 9,999 </asp:Label>
                        <asp:Label runat="server" ID="Label12" Text=" <%$ Resources:LocalizedResource, Reviews%>"></asp:Label></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 border-top-light-dark-gray">
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-5">
                    <div class="row">
                        <div class="col-md-12 col-xs-12 m-rating-container">
                            <span class="pull-right">
                                <h2>
                                    <asp:Label ID="lblCourseStatRating" runat="server"> 5.5 </asp:Label>
                                </h2>
                            </span>
                        </div>
                        <div class="col-md-12 col-xs-12 m-star-container">
                            <span class="pull-right">
                                <rad:RadRating RenderMode="Lightweight" ID="rrCourseStatRating" runat="server" ItemCount="5"
                                    Value="4.5" SelectionMode="Continuous" Precision="Exact" Orientation="Horizontal"
                                    Enabled="false">
                                </rad:RadRating>
                            </span>
                        </div>
                        <div class="col-md-12 col-xs-12 m-user-container">
                            <span class="pull-right">
                                <h4>
                                    <i class="fa fa-users"></i>
                                    <asp:Label ID="lblUserCount" runat="server">999,999</asp:Label>
                                    <asp:Label runat="server" ID="Label13" Text=" <%$ Resources:LocalizedResource, TotalUsers%>"></asp:Label>
                                </h4>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-md-12">
                                <div class="row vertical-align">
                                    <div class="col-xs-2 no-paddings">
                                        <h5 class="font-bold">
                                            <i class="fa fa-star"></i>&nbsp;&nbsp;&nbsp;5</h5>
                                    </div>
                                    <div class="col-xs-10  no-paddings nowrap">
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-fivestar" role="progressbar" runat="server"
                                                id="FiveStarBar">
                                            </div>
                                            <asp:Label ID="lblFiveStarCount" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row vertical-align">
                                    <div class="col-xs-2  no-paddings">
                                        <h5 class="font-bold">
                                            <i class="fa fa-star"></i>&nbsp;&nbsp;&nbsp;4</h5>
                                    </div>
                                    <div class="col-xs-10  no-paddings nowrap">
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-fourstar" role="progressbar" style="width: 40%"
                                                runat="server" id="FourStarBar">
                                            </div>
                                            <asp:Label ID="lblFourStarCount" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row vertical-align">
                                    <div class="col-xs-2  no-paddings">
                                        <h5 class="font-bold">
                                            <i class="fa fa-star"></i>&nbsp;&nbsp;&nbsp;3</h5>
                                    </div>
                                    <div class="col-xs-10  no-paddings nowrap">
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-threestar" role="progressbar" style="width: 40%"
                                                runat="server" id="ThreeStarBar">
                                            </div>
                                            <asp:Label ID="lblThreeStarCount" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row vertical-align">
                                    <div class="col-xs-2 no-paddings">
                                        <h5 class="font-bold">
                                            <i class="fa fa-star"></i>&nbsp;&nbsp;&nbsp;2</h5>
                                    </div>
                                    <div class="col-xs-10 no-paddings nowrap">
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-twostar" role="progressbar" style="width: 40%"
                                                runat="server" id="TwoStarBar">
                                            </div>
                                            <asp:Label ID="lblTwoStarCount" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row vertical-align">
                                    <div class="col-xs-2 no-paddings">
                                        <h5 class="font-bold">
                                            <i class="fa fa-star"></i>&nbsp;&nbsp;&nbsp;1</h5>
                                    </div>
                                    <div class="col-xs-10 no-paddings nowrap">
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-onestar" role="progressbar" runat="server"
                                                id="OneStarBar">
                                            </div>
                                            <asp:Label ID="lblOneStarCount" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                        </div>
                    </div>
                </div>
            </div>
            <div class="horizontal-dotted-gray margin-bottom-10px display-none">
            </div>
        </div>
    </asp:Panel>
    <div class="row" style="margin-top: 30px;">
        <div class="col-sm-12">
            <div class="panel-group" id="CourseAccordion">
                <asp:Panel ID="pnlLearnerTab" runat="server" CssClass="panel panel-default">
                    <div class="panel-heading panel-heading-black display-flex">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" class="js-collapse-event no-underline-hover" data-parent="#CourseAccordion"
                                href="#learnerTab" onclick="tabClick(this);">Learner Resources</a> <i class="fa fa-sort-down pull-right hover-pointer js-collapse-event"
                                    data-toggle="collapse" data-parent="#CourseAccordion" href="#learnerTab" onclick="tabClick(this);">
                                </i>
                        </h4>
                    </div>
                    <div id="learnerTab" class="panel-collapse collapse">
                        <div class="panel-body">
                            <ucCourseLaunch:courseLaunch ID="cLaunch" runat="server" />
                            <%--<ucCourseLaunch_ns:courseLaunch_ns ID="cLaunch_ns" runat="server" Visible="false" />--%>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlTrainer" runat="server" CssClass="panel panel-default">
                    <div class="panel-heading panel-heading-black display-flex">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" class="js-collapse-event no-underline-hover" data-parent="#CourseAccordion"
                                href="#trainerTab" onclick="tabClick(this);">Trainer Resources</a> <i class="fa fa-sort-down pull-right hover-pointer js-collapse-event"
                                    data-toggle="collapse" data-parent="#CourseAccordion" href="#trainerTab" onclick="tabClick(this);">
                                </i>
                        </h4>
                    </div>
                    <div id="trainerTab" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="col-md-14" style="margin-left: 1%; margin-right: 1%">
                                <div class="col-md-12">
                                    <asp:Panel ID="Panel3" runat="server">
                                        <div id="div3" class="demo-container col-md-14">
                                            <rad:RadListView ID="lvCourseDefault_Trainer" runat="server" ItemPlaceholderID="SectionHolder"
                                                AllowPaging="false" OnItemDataBound="lvCourseDefault_Trainer_SectionDataBound"
                                                OnNeedDataSource="lvCourseDefault_Trainer_NeedDataSource" DataKeyNames="SectionID">
                                                <LayoutTemplate>
                                                    <div id="SectionHolder" runat="server">
                                                    </div>
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <asp:Panel ID="pnlSection" runat="server" CssClass="panel panel-default">
                                                        <div class="panel-heading panel-heading-black display-flex">
                                                            <h4 class="panel-title">
                                                                <a id="A1" data-toggle="collapse" class="js-collapse-event no-underline-hover" data-parent="#trainerTab"
                                                                    href='<%# String.Format("#_{0}", Eval("SectionID")) %>' onclick="tabClick(this);">
                                                                    <%# Eval("SectionName") %>
                                                                </a><i id="I10" class="fa fa-sort-down pull-right hover-pointer js-collapse-event"
                                                                    runat="server" data-toggle="collapse" data-parent="#trainerTab" href='<%#String.Format("#_{0}", Eval("SectionID")) %>'
                                                                    onclick="tabClick(this);"></i>
                                                            </h4>
                                                        </div>
                                                        <div id='<%#String.Format("_{0}", Eval("SectionID")) %>' class='panel-collapse collapse <%# Eval("SectionName").ToString().Replace(" ", "") %>'>
                                                            <div class="panel-body m-course-launcher-item">
                                                                <div id="ContentAccordion">
                                                                    <rad:RadListView ID="lvCourseDefault_Trainer_Item" runat="server" ItemPlaceholderID="ItemHolder"
                                                                        AllowPaging="false" DataKeyNames="dripEnable" OnItemDataBound="lvCourseDefault_Trainer_Item_OnItemDataBound">
                                                                        <LayoutTemplate>
                                                                            <div id="ItemHolder" runat="server">
                                                                            </div>
                                                                        </LayoutTemplate>
                                                                        <ItemTemplate>
                                                                            <div class="group">
                                                                                <div class="row panel-gray panel-course-category m-course-launcher-item">
                                                                                    <div class="col-xs-11 m-item " title='<%#Eval("ScoTitle")%>'>
                                                                                        <i id="I1" runat="server" class="fa fa-file pull-left" visible='<%# isFile(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I2" runat="server" class="fa fa-file-pdf-o pull-left" visible='<%# isPdf(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I3" runat="server" class="fa fa-file-word-o pull-left" visible='<%# isWord(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I4" runat="server" class="fa fa-file-excel-o pull-left" visible='<%# isExcel(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I5" runat="server" class="fa fa-file-powerpoint-o pull-left" visible='<%# isPpt(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I6" runat="server" class="fa fa-file-image-o pull-left" visible='<%# isImg(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I7" runat="server" class="fa fa-file-archive-o pull-left" visible='<%# isZip(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I8" runat="server" class="fa fa-file-audio-o pull-left" visible='<%# isMp3(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I9" runat="server" class="fa fa-file-video-o pull-left" visible='<%# isMp4(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i>
                                                                                        <asp:HiddenField runat="server" ID="hfdripEnable" Value='<%# Eval("dripEnable").ToString() %>' />
                                                                                        <asp:LinkButton ID="btnScoTitleTR" runat="server" CssClass="bold color-black" OnClientClick='<%# Convert.ToInt32(Eval("TestCategoryID")) == 0 ? completePath(Eval("ScoTitle").ToString(), Convert.ToInt32(Eval("ScoID")), Convert.ToInt32(Eval("scoTypeID"))) : getHotLink((int?)Eval("TestCategoryID"), Convert.ToInt32(Eval("scoTypeID")), Convert.ToInt32(Eval("CourseTypeID")))%>'> <%# Eval("ScoTitle")%> </asp:LinkButton>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </rad:RadListView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </asp:Panel>
                                                </ItemTemplate>
                                                <EmptyDataTemplate>
                                                    <asp:Label runat="server" ID="lblRecommendedLeadershipCoursesForYou" Text="No Resource to display"></asp:Label>
                                                </EmptyDataTemplate>
                                            </rad:RadListView>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlAdditional" runat="server" CssClass="panel panel-default">
                    <div class="panel-heading panel-heading-black display-flex">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" class="js-collapse-event no-underline-hover" data-parent="#CourseAccordion"
                                href="#additionalTab" onclick="tabClick(this);">Additional Resources</a> <i class="fa fa-sort-down pull-right hover-pointer js-collapse-event"
                                    data-toggle="collapse" data-parent="#CourseAccordion" href="#additionalTab" onclick="tabClick(this);">
                                </i>
                        </h4>
                    </div>
                    <div id="additionalTab" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="col-md-14" style="margin-left: 1%; margin-right: 1%">
                                <div class="col-md-12">
                                    <asp:Panel ID="Panel2" runat="server">
                                        <div id="div2" class="demo-container col-md-14">
                                            <rad:RadListView ID="lvCourseDefault_Additional" runat="server" ItemPlaceholderID="SectionHolder"
                                                AllowPaging="false" OnItemDataBound="lvCourseDefault_Additional_SectionDataBound"
                                                OnNeedDataSource="lvCourseDefault_Additional_NeedDataSource" DataKeyNames="SectionID">
                                                <LayoutTemplate>
                                                    <div id="SectionHolder" runat="server">
                                                    </div>
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <asp:Panel ID="pnlSection" runat="server" CssClass="panel panel-default">
                                                        <div class="panel-heading panel-heading-black display-flex">
                                                            <h4 class="panel-title">
                                                                <a id="A1" data-toggle="collapse" class="js-collapse-event no-underline-hover" data-parent="#additionalTab"
                                                                    href='<%# String.Format("#_{0}", Eval("SectionID")) %>' onclick="tabClick(this);">
                                                                    <%# Eval("SectionName") %>
                                                                </a><i id="I10" class="fa fa-sort-down pull-right hover-pointer js-collapse-event"
                                                                    runat="server" data-toggle="collapse" data-parent="#additionalTab" href='<%#String.Format("#_{0}", Eval("SectionID")) %>'
                                                                    onclick="tabClick(this);"></i>
                                                            </h4>
                                                        </div>
                                                        <div id='<%#String.Format("_{0}", Eval("SectionID")) %>' class='panel-collapse collapse <%# Eval("SectionName").ToString().Replace(" ", "") %>'>
                                                            <div class="panel-body m-course-launcher-item">
                                                                <div id="ContentAccordion">
                                                                    <rad:RadListView ID="lvCourseDefault_Additional_Item" runat="server" ItemPlaceholderID="ItemHolder"
                                                                        AllowPaging="false" DataKeyNames="dripEnable" OnItemDataBound="lvCourseDefault_Additional_Item_OnItemDataBound">
                                                                        <LayoutTemplate>
                                                                            <div id="ItemHolder" runat="server">
                                                                            </div>
                                                                        </LayoutTemplate>
                                                                        <ItemTemplate>
                                                                            <div class="group">
                                                                                <div class="row panel-gray panel-course-category m-course-launcher-item">
                                                                                    <div class="col-xs-11 m-item" title='<%#Eval("ScoTitle")%>'>
                                                                                        <i id="I1" runat="server" class="fa fa-file pull-left" visible='<%# isFile(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I2" runat="server" class="fa fa-file-pdf-o pull-left" visible='<%# isPdf(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I3" runat="server" class="fa fa-file-word-o pull-left" visible='<%# isWord(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I4" runat="server" class="fa fa-file-excel-o pull-left" visible='<%# isExcel(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I5" runat="server" class="fa fa-file-powerpoint-o pull-left" visible='<%# isPpt(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I6" runat="server" class="fa fa-file-image-o pull-left" visible='<%# isImg(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I7" runat="server" class="fa fa-file-archive-o pull-left" visible='<%# isZip(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I8" runat="server" class="fa fa-file-audio-o pull-left" visible='<%# isMp3(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i><i id="I9" runat="server" class="fa fa-file-video-o pull-left" visible='<%# isMp4(Eval("ScoTitle").ToString()) %>'>
                                                                                        </i>
                                                                                        <asp:HiddenField runat="server" ID="hfdripEnable" Value='<%# Eval("dripEnable").ToString() %>' />
                                                                                        <asp:LinkButton ID="btnScoTitleAR" runat="server" CssClass="bold color-black" OnClientClick='<%# Convert.ToInt32(Eval("TestCategoryID")) == 0 ? completePath(Eval("ScoTitle").ToString(), Convert.ToInt32(Eval("ScoID")), Convert.ToInt32(Eval("scoTypeID"))) : getHotLink((int?)Eval("TestCategoryID"), Convert.ToInt32(Eval("scoTypeID")), Convert.ToInt32(Eval("CourseTypeID")))%>'> <%# Eval("ScoTitle")%> </asp:LinkButton>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </rad:RadListView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </asp:Panel>
                                                </ItemTemplate>
                                                <EmptyDataTemplate>
                                                    <asp:Label runat="server" ID="lblRecommendedLeadershipCoursesForYou" Text="No Resource to display"></asp:Label>
                                                </EmptyDataTemplate>
                                            </rad:RadListView>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlSimilar" runat="server" CssClass="panel panel-default">
                    <div class="panel-heading panel-heading-black display-flex">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" class="js-collapse-event no-underline-hover" data-parent="#CourseAccordion"
                                href="#similarCourseTab" onclick="tabClick(this);">Similar Courses</a> <i class="fa fa-sort-down pull-right hover-pointer js-collapse-event"
                                    data-toggle="collapse" data-parent="#CourseAccordion" href="#similarCourseTab"
                                    onclick="tabClick(this);"></i>
                        </h4>
                    </div>
                    <div id="similarCourseTab" class="panel-collapse collapse no-border">
                        <div class="panel-body content-tab no-border">
                            <asp:Panel ID="pnlLvSimilarCourses" runat="server">
                                <div id="sc-container" class="mc-container col-md-12 content-tab">
                                    <div class="loader js-sc-loader">
                                    </div>
                                    <rad:RadListView ID="lvSimilarCourses" runat="server" AllowPaging="true" PageSize="6"
                                        AllowMultiFieldSorting="true">
                                        <LayoutTemplate>
                                            <div id="similarCourseListView">
                                                <div class="scPager">
                                                    <a class="pagePrev pull-left relative-vr-left-center no-underline-hover display-none"
                                                        href="javascript:void(0);" title="Go to previous page" style="z-index: 1; margin-left: 5px;">
                                                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Media/Images/chevron-left.png"
                                                            Width="40" CssClass="zoom-hover" />
                                                    </a><a class="pageNext pull-right relative-vr-right-center no-underline-hover display-none"
                                                        href="javascript:void(0);" title="Go to next page" style="z-index: 1; margin-right: 5px;">
                                                        <asp:Image ID="Image5" runat="server" ImageUrl="~/Media/Images/chevron-right.png"
                                                            Width="40" CssClass="zoom-hover" />
                                                    </a>
                                                </div>
                                                <div style="overflow: hidden;">
                                                    <%-- <ul id="sc-items" class="nostyle">
                                                    </ul>--%>
                                                    <div class="display-fluid" id="sc-items" style="width: 100%;">
                                                    </div>
                                                </div>
                                            </div>
                                        </LayoutTemplate>
                                        <ClientSettings>
                                            <DataBinding ItemPlaceHolderID="sc-items">
                                                <ItemTemplate>
                                                    <%--<li>
                                                        <a id="hllvSimilarCourses" href="CourseRedirect.aspx?CourseID=#= EncryptedCourseID #" class="similar-course">
                                                            <image src='Media/Uploads/CourseImg/#= CourseID #/#= CourseImage #' class="pull-left"></image>
                                                        </a>
                                                    <li>--%>
                                                        <a id="hllvSimilarCourses" href="" class="similar-course no-underline-hover hover-pointer text-trim" onclick='confirmLaunchCourse("#= EncryptedCourseID #");' title=" #= CourseTitle #" >
                                                            <image src='Media/Uploads/CourseImg/#= CourseID #/#= CourseImage #' onerror="this.src='Media/Uploads/CourseImg/No_image.jpg'" class="img-responsive"></image>
                                                            <h5>#= CourseTitle #</h5>
                                                        </a>
                                                 
                                                </ItemTemplate>
                                                <EmptyDataTemplate>
                                                    <p> &nbsp&nbsp&nbsp No Similar Course.</p> 
                                                </EmptyDataTemplate>
                                                <DataService EnableCaching="true" />
                                            </DataBinding>
                                            <ClientEvents OnCommand="scOnListViewCommand" OnDataBinding="scOnListViewDataBinding">
                                            </ClientEvents>
                                        </ClientSettings>
                                    </rad:RadListView>
                                </div>
                            </asp:Panel>
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>
    <div class="modal fade success-popup tc-modal-1 top-9999" id="forumGuidelinesWindown"
        tabindex="-1">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="H1">
                        <asp:Label runat="server" ID="label32" Text=" <%$ Resources:LocalizedResource, RemoveGroup%>"></asp:Label>
                    </h4>
                </div>
                <div class="modal-body text-center">
                    <div class="row">
                        <div class="col-md-2">
                            <i class="fa fa-exclamation-triangle warning-icon"></i>
                        </div>
                        <div class="col-md-10">
                            <p class="confirmationMsg" style="text-align: left;">
                                <asp:Label runat="server" ID="Label14" Text=" <%$ Resources:LocalizedResource, Areyousureyouwantto%>"></asp:Label>
                                <br />
                                <asp:Label runat="server" ID="Label15" Text=" <%$ Resources:LocalizedResource, Remove%>"></asp:Label>
                                <i id="modalGroupName" class="bold-only"></i>in this Course?
                            </p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <i data-toggle="modal" data-target="#confirmationModalGroup" style="font-style: normal;
                        font-weight: bold;" class="pull-right ">
                        <asp:LinkButton ID="btnRemoveGroup" runat="server" Text="<%$ Resources:LocalizedResource, Yes%>"
                            runat="server" CssClass="btn tc-btn-md btn-teal tc-btn-flat padding-side-10"></asp:LinkButton></i>
                    <a class="btn tc-btn-md btn-default pull-right tc-btn-flat" data-toggle="modal" data-target="#confirmationModalGroup">
                        <asp:Label runat="server" ID="Label16" Text=" <%$ Resources:LocalizedResource, No%>"></asp:Label></a>
                </div>
            </div>
        </div>
    </div>
</asp:Panel>
<asp:HiddenField ID="hfCourseImage" runat="server" ClientIDMode="Static" />
<asp:HiddenField ID="hfShowRating" runat="server" ClientIDMode="Static" />
<asp:HiddenField ID="hfAreyousureyouwanttoaddthiscoursetothisfolder" runat="server"
    Value="<%$ Resources:LocalizedResource, Areyousureyouwanttoaddthiscoursetothisfolder%>" />
<asp:HiddenField ID="hfErrorMessage" runat="server" Value="<%$ Resources:LocalizedResource, ErrorMessage%>" />
<asp:HiddenField ID="hfPleaseselectavalidDate" runat="server" Value="<%$ Resources:LocalizedResource, PleaseselectavalidDate%>" />
<asp:HiddenField ID="hfError" runat="server" Value="<%$ Resources:LocalizedResource, Error%>" />
<asp:HiddenField ID="hfSuccess" runat="server" Value="<%$ Resources:LocalizedResource, Success%>" />
<asp:HiddenField ID="hfThisSectionHasNoItemToDisplay" runat="server" Value="<%$ Resources:LocalizedResource, ThisSectionHasNoItemToDisplay%>" />
<asp:HiddenField ID="hfPleasedisableyourpopupblockerorallowPopuptoloadthecourseresourceandclickthelinkagain"
    runat="server" Value="<%$ Resources:LocalizedResource, PleasedisableyourpopupblockerorallowPopuptoloadthecourseresourceandclickthelinkagain%>" />
<asp:HiddenField runat="server" ID="hfThiscourseiscurrentlyclose" Value="<%$ Resources:LocalizedResource, Thiscourseiscurrentlyclose%>" />
<asp:HiddenField runat="server" ID="hfNotOpen" Value="<%$ Resources:LocalizedResource, NotOpen%>" />
<asp:HiddenField runat="server" ID="hfwassuccessfullycreated" Value="<%$ Resources:LocalizedResource, wassuccessfullycreated%>" />
<asp:HiddenField runat="server" ID="hfNotifyDateshouldbegreaterthancurrentdate" Value="<%$ Resources:LocalizedResource, NotifyDateshouldbegreaterthancurrentdate%>" />
<asp:HiddenField runat="server" ID="hfErrorDate" Value="<%$ Resources:LocalizedResource, ErrorDate%>" />
<asp:HiddenField runat="server" ID="hfNotifyDateshouldbelessthanexpirationdate" Value="<%$ Resources:LocalizedResource, NotifyDateshouldbelessthanexpirationdate%>" />
<asp:HiddenField runat="server" ID="hfSomethingWentWhileSubmittingYourReportPleaseTryAgain"
    Value="<%$ Resources:LocalizedResource, SomethingWentWhileSubmittingYourReportPleaseTryAgain%>" />
<asp:HiddenField runat="server" ID="hfYourreviewhasbeenrecordedThankyou" Value="<%$ Resources:LocalizedResource, YourreviewhasbeenrecordedThankyou%>" />
<asp:HiddenField runat="server" ID="hfFailedtosaveyourreviewPleasecontacttheadministrator"
    Value="<%$ Resources:LocalizedResource, FailedtosaveyourreviewPleasecontacttheadministrator%>" />
<asp:HiddenField runat="server" ID="hfMoveTo" Value="<%$ Resources:LocalizedResource, MoveTo%>" />
<asp:HiddenField ID="hfCourseName" runat="server" ClientIDMode="Static" />
<asp:HiddenField ID="hfenrolledAndIsValid" runat="server" ClientIDMode="Static" />
