﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;
using TranscomUniversityV3Model;
using Telerik.Web.UI;
using System.IO;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Security;
using NuComm.Security.Encryption;
using System.Net;
using System.Collections;

public partial class CoursePreviewUserCtrl : System.Web.UI.UserControl
{
    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);
    }

    #region FileExtension
    private static int itemIndex = 0;
    public static bool isFile(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (!isPdf(filename) && !isWord(filename) && !isExcel(filename) && !isPpt(filename) && !isImg(filename) && !isZip(filename) && !isMp3(filename) && !isMp4(filename))
            return true;
        else
            return false;
    }

    public static bool isPdf(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (ext == "pdf")
            return true;
        else
            return false;
    }

    public static bool isWord(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (ext == "doc" || ext == "docx")
            return true;
        else
            return false;
    }

    public static bool isExcel(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (ext == "xls" || ext == "xlsx")
            return true;
        else
            return false;
    }

    public static bool isPpt(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (ext == "ppt" || ext == "pptx")
            return true;
        else
            return false;
    }

    public static bool isImg(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (ext == "jpg" || ext == "jpeg" || ext == "png")
            return true;
        else
            return false;
    }

    public static bool isZip(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (ext == "rar" || ext == "zip")
            return true;
        else
            return false;
    }

    public static bool isMp3(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (ext == "mp3")
            return true;
        else
            return false;
    }

    public static bool isMp4(string filename)
    {
        string ext;
        string[] data = filename.Split('.');
        ext = data[data.Length - 1].ToLower();

        if (ext == "mp4")
            return true;
        else
            return false;
    }

    #endregion

    private DataHelper _db;

    private static SqlConnection _oconn;

    private static SqlCommand _ocmd;

    private int CourseID;
    private int CurrCourseID;
    private int enrolleeCIM;
    private string overAllProgress;
    private static DateTime dtServerTime;
    private static DateTime dtCourseEnd;

    private static IEnumerable<pr_Scorm_Cor_GetCurriculumPackages_Result> resources;
    private static TranscomUniversityV3ModelContainer db;
    private static int global_courseType;

    private static int global_TimezoneOffset;

    public DataTable DtOverAllBundle
    {
        get
        {
            return ViewState["DtOverAllBundle"] as DataTable;
        }
        set
        {
            ViewState["DtOverAllBundle"] = value;
        }
    }

    private static void Dbconn(string connStr)
    {

        _oconn = new SqlConnection
        {
            ConnectionString = ConfigurationManager.ConnectionStrings[connStr].ConnectionString
        };
        _oconn.Open();
    }

    //public void CreateOverAllPackageDataTableColumns()
    //{
    //    DtOverAllBundle = new DataTable();
    //    var column = new DataColumn
    //    {
    //        DataType = Type.GetType("System.Int32"),
    //        ColumnName = "ScoId",
    //        AutoIncrement = true,
    //        AutoIncrementSeed = 1,
    //        AutoIncrementStep = 1
    //    };
    //    DtOverAllBundle.Columns.Add(column);
    //    DtOverAllBundle.Columns.Add("ScoPackageId", typeof(int));
    //    DtOverAllBundle.Columns.Add("ScoTitle", typeof(String));
    //    DtOverAllBundle.Columns.Add("ScoTypeId", typeof(int));
    //    DtOverAllBundle.Columns.Add("ScoType", typeof(String));
    //    DtOverAllBundle.Columns.Add("CourseTypeID", typeof(int));
    //    DtOverAllBundle.Columns.Add("curriculumcourseID", typeof(int));
    //}

    private DataSet GetCurriculumPackages(int CurriculumID, int CurriculumCourseID)
    {
        _db = new DataHelper("DefaultConnection");
        var ds = _db.GetCurriculumPackages(CurriculumID, CurriculumCourseID);
        return ds;
    }

    public void GetDBPackages(int ScoTypeID, RadListView radListView1, int SectionID)
    {
        if (resources != null)
        {
            var dt = resources.Where(r => r.ScoTypeId == ScoTypeID && r.SectionID == SectionID)
               .ToList();
            radListView1.DataSource = dt;
            radListView1.DataBind();
        }

    }

    //protected void lvTrainerResource_OnItemDataBound(object sender, RadListViewItemEventArgs e)
    ////{
    ////    if (e.Item is RadListViewItem)
    ////    {

    ////        RadListViewDataItem dataItem = e.Item as RadListViewDataItem;
    ////        var linkTitle = dataItem.FindControl("btnScoTitleTR") as LinkButton;
    ////        var dripEnable = lvTrainerResource.DataKeyValues[dataItem.DataItemIndex]["dripEnable"].ToString();
    ////        int _userCIM = Convert.ToInt32(DataHelper.GetCurrentUserCIM());

    ////        if (dripEnable == "1" || (DataHelper.isOverDue(CourseID) || !DataHelper.isEnrolled(_userCIM, CourseID) || !DataHelper.hasCourseStarted(CourseID)) && DataHelper.isCourseOpen(CourseID) == false && DataHelper.isEnrollmentAllowed(CourseID) == false)
    ////        {
    ////            linkTitle.OnClientClick = "";
    ////            linkTitle.Enabled = false;
    ////        }

    ////        else
    ////        {
    ////            linkTitle.Enabled = true;
    ////        }
    ////    }
    ////}

    ////protected void lvAdditionalResource_OnItemDataBound(object sender, RadListViewItemEventArgs e)
    //{
    //    itemIndex = 0;
    //    if (e.Item is RadListViewItem)
    //    {
    //        RadListViewDataItem dataItem = e.Item as RadListViewDataItem;

    //        var linkTitle = dataItem.FindControl("btnScoTitleAR") as LinkButton;

    //        var dripEnable = lvAdditionalResource.DataKeyValues[dataItem.DataItemIndex]["dripEnable"].ToString();

    //        int _userCIM = Convert.ToInt32(DataHelper.GetCurrentUserCIM());



    //        if (dripEnable == "1" || (DataHelper.isOverDue(CourseID) || !DataHelper.isEnrolled(_userCIM, CourseID) || !DataHelper.hasCourseStarted(CourseID)) && DataHelper.isCourseOpen(CourseID) == true && !DataHelper.isEnrollmentAllowed(CourseID) == true)
    //        {
    //            linkTitle.OnClientClick = "";
    //            linkTitle.Enabled = false;

    //        }

    //        else
    //        {
    //            linkTitle.Enabled = true;
    //        }
    //    }

    //}

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            db = new TranscomUniversityV3ModelContainer();

            CourseID = Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"]));
            global_courseType = (int)DataHelper.GetCourseType(CourseID);

            if (global_TimezoneOffset == 0)
            {
                var tz = "";

                tz = Utils.Decrypt(Request.QueryString["tz"]);
                global_TimezoneOffset = Convert.ToInt32(tz);

            }

            

            if (DataHelper.GetCurrentUserCIM() != "")
            {
                enrolleeCIM = Convert.ToInt32(DataHelper.GetCurrentUserCIM());

                //GetDBPackages(CourseID, PreviewScormCourse);
                //if (DataHelper.GetCourseType(CourseID) == 1)//if non scorm
                //{
                //    cLaunch.Visible = false;
                //    cLaunch_ns.Visible = true;
                //}

                //setProgress();
                loadCourseDetails(enrolleeCIM, CourseID);
                bool notInTakeLater = isNotInTakeLater(CourseID);
                bool inEnrolled = DataHelper.isEnrolled(enrolleeCIM, CourseID);
                if (inEnrolled == true)
                {
                    BtnTakeLater.BackColor = Color.LightGray;
                    BtnTakeLater.OnClientClick = "";
                    radTakeLaterCourses.Visible = false;
                    BtnTakeLater.Enabled = false;
                    BtnTakeLater.Visible = false;
                    //BtnEnrolled.Visible = false;

                }
                else
                {
                    radTakeLaterCourses.Visible = true;
                    BtnTakeLater.OnClientClick = "openTakeCoursesWindow(); return false;";
                    BtnTakeLater.Enabled = notInTakeLater;
                    if (!notInTakeLater)
                    {
                        BtnTakeLater.Style.Add("background-color", "lightgray !important");
                        BtnTakeLater.OnClientClick = "javascript:void(0); return false;";
                    }

                }

                //true while settings is not finished
                Panel pnlCoursePrev = (Panel)(this.Parent.Parent).FindControl("pnlCoursePrev");
                Panel pnlCourseForum = (Panel)(this.Parent.Parent).FindControl("pnlCourseForum");
                if (isForumEnabled(CourseID))
                {

                    pnlCoursePrev.CssClass = pnlCoursePrev.CssClass.Replace("col-sm-12", "col-sm-8");
                    pnlCourseForum.CssClass = pnlCourseForum.CssClass.Replace("display-none", "");
                    pnlCourseForum.Visible = true;
                    if (DataHelper.isThreadMember(enrolleeCIM, CourseID))
                    {
                        //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", Utils.callClientScript("showForumPanel"), true);

                        BtnJoinForumFake.Visible = false;
                        BtnJoinForum.Visible = false;
                        BtnLeaveForum.Visible = true;
                        BtnLeaveForumFake.Visible = true;
                    }
                    else
                    {

                        BtnJoinForumFake.Visible = true;
                        BtnJoinForum.Visible = true;
                        BtnLeaveForum.Visible = false;
                        BtnLeaveForumFake.Visible = false;
                    }

                }
                else
                {
                    pnlCourseForum.Visible = false;
                    hideBtnForums();
                }


                //var db = new TranscomUniversityV3ModelContainer();

                List<vw_TranscomUniversity_TakeLateCoursesFolders> takeLaterCourseItems = DataHelper.getTakeLaterCourses(enrolleeCIM);
                rdtTakeLaterCourses.DataSource = takeLaterCourseItems;
                rdtTakeLaterCourses.DataBind();
                lblItemCount.Text = takeLaterCourseItems.Count.ToString();
                rdtTakeLaterCourses.Visible = true;

                //var dtcurrent =
                dtServerTime = DataHelper.serverDate();
                lblCurrentDate.Text = dtServerTime.ToString("MM/dd/yyyy");
                //imgPicStatus.Src = HttpContext.Current.User.Identity.Name.Split('|')[2];

                SetPage_RolePreviledges();
                //generateControl();

            }
            else
            {
                //in logged out state
                //hdnOverall.Value = "0";
                loadCourseDetails(CourseID);

            }
            DataSet ds = DataHelper.GetCourseSetting(CourseID);


            pnlSimilar.Visible = Convert.ToInt32(ds.Tables[0].Rows[0]["SimilarCourseWidget"]) == 1 ? true : false;

            if (Session["EnrollmentSuccessful"] != null)
            {
                Session["EnrollmentSuccessful"] = null;
                rwmCoursePrev.RadAlert("You have successfully enrolled in this Course.", 330, 180, "Success", "");
            }
            
        }
    }


    //private void setProgress()
    //{

    //    var email = HttpContext.Current.User.Identity.Name.Split('|')[0];
    //    if (DataHelper.GetCourseType(CourseID) == 2)
    //    {
    //        overAllProgress = ScormDataHelper.GetOverallProgressByCourseID(email, CourseId);
    //    }
    //    else
    //    {
    //        overAllProgress = db.pr_TranscomUniversity_GetNonScormProgress(enrolleeCIM, CourseId).SingleOrDefault().NonScormProgress.ToString();
    //    }


    //    //lblBundleTitle.Text = bundle.Title;
    //    //.Text = bundle.Description;
    //    hdnOverall.Value = overAllProgress;
    //}

    protected void generateControl()
    {
        PlaceHolder plForum = (PlaceHolder)(this.Parent).FindControl("phForum");
        UserControl myControl = (UserControl)Page.LoadControl("~/UserControl/Pages/CourseForumUserCtrl.ascx");
        plForum.Controls.Add(myControl);
    }

    protected void InsertCertificates(int? courseID) {
        var googleId = DataHelper.GetGoogleID();

        db.pr_TranscomUniversity_InsertInternalCertificates(googleId, courseID);

    }

    void loadCourseDetails(int enrolleeCIM, int CourseID)
    {
        var email = DataHelper.getCurrentUserEmail();
        bool isDue = false;

        DataSet ds = DataHelper.GetCourseDetails(CourseID);
        LblCourseTitle.Text = ds.Tables[0].Rows[0]["Title"].ToString();
        LblDesc.Text = ds.Tables[0].Rows[0]["Description"].ToString();
        ltCourseName.Text = ds.Tables[0].Rows[0]["Title"].ToString();
        hfCourseName.Value = ds.Tables[0].Rows[0]["Title"].ToString(); ;

        int type = Convert.ToInt32(ds.Tables[0].Rows[0]["TrainingTypeID"]);

        if (type == 1)
        {
            //OLT
            LblType.Text = "OLT";  
        }
        else
        {
            //ILT
            LblType.Text = "ILT";
        }

        var courseStatus = db.pr_TranscomUniversity_getCourseProgressStatus(CourseID, email).SingleOrDefault();
        lblCourseStatus.Text = courseStatus.Status;
        //if (courseStatus.Status == "Complete")
        //    InsertCertificates(CourseID);
        LblUploadDate.Text = ((DateTime)(ds.Tables[0].Rows[0]["StartDate"])).ToString("MMMM dd, yyyy");
        LblValidDate.Text = ((DateTime)(ds.Tables[0].Rows[0]["EndDate"])).ToString("MMMM dd, yyyy");
        lblValid.Text = ((DateTime)(ds.Tables[0].Rows[0]["EndDate"])).ToString("MMMM dd, yyyy");
        lblCategory.Text = (ds.Tables[0].Rows[0]["CategoryName"]).ToString();
        lblSubcategory.Text = (ds.Tables[0].Rows[0]["Subcategory"]).ToString();
        isDue = (DateTime)(ds.Tables[0].Rows[0]["EndDate"]) < DataHelper.serverDate();
        hfIsDue.Value = isDue == true ? "1" : "0";
        int? isInProgress = db.tbl_TranscomUniversity_Cor_CourseLogin
                                .Where(c => c.CourseID == CourseID && c.EnrolleeCIM == enrolleeCIM)
                                .Select(c => c.CourseID)
                                .FirstOrDefault();
        hfIsInProgress.Value = isInProgress == null ? "0" : isInProgress.ToString();
        dtCourseEnd = (DateTime)(ds.Tables[0].Rows[0]["EndDate"]);
        //if ((DataHelper.isEnrolled(enrolleeCIM, CourseID) && overAllProgress == "100"))
        //    pnlRating.Visible = true;
        //else
        //    pnlRating.Visible = false;

        string CourseImg = "Media/Uploads/CourseImg/" + CourseID.ToString() + "/" + HttpUtility.HtmlEncode(ds.Tables[0].Rows[0]["CourseImage"].ToString());
        BgCourse.Attributes["style"] = "background-image: url('" + CourseImg + "'), url('Media/Uploads/CourseImg/No_image.jpg')";
        hfCourseImage.Value = WebUtility.HtmlEncode(CourseImg);
        decimal userRating = db.vw_TranscomUniversity_CourseReview.Where(u => u.EnrolleeCIM == enrolleeCIM && u.CourseID == CourseID).Select(u => (decimal)u.Rating).SingleOrDefault();
        txtComment.Text = db.vw_TranscomUniversity_CourseReview.Where(u => u.EnrolleeCIM == enrolleeCIM && u.CourseID == CourseID).Select(u => u.Comment).SingleOrDefault();
        rrCourse.Value = userRating;
        lblName.Text = DataHelper.getUserName(enrolleeCIM);
        hfOldValue.Value = userRating.ToString();
        int status = DataHelper.EnrollmentStatus(enrolleeCIM, CourseID);
        imgPicStatus.Src = HttpContext.Current.User.Identity.Name.Split('|')[2].ToString();
        updateBtnEnrolled(status, DataHelper.isCourseValid(CourseID, global_TimezoneOffset));
        hfRating.Value = userRating.ToString();
        if (userRating == 0 || txtComment.Text.Trim().Length == 0)
            hfShowRating.Value = "true";

        //double userCount = db.vw_TranscomUniversity_ApprovedEnrollments.Where(a => a.CourseID == CourseID).Count();
        double userCount = 0.0;
        try
        {
            var courseStats = DataHelper.courseReviewStats(CourseID);
            lblStarRating.Text = string.Format("{0:0.0}", courseStats[0].Rating);
            lblViews.Text = string.Format("{0:n0}", courseStats[0].Views);
            lblReviews.Text = string.Format("{0:n0}", courseStats[0].Reviews);
            userCount = Convert.ToInt32(string.Format("{0:n0}", courseStats[0].Users));
            lblUserCount.Text = string.Format("{0:n0}", courseStats[0].Users);
            rrCourseStatRating.Value = courseStats[0].Rating;
            lblCourseStatRating.Text = string.Format("{0:0.0}", courseStats[0].Rating);


        }
        catch (Exception)
        {

            lblStarRating.Text = string.Format("{0:0.0}", 0);
            lblViews.Text = string.Format("{0:n0}", 0);
            lblReviews.Text = string.Format("{0:n0}", 0);
            lblUserCount.Text = string.Format("{0:n0}", userCount);
            rrCourseStatRating.Value = 0;
            lblCourseStatRating.Text = string.Format("{0:0.0}", 0);


        }


        double FiveStarCount = db.vw_TranscomUniversity_CourseComments.Where(c => c.CourseID == CourseID && c.Rating == 5).Count();
        double FourStarCount = db.vw_TranscomUniversity_CourseComments.Where(c => c.CourseID == CourseID && c.Rating == 4).Count();
        double ThreeStarCount = db.vw_TranscomUniversity_CourseComments.Where(c => c.CourseID == CourseID && c.Rating == 3).Count();
        double TwoStarCount = db.vw_TranscomUniversity_CourseComments.Where(c => c.CourseID == CourseID && c.Rating == 2).Count();
        double OneStarCount = db.vw_TranscomUniversity_CourseComments.Where(c => c.CourseID == CourseID && c.Rating == 1).Count();

        lblFiveStarCount.Text = string.Format("{0:n0}", FiveStarCount.ToString());
        lblFourStarCount.Text = string.Format("{0:n0}", FourStarCount.ToString());
        lblThreeStarCount.Text = string.Format("{0:n0}", ThreeStarCount.ToString());
        lblTwoStarCount.Text = string.Format("{0:n0}", TwoStarCount.ToString());
        lblOneStarCount.Text = string.Format("{0:n0}", OneStarCount.ToString());
        FiveStarBar.Attributes.Add("style", "width:" + calculateRate(FiveStarCount, userCount) + "%");
        FourStarBar.Attributes.Add("style", "width:" + calculateRate(FourStarCount, userCount) + "%");
        ThreeStarBar.Attributes.Add("style", "width:" + calculateRate(ThreeStarCount, userCount) + "%");
        TwoStarBar.Attributes.Add("style", "width:" + calculateRate(TwoStarCount, userCount) + "%");
        OneStarBar.Attributes.Add("style", "width:" + calculateRate(OneStarCount, userCount) + "%");

        //string category = db.tbl_TranscaomUniversity_Cor_Course.Where(c => c.CourseID == CourseID).Select(c => c.ca)
        //    DataHelper.GetCourseDetails
        //    string Title = 

        //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", Utils.callClientScript("confirmRating"), true);
        

    }

    void loadCourseDetails(int CourseID)
    {
        var db = new TranscomUniversityV3ModelContainer();
        var email = HttpContext.Current.User.Identity.Name.Split('|')[0];
        bool isDue = false;

        DataSet ds = DataHelper.GetCourseDetails(CourseID);
        LblCourseTitle.Text = ds.Tables[0].Rows[0]["Title"].ToString();
        LblDesc.Text = ds.Tables[0].Rows[0]["Description"].ToString();


        int type = 1;

        if (type == 1)
        {
            //OLT
            LblType.Text = "OLT";
        }
        else
        {
            //ILT
            LblType.Text = "ILT";
        }

        lblCourseStatus.Visible = false;
        LblUploadDate.Text = ((DateTime)(ds.Tables[0].Rows[0]["StartDate"])).ToString("MMMM dd, yyyy");
        LblValidDate.Text = ((DateTime)(ds.Tables[0].Rows[0]["EndDate"])).ToString("MMMM dd, yyyy");
        lblValid.Text = ((DateTime)(ds.Tables[0].Rows[0]["EndDate"])).ToString("MMMM dd, yyyy");
        lblCategory.Text = (ds.Tables[0].Rows[0]["CategoryName"]).ToString();
        lblSubcategory.Text = (ds.Tables[0].Rows[0]["Subcategory"]).ToString();
        isDue = (DateTime)(ds.Tables[0].Rows[0]["EndDate"]) < DataHelper.serverDate();
        hfIsDue.Value = isDue == true ? "1" : "0";
        int? isInProgress = 0;

        dtCourseEnd = (DateTime)(ds.Tables[0].Rows[0]["EndDate"]);
        hfIsInProgress.Value = isInProgress == null ? "0" : isInProgress.ToString();
        //pnlRating.Visible = false;

        string CourseImg = "Media/Uploads/CourseImg/" + CourseID.ToString() + "/" + ds.Tables[0].Rows[0]["CourseImage"].ToString();
        BgCourse.Attributes["style"] = "background-image: url('" + CourseImg + "'), url('Media/Uploads/CourseImg/No_image.jpg')";


        txtComment.Visible = false;
        rrCourse.Visible = false;
        lblName.Visible = false;
        hfOldValue.Visible = false;

        double userCount = 0.0;
        try
        {
            var courseStats = DataHelper.courseReviewStats(CourseID);
            lblStarRating.Text = string.Format("{0:0.0}", courseStats[0].Rating);
            lblViews.Text = string.Format("{0:n0}", courseStats[0].Views);
            lblReviews.Text = string.Format("{0:n0}", courseStats[0].Reviews);
            userCount = Convert.ToInt32(string.Format("{0:n0}", courseStats[0].Users));
            lblUserCount.Text = string.Format("{0:n0}", courseStats[0].Users);
            rrCourseStatRating.Value = courseStats[0].Rating;
            lblCourseStatRating.Text = string.Format("{0:0.0}", courseStats[0].Rating);

        }
        catch (Exception)
        {

            lblStarRating.Text = string.Format("{0:0.0}", 0);
            lblViews.Text = string.Format("{0:n0}", 0);
            lblReviews.Text = string.Format("{0:n0}", 0);
            lblUserCount.Text = string.Format("{0:n0}", userCount);
            rrCourseStatRating.Value = 0;
            lblCourseStatRating.Text = string.Format("{0:0.0}", 0);

        }


        double FiveStarCount = db.vw_TranscomUniversity_CourseComments.Where(c => c.CourseID == CourseID && c.Rating == 5).Count();
        double FourStarCount = db.vw_TranscomUniversity_CourseComments.Where(c => c.CourseID == CourseID && c.Rating == 4).Count();
        double ThreeStarCount = db.vw_TranscomUniversity_CourseComments.Where(c => c.CourseID == CourseID && c.Rating == 3).Count();
        double TwoStarCount = db.vw_TranscomUniversity_CourseComments.Where(c => c.CourseID == CourseID && c.Rating == 2).Count();
        double OneStarCount = db.vw_TranscomUniversity_CourseComments.Where(c => c.CourseID == CourseID && c.Rating == 1).Count();

        lblFiveStarCount.Text = string.Format("{0:n0}", FiveStarCount.ToString());
        lblFourStarCount.Text = string.Format("{0:n0}", FourStarCount.ToString());
        lblThreeStarCount.Text = string.Format("{0:n0}", ThreeStarCount.ToString());
        lblTwoStarCount.Text = string.Format("{0:n0}", TwoStarCount.ToString());
        lblOneStarCount.Text = string.Format("{0:n0}", OneStarCount.ToString());

        FiveStarBar.Attributes.Add("style", "width:" + calculateRate(FiveStarCount, userCount) + "%");
        FourStarBar.Attributes.Add("style", "width:" + calculateRate(FourStarCount, userCount) + "%");
        ThreeStarBar.Attributes.Add("style", "width:" + calculateRate(ThreeStarCount, userCount) + "%");
        TwoStarBar.Attributes.Add("style", "width:" + calculateRate(TwoStarCount, userCount) + "%");
        OneStarBar.Attributes.Add("style", "width:" + calculateRate(OneStarCount, userCount) + "%");

        BtnJoinForum.Visible = false;
        BtnJoinForumFake.Visible = false;
        BtnLeaveForum.Visible = false;
        BtnLeaveForumFake.Visible = false;
        radTakeLaterCourses.Visible = false;
        //pnlOverallProgress.Visible = false;
        BtnEnrolled.OnClientClick = "showPleaseSignin(1); return false;";
        pnlAdditional.Visible = false;
        pnlTrainer.Visible = false;

    }

    protected void btnLeaveComment_Click(object sender, EventArgs e)
    {
        int enrolleeCIM = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
        int courseID = Convert.ToInt16(Utils.Decrypt(Request.QueryString["CourseID"]));
        int rating = Convert.ToInt32(hfRating.Value.Trim());

        string comment = txtComment.Text.Trim();
        comment = comment == "" ? " " : comment;

        try
        {
            var db = new TranscomUniversityV3ModelContainer();
            db.pr_TranscomUniversity_InsertCourseReview(enrolleeCIM, courseID, rating, comment);
            db.SaveChanges();
            hfOldValue.Value = rating.ToString();
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", Utils.callClientScript("closeRating", "1"), true);
            //RadListView lvComments = (RadListView)ucComments.FindControl("rdComments");
            //lvComments.Rebind();
            loadCourseDetails(enrolleeCIM, courseID);
            //rwmCoursePrev.RadAlert("Your review has been recorded, Thank you!", 330, 180, "Success Message", "callBackReload");
        }
        catch
        {
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", Utils.callClientScript("closeRating" , "2"), true);
            rwmCoursePrev.RadAlert("Failed to save your review, Please contact the administrator", 330, 180, "Error Message", "callBackReload");
        }
        //radCommentWindow.Visible = false;

        string showRating = "false";
        if (rating == 0 || comment.Trim().Length == 0)
            showRating = "true";

        ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "setStatus", Utils.callClientScript("setStatus", "Completed", "1", showRating), true);


    }

    private double calculateRate(double starCount, double userCount)
    {
        double rating = (starCount / userCount);
        return Math.Round(rating * 100, 2);
    }

    protected void BtnEnrolled_Click(object sender, EventArgs e)
    {
        if (DataHelper.GetCurrentUserCIM() != "")
        {
            string cmd = BtnEnrolled.CommandArgument.ToString();
            int enrolleeCIM = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
            int courseID = Convert.ToInt16(Utils.Decrypt(Request.QueryString["CourseID"]));


            if (DataHelper.EnrollmentStatus(enrolleeCIM, courseID) == 3)
            {
                rwmCoursePrev.RadAlert("You enrollment in this course was cancelled by the admin.", 330, 180, "Enrollement Denied", "");
            }
            else if (DataHelper.isCourseValid(courseID, global_TimezoneOffset))
            {
                if (cmd == "1")
                {
                    var db = new TranscomUniversityV3ModelContainer();
                    pr_TranscomUniversity_CancelEnrollToCourse_Result res = db.pr_TranscomUniversity_CancelEnrollToCourse(enrolleeCIM, courseID, enrolleeCIM).SingleOrDefault();
                    updateBtnEnrolled(res.EnrollmentStatus, DataHelper.isCourseValid(courseID, global_TimezoneOffset));
                }
                else if (cmd == "2")
                {
                    if (DataHelper.isCourseOpen(courseID) == true)
                    {
                        int enrollmentStatus = DataHelper.selfEnrollment(enrolleeCIM, courseID);
                        //updateBtnEnrolled(enrollmentStatus, true);
                        if (enrollmentStatus == 1)
                        {
                            Session["EnrollmentSuccessful"] = true;
                            Response.Redirect("CourseLauncher.aspx?CourseID=" + Request.QueryString["CourseID"] + "&tz=" + Request.QueryString["tz"], true);
                        }
                        else
                            rwmCoursePrev.RadAlert("Enrollment Not Allowed", 330, 180, "Error", "");
                                
                    }
                    
                }
                else
                {
                    var db = new TranscomUniversityV3ModelContainer();
                    pr_TranscomUniversity_EnrollToCourse_Result res = db.pr_TranscomUniversity_EnrollToCourse(enrolleeCIM, courseID, enrolleeCIM).SingleOrDefault();
                    updateBtnEnrolled(res.EnrollmentStatus, DataHelper.isCourseValid(courseID, global_TimezoneOffset));
                }
            }
            else
            {
                rwmCoursePrev.RadAlert("You can\\'t enroll anymore to this course", 330, 180, "Error Message", "");
            }
        }
        else
        {
            rwmCoursePrev.RadAlert("Please sign-in to enroll to this course", 330, 180, "Sign in", "");
        }
    }

    protected void btnAddNewFolder_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            int enrolleeCIM = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
            string folderName = txtFolderName.Text.Trim();
            int selectedParentID = Convert.ToInt32(hfSelectedFolderID.Value.Trim());
            var db = new TranscomUniversityV3ModelContainer();

            try
            {
                pr_TranscomUniversity_AddTakeLaterCourseFolder_Result res = db.pr_TranscomUniversity_AddTakeLaterCourseFolder(enrolleeCIM, folderName).SingleOrDefault();

                if (res.Response == 2)
                {
                   
                    lblFolderError.Text = "Folder name already exists!";
                    lblFolderError.Visible = true;
                    ScriptManager.RegisterStartupScript(Page, typeof(Page), "FolderkeyHideLoading", Utils.callClientScript("hideLoading"), true);
                    rwmCoursePrev.RadAlert("Folder name already exists!", 330, 180, "Error Message", "");
                }
                else
                {
                    lblFolderError.Visible = false;
                    rdtTakeLaterCourses.DataSource = DataHelper.getTakeLaterCourses(enrolleeCIM);
                    rdtTakeLaterCourses.DataBind();
                    ScriptManager.RegisterStartupScript(Page, typeof(Page), "activateRadTakeCourses", Utils.callClientScript("activateRadTakeCourses"), true);
                }

                //rdtTakeLaterCourses.DataSource = DataHelper.getTakeLaterCourses(enrolleeCIM);
                //rdtTakeLaterCourses.DataBind();

            }
            catch
            {
                lblFolderError.Text = "Something went wrong in adding folder, Please try again.";
                lblFolderError.Visible = true;
                //rwmCoursePrev.RadAlert("Failed in adding Folder, Please contact your Administrator!", 330, 180, "Error Message", "");
            }

           

        }

    }

    protected void rdtTakeLaterCourses_NeedDataSource(object sender, EventArgs e)
    {
        int enrolleeCIM = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
        rdtTakeLaterCourses.DataSource = DataHelper.getTakeLaterCourses(enrolleeCIM);
    }

    protected void rdtTakeLaterCourses_DataBound(object sender, EventArgs e)
    {
        RadTreeNode root = new RadTreeNode("Take Later Courses");

        while (rdtTakeLaterCourses.Nodes.Count > 0)
        {
            root.Nodes.Add(rdtTakeLaterCourses.Nodes[0]);
        }
        rdtTakeLaterCourses.Nodes.Add(root);
    }

    protected void btnAddToTakeLater_OnClick(object sender, EventArgs e)
    {
        if (DataHelper.GetCurrentUserCIM() != "")
        {
            int enrolleeCIM = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
            int fileID = Convert.ToInt16(Utils.Decrypt(Request.QueryString["CourseID"]));
            int? parentFileID = Convert.ToInt32(hfSelectedFolderID.Value.Trim()) > 0 ? Convert.ToInt32(hfSelectedFolderID.Value.Trim()) : (int?)null;
            string parentFileName = hfSelectedFolderName.Value.Trim();
            //dtCourseEnd = DateTime.TryParse(LblValidDate.Text.ToString(), out temp);

            DateTime? notifyMeDate = null;

            if (DataHelper.isCourseValid(fileID, global_TimezoneOffset))
            {
                //DateTime temp;
                //if (DateTime.TryParse(dpNotifyMeDate.SelectedDate.ToString(), out temp) == false && dpNotifyMeDate.InvalidTextBoxValue.Trim().Length > 0)
                //{
                //    rwmCoursePrev.RadAlert("Invalid Date", 330, 180, "Error Date", "");
                //    return;
                //}
                //else if (dpNotifyMeDate.SelectedDate != null)
                //{
                //    notifyMeDate = Convert.ToDateTime(dpNotifyMeDate.SelectedDate);
                //    if (notifyMeDate <= dtServerTime)
                //    {
                //        rwmCoursePrev.RadAlert("Notify Date should be greater than current date", 330, 180, "Error Date", "");
                //        return;
                //    }
                //    else if (notifyMeDate > dtCourseEnd)
                //    {
                //        rwmCoursePrev.RadAlert("Notify Date should be less than expiration date", 330, 180, "Error Date", "");
                //        return;
                //    }


                //}

                try
                {
                    var db = new TranscomUniversityV3ModelContainer();

                    pr_TranscomUniversity_AddCourseToTakeLaterCourses_Result res = db.pr_TranscomUniversity_AddCourseToTakeLaterCourses(enrolleeCIM, fileID, parentFileID, parentFileName, notifyMeDate).SingleOrDefault();

                    if (res.Response == 2)
                    {
                        rwmCoursePrev.RadAlert("Selected Folder does not exists", 330, 180, "Error Message", "");
                    }
                    else if (res.Response == 3)
                    {
                        rwmCoursePrev.RadAlert("Course already exists in Take Later Courses", 330, 180, "Error Message", "");

                    }
                    else
                    {
                        //radTakeLaterCourses.Behaviors =  WindowBehaviors.Close | WindowBehaviors.Move ;
                        rwmCoursePrev.RadAlert("Course added Successfuly!", 330, 180, "Success Message", "");
                        //btnAddToTakeLater.Enabled = false;
                        //BtnTakeLater.Style.Add("background-color", "lightgray !important");
                        //BtnTakeLater.Enabled = false;
                        //BtnTakeLater.OnClientClick = "javascript:void(0); return false;";
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "enableAddTakeLaterBehavior", Utils.callClientScript("enableAddTakeLaterBehavior"), true);


                    }
                    rdtTakeLaterCourses.DataSource = DataHelper.getTakeLaterCourses(enrolleeCIM);
                    rdtTakeLaterCourses.DataBind();
                }
                catch
                {
                    rwmCoursePrev.RadAlert("Something went wrong in adding the course, Please contact your Administrator!", 330, 180, "Error Message", "");
                }
            }
            else
            {
                rwmCoursePrev.RadAlert("You can\\'t add this course anymore to Take Later Courses!", 330, 180, "Error Message", "");
            }
        }
        else
        {
            rwmCoursePrev.RadAlert("Please sign-in to add this course to your take later list.", 330, 180, "Sign in", "");
        }
        //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", Utils.callClientScript("enableAddTakeLaterBehavior"), true);

    }

    private void updateBtnEnrolled(int status, bool isValid)
    {

        if (isValid == true)
        {

            hfenrolledAndIsValid.Value = status == 2 && isValid == true ? "1" : "0";
            
            //if (DataHelper.isEnrollmentAllowed(CourseID) == false)
            //{
            //    BtnEnrolled.Enabled = false;
            //    BtnEnrolled.BackColor = Color.LightGray;

            //    //BtnTakeLater.Enabled = false;
            //    //BtnTakeLater.BackColor = Color.LightGray;
            //}
            CourseID = Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"]));
            enrolleeCIM = Convert.ToInt32(DataHelper.GetCurrentUserCIM());

            if (status == 2)
            { 
                //enrolled
                BtnEnrolled.Text = "Enrolled";
                BtnEnrolled.BackColor = Color.LightGray;
                //BtnEnrolled.CommandArgument = "";
                BtnEnrolled.Enabled = true;
                BtnEnrolled.OnClientClick = "javascript:void(0); return false;";
                BtnEnrolled.Style.Add("background-color", "lightgray !important");
                BtnEnrolled.Visible = false;
                BtnTakeLater.Visible = false;

            }else if (status == 1)
            {
                 //Enrollment Pending
                    //pnlRating.Visible = false;
                    BtnEnrolled.Text = "Pending Approval";
                    BtnEnrolled.Style.Add("background-color", "lightgray !important");
                    BtnEnrolled.CommandArgument = "1";
                    BtnEnrolled.CssClass += " btn-hover-forAprroval";
                    BtnEnrolled.Enabled = true;
                    BtnTakeLater.Enabled = true;
                    BtnTakeLater.BackColor = System.Drawing.ColorTranslator.FromHtml("#0D9E9E");
            }
            else if (DataHelper.isCourseEnrollmentWithApproval(CourseID) == true && !DataHelper.isEnrolled(enrolleeCIM, CourseID))
            {

                BtnEnrolled.Text = "Enroll";
                //BtnEnrolled.BackColor = System.Drawing.ColorTranslator.FromHtml("#0D9E9E");
                BtnEnrolled.Style.Add("background-color", " #ff5046 !important");
                BtnEnrolled.CommandArgument = "3";
                BtnEnrolled.CssClass = BtnEnrolled.CssClass.Replace("btn-hover-forAprroval", "");
                BtnEnrolled.Enabled = true;
                BtnTakeLater.Enabled = true;
                BtnTakeLater.BackColor = System.Drawing.ColorTranslator.FromHtml("#0D9E9E");
            }
            else if (DataHelper.isCourseOpen(CourseID) == true && !DataHelper.isEnrolled(enrolleeCIM, CourseID))
            {

                BtnEnrolled.Text = "Enroll";
                //BtnEnrolled.BackColor = System.Drawing.ColorTranslator.FromHtml("#0D9E9E");
                BtnEnrolled.Style.Add("background-color", " #ff5046 !important");
                BtnEnrolled.CommandArgument = "2";
                BtnEnrolled.CssClass = BtnEnrolled.CssClass.Replace("btn-hover-forAprroval", "");
                BtnEnrolled.Enabled = true;
                BtnTakeLater.Enabled = true;
                BtnTakeLater.BackColor = System.Drawing.ColorTranslator.FromHtml("#0D9E9E");
            }
            //else if (DataHelper.isEnrollmentAllowed(CourseID) == true && DataHelper.isCourseOpen(CourseID) == true && DataHelper.isEnrolled(enrolleeCIM, CourseID))
            //{
            //    BtnEnrolled.Text = "Enrolled";
            //    BtnEnrolled.BackColor = Color.LightGray;
            //    BtnEnrolled.CommandArgument = "none";
            //    BtnEnrolled.CssClass = BtnEnrolled.CssClass.Replace("btn-hover-forAprroval", "");
            //    BtnEnrolled.Enabled = false;
            //    BtnTakeLater.Enabled = true;
            //    BtnTakeLater.BackColor = System.Drawing.ColorTranslator.FromHtml("#0D9E9E");

            //}
            //if (DataHelper.EnrollmentStatus(enrolleeCIM,CourseID) == 1)
            else
            {

                //if (status == 1)
                //{
                //    //Enrollment Pending
                //    //pnlRating.Visible = false;
                //    BtnEnrolled.Text = "Pending Approval";
                //    BtnEnrolled.Style.Add("background-color", "lightgray !important");
                //    BtnEnrolled.CommandArgument = "1";
                //    BtnEnrolled.CssClass += " btn-hover-forAprroval";
                //    BtnEnrolled.Enabled = true;
                //    BtnTakeLater.Enabled = true;
                //    BtnTakeLater.BackColor = System.Drawing.ColorTranslator.FromHtml("#0D9E9E");
                //}
                //else if (status == 2)
                //{
                //    //Enrollment Approved
                //    //pnlRating.Visible = true;
                //    BtnEnrolled.Text = "Enrolled";
                //    BtnEnrolled.BackColor = Color.LightGray;
                //    BtnEnrolled.CommandArgument = "none";
                //    BtnEnrolled.Enabled = false;
                //    BtnTakeLater.Visible = false;

                //}


                //Not Enrolled and enrollment is not allowed 
                //pnlRating.Visible = false;
                BtnEnrolled.Text = "Enroll";
                //BtnEnrolled.BackColor = System.Drawing.ColorTranslator.FromHtml("#0D9E9E");
                BtnEnrolled.Style.Add("background-color", " #ff5046 !important");
                BtnEnrolled.OnClientClick = "";
                BtnEnrolled.CssClass = BtnEnrolled.CssClass.Replace("btn-hover-forAprroval", "");
                BtnEnrolled.Enabled = false;
                BtnTakeLater.Enabled = true;
                BtnTakeLater.BackColor = System.Drawing.ColorTranslator.FromHtml("#0D9E9E");
            }
        }
        else
        {
            BtnEnrolled.Enabled = false;
            BtnEnrolled.Style.Add("background-color", "lightgray !important");
            BtnEnrolled.CommandArgument = "none";
            BtnEnrolled.OnClientClick = "";
        }


    }

    protected void BtnJoinForum_Clicked(object sender, CommandEventArgs e)
    {
        //1 for not member
        //2 for member
        string membership = e.CommandArgument.ToString();
        var db = new TranscomUniversityV3ModelContainer();
        int courseId = Convert.ToInt16(Utils.Decrypt(Request.QueryString["CourseID"]));
        int userCim = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
        if (membership == "2")
        {

            int forumRole = 1; //default 1 for participant

            int addedBy = Convert.ToInt32(DataHelper.GetCurrentUserCIM());


            pr_TranscomUniversity_InsertThreadMember_Result res = db.pr_TranscomUniversity_InsertThreadMember(courseId, userCim, forumRole, addedBy,1).SingleOrDefault();

            if (res.Response == 1)
            {
                BtnJoinForum.CommandArgument = "1";
                Panel pnlForum = (Panel)(this.Parent.Parent).FindControl("forumContainer");
                pnlForum.Visible = true;
                //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", Utils.callClientScript("refreshPage"), true);
                //showForum();
            }
            else
                rwmCoursePrev.RadAlert("Unable to join this forum", 330, 180, "Error Message", "");
        }
        else
        {
            //leave
            pr_TranscomUniversity_RemoveForumMember_Result res = db.pr_TranscomUniversity_RemoveForumMember(courseId, userCim).SingleOrDefault();
            if (res.Response == 1)
            {
                BtnJoinForum.CommandArgument = "2";
                //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", Utils.callClientScript("hideForumPanel"), true);
                hideForum();
            }
            else
                rwmCoursePrev.RadAlert("Error in leaving this forum. Please contact administrator", 330, 180, "Error Message", "");
        }
    }

    protected void BtnLeaveForum_Clicked(object sender, EventArgs e)
    {

        var db = new TranscomUniversityV3ModelContainer();
        int courseId = Convert.ToInt16(Utils.Decrypt(Request.QueryString["CourseID"]));
        int userCim = Convert.ToInt32(DataHelper.GetCurrentUserCIM());

        //leave
        pr_TranscomUniversity_RemoveForumMember_Result res = db.pr_TranscomUniversity_RemoveForumMember(courseId, userCim).SingleOrDefault();
        if (res.Response == 1)
        {

            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", Utils.callClientScript("hideForumPanel"), true);
            //Panel pnlForum = (Panel)(this.Parent.Parent).FindControl("forumContainer");
            //pnlForum.Visible = false;
            //hideForum();

            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", Utils.callClientScript("pageReload"), true);
            Response.Redirect(Request.RawUrl);
        }
        else
            rwmCoursePrev.RadAlert("Error in leaving this forum. Please contact administrator", 330, 180, "Error Message", "");

    }

    protected void cbAcceptForumGuide_CheckedChanged(object sender, EventArgs e)
    {

        //if (accept == true)
        //{

        var db = new TranscomUniversityV3ModelContainer();
        int courseId = Convert.ToInt16(Utils.Decrypt(Request.QueryString["CourseID"]));
        int userCim = Convert.ToInt32(DataHelper.GetCurrentUserCIM());


        int forumRole = 1; //default 1 for participant

        int addedBy = Convert.ToInt32(DataHelper.GetCurrentUserCIM());

        pr_TranscomUniversity_InsertThreadMember_Result res = db.pr_TranscomUniversity_InsertThreadMember(courseId, userCim, forumRole, addedBy,1).SingleOrDefault();

        if (res.Response == 1)
        {

            //Panel pnlForum = (Panel)(this.Parent.Parent).FindControl("forumContainer");
            //pnlForum.Visible = true;
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", Utils.callClientScript("pageReload"), true);
            //showForum();

            Response.Redirect(Request.RawUrl);
        }
        else
            rwmCoursePrev.RadAlert("Unable to join this forum", 330, 180, "Error Message", "");

        //}
        //else
        //{
        //    rwmCoursePrev.RadAlert("Check the Terms and Conditions", 330, 180, "Error Message", "");
        //}
    }

    private void hideForum()
    {
        BtnJoinForum.Text = "<i class='fa fa-comments'></i>Join Forum";
        //PlaceHolder plForum = (PlaceHolder)(this.Parent).FindControl("phForum");
        //plForum.Controls.Clear();
    }

    private bool isForumEnabled(int courseId)
    {

        var db = new TranscomUniversityV3ModelContainer();
        var showForum = db.tbl_TranscomUniversity_Rlt_CourseSetting.Where(c => c.CourseID == courseId && c.ForumWidget == true).SingleOrDefault();
        if (showForum != null)
            return true;

        return false;

    }

    private void hideBtnForums()
    {
        BtnJoinForumFake.Visible = false;
        BtnJoinForum.Visible = false;
        BtnLeaveForum.Visible = false;
        BtnLeaveForumFake.Visible = false;
        BtnJoinForumFake.Visible = false;
        BtnJoinForum.Visible = false;
        BtnLeaveForum.Visible = false;
        BtnLeaveForumFake.Visible = false;
    }

    private void SetPage_RolePreviledges()
    {
        if (Roles.IsUserInRole(HttpContext.Current.User.Identity.Name.Split('|')[0], "Admin") || Roles.IsUserInRole(HttpContext.Current.User.Identity.Name.Split('|')[0], "Trainer"))
        {
            pnlTrainer.Visible = true;
        }
        else
        {
            pnlTrainer.Visible = false;
        }
    }

    protected bool isNotInTakeLater(int courseID)
    {
        bool isIn = false;
        int userCIM = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
        var db = new TranscomUniversityV3ModelContainer();

        var id = db.tbl_TranscomUniversity_Cor_TakeLaterCourses
            .Where(c => c.FileID == courseID && c.EnrolleeCIM == userCIM && c.HideFromList == false && c.FileType == 1)
            .Select(c => c.TakeLaterCourseID)
            .SingleOrDefault();
        isIn = id > 0 ? false : true;

        return isIn;

    }

    public string completePath(string filename, int scoID, int scoTypeID)
    {
        var targetFolder = ConfigurationManager.AppSettings["OtherResourcesDirectoryPath"].ToString();
        string path = Page.ResolveClientUrl(targetFolder.Replace("\\", "//") + "/" + filename);
        //return string.Format("javascript: this.value = 'Launching...'; this.disabled = true; this.disabled = 'disabled'; window.open('{0}', '_blank');return false;", completePath);
        //return string.Format("javascript: this.value = 'Launching...'; this.disabled = true; this.disabled = 'disabled'; openNonScorm('{0}', '{1}', {2}, '{3}');return false;", completePath, enrolleeCIM, "false", Utils.Encrypt(CourseID));
        //return string.Format("openNonScorm('{0}', '{1}', {2}, '{3}');return false;", completePath, enrolleeCIM, null, Utils.Encrypt(CourseID));
        return string.Format("openNonScormNewTab('{0}', {1}, '{2}', {3});return false;", path, enrolleeCIM, Utils.Encrypt(scoID), scoTypeID);
    }

    public string getHotLink(int? TestCategoryID, int scoTypeID, int courseTypeID)
    {
        string testlink = "";
        if (HttpContext.Current.Request.Url.Host == "localhost")
            testlink = ResolveUrl("~/") + "TakeAssessment.aspx?CourseID=" + Utils.Encrypt(CourseID) + "&tid=" + HttpUtility.UrlEncode(UTF8.EncryptText(TestCategoryID.ToString()));
        //testlink = ResolveUrl("~/") + "hotlink.aspx?CourseID=" + WebUtility.HtmlEncode(Utils.Encrypt(global_CourseID)) + "&tid=" + WebUtility.HtmlEncode(UTF8.EncryptText(TestCategoryID.ToString()));
        else
            testlink = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + ResolveUrl("~/") + "TakeAssessment.aspx?tid=" + HttpUtility.UrlEncode(UTF8.EncryptText(TestCategoryID.ToString())) + "&CourseID=" + Utils.Encrypt(CourseID);
        //testlink = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + ResolveUrl("~/") + "hotlink.aspx?tid=" + HttpUtility.UrlEncode(UTF8.EncryptText(TestCategoryID.ToString())) + "&CourseID=" + Utils.Encrypt(global_CourseID);

        //return string.Format(testlink);
        return string.Format("openNonScormSamePage('{0}', {1}, '{2}', {3});return false;", testlink, enrolleeCIM, Utils.Encrypt(TestCategoryID), scoTypeID, courseTypeID);
    }

    //Adding Sections Trainer

    protected void lvCourseDefault_Trainer_SectionDataBound(object sender, RadListViewItemEventArgs e)
    {
        var dataItem = e.Item as RadListViewDataItem;
        if (dataItem != null)
        {
            RadListView lvItem = dataItem.FindControl("lvCourseDefault_Trainer_Item") as RadListView;
            int sectionID = Convert.ToInt32(lvCourseDefault_Trainer.DataKeyValues[dataItem.DataItemIndex]["SectionID"].ToString());

            GetDBPackages(8, lvItem, sectionID);

        }
    }

    protected void lvCourseDefault_Trainer_NeedDataSource(object sender, EventArgs e)
    {
        try
        {
            //foreach (var currid in CURRCOURSEID)
            //{
            var dtResources = DataHelper.GetCourseSections(CourseID, 8);
            lvCourseDefault_Trainer.DataSource = dtResources;
            var db = new TranscomUniversityV3ModelContainer();

            Dbconn("DefaultConnection");
            _ocmd = new SqlCommand("pr_Scorm_Cor_GetCurriculumPackages", _oconn)
            {
                CommandType = CommandType.StoredProcedure
            };

            _ocmd.Parameters.AddWithValue("@CurriculumID", CourseID);
            _ocmd.Parameters.AddWithValue("@isBundle", 1);
            SqlDataReader oDataReader = _ocmd.ExecuteReader();

            if (oDataReader.Read())
            {
                CurrCourseID = Convert.ToInt32(oDataReader["curriculumcourseID"].ToString());

                resources = db.pr_Scorm_Cor_GetCurriculumPackages(CourseID, CurrCourseID, 0)
                            .ToList();
            }
            //}
        }
        catch (Exception ex)
        {
            throw;
        }
    }

    protected void lvCourseDefault_Trainer_Item_OnItemDataBound(object sender, RadListViewItemEventArgs e)
    {
        if (e.Item is RadListViewItem)
        {
            //RadListView lvItem = e.Item.FindControl("lvCourseDefault_Trainer_Item") as RadListView;
            RadListViewDataItem dataItem = e.Item as RadListViewDataItem;
            var linkTitle = dataItem.FindControl("btnScoTitleTR") as LinkButton;
            //var dripEnable = lvItem.DataKeyValues[dataItem.DataItemIndex]["dripEnable"].ToString();
            int _userCIM = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
            var dripEnable = (dataItem.FindControl("hfdripEnable") as HiddenField).Value.ToString();

            if (dripEnable == "1" || (DataHelper.isOverDue(CourseID, global_TimezoneOffset) || !DataHelper.isEnrolled(_userCIM, CourseID) || !DataHelper.hasCourseStarted(CourseID)) && DataHelper.isCourseOpen(CourseID) == false && DataHelper.isEnrollmentAllowed(CourseID) == false)
            {
                linkTitle.OnClientClick = "";
                linkTitle.Enabled = false;
            }

            else
            {
                linkTitle.Enabled = true;
            }
        }
    }

    //Adding Sections Additional Resources

    protected void lvCourseDefault_Additional_SectionDataBound(object sender, RadListViewItemEventArgs e)
    {
        var dataItem = e.Item as RadListViewDataItem;
        if (dataItem != null)
        {
            RadListView lvItem = dataItem.FindControl("lvCourseDefault_Additional_Item") as RadListView;
            int sectionID = Convert.ToInt32(lvCourseDefault_Additional.DataKeyValues[dataItem.DataItemIndex]["SectionID"].ToString());
            GetDBPackages(5, lvItem, sectionID);

        }
    }

    protected void lvCourseDefault_Additional_Item_OnItemDataBound(object sender, RadListViewItemEventArgs e)
    {
        if (e.Item is RadListViewItem)
        {
            //RadListView lvItem = e.Item.FindControl("lvCourseDefault_Additional_Item") as RadListView;
            RadListViewDataItem dataItem = e.Item as RadListViewDataItem;
            var linkTitle = dataItem.FindControl("btnScoTitleAR") as LinkButton;
            var dripEnable = (e.Item.FindControl("hfdripEnable") as HiddenField).Value.ToString();
            int _userCIM = Convert.ToInt32(DataHelper.GetCurrentUserCIM());

            //if (dripEnable == "1" || (DataHelper.isOverDue(CourseID) || !DataHelper.isEnrolled(_userCIM, CourseID) || !DataHelper.hasCourseStarted(CourseID)) && DataHelper.isCourseOpen(CourseID) == false && DataHelper.isEnrollmentAllowed(CourseID) == false)
            if ((DataHelper.isOverDue(CourseID, global_TimezoneOffset) && !DataHelper.isEnrolled(_userCIM, CourseID)) || !DataHelper.isEnrolled(_userCIM, CourseID) || !DataHelper.isLoggedIn() || dripEnable == "1")
            {
                linkTitle.OnClientClick = "";
                linkTitle.Enabled = false;
            }

            else
            {
                linkTitle.Enabled = true;
            }
        }
    }

    protected void lvCourseDefault_Additional_NeedDataSource(object sender, EventArgs e)
    {
        try
        {
            //foreach (var currid in CURRCOURSEID)
            //{
            var dtResources = DataHelper.GetCourseSections(CourseID, 5);
            lvCourseDefault_Additional.DataSource = dtResources;
            //}


        }
        catch (Exception ex)
        {
            throw;
        }

    }
}

