﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DefaultLearnerUserCtrl.ascx.cs" Inherits="DefaultLearnerUserCtrl" %>

<div class="col-sm-11 col-md-12">
				<div class="row">
			<div class="col-md-12">
				<div class="row">
				<div class="col-md-12 thirty-px-padding">
					<div class="col-md-12 no-paddings">
						<ul class="breadcrumb">
						  <li><a href="#"><asp:Label runat="server" ID="lblAdminDashboard" Text="<%$ Resources:LocalizedResource, AdminDashboard %>"></asp:Label></a></li>
						  <li><asp:Label runat="server" ID="lblOverview" Text="<%$ Resources:LocalizedResource, Overview %>"></asp:Label></li>
						</ul>

                        <div class="header-cover-photo">
							<div class="header-cover-photo-profile-pic-edit-icon"><a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></div>
							<div class="header-cover-photo-profile-pic">
								<img src="https://images-na.ssl-images-amazon.com/images/I/51McXHtxlWL.jpg" class="img-circle border-circle-violet" width="150"/>
								
							</div>
							<div class="header-cover-photo-profile-pic-details">
								<div class="row">
									<div class="col-md-12"><h2 class="color-white">CHARLIE PERENA</h2></div>
									<div class="col-md-12"><h3 class="color-white">IT Dev</h3></div>
									<div class="col-md-12"><h4 class="color-white">Manila, Ortigas</h4></div>
								</div>
							</div>
						</div>
					</div>

                    <div class="col-md-12 ten-px-paddings">
                        <div class="row">
                            <div class="col-md-2 col-md-12">
                                <div class="bg-color-light-teal color-white font-bold five-px-padding">
                                    <asp:Label runat="server" ID="lblTranscomProfile" Text="<%$ Resources:LocalizedResource, TranscomProfile %>"></asp:Label>:
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-md-12">
                                <div class="border-top-light-teal">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <asp:Label runat="server" ID="lblFullName" Text="<%$ Resources:LocalizedResource, FullName %>"></asp:Label>:
                                        </div>

                                        <div class="col-md-6 pull-left">
                                            Charlie Perena
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <asp:Label runat="server" ID="lblRegion" Text="<%$ Resources:LocalizedResource, Region %>"></asp:Label>:
                                        </div>

                                        <div class="col-md-6 pull-left">
                                            <asp:Label runat="server" ID="Label1" Text="<%$ Resources:LocalizedResource, Asia %>"></asp:Label>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

					<div class="col-md-12 no-paddings">
						<div class="row">
							<div class="col-md-2">
								<div class="big-menu-dashboard">
									<div class="icon-holder">
										<div class="col-md-12 icon-glyph text-center">
												<span class="fa fa-search"></span>
										</div>
									</div>
									<p class="current">
                                        <asp:Label runat="server" ID="lblOverviewCurrent" Text="<%$ Resources:LocalizedResource, Overview %>"></asp:Label>:
									</p>
								</div>
							</div>
							<div class="col-md-2">
								<div class="big-menu-dashboard">
									<div class="icon-holder">
											<div class="col-xs-6 counter text-center">
												1.5K
											</div>
											<div class="col-xs-6 icon-glyph text-center">
												<span class="fa fa-users"></span>
											</div>
									</div>
									<p>
                                        <asp:Label runat="server" ID="lblAllClasses" Text="<%$ Resources:LocalizedResource, AllClasses %>"></asp:Label>:
									</p>
								</div>
							</div>
							<div class="col-md-2">
								<div class="big-menu-dashboard">
									<div class="icon-holder">
											<div class="col-xs-6 counter text-center">
												300
											</div>
											<div class="col-xs-6 icon-glyph text-center">
												<span class="fa  fa-graduation-cap"></span>
											</div>
									</div>
									<p>
                                        <asp:Label runat="server" ID="lblAllCourses" Text="<%$ Resources:LocalizedResource, AllCourses %>"></asp:Label>:
									</p>
								</div>
							</div>
							<div class="col-md-2">
								<div class="big-menu-dashboard">
									<div class="icon-holder">
											<div class="col-xs-6 counter text-center">
												1.5K
											</div>
											<div class="col-xs-6 icon-glyph text-center">
												<span class="fa fa-users"></span>
											</div>
									</div>
									<p>
                                        <asp:Label runat="server" ID="lblUsers" Text="<%$ Resources:LocalizedResource, Users %>"></asp:Label>
									</p>
								</div>
							</div>
							<div class="col-md-2">
								<div class="big-menu-dashboard">
									<div class="icon-holder">
											<div class="col-xs-6 counter text-center">
												4.8K
											</div>
											<div class="col-xs-6 icon-glyph text-center">
												<span class="fa fa-laptop"></span>
											</div>
									</div>
									<p>
                                        <asp:Label runat="server" ID="lblEnrollments" Text="<%$ Resources:LocalizedResource, Enrollments %>"></asp:Label>
									</p>
								</div>
							</div>
							<div class="col-md-2">
								<div class="big-menu-dashboard">
									<div class="icon-holder">
										<div class="col-xs-4 counter text-center">
												1K
											</div>
											<div class="col-xs-8 icon-glyph text-center">
												<p><asp:Label runat="server" ID="lblActive" Text="<%$ Resources:LocalizedResource, Active %>"></asp:Label></p>
												<p><asp:Label runat="server" ID="lblVisitors" Text="<%$ Resources:LocalizedResource, Visitors %>"></asp:Label></p>
												<p><asp:Label runat="server" ID="lblOnSite" Text="<%$ Resources:LocalizedResource, onsite %>"></asp:Label></p>
											</div>
									</div>
									<p>
                                        <asp:Label runat="server" ID="lblSiteTraffic" Text="<%$ Resources:LocalizedResource, SiteTraffic %>"></asp:Label>
									</p>
								</div>
							</div>
						</div>
					</div>


					<div class="col-md-12 gray-border-no-pads ten-px-padding">
						<div class="row">
							<div class="col-md-6">
							  
								<form class="form-horizontal" role="form">
									<div class="row">
										<span class="col-md-6 control-label color-0D9E9E font-bold font-uppercase"><asp:Label runat="server" ID="lblShowingOverviewInTheLast" Text="<%$ Resources:LocalizedResource, ShowingOverviewInTheLast %>"></asp:Label></span>
										<div class="col-md-6">
											<div class="form-group row">
												<div class="col-md-4">
													<input type="text" class="form-control" id="inputKey" placeholder="30" value="30">
												</div>
												<label for="inputValue" class="col-md-1 control-label color-0D9E9E font-bold font-uppercase"><asp:Label runat="server" ID="lblDays" Text="<%$ Resources:LocalizedResource, Days %>"></asp:Label></label>
											</div>
										</div>
									</div>
								</form>
							</div>
							<div class="col-md-6">
								<form class="form-horizontal" role="form">
									<div class="form-group">
										<div class="col-md-12">
											<div class="form-group row">
												<label for="inputKey" class="col-md-3 control-label font-bold"><asp:Label runat="server" ID="lblStartDate" Text="<%$ Resources:LocalizedResource, StartDate %>"></asp:Label></label>
												<div class="col-md-3">
													<input type="date" class="form-control" id="inputKey">
												</div>
												<label for="inputValue" class="col-md-3 control-label font-bold"><asp:Label runat="server" ID="Label2" Text="<%$ Resources:LocalizedResource, EndDate %>"></asp:Label></label>
												<div class="col-md-3">
													<input type="date" class="form-control" id="inputValue">
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>

						<div class="row">

								<div class="col-md-3">
									<div class="content-center">
										<div class="c100 p33">
										  <span>33%</span>
										  <div class="slice">
											<div class="bar"></div>
											<div class="fill"></div>

										  </div>
										</div>
									</div>
									
									<div class="col-xs-12 text-center">
										<h2 class="color-created font-bold">23</h2>
										<p class="color-created font-bold font-uppercase"><asp:Label runat="server" ID="lblCreatedClasses" Text="<%$ Resources:LocalizedResource, CreatedClasses %>"></asp:Label></p>
									</div>

									<div class="col-xs-12 text-center">
										<h2 class="color-completed font-bold">16</h2>
										<p class="color-completed font-bold font-uppercase"><asp:Label runat="server" ID="lblCompletedClasses" Text="<%$ Resources:LocalizedResource, CompletedClasses %>"></asp:Label></p>
									</div>
						   
									<div class="col-xs-12 text-center">
										<h2 class="color-in-progress font-bold">45</h2>
										<p class="color-in-progress font-bold font-uppercase"><asp:Label runat="server" ID="lblClassesInProgress" Text="<%$ Resources:LocalizedResource, ClassesInprogress %>"></asp:Label></p>
									</div>

									<div class="col-xs-12 text-center">
										<h2 class="color-not-started font-bold">9</h2>
										<p class="color-not-started font-bold font-uppercase"><asp:Label runat="server" ID="lblNotStartedCourses" Text="<%$ Resources:LocalizedResource, NotStartedCourses %>"></asp:Label></p>
									</div>

									<div class="col-xs-12 text-center">
										<h2 class="color-overdue font-bold">171</h2>
										<p class="color-overdue font-bold font-uppercase"><asp:Label runat="server" ID="lblOverdueCourses" Text="<%$ Resources:LocalizedResource, OverdueCourses %>"></asp:Label></p>
									</div>

								</div>
								<div class="col-md-3 content-center">
									<div class="c100 p74">
									  <span>74%</span>
									  <div class="slice">
										<div class="bar"></div>
										<div class="fill"></div>

									  </div>

									</div>
								</div>
								<div class="col-md-3 content-center">
									<div class="c100 p85">
									  <span>85%</span>
									  <div class="slice">
										<div class="bar"></div>
										<div class="fill"></div>

									  </div>

									</div>
								</div>
								<div class="col-md-3 content-center">
									<div class="c100 p67">
									  <span>67%</span>
									  <div class="slice">
										<div class="bar"></div>
										<div class="fill"></div>

									  </div>

									</div>
								</div>
						</div>
					</div>


					<div class="col-md-12 gray-border-no-pads">
						<div class="row widgets black-header">
							<div class="col-md-6 font-bold"><asp:Label runat="server" ID="lblClassStatistics" Text="<%$ Resources:LocalizedResource, ClassStatisticsWest %>"></asp:Label></div>
							<div class="col-md-6 font-bold">
								<div class="row-fluid">
										   
										<div class="col-xs-4 pull-right">
											<div class="progress" style="width: 100%">
												<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
													40% 
												</div>
											</div>
										</div>
										<div class="col-xs-8 text-right">
                                             <asp:Label runat="server" ID="lblOverallCompletion" Text="<%$ Resources:LocalizedResource, OverallCompletion %>"></asp:Label>
										</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-3 font-uppercase class-menu-active font-bold color-white">
                                <asp:Label runat="server" ID="lblLeaderboard" Text="<%$ Resources:LocalizedResource, Leaderboard %>"></asp:Label>
							</div>
							<div class="col-md-3 font-uppercase class-menu font-bold color-black">
                                <asp:Label runat="server" ID="lblClassLearning" Text="<%$ Resources:LocalizedResource, ClassLearningPlan %>"></asp:Label>
							</div>
							<div class="col-md-3 font-uppercase class-menu font-bold color-black">
                                <asp:Label runat="server" ID="lblClassTranscripts" Text="<%$ Resources:LocalizedResource, ClassTranscripts %>"></asp:Label>
							</div>
							<div class="col-md-3 font-uppercase class-menu font-bold color-black">
                                <asp:Label runat="server" ID="lblQuiz" Text="<%$ Resources:LocalizedResource, QuizStatistics %>"></asp:Label>
							</div>
						</div>
						<div class="row ten-px-padding">
							<div class="col-md-10">
							  
								<form class="form-horizontal" role="form">
									<div class="form-group">
										<label class="col-md-2 control-label2">
											<span class="pull-right color-0D9E9E font-bold font-uppercase"><asp:Label runat="server" ID="lblShowingTop" Text="<%$ Resources:LocalizedResource, ShowingTop %>"></asp:Label></span>
										</label>
										<div class="col-md-10">
											<div class="form-group row">
												
												<div class="col-xs-2">
													<select class="form-control input-sm" id="inputKey" placeholder="Key">
														<option value="30">30</option>
													</select>
												</div>
												<label for="inputValue" class="col-md-1 pull-left control-label2 color-0D9E9E font-bold font-uppercase"><asp:Label runat="server" ID="Label4" Text="<%$ Resources:LocalizedResource, Of %>"></asp:Label></label>
												<div class="col-xs-2">
													<input type="text" class="form-control input-sm" id="inputValue" placeholder="200">
												</div>
												<label for="inputKey" class="col-md-4 control-label2 color-0D9E9E font-bold font-uppercase"><asp:Label runat="server" ID="lblParticipantsInClass" Text="<%$ Resources:LocalizedResource, ParticipantsInTheClass %>"></asp:Label></label>
											</div>
										</div>
									</div>
								</form>
							</div>
							<div class="col-md-2"></div>
							<div class="col-md-12 grey-background ten-px-padding">
								<div class="row">
									<div class="col-md-4">
										<div class="two-color-header">
											<div class="col-xs-6 bg-color-created text-size-16px ten-px-padding color-black font-bold text-center"><asp:Label runat="server" ID="Label3" Text="<%$ Resources:LocalizedResource, ClassAverage %>"></asp:Label></div>
											<div class="col-xs-6 bg-color-gray ten-px-padding font-bold color-white text-center"><span class="text-size-16px">89.58%</span></div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12">
										<ol class="ProgressBar">
										 <li class="ProgressBar-step is-complete">
											<span class="ProgressBar-icon2">
											   <i class="icon-holder fa fa-chevron-left"></i>
											</span>
										   
										  </li>
										  <li class="ProgressBar-step is-complete">
											<svg class="ProgressBar-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#checkmark-bold"></use></svg>
											<span class="ProgressBar-stepLabel">Week 1</span>
										  </li>
										  <li class="ProgressBar-step is-current">
											<svg class="ProgressBar-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#checkmark-bold"></use></svg>
											<span class="ProgressBar-stepLabel">Week 2</span>
										  </li>
										  <li class="ProgressBar-step">
											<svg class="ProgressBar-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#checkmark-bold"></use></svg>
											<span class="ProgressBar-stepLabel">Week 3</span>
										  </li>
										  <li class="ProgressBar-step">
											<svg class="ProgressBar-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#checkmark-bold"></use></svg>
											<span class="ProgressBar-stepLabel">Week 4</span>
										  </li>
										  <li class="ProgressBar-step">
											<svg class="ProgressBar-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#checkmark-bold"></use></svg>
											<span class="ProgressBar-stepLabel">Week 5</span>
										  </li>
										  <li class="ProgressBar-step">
											<svg class="ProgressBar-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#checkmark-bold"></use></svg>
											<span class="ProgressBar-stepLabel">Week 6</span>
										  </li>
										  <li class="ProgressBar-step">
											<svg class="ProgressBar-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#checkmark-bold"></use></svg>
											<span class="ProgressBar-stepLabel">Week 7</span>
										  </li>
										  <li class="ProgressBar-step is-complete">
											<span class="ProgressBar-icon2">
											   <i class="icon-holder fa fa-chevron-right"></i>
											</span>
										   
										  </li>
										</ol>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<table class="table table-borderless">
											<tbody><tr>
												<td class="color-0D9E9E text-center font-bold">1</td>
												<td class="color-0D9E9E text-center font-bold">97939</td>
												<td class="img-responsive">
													<img src="http://azoom-sites.rockthemes.net/abboxed/wp-content/uploads/sites/14/2015/05/abboxed-restaurant-portfolio2.jpg" alt="First Slide" width="100">
												</td>
												<td>
													<div class="progress bg-color-0D9E9E bg-color-gray">
														<div class="progress-bar bg-color-0D9E9E" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
															40% 
														</div>
													</div>
												</td>
											</tr>
											<tr>
												<td class="color-0D9E9E text-center font-bold">2</td>
												<td class="color-0D9E9E text-center font-bold">3213131</td>
												<td class="img-responsive">
													<img src="http://azoom-sites.rockthemes.net/abboxed/wp-content/uploads/sites/14/2015/05/abboxed-restaurant-portfolio2.jpg" alt="First Slide" width="100">
												</td>
												<td>
													<div class="progress bg-color-gray">
														<div class="progress-bar bg-color-0D9E9E" role="progressbar" aria-valuenow="89" aria-valuemin="0" aria-valuemax="100" style="width: 89%">
															89% 
														</div>
													</div>
												</td>
											</tr>
										</tbody></table>
									</div>

									<div class="col-md-6">
										<table class="table table-border-gray">
											<thead>
												<tr>
													<th class="text-center"><asp:Label runat="server" ID="lblClass" Text="<%$ Resources:LocalizedResource, Class %>"></asp:Label> <br> <asp:Label runat="server" ID="lblRanking" Text="Ranking"></asp:Label></th>
													<th valign="middle" class="text-center"><asp:Label runat="server" ID="lblBadgesEarned" Text="Badges earned"></asp:Label></th>
												</tr>
											</thead>
											<tbody><tr>
												<td class="color-0D9E9E text-center font-bold">1</td>
												<td class="color-0D9E9E text-center font-bold">97939</td>
												
											</tr>
											<tr>
												<td class="color-0D9E9E text-center font-bold">2</td>
												<td class="color-0D9E9E text-center font-bold">3213131</td>
												
											</tr>
										</tbody></table>
									</div>

								</div>
							</div>
						</div>

                       

					</div>
					
				</div>
				</div>    
			</div>
		 </div>
	
			</div>