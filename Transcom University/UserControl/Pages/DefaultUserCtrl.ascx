﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DefaultUserCtrl.ascx.cs"Inherits="DefaultUserCtrl" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<div class="col-sm-11 col-md-12 margin-bottom-10px">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12 thirty-px-padding">
                    <div class="col-md-12 no-paddings">
                        <ul class="breadcrumb">
                            <li><a href="#"><asp:Label runat="server" ID="lblAdminDashboard" Text="<%$ Resources:LocalizedResource, AdminDashboard %>"></asp:Label></a></li>
                            <li><a href="#"><asp:Label runat="server" ID="lblHomepageLayout" Text="<%$ Resources:LocalizedResource, HomepageLayout %>"></asp:Label></a></li>
                            <li><a href="#"><asp:Label runat="server" ID="lblImageSliderk" Text="<%$ Resources:LocalizedResource, ImageSlider %>"></asp:Label></a></li>
                            <li><asp:Label runat="server" ID="Label2" Text="<%$ Resources:LocalizedResource, SelectPlaylist %>"></asp:Label></li>
                        </ul>
                    </div>
                    <div class="col-md-12 gray-border-no-pads ten-px-padding bg-color-light-gray">
                        <div class="row">
                            <div class="col-md-12 col-md-8">
                                <div class="col-md-12">
                                    <h5 class="font-bold">
                                       <asp:Label runat="server" ID="Label1" Text="<%$ Resources:LocalizedResource, CurrentPlaylist %>"></asp:Label>:</h5>
                                    <div class="row">
                                        <asp:Repeater ID="rptActivePlaylist" runat="server">
                                            <ItemTemplate>
                                                <div class="col-xs-9">
                                                    <%#Eval("PlaylistName") %>
                                                </div>
                                                <div class="col-xs-3">
                                                    <span class="fa fa-eye" style="font-size: 20px;"></span>&nbsp;&nbsp;<span class="fa fa-pencil-square-o"
                                                        style="font-size: 20px;"></span>&nbsp;&nbsp;<span class="fa fa-times" style="font-size: 20px;
                                                            color: red; font-weight: bold;"></span>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <h5 class="font-bold">
                                        <asp:Label runat="server" ID="Label3" Text="<%$ Resources:LocalizedResource, OtherPlaylists %>"></asp:Label>:</h5>

                                    <asp:Repeater ID="rptInactivePlaylist" runat="server">
                                        <ItemTemplate>
                                             <div class="row">
                                                <div class="col-xs-3">
                                                    <asp:HyperLink ID="PlaylistID" NavigateUrl="" runat="server" CssClass="btn btn-sm btn-teal font-bold"><asp:Label runat="server" ID="Label3" Text="<%$ Resources:LocalizedResource, LoadPlaylist %>"></asp:Label></asp:HyperLink>
                                                </div>
                                                <div class="col-xs-6">
                                                     <%#Eval("PlaylistName") %>
                                                </div>
                                                <div class="col-xs-3">
                                                    <span class="fa fa-eye" style="font-size: 20px;"></span>
                                                    <span class="fa fa-pencil-square-o" style="font-size: 20px;"></span>
                                                    <span class="fa fa-times" style="font-size: 20px; color: red; font-weight: bold;"></span>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
