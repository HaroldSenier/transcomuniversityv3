﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;


public partial class DefaultUserCtrl : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // DataSet dsUserInfo = DataHelper.GetAllPlaylists();

        LoadActivePlaylists();
        LoadInactivePlaylists();
    }

    void LoadInactivePlaylists()
    {
        rptInactivePlaylist.DataSource = DataHelper.GetAllPlaylists(true);
        rptInactivePlaylist.DataBind();
    }

    void LoadActivePlaylists()
    {
        rptActivePlaylist.DataSource = DataHelper.GetAllPlaylists(false);
        rptActivePlaylist.DataBind();
    }
}