﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Collections;

public partial class FeaturedCoursesUserCtrl : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            loadDdl();
            loadPreviousSettings();
        }

    }

    void loadPreviousSettings()
    {
        var settings = DataHelper.getFeaturedCourseSettings();

        bool isGrid;
        int count;
        bool isManual;
        bool isVisible;
        int items;
        bool mostViews;
        int topMostViews;
        bool mostRatings;
        int topMostRatings;
        bool mostRatingWithin;
        int ratingWithin;
        bool isCompleted;
        bool mostEnrollee;
        int mostEnrolleeCount;
        bool addedLast;
        int lastAddInMilisecond;
        int lastAddUnit;
        bool modifiedLast;
        int lastModifiedInMilisecond;
        int lastModifiedUnit;
        int itemPerPage;

        try
        {
            isGrid = settings.DefaultView;
            count = settings.DisplayCount == 0 ? 1 : settings.DisplayCount;
            isManual = settings.ManualSelection;
            isVisible = settings.Visible;
            items = settings.ItemsPerPage == 0 ? 1 : settings.ItemsPerPage;
            mostViews = settings.EnableMostView;
            topMostViews = settings.MostViewCount == 0 ? 1 : settings.MostViewCount;
            mostRatings = settings.EnableMostRating;
            topMostRatings = settings.MostRatingCount == 0 ? 1 : settings.MostRatingCount;
            mostRatingWithin = settings.EnableRatingWithin;
            ratingWithin = settings.RatingWithin;
            isCompleted = settings.Completed;
            mostEnrollee = settings.EnableMostEnrollee;
            mostEnrolleeCount = settings.MostEnrolleeCount == 0 ? 1 : settings.MostEnrolleeCount;
            addedLast = settings.EnableAddedLast;
            lastAddInMilisecond = settings.LastAddInMillisecond == 0 ? 1 : settings.LastAddInMillisecond;
            lastAddUnit = settings.LastAddUnit;
            modifiedLast = settings.EnableLastModified;
            lastModifiedInMilisecond = settings.LastModifiedInMillisecond == 0 ? 1 : settings.LastModifiedInMillisecond;
            lastModifiedUnit = settings.LastModifiedUnit;
            itemPerPage = settings.ItemsPerPage == 0 ? 1 : settings.ItemsPerPage;
        }
        catch (Exception )
        {
            isGrid = false;
            count = 1;
            isManual = false;
            isVisible = false;
            items = 1;
            mostViews = false;
            topMostViews = 1;
            mostRatings = false;
            topMostRatings = 1;
            mostRatingWithin = false;
            ratingWithin = 1;
            isCompleted = false;
            mostEnrollee = false;
            mostEnrolleeCount = 1;
            addedLast = false;
            lastAddInMilisecond = 1;
            lastAddUnit = 1;
            modifiedLast = false;
            lastModifiedInMilisecond = 1;
            lastModifiedUnit = 1;
            itemPerPage = 1;
          
        }
        



        cbGridView.Checked = isGrid == true ? true : false;
        cbListView.Checked = isGrid == false ? true : false;
        ddlDisplayCount.SelectedIndex = ddlDisplayCount.FindItemByValue(count.ToString()).Index;
        cbManualSelection.Checked = isManual;
        cbVisible.Checked = isVisible;

        cbMostViews.Checked = mostViews == true ? true : false;
        ddlTopMostViews.SelectedIndex = ddlTopMostViews.FindItemByValue(topMostViews.ToString()).Index;

        cbMostRatings.Checked = mostRatings == true ? true : false;
        ddlTopMostRating.SelectedIndex = ddlTopMostRating.FindItemByValue(topMostRatings.ToString()).Index;

        cbRating.Checked = mostRatingWithin == true ? true : false;
        ddlRating.SelectedIndex = ratingWithin;

        cbCompletedCourse.Checked = isCompleted == true ? true : false;

        cbMostEnrollee.Checked = mostEnrollee == true ? true : false;
        ddlTopMostEnrollee.SelectedIndex = ddlTopMostEnrollee.FindItemByValue(mostEnrolleeCount.ToString()).Index;

        cbLatestAdded.Checked = addedLast == true ? true : false;
        ddlLatestAddedNumber.SelectedIndex = ddlLatestAddedNumber.FindItemByValue(lastAddInMilisecond.ToString()).Index;
        ddlLatestAddedUnit.SelectedIndex = lastAddUnit;

        cbLatestModified.Checked = modifiedLast == true ? true : false;
        ddlLatestModifiedNumber.SelectedIndex = ddlLatestAddedNumber.FindItemByValue(lastAddInMilisecond.ToString()).Index;
        ddlLatestModifiedUnit.SelectedIndex = lastModifiedUnit;

    }

    void loadDdl()
    {
        for (int i = 1; i <= 20; i++)
        {
            DropDownListItem itemView = new DropDownListItem(i.ToString(), i.ToString());
            DropDownListItem itemRating = new DropDownListItem(i.ToString(), i.ToString());
            DropDownListItem itemEnrollee = new DropDownListItem(i.ToString(), i.ToString());
            DropDownListItem itemDisplay = new DropDownListItem(i.ToString(), i.ToString());
            DropDownListItem itemLibrary = new DropDownListItem(i.ToString(), i.ToString());
            DropDownListItem itemPager = new DropDownListItem(i.ToString(), i.ToString());
            ddlDisplayCount.Items.Add(itemDisplay);
            ddlTopMostViews.Items.Add(itemView);
            ddlTopMostRating.Items.Add(itemRating);
            ddlTopMostEnrollee.Items.Add(itemEnrollee);
            ddlCourseLibrary.Items.Add(itemLibrary);

            ddlCourseCatalogPager.Items.Add(itemPager);
        }

        for (int i = 1; i <= 60; i++)
        {
            DropDownListItem itemLastAddedNumber = new DropDownListItem(i.ToString(), i.ToString());
            DropDownListItem itemLastModifiedNumber = new DropDownListItem(i.ToString(), i.ToString());
            DropDownListItem itemRating2 = new DropDownListItem(i.ToString(), i.ToString());
            DropDownListItem itemEnrollee = new DropDownListItem(i.ToString(), i.ToString());
            DropDownListItem itemDisplay = new DropDownListItem(i.ToString(), i.ToString());
            ddlLatestAddedNumber.Items.Add(itemLastModifiedNumber);
            ddlLatestModifiedNumber.Items.Add(itemLastAddedNumber);
            
        }

        ArrayList itemsList = new ArrayList();
        itemsList.Add("Seconds");
        itemsList.Add("Minutes");
        itemsList.Add("Hours");
        itemsList.Add("Days");
        itemsList.Add("Weeks");
        itemsList.Add("Months");
        itemsList.Add("Years");
        ddlLatestAddedUnit.DataSource = itemsList;
        ddlLatestAddedUnit.DataBind();
        ddlLatestModifiedUnit.DataSource = itemsList;
        ddlLatestModifiedUnit.DataBind();

        ArrayList itemsList2 = new ArrayList();
        itemsList2.Add("5");
        itemsList2.Add("5 and 4");
        itemsList2.Add("4");
        itemsList2.Add("4 and 3");
        itemsList2.Add("3");
        itemsList2.Add("3 and 2");
        itemsList2.Add("2");
        itemsList2.Add("2 and 1");
        itemsList2.Add("1");
        ddlRating.DataSource = itemsList2;
        ddlRating.DataBind();
    }
}