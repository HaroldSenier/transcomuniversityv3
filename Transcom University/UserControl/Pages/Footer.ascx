﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Footer.ascx.cs" Inherits="UserControl_Pages_Footer" %>
<div class="bg-gray">
    <%--<div class="site-map">
        <div class="footer-column col-md-2">
            <h5>
                THE COMPANY
            </h5>
            <ul class="list-unstyled">
                <li><a href="#">About Us</a></li>
                <li><a href="#">Contact us</a></li>
                <li><a href="#">Policy</a></li>
            </ul>
        </div>
        <div class="footer-column col-md-2">
            <h5>
                SUPPORT
            </h5>
            <ul class="list-unstyled">
                <li><a href="#">FAQ</a></li>
                <li><a href="#">TU v3 User Guide</a></li>
            </ul>
        </div>
        <div class="footer-column col-md-4">
            <h5>
                THE SITE
            </h5>
            <div class="col-md-6">
                <ul class="list-unstyled">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Featured Courses</a></li>
                    <li><a href="#">Latest News</a></li>
                    <li><a href="#">Course Catalog</a></li>
                </ul>
            </div>
            <div class="col-md-6">
                <ul class="list-unstyled">
                    <li><a href="#">Testimonials</a></li>
                    <li><a href="#">Search</a></li>
                    <li><a href="#">Latest News</a></li>
                    <li><a href="#">Site Map</a></li>
                </ul>
            </div>
        </div>
        <div class="footer-column col-md-4 no-border">
            <h5>
                INTEGRATED WITH
            </h5>
            <ul class="list-unstyled">
                <li><a href="#">The Coach v2</a></li>
                <li><a href="#">Transcom Assessment Suite (TAS)</a></li>
            </ul>
        </div>
        <div class="line-gray">
        </div>
    </div>--%>
    <ul class="unstyled nav nav-footer display-inline-flex font-small" style="height: 35px;">
        <%--<li><a href="#"><asp:Label runat="server" ID="lblVIsibleonHomepage" Text="<%$ Resources:LocalizedResource, Policy %>"></asp:Label></a></li>--%>
        <li><a href="#" data-toggle="modal" data-target="#AboutUs"><asp:Label runat="server" ID="Label1" Text="<%$ Resources:LocalizedResource, AboutUs %>"></asp:Label></a></li>
        <li><a href="#" data-toggle="modal" data-target="#ContactUs"><asp:Label runat="server" ID="Label2" Text="<%$ Resources:LocalizedResource, ContactUs %>"></asp:Label></a></li>
        <li><a href="#"><asp:Label runat="server" ID="Label3" Text="<%$ Resources:LocalizedResource, Faqs %>"></asp:Label></a></li>
        <%--<li><a href="#"><asp:Label runat="server" ID="Label4" Text="<%$ Resources:LocalizedResource, SiteMap %>"></asp:Label></a></li>--%>
    </ul>
    <div class="footer font-small">
        <h6 class="pull-left" style="font-size: 14px;">
            <asp:Label runat="server" ID="Label5" Text="<%$ Resources:LocalizedResource, Copyright2018AllRightsReservedTranscomPhilippines %>"></asp:Label>
        </h6>
        <h5 class="pull-right tc-text-banner">
            <asp:Label runat="server" ID="Label6" Text="<%$ Resources:LocalizedResource, Transcom %>"></asp:Label>
        </h5>
    </div>
</div>

<div class="modal fade" id="AboutUs" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered"`style="margin: 30px auto!important;">
        <div class="modal-content" style="border-radius: 0px;border: 1px solid #000;width: 700px;max-width: 700px;">
            <div class="modal-header" style="border-bottom: 0px;padding: 10px;">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: #000;">&times;</button>
            </div>
            <div class="modal-body">
            <div class="row" style="margin: 25px 25px 25px 25px;">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <label style="color: #000;">T: About Transcom University</label>
                    <p style="color: #000;font-weight: normal;">To stay ahead in today's fast-paced business environment, you need to have the right skill-set. More than a portal for compliance training, <label style="color:#EC3E33;">T:University</label> provides you access to learning experiences that help you to be successful in your current role and advance your career.
                    From Call Handling Skills to Performance Management, <label style="color:#EC3E33;">T:University</label> strives to contact all employees to relevant, on-demand and just-in-time content.</p>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ContactUs" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered"`style="margin: 30px auto!important;">
        <div class="modal-content" style="border-radius: 0px;border: 1px solid #000;width: 700px;max-width: 700px;">
            <div class="modal-header" style="border-bottom: 0px;padding: 10px;">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: #000;">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row" style="margin: 25px 25px 25px 25px;color: #000;font-weight:normal;">
                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8" style="border-right: 1px solid #a9a9a9;">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <label>Contact Us</label>
                            <p>We would love to hear from you. Please send us your questions, comments or concern by
                            filling out the form below</p>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label class="font-small" style="font-weight:normal;">Subject</label>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <asp:TextBox runat="server" ID="ftrtxtSendMailSubject" CssClass="form-control"></asp:TextBox>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label class="font-small" style="font-weight:normal;">Your Message</label>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <asp:TextBox runat="server" TextMode="MultiLine" Rows="10" ID="ftrtxtSendMailMessage" CssClass="form-control" style="resize:none;"></asp:TextBox>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12 text-left" style="margin-top: 20px;">
                            <asp:LinkButton runat="server" ID="ftrlnkSendMail" CssClass="btn btn-md btn-flat btn-teal white hover-teal" OnClick="SendMail_Click" OnClientClick="return SomeMethod();">Send Mail</asp:LinkButton>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        </br></br></br></br>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="input-group">
                              <span class="input-group-addon" style="background: transparent;border: none;padding: 0px 0px 5px 0px;">
                                <span class="glyphicon glyphicon-envelope" aria-hidden="true" style="font-size: 20px;color: #ff5046;"></span>
                              </span>
                              <a href="#" class="font-small" style="padding-left:4px;color:#0000a0;">university@transcom.com</a>
                            </div><!-- /input-group -->
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="input-group">
                              <span class="input-group-addon" style="background: transparent;border: none;padding: 0px 0px 20px 0px;">
                                <span class="glyphicon glyphicon-comment" aria-hidden="true" style="font-size: 20px;color: #ff5046;"></span>
                              </span>
                              <a href="#" class="font-small" style="padding-left:4px;color:#0000a0;">Transcom University</a>
                            </div><!-- /input-group -->
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <p style="padding-bottom:5px;">Business Hours:</p>
                            <p style="padding-bottom:5px;">Monday - Friday</p>
                            <p style="padding-bottom:5px;">6:00 AM to 12:00 AM CET</p>
                            <p style="padding-bottom:5px;">12:00 AM to 6:00 PM EST</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function SomeMethod() {
        var Subject = document.getElementById('<%= ftrtxtSendMailSubject.ClientID%>').value;
        var Message = document.getElementById('<%= ftrtxtSendMailMessage.ClientID%>').value;

        if (Subject == null || Subject == '') {
            radalert("Please fill in your Subject.", 330, 180, "Error Message", "");
            return false;
        }
        else if (Message == null || Message == '') {
            radalert("Please fill in your Message.", 330, 180, "Error Message", "");
            return false;
        }

        return true;
    }

    $('.close').click(function (e) {
        document.getElementById('<%= ftrtxtSendMailSubject.ClientID%>').value = "";
        document.getElementById('<%= ftrtxtSendMailMessage.ClientID%>').value = "";
    });
</script>
