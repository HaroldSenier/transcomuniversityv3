﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ForumThreadUserCtrl.ascx.cs"
    Inherits="UserControl_Pages_ForumThreadUserCtrl" %>
<rad:RadCodeBlock ID="rcbCourseCatalog" runat="server">
    <script type="text/javascript">

        var threadPostList;
        var threadID;
        var courseID;
        var virtualCount;
        var pageSize;
        var pageIndex;

        var panelReplyHtml;
        var panelReportHtml;

        function pageLoad() {
            debugger;
                var tab = 'AdminForum';
                highlightSelectedSubmenu();
                threadID = '<%= Request.QueryString["ThreadID"] %>';
                courseID = '<%= Request.QueryString["CourseID"] %>';
            
                panelReplyHtml = $("#panelReply").wrap('<p/>').parent().html();
                panelReportHtml = $("#panelReport").wrap('<p/>').parent().html();
                initializePostList();


            }

//        $(document).ready(function () {
//            threadID = '<%= Request.QueryString["ThreadID"] %>';
//            courseID = '<%= Request.QueryString["CourseID"] %>';
//            
//            panelReplyHtml = $("#panelReply").wrap('<p/>').parent().html();
//            panelReportHtml = $("#panelReport").wrap('<p/>').parent().html();
//            
//        })

        function initializeForumConversionDates() {
            var ctDateCreated = $(".convertTimeZone");
            $.each(ctDateCreated, function (idx, el) {
                var txtDateCreated = $(el).text();

                var txtThreadDate = convertTimeZone(txtDateCreated);

                $(el).text(moment(txtThreadDate).format("MM/DD/YYYY hh:mm A"));
            });
        }

        function initializeImageModal(){
            var modal = $('#imageModal');
            // Get the image and insert it inside the modal - use its "alt" text as a caption
            var img = $(".js-img-zoom");
            var modalImg = $("#img01");
            img.click(function(){
                modal.css({ "display" : "block"});
                modalImg.attr("src", $(this).attr("src"));
                disableBodyScroll();
            });
           
            // Get the <span> element that closes the modal
            var span = $("#imageModal span")

            // When the user clicks on <span> (x), close the modal
            span.click(function() { 
                 modal.css({ "display" : "none"});
                 enableBodyScroll();
            });

        }

        function decodeComment(){
                //console.log("decode");
                $(".JSDecodeComment").each(function(){
                  var comment =  $(this).html();
                  try {
                          comment = decodeURI(comment);
                          //comment = imagify(comment);
                          comment = linkify(comment);
                            $(this).html(comment);
                    } catch (e) {
                        console.log(e.toString());
                        $(this).html("Invalid Comment");
                    }
               
            });
        }
         function linkify(text) {
            var urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
            return text.replace(urlRegex, function(url) {
                return '<a href="' + url + '" target="_blank">' + url + '</a>';
            });
        }

        function initializePostList() {
            //$ = $telerik.$;
            //this will fire the OnCommand event
            threadPostList = $find("<%= lvThreadPostList.ClientID%>");
            pageSize = 8;
            pageIndex = 0;
            threadPostList.rebind();
            threadPostList.set_pageSize(pageSize);
            threadPostList.page(0);
            $(".plPager .pagePrev").click(function (e) {
                if(pageIndex != 0){
                    showThreadLoading();
                    threadPostList.page(threadPostList.get_currentPageIndex() - 1);
                }
            });
            $(".plPager .pageNext").click(function (e) {
                var lastpage = (Math.ceil(threadPostList.get_virtualItemCount() / threadPostList.get_pageSize()));
                 if(pageIndex != (lastpage-1)){
                     showThreadLoading();
                    threadPostList.page(threadPostList.get_currentPageIndex() + 1);
                }
            });
            $(".plPager .pageFirst").click(function (e) {
                if(0 != pageIndex){
                    showThreadLoading();
                    threadPostList.page(0);
                }
            });
            $(".plPager .pageLast").click(function (e) {
                
                var lastpage = (Math.ceil(threadPostList.get_virtualItemCount() / threadPostList.get_pageSize()));
                if((lastpage-1) != pageIndex)
                    showThreadLoading();
                //console.log(lastpage);
                threadPostList.page(lastpage - 1);
            });


            $("#threadPostList").on("click", ".pl-item", function (e) {
                showThreadLoading();
                threadPostList.toggleSelection($(this).index());

            });

        }

        function bindThreadPostList() {
            var startRowIndex = threadPostList.get_currentPageIndex() * threadPostList.get_pageSize(),
                maximumRows = threadPostList.get_pageSize(),
                sortExpression = threadPostList.get_sortExpressions().toLinq();


                $.ajax({
                    type: "POST",
                    url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "GetThreadPosts",
                    data: "{startRowIndex: " + startRowIndex + ", maximumRows: " + maximumRows + ", sortExpression: '" + sortExpression  + "', threadID: '" + threadID + "'}",
                    contentType: "application/json; charset=utf-8",
                    success: function (response) {
                        var result = response.d;

                        virtualCount = result.count;

                        threadPostList.set_virtualItemCount(virtualCount);
                        threadPostList.set_dataSource(result.data);
                        //console.log(result.data);

                        if (virtualCount > 8) {
                            $(".plPager").removeClass("display-none");
                            $(".js-btn-postreply").removeClass("display-none");

                            generatePageNumbers();
                            setPage();
                        }
                        $(".Loading2").hide();

                        $(".js-pl-loader").hide();
                        threadPostList.dataBind();
                        decodeComment();
                        initializeImageModal();

                        initializeForumConversionDates();
                    },
                    error: function (res) { 
                        if(res.status == 401)
                           location.reload();
                        else if(res.status == 500)
                        var hndradalertwentwrong = $("#<%=hndradalertwentwrong.ClientID%>").val();
                        var hndradalerterrormess = $("#<%=hndradalerterrormess.ClientID%>").val();
                            radalert(hndradalertwentwrong, 330, 180, hndradalerterrormess, "");
                    }
                });
        }

        function showThreadLoading(){
            var panel = $find("<%= localLoadingPanel.ClientID %>");
                panel.show("<%= threadPostPanel.ClientID %>");
        }

        function plOnListViewDataBinding(sender, args) {
            sender.set_selectedIndexes([]);
        }

        function plOnListViewCommand(sender, args) {
            args.set_cancel(true);
            bindThreadPostList();
        }

        function plOnViewCreated(sender, args) {
            debugger;
            initializePostList();
        }

        function sortPostList() {
            threadPostList.get_sortExpressions().clear();
            threadPostList.get_sortExpressions().add('CourseTitle', "DESC");
            threadPostList.rebind();
        }
        function generatePageNumbers() {
            var maxPage = Math.ceil(virtualCount / pageSize);
            //var maxPageToDisplay = maxPage > 5 ? 5 : (virtualCount % pageSize > 1 ? maxPage : maxPage);
            var pageVirtualCount = virtualCount;
            var pages = $(".js-page-numbers ul").html('');

            for (var i = 1; i <= maxPage; i++) {
                var pages = $(".js-page-numbers ul").html();
                var rp = i - 1;
                $(".js-page-numbers ul").html(pages + '<li class="page-number"> <a href="javascript:void(0);" onclick="gotoPage(' + rp + ');">' + i + '</a></li>');
                //console.log("page" + i);
            }
        }

        function gotoPage(pageNum) {
            showThreadLoading();
            $(".js-page-numbers ul li").removeClass("selected-page");
            threadPostList.page(pageNum);
        }

        function setPage() {
            pageIndex = threadPostList.get_currentPageIndex();
            var myPage = pageIndex + 1;
            $(".js-page-numbers ul li").removeClass("selected-page");
            $(".js-page-numbers ul li:nth-child(" + myPage + ")").addClass("selected-page");
            var itemCount = pageSize * (pageIndex + 1) < virtualCount ? (pageSize * (pageIndex + 1)) : ((pageIndex * pageSize) + (virtualCount % pageSize));
            //$(".lblShowNumber").html("Showing " + itemCount + " of " + virtualCount + " results");
        }

        function showReplyForm() {
            $(".add-post").removeClass("display-none");
            $("#txtPostComment").val("");
            $("#txtPostComment").focus();

        }

        function hideReplyForm() {
            $(".add-post").addClass("display-none");
        }

        function addNewPost() {
            var comment = $("#txtPostComment").val().trim();
            if (comment != "") {
                //show client loading
                var panel = $find('<%= localLoadingPanel.ClientID  %>');
                panel.show("<%= threadPostPanel.ClientID%>");

                 $.ajax({
                    type: "POST",
                    url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "addNewPost",
                    data: "{comment: '" + comment + "', _courseID: '" + courseID + "', _threadID: '" + threadID + "'}",
                    contentType: "application/json; charset=utf-8",
                    success: function (response) {
                        var result = response.d;
                         $(".Loading2").hide();
                        if (result == 1) {
                            $(".add-post").addClass("display-none");
                            threadPostList.rebind();
                            threadPostList.page(0);
                            var hndradalertpostadd = $("#<%=hndradalertpostadd.ClientID%>").val();
                            var hndradalertsuccessmess = $("#<%=hndradalertsuccessmess.ClientID%>").val();
                            radalert(hndradalertpostadd, 330, 180, hndradalertsuccessmess, "");
                        } else if (result == 2) {
                         var hndradalertnopermis = $("#<%=hndradalertnopermis.ClientID%>").val();
                            var hndradalerterrormess = $("#<%=hndradalerterrormess.ClientID%>").val();
                            radalert(hndradalertnopermis, 330, 180, hndradalerterrormess, "");
                        } else {
                        var hndradalertaddingpost = $("#<%=hndradalertaddingpost.ClientID%>").val();
                            var hndradalerterrormess = $("#<%=hndradalerterrormess.ClientID%>").val();
                            radalert(hndradalertaddingpost, 330, 180, hndradalerterrormess, "");
                        }
                    },
                    error: function (res) { 
                        if(res.status == 401)
                           location.reload();
                        else if(res.status == 500)
                        var hndradalertcontactadmin = $("#<%=hndradalertcontactadmin.ClientID%>").val();
                            var hndradalerterrormess = $("#<%=hndradalerterrormess.ClientID%>").val();
                                radalert(hndradalertcontactadmin, 330, 180, hndradalerterrormess, "");
                    }
                });

            } else {
                $("#txtPostComment").focus();
            }
        }

        function fakeClickBtnHelpful(el, postid) {

            if($(el).hasClass("btn-lime")){
                 //$(el).removeClass("btn-lime").addClass("btn-teal");
                 $(el).css({ "background-color": "#0D9E9E" });
            }else{
                 //$(el).removeClass("btn-teal").addClass("btn-lime");
                 $(el).css({ "background-color": "Lime" });
            }
           console.log(postid);
            $.ajax({
                type: "POST",
                url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "btnHelpfulRes",
                data: '{postId:"' + postid + '"}',  
                contentType: "application/json; charset=utf-8",
                success: function (res) {
                    if(res.d == 1)
                       $(el).removeClass("btn-teal").addClass("btn-lime");
                    else if(res.d == 4)
                         $(el).removeClass("btn-lime").addClass("btn-teal");
                    

                    console.log(res.d);
                },
                error: function (res) { 
                    if(res.status == 401)
                       location.reload();
                },
            });
        }

        function showReport(e, postid) {
            hideReplyPanel();
            var id = $(e).attr("id");

            

            if($('#panelReport').length > 0){
                $("#panelReport").insertAfter($("#" + id).parent().parent().parent().parent());
            }else{

                $("#" + id).parent().parent().append(panelReportHtml);
            }
            
            $("#rfvReport").hide();
            $("#" + '<%= hfSelectedPostId.ClientID %>').val(postid);
            $("#ReportMessage").val('');

            $('#panelReport').removeClass("display-none");

        }

        function returnReportPanel() {
            $("#ReportMessage").val('');
            $('#panelReport').removeClass("display-none");
            $("#panelReport").insertAfter($("#ThreadContainer"));

        }

        function btnReportSubmit(el) {
            if (Page_ClientValidate("vgReport")) {
                var panel = $find("<%= localLoadingPanel.ClientID %>");
                panel.show("<%= threadPostPanel.ClientID %>");
                var postid = $('#<%= hfSelectedPostId.ClientID %>').val();
                var msg = ($("#ReportMessage").val()).trim();
                returnReportPanel();
                 $.ajax({
                    type: "POST",
                    url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "btnSubmitReport",
                    data: '{postId : "' +  postid + '",txtReportMessage : "' + msg + '"}',  
                    contentType: "application/json; charset=utf-8",
                    success: function (response) {
                        var res = response.d;
                        //console.log(res);
                        $(".Loading2").hide();
                        hideReplyPanel();
                        hideReportPanel();
                        if (res == 1) {
                        var hndradalertsuccesssubmit = $("#<%=hndradalertsuccesssubmit.ClientID%>").val();
                        var hndradalertsuccessmess = $("#<%=hndradalertsuccessmess.ClientID%>").val();
                            radalert(hndradalertsuccesssubmit, 330, 180, hndradalertsuccessmess, "");
                            }
                        else if (res == 2) {
                        var hndradalertnotmem = $("#<%=hndradalertnotmem.ClientID%>").val();
                        var hndradalertsuccessmess = $("#<%=hndradalertsuccessmess.ClientID%>").val();
                            radalert(hndradalertnotmem, 330, 180, hndradalertsuccessmess, "");
                            }
                        else {
                        var hndradalerterroradmin = $("#<%=hndradalerterroradmin.ClientID%>").val();
                        var hndradalertsuccessmess = $("#<%=hndradalertsuccessmess.ClientID%>").val();
                            radalert(hndradalerterroradmin, 330, 180, hndradalertsuccessmess, "");
                            }
                    },
                    error: function (res) { 
                        if(res.status == 401)
                           location.reload();
                        else if(res.status == 500)
                         var hndradalerttryagain = $("#<%=hndradalerttryagain.ClientID%>").val();
                        var hndradalerterrormess = $("#<%=hndradalerterrormess.ClientID%>").val();
                            radalert(hndradalerttryagain, 330, 180, hndradalerterrormess, "");
                    },
                });
            }
        }

        function hideReportPanel() {
            $("#ReportMessage").val("");
            $('#panelReport').addClass("display-none");
            return false;
        }

        function hideReplyPanel() {
            $("#txtReplyMessage").val("");
            $('#panelReply').addClass("display-none");
            return false;
        }

         function showReply(e, postid) {
            hideReportPanel();
            var id = $(e).attr("id");
            $("#" + '<%= hfSelectedPostId.ClientID %>').val(postid);

            
            if($('#panelReply').length > 0){
               $("#panelReply").insertAfter($("#btnReport" + postid).parent().parent().parent().parent());
            }else{
                $("#btnReport" + postid).parent().parent().parent().parent().append(panelReplyHtml);
            }

            var name = $("#name" + postid).html();
            var comment = $("#comment" + postid).html();
            $("#txtReplyMessage").val('');
            //var date = $("#date" + postid).html();
            $('#panelReply .talktext').html(
                '<p>' +
                '<b>-' + name
                + '<br>'
                + '<i> \"' + comment + '\" </i>' +
                '</p>'
            );
            
            

            $('#panelReply').removeClass("display-none");

        }

        function returnReplyPanel(){
            $("#txtReplyMessage").val('');
            $('#panelReply').removeClass("display-none");
            $("#panelReply").insertAfter($("#ThreadContainer"));
        }


        function btnReplySubmit(el) {
            //console.log(9);
            if (Page_ClientValidate("vgReply")) {
                var panel = $find("<%= localLoadingPanel.ClientID %>");
                panel.show("<%= threadPostPanel.ClientID %>");
                var postid = $('#<%= hfSelectedPostId.ClientID %>').val();
                var msg = ($('#txtReplyMessage').val()).trim();
                
                $.ajax({
                    type: "POST",
                    url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "btnSubmitReply",
                    data: '{postId: "' + postid +'" ,_threadId: "' + threadID + '", txtReportMessage:"' + msg + '"}',  
                    contentType: "application/json; charset=utf-8",
                    success: function (response) {
                        var res = response.d;
                        //console.log(res);
                        if (res == 1) {
                        var hndradalertsuccesssub = $("#<%=hndradalertsuccesssub.ClientID%>").val();
                        var hndradalertsuccessmess = $("#<%=hndradalertsuccessmess.ClientID%>").val();
                            radalert(hndradalertsuccesssub, 330, 180, hndradalertsuccessmess, "");
                            returnReplyPanel();
                            threadPostList.rebind();
                            threadPostList.page(0);
                        }else if (res == 2) {
                         var hndradalertnotmem = $("#<%=hndradalertnotmem.ClientID%>").val();
                        var hndradalerterrormess = $("#<%=hndradalerterrormess.ClientID%>").val();
                            radalert(hndradalertnotmem, 330, 180, hndradalerterrormess, "");
                       } else 
                       var hndradalertreplyadmin = $("#<%=hndradalertreplyadmin.ClientID%>").val();
                        var hndradalerterrormess = $("#<%=hndradalerterrormess.ClientID%>").val();
                            radalert(hndradalertreplyadmin, 330, 180, hndradalerterrormess, "");

                        hidePanels();
                    },
                    error: function (err) { 
                        console.log(err);
                        if(err.status == 401)
                           location.reload();
                        else if(err.status == 500){
                            var hndradalertwentsomethingreply = $("#<%=hndradalertwentsomethingreply.ClientID%>").val();
                        var hndradalerterrormess = $("#<%=hndradalerterrormess.ClientID%>").val();
                            radalert(hndradalertwentsomethingreply, 330, 180, hndradalerterrormess, "");
                            
                        }
                        

                    },
                });
            }
            function hidePanels() {
                $(".Loading2").hide();
                hideReplyPanel();
                hideReportPanel();
            }

        }

        
    </script>
</rad:RadCodeBlock>
<rad:RadAjaxLoadingPanel ID="localLoadingPanel" runat="server" CssClass="Loading2"
    Transparency="25" />
<rad:RadWindowManager ID="rwmThread" runat="server">
</rad:RadWindowManager>
<div id="ThreadContainer" class="container">
    <div class="thread-outer-container">
        <asp:Panel ID="threadPanel" runat="server" CssClass="thread-main-container">
            <div id="course-details">
                <asp:Label ID="lblCourseTitle" runat="server" Text="<%$ Resources:LocalizedResource, CourseTitle %>"
                    CssClass="forum-course-title pull-left"></asp:Label>
                <span class="pull-right">
                    <asp:Label ID="Label1" runat="server" Text="<%$ Resources:LocalizedResource, Status %>"
                        CssClass="yellow"></asp:Label>:
                    <asp:Literal ID="lblStatus" runat="server" Text="<%$ Resources:LocalizedResource, NotYetStarted %>"></asp:Literal></span>
                <br />
                <asp:Literal ID="ltCourseType" Text="OLT" runat="server" />
                <br />
                <asp:Label runat="server" ID="ctile1" Text="<%$ Resources:LocalizedResource, UploadDate %>"></asp:Label>
                :
                <asp:Literal ID="ltUploadDate" Text="January 1, 9999" runat="server" />
                <br />
                <asp:Label runat="server" ID="Label2" Text="<%$ Resources:LocalizedResource, ValidUntil %>"></asp:Label>:
                <asp:Literal ID="ltValidUntil" Text="January 1, 9999" runat="server" />
            </div>
        </asp:Panel>
        <div class="forum-navigation-tree">
            <rad:RadTreeView RenderMode="Lightweight" ID="rdtForumLevel" runat="server" Height="250px"
                BorderWidth="2px" BorderColor="Teal" BorderStyle="Solid" DataTextField="Title"
                DataFieldID="FileID" DataFieldParentID="ParentID" DataValueField="OFileID">
                <%--<NodeTemplate>
                    <asp:Image ID="icon" ImageUrl='<%# Eval("FileType", "~/Media/Images/tree-item-{0}.png") %>'
                        runat="server" />
                    <%# Eval("Title") %>
                </NodeTemplate>--%>
                <DataBindings>
                    <rad:RadTreeNodeBinding Depth="0"></rad:RadTreeNodeBinding>
                </DataBindings>
            </rad:RadTreeView>
        </div>
        <%---Button to Show New Post Form---%>
        <asp:LinkButton ID="btnPostReply" runat="server" CssClass="btn btn-md btn-teal pull-left js-btn-postreply"
            OnClientClick="showReplyForm();return false;" Style="margin: .5%;">
            <i class="fa fa-pencil-square-o"></i>
            <asp:Label runat="server" ID="Repspost" Text="<%$ Resources:LocalizedResource, PostReply %>"></asp:Label>
        </asp:LinkButton>
        <%---Thread Pager---%>
        <div class="plPager row display-none">
            <div class="js-page-numbers pull-right display-inline-flex page-numbers">
                <a class="pageFirst fa fa-backward no-underline-hover" href="javascript:void(0);"
                    title="Go to first page"></a><a class="pagePrev fa fa-caret-left no-underline-hover"
                        href="javascript:void(0);" title="Go to previous page"></a>
                <ul class="nostyle display-inline-flex">
                </ul>
                <a class="pageNext fa fa-caret-right no-underline-hover" href="javascript:void(0);"
                    title="Go to next page"></a><a class="pageLast fa fa-forward no-underline-hover"
                        href="javascript:void(0);" title="Go to last page"></a>
            </div>
        </div>
        <%--- Add New Post Form---%>
        <br />
        <br />
        <br />
        <div class="add-post display-none">
            <p class="title">
                <asp:Label runat="server" ID="Newrepspost" Text="<%$ Resources:LocalizedResource, PostNewReply %>"></asp:Label></p>
            <asp:RequiredFieldValidator ID="rfvtxtComment" runat="server" ControlToValidate="txtPostComment"
                ErrorMessage="Enter your Message" ValidationGroup="addPostValidation" Display="Dynamic"
                ForeColor="Red"></asp:RequiredFieldValidator>
            <asp:TextBox ID="txtPostComment" runat="server" Rows="6" TextMode="MultiLine" CssClass="form-control"
                placeholder="Write your message here..." ClientIDMode="Static" CausesValidation="true">
            </asp:TextBox>
            <div class="btn-group">
                <asp:LinkButton ID="LinkButton1" Text="Cancel" runat="server" CssClass="btn btn-md btn-teal"
                    OnClientClick="hideReplyForm(); return false;" />
                <asp:LinkButton ID="btnAddPostReply" Text="Add Post" runat="server" CssClass="btn btn-md btn-teal"
                    OnClientClick="addNewPost(); return false;" />
            </div>
        </div>
        <asp:Panel ID="threadPostPanel" runat="server">
            <div class="loader js-pl-loader ">
            </div>
            <%--<asp:TextBox runat="server" />--%>
            <%--<asp:Image ImageUrl="imageurl" runat="server" />--%>
            <rad:RadListView ID="lvThreadPostList" runat="server" AllowPaging="true" PageSize="8"
                AllowMultiFieldSorting="true" CssClass="display-none">
                <LayoutTemplate>
                    <div id="threadPostList">
                        <div id="pl-items" class="display-flow no-margin row">
                        </div>
                    </div>
                    <div class="panel panel-default display-none" id="panelReport" runat="server" clientidmode="Static">
                        <div class="panel-heading panel-heading-medium orange-header">
                            <asp:Label runat="server" ID="report" Text="<%$ Resources:LocalizedResource, Report %>"></asp:Label>
                            <button id="btnCloseReport" class="btn-transparent pull-right" onclick="hideReportPanel(); return false;">
                                x</button>
                        </div>
                        <div class="panel-body">
                            <asp:TextBox ID="ReportMessage" runat="server" TextMode="MultiLine" Rows="4" Columns="4"
                                ValidationGroup="vgReport" CssClass="form-control" placeholder="Message..." ClientIDMode="Static">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvReport" runat="server" ControlToValidate="ReportMessage"
                                Display="Dynamic" SetFocusOnError="true" ForeColor="Red" ValidationGroup="vgReport"
                                ClientIDMode="Static">
                                <asp:Label runat="server" ID="messenter" Text="<%$ Resources:LocalizedResource, EnteryourMessage %>"></asp:Label></asp:RequiredFieldValidator>
                            <br />
                            <asp:LinkButton ID="btnSubmitReport" ValidationGroup="vgReport" runat="server" Text="Submit"
                                OnClientClick="btnReportSubmit(this); return false;" CssClass="btn btn-teal btn-thin btn-sm btn-flat pull-right margin-side-5"></asp:LinkButton>
                            <button id="btnCancelReport" onclick="hideReportPanel(); return false;" class="btn btn-gray btn-thin btn-sm btn-flat pull-right margin-side-5">
                                <asp:Label runat="server" ID="Canc" Text="<%$ Resources:LocalizedResource, Cancel %>"></asp:Label>
                            </button>
                        </div>
                    </div>
                    <div class="panel panel-default display-none " id="panelReply">
                        <div class="panel-heading panel-heading-medium teal-header">
                            <asp:Label runat="server" ID="reps" Text="<%$ Resources:LocalizedResource, Reply %>"></asp:Label>
                            <button id="btnCloseReply" class="btn-transparent pull-right" onclick="hideReplyPanel(); return false;">
                                x</button>
                        </div>
                        <div class="panel-body">
                            <div class="talk-bubble tri-right left-top">
                                <div class="talktext">
                                    <p>
                                    </p>
                                </div>
                            </div>
                            <asp:TextBox ID="txtReplyMessage" runat="server" TextMode="MultiLine" Rows="4" Columns="4"
                                CssClass="form-control" placeholder="Message..." ValidationGroup="vgReply" ClientIDMode="Static">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvReply" runat="server" ControlToValidate="txtReplyMessage"
                                Display="Dynamic" SetFocusOnError="true" ForeColor="Red" ValidationGroup="vgReply">
                                <asp:Label runat="server" ID="messenteryour" Text="<%$ Resources:LocalizedResource, EnteryourMessage %>"></asp:Label>.</asp:RequiredFieldValidator>
                            <br />
                            <%--OnClick="btnReplySubmit"--%>
                            <asp:LinkButton ID="btnSubmitReply" runat="server" Text="Submit" OnClientClick="btnReplySubmit(this); return false;"
                                CssClass="btn btn-teal btn-thin btn-sm btn-flat pull-right margin-side-5" ValidationGroup="vgReply"></asp:LinkButton>
                            <button id="Button2" onclick="hideReplyPanel(); return false;" class="btn btn-gray btn-thin btn-sm btn-flat pull-right margin-side-5">
                                <asp:Label runat="server" ID="cancels" Text="<%$ Resources:LocalizedResource, Cancel %>"></asp:Label>
                            </button>
                        </div>
                    </div>
                </LayoutTemplate>
                <ClientSettings DataBinding-DataService-EnableCaching="true">
                    <DataBinding ItemPlaceHolderID="pl-items">
                        <ItemTemplate>
                            <table width="100%" class="tbl-post">
                            <tr class="post-details">
                                <td> <img src="http://www.myiconfinder.com/uploads/iconsets/256-256-bbbbe23abcae4390c132e229053fe77c.png" width="20px" class="pull-left"/>
                                   
                                   &nbsp;<span class="convertTimeZone">#= DatePosted #</span>
                                </td>
                                <td><i class="fa fa-cog pull-right pointer display-none"></i></td>
                            </tr>
                            <tr class="author-details">
                                <td  colspan="2">
                                <img src='#= UserImageUrl #' width="50px" class="pull-left"/>
                                    <p class=""> <a href="UserProfile.aspx" class="" id="name#= PostId #"> #= AuthorName #  </a> <br/>  #= RoleTitle # </p>
                                </td>
                            </tr>
                            <tr class="comment-details">
                                <td  colspan="2">
                                    <div class='talk-bubble tri-right left-top  #= QuotePostClass #'
                                        style="display: block; width: fit-content; padding: 5px 10px;">
                                        <div>
                                            <p>
                                                <b>-#= OriginalPostAuthor #</b><br>
                                                <i class="JSDecodeComment">
                                                #= OriginalPostComment #
                                                </i>
                                            </p>
                                        </div>
                                    </div>
                                    <p class="JSDecodeComment width-fill break-word" id="comment#= PostId #" style="padding-left:10px;"> #= PostComment #  </p>
                                </td>
                            </tr>
                            <tr class="action-details">
                                <td  colspan="2" style="text-align: right;">
                                    <button id="btnQuote#= PostId #" class="btn btn-sm btn-teal" onclick='showReply(this,#= PostId #); return false;'>
                                        <i class="fa fa-pencil-square-o"></i>
                                          Quote
                                    </button>
                                    <button class="btn btn-sm btn-teal #= ButtonClassHelpful #" onclick='fakeClickBtnHelpful(this, #= PostId #); return false;'>
                                        <i class="fa fa-thumbs-up" aria-hidden="true"></i>
                                      Helpful
                                    </button>
                                    <button id="btnReport#= PostId #" class="btn btn-sm btn-teal" onclick='showReport(this, #= PostId #); return false;'>
                                        <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                       Report
                                    </button>
                                </td>
                            </tr>
                        </table>
                        </ItemTemplate>
                        <EmptyDataTemplate>
                       No Post To Display.
                        </EmptyDataTemplate>
                    </DataBinding>
                    <%--OnTemplateCreated="plOnViewCreated" --%>
                    <ClientEvents OnCommand="plOnListViewCommand" OnDataBinding="plOnListViewDataBinding" >
                    </ClientEvents>
                </ClientSettings>
            </rad:RadListView>
            <br />
            <br />
            <br />
        </asp:Panel>
        
    </div>
    <br />
    <br />
</div>
<asp:HiddenField ID="hfSelectedPostId" runat="server" Value="0" />
<div id="imageModal" class="modal top-9999">
    <span class="close">&times;</span>
    <img class="modal-content img-large" id="img01" />
</div>
<asp:HiddenField runat="server" ID="hndradalertwentwrong" Value="Something went while loading thread post. Please try again." />
<asp:HiddenField runat="server" ID="hndradalerterrormess" Value="Error Message" />
<asp:HiddenField runat="server" ID="hndradalertpostadd" Value="Post successfully added." />
<asp:HiddenField runat="server" ID="hndradalertsuccessmess" Value="Success Message" />
<asp:HiddenField runat="server" ID="hndradalertnopermis" Value="You don't have permission to add post in this thread!" />
<asp:HiddenField runat="server" ID="hndradalertaddingpost" Value="Something went wrong in adding new post. Please contact your administrator" />
<asp:HiddenField runat="server" ID="hndradalertcontactadmin" Value="Something went wrong in adding new post. Please try again." />
<asp:HiddenField runat="server" ID="hndradalertsuccesssubmit" Value="Report was successfully submitted" />
<asp:HiddenField runat="server" ID="hndradalertnotmem" Value="You are not a member of this thread." />
<asp:HiddenField runat="server" ID="hndradalerterroradmin" Value="Error in submitting your report. Please contact your administrator." />
<asp:HiddenField runat="server" ID="hndradalerttryagain" Value="Something went while submitting your report. Please try again." />
<asp:HiddenField runat="server" ID="hndradalertsuccesssub" Value="Post was successfully submitted" />
<asp:HiddenField runat="server" ID="hndradalertreplyadmin" Value="Error in submitting your reply. Please contact your administrator." />
<asp:HiddenField runat="server" ID="hndradalertwentsomethingreply" Value="Something went while submitting your reply. Please try again." />
