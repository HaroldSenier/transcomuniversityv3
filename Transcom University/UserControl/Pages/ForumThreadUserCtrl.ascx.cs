﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TranscomUniversityV3Model;
using Telerik.Web.UI;

public partial class UserControl_Pages_ForumThreadUserCtrl : System.Web.UI.UserControl
{
    private int courseID;
    private int threadID;

    protected void Page_Load(object sender, EventArgs e)
    {
        courseID = Request.QueryString["CourseID"] != null ? Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"].ToString())) : -1;

        threadID = Request.QueryString["ThreadID"] != null ? Convert.ToInt32(Utils.Decrypt(Request.QueryString["ThreadID"].ToString())) : -1;

        string courseTitle = DataHelper.getCourseTitleByID(courseID);
        string threadTitle = DataHelper.getThreadTitleByID(threadID);

        if (!Page.IsPostBack)
        {
            if (courseID > 0)
                setCourseDetails();
        }
        
        RadTreeNode node1 = new RadTreeNode();
        RadTreeNode node2 = new RadTreeNode();
        RadTreeNode node3 = new RadTreeNode();

        //treeItem.Parent = "Forum";
        string folderIcon = "~/Media/Images/tree-item-2.png";
        node1.Text = "Course Forums";
        node1.ImageUrl = folderIcon;
        node2.Text = courseTitle;
        node2.ImageUrl = folderIcon;
        node3.Text = threadTitle;
        node1.Nodes.Add(node2);
        node3.ImageUrl = folderIcon;
        node2.Nodes.Add(node3);
        rdtForumLevel.Nodes.Add(node1);
        rdtForumLevel.ExpandAllNodes();

    }

    private void setCourseDetails()
    {
        var db = new TranscomUniversityV3ModelContainer();
        var courseDetails = db.tbl_TranscomUniversity_Cor_Course
            .SingleOrDefault(c => c.CourseID == courseID);
        lblCourseTitle.Text = courseDetails.Title;
        ltCourseType.Text = "OLT";
        ltUploadDate.Text = string.Format("{0: MMMM dd, yyyy }", courseDetails.DateTimePublished);
        ltValidUntil.Text = string.Format("{0: MMMM dd, yyyy }", courseDetails.Enddate);
        string status = "Not yet Started";
        DateTime dtServer = DataHelper.serverDate();
        DateTime dtStart = (DateTime)courseDetails.StartDate;
        DateTime dtEnd = (DateTime)courseDetails.Enddate;
        if(dtEnd < dtServer)
            status = "Completed";
        else if(dtEnd > dtServer && dtStart < dtServer)
            status="In Progress";
        else
            status = "Not yet Started";
        lblStatus.Text = status;
    }
}