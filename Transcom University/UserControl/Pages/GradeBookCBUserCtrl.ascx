﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GradeBookCBUserCtrl.ascx.cs"
    Inherits="GradeBookCBUserCtrl" %>
<%--OnNeedDataSource="rgGradeBook_OnNeedDataSource" --%>
<rad:RadGrid RenderMode="Lightweight" ID="rgGradeBook" runat="server" AllowSorting="True"
    Visible="true" CssClass="GridLess" Height="500px" AllowPaging="true" OnPreRender="rgGradeBook_PreRender"
    PageSize="10">
    <MasterTableView AutoGenerateColumns="false" HeaderStyle-ForeColor="Teal" NoMasterRecordsText="No Gradebook for this Course">
        <Columns>
            <rad:GridTemplateColumn HeaderText="#">
                <ItemTemplate>
                    <%#Container.ItemIndex+1 %></ItemTemplate>
            </rad:GridTemplateColumn>
            <rad:GridBoundColumn DataField="EnrolleeCIM" HeaderText="CIM" UniqueName="EnrolleeCIM">
            </rad:GridBoundColumn>
            <rad:GridTemplateColumn HeaderText="Name">
                <ItemTemplate>
                    <asp:Image ID="userImage" ImageUrl='<%# Eval("UserImageUrl", "{0}") %>' runat="server"
                        Height="50px" Width="50px" />
                    <asp:Label Text='<%# Eval("Name", "{0}") %>' runat="server" />
                </ItemTemplate>
            </rad:GridTemplateColumn>
            <rad:GridBoundColumn DataField="DateCourseTaken" HeaderText="Date Course Taken" UniqueName="DateCourseTaken">
            </rad:GridBoundColumn>
        </Columns>
    </MasterTableView>
    <ClientSettings Scrolling-AllowScroll="true">
    </ClientSettings>
</rad:RadGrid>
<rad:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <rad:AjaxSetting AjaxControlID="rgGradeBook">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="rgGradeBook" />
            </UpdatedControls>
        </rad:AjaxSetting>
    </AjaxSettings>
</rad:RadAjaxManagerProxy>
<rad:RadScriptBlock ID="rsb" runat="server">
    <script type="text/javascript">
        function changeNullToZero() {
            $("#<%= rgGradeBook.ClientID %> table tr td").each(function (i, l) {
                var cont = $(l).html();
                if (cont == '&nbsp;')
                    $(l).html('0');
            })
        }
      
    </script>
</rad:RadScriptBlock>
