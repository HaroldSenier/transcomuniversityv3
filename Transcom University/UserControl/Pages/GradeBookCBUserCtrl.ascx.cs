﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TranscomUniversityV3Model;
using Telerik.Web.UI;
using System.Data;

public partial class GradeBookCBUserCtrl : System.Web.UI.UserControl
{
    DataSet gb;

    protected void rgGradeBook_PreRender(object source, EventArgs e)
    {
        try
        {
            if (Request.QueryString["CourseID"] != null)
            {
                int courseID = Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"].ToString()));
                gb = DataHelper.GetGradeBookDetails(courseID);

                int count = gb.Tables[0].Columns.Count;
                if (rgGradeBook.Columns.Count <= count - 1)
                {
                    int rgCount = rgGradeBook.Columns.Count;
                    for (int i = 4; i < rgCount; i++)
                    {
                        rgGradeBook.MasterTableView.Columns.RemoveAt(4);
                    }
                }
                for (int i = 5; i < count; i++)
                {

                    GridBoundColumn boundColumn;
                    boundColumn = new GridBoundColumn();
                    boundColumn.DataField = gb.Tables[0].Columns[i].ColumnName.ToString();
                    boundColumn.HeaderText = gb.Tables[0].Columns[i].ColumnName.ToString();
                    rgGradeBook.MasterTableView.Columns.Add(boundColumn);
                }

                rgGradeBook.DataSource = gb;
                rgGradeBook.DataBind();

                ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", Utils.callClientScript("changeNullToZero"), true);
            }
        }
        catch
        {
            rgGradeBook.DataSource = null;
            rgGradeBook.DataBind();
        }

    }

    protected void Page_Load(object sender, EventArgs e)
    {


        if (!Page.IsPostBack)
        {

        }
    }


}