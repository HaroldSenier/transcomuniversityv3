﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HomeContentSettingsUserCtrl.ascx.cs"
    Inherits="UserControl_Pages_Home" %>
<rad:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        function pageLoad() {
//            $("#testimonialCarousel").on('slid.bs.carousel', function () {
//                //                var currentActiveItem = $("#testimonialCarousel .active");
//                //                $(currentActiveItem).removeClass("active");
//                //                $("#testimonialCarousel .item").removeClass("item-slide-out");
//                //                currentActiveItem.addClass("item-slide-out");

//                var currentActiveItem = $("#testimonialCarousel .active");
//                $("#testimonialCarousel .item").removeClass("slide-in");
//                $(currentActiveItem).addClass("slide-in").removeClass("active");
//                $("#testimonialCarousel .item").removeClass("item-slide-out");
//                currentActiveItem.prev().addClass("item-slide-out");
//                console.log(currentActiveItem.html());

//            });
            initializeEditorDblClick();
            initilizeImageSliderSettings();
            initilizeTestimonialSettings();

            $(".tc-slick-carousel.course-slider").slick({
                autoplay: true,
                slidesToShow: 3,
                slidesToScroll:3,
                rows: 2,
                speed: 3000,
                autoplaySpeed: 5000,
                arrows: true,
                dots:false,
                draggable:false,
                infinite: true,
                prevArrow: "#wrapper-catalog .prev",
                nextArrow: "#wrapper-catalog .next",
            });

//            $(".tc-slick-carousel.testimonial-slider").slick({
//                autoplay: true,
//                slidesToShow: 1,
//                slidesToScroll:1,
//                speed: 5000,
//                autoplaySpeed: 6000,
//                arrows: true,
//                dots:false,
//                vertical:true,
//                infinite: true,
//                draggable: false,
//            });
            //$('.tc-slick-carousel').slick('setPosition').slick();
        }

        function initilizeImageSliderSettings(){

            var maxImage = $("#<%= hfmaxImage.ClientID %>").val().trim();
            var playInterval = $("#<%= hfplayInterval.ClientID %>").val().trim() * 1000;
            var isVideoOnClick  = $("#<%= hfisVideoOnClick.ClientID %>").val().trim();
            var isVideoAutoPlay = $("#<%= hfisVideoAutoPlay.ClientID %>").val().trim();
            var videoSlideAfter = $("#<%= hfvideoSlideAfter.ClientID %>").val().trim();
            var isVideoOnClick = $("#<%= hfisVideoOnClick.ClientID %>").val().trim();
            var loopDirection = $("#<%= hfloopDirection.ClientID %>").val().trim();
                    
            var rtl;
            var vertical;
            if(loopDirection == 1){
                rtl = false;
                vertical = false;
            }else if(loopDirection == 2){
                $(".tc-slick-carousel.image-slider").parent().attr("dir", "rtl");
                rtl = true;
                vertical = false;
            }else if(loopDirection == 3){
                rtl = false;
                vertical = true;
            }else if(loopDirection == 4){
                $(".tc-slick-carousel.image-slider").parent().attr("dir", "rtl");
                rtl = true;
                vertical = true;
            }
            $(".tc-slick-carousel.image-slider").slick({
                autoplay: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                speed: 5000,
                autoplaySpeed: playInterval,
                arrows: false,
                dots:true,
                draggable:false,
                infinite: true,
                rtl:rtl,
                vertical : vertical,
            });

        }

        function initilizeTestimonialSettings(){


            var maxImage = $("#<%= hftMaxImage.ClientID %>").val().trim();
            var playInterval = $("#<%= hftPlayInterval.ClientID %>").val().trim() * 1000;
            var loopDirection = $("#<%= hftLoopDirection.ClientID %>").val().trim();
                    
            var rtl;
            var vertical;
            if(loopDirection == 1){
                rtl = false;
                vertical = false;
            }else if(loopDirection == 2){
                $(".tc-slick-carousel.testimonial-slider").parent().attr("dir", "rtl");
                rtl = true;
                vertical = false;
            }else if(loopDirection == 3){
                rtl = false;
                vertical = true;
            }else if(loopDirection == 4){
                $(".tc-slick-carousel.testimonial-slider").parent().attr("dir", "rtl");
                rtl = true;
                vertical = true;
            }
            $(".tc-slick-carousel.testimonial-slider").slick({
                autoplay: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                speed: 1000,
                autoplaySpeed: playInterval,
                arrows: false,
                dots:false,
                draggable:false,
                infinite: true,
                rtl:rtl,
                vertical : vertical,
            });

        }

        function gotoDashboard() {
            window.location.href = "Learner.aspx";
        }

        
//        function loadSliderSettings() {
//            debugger;
//            $.ajax({
//                type: "POST",
//                contentType: "application/json; charset=utf-8",
//                url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "GetSliderSettings",
//                dataType: 'json',
//                success: function (res) {
//                    var data = res.d;

//                    var maxImage = data.MaxItemCount;
//                    var playInterval = data.PlayInterval * 1000;
//                    var isVideoOnClick = data.IsOnClick;
//                    var isVideoAutoPlay = data.IsAutoPlay;
//                    var videoSlideAfter = data.VideoDelay;
//                    var isVideoOnClick = data.IsVideoOnClick;
//                    var loopDirection = data.SlideDirection;
//                    
//                    var direction;
//                    var vertical;
//                    if(loopDirection == 1){
//                        direction = false;
//                        vertical = false;
//                    }else if(loopDirection == 2){
//                        direction = "rtl";
//                        vertical = false;
//                    }else if(loopDirection == 3){
//                        direction = true;
//                        vertical = false;
//                    }else if(loopDirection == 4){
//                        direction = "rtl";
//                        vertical = true;
//                    }
//                    
//                   

//                  
//                },
//                error: function (XMLHttpRequest, textStatus, errorThrown) {
//                    var code = XMLHttpRequest.status;
//                    if (code == "401")
//                        //window.location.reload();
//                    else
//                        alert(XMLHttpRequest.responseText);
//                }
//            });
//        }

    function pageEditorRedirectTo(to){
        window.location.href = "Admin.aspx?Tab=" + to;
    }

    function initializeEditorDblClick(){
        $(".overlay-page-editor").dblclick(function (event) {
            var to  = $(event.target).attr("id");
            pageEditorRedirectTo(to);
        });
       
    }

    function redirectToCourseLaunch(CourseID) {
        var tz = $("#hfEncryptedOffset").val();
        window.location.href = "CourseLauncher.aspx?CourseID=" + CourseID + "&tz=" + tz;
    }
    </script>
</rad:RadScriptBlock>
<div id="wrapper-carousel" class="region" style="height: 130vh; width: 100%;">
    
    <div class="overlay-page-editor show-on-hover" style="height:130vh;" id="ImageSlider">
        <button class="btn-transparent" onclick='return false;'>
            <i class="fa fa-hand-pointer-o" aria-hidden="true"></i>&nbsp;<asp:Label runat="server" ID="Doubleclick" Text= "<%$ Resources:LocalizedResource, DoubleclickonthegrayedoutareatoconfiguretheImageSlidersettingsandplaylist %>"></asp:Label>
        </button>
        <br />
        <br />
        <button class="btn-transparent move" onclick='return false;' >
            <i class="fa fa-bars" aria-hidden="true"></i>&nbsp;<asp:Label runat="server" ID="Dragstoreor" Text= "<%$ Resources:LocalizedResource, DragtoreorderthesequenceofyourHomepagescontentssections %>"></asp:Label>
        </button>
        <br />
        <br />
        <button class="btn-transparent" onclick='return false;' >
            <i class="fa fa-eye-slash" aria-hidden="true"></i>&nbsp;<asp:Label runat="server" ID="hidesimage" Text= "<%$ Resources:LocalizedResource, Hidetheimageslider %>"></asp:Label>
        </button>
    </div>
    <div class="tc-slick-carousel image-slider">
        <asp:Repeater ID="rptSlides" runat="server">
            <ItemTemplate>
                <div class="with-caption">
                    <img src='Media/Uploads/Sliders/<%#Eval("PlaylistID")%>/<%#Eval("PlaylistImg")%>'
                        class="img-responsive" alt="" onerror='this.src="Media/Images/error-image.png"'>
                    <div class="carousel-caption">
                        <p class="link-yellow">
                            <%#Eval("PlaylistTitle")%></p>
                        <p>
                            <%#Eval("PlaylistDesc") %></p>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
        <%--  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
        <span class="icon-prev" aria-hidden="true"></span><span class="sr-only">Previous</span>
    </a><a class="right carousel-control" href="#carousel-example-generic" role="button"
        data-slide="next"><span class="icon-next" aria-hidden="true"></span><span class="sr-only">
            Next</span> </a>--%>
    </div>
</div>
<asp:Panel ID="wrapper_featured" runat="server" CssClass="region" ClientIDMode="Static">
    <div  class="overlay-page-editor show-on-hover" style="height:80vh;"  id="FeaturedCourses">
        <button class="btn-transparent" onclick="return false;">
            <i class="fa fa-hand-pointer-o" aria-hidden="true"></i>&nbsp;<asp:Label runat="server" ID="doublehideclick" Text= "<%$ Resources:LocalizedResource, DoubleclickonthegrayedoutareatoconfiguretheImageSlidersettingsandplaylist %>"></asp:Label>
        </button>
        <br />
        <br />
        <button class="btn-transparent move" onclick="return false;">
            <i class="fa fa-bars" aria-hidden="true"></i>&nbsp;<asp:Label runat="server" ID="reordragsec" Text= "<%$ Resources:LocalizedResource, DragtoreorderthesequenceofyourHomepagescontentssections %>"></asp:Label>
        </button>
        <br />
        <br />
        <button class="btn-transparent" onclick="return false;">
            <i class="fa fa-eye-slash" aria-hidden="true"></i>&nbsp;<asp:Label runat="server" ID="feathide" Text= "<%$ Resources:LocalizedResource, HidetheFeaturedCourses %>"></asp:Label>
        </button>
    </div>

    <br />
    <br />
    <div class="col-md-12 header">
        <h4 class="orange pull-left">
           <asp:Label runat="server" ID="Featurecourse" Text= "<%$ Resources:LocalizedResource,  SeeAll %>"></asp:Label>
        </h4>
        <h4>
            <asp:Button ID="Button4" Text="<%$ Resources:LocalizedResource, Views %>" runat="server" CssClass="btn-link orange underline pull-right" />
        </h4>
        <div class="orange-line">
        </div>
    </div>
    <div class="details col-md-9">
        <asp:Repeater ID="rptFeaturedCourses" runat="server">
            <ItemTemplate>
                <div class="col-md-4">
                    <img class="img-responsive" src='Media/Uploads/CourseImg/<%#Eval("CourseID")%>/<%#Eval("CourseImage")%>'
                        onerror='this.src="Media/Images/error-image.png"' />
                    <h5 class="course-title">
                        <%# Eval("CourseTitle")%></h5>
                    <p class="course-description">
                        <%# Eval("CourseDescription")%></p>
<<<<<<< HEAD
                    <a href='CourseRedirect.aspx?CourseID=<%#Eval("EncCourseID")%>' class="btn btn-xs btn-thin btn-orange btn-flat">
                      <asp:Label runat="server" ID="moreread" Text= "<%$ Resources:LocalizedResource, Readmore %>"></asp:Label></a>
=======
                    <a href='#' onclick='redirectToCourseLaunch(<%#Eval("EncCourseID")%>)' class="btn btn-xs btn-thin btn-orange btn-flat">
                        Read more</a>
>>>>>>> 9199dc429b855ab0966dd800fbdd249672ee1306
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</asp:Panel>
<div id="wrapper-catalog" class="region">
    <div class="overlay-page-editor show-on-hover" style="height:80vh;" id="HomepageEditor">
        <button class="btn-transparent" onclick='return false;'>
            <i class="fa fa-hand-pointer-o" aria-hidden="true"></i>&nbsp;<asp:Label runat="server" ID="clickdouble" Text= "<%$ Resources:LocalizedResource, DoubleclickonthegrayedoutareatoconfiguretheImageSlidersettingsandplaylist %>"></asp:Label>
        </button>
        <br />
        <br />
        <button class="btn-transparent move" onclick='return false;'>
            <i class="fa fa-bars" aria-hidden="true"></i>&nbsp;<asp:Label runat="server" ID="Pagehomedrag" Text= "<%$ Resources:LocalizedResource, DragtoreorderthesequenceofyourHomepagescontentssections %>"></asp:Label>
        </button>
        <br />
        <br />
        <button class="btn-transparent" onclick='return false;'>
            <i class="fa fa-eye-slash" aria-hidden="true"></i>&nbsp;<asp:Label runat="server" ID="imagehidecata" Text= "<%$ Resources:LocalizedResource, HidetheimageCourseCatalog %>"></asp:Label>
        </button>
    </div>
    <br />
    <br />
    <div class="col-md-12 header">
        <h4 class="orange pull-left">
            <asp:Label runat="server" ID="catacourse" Text= "<%$ Resources:LocalizedResource, CourseCatalog %>"></asp:Label>
        </h4>
        <h4>
            <asp:Button ID="Button1" Text="See all" runat="server" CssClass="btn-link orange underline pull-right" />
        </h4>
        <div class="orange-line">
        </div>
    </div>
    <div class="details col-md-12">
        <div id="csCourseCatalog" class="tc-slick-carousel course-slider" style="position: relative;
            left: 50%; transform: translate(-47%, 0);">
            <asp:Repeater ID="rptCourseItem" runat="server">
                <ItemTemplate>
                    <div class="col-md-4 course">
                        <img src='Media/Uploads/CourseImg/<%#Eval("CourseID")%>/<%#Eval("CourseImage")%>'
                            onerror='this.src="Media/Images/error-image.png"' width="380px" height="200px" />
                        <h5 class="course-title">
                            <%# Eval("CourseTitle")%></h5>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <a class="prev fa fa-chevron-left pull-left relative-vr-left-center gray-arrow no-underline-hover"
            role="button" title="Go to previous page"></a><a class="next fa fa-chevron-right pull-right relative-vr-right-center gray-arrow no-underline-hover"
                role="button" title="Go to next page"></a>
    </div>
</div>
<div id="wrapper-news" class="region">
    <div class="overlay-page-editor show-on-hover" style="height:110vh;" id="LatestNews">
        <button class="btn-transparent" onclick='return false;'>
            <i class="fa fa-hand-pointer-o" aria-hidden="true"></i>&nbsp;<asp:Label runat="server" ID="doublesettclick" Text= "<%$ Resources:LocalizedResource, DoubleclickonthegrayedoutareatoconfiguretheImageSlidersettingsandplaylist %>"></asp:Label>
        </button>
        <br />
        <br />
        <button class="btn-transparent move" onclick='return false;'>
            <i class="fa fa-bars" aria-hidden="true"></i>&nbsp;<asp:Label runat="server" ID="sechomedragpage" Text= "<%$ Resources:LocalizedResource, DragtoreorderthesequenceofyourHomepagescontentssections %>"></asp:Label>
        </button>
        <br />
        <br />
        <button class="btn-transparent" onclick='return false;'>
            <i class="fa fa-eye-slash" aria-hidden="true"></i>&nbsp;<asp:Label runat="server" ID="newhidelatest" Text= "<%$ Resources:LocalizedResource, HidetheLatestNews %>"></asp:Label>
        </button>
    </div>
    <br />
    <br />
    <div class="col-md-12 header">
        <h4 class="orange pull-left">
           <asp:Label runat="server" ID="ctile1" Text= "<%$ Resources:LocalizedResource,  LatestNews %>"></asp:Label>
        </h4>
        <h4>
            <asp:Button ID="Button2" Text="See all" runat="server" CssClass="btn-link orange underline pull-right" />
        </h4>
        <div class="orange-line">
        </div>
    </div>
    <asp:Repeater ID="rptNews" runat="server">
        <ItemTemplate>
            <div class="details row">
                <div class="col-md-3 inner-image">
                    <img src="Media/Uploads/News/<%# Eval("id")%>/<%# Eval("FeaturedImage")%>" alt="<%# Eval("NewsTitle")%>"
                        onerror='this.src="Media/Images/error-image.png"' />
                </div>
                <div class="col-md-8 inner-details">
                    <h5 class="orange">
                        <%# Eval("NewsTitle")%>
                    </h5>
                    <p>
                        <%# Eval("NewsShortDesc")%>
                    </p>
                    <asp:Button ID="Button3" Text="Read more" runat="server" CssClass="btn btn-xs btn-flat btn-teal pull-right right-offset" />
                </div>
            </div>
        </ItemTemplate>
    </asp:Repeater>
</div>
<div id="wrapper-testimonials" class="region">
    <div class="overlay-page-editor show-on-hover" style="height:60vh;" id="Testimonials">
        <button class="btn-transparent" onclick='return false;'>
            <i class="fa fa-hand-pointer-o" aria-hidden="true"></i>&nbsp;<asp:Label runat="server" ID="doublehideclicks" Text= "<%$ Resources:LocalizedResource, DoubleclickonthegrayedoutareatoconfiguretheImageSlidersettingsandplaylist %>"></asp:Label>
        </button>
        <br />
        <br />
        <button class="btn-transparent move" onclick='return false;'>
            <i class="fa fa-bars" aria-hidden="true"></i>&nbsp;<asp:Label runat="server" ID="pagehomedragsequ" Text= "<%$ Resources:LocalizedResource, DragtoreorderthesequenceofyourHomepagescontentssections %>"></asp:Label>
        </button>
        <br />
        <br />
        <button class="btn-transparent" onclick='return false;'>
            <i class="fa fa-eye-slash" aria-hidden="true"></i>&nbsp;<asp:Label runat="server" ID="hidetesti" Text= "<%$ Resources:LocalizedResource, HidetheTestimonials %>"></asp:Label>
        </button>
    </div>
    <br />
    <br />
    <div class="col-md-12 header">
        <h4 class="orange pull-left">
            <asp:Label ID="lblTestimonialTitle" runat="server">
            <asp:Label runat="server" ID="talkTU" Text= "<%$ Resources:LocalizedResource, SeewhypeoplearetalkingaboutthelatestversionofTranscomUniversity %>"></asp:Label>
            </asp:Label>
        </h4>
        <h4>
            <asp:Button ID="Button7" Text="See all" runat="server" CssClass="btn-link orange underline pull-right" />
        </h4>
        <div class="orange-line">
        </div>
    </div>
    <div class="details col-md-12">
        <div class="container">
            <div id="testimonialCarousel" class="tc-slick-carousel testimonial-slider col-md-6"
                style="position: relative; left: 50%; transform: translateX(-50%); height: 340px;">
                <!-- Carousel items -->
                <asp:Repeater ID="rptTestimonials" runat="server">
                    <ItemTemplate>
                        <div class="item" style="margin-bottom: 45px;">
                            <div class="col-md-3">
                                <img src="default-user.png" alt="Alternate Text" class="img-circle border-md-gray" />
                                <h5>
                                    <strong>
                                        <%# Eval("Name")%></strong>
                                </h5>
                                <h5>
                                    <%# Eval("MyRole")%>
                                </h5>
                                <h5>
                                    <%# Eval("Department")%>
                                </h5>
                                <h5>
                                    <%# Eval("Program")%>
                                </h5>
                            </div>
                            <div class="col-md-8 col-md-offset-1">
                                <p class="testimonial-comment teal">
                                    <%# Eval("Comment")%>
                                </p>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
                <!-- Carousel nav -->
                <%--<a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
                    <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>--%>
            </div>
        </div>
    </div>
</div>
<asp:Panel ID="wrapper_signin" runat="server" CssClass="region" ClientIDMode="Static">
    <h1 class="inner-message">
      <asp:Label runat="server" ID="Label1" Text= "<%$ Resources:LocalizedResource, NeedToCheckYourCourses %>"></asp:Label>  
    </h1>
    <asp:Button ID="btnSign" Text="<%$ Resources:LocalizedResource, GoToMyDashboard %>" runat="server" CssClass="btn btn-lg btn-transparent btn-gray soft border-white" />
</asp:Panel>
<asp:HiddenField ID="hfmaxImage" runat="server" />
<asp:HiddenField ID="hfplayInterval" runat="server" />
<asp:HiddenField ID="IsOnClick" runat="server" />
<asp:HiddenField ID="hfisVideoAutoPlay" runat="server" />
<asp:HiddenField ID="hfvideoSlideAfter" runat="server" />
<asp:HiddenField ID="hfisVideoOnClick" runat="server" />
<asp:HiddenField ID="hfloopDirection" runat="server" />
<asp:HiddenField ID="hftMaxImage" runat="server" />
<asp:HiddenField ID="hftPlayInterval" runat="server" />
<asp:HiddenField ID="hftLoopDirection" runat="server" />
<asp:HiddenField ID="hftSelectedPlaylist" runat="server" />
