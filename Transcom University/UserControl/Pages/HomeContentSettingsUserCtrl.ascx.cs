﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using TranscomUniversityV3Model;
using Microsoft.AspNet.Membership.OpenAuth;

public partial class UserControl_Pages_Home : System.Web.UI.UserControl
{
    Dictionary<int, List<catalogItem>> catalogDictionary;
    public string ReturnUrl { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        ReturnUrl = Request.QueryString["ReturnUrl"];
        //set button text
        bool isLoggedIn = DataHelper.isLoggedIn();


        string text = isLoggedIn == true ? "GO TO MY DASHBOARD" : "SIGN-IN";
        string onClickFn = isLoggedIn == true ? "gotoDashboard();" : "";

        btnSign.Text = text;
        btnSign.OnClientClick = onClickFn;
        if (!isLoggedIn)
        {
            btnSign.Click += new EventHandler(btnRequestLogin_Click);
        }

        if (!Page.IsPostBack)
        {
            var playlist = DataHelper.getCurrentPlaylist();
            rptSlides.DataSource = playlist;
            rptSlides.DataBind();

            LoadFeaturedCourses();

            LoadCourseCatalog();

            LoadLatestNews();

            LoadTestimonials();

            //set slider settings
            var sliderSettings = DataHelper.getSliderSettings();
            if (sliderSettings != null)
            {
                hfmaxImage.Value = sliderSettings.MaxItemCount + "";
                hfplayInterval.Value = sliderSettings.PlayInterval + "";
                IsOnClick.Value = sliderSettings.IsAutoplay + "";
                hfisVideoAutoPlay.Value = sliderSettings.IsOnClick + "";
                hfvideoSlideAfter.Value = sliderSettings.VideoDelay + "";
                hfisVideoOnClick.Value = sliderSettings.IsVideoOnClick + "";
                hfloopDirection.Value = sliderSettings.SlideDirection + "";
            }

            var testimonialSettings = DataHelper.getTestimonialSettings();
            if (testimonialSettings != null)
            {
               
                hftMaxImage.Value = testimonialSettings.MaxItems + "";
                hftPlayInterval.Value = testimonialSettings.ImagePlayInterval + "";
                hftLoopDirection.Value = testimonialSettings.LoopDirection + "";
                hftSelectedPlaylist.Value = testimonialSettings.SelectedTestimonialID + "";
            }

            if (Request.QueryString["Tab"] == "HomepageEditor")
                wrapper_signin.Visible = false;
            else
                wrapper_signin.Visible = true;

        }


    }

    //protected void rptPlaylist_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    //{
    //    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
    //    {
    //        //Repeater rptIndicators = e.Item.FindControl("rptIndicators") as Repeater;
    //        Repeater rptSlides = e.Item.FindControl("rptSlides") as Repeater;
    //        //rptIndicators.DataSource = DataHelper.GetAllPlaylistsSlides();
    //        //rptIndicators.DataBind();
    //        rptSlides.DataSource = DataHelper.GetAllPlaylistsSlides();
    //        rptSlides.DataBind();

    //        LoadFeaturedCourses();
    //    }
    //}

    void LoadFeaturedCourses()
    {
        rptFeaturedCourses.DataSource = DataHelper.GetFeaturedCourses();
        rptFeaturedCourses.DataBind();
    }

    protected void rptCourseCatalog_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

        }
    }

    void LoadCourseCatalog()
    {
        var db = new TranscomUniversityV3ModelContainer();
        var courseCatalog = db.vw_TranscomUniversity_AllCourses.Take(20).Select(c => new
        {
            c.CourseID,
            c.CourseTitle,
            c.CourseImage
        }).ToList();


        rptCourseItem.DataSource = courseCatalog;
        rptCourseItem.DataBind();


        //catalogDictionary = new Dictionary<int, List<catalogItem>>();

        // int slideNum = courseCatalog.Count() / 6;
        // int x = 0;
        // for (int i = 0; i < slideNum; i++) {
        //     List<catalogItem> itemList = new List<catalogItem>();
        //     for(int z = 0; z < 6;z++){
        //         x++;
        //         catalogItem item = new catalogItem();
        //         item.courseID = courseCatalog[x].CourseID;
        //         item.courseImage = courseCatalog[x].CourseImage;
        //         item.courseTitle = courseCatalog[x].CourseTitle;
        //         itemList.Add(item);
        //     }
        //     catalogDictionary.Add(i, itemList);
        // }

        //DataTable dummy = new DataTable();
        //for (int i = 0; i < slideNum; i++) {
        //    dummy.Columns.Add();
        //    dummy.Rows.Add();
        //}

        //rptCourseCatalog.DataSource = dummy;
        //rptCourseCatalog.DataBind();
    }

    void LoadLatestNews()
    {
        var db = new TranscomUniversityV3ModelContainer();
        var news = db.tbl_trancomuniversity_NewsArticles
                        .Where(n => n.Inactive == false)
                        .ToList();
        rptNews.DataSource = news;
        rptNews.DataBind();
    }

    void LoadTestimonials()
    {
        var db = new TranscomUniversityV3ModelContainer();
        var testimonials = db.vw_TranscomUniversity_CurrentTestimonials
                              .AsEnumerable()
                              .Select(c => new
                              {
                                  CommentID = Utils.Encrypt(c.ID),
                                  Comment = c.OriginalComment,
                                  DateCreated = c.ShortDateCreated,
                                  c.UserImageUrl,
                                  c.Rating,
                                  c.Name,
                                  c.Title,
                                  UID = c.ID,
                                  MyRole = c.Role,
                                  Program = c.Program,
                                  Department = c.Department
                              })
                              .ToList();
        rptTestimonials.DataSource = testimonials;
        rptTestimonials.DataBind();
    }
    

    class catalogItem
    {

        public int courseID { get; set; }
        public string courseTitle { get; set; }
        public string courseImage { get; set; }
    }

    protected void btnRequestLogin_Click(object sender, EventArgs e)
    {
        var redirectUrl = "~/ExternalLandingPage.aspx";

        if (!String.IsNullOrEmpty(ReturnUrl))
        {
            var resolvedReturnUrl = ResolveUrl(ReturnUrl);
            redirectUrl += "?ReturnUrl=" + HttpUtility.UrlEncode(resolvedReturnUrl);
        }

        OpenAuth.RequestAuthentication("google", redirectUrl);
    }
}