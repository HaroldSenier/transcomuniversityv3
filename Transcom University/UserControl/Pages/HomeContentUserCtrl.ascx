﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HomeContentUserCtrl.ascx.cs"
    Inherits="UserControl_Pages_Home" %>
<rad:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        var COURSESLIDERSIZE;
        var COURSESLIDERROW;
        
        function pageLoad() {
            
            initilizeImageSliderSettings();
            //initilizeTestimonialSettings();

            if(ISMOBILE){
                COURSESLIDERSIZE = 1;
                COURSESLIDERROW = 1;
            }else{
                COURSESLIDERSIZE = 3;
                COURSESLIDERROW = 2;
            }
                
            $(".tc-slick-carousel.course-slider").slick({
                autoplay: true,
                slidesToShow: COURSESLIDERSIZE,
                slidesToScroll:COURSESLIDERSIZE,
                rows: COURSESLIDERROW,
                speed: 3000,
                autoplaySpeed: 5000,
                arrows: true,
                dots:false,
                draggable:false,
                infinite: true,
                prevArrow: "#wrapper-catalog .prev",
                nextArrow: "#wrapper-catalog .next",
            });

        }

        function initilizeImageSliderSettings(){

            var maxImage = $("#<%= hfmaxImage.ClientID %>").val().trim();
            var playInterval = $("#<%= hfplayInterval.ClientID %>").val().trim() * 1000;
            var isVideoOnClick  = $("#<%= hfisVideoOnClick.ClientID %>").val().trim();
            var isVideoAutoPlay = $("#<%= hfisVideoAutoPlay.ClientID %>").val().trim();
            var videoSlideAfter = $("#<%= hfvideoSlideAfter.ClientID %>").val().trim();
            var isVideoOnClick = $("#<%= hfisVideoOnClick.ClientID %>").val().trim();
            var loopDirection = $("#<%= hfloopDirection.ClientID %>").val().trim();
                    
            var rtl;
            var vertical;
            if(loopDirection == 1){
                rtl = false;
                vertical = false;
            }else if(loopDirection == 2){
                $(".tc-slick-carousel.image-slider").parent().attr("dir", "rtl");
                rtl = true;
                vertical = false;
            }else if(loopDirection == 3){
                rtl = false;
                vertical = true;
            }else if(loopDirection == 4){
                $(".tc-slick-carousel.image-slider").parent().attr("dir", "rtl");
                rtl = true;
                vertical = true;
            }
            $(".tc-slick-carousel.image-slider").slick({
                autoplay: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                speed: 5000,
                autoplaySpeed: playInterval,
                arrows: false,
                dots:true,
                draggable:false,
                infinite: true,
                rtl:rtl,
                vertical : vertical,
            });

        }

        function initilizeTestimonialSettings(){


            var maxImage = $("#<%= hftMaxImage.ClientID %>").val().trim();
            var playInterval = $("#<%= hftPlayInterval.ClientID %>").val().trim() * 1000;
            var loopDirection = $("#<%= hftLoopDirection.ClientID %>").val().trim();
                    
            var rtl;
            var vertical;
            if(loopDirection == 1){
                rtl = false;
                vertical = false;
            }else if(loopDirection == 2){
                $(".tc-slick-carousel.testimonial-slider").parent().attr("dir", "rtl");
                rtl = true;
                vertical = false;
            }else if(loopDirection == 3){
                rtl = false;
                vertical = true;
            }else if(loopDirection == 4){
                $(".tc-slick-carousel.testimonial-slider").parent().attr("dir", "rtl");
                rtl = true;
                vertical = true;
            }
            $(".tc-slick-carousel.testimonial-slider").slick({
                autoplay: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                speed: 1000,
                autoplaySpeed: playInterval,
                arrows: false,
                dots:false,
                draggable:false,
                infinite: true,
                rtl:rtl,
                vertical : vertical,
            });

        }

        function gotoDashboard() {
            window.location.href = "Learner.aspx";
        }

        function redirectToCourseLaunch(CourseID) {
            var tz = $("#hfEncryptedOffset").val();
            window.location.href = "CourseLauncher.aspx?CourseID=" + CourseID + "&tz=" + tz;
        }
    </script>
</rad:RadScriptBlock>
<div id="wrapper-carousel">
    <div class="tc-slick-carousel image-slider">
        <asp:Repeater ID="rptSlides" runat="server">
            <ItemTemplate>
                <div class="with-caption">
                    <img src='Media/Uploads/Sliders/<%#Eval("PlaylistID")%>/<%#Eval("PlaylistImg")%>'
                        class="img-responsive" alt="" onerror='this.src="Media/Images/error-image.png"'>
                    <div class="carousel-caption bg-white">
                        <p class="teal bold">
                            <%#Eval("PlaylistTitle")%></p>
                        <div>
                            <%#Eval("PlaylistDesc") %></div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</div>
<asp:Panel ID="wrapper_featured" runat="server" CssClass="region" ClientIDMode="Static">
    <br />
    <br />
    <div class="col-md-12 header">
        <h4 class="orange pull-left">
           <asp:Label runat="server" ID="fcourse" Text= "<%$ Resources:LocalizedResource,  FeaturedCourses %>"></asp:Label> Featured Courses
        </h4>
        <h4>
            <asp:Button ID="Button4" Text="See all" runat="server" CssClass="btn-link orange underline pull-right" />
        </h4>
        <div class="orange-line">
        </div>
    </div>
    <div class="details col-md-9">
        <asp:Repeater ID="rptFeaturedCourses" runat="server" >
            <ItemTemplate>
                <div class="col-md-4">
                    <img class="img-responsive" src='Media/Uploads/CourseImg/<%#Eval("CourseID")%>/<%#Eval("CourseImage")%>'
                        onerror='this.src="Media/Images/error-image.png"' />
                    <h5 class="course-title">
                        <%# Eval("CourseTitle")%></h5>
                    <p class="course-description">
                        <%# Eval("CourseDescription")%></p>
                    <a href='#' onclick='redirectToCourseLaunch(<%# Utils.Encrypt(Convert.ToInt32(Eval("CourseID"))) %>)' class="btn btn-xs btn-thin btn-orange btn-flat">Read more</a>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</asp:Panel>
<br />
<br />
<asp:Panel ID="wrapper_signin" runat="server" CssClass="container col-lg-12 bg-transparent wrapper_signin">
            <asp:Panel ID="Panel1" runat="server" CssClass="relative-vr-center z-index-2 text-center col-md-12">
                <h1 class="inner-message white" style="font-size: 40px;">
                   <asp:Label runat="server" ID="ctile1" Text= "<%$ Resources:LocalizedResource,  NeedToCheckYourCourses %>"></asp:Label> 
                </h1>
                <asp:Button ID="btnSign" Text="GOTO MY DASHBOARD" runat="server" CssClass="btn btn-lg btn-transparent btn-gray soft border-white" OnClientClick="gotoLearnerDashboard();return false;"  />
            </asp:Panel>
        </asp:Panel>

<asp:HiddenField ID="hfmaxImage" runat="server" />
<asp:HiddenField ID="hfplayInterval" runat="server" />
<asp:HiddenField ID="IsOnClick" runat="server" />
<asp:HiddenField ID="hfisVideoAutoPlay" runat="server" />
<asp:HiddenField ID="hfvideoSlideAfter" runat="server" />
<asp:HiddenField ID="hfisVideoOnClick" runat="server" />
<asp:HiddenField ID="hfloopDirection" runat="server" />

<asp:HiddenField ID="hftMaxImage" runat="server" />
<asp:HiddenField ID="hftPlayInterval" runat="server" />
<asp:HiddenField ID="hftLoopDirection" runat="server" />
<asp:HiddenField ID="hftSelectedPlaylist" runat="server" />
