﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using TranscomUniversityV3Model;
using Microsoft.AspNet.Membership.OpenAuth;

public partial class UserControl_Pages_Home : System.Web.UI.UserControl
{
    public string ReturnUrl { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        ReturnUrl = Request.QueryString["ReturnUrl"];
        //set button text
        bool isLoggedIn = DataHelper.isLoggedIn();

        string text = isLoggedIn == true ? "GO TO MY DASHBOARD" : "SIGN-IN";
        string onClickFn = isLoggedIn == true ? "gotoDashboard();" : "";

        btnSign.Text = text;
        btnSign.OnClientClick = onClickFn;
        if (!isLoggedIn)
        {
            btnSign.Click += new EventHandler(btnRequestLogin_Click);
        }

        if (!Page.IsPostBack)
        {
            var playlist = DataHelper.getCurrentPlaylist();
            rptSlides.DataSource = playlist;
            rptSlides.DataBind();

            LoadFeaturedCourses();

            //LoadCourseCatalog();

            //LoadLatestNews();

            //LoadTestimonials();

            //set slider settings
            var sliderSettings = DataHelper.getSliderSettings();
            if (sliderSettings != null)
            {
                hfmaxImage.Value = sliderSettings.MaxItemCount + "";
                hfplayInterval.Value = sliderSettings.PlayInterval + "";
                IsOnClick.Value = sliderSettings.IsAutoplay + "";
                hfisVideoAutoPlay.Value = sliderSettings.IsOnClick + "";
                hfvideoSlideAfter.Value = sliderSettings.VideoDelay + "";
                hfisVideoOnClick.Value = sliderSettings.IsVideoOnClick + "";
                hfloopDirection.Value = sliderSettings.SlideDirection + "";


            }

            var testimonialSettings = DataHelper.getTestimonialSettings();
            if (testimonialSettings != null)
            {
                hftMaxImage.Value = testimonialSettings.MaxItems + "";
                hftPlayInterval.Value = testimonialSettings.ImagePlayInterval + "";
                hftLoopDirection.Value = testimonialSettings.LoopDirection + "";
                hftSelectedPlaylist.Value = testimonialSettings.SelectedTestimonialID + "";
            }



            if (Request.QueryString["Tab"] == "HomepageEditor")
                wrapper_signin.Visible = false;
            else
                wrapper_signin.Visible = true;

        }
    }

    void LoadFeaturedCourses()
    {
        rptFeaturedCourses.DataSource = DataHelper.GetFeaturedCourses();
        rptFeaturedCourses.DataBind();
    }

    protected void rptCourseCatalog_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

        }
    }

    protected void btnRequestLogin_Click(object sender, EventArgs e)
    {
        var redirectUrl = "~/ExternalLandingPage.aspx";

        if (!String.IsNullOrEmpty(ReturnUrl))
        {
            var resolvedReturnUrl = ResolveUrl(ReturnUrl);
            redirectUrl += "?ReturnUrl=" + HttpUtility.UrlEncode(resolvedReturnUrl);
        }

        OpenAuth.RequestAuthentication("google", redirectUrl);
    }
}