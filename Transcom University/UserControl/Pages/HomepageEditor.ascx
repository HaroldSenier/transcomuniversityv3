﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HomepageEditor.ascx.cs" Inherits="UserControl_Pages_HomepageEditor" %>

<%@ Register Src="~/UserControl/Pages/HomeContentSettingsUserCtrl.ascx" TagName="Home" TagPrefix="ucHome" %>

    <div class="container-fluid no-paddings" id="main-content">
        <ucHome:Home ID="Home1" runat="server" />
    </div>
