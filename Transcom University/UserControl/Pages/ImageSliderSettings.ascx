﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ImageSliderSettings.ascx.cs"
    Inherits="UserControl_Pages_ImageSliderSettings" %>
<rad:RadScriptBlock ID="rsbImageSlider" runat="server">
    <script type="text/javascript">
        var PLAYLIST_SLIDES;
        var UPLOADED_SLIDES = new Array();
        var CURRENT_PLAYLIST_DS;
        var CURRENT_PLAYLIST_COUNT;
        function pageLoad() {

           
           bindCurrentPlaylist(0);
            $("#imageSlider").dblclick(function () {
                openSliderSettings();
            });
            //UPLOADED_SLIDES = nul;

        }

        function setDirectionActive(direction) {
           
            if($.isNumeric(direction) && (direction >=1 && direction <= 4)){
                $(".js-direction-selector i").removeClass("active");
                $("#<%= hfLoopDirection.ClientID %>").val(direction);
                    switch (direction) {
                        case 1:
                            $(".js-direction-selector #arrowLeft").addClass("active");
                            break;
                        case 2:
                            $(".js-direction-selector #arrowRight").addClass("active");
                            break;
                        case 3:
                            $(".js-direction-selector #arrowUp").addClass("active");
                            break;
                        case 4:
                            $(".js-direction-selector #arrowDown").addClass("active");
                            break;
                        default:
                            $(".js-direction-selector i").removeClass("active");
                    }
                    
            }else{
                
            }
            
            
        }

        function openSliderSettings() {
            bindOtherPlaylist(0);
            $find("<%= rwmImageSliderSettings.ClientID %>").show();
            bindInnerCurrentPlaylist();
        }

        function closeSliderSettings() {
            $find("<%= rwmImageSliderSettings.ClientID %>").close();
        }

        function saveSliderSettings() {
            showfullPageLoading();
            //validate
            var maxImages;
            var playInterval;
            var isAutoplay;
            var isOnclick;
            var videoDelay;
            var isVideoOnClick;
            var loopDirection;
            var selectedPlaylistID;

            maxImages =  $find("<%= ddlMaxItem.ClientID %>").get_selectedItem().get_value();
            playInterval = $find("<%= ddlInterval.ClientID %>").get_selectedItem().get_value();
            isAutoplay = $('#<%= cbAutoplay.ClientID %>').is(':checked') == true ? true : false;
            isOnclick = $('#<%= cbOnclick.ClientID %>').is(':checked') == true ? true : false;
            videoDelay = $find("<%= ddlSlideAfter.ClientID %>").get_selectedItem().get_value();
            isVideoOnClick = $('#<%= cbVideoOnclick.ClientID %>').is(':checked') == true ? true : false;
            loopDirection =  $('#<%= hfLoopDirection.ClientID %>').val().trim();
            selectedPlaylistID = $('#<%= hfSelectedPlaylistID.ClientID %>').val().trim();
            
            console.log("maxImages = " + maxImages);
            console.log("playInterval = " + playInterval);
            console.log("isAutoplay = " + isAutoplay);
            console.log("isOnclick = " + isOnclick);
            console.log("videoDelay = " + videoDelay);
            console.log("isVideoOnClick = " + isVideoOnClick);
            console.log("loopDirection = " + loopDirection);
            console.log("selectedPlaylistID = " + selectedPlaylistID);
            //save
            $.ajax({
                type: "POST",
                data: '{ maxImages: "' + maxImages + '", playInterval: "' + playInterval + '", isAutoplay: "' + isAutoplay + '", isOnclick: "' + isOnclick+ '", videoDelay: "' + videoDelay+ '", isVideoOnClick: "' + isVideoOnClick+ '", loopDirection: "' + loopDirection+ '", selectedPlaylistID: "' + selectedPlaylistID + '", user: "<%= HttpContext.Current.User.Identity.Name.Split('|')[0] %>"}',
                contentType: "application/json; charset=utf-8",
                url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "SaveSliderSetting",
                dataType: 'json',
                success: function (res) {
                    console.log(res);
                    if(res.d.isSuccess == true){
                        //window.location.reload();
                        bindCurrentPlaylist(1);
                        hidefullPageLoading();
                    }
                    else
                        radalert(res.d.message, 330, 180, "Error", "");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var code = XMLHttpRequest.status;
                    if (code == "401")
                        window.location.reload();
                    else
                        console.log(XMLHttpRequest.responseText);
                }
            });

            


            $find("<%= rwmImageSliderSettings.ClientID %>").close();
        }

        function openPlaylistCreator() {
            $find("<%= rwPlaylistCreator.ClientID %>").show();

            //            $('.allSlideList').on('lazyLoadError', function(event, slick, image, imageSource){
            //                console.log(event);
            //                console.log(slick);
            //                console.log(image);
            //                console.log(imageSource);
            //                $(image).attr("src","Media/Images/error-image.png");
            //            });
            //            setTimeout(function(){
            //                $('.allSlideList').slick({
            //                  lazyLoad: 'ondemand',
            //                  slidesToShow: 6,
            //                  slidesToScroll: 6,
            //                  rows: 2,
            //                  arrows: true,
            //                  autoPlay:false,
            //                  prevArrow: '<button type="button" class="slick-prev arrow-gray"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
            //                  nextArrow: '<button type="button" class="slick-next arrow-gray"><i class="fa fa-angle-right" aria-hidden="true"></i></button>',
            //                });
            //            })
            
            //}


        }

        function closePlaylistCreator() {
            $find("<%= rwPlaylistCreator.ClientID %>").close();
        }

        function savePlaylistCreator() {
            debugger;
            showfullPageLoading();
            var slideCreatorItems = {};
            var newData = [];
            var playlistName = $("#<%= txtPlaylistName.ClientID %>").val().trim();

           if (Page_ClientValidate("savePlaylist")) {
                
                $("#slideCreatorSlider .slick-slide").each(function(){
	                
                    if(!$(this).hasClass("slick-cloned")){
                        var filename = escape($(this).find('input[name="filename"]').val().trim());
                        var caption_title = escape($(this).find('input[name="title"]').val().trim());
                        var caption_desc = escape($(this).find('textarea[name="description"]').val().trim());
                
                        var slideCreatorItem = {filename: filename.toString(), title: caption_title.toString(), desc: caption_desc.toString()};
                        newData.push(slideCreatorItem);
                    }
                });
            
                slideCreatorItems = {"slide": newData };

                if (newData.length > 0){
                    $.ajax({
                        type: "POST",
                        data: '{selectedSlides: \'' + JSON.stringify(newData) + '\', playlistName: "' + playlistName + '", user: "<%= HttpContext.Current.User.Identity.Name.Split('|')[0] %>"}',
                        contentType: "application/json; charset=utf-8",
                        url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "SaveUploadedSliderImages",
                        dataType: 'json',
                
                        success: function (res) {
                            if(res.d.isSuccess == true){
                                 
                                 //location.reload();
                                 //reload playlist list
                                 closePlaylistCreator();
                                 //showSliderSettingsLoading();
                                 bindOtherPlaylist(2);
                                 //$("#<%= btnReloadSlideList.ClientID %>").click();
                                 $('.tc-slick-carousel-selected-images').slick('unslick');
                                 $(".tc-slick-carousel-selected-images").html("");
                                 hidefullPageLoading();
                             
                            }else{
                                radalert("ERROR \n" + res.d.message, 330, 180, "Error", "");
                            }
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            var code = XMLHttpRequest.status;
                            if (code == "401")
                                window.location.reload();
                            else
                                console.log(XMLHttpRequest.responseText);
                        }
                    });
                }else{
                    radalert("Please Upload some image to proceed!", 330, 180, $('#<%= hfNoImageUploaded.ClientID %>').val(), "");
                }
                

            //$find("<%= rwPlaylistCreator.ClientID %>").close();
           }else{
                radalert("Playlist Name or Uploaded File is Not Valid. Please try again.", 330, 180, $('#<%= hfError.ClientID %>').val(), "");
           } 
        }

        function setItemActive(item) {
            
            if ($(item).hasClass("active")) {
                $(item).removeClass("active");
            } else {
                $(item).addClass("active");
            }

        }

        function openUploader() {
            var uploader = $("#<%= slideUploader.ClientID %> .ruFileInput");
            uploader.click();
        }

        //slide creator
        function RadUploader_filesSelected(sender, args) {

            showSlideCreatorLoading();


//            var fileExtention = args.get_fileName().substring(args.get_fileName().lastIndexOf('.') + 1, args.get_fileName().length);
//            if (args.get_fileName().lastIndexOf('.') != -1) {//this checks if the extension is correct
//                if (sender.get_allowedFileExtensions().toLowerCase().indexOf(fileExtention.toLowerCase()) == -1) {
//                    radalert("Invalid File Type.<br> Allowed Files are JPG, GIF, PNG.", 330, 180, "Error", "");
//                }
//                else {
//                    console.log("File Supported.");
//                }
//            }
//            else {
//                radalert("Invalid File Type.<br> Allowed Files are JPG, GIF, PNG.", 330, 180, "Error", "");
//            }
        }

        function escapeUnicode(str) {
            return str.replace(/[^\0-~]/g, function(ch) {
                return "\\u" + ("0000" + ch.charCodeAt().toString(16)).slice(-4);
            });
        }

        function RadUploader_ClientFilesUploaded(sender, args) {
                        debugger;
            var upload = $find("<%= slideUploader.ClientID %>");
            var inputs = upload.getUploadedFiles();
            var count = inputs.length - 1;
            for (i = inputs.length - 1; i >=0; i--) {
                if (!upload.isExtensionValid(inputs[i])){
                    upload.deleteFileInputAt(i);
                    radalert("Invalid file extension for " + inputs[i].get_fileName(), 330 ,180, "Invalid Extension");
                }
            }
            //            console.log(sender);
            //            console.log(args);
            //            console.log(args.get_fileInfo().ContentType);
            //            $("#panelUploadManager #EmptyMessage").hide();
            //            $("#<%= slideUploader.ClientID %>").insertAfter($("#"));
           
            $("#<%= refreshPreview.ClientID %>").click();


            console.log("file uploaded");

        }


        function loadUploadedImages(filename, src) {
            
            var fn = filename.split(",");
            var url = src.split(",");
            
            var uploader = $find("<%= slideUploader.ClientID %>");
            uploader.deleteAllFileInputs();
            console.log(fn);
            console.log(url);
            
                    for(var i =0;i<fn.length-1;i++){
                        var src = url[i];
                        var filename = fn[i];
                        UPLOADED_SLIDES.push(src);
                        if (!$(".tc-slick-carousel-selected-images").hasClass('slick-slider')) {
                            $(".tc-slick-carousel-selected-images").html("");
                            $('.tc-slick-carousel-selected-images').slick({
                                lazyLoad: 'ondemand',
                                slidesToShow: 4,
                                slidesToScroll: 4,
                                arrows: true,
                                autoPlay:false,
                                centerMode:false,
                                draggable:false,
                                prevArrow: '<button type="button" class="slick-prev arrow-gray"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
                                nextArrow: '<button type="button" class="slick-next arrow-gray"><i class="fa fa-angle-right" aria-hidden="true"></i></button>',
                            });
                             addSlide(src, filename);
                        }else{
                             addSlide(src, filename);
                        }
                    }
                    //if (UPLOADED_SLIDES.inde)
                    hideSlideCreatorLoading();
                  
      
            function addSlide(src, filename){
                $('.tc-slick-carousel-selected-images').slick('slickAdd', '<div class="col-md-3 no-paddings image-container"><div class="overlay actions"><i class="fa fa-times medium" aria-hidden="true" onclick="removeItemInPlaylistCreator(this);return false;" ></i></div><input name="filename" type="hidden" class="display-none" value="' + filename + '" /><img height="auto" width="100%" src="' + src + '"/><div class="caption" style="color:black; display:grid;"><input type="type" name="title" value="" placeholder="Title" /><textarea name="description" rows="5" cols="5" placeholder="Description" style="margin-top:2px;"></textarea></div></div>');
                //$('.tc-slick-carousel-selected-images').slick('slickAdd', '<div class="col-md-3 no-paddings"><div class="overlay"></div><img height="188px" width="200px" src="' + src + '"/></div>');
            }
        
        }
     

        function showSlideCreatorLoading() {
            var panel = $find("<%= localLoadingPanel.ClientID %>");
            panel.show("slideCreatorSlider");
        }

        function hideSlideCreatorLoading() {
            var panel = $find("<%= localLoadingPanel.ClientID %>");
            panel.hide("slideCreatorSlider");
        }

        function showSliderSettingsLoading() {
            var panel = $find("<%= localLoadingPanel.ClientID %>");
            panel.show("sliderSettings");
        }

        function hideSliderSettingsLoading() {
            //radalert("Playlist successfully Uploaded.", 330, 180, "Success", "");
            var panel = $find("<%= localLoadingPanel.ClientID %>");
            panel.hide("sliderSettings");

        }

        function btnPlayClick(playlistID, playlistName){
            //console.log(playlist);
            $("#<%= lblSelectedPlaylistName.ClientID %>").html(playlistName);
            $("#<%= hfSelectedPlaylistID.ClientID %>").val(playlistID);
            $("#<%= hfSelectedPlaylistName.ClientID %>").val(playlistName);
            
            //unslick
            bindInnerCurrentPlaylist();
            
            //console.log(playlistID);
        }

        function removeItemInPlaylistCreator(element){
            var index =  $(element).parent().parent().index(".slick-slide:not(.slick-cloned)");
            console.log(index);
            var src = $(element).parent().parent().find("img").attr("src");
            for (var i=UPLOADED_SLIDES.length-1; i>=0; i--) {
                if (UPLOADED_SLIDES[i] === src) {
                    UPLOADED_SLIDES.splice(i, 1);
                    // break;       //<-- Uncomment  if only the first term has to be removed
                }
            }
            $('.tc-slick-carousel-selected-images').slick('slickRemove', index);
        }
        
        function removePlaylist(playlistID, playlistName){
            radconfirm("Are you sure you want to delete the playlist " + playlistName + "?", callBackConfirmDeletePlaylist, 330, 180, "", "Confirm Delete");
            function callBackConfirmDeletePlaylist(arg){
                if(arg){
                     $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        data: "{playlistID: '" + playlistID + "'}",
                        url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "RemovePlaylist",
                        dataType: 'json',
                        success: function (res) {
                            var isSuccess = res.d.isSuccess;
                            if(isSuccess == true){
                                bindOtherPlaylist(1);
                                //$("#<%= btnReloadSlideList.ClientID %>").click();
                            }else{
                                radalert("Error in playlist.", 330, 180, "Success", "");
                            }
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            var code = XMLHttpRequest.status;
                            if (code == "401")
                                window.location.reload();
                            else
                                console.log(XMLHttpRequest.responseText);
                        }
                    });
                }
            }
           
        }

      

        function bindOtherPlaylist(operation) {
          
            var otherpl = $find("<%= lvOtherPlaylist.ClientID %>");
//            var startRowIndex = otherpl.get_currentPageIndex() * otherpl.get_pageSize(),
//            maximumRows = otherpl.get_pageSize(),
//            sortExpression = otherpl.get_sortExpressions().toLinq();
            showSliderSettingsLoading();
             $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                //data: '{startRowIndex: "' + startRowIndex + '", maximumRows: "' + maximumRows + '", sortExpression: "' + sortExpression + '"}',
                data: '{optional : "1"}' ,
                url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "GetOtherPlaylist",
                dataType: 'json',
                success: function (res) {
                    var count = res.d.count;
                    var data = res.d.data;
                    otherpl.set_virtualItemCount(count);
                    otherpl.set_dataSource(data);
                    otherpl.dataBind();
                    if(count > 0){
                        hideSliderSettingsLoading();
                        //radalert("Playlist successfully removed.", 330, 180, "Success", "");
                    }

                    if(operation == 1)//remove
                        radalert("Playlist Removed", 330, 180, "Success", "");
                    else if (operation == 2)
                       radalert("Playlist added successfully", 330, 180, "Success", "");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var code = XMLHttpRequest.status;
                    if (code == "401")
                        window.location.reload();
                    else
                        console.log(XMLHttpRequest.responseText);
                }
            });
        }

        function plOnListViewDataBinding(sender, args) {
            sender.set_selectedIndexes([]);
        }

        function plOnListViewCommand(sender, args) {
            args.set_cancel(true);
            bindOtherPlaylist(0);
        }

        function bindCurrentPlaylist(operation) {
            //slicks
           
           $(".tc-slick-carousel-preview").css({"opacity" : 0});
           $(".tc-slick-carousel-thumbnails").css({"opacity" : 0});

            if ($(".tc-slick-carousel-preview").hasClass("slick-slider"))
                $('.tc-slick-carousel-preview').slick('unslick');
            if ($(".tc-slick-carousel-thumbnails").hasClass("slick-slider"))
                $('.tc-slick-carousel-thumbnails').slick('unslick');


            var playlistID = $("#<%= hfSelectedPlaylistID.ClientID %>").val().trim();
           console.log("playlistID" + playlistID);
           if(playlistID != ""){
             var prevpl = $find("<%= lvPlaylistPrev.ClientID %>");
            var thumbprev = $find("<%= lvPlaylistPrevThumbnail.ClientID %>");
            var pln = $("#<%= hfSelectedPlaylistName.ClientID %>").val();
            $("#<%= lblPrevPlName.ClientID %>").html(pln);
             $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: '{playlistID: "' + playlistID + '"}',
                url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "GetPlaylist",
                dataType: 'json',
                success: function (res) {
                    var count = res.d.count;
                    var data = res.d.data;
                    CURRENT_PLAYLIST_DS = data;
                    CURRENT_PLAYLIST_COUNT = count;

                    prevpl.set_virtualItemCount(count);
                    prevpl.set_dataSource(data);
                    prevpl.dataBind();

                    thumbprev.set_virtualItemCount(count);
                    thumbprev.set_dataSource(data);
                    thumbprev.dataBind();

                    if(count > 0){
                        //prevpl
                        if (!$(".tc-slick-carousel-preview").hasClass("slick-slider")) {
                            $('.tc-slick-carousel-preview').slick({
                                lazyLoad: 'ondemand',
                                slidesToShow: 1,
                                slidesToScroll: 1,
                                arrows: false,
                                fade: true,
                                asNavFor: '.tc-slick-carousel-thumbnails'
                            });
                        }
            
                        //thumbpl
                        if (!$(".tc-slick-carousel-thumbnails").hasClass("slick-slider")) {
                            $('.tc-slick-carousel-thumbnails').slick({
                                lazyLoad: 'ondemand',
                                slidesToShow: 4,
                                slidesToScroll: 1,
                                centerPadding: "0px",
                                asNavFor: '.tc-slick-carousel-preview',
                                dots: false,
                                focusOnSelect: true
                            });
                        }

                        $(".tc-slick-carousel-preview").css({"opacity" : 1});
                        $(".tc-slick-carousel-thumbnails").css({"opacity" : 1});
                        
                        if(operation == 1)//save settings
                        {
                            var plName = $("#<%= hfSelectedPlaylistName.ClientID %>").val();
                            $("#<%= lblPrevPlName.ClientID%>").html(plName);

                        radalert("New settings have been saved.", 330, 180, "Saved", "");
                        }
                            
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var code = XMLHttpRequest.status;
                    if (code == "401")
                        window.location.reload();
                    else
                        console.log(XMLHttpRequest.responseText);
                }
            });
           }
          


        }

        function bindInnerCurrentPlaylist() {
            //slicks
           var currentpl = $find("<%= lvCurrentPL.ClientID %>");
            if ($(".tc-slick-carousel-currentpl").hasClass("slick-slider"))
                $('.tc-slick-carousel-currentpl').slick('unslick');

            $(".tc-slick-carousel-currentpl").css({"opacity" : 0});
            
            var playlistID = $("#<%= hfSelectedPlaylistID.ClientID %>").val().trim();
            playlistID = playlistID == "" ? 0 : playlistID;
                $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: '{playlistID: "' + playlistID + '"}',
                url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "GetPlaylist",
                dataType: 'json',
                success: function (res) {

                    var count = res.d.count;
                    var data = res.d.data;
                    CURRENT_PLAYLIST_DS = data;
                    CURRENT_PLAYLIST_COUNT = count;

                    currentpl.set_virtualItemCount(CURRENT_PLAYLIST_COUNT);
                    currentpl.set_dataSource(CURRENT_PLAYLIST_DS);
                    currentpl.dataBind();

                      $(".tc-slick-carousel-currentpl").css({"opacity" : 1});
                     if(CURRENT_PLAYLIST_COUNT > 0){
                            //currentpl
                      

                        if (!$(".tc-slick-carousel-currentpl").hasClass("slick-slider")) {
                            $('.tc-slick-carousel-currentpl').slick({
                                lazyLoad: 'ondemand',
                                slidesToShow: 4,
                                slidesToScroll: 1,
                                autoPlay:false,
                                arrows: true,
                                prevArrow: '<button type="button" class="slick-prev arrow-gray"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
                                nextArrow: '<button type="button" class="slick-next arrow-gray"><i class="fa fa-angle-right" aria-hidden="true"></i></button>',
                            });
                        }
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var code = XMLHttpRequest.status;
                    if (code == "401")
                        window.location.reload();
                    else
                        console.log(XMLHttpRequest.responseText);
                }
            });
         
        }

        function cplOnListViewDataBinding(sender, args) {
            sender.set_selectedIndexes([]);
        }

        function cplOnListViewCommand(sender, args) {
            args.set_cancel(true);
            bindInnerCurrentPlaylist();
        }

        function tpplOnListViewDataBinding(sender, args) {
            sender.set_selectedIndexes([]);
        }

        function tpplOnListViewCommand(sender, args) {
            args.set_cancel(true);
            bindCurrentPlaylist(0);
        }
        function pplOnListViewDataBinding(sender, args) {
            sender.set_selectedIndexes([]);
        }

        function pplOnListViewCommand(sender, args) {
            args.set_cancel(true);
            bindCurrentPlaylist(0);
        }

         function showfullPageLoading () {
            var panel = $find("<%= fullPageLoading.ClientID %>");
            panel.show("pnlCatalogSettings");
        }

        function hidefullPageLoading (){
            var panel = $find("<%= fullPageLoading.ClientID %>");
            panel.hide("pnlCatalogSettings");
        }
    </script>
</rad:RadScriptBlock>
<rad:RadWindowManager ID="rwmImageSlider" RenderMode="Lightweight" EnableShadow="true"
    VisibleOnPageLoad="false" Behaviors="Close, Move" DestroyOnClose="true" Modal="true"
    Opacity="99" runat="server" VisibleStatusbar="false" Skin="Bootstrap">
    <Windows>
        <rad:RadWindow ID="rwmImageSliderSettings" runat="server" Title="" CssClass="tc-radwindow-3 font-size-normal"
            Width="985px" AutoSizeBehaviors="Height" Height="800px">
            <ContentTemplate>
                <h5>
                    <strong>
                        <asp:Label ID="lblSelectedPlaylistName" runat="server">No Playlist has been set.</asp:Label></strong>
                </h5>
                <rad:RadListView ID="lvCurrentPL" runat="server" AllowPaging="false" PageSize="999">
                    <LayoutTemplate>
                        <div id="currentPL" class="tc-slick-carousel-currentpl" style="height: 200px; width: 85%;
                            margin: 0 7% 0 3%;">
                        </div>
                    </LayoutTemplate>
                    <ClientSettings DataBinding-DataService-EnableCaching="true">
                        <DataBinding ItemPlaceHolderID="currentPL">
                            <ItemTemplate>
                                <div class="col-md-3 no-paddings image-container" style="margin-right: 5px;">
                                    <div class="overlay actions">
                                        <i class="fa fa-ellipsis-v medium" aria-hidden="true"></i><i class="fa fa-pencil-square-o medium"
                                            aria-hidden="true"></i><i class="fa fa-times medium" aria-hidden="true"></i>
                                    </div>
                                    <img data-lazy="Media/Uploads/Sliders/#= PlaylistID #/#= PlaylistImg #"
                                        class="img-responsive" alt="" onerror='this.src="Media/Images/error-image.png"'
                                        width="100%" height="auto">
                                    <div class="caption">
                                        <h6 class="caption-title">
                                            #= PlaylistTitle #
                                        </h6>
                                        <p>
                                            #= PlaylistDesc #
                                        </p>
                                    </div>
                                </div>

                            </ItemTemplate>
                            <EmptyDataTemplate>
                                No Slide to display.
                            </EmptyDataTemplate>
                        </DataBinding>
                        <ClientEvents OnCommand="cplOnListViewCommand" OnDataBinding="cplOnListViewDataBinding">
                        </ClientEvents>
                    </ClientSettings>
                </rad:RadListView>
                <div id="sliderSettings" class="col-md-12 margin-top-10px no-paddings" style="height: 60vh;
                    overflow: auto;">
                    <h5>
                        <strong>SLIDER PLAY SETTINGS:</strong>
                    </h5>
                    <table class="padding-md" width="50%">
                        <tr>
                            <td>
                                Max images / videos
                            </td>
                            <td>
                                <rad:RadDropDownList ID="ddlMaxItem" runat="server" CssClass="soft bg-white" Width="50px">
                                </rad:RadDropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Image play interval
                            </td>
                            <td>
                                <rad:RadDropDownList ID="ddlInterval" runat="server" CssClass="soft bg-white" Width="50px">
                                </rad:RadDropDownList>
                                seconds
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Play video
                            </td>
                            <td>
                                <input type="checkbox" id="cbAutoplay" runat="server" class="js-switch-transparent " />
                                Auto-play &nbsp;&nbsp;
                                <input type="checkbox" id="cbOnclick" runat="server" class="js-switch-transparent " />
                                On click
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Video slide after
                            </td>
                            <td>
                                <rad:RadDropDownList ID="ddlSlideAfter" runat="server" CssClass="soft bg-white" Width="50px">
                                </rad:RadDropDownList>
                                seconds &nbsp;&nbsp;
                                <input type="checkbox" id="cbVideoOnclick" runat="server" class="js-switch-transparent " />
                                On click
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="label1" Text=" <%$ Resources:LocalizedResource, Loopdirection%>" ></asp:Label>
                            </td>
                            <td>
                                <div class="js-direction-selector">
                                    <i id="arrowLeft" runat="server" class="fa fa-arrow-circle-left pointer" aria-hidden="true"
                                        onclick="setDirectionActive(1);" clientidmode="Static"></i>&nbsp;&nbsp;&nbsp;
                                    <i id="arrowRight" runat="server" class="fa fa-arrow-circle-right pointer" aria-hidden="true"
                                        onclick="setDirectionActive(2);" clientidmode="Static"></i>&nbsp;&nbsp;&nbsp;
                                    <i id="arrowUp" runat="server" class="fa fa-arrow-circle-up pointer" aria-hidden="true"
                                        onclick="setDirectionActive(3);" clientidmode="Static"></i>&nbsp;&nbsp;&nbsp;
                                    <i id="arrowDown" runat="server" class="fa fa-arrow-circle-down pointer" aria-hidden="true"
                                        onclick="setDirectionActive(4);" clientidmode="Static"></i>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <h5>
                        <strong>SLIDER PLAYLIST:</strong>
                    </h5>
                    <table class="padding-md" width="60%">
                        <tr>
                            <td>
                                <p class="teal bold">
                                    <asp:Label runat="server" ID="label2" Text=" <%$ Resources:LocalizedResource, CurrentPlaylist%>" ></asp:Label>:
                                </p>
                            </td>
                            <td>
                                <strong><asp:Label ID="lblCurrentPlaylist" Text="No Playlist has been set." runat="server"></asp:Label></strong>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p class="teal bold">
                                    <asp:Label runat="server" ID="label3" Text=" <%$ Resources:LocalizedResource, OtherPlaylists%>" ></asp:Label>:
                                </p>
                            </td>
                        </tr>
                    </table>
                    <div id="slideListContainer" height="100%" runat="server">
                        <rad:RadListView ID="lvOtherPlaylist" runat="server" AllowPaging="false" PageSize="999">
                            <LayoutTemplate>
                                <div id="otherPlaylist">
                                </div>
                            </LayoutTemplate>
                            <ClientSettings DataBinding-DataService-EnableCaching="true">
                                <DataBinding ItemPlaceHolderID="otherPlaylist">
                                    <ItemTemplate>
                                        <div class="col-md-2" style="margin: 5px 0;">
                                            #= PlaylistName #
                                        </div>
                                        <div class="col-md-8" style="margin: 5px 0;">
                                            <button id="btnPreview" class="btn btn-md btn-teal btn-oval">
                                                Preview
                                            </button>
                                             &nbsp;&nbsp;&nbsp;
                                            <button id="btnPlay" class="btn btn-md btn-teal btn-oval" onclick='btnPlayClick("#= ID #", "#= PlaylistName #"); return false;'>
                                                Play
                                            </button>
                                            &nbsp;&nbsp;&nbsp;
                                            <button id="btnRemovePl" class="btn btn-md btn-teal btn-oval" onclick='removePlaylist("#= ID #", " #= PlaylistName # "); return false'>
                                                Remove
                                            </button>
                                        </div>
                                    </ItemTemplate>
                                    <EmptyDataTemplate>
                                        No other playlist
                                    </EmptyDataTemplate>
                                </DataBinding>
                                <ClientEvents OnCommand="plOnListViewCommand" OnDataBinding="plOnListViewDataBinding">
                                </ClientEvents>
                            </ClientSettings>
                        </rad:RadListView>
                    </div>
                    <asp:Button ID="btnReloadSlideList" runat="server" OnClick="btnReloadSlideList_Click"
                        CssClass="display-none" />
                    <asp:LinkButton ID="LinkButton1" Text="" runat="server" CssClass="btn btn-md btn-default"
                        OnClientClick="openPlaylistCreator(); return false;" Style="padding: 0px 15px;
                        display: inline-flex; align-items: center;">
                        <i class="fa fa-plus-circle" aria-hidden="true" style="font-size:30px;color:Gray"></i>&nbsp;
                        <asp:Label runat="server" ID="label4" Text=" <%$ Resources:LocalizedResource, CreateNew%>" ></asp:Label>
                    </asp:LinkButton>
                </div>
                <div class="row bottom-center">
                    <asp:Button ID="btnCloseSlider" Text="<%$ Resources:LocalizedResource, Cancel%>" runat="server" CssClass="btn btn-md btn-teal btn-flat"
                        OnClientClick="closeSliderSettings(); return false" />
                    <asp:Button ID="btnSaveSlider" Text="Save Changes" runat="server" CssClass="btn btn-md btn-teal btn-flat"
                        OnClientClick="saveSliderSettings(); return false;" />
                </div>
            </ContentTemplate>
        </rad:RadWindow>
    </Windows>
    <Windows>
        <rad:RadWindow ID="rwPlaylistCreator" runat="server" Title="" CssClass="tc-radwindow-3 font-size-normal"
            Width="985px" AutoSizeBehaviors="Height" Height="500px">
            <ContentTemplate>
                <div class="col-md-6">
                    <div class="col-md-6 form-group">
                        <asp:Label ID="lblPlayingPlaylist" runat="server"><strong>Playlist Name:</strong></asp:Label>
                        <asp:RequiredFieldValidator ID="rfvtxtPlaylistName" runat="server" ErrorMessage="*"
                            ControlToValidate="txtPlaylistName" SetFocusOnError="True" Display="Dynamic"
                            ValidationGroup="savePlaylist" ForeColor="Red" />
                        <asp:TextBox ID="txtPlaylistName" runat="server" CssClass="form-control input-md"
                            Height="27px" />
                    </div>
                </div>
                <asp:Panel ID="slideCreatorSlider" runat="server" CssClass="tc-slick-carousel-selected-images col-md-12 no-paddings"
                    ClientIDMode="Static" Width="90%" Style="height: 25vh; position: relative; left: 50%;
                    transform: translate(-50%);">
                    <center>
                        <strong>Upload some images.</strong></center>
                </asp:Panel>
                <div class="col-md-6 margin-top-10px">
                    <asp:Button ID="Button7" Text="Upload" runat="server" CssClass="btn btn-md btn-teal btn-flat pull-left"
                        OnClientClick="openUploader(); return false;" />
                    <rad:RadAsyncUpload ID="slideUploader" runat="server" MultipleFileSelection="Automatic"
                        ViewStateMode="Enabled" MaxFileInputsCount="10" MaxFileSize="20000000" AllowedFileExtensions="jpeg,jpg,gif,png"
                        TemporaryFileExpiration="00:10:00" OnClientFilesUploaded="RadUploader_ClientFilesUploaded" OnClientFilesSelected="RadUploader_filesSelected"
                        CssClass="display-none">
                    </rad:RadAsyncUpload>
                </div>
                <asp:Button ID="refreshPreview" runat="server" OnClick="refreshPreview_Clicked" CssClass="display-none" />
                <div class="col-md-3 no-paddings image-container display-none">
                    <div class="overlay actions">
                        <i class="fa fa-ellipsis-v medium" aria-hidden="true"></i><i class="fa fa-pencil-square-o medium"
                            aria-hidden="true"></i><i class="fa fa-times medium" aria-hidden="true"></i>
                    </div>
                    <asp:Image ID="Image15" ImageUrl="http://via.placeholder.com/200x188/aaaaaa" runat="server" />
                    <div class="caption">
                        <h6 class="caption-title">
                            November 2016
                        </h6>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                        </p>
                    </div>
                </div>
                <%-- <div class="allSlideList col-md-12" style="height: 26vh; width: 55vw; margin: 0 20px;">
                    <asp:Repeater ID="rptSlides" runat="server">
                        <ItemTemplate>
                            <div class="slide-container" onclick="setItemActive(this);">
                                <div class="overlay">
                                    <i class="fa fa-check fa-md fa-white" aria-hidden="true"></i>
                                </div>
                                <img class="img-responsive lazy-items width-fill no-paddings no-margin pointer" data-lazy="Media/Uploads/Sliders/<%#Eval("PlaylistID")%>/<%#Eval("PlaylistImg")%>"
                                    onerror='this.src="Media/Images/error-image.png"' style="height: 13vh;" />
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>--%>
                <div class="row bottom-center">
                    <asp:Button ID="Button18" Text="Cancel" runat="server" CssClass="btn btn-md btn-teal btn-flat"
                        OnClientClick="closePlaylistCreator(); return false" />
                    <asp:Button ID="Button19" Text="Save Changes" runat="server" CssClass="btn btn-md btn-teal btn-flat"
                        ValidationGroup="savePlaylist" OnClientClick="savePlaylistCreator(); return false;" />
                    <%--OnClick="btnPlaylistCreatorSave_Click"--%>
                </div>
            </ContentTemplate>
        </rad:RadWindow>
    </Windows>
</rad:RadWindowManager>
<rad:RadAjaxManagerProxy ID="RadAjaxManager1" runat="server">
    <AjaxSettings>
        <rad:AjaxSetting AjaxControlID="refreshPreview">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="refreshPreview" LoadingPanelID="localLoadingPanel" />
            </UpdatedControls>
        </rad:AjaxSetting>
    </AjaxSettings>
</rad:RadAjaxManagerProxy>
<rad:RadAjaxLoadingPanel ID="localLoadingPanel" runat="server" CssClass="Loading2"
    Transparency="25" />
<rad:RadAjaxLoadingPanel ID="fullPageLoading" runat="server" CssClass="Loading" />
<asp:Panel ID="pnlCatalogSettings" runat="server" CssClass="region">
    <div class="wrapper-main-content">
        <div class="inner-content col-md-6">
            <h5>
                <strong>IMAGE / VIDEO SLIDER </strong>
            </h5>
            <p>
                These are the images / videos that appear and loop on the Sign-in / Home Page (signed
                out state) and User's Home Page (signed in state)
            </p>
        </div>
        <div class="inner-content col-md-5">
            <h5>
                <p class="teal bold">
                    <asp:Label runat="server" ID="label5" Text=" <%$ Resources:LocalizedResource, CurrentPlaylist%>" ></asp:Label>:
                    <asp:Label ID="lblPrevPlName" Text="No Playlist has been set." runat="server" ForeColor="#636363" />
                </p>
            </h5>
            <div id="main-content" class="bottom-margin-60vh" style="width: 32vw;">
                <div style="    height: -webkit-fill-available;
    width: 100%;
    border: 1px solid gray;
    position: relative;
    top: 0;
    left: 0;
    right: 0;" id="playlistPlaceholder">
                    
                </div>
                <rad:RadListView ID="lvPlaylistPrev" runat="server" AllowPaging="false" PageSize="999">
                    <LayoutTemplate>
                        <div id="imageSlider" class="tc-slick-carousel-preview width-fill hover-pointer"
                            style="min-height: 50vh;position:absolute;top:0;">
                        </div>
                    </LayoutTemplate>
                    <ClientSettings DataBinding-DataService-EnableCaching="true">
                        <DataBinding ItemPlaceHolderID="imageSlider">
                            <ItemTemplate>
                                <div>
                                <img data-lazy="Media/Uploads/Sliders/#= PlaylistID #/#= PlaylistImg #"
                                    class="width-fill" alt="" onerror='this.src="Media/Images/error-image.png"' />
                                </div>
                            </ItemTemplate>
                            <EmptyDataTemplate>
                                No Slide to display.
                            </EmptyDataTemplate>
                        </DataBinding>
                        <ClientEvents OnCommand="pplOnListViewCommand" OnDataBinding="pplOnListViewDataBinding">
                        </ClientEvents>
                    </ClientSettings>
                </rad:RadListView>
                <rad:RadListView ID="lvPlaylistPrevThumbnail" runat="server" AllowPaging="false"
                    PageSize="999">
                    <LayoutTemplate>
                        <div id="imageSliderThumbnail" class="tc-slick-carousel-thumbnails width-fill">
                        </div>
                    </LayoutTemplate>
                    <ClientSettings DataBinding-DataService-EnableCaching="true">
                        <DataBinding ItemPlaceHolderID="imageSliderThumbnail">
                            <ItemTemplate>
                                <div>
                                    <img data-lazy="Media/Uploads/Sliders/#= PlaylistID #/#= PlaylistImg #"
                                    class="no-margin" alt="" onerror='this.src="Media/Images/error-image.png"' width="128px"
                                    height="82px" />
                                </div>

                            </ItemTemplate>
                            <EmptyDataTemplate>
                                        No Slide to display.
                            </EmptyDataTemplate>
                        </DataBinding>
                        <ClientEvents OnCommand="tpplOnListViewCommand" OnDataBinding="tpplOnListViewDataBinding">
                        </ClientEvents>
                    </ClientSettings>
                </rad:RadListView>
                <p class="">
                    <asp:Label runat="server" ID="label6" Text=" <%$ Resources:LocalizedResource, Doubleclickontheslidertoeditandconfiguretheplaysettings%>" ></asp:Label>
                </p>
            </div>
        </div>
    </div>
</asp:Panel>
<asp:HiddenField ID="hfSelectedPlaylistID" runat="server" />
<asp:HiddenField ID="hfSelectedPlaylistName" runat="server" />
<asp:HiddenField ID="hfLoopDirection" runat="server" />
<asp:HiddenField ID="hfNoImageUploaded" runat="server" value="<%$ Resources:LocalizedResource, NoImageUploaded%>"/>
<asp:HiddenField ID="hfError" runat="server" value="<%$ Resources:LocalizedResource, Error%>" />

