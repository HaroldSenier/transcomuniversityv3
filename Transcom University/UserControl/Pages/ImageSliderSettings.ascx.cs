﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using TranscomUniversityV3Model;
using System.Security.Cryptography;
using System.Text;
using System.Collections;
using System.IO;

public partial class UserControl_Pages_ImageSliderSettings : System.Web.UI.UserControl
{
    private int CURRENT_PLAYLIST_ID;
    private dynamic currentPlaylistData;

    protected void Page_Load(object sender, EventArgs e)
    {
        //slideUploader.TemporaryFolder = "Media/Uploads/Slide/Temporary/" + DataHelper.GetCurrentUserCIM();

       

        //DeleteSliderTempFolder();
        Session["SliderUploadedImages"] = null;
        if (!Page.IsPostBack)
        {
            loadDropDownDefault();
            loadAllSlide();
            DeleteSliderTempFolder();
            loadSliderSetting();

            TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();
            //currentPlaylistData = DataHelper.getCurrentPlaylist();
            //load slider in image slider preview with thumbnails
            //rptImageSlider.DataSource = currentPlaylistData;
            //rptImageSlider.DataBind();

            //rptImageSliderThumbnails.DataSource = currentPlaylistData;
            //rptImageSliderThumbnails.DataBind();
        }
    }

    private void loadDropDownDefault()
    {
        //20 items
        for (int i = 1; i <= 20; i++)
        {
            DropDownListItem item = new DropDownListItem(i.ToString(), i.ToString());
            ddlMaxItem.Items.Add(item);
            DropDownListItem item2 = new DropDownListItem(i.ToString(), i.ToString());
        }
        //60 items
        for (int i = 1; i <= 60; i++)
        {
            DropDownListItem item = new DropDownListItem(i.ToString(), i.ToString());
            ddlInterval.Items.Add(item);
            DropDownListItem item2 = new DropDownListItem(i.ToString(), i.ToString());
            ddlSlideAfter.Items.Add(item2);
        }
    }

    private void loadAllSlide()
    {
        //try
        //{
        //    using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        //    {
        //        var playlist_slides = db.vw_TranscomUniversity_GetAllPlaylistSlides.ToList();


        //        rptSlides.DataSource = playlist_slides;
        //        rptSlides.DataBind();
        //    }
        //}
        //catch (Exception)
        //{
        //    rptSlides.DataSource = null;
        //    rptSlides.DataBind();
        //}

    }

    protected void btnPlaylistCreatorSave_Click(object sender, EventArgs e)
    {
        //save to target folder

    }

    protected void refreshPreview_Clicked(object sender, EventArgs e)
    {
        try
        {
            ArrayList UploadedSliderImages = new ArrayList();
            string uploadFolder;
            string currentuser = DataHelper.GetCurrentUserCIM();

            if (HttpContext.Current.Request.Url.Host == "localhost")
                uploadFolder = Request.PhysicalApplicationPath + "Media\\Uploads\\Slider\\Temporary\\" + currentuser + "\\";
            else
                uploadFolder = HttpRuntime.AppDomainAppPath + "Media\\Uploads\\Slider\\Temporary\\" + currentuser + "\\";

            string uploadedImagesSrc = "";
            string uploadedImagesFileName = "";
            foreach (UploadedFile file in slideUploader.UploadedFiles)
            {
                if (file.ContentType == "image/jpeg" || file.ContentType == "image/png" || file.ContentType == "image/jpg")
                {
                    string tempImage = uploadFolder + file.FileName;
                    try
                    {
                        file.SaveAs(tempImage, true);
                    }
                    catch (DirectoryNotFoundException dnfe)
                    {

                        string directoryPath = Server.MapPath(string.Format("~/{0}/", "Media/Uploads/Slider/Temporary/" + currentuser + "/"));

                        Directory.CreateDirectory(directoryPath);

                        file.SaveAs(tempImage, true);
                    }
                    string url = "Media/Uploads/Slider/Temporary/" + currentuser + "/" + file.FileName;

                    selectedSlideData data = new selectedSlideData();
                    data.filename = file.FileName;
                    data.src = url;
                    uploadedImagesFileName += file.FileName + ",";
                    uploadedImagesSrc += url + ",";
                    UploadedSliderImages.Add(data);
                }
                
            }
            slideUploader.UploadedFiles.Clear();
            Session["SliderUploadedImages"] = UploadedSliderImages;


            ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", Utils.callClientScript("loadUploadedImages", uploadedImagesFileName, uploadedImagesSrc), true);

        }
        catch (FileNotFoundException fnfe)
        {
            rwmImageSlider.RadAlert("Source File was not found. Error:" + fnfe.Message, 330, 180, "File Not Found", "");
        }
    }

    private void DeleteSliderTempFolder()
    {
        string currentuser = DataHelper.GetCurrentUserCIM();

        string directoryPath = Server.MapPath(string.Format("~/{0}/", "Media/Uploads/Slider/Temporary/" + currentuser + "/"));

        if (Directory.Exists(directoryPath))
            Directory.Delete(directoryPath, true);

        Directory.CreateDirectory(directoryPath);

    }

    private void loadSliderSetting()
    {
        try
        {
            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                var settings = DataHelper.getSliderSettings();
                if (settings != null)
                {
                    ddlMaxItem.SelectedIndex = ddlMaxItem.FindItemByValue(settings.MaxItemCount.ToString()).Index;
                    ddlInterval.SelectedIndex = ddlInterval.FindItemByValue(settings.PlayInterval.ToString()).Index;

                    cbAutoplay.Checked = settings.IsAutoplay;
                    cbOnclick.Checked = settings.IsOnClick;

                    ddlSlideAfter.SelectedIndex = ddlSlideAfter.FindItemByValue(settings.VideoDelay.ToString()).Index;
                    cbVideoOnclick.Checked = settings.IsVideoOnClick;

                    hfLoopDirection.Value = settings.SlideDirection.ToString();
                    if (settings.SlideDirection == 1)
                    {
                        arrowLeft.Attributes.Add("class", "fa fa-arrow-circle-left pointer active");
                    }
                    else if (settings.SlideDirection == 2)
                    {
                        arrowRight.Attributes.Add("class", "fa fa-arrow-circle-right pointer active");
                    }
                    else if (settings.SlideDirection == 3)
                    {
                        arrowUp.Attributes.Add("class", "fa fa-arrow-circle-up pointer active");
                    }
                    else
                    {
                        arrowDown.Attributes.Add("class", "fa fa-arrow-circle-down pointer active");
                    }

                    CURRENT_PLAYLIST_ID = settings.SelectedPlaylistID;
                    var playlistDetails = DataHelper.getPlayListDetail(CURRENT_PLAYLIST_ID);
                    lblCurrentPlaylist.Text = playlistDetails.PlaylistName;
                    lblSelectedPlaylistName.Text = playlistDetails.PlaylistName;
                    lblCurrentPlaylist.Text = playlistDetails.PlaylistName;
                    hfSelectedPlaylistID.Value = Utils.Encrypt(CURRENT_PLAYLIST_ID);
                    hfSelectedPlaylistName.Value = playlistDetails.PlaylistName;
                    lblPrevPlName.Text = playlistDetails.PlaylistName;
                }
               
                //var currentPlaylistDS = db.tbl_trancomuniversity_PlaylistSlides
                //                        .Where(p => p.PlaylistID == CURRENT_PLAYLIST_ID)
                //                        .ToList();
                //rptCurrentPlaylist.DataSource = currentPlaylistDS;
                //rptCurrentPlaylist.DataBind();

                //load all available playlist
                //loadOtherPlaylist();
                
            }
        }
        catch (Exception e)
        {
            rwmImageSlider.RadAlert("Error " + e.Message, 330, 180, "Something went wrong", "");
        }
    }

    public void loadOtherPlaylist() 
    {
        //txtPlaylistName.Text = "";
        //TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();
        //var playlistsList = db.tbl_trancomuniversity_Playlist
        //                   .Where(p => p.Inactive == false)
        //                   .AsEnumerable()
        //                   .Select(p => new
        //                   {
        //                       ID = Utils.Encrypt(p.ID),
        //                       p.PlaylistName,
        //                   })
        //                   .ToList();
        //rptSlideList.DataSource = playlistsList;
        //rptSlideList.DataBind();

        //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", Utils.callClientScript("hideSliderSettingsLoading"), true);
        
    }

    protected void btnReloadSlideList_Click(object sender, EventArgs e)
    {
        loadOtherPlaylist();
    }

    protected void lbtnRemovePlaylist_Click(object sender, CommandEventArgs e)
    {
        TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();

        int playlistID = Convert.ToInt32(e.CommandArgument.ToString());
        db.pr_transcomuniversity_DeletePlaylist(Convert.ToInt32(playlistID));
        db.SaveChanges();

        loadOtherPlaylist();
    }
}

public class selectedSlideData
{
    public string src { get; set; }
    public string filename { get; set; }
}