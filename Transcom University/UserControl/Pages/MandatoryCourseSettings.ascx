﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MandatoryCourseSettings.ascx.cs"
    Inherits="UserControl_Pages_MandatoryCourseSettings" %>
<rad:RadScriptBlock ID="rsbMandatory" runat="server">
    <script type="text/javascript">

        //vars
        var COURSECATALOG_LISTVIEW;
        var COURSECATALOG_GRIDVIEW;

        var COURSELIBRARY_LISTVIEW;
        var COURSELIBRARY_GRIDVIEW;

        var COURSELIBRARY_DATA;
        var COURSELIBRARY_COUNT;
        var COURSELIBRARY_PAGESIZE;
        var COURSELIBRARY_PAGEINDEX;

        var COURSECATALOG_DATA;
        var COURSECATALOG_COUNT;
        var COURSECATALOG_PAGESIZE;
        var COURSECATALOG_PAGEINDEX;

        var SELECTEDCOURSES_TOADD = [];

        var ISCOURSECATALOGLISTVIEW = false;
        var ISCOURSELIBRARYLISTVIEW = false;

        var COURSECATALOG_LIST_CACHE;
        var COURSECATALOG_GRID_CACHE;

        var SETTINGSCHANGED;
       

        function pageLoad() {
            initializeSwitchery();
            initializeAccordion();

            SETTINGSCHANGED = false;
            highlightSelectedSubmenu();
        }

        function initializeSwitchery() {
            var elems2 = Array.prototype.slice.call(document.querySelectorAll('.js-switch-transparent'));

            elems2.forEach(function (html) {
                new Switchery(html, {
                    size: 'small',
                    color: 'none',
                    secondaryColor: 'none', //off
                    jackColor: '#09A8A4', //on
                    jackSecondaryColor: '#bbb' //off
                });
            });
            $(".js-switch-transparent").addClass("border-gray");
        }

        function initializeAccordion() {
            $(function () {
                $("#MCListAccordion")
                .accordion({
                    header: "> div > div"
                })
                .sortable({
                    handle: "div",
                    stop: function (event, ui) {
                        ui.item.children("div").triggerHandler("focusout");
                        var i = 1;
                        $("#MCListAccordion").find('input[name*=hfIndex]').each(function (index) {
                            var newIndex = $(this).val(i);
                            i++;
                        });
                    }
                });
            });

            $(function () {
                $("#MCGridAccordion")
                .accordion({
                    header: "> tr > td"
                })
                .sortable({
                    handle: "td",
                    stop: function (event, ui) {
                        ui.item.children("td").triggerHandler("focusout");
                        var i = 1;
                        $("#MCGridAccordion").find('input[name*=hfIndex]').each(function (index) {
                            var newIndex = $(this).val(i);
                            i++;
                        });
                    }
                });

            });
        }

        function openCourseCatalog() {
            if (SETTINGSCHANGED == true || COURSECATALOG_COUNT == 0) {

                COURSECATALOG_LISTVIEW.page(0);
                COURSECATALOG_LISTVIEW.rebind();
                SETTINGSCHANGED = false;
            }

            initializeCourseCatalogListView();
            $find("<%= rwPreviewCourseCatalog.ClientID %>").show();
        }

        function closeCourseCatalog() {
            $find("<%= rwPreviewCourseCatalog.ClientID %>").close();
        }

        function initializeCourseCatalogListView() {
            COURSECATALOG_PAGESIZE = 12;
            COURSECATALOG_LISTVIEW = $find("<%= lvCourseCatalog.ClientID%>");
            COURSECATALOG_LISTVIEW.rebind();
            COURSECATALOG_LISTVIEW.set_pageSize(COURSECATALOG_PAGESIZE);
            COURSECATALOG_LISTVIEW.page(0);

            $(".mcPager .pagePrev").click(function (e) {
                COURSECATALOG_LISTVIEW.page(COURSECATALOG_LISTVIEW.get_currentPageIndex() - 1);
            });

            $(".mcPager .pageNext").click(function (e) {
                COURSECATALOG_LISTVIEW.page(COURSECATALOG_LISTVIEW.get_currentPageIndex() + 1);
            });
        }

        function bindCourseCatalog() {
            showLoading("pnlCourseCatalogContainer");
            COURSECATALOG_LISTVIEW = $find("<%= lvCourseCatalog.ClientID %>");
            COURSECATALOG_GRIDVIEW = $find("<%= gvCourseCatalog.ClientID %>");
            var startRowIndex = COURSECATALOG_LISTVIEW.get_currentPageIndex() * COURSECATALOG_LISTVIEW.get_pageSize(),
                maximumRows = COURSECATALOG_LISTVIEW.get_pageSize(),
                sortExpression = COURSECATALOG_LISTVIEW.get_sortExpressions().toLinq();

            $.ajax({
                type: "POST",
                data: "{startRowIndex: " + startRowIndex + ", maximumRows: " + maximumRows + ", sortExpression: '" + sortExpression + "'}",
                contentType: "application/json; charset=utf-8",
                url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "GetMandatoryCourseList",
                dataType: 'json',
                success: function (res) {
                    COURSECATALOG_DATA = res.d.data;
                    COURSECATALOG_COUNT = res.d.count;

                    COURSECATALOG_PAGESIZE = COURSECATALOG_LISTVIEW.get_pageSize();
                    COURSECATALOG_LISTVIEW.set_virtualItemCount(COURSECATALOG_COUNT);
                    COURSECATALOG_LISTVIEW.set_dataSource(COURSECATALOG_DATA);
                    COURSECATALOG_LISTVIEW.dataBind();

                    COURSECATALOG_GRIDVIEW.set_virtualItemCount(COURSECATALOG_COUNT);
                    COURSECATALOG_GRIDVIEW.set_dataSource(COURSECATALOG_DATA);
                    COURSECATALOG_GRIDVIEW.dataBind();

                    if (COURSECATALOG_COUNT > 0)
                        $(".mcPager").removeClass("display-none");

                    courseCatalogGeneratePageNumbers();
                    setCourseCatalogPage();
                    //remove delete classes to refresh styles after loading items
                    removeDeleteClasses();
                    hideLoading();

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var code = XMLHttpRequest.status;
                    if (code == "401")
                        window.location.reload();
                    else
                        alert(XMLHttpRequest.responseText);
                }
            });
        }

        function ccOnListViewDataBinding(sender, args) {
            sender.set_selectedIndexes([]);
        }

        function ccOnListViewCommand(sender, args) {
            args.set_cancel(true);
            bindCourseCatalog();
        }

        function saveCourseCatalogOrder() {
            var newIndexes = {};
            var newData = [];
            var newItemsPerPage = $find("<%= ddlCourseCatalogPager.ClientID %>").get_selectedItem().get_value();
            try {
                $("#ccMainWrapper").find(".item.move .js-hiddenfields").each(function (index, value) {
                    var txtIndex = $(value).find("input[name*=hfIndex]");
                    var txtId = $(value).find("input[name*=hfCourseID]");
                    var index = $(txtIndex).val();
                    var courseId = $(txtId).val();
                    var data = {
                        "index": index,
                        "courseid": courseId
                    }
                    newData.push(data);
                });
            } catch (e) {
                newData = [];
                radalert("Something went wrong. Please try again <br> Error: " + e.Message, 330, 180, "Error", "");
            }

            newIndexes = { "courses": newData, "items": newItemsPerPage };

            if (newData.length > 0) {
                $.ajax({
                    type: "POST",
                    data: "{ jsonData: '" + JSON.stringify(newIndexes) + "'}",
                    contentType: "application/json; charset=utf-8",
                    url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "UpdateMandatoryCourseOrder",
                    dataType: 'json',
                    success: function (res) {
                        var result = res.d.isSuccess;
                        var message = res.d.message;
                        if (result == true) {
                            var RadAlertCourseSuccessMessage = $("#<%=hdnRadAlertCourseSuccessMessage.ClientID%>").val();
                            var RadAlertCourseSuccessConfirm = $("#<%=hdnRadAlertCourseSuccessConfirm.ClientID%>").val();
                            radalert(RadAlertCourseSuccessMessage, 330, 180, RadAlertCourseSuccessConfirm, "");
                            bindCourseCatalog();
                            COURSECATALOG_LIST_CACHE = null;
                            COURSECATALOG_GRID_CACHE = null;
                        } else {
                            radalert("Something went wrong. Please try again <br> Error: " + message, 330, 180, "Error", "");
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        var code = XMLHttpRequest.status;
                        if (code == "401")
                            window.location.reload();
                        else
                            alert(XMLHttpRequest.responseText);
                    }
                });
            } else {
                var RadAlertErrorMessage = $("#<%=hdnRadAlertErrorMessage.ClientID%>").val();
                var RadAlertErrorConfirm = $("#<%=hdnRadAlertErrorConfirm.ClientID%>").val();
                radalert(RadAlertErrorMessage, 330, 180, RadAlertErrorConfirm, "");
            }
        }

        function deleteSelectedCourse() {
            var RadConfirmMessage = $("#<%=hdnRadConfirmMessage.ClientID%>").val();
            var RadConfirmConfirm = $("#<%=hdnRadConfirmConfirm.ClientID%>").val();
            radconfirm(RadConfirmMessage, confirmDeleteCourseCatalogFn, 330, 180, "", RadConfirmConfirm);

            function confirmDeleteCourseCatalogFn(arg) {
                if (arg) {
                    var deleteCourseData = {};
                    var newData = [];
                    try {
                        if (!ISCOURSECATALOGLISTVIEW) {
                            $("#ccMainWrapper").find(".item.delete.selected").each(function (index, value) {
                                var txtId = $(value).find("input[name*=hfCourseID]");
                                var courseId = $(txtId).val();
                                newData.push(courseId);
                            });
                        } else {
                            $("#ccGridWrapper").find(".item.delete.selected").each(function (index, value) {
                                var txtId = $(value).find("input[name*=hfCourseID]");
                                var courseId = $(txtId).val();
                                newData.push(courseId);
                            });
                        }

                        deleteCourseData = { "courses": newData };
                        if (newData.length > 0) {
                            $.ajax({
                                type: "POST",
                                data: "{ selectedCourses: '" + JSON.stringify(deleteCourseData) + "'}",
                                contentType: "application/json; charset=utf-8",
                                url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "DeleteMandatoryCourse",
                                dataType: 'json',
                                success: function (res) {
                                    var result = res.d.isSuccess;
                                    var message = res.d.message;
                                    if (result == true) {
                                        var RadAlertCourseDeleteMessage = $("#<%=hdnRadAlertCourseDeleteMessage.ClientID%>").val();
                                        var RadAlertCourseDeleteConfirm = $("#<%=hdnRadAlertCourseDeleteConfirm.ClientID%>").val();
                                        radalert(RadAlertCourseDeleteMessage, 330, 180, RadAlertCourseDeleteConfirm, "");
                                        bindCourseCatalog();
                                    } else {
                                        radalert("Something went wrong. Please try again <br> Error: " + message, 330, 180, "Error", "");
                                    }
                                },
                                error: function (XMLHttpRequest, textStatus, errorThrown) {
                                    var code = XMLHttpRequest.status;
                                    if (code == "401")
                                        window.location.reload();
                                    else
                                        alert(XMLHttpRequest.responseText);
                                }
                            });
                        } else {
                            var RadAlertErrorMessage = $("#<%=hdnRadAlertErrorMessage.ClientID%>").val();
                            var RadAlertErrorConfirm = $("#<%=hdnRadAlertErrorConfirm.ClientID%>").val();
                            radalert(RadAlertErrorMessage, 330, 180, RadAlertErrorConfirm, "");
                        }
                    } catch (Error) {
                        newData = [];
                        console.log(Error);
                        radalert("Something went wrong. Error : " + Error.Message, 330, 180, "Error", "");
                    }
                } else {
                    return false;
                }
            }
        }

        function DefaultViewChanged(el) {
            var id = $(el).attr("id");
            if (id == "cbGridView")
                $('#cbListView').prop('checked', false);
            else
                $('#cbGridView').prop('checked', false);
        }

        function setCourseCatalogPage() {
            COURSECATALOG_PAGEINDEX = COURSECATALOG_LISTVIEW.get_currentPageIndex();
            COURSECATALOG_COUNT = COURSECATALOG_LISTVIEW.get_virtualItemCount();
            COURSECATALOG_PAGESIZE = COURSECATALOG_LISTVIEW.get_pageSize();
            var myPage = COURSECATALOG_PAGEINDEX + 1;
            var startNum = myPage == 1 ? 1 : ((myPage * COURSECATALOG_PAGESIZE) > COURSECATALOG_COUNT ? COURSECATALOG_COUNT : (myPage * COURSECATALOG_PAGESIZE));
            $(".js-page-numbers.coursecatalog ul li").removeClass("selected-page");
            $(".js-page-numbers.coursecatalog ul li:nth-child(" + myPage + ")").addClass("selected-page");
            var itemCount = COURSECATALOG_PAGESIZE * (myPage) <= COURSECATALOG_COUNT ? (COURSECATALOG_PAGESIZE * (myPage)) : ((COURSECATALOG_PAGEINDEX * COURSECATALOG_PAGESIZE) + (COURSECATALOG_COUNT % COURSECATALOG_PAGESIZE));
            var itemCountIndex = myPage == 1 ? (COURSECATALOG_COUNT == 0 ? 0 : 1) : ((COURSECATALOG_PAGEINDEX * COURSECATALOG_PAGESIZE) > COURSECATALOG_COUNT ? COURSECATALOG_COUNT : (COURSECATALOG_PAGEINDEX * COURSECATALOG_PAGESIZE) + 1);
            $(".lblCourseCatalogShowNumber").html("Showing " + itemCountIndex + " - " + itemCount + " of " + COURSECATALOG_COUNT);
        }

        function changeCourseCatalogPageSize(sender, args) {
            COURSECATALOG_LISTVIEW = $find("<%= lvCourseCatalog.ClientID %>");
            var ps = $find("<%= ddlCourseCatalogPager.ClientID %>").get_selectedItem().get_value();
            COURSECATALOG_PAGESIZE = ps;
            COURSECATALOG_PAGEINDEX = 0;

            var defaultPageSize = COURSECATALOG_LISTVIEW.get_pageSize();
            if (!$.isNumeric(ps)) {
                COURSECATALOG_PAGESIZE = defaultPageSize;
            }
            COURSECATALOG_LISTVIEW.set_pageSize(COURSECATALOG_PAGESIZE);
            COURSECATALOG_LISTVIEW.page(COURSECATALOG_PAGEINDEX);
            COURSECATALOG_LISTVIEW.rebind();
        }

        function deleteCourseCatalogItem(sender) {
             //remove move
            //add delete
            var btnDelete = $("#btnDelete");
            if (!(btnDelete.hasClass("deleting"))) {
                addDeleteClasses();
            } else {
                removeDeleteClasses();
            }
        }

        function addDeleteClasses() {
            var btnDelete = $("#btnDelete");
            btnDelete.addClass("deleting");
            $("#MCListAccordion").sortable("disable");

            $("#MCListAccordion").find(".item").each(function (index, value) {
                $(value).removeClass("move");
                $(value).addClass("delete");
            });
            $("#MCGridAccordion").find(".item").each(function (index, value) {
                $(value).removeClass("move");
                $(value).addClass("delete");
            });
            $("#btnSaveChanges").addClass("display-none");
            $("#btnDeleteCourse").removeClass("display-none");
        }

        function removeDeleteClasses() {
            var btnDelete = $("#btnDelete");
            $("#btnSaveChanges").removeClass("display-none");
            $("#btnDeleteCourse").addClass("display-none");
            $("#MCListAccordion").find(".item").each(function (index, value) {
                $(value).addClass("move");
                $(value).removeClass("delete");
                $(value).removeClass("selected");
            });
            $("#MCGridAccordion").find(".item").each(function (index, value) {
                $(value).addClass("move");
                $(value).removeClass("delete");
                $(value).removeClass("selected");
            });
            btnDelete.removeClass("deleting");
            $("#MCListAccordion").sortable("enable");
        }

        function clickCourseCatalogItem(sender) {
            if ($(sender).hasClass("selected")) {
                $(sender).removeClass("selected");
            } else if ($(sender).hasClass("delete")) {
                $(sender).addClass("selected");
            }
        }

        function courseCatalogGeneratePageNumbers() {
            var maxPage = Math.ceil(COURSECATALOG_COUNT / COURSECATALOG_PAGESIZE);
            //var maxPageToDisplay = maxPage > 5 ? 5 : (virtualCount % pageSize > 1 ? maxPage : maxPage);
            var pageVirtualCount = COURSECATALOG_COUNT;
            var pages = $(".js-page-numbers.coursecatalog ul").html('');
            //end = end > maxPage ? maxPage : end;
            for (var i = 1; i <= maxPage; i++) {
                var pages = $(".js-page-numbers.coursecatalog ul").html();
                var rp = i - 1;
                $(".js-page-numbers.coursecatalog ul").html(pages + '<li class="page-number"> <a href="javascript:void(0);" onclick="gotoPageCourseCatalog(' + rp + ');">' + i + '</a></li>');
            }
        }

        function btnSwitchViewCourseLibray_Click() {
            if ($('#<%= pnlCourseLibraryListView.ClientID %>').hasClass("display-none")) {
                ISCOURSELIBRARYLISTVIEW = true;
                $("#<%= pnlCourseLibraryListView.ClientID %>").removeClass("display-none");
                $("#<%= pnlGridCourseLibrary.ClientID %>").addClass("display-none");
                $("#<%= lbtnSwitchToGridCourseLibrary.ClientID %>").addClass("display-none");
                $("#<%= lbtnSwitchToListCourseLibrary.ClientID %>").removeClass("display-none");
            } else {
                ISCOURSELIBRARYLISTVIEW = false;
                $("#<%= pnlCourseLibraryListView.ClientID %>").addClass("display-none");
                $("#<%= pnlGridCourseLibrary.ClientID %>").removeClass("display-none");
                $("#<%= lbtnSwitchToListCourseLibrary.ClientID %>").addClass("display-none");
                $("#<%= lbtnSwitchToGridCourseLibrary.ClientID %>").removeClass("display-none");
            }
            setCourseLibrarySelected();
            return false;
        }

        function gotoPageCourseCatalog(pageNum) {
            $(".js-page-numbers.coursecatalog ul li").removeClass("selected-page");

            COURSECATALOG_LISTVIEW.page(pageNum);
            COURSECATALOG_LISTVIEW.page(pageNum);
        }

        function btnSwitchViewCourseCatalog_Click() {
            if (COURSECATALOG_LIST_CACHE == null || COURSECATALOG_GRID_CACHE == null) {
                COURSECATALOG_LIST_CACHE = $("#MCListAccordion").html();
                COURSECATALOG_GRID_CACHE = $("#MCGridAccordion").html();
            }

            if ($('#<%= pnlCourseCatalogListView.ClientID %>').hasClass("display-none")) {
                removeDeleteClasses();
                ISCOURSECATALOGLISTVIEW = false;
                $("#MCListAccordion").html(COURSECATALOG_LIST_CACHE);
                $("#<%= pnlCourseCatalogListView.ClientID %>").removeClass("display-none");
                $("#<%= pnlCourseCatalogGridView.ClientID %>").addClass("display-none");
                $("#<%= lbtnSwitchToGridViewCourseCatalog.ClientID %>").addClass("display-none");
                $("#<%= lbtnSwitchToListViewCourseCatalog.ClientID %>").removeClass("display-none");
            } else {
                removeDeleteClasses();
                ISCOURSECATALOGLISTVIEW = true;
                $("#MCGridAccordion").html(COURSECATALOG_GRID_CACHE);
                $("#<%= pnlCourseCatalogListView.ClientID %>").addClass("display-none");
                $("#<%= pnlCourseCatalogGridView.ClientID %>").removeClass("display-none");
                $("#<%= lbtnSwitchToListViewCourseCatalog.ClientID %>").addClass("display-none");
                $("#<%= lbtnSwitchToGridViewCourseCatalog.ClientID %>").removeClass("display-none");
            }
            return false;
        }

        //COURSE LIBRARY
        function openCourseLibrary() {
             SELECTEDCOURSES_TOADD = [];
            initializeCourseLibraryView();
            $find("<%= rwCourseLibrary.ClientID %>").show();
        }

        function closeCourseLibrary() {
            $find("<%= rwCourseLibrary.ClientID %>").close();
        }

        function initializeCourseLibraryView() {
            COURSELIBRARY_LISTVIEW = $find("<%= lvCourseLibrary.ClientID%>");
            COURSELIBRARY_GRIDVIEW = $find("<%= gridCourseLibrary.ClientID%>");
            COURSELIBRARY_LISTVIEW.rebind();
            COURSELIBRARY_PAGESIZE = 12;
            COURSELIBRARY_LISTVIEW.set_pageSize(COURSELIBRARY_PAGESIZE);
            COURSELIBRARY_LISTVIEW.page(0);

            $(".clPager .pagePrev").click(function (e) {

                COURSELIBRARY_LISTVIEW.page(COURSELIBRARY_LISTVIEW.get_currentPageIndex() - 1);

            });

            $(".clPager .pageNext").click(function (e) {

                COURSELIBRARY_LISTVIEW.page(COURSELIBRARY_LISTVIEW.get_currentPageIndex() + 1);

            });
        }

        function bindCourseLibrary() {
            COURSELIBRARY_LISTVIEW = $find("<%= lvCourseLibrary.ClientID %>");
            var startRowIndex = COURSELIBRARY_LISTVIEW.get_currentPageIndex() * COURSELIBRARY_LISTVIEW.get_pageSize(),
                maximumRows = COURSELIBRARY_LISTVIEW.get_pageSize(),
                sortExpression = COURSELIBRARY_LISTVIEW.get_sortExpressions().toLinq();

            $.ajax({
                type: "POST",
                data: "{startRowIndex: " + startRowIndex + ", maximumRows: " + maximumRows + ", sortExpression: '" + sortExpression + "'}",
                contentType: "application/json; charset=utf-8",
                url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "GetMandatoryCourseLibraryList",
                dataType: 'json',
                success: function (res) {
                    //console.log(res);
                    COURSELIBRARY_DATA = res.d.data;
                    COURSELIBRARY_COUNT = res.d.count;
                    COURSELIBRARY_PAGESIZE = COURSELIBRARY_LISTVIEW.get_pageSize();
                    COURSELIBRARY_LISTVIEW.set_virtualItemCount(COURSELIBRARY_COUNT);
                    COURSELIBRARY_LISTVIEW.set_dataSource(COURSELIBRARY_DATA);
                    COURSELIBRARY_LISTVIEW.dataBind();

                    COURSELIBRARY_GRIDVIEW.set_dataSource(COURSELIBRARY_DATA);
                    COURSELIBRARY_GRIDVIEW.dataBind();

                    if (COURSELIBRARY_COUNT > 0) {
                        $(".clPager").removeClass("display-none");
                        $(".lblCourseLibraryShowNumber").removeClass("display-none");
                    }

                    courseLibraryGeneratePageNumbers();
                    setCourseLibraryPage();

                    setCourseLibrarySelected();
                    removeDeleteClasses();

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var code = XMLHttpRequest.status;
                    if (code == "401")
                        window.location.reload();
                    else
                        alert(XMLHttpRequest.responseText);
                }
            });
        }

        function courseLibraryGeneratePageNumbers() {
            var maxPage = Math.ceil(COURSELIBRARY_COUNT / COURSELIBRARY_PAGESIZE);
            //var maxPageToDisplay = maxPage > 5 ? 5 : (virtualCount % pageSize > 1 ? maxPage : maxPage);
            var pageVirtualCount = COURSELIBRARY_COUNT;
            var pages = $(".js-page-numbers.courselibrary ul").html('');
            //end = end > maxPage ? maxPage : end;
            for (var i = 1; i <= maxPage; i++) {
                var pages = $(".js-page-numbers.courselibrary ul").html();
                var rp = i - 1;
                $(".js-page-numbers.courselibrary ul").html(pages + '<li class="page-number"> <a href="javascript:void(0);" onclick="gotoPageCourseLibrary(' + rp + ');">' + i + '</a></li>');
            }
        }

        function setCourseLibraryPage() {
            COURSELIBRARY_PAGEINDEX = COURSELIBRARY_LISTVIEW.get_currentPageIndex();
            COURSELIBRARY_COUNT = COURSELIBRARY_LISTVIEW.get_virtualItemCount();
            COURSELIBRARY_PAGESIZE = COURSELIBRARY_LISTVIEW.get_pageSize();
            var myPage = COURSELIBRARY_PAGEINDEX + 1;
            var startNum = myPage == 1 ? (COURSELIBRARY_COUNT == 0 ? 0 : 1) : ((myPage * COURSELIBRARY_PAGESIZE) > COURSELIBRARY_COUNT ? COURSELIBRARY_COUNT : (myPage * COURSELIBRARY_PAGESIZE));
            $(".js-page-numbers.courselibrary ul li").removeClass("selected-page");
            $(".js-page-numbers.courselibrary ul li:nth-child(" + myPage + ")").addClass("selected-page");
            var itemCount = COURSELIBRARY_PAGESIZE * (myPage) <= COURSELIBRARY_COUNT ? (COURSELIBRARY_PAGESIZE * (myPage)) : ((COURSELIBRARY_PAGEINDEX * COURSELIBRARY_PAGESIZE) + (COURSELIBRARY_COUNT % COURSELIBRARY_PAGESIZE));
            $(".lblCourseLibraryShowNumber").html("Showing " + itemCount + " of " + COURSELIBRARY_COUNT);
        }

        function clOnListViewDataBinding(sender, args) {
            sender.set_selectedIndexes([]);
        }

        function clOnListViewCommand(sender, args) {
            args.set_cancel(true);
            bindCourseLibrary();
        }

        function selectCourseToAdd(sender) {
            var courseId = $(sender).find("[name=hfCourseID]").val();

            if ($(sender).hasClass("selected")) {
                $(sender).removeClass("selected");

                if (SELECTEDCOURSES_TOADD.indexOf(courseId.toString()) > -1) {
                    var index = SELECTEDCOURSES_TOADD.indexOf(courseId);
                    if (index > -1) {
                        SELECTEDCOURSES_TOADD.splice(index, 1);
                    }
                }
            }
            else {
                $(sender).addClass("selected");

                if (SELECTEDCOURSES_TOADD.indexOf(courseId.toString()) < 0) {
                    SELECTEDCOURSES_TOADD.push(courseId.toString());
                }
            }
        }

        function setCourseLibrarySelected() {
            if (ISCOURSELIBRARYLISTVIEW) {
                $("#CourseLibraryView .item").removeClass("selected");
                $("#CourseLibraryView input[name=hfCourseID]").each(function (index, value) {
                    var cid = $(value).val();
                    if (SELECTEDCOURSES_TOADD.indexOf(cid) >= 0) {
                        $(value).parent().addClass("selected");
                    }
                });
            } else {
                $("#CourseLibraryGridView .item").removeClass("selected");
                $("#CourseLibraryGridView input[name=hfCourseID]").each(function (index, value) {
                    var cid = $(value).val();
                    if (SELECTEDCOURSES_TOADD.indexOf(cid) >= 0) {
                        $(value).parent().addClass("selected");
                    }
                });
            }
        }

        function saveSelectedCourse() {
            var coursesData = { "courses": SELECTEDCOURSES_TOADD };

            if (SELECTEDCOURSES_TOADD.length > 0) {
                $.ajax({
                    type: "POST",
                    data: "{selectedCourses: '" + JSON.stringify(coursesData) + "'}",
                    contentType: "application/json; charset=utf-8",
                    url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "SaveSelectedMandatoryCourses",
                    dataType: 'json',
                    success: function (res) {
                        var result = res.d.isSuccess;
                        var message = res.d.message;
                        if (result == true) {
                            closeCourseLibrary();
                            var RadAlertSuccessMessage = $("#<%=hdnRadAlertSuccessMessage.ClientID%>").val();
                            var RadAlertSuccessConfirm = $("#<%=hdnRadAlertSuccessConfirm.ClientID%>").val();
                            radalert("Course successfully added.", 330, 180, "Success", "");
                            bindCourseLibrary();
                            bindCourseCatalog();
                        } else {
                            radalert(RadAlertSuccessMessage, 330, 180, RadAlertSuccessConfirm, "");
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        var code = XMLHttpRequest.status;
                        if (code == "401")
                            window.location.reload();
                        else
                            alert(XMLHttpRequest.responseText);
                    }
                });
            } else {
                var RadAlertErrorMessage = $("#<%=hdnRadAlertErrorMessage.ClientID%>").val();
                var RadAlertErrorConfirm = $("#<%=hdnRadAlertErrorConfirm.ClientID%>").val();
                radalert(RadAlertErrorMessage, 330, 180, RadAlertErrorConfirm, "");
            }
        }

        function changeCourseLibraryPageSize(sender, args) {
            var ps = $find("<%= ddlCourseLibrary.ClientID %>").get_selectedItem().get_value();
            COURSELIBRARY_PAGESIZE = ps;
            COURSELIBRARY_PAGEINDEX = 0;

            var defaultPageSize = COURSELIBRARY_LISTVIEW.get_pageSize();
            if (!$.isNumeric(ps)) {
                COURSELIBRARY_PAGESIZE = defaultPageSize;
            }

            COURSELIBRARY_LISTVIEW.set_pageSize(COURSELIBRARY_PAGESIZE);
            COURSELIBRARY_LISTVIEW.page(COURSELIBRARY_PAGEINDEX);
            COURSELIBRARY_LISTVIEW.rebind();
        }

        function gotoPageCourseLibrary(pageNum) {
            $(".js-page-numbers.courselibrary ul li").removeClass("selected-page");

            COURSELIBRARY_LISTVIEW.page(pageNum);
            COURSELIBRARY_LISTVIEW.page(pageNum);
        }

        //---
        function showLoading(controlToLoad) {
            var panel = $find("<%= localLoadingPanel.ClientID %>");
            panel.show(controlToLoad);
        }

        function hideLoading() {
            $(".Loading2").hide();
        }

        function UpdateSettings() {
            var isGridView = $('#<%= cbGridView.ClientID %>').is(':checked') == true ? true : false;
            var displayCount = $find("<%= ddlDisplayCount.ClientID %>").get_selectedItem().get_value();
            var isManual = $('#<%= cbManualSelection.ClientID %>').is(':checked');
            var isVisible = $('#<%= cbVisible.ClientID %>').is(':checked');
            //test
            //console.log("defaultView = " + defaultView);
            //console.log("displayCount = " + displayCount);
            //console.log("isManual = " + isManual);
            //console.log("isVisible = " + isVisible);

            $.ajax({
                type: "POST",
                data: "{ IsGridView: '" + isGridView + "', DisplayCount: '" + displayCount + "', IsManual: '" + isManual + "', IsVisible: '" + isVisible + "'}",
                contentType: "application/json; charset=utf-8",
                url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "SaveMandatoryCourseSetting",
                dataType: 'json',
                success: function (msg) {
                    //console.log(bitVal);
                    var RadAlertSettings = $("#<%=hdnRadAlertSettings.ClientID%>").val();
                    var RadAlertSettingsConfirm = $("#<%=hdnRadAlertSettingsConfirm.ClientID%>").val();
                    radalert(RadAlertSettings, 330, 180, RadAlertSettingsConfirm, "");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var code = XMLHttpRequest.status;
                    if (code == "401")
                        window.location.reload();
                    else
                        alert(XMLHttpRequest.responseText);
                }
            });

            return false;
        }
    </script>
</rad:RadScriptBlock>
<rad:RadAjaxLoadingPanel ID="localLoadingPanel" runat="server" CssClass="Loading2"
    Transparency="25" />
<rad:RadWindowManager ID="rwmCourseCatalog" RenderMode="Lightweight" EnableShadow="true"
    VisibleOnPageLoad="false" Behaviors="Close, Move" DestroyOnClose="true" Modal="true"
    Opacity="99" runat="server" VisibleStatusbar="false" Skin="Bootstrap">
    <Windows>
        <rad:RadWindow ID="rwPreviewCourseCatalog" runat="server" Title="" CssClass="tc-radwindow-3 font-size-normal"
            Width="950px" AutoSizeBehaviors="Height" Height="750px">
            <ContentTemplate>
                <asp:Panel ID="pnlPreviewContent" runat="server" class="mc-preview-container">
                    <div class="row no-margin header-content border-box">
                        <div class="col-md-4 pull-left no-paddings">
                            <asp:LinkButton ID="lbtnSwitchToListViewCourseCatalog" runat="server" OnClientClick="btnSwitchViewCourseCatalog_Click(); return false;"
                                Font-Underline="false" aria-hidden="true" ToolTip="Switch to List View">
                                <i id="switchClassListView" runat="server" class="fa fa-th-list fa-md fa-gray ">
                                </i>
                            </asp:LinkButton>
                            <asp:LinkButton ID="lbtnSwitchToGridViewCourseCatalog" runat="server" OnClientClick="btnSwitchViewCourseCatalog_Click(); return false;"
                                Font-Underline="false" aria-hidden="true" CssClass="display-none" ToolTip="Switch to Grid View">
                                <i id="switchClassGridView" runat="server" class="fa fa-th-large  fa-md fa-gray ">
                                </i>
                            </asp:LinkButton>
                        </div>
                        <div class="col-md-4">
                            <p class="text-center lblCourseCatalogShowNumber">
                                <asp:Label runat="server" ID="Label1" Text="<%$ Resources:LocalizedResource, Showing %>"></asp:Label> 1-9 <asp:Label runat="server" ID="Label2" Text="<%$ Resources:LocalizedResource, Of %>"></asp:Label> 29
                            </p>
                        </div>
                        <div class="col-md-4 no-paddings">
                            <div class="col-md-4 pull-right no-margin no-paddings">
                                <rad:RadDropDownList ID="ddlCourseCatalogPager" runat="server" CssClass="soft bg-white"
                                    Width="100px" ClientIDMode="Static" OnClientSelectedIndexChanged="changeCourseCatalogPageSize">
                                </rad:RadDropDownList>
                            </div>
                            <div class="col-md-8 no-paddings">
                                <p class="pull-right">
                                    <asp:Label runat="server" ID="Label3" Text="<%$ Resources:LocalizedResource, ShowingItemsPerPage %>"></asp:Label> &nbsp;&nbsp;
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="mcPager container search-result no-paddings">
                        <p class="pull-left" style="line-height: 34px;">
                            &nbsp;&nbsp;<asp:Label runat="server" ID="Label4" Text="<%$ Resources:LocalizedResource, MandatoryCourses %>"></asp:Label>
                        </p>
                    </div>
                    <div class="row no-margin padding-vertical-md settings-button">
                        <div class="pull-right">
                            <asp:LinkButton ID="btnAdd" Text="text" runat="server" OnClientClick="openCourseLibrary(); return false;">
                                <i class="fa fa-plus-circle fa-md fa-gray" aria-hidden="true"></i>
                            </asp:LinkButton>
                            <asp:LinkButton ID="btnDelete" Text="text" runat="server" OnClientClick="deleteCourseCatalogItem(); return false;"
                                ClientIDMode="Static">
                                <i class="fa fa-trash-o fa-md fa-gray" aria-hidden="true"></i>
                            </asp:LinkButton>
                        </div>
                    </div>
                    <asp:Panel ID="pnlCourseCatalogListView" runat="server" ClientIDMode="Static">
                        <div id="pnlCourseCatalogContainer" class="main-content border-box">
                            <rad:RadListView ID="lvCourseCatalog" runat="server" AllowPaging="true" AllowMultiFieldSorting="true"
                                PageSize="12">
                                <LayoutTemplate>
                                    <div id="ccMainWrapper">
                                        <div id="mc-items" class="display-flow pull-left width-fill">
                                            <div id="MCListAccordion">
                                            </div>
                                        </div>
                                    </div>
                                </LayoutTemplate>
                                <ClientSettings>
                                    <DataBinding ItemPlaceHolderID="MCListAccordion">
                                        <ItemTemplate>
                                        <div class="col-md-3 move item" onclick="clickCourseCatalogItem(this);">
                                            <div class="image-wrapper container no-paddings">
                                                <div class="overlay-move overlay-item">
                                                    <i class="fa fa-arrows-alt fa-md fa-white" aria-hidden="true"></i>
                                                </div>
                                                    <div class="overlay-delete overlay-item">
                                                    <i class="fa fa-trash-o fa-md fa-white" aria-hidden="true"></i>
                                                </div>
                                                <image src='Media/Uploads/CourseImg/#= CourseID #/#= CourseImage #' onerror="this.src='Media/Uploads/CourseImg/No_image.jpg'" />
                                            </div>
                                            <div class="course-title font-bold teal">
                                                <p class="teal">
                                                <strong>#= CourseTitle #</strong>
                                                </p>
                                            </div>
                                            <div class="line-gray"></div>
                                            <div class="description font-bold">
                                                #= CourseDescription #
                                            </div>
                                            <div class="js-hiddenfields">
                                                <input type="number" name="hfIndex" value='#= OrderNumber #' class="display-none" />
                                                <input type="text" name="hfCourseID" value='#= EncryptedCourseID #' class="display-none" />
                                            </div>
                                        </div>
                                        </ItemTemplate>
                                        <EmptyDataTemplate>
                                    <p>No Mandatory Course Selected.
                                    </p>
                                        </EmptyDataTemplate>
                                        <DataService EnableCaching="true" />
                                    </DataBinding>
                                    <ClientEvents OnCommand="ccOnListViewCommand" OnDataBinding="ccOnListViewDataBinding">
                                    </ClientEvents>
                                </ClientSettings>
                            </rad:RadListView>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="pnlCourseCatalogGridView" runat="server" CssClass="display-none" ClientIDMode="Static">
                        <rad:RadListView ID="gvCourseCatalog" runat="server" AllowPaging="true" PageSize="8">
                            <LayoutTemplate>
                                <div id="ccGridWrapper">
                                    <table class="gridMainTable table table-bordered table-striped course-grid">
                                        <thead>
                                            <tr class="rlvHeader">
                                                <th class="btn-teal">
                                                    Title
                                                </th>
                                                <th class="btn-teal">
                                                    Description
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody id="MCGridAccordion">
                                        </tbody>
                                        <tfoot>
                                        </tfoot>
                                    </table>
                                </div>
                            </LayoutTemplate>
                            <ClientSettings>
                                <DataBinding ItemPlaceHolderID="MCGridAccordion">
                                    <ItemTemplate>
                                        <tr class="rlvI move item" title=" #= CourseTitle #" onclick='clickCourseCatalogItem(this);'>
                                            <td>
                                                <i class="fa fa-trash-o fa-md fa-gray pull-left" aria-hidden="true"></i>
                                                &nbsp;&nbsp;&nbsp;
                                                #= CourseTitle #
                                            </td>
                                            <td>
                                                &nbsp;&nbsp;&nbsp;
                                                #= CourseDescription #
                                            </td>
                                            <input type="hidden" name="hfCourseID" value="#= EncryptedCourseID #" />
                                        </tr>
                                    </ItemTemplate>
                                    <EmptyDataTemplate>
                                    <p>No result found.
                                    </p>
                                    </EmptyDataTemplate>
                                    <DataService EnableCaching="true" />
                                </DataBinding>
                            </ClientSettings>
                        </rad:RadListView>
                    </asp:Panel>
                    <div class="mcPager pager-bottom container display-none">
                        <div class="js-page-numbers coursecatalog display-inline-flex" style="padding-top: 5%;">
                            <a class="pagePrev black-arrow fa fa-chevron-left no-underline-hover" href="javascript:void(0);"
                                title="Go to previous page"></a>
                            <ul class="nostyle display-inline-flex black-font">
                            </ul>
                            <a class="pageNext black-arrow fa fa-chevron-right no-underline-hover" href="javascript:void(0);"
                                title="Go to next page"></a>
                        </div>
                    </div>
                </asp:Panel>
                <div class="row bottom-center">
                    <asp:Button ID="btnCancel" Text="<%$ Resources:LocalizedResource, Cancel %>" runat="server" CssClass="btn btn-md btn-teal btn-flat"
                        OnClientClick="closeCourseCatalog(); return false" />
                    <asp:Button ID="btnSaveChanges" Text="<%$ Resources:LocalizedResource, SaveChanges %>" runat="server" CssClass="btn btn-md btn-teal btn-flat"
                        OnClientClick="saveCourseCatalogOrder(); return false;" ClientIDMode="Static" />
                    <asp:Button ID="btnDeleteCourse" Text="<%$ Resources:LocalizedResource, Delete %>" runat="server" CssClass="btn btn-md btn-teal btn-flat diplay-none"
                        OnClientClick="deleteSelectedCourse(); return false;" ClientIDMode="Static" />
                </div>
            </ContentTemplate>
        </rad:RadWindow>
        <rad:RadWindow ID="rwCourseLibrary" runat="server" Title="" CssClass="tc-radwindow-3 font-size-normal"
            Width="950px" AutoSizeBehaviors="Height" Height="750px">
            <ContentTemplate>
                <asp:Panel ID="pnlCourseLibrary" runat="server">
                    <div class="container no-paddings">
                        <p class="teal text-center bold" style="line-height: 34px;">
                            <asp:Label runat="server" ID="Label5" Text="<%$ Resources:LocalizedResource, CourseLibrary %>"></asp:Label>
                        </p>
                    </div>
                    <div class="row no-margin header-content border-box">
                        <div class="col-md-4 pull-left no-paddings">
                            <asp:LinkButton ID="lbtnSwitchToGridCourseLibrary" runat="server" OnClientClick="btnSwitchViewCourseLibray_Click(); return false;"
                                Font-Underline="false" aria-hidden="true" ToolTip="Switch to List View">
                                <i id="I1" runat="server" class="fa fa-th-list fa-md fa-gray "></i>
                            </asp:LinkButton>
                            <asp:LinkButton ID="lbtnSwitchToListCourseLibrary" runat="server" OnClientClick="btnSwitchViewCourseLibray_Click(); return false;"
                                Font-Underline="false" aria-hidden="true" CssClass="display-none" ToolTip="Switch to Grid View">
                                <i id="I2" runat="server" class="fa fa-th-large fa-md fa-gray "></i>
                            </asp:LinkButton>
                        </div>
                        <div class="col-md-4 text-center">
                            <p>
                                <p class="lblCourseLibraryShowNumber">
                                </p>
                            </p>
                        </div>
                        <div class="col-md-4">
                            <div class="col-md-3 pull-right no-margin no-paddings">
                                <rad:RadDropDownList ID="ddlCourseLibrary" runat="server" CssClass="soft bg-white"
                                    Width="50px" ClientIDMode="Static" OnClientSelectedIndexChanged="changeCourseLibraryPageSize">
                                </rad:RadDropDownList>
                            </div>
                            <div class="col-md-8 no-margin no-paddings">
                                <p class="pull-right">
                                    <asp:Label runat="server" ID="Label6" Text="<%$ Resources:LocalizedResource, ShowingItemsPerPage %>"></asp:Label>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="main-content border-box">
                        <asp:Panel ID="pnlCourseLibraryListView" runat="server">
                            <rad:RadListView ID="lvCourseLibrary" runat="server" AllowPaging="true" AllowMultiFieldSorting="true"
                                PageSize="12">
                                <LayoutTemplate>
                                    <div id="CourseLibraryView">
                                        <div id="cl-items" class="display-flow pull-left width-fill">
                                            <div id="CourseLibraryListItemContainer">
                                            </div>
                                        </div>
                                    </div>
                                </LayoutTemplate>
                                <ClientSettings>
                                    <DataBinding ItemPlaceHolderID="CourseLibraryListItemContainer">
                                        <ItemTemplate>
                                        <div class="col-md-3 no-paddings no-margin item pointer " onclick="selectCourseToAdd(this);" id="i#= EncryptedCourseID #" >
                                            <div class="image-wrapper container no-paddings text-center">
                                                <i class="fa fa-check-circle-o fa-md fa-white" aria-hidden="true"></i>
                                                <h5 class="show-on-hover center-both white">
                                                    #= CourseTitle #</h5>
                                                <image src='Media/Uploads/CourseImg/#= CourseID #/#= CourseImage #' onerror="this.src='Media/Uploads/CourseImg/No_image.jpg'" class="img-fit" />
                                            </div>
                                            <input type="hidden" name="hfCourseID" value="#= EncryptedCourseID #" />
                                        </div>
                                        </ItemTemplate>
                                        <EmptyDataTemplate>
                                            <p>No Course to display.
                                            </p>
                                        </EmptyDataTemplate>
                                        <DataService EnableCaching="true" />
                                    </DataBinding>
                                    <ClientEvents OnCommand="clOnListViewCommand" OnDataBinding="clOnListViewDataBinding">
                                    </ClientEvents>
                                </ClientSettings>
                            </rad:RadListView>
                        </asp:Panel>
                        <asp:Panel ID="pnlGridCourseLibrary" runat="server" CssClass="display-none search-view">
                            <rad:RadListView ID="gridCourseLibrary" runat="server" AllowPaging="true" PageSize="8">
                                <LayoutTemplate>
                                    <div id="CourseLibraryGridView">
                                        <div class="clPager container search-result display-none">
                                            <p class="lblShowNumber" class="pull-left">
                                            </p>
                                        </div>
                                        <table class="gridMainTable table table-bordered table-striped course-grid">
                                            <thead>
                                                <tr class="rlvHeader">
                                                    <th class="btn-teal">
                                                        Title
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody id="CourseLibraryGridItemContainer">
                                            </tbody>
                                            <tfoot>
                                            </tfoot>
                                        </table>
                                    </div>
                                </LayoutTemplate>
                                <ClientSettings>
                                    <DataBinding ItemPlaceHolderID="CourseLibraryGridItemContainer">
                                        <ItemTemplate>
                                        <tr class="rlvI hover-pointer item" title=" #= CourseTitle #" onclick='selectCourseToAdd(this);'>
                                            <td>
                                                <i class="fa fa-check-circle-o fa-md fa-white pull-left" aria-hidden="true"></i>
                                                &nbsp;&nbsp;&nbsp;
                                                #= CourseTitle #
                                               
                                            </td>
                                             <input type="hidden" name="hfCourseID" value="#= EncryptedCourseID #" />
                                        </tr>
                                        </ItemTemplate>
                                        <EmptyDataTemplate>
                                    <p>No result found.
                                    </p>
                                        </EmptyDataTemplate>
                                        <DataService EnableCaching="true" />
                                    </DataBinding>
                                </ClientSettings>
                            </rad:RadListView>
                        </asp:Panel>
                    </div>
                </asp:Panel>
                <div class="clPager pager-bottom container display-none no-paddings">
                    <div class="js-page-numbers courselibrary display-inline-flex" style="padding-top: 5%;">
                        <a class="pagePrev black-arrow fa fa-chevron-left no-underline-hover" href="javascript:void(0);"
                            title="Go to previous page"></a>
                        <ul class="nostyle display-inline-flex black-font">
                        </ul>
                        <a class="pageNext black-arrow fa fa-chevron-right no-underline-hover" href="javascript:void(0);"
                            title="Go to next page"></a>
                    </div>
                </div>
                <div class="row bottom-center">
                    <asp:Button ID="Button1" Text="<%$ Resources:LocalizedResource, Cancel %>" runat="server" CssClass="btn btn-md btn-teal btn-flat"
                        OnClientClick="closeCourseLibrary(); return false" />
                    <asp:Button ID="Button2" Text="<%$ Resources:LocalizedResource, Add %>" runat="server" CssClass="btn btn-md btn-teal btn-flat"
                        OnClientClick="saveSelectedCourse(); return false;" />
                </div>
            </ContentTemplate>
        </rad:RadWindow>
    </Windows>
</rad:RadWindowManager>
<asp:Panel ID="pnlMandatorySettings" runat="server" CssClass="region">
    <div class="wrapper-main-content">
        <div class="inner-content col-md-6">
            <h5>
                <asp:Label runat="server" ID="Label7" Text="<%$ Resources:LocalizedResource, CriteriaSelection %>"></asp:Label>
            </h5>
            <p>
                <asp:Label runat="server" ID="Label8" Text="<%$ Resources:LocalizedResource, SelectTheCriteriaToDisplayACourseUnderTheMandatoryCoursesCatalogDisplay %>"></asp:Label>
            </p>
        </div>
        <div class="inner-content col-md-5">
            <h5>
                <asp:Label runat="server" ID="Label9" Text="<%$ Resources:LocalizedResource, CriteriaFilters %>"></asp:Label>:
            </h5>
            <table>
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="Label10" Text="<%$ Resources:LocalizedResource, SetDefaultView %>"></asp:Label>
                    </td>
                    <td>
                        <asp:CheckBox ID="cbGridView" Text="<%$ Resources:LocalizedResource, Grid %>" runat="server" ClientIDMode="Static" onclick="DefaultViewChanged(this);"
                            CssClass="js-cbox-defaultview" />
                    </td>
                    <td>
                        <asp:CheckBox ID="cbListView" Text="<%$ Resources:LocalizedResource, List %>" runat="server" ClientIDMode="Static" onclick="DefaultViewChanged(this);"
                            CssClass="js-cbox-defaultview" />
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="Label11" Text="<%$ Resources:LocalizedResource, Display %>"></asp:Label>
                    </td>
                    <td>
                        <rad:RadDropDownList ID="ddlDisplayCount" runat="server" CssClass="soft bg-white"
                            Width="50px">
                        </rad:RadDropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="checkbox" id="cbManualSelection" runat="server" class="js-switch-transparent " />
                    </td>
                    <td>
                        <asp:Label runat="server" ID="Label12" Text="<%$ Resources:LocalizedResource, EnableManualSelection %>"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="checkbox" id="cbVisible" runat="server" class="js-switch-transparent" />
                    </td>
                    <td>
                        <asp:Label runat="server" ID="Label13" Text="<%$ Resources:LocalizedResource, VisibleOnHomepageSigninPage %>"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="row btn-group-center">
        <asp:Button Text="<%$ Resources:LocalizedResource, Preview %>" runat="server" CssClass="btn btn-md btn-teal btn-flat"
            OnClientClick="openCourseCatalog(); return false" />
        <asp:Button Text="<%$ Resources:LocalizedResource, SaveChanges %>" runat="server" CssClass="btn btn-md btn-teal btn-flat"
            OnClientClick="UpdateSettings(); return false;" />
    </div>
</asp:Panel>
<asp:HiddenField ID="asd" runat="server" />
<asp:HiddenField runat="server" ID="hdnRadAlertErrorMessage" Value="<%$ Resources:LocalizedResource, Pleaseselectcoursefirst %>" />
<asp:HiddenField runat="server" ID="hdnRadAlertErrorConfirm" Value="<%$ Resources:LocalizedResource, Error %>" />
<asp:HiddenField runat="server" ID="hdnRadConfirmMessage" Value="<%$ Resources:LocalizedResource, AreYouSureYouWantToDeleteThisCourses %>" />
<asp:HiddenField runat="server" ID="hdnRadConfirmConfirm" Value="<%$ Resources:LocalizedResource, ConfirmDelete %>" />
<asp:HiddenField runat="server" ID="hdnRadAlertSuccessMessage" Value="<%$ Resources:LocalizedResource, CourseSuccessfullyAdded %>" />
<asp:HiddenField runat="server" ID="hdnRadAlertSuccessConfirm" Value="<%$ Resources:LocalizedResource, Success %>" />
<asp:HiddenField runat="server" ID="hdnRadAlertSettings" Value="<%$ Resources:LocalizedResource, NewSettingsHaveBeenSaved %>" />
<asp:HiddenField runat="server" ID="hdnRadAlertSettingsConfirm" Value="<%$ Resources:LocalizedResource, Saved %>" />
<asp:HiddenField runat="server" ID="hdnRadAlertCourseSuccessMessage" Value="<%$ Resources:LocalizedResource, Coursesuccessfullysaved %>" />
<asp:HiddenField runat="server" ID="hdnRadAlertCourseSuccessConfirm" Value="<%$ Resources:LocalizedResource, Success %>" />
<asp:HiddenField runat="server" ID="hdnRadAlertCourseDeleteMessage" Value="<%$ Resources:LocalizedResource, Coursesuccessfullydeleted %>" />
<asp:HiddenField runat="server" ID="hdnRadAlertCourseDeleteConfirm" Value="<%$ Resources:LocalizedResource, Success %>" />
<asp:HiddenField runat="server" ID="hdnRadAlertCourseTryAgainMessage" Value="<%$ Resources:LocalizedResource, SomethingWentWhileSubmittingYourReportPleaseTryAgain %>" />
<asp:HiddenField runat="server" ID="hdnRadAlertCourseTryAgainConfirm" Value="<%$ Resources:LocalizedResource, Error %>" />

