﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class UserControl_Pages_MandatoryCourseSettings : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (!Page.IsPostBack)
        {
            loadDdl();
            loadPreviousSettings();
        }

        ddlCourseLibrary.SelectedValue = "16";
    }

    void loadPreviousSettings()
    {
        var settings = DataHelper.getMandatoryCourseSettings();
        if (settings != null)
        {
            bool isGrid = settings.DefaultView;
            int count = settings.DisplayCount;
            bool isManual = settings.ManualSelection;
            bool isVisible = settings.Visible;
            int items = settings.ItemsPerPage;

            cbGridView.Checked = isGrid == true ? true : false;
            cbListView.Checked = isGrid == false ? true : false;
            ddlDisplayCount.SelectedIndex = ddlDisplayCount.FindItemByValue(count.ToString()).Index;
            cbManualSelection.Checked = isManual;
            cbVisible.Checked = isVisible;
            ddlCourseCatalogPager.SelectedIndex = ddlCourseCatalogPager.FindItemByValue(items.ToString()).Index;
        }
     

    }

    void loadDdl()
    {
        for (int i = 1; i <= 20; i++)
        {
            DropDownListItem item = new DropDownListItem(i.ToString(), i.ToString());
            ddlDisplayCount.Items.Add(item);
            DropDownListItem item2 = new DropDownListItem(i.ToString(), i.ToString());
            ddlCourseLibrary.Items.Add(item2);
            DropDownListItem item3 = new DropDownListItem(i.ToString(), i.ToString());
            ddlCourseCatalogPager.Items.Add(item3);
        }
    }
}
