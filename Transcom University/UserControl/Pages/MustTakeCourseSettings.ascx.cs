﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class UserControl_Pages_MustTakeCourseSettings : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            loadDdl();
            loadPreviousSettings();
        }

    }

    void loadPreviousSettings()
    {
        var settings = DataHelper.getMustTakeCourseSettings();

        if (settings != null)
        {
            bool overdue = settings.OverdueCourse;
            bool incomplete = settings.IncompleteCourse;
            bool isGrid = settings.DefaultView;
            int displayCount = settings.DisplayCount == 0 ? 1 : settings.DisplayCount;
            bool isManual = settings.ManualSelection;
            bool isVisible = settings.Visible;
            bool notifyAlert = settings.NotifyAlert;
            bool notifyEmail = settings.NotifyEmail;

            cbOverdue.Checked = overdue;
            cbIncomplete.Checked = incomplete;
            cbGridView.Checked = isGrid == true ? true : false;
            cbListView.Checked = isGrid == false ? true : false;
            ddlDisplayCount.SelectedIndex = ddlDisplayCount.FindItemByValue(displayCount.ToString()).Index;
            cbManualSelection.Checked = isManual;
            cbNotifyAlert.Checked = notifyAlert;
            cbNotifyEmail.Checked = notifyEmail;
            cbVisible.Checked = isVisible;
        }
    }

    void loadDdl()
    {
        for (int i = 1; i <= 20; i++)
        {
            DropDownListItem itemDisplay = new DropDownListItem(i.ToString(), i.ToString());
            DropDownListItem itemLibrary = new DropDownListItem(i.ToString(), i.ToString());
            DropDownListItem itemPager = new DropDownListItem(i.ToString(), i.ToString());
            ddlDisplayCount.Items.Add(itemDisplay);
            ddlCourseLibrary.Items.Add(itemLibrary);
            ddlCourseCatalogPager.Items.Add(itemPager);
        }
    }

}