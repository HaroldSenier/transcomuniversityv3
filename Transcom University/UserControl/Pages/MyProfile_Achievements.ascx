﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MyProfile_Achievements.ascx.cs"
    Inherits="UserControl_Pages_MyProfile_Achievements" %>
<rad:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <rad:AjaxSetting AjaxControlID="LinkBtnUploadCert">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="LinkBtnUploadCert" UpdatePanelRenderMode="Inline"
                    LoadingPanelID="fullPageLoading" />
                <rad:AjaxUpdatedControl ControlID="pnlUploadCert" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="lbtnCancel">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="lbtnCancel" UpdatePanelRenderMode="Inline" LoadingPanelID="fullPageLoading" />
                <rad:AjaxUpdatedControl ControlID="LinkBtnUploadCert" />
                <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="cbCourseList">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="cbCourseList" UpdatePanelRenderMode="Inline" LoadingPanelID="fullPageLoading" />
                <rad:AjaxUpdatedControl ControlID="RadMultiPage1" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="BtnSaveUpload">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="gridExternal" />
                <rad:AjaxUpdatedControl ControlID="div_upload_cert" LoadingPanelID="fullPageLoading" />
            </UpdatedControls>
        </rad:AjaxSetting>
    </AjaxSettings>
</rad:RadAjaxManagerProxy>
<br />
<div class="col-md-12 ten-px-paddings" style="padding-bottom: 30px">
    <div class="row">
        <div class="col-md-2">
            <div class="bg-color-light-teal color-white font-bold five-px-padding">
                <asp:Label runat="server" ID="Label1" Text="<%$ Resources:LocalizedResource, InternalCertifications %>"></asp:Label>
            </div>
        </div>
        <div class="col-md-10">
            <div class="row" align="center">
                <div class="col-lg-12 col-xs-6 col-md-6">
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-bottom: 10px;">
        <div class="col-md-4 col-md-12">
            <div class="border-top-light-teal">
                <div class="row">
                </div>
            </div>
        </div>
        <div class="col-md-8">
        </div>
    </div>
    <div class="row" style="margin-bottom: 20px;">
        <div class="col-md-12">
            <rad:RadComboBox ID="cbCourseList" runat="server" Skin="Bootstrap" CssClass="selectpicker"
                data-style="btn-danger" EmptyMessage="Choose course type" Width="300" OnSelectedIndexChanged="cbCourseList_SelectedIndexChanged"
                DataSourceID="sdCategory" DataTextField="Name" DataValueField="ID" AutoPostBack="true">
            </rad:RadComboBox>
            <asp:SqlDataSource ID="sdCategory" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection%>"
                SelectCommand="SELECT CategoryID as 'ID', Description, Category as 'Name' FROM [tbl_TranscomUniversity_Lkp_Category] where hidefromlist = 0">
            </asp:SqlDataSource>
        </div>
        <div class="col-md-10" style="margin-top: 20px;">
            <rad:RadGrid ID="gridInternal" runat="server" OnNeedDataSource="gridInternal_NeedDataSource"
                Skin="Bootstrap" OnItemDataBound="gridInternal_ItemDataBound" OnPreRender="gridInternal_PreRender">
                <MasterTableView AutoGenerateColumns="false" DataKeyNames="ID" CommandItemDisplay="None">
                    <CommandItemSettings ShowExportToExcelButton="false" ShowExportToCsvButton="false"
                        ShowRefreshButton="false" ShowAddNewRecordButton="false" />
                    <Columns>
                        <rad:GridTemplateColumn UniqueName="CertificateName" SortExpression="CertificateName"
                            DataField="CertificateName" ColumnEditorID="null" HeaderText="<%$ Resources:LocalizedResource, CertificateName %>"
                            HeaderStyle-Font-Bold="true">
                            <ItemTemplate>
                                <%# Eval("CertificateName")%>
                            </ItemTemplate>
                        </rad:GridTemplateColumn>
                        <rad:GridTemplateColumn UniqueName="ID" SortExpression="Certificate" DataField="ID"
                            ColumnEditorID="Certificate" HeaderText="<%$ Resources:LocalizedResource, Certificate %>" HeaderStyle-Font-Bold="true">
                            <ItemTemplate>
                                <a id="hllvRecommendedLeadershipCourses" onclick='openInternalNewTab("<%# DataBinder.Eval(Container.DataItem, "encryptedID") %>");return false;'  href="javascript:void(0);" style="color:Black;" title="View" style="Color: blue;">View</a>                               
                               <%-- <asp:HyperLink ID="CertLink" runat="server" NavigateUrl='<%# "~/ViewCertificate.aspx?Tab=Cert&Type=Int&CertID="+ DataBinder.Eval(Container.DataItem, "encryptedID") %>'>View</asp:HyperLink>--%>
                            </ItemTemplate>
                        </rad:GridTemplateColumn>
                        <rad:GridDateTimeColumn UniqueName="DateAcquired" HeaderText="<%$ Resources:LocalizedResource, DateAcquired %>" DataField="DateAcquired"
                            DataType="System.DateTime" DataFormatString="{0:MMM dd, yyyy}" HeaderStyle-Font-Bold="true">
                        </rad:GridDateTimeColumn>
                        <rad:GridTemplateColumn UniqueName="ID" SortExpression="<%$ Resources:LocalizedResource, Badges %>" DataField="ID" ColumnEditorID="null"
                            HeaderText="Badges" HeaderStyle-Font-Bold="true">
                            <ItemTemplate>
                                <asp:DataList ID="repBadges" runat="server" RepeatLayout="Table" RepeatColumns="5"
                                    CellPadding="4" CellSpacing="4">
                                    <ItemTemplate>
                                        <a href="javascript:;" onclick="popovertrigger(this); return false;" class="popovers"
                                            data-placement="top" data-html="true" data-toggle="popover" data-trigger="focus"
                                            title="#= Resources:LocalizedResource, BadgeInformation #" data-container="body" data-content="<div class='row'><div class='col-md-6'><img src='/Media/Images/<%# Eval("BadgeImgBig") %>' width='75' class='img-responsive'></div><div class='col-md-6'><%#Eval("BadgeName") %><br /><%#Eval("Description") %></div></div>">
                                            <asp:Image ID="Image1" runat="server" ImageUrl='<%# "~/Media/Images/"+Eval("BadgeImgSmall") %>' />
                                        </a>
                                        <table>
                                            <tr>
                                                <td colspan="5">
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:DataList>
                            </ItemTemplate>
                        </rad:GridTemplateColumn>
                    </Columns>
                </MasterTableView>
            </rad:RadGrid>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-md-12">
            <div class="bg-color-light-teal color-white font-bold five-px-padding">
                <asp:Label runat="server" ID="Label2" Text="<%$ Resources:LocalizedResource, ExternalCertifications %>"></asp:Label>
            </div>
        </div>
        <div class="col-md-10">
        </div>
    </div>
    <div class="row" style="margin-bottom: 10px;">
        <div class="col-md-4 col-md-12">
            <div class="border-top-light-teal">
                <div class="row">
                </div>
            </div>
        </div>
        <div class="col-md-8">
        </div>
    </div>
    <div class="row" style="margin-top: 20px;">
        <div class="col-md-10">
            <rad:RadGrid ID="gridExternal" runat="server" OnNeedDataSource="gridExternal_NeedDataSource"
                OnPreRender="gridExternal_PreRender" Skin="Bootstrap" OnItemDataBound="gridExternal_ItemDataBound">
                <MasterTableView AutoGenerateColumns="false" DataKeyNames="ID" CommandItemDisplay="None">
                    <CommandItemSettings ShowExportToExcelButton="false" ShowExportToCsvButton="false"
                        ShowRefreshButton="false" ShowAddNewRecordButton="false" />
                    <Columns>
                        <rad:GridTemplateColumn UniqueName="CertificateName" SortExpression="CertificateName"
                            DataField="CertificateName" ColumnEditorID="null" HeaderText="<%$ Resources:LocalizedResource, CertificateName %>"
                            HeaderStyle-Font-Bold="true">
                            <ItemTemplate>
                                <%# Utils.ClipStringTo100(Eval("CertificateName").ToString())%>
                            </ItemTemplate>
                        </rad:GridTemplateColumn>
                        <rad:GridTemplateColumn UniqueName="ID" SortExpression="Certificate" DataField="ID"
                            ColumnEditorID="Certificate" HeaderText="<%$ Resources:LocalizedResource, Certificate %>" HeaderStyle-Font-Bold="true">
                            <ItemTemplate>
                            <a id="hllvRecommendedLeadershipCourses" onclick='openExternalNewTab("<%# DataBinder.Eval(Container.DataItem, "encryptedID") %>");return false;'  href="javascript:void(0);" style="color:Black;" title="View" style="Color: blue;">View</a>                               
                                <%--<asp:HyperLink ID="CertLink" runat="server" NavigateUrl='<%# "~/ViewCertificate.aspx?Tab=Cert&Type=Ext&CertID="+ Utils.Encrypt(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "ID"))) %>'>View</asp:HyperLink>--%>
                            </ItemTemplate>
                        </rad:GridTemplateColumn>
                        <rad:GridDateTimeColumn UniqueName="DateAcquired" HeaderText="<%$ Resources:LocalizedResource, DateAcquired %>" DataField="DateAcquired"
                            DataType="System.DateTime" DataFormatString="{0:MMM dd, yyyy}" HeaderStyle-Font-Bold="true">
                        </rad:GridDateTimeColumn>
                        <rad:GridTemplateColumn UniqueName="ID" SortExpression="<%$ Resources:LocalizedResource, Badges %>" DataField="ID" ColumnEditorID="null"
                            HeaderText="Badges" HeaderStyle-Font-Bold="true">
                            <ItemTemplate>
                                <asp:DataList ID="repBadges" runat="server" RepeatLayout="Table" RepeatColumns="5"
                                    CellPadding="4" CellSpacing="4">
                                    <ItemTemplate>
                                        <a href="javascript:;" onclick="popovertrigger(this); return false;" class="popovers"
                                            data-placement="top" data-html="true" data-toggle="popover" data-trigger="focus"
                                            title="Badge Information" data-container="body" data-content="<div class='row'><div class='col-md-6'><img src='/Media/Images/<%# Eval("BadgeImgBig") %>' width='75' class='img-responsive'></div><div class='col-md-6'><%#Eval("BadgeName") %><br /><%#Eval("Description") %></div></div>">
                                            <asp:Image ID="Image1" ImageUrl='<%# "~/Media/Images/"+Eval("BadgeImgSmall") %>'
                                                runat="server" />
                                        </a>
                                        <table>
                                            <tr>
                                                <td colspan="5">
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:DataList>
                            </ItemTemplate>
                        </rad:GridTemplateColumn>
                    </Columns>
                </MasterTableView>
            </rad:RadGrid>
        </div>
        <div class="col-md-2">
        </div>
    </div>
    <div class="row" style="margin-top: 20px;">
        <div class="col-md-5">
            <asp:LinkButton ID="LinkBtnUploadCert" runat="server" CssClass="btn btn-teal" OnClick="LinkBtnUploadCert_Click"> <i class="fa fa-upload"></i> <asp:Label runat="server" ID="Label3" Text="<%$ Resources:LocalizedResource, UploadCertificate %>"></asp:Label></asp:LinkButton>
        </div>
    </div>
    <asp:Panel ID="pnlUploadCert" runat="server">
        <div id="div_upload_cert" runat="server" class="row" style="margin-top: 20px;">
            <div class="col-md-12">
                <%--      <div class="row">
                <asp:Label ID="fakeNotice" runat="server" Visible="false"></asp:Label>
            </div>--%>
                <asp:Panel ID="PanelUpload" runat="server" Visible="false">
                    <div class="row">
                        <div class="col-md-2 col-md-12">
                            <div class="bg-color-light-teal color-white font-bold five-px-padding">
                                <asp:Label runat="server" ID="Label4" Text="<%$ Resources:LocalizedResource, CreateEntry %>"></asp:Label>
                            </div>
                        </div>
                        <div class="col-md-10">
                        </div>
                    </div>
                    <div class="row" style="margin-bottom: 10px;">
                        <div class="col-md-4 col-md-12">
                            <div class="border-top-light-teal">
                                <div class="row">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                        </div>
                    </div>
                    <div class="form-horizontal">
                        <div class="form-group">
                            <div class="col-md-5">
                                <asp:TextBox TextMode="MultiLine" ID="txtCertificateName" runat="server" CssClass="form-control"
                                    onkeyup="CheckMaxLength(this,50);" ondragstart="return false;" ondrop="return false;"
                                    placeholder="<%$ Resources:LocalizedResource, Typecertificatename %>" MaxLength="50"></asp:TextBox>
                                <label class="text-danger text-Chars">
                                    <span runat="server" id="txtCertificateNameChars">50</span> <asp:Label runat="server" ID="Label5" Text="<%$ Resources:LocalizedResource, CharactersRemaining %>"></asp:Label></label>
                            </div>
                            <div class="col-md-5">
                                <asp:RequiredFieldValidator ID="rfvCertName" runat="server" ControlToValidate="txtCertificateName"
                                    SetFocusOnError="true" ValidationGroup="uploadExtCert" Display="Dynamic" ForeColor="Red"
                                    ErrorMessage="<%$ Resources:LocalizedResource, PleaseEntertheCertificateName %>">
                                </asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" ID="Label6" CssClass="col-sm-3 form-label" Text="<%$ Resources:LocalizedResource, Whendidyouacquirethiscertificate %>"></asp:Label>
                            <div class="col-md-2">
                                <rad:RadDatePicker ID="txtDateAcquired" runat="server" RenderMode="Lightweight" ToolTip="Set Acquired Date"
                                    CssClass="left-calendar-icon width-fill">
                                    <DateInput ReadOnly="true" runat="server">
                                    </DateInput>
                                    <Calendar ID="Calendar2" runat="server">
                                        <SpecialDays>
                                            <rad:RadCalendarDay Repeatable="Today" ItemStyle-BackColor="Gray">
                                            </rad:RadCalendarDay>
                                        </SpecialDays>
                                    </Calendar>
                                    <ClientEvents OnDateSelected="validateDateAcquired" />
                                </rad:RadDatePicker>
                            </div>
                            <div class="col-md-6">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="<%$ Resources:LocalizedResource, Enterthedateyouacquiredthiscertificate %>"
                                    SetFocusOnError="true" Display="Dynamic" ForeColor="Red" ControlToValidate="txtDateAcquired"
                                    ValidationGroup="uploadExtCert"></asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="cvDate" runat="server" ErrorMessage="<%$ Resources:LocalizedResource, DateAcquiredshouldnotbebegreaterthandatetoday %>"
                                    SetFocusOnError="true" Display="Dynamic" ForeColor="Red" ControlToValidate="txtDateAcquired"
                                    ClientValidationFunction="validateDateAcquired" ValidationGroup="uploadExtCert"></asp:CustomValidator>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" ID="Label7" CssClass="col-sm-2 form-label" Text="<%$ Resources:LocalizedResource, CertificateImage %>"></asp:Label>
                            <div class="col-md-3 display-inline-flex text-right">
                                <asp:Button Text="<%$ Resources:LocalizedResource, ChooseFile %>" runat="server" OnClientClick="openFileWindow(); return false;"
                                    Height="25px" />
                                &nbsp;&nbsp;&nbsp;
                                <rad:RadAsyncUpload ID="ImgFileUpload" runat="server" MultipleFileSelection="Disabled"
                                    ViewStateMode="Enabled" MaxFileInputsCount="1" ControlObjectsVisibility="None"
                                    Width="100%" ToolTip="<%$ Resources:LocalizedResource, SelectCertificateImage %>" OnClientFileSelected="fileSelected"
                                    AllowedFileExtensions="jpeg,jpg,png" CssClass="uploader-noselect">
                                </rad:RadAsyncUpload>
                            </div>
                            <div class="col-md-6">
                                <asp:CustomValidator runat="server" ID="CustomValidator" ClientValidationFunction="validateUploadCert"
                                    ErrorMessage="<%$ Resources:LocalizedResource, PleaseSelectaCertificatetoUpload %>" ValidationGroup="uploadExtCert"
                                    SetFocusOnError="true" Display="Dynamic" ForeColor="Red">
                                </asp:CustomValidator>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <rad:RadButton ID="BtnSaveUpload" runat="server" Text="<%$ Resources:LocalizedResource, SaveOnly %>" CssClass="btn btn-teal"
                                    OnClientClicking="validateCertificateUpload" OnClick="BtnSaveUpload_Click" RenderMode="Lightweight"
                                    Skin="Bootstrap">
                                </rad:RadButton>
                                <%--  <asp:Button ID="BtnSaveUpload" runat="server" Text="Save" CssClass="btn btn-teal"
                                OnClick="BtnSaveUpload_Click" ValidationGroup="dateValid" />--%>
                                &nbsp;
                                <asp:LinkButton ID="lbtnCancel" runat="server" Text="<%$ Resources:LocalizedResource, Cancel %>" CssClass="btn btn-teal"
                                    OnClick="Cancel_Click1" />
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </div>
    </asp:Panel>
</div>
<rad:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true" RenderMode="Lightweight"
    Skin="Bootstrap">
</rad:RadWindowManager>
<rad:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        function fileSelected(sender, args) {
            var fileExtention = args.get_fileName().substring(args.get_fileName().lastIndexOf('.') + 1, args.get_fileName().length);
            if (args.get_fileName().lastIndexOf('.') != -1) {//this checks if the extension is correct
                if (sender.get_allowedFileExtensions().toLowerCase().indexOf(fileExtention.toLowerCase()) == -1) {
                    var RadAlertInvalidFileMessage = $("#<%=hdnRadAlertInvalidFileMessage.ClientID%>").val();
                    var RadAlertInvalidFileError = $("#<%=hdnRadAlertInvalidFileError.ClientID%>").val();
                    radalert(RadAlertInvalidFileMessage, 330, 180, RadAlertInvalidFileError, "");
                    sender.deleteAllFileInputs();
                }
                else {
                    console.log("File Supported.");
                }
            }
            else {
                sender.deleteAllFileInputs();
                var RadAlertInvalidFileMessage = $("#<%=hdnRadAlertInvalidFileMessage.ClientID%>").val();
                var RadAlertInvalidFileError = $("#<%=hdnRadAlertInvalidFileError.ClientID%>").val();
                radalert(RadAlertInvalidFileMessage, 330, 180, RadAlertInvalidFileError, "");
            }
        }

        function openFileWindow() {
            $telerik.$(".ruFileInput").click();
        }

        function validateCertificateUpload(sender, args) {
            debugger;
            console.log("dv");
            var validated = Page_ClientValidate('uploadExtCert');
            if (validated) {
                sender.click();
                if (Telerik.Web.Browser.ff) { //work around a FireFox issue with form submission
                    sender.get_element().click();
                }
            } else {
                //$find("<%= txtDateAcquired.ClientID %>").get_dateInput().focus();

                args.set_cancel(true);
                return;

            }


        }
        function validateDateAcquired(sender, args) {
            debugger;
            var dateAc = $find("<%= txtDateAcquired.ClientID %>").get_selectedDate();
            var serverDate = new Date($("#masterServerDate").val());
            var valid = (dateAc <= serverDate);
            args.IsValid = valid;
            if (valid) {
                console.log("dv");
                $find("<%= txtDateAcquired.ClientID %>").get_dateInput().focus();
                //radalert("")
            }
        }

        function validateUploadCert(sender, args) {
            debugger;
            var upload = $find("<%= ImgFileUpload.ClientID %>");
            args.IsValid = upload.getUploadedFiles().length != 0;
        }

        function CheckMaxLength(sender, Maxlength) {
            var length = $(sender).val().length;
            if (sender.value.length > Maxlength) {

                sender.value = sender.value.substr(0, Maxlength);
            }

            var length = Maxlength - length;

            if (sender.name == "ctl00$contentPlaceHolderMain$ACHIEVEMENTS_userControl$txtCertificateName") {
                $('#contentPlaceHolderMain_ACHIEVEMENTS_userControl_txtCertificateNameChars').text(length);
            }

        }

        function openInternalNewTab(certID) {
            var url = "Certificate.aspx?tab=int&CertID=" + certID;
            openNonScormNewTab(url);
        }

        function openExternalNewTab(certID) {
            var url = "Certificate.aspx?tab=ext&CertID=" + certID;
            openNonScormNewTab(url);
        }

        function openNonScormNewTab(url) {
            debugger;
           
            var popUp = window.open(url);
            if (popUp == null || typeof (popUp) == 'undefined') {
                alert($('#<%= hfPleasedisableyourpopupblockerorallowPopuptoloadthecourseresourceandclickthelinkagain.ClientID %>').val());
                return;
            }
            else {
                popUp.focus();
            }
        }
       
    </script>
</rad:RadScriptBlock>
<asp:HiddenField runat="server" ID="hfcourseID" Value="<%$ Resources:LocalizedResource, Error %>" />
<asp:HiddenField runat="server" ID="hdnRadAlertInvalidFileMessage" Value="<%$ Resources:LocalizedResource, InvalidFileTypeAllowedFilesareJPGJPEGPNG %>" />
<asp:HiddenField runat="server" ID="hdnRadAlertInvalidFileError" Value="<%$ Resources:LocalizedResource, Error %>" />
<asp:HiddenField ID="hfPleasedisableyourpopupblockerorallowPopuptoloadthecourseresourceandclickthelinkagain" runat="server" value="<%$ Resources:LocalizedResource, PleasedisableyourpopupblockerorallowPopuptoloadthecourseresourceandclickthelinkagain%>"/>