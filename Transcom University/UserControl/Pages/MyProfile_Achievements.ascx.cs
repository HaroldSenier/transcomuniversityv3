﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using TranscomUniversityV3Model;
using System.IO;
using System.Data;


public partial class UserControl_Pages_MyProfile_Achievements : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
       
        gridInternal.Rebind();
        gridExternal.Rebind();

        //if(!Page.IsPostBack)
        //    LoadCourseCategory();
    }

    #region MyCertificaton

    void GetAllIntCerts(int? CourseID)
    {
            var googleId = DataHelper.GetGoogleID();
            var ds = DataHelper.GetAllUserIntCerts(googleId, CourseID);
            foreach (DataRow dr in ds.Tables[0].Rows) {
                dr["encryptedID"] = Utils.Encrypt(Convert.ToInt32(dr["ID"].ToString()));
               }
            gridInternal.DataSource = ds;
            gridInternal.DataBind();
     }

    void GetAllExtCerts()
    {
       
            var googleId = DataHelper.GetGoogleID();
            
            var ds =  DataHelper.GetAllUserExtCerts(googleId);
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                dr["encryptedID"] = Utils.Encrypt(Convert.ToInt32(dr["ID"].ToString()));
            }
            gridExternal.DataSource = ds;
            gridExternal.DataBind();
        
    }

    protected void gridExternal_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        if (e.RebindReason == Telerik.Web.UI.GridRebindReason.ExplicitRebind || e.RebindReason == Telerik.Web.UI.GridRebindReason.PostBackEvent)
        {
            try
            {
                GetAllExtCerts();
            }
            catch (Exception ex)
            {
                gridExternal.Controls.Add(new LiteralControl(string.Format("<strong style='color: red'>Error fetching records: {0}</strong>", ex.Message)));
            }
        }
    }

    protected void gridExternal_PreRender(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //radNews.EditIndexes.Add(0);
            gridExternal.Rebind();
        }
    }

    protected void rptPlaylist_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            var hfID = e.Item.NamingContainer.NamingContainer.FindControl("ID") as HiddenField;
        }
    }

    protected void gridExternal_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            GridDataItem item = (GridDataItem)e.Item;
            string Id = item.OwnerTableView.DataKeyValues[item.ItemIndex]["ID"].ToString();

            DataList rptTest = item.FindControl("repBadges") as DataList;

            rptTest.DataSource = DataHelper.GetAllCertBadges(Convert.ToInt32(Id));
            //rptTest.DataBind();
        }
    }

    protected void gridInternal_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        if (e.RebindReason == Telerik.Web.UI.GridRebindReason.ExplicitRebind || e.RebindReason == Telerik.Web.UI.GridRebindReason.PostBackEvent)
        {
            try
            {
                GetAllIntCerts(null);
            }
            catch (Exception ex)
            {
                gridInternal.Controls.Add(new LiteralControl(string.Format("<strong style='color: red'>Error fetching records: {0}</strong>", ex.Message)));
            }
        }
    }

    protected void gridInternal_PreRender(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //radNews.EditIndexes.Add(0);
            gridInternal.Rebind();
        }
    }

    protected void gridInternal_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            GridDataItem item = (GridDataItem)e.Item;
            string Id = item.OwnerTableView.DataKeyValues[item.ItemIndex]["ID"].ToString();

            DataList rptTest = item.FindControl("repBadges") as DataList;

            rptTest.DataSource = DataHelper.GetAllCertBadges(Convert.ToInt32(Id));
            //rptTest.DataBind();


            //var linkTitle = item.FindControl("btnLaunch") as Button;

            //linkTitle.Attributes["onclick"] = directoryPath;
        }
    }

    protected void LinkBtnUploadCert_Click(object sender, EventArgs e)
    {
        var CertificateNamechars = 50 - txtCertificateName.Text.Length;
        txtCertificateNameChars.InnerText = CertificateNamechars.ToString();

        PanelUpload.Visible = true;
        LinkBtnUploadCert.Visible = false;
    }

    protected void Cancel_Click1(object sender, EventArgs e)
    {
        txtCertificateName.Text = "";
        txtDateAcquired.Clear();

        PanelUpload.Visible = false;
        LinkBtnUploadCert.Visible = true;
    }

    protected void BtnSaveUpload_Click(object sender, EventArgs e)
    {

        try
        {

            if (ImgFileUpload.UploadedFiles.Count == 0)
            {
                RadWindowManager1.RadAlert(Resources.LocalizedResource.Pleaseselectanimage.ToString(), 330, 180, Resources.LocalizedResource.Noimageselected.ToString(), "");
                return;
            }

            UploadedFile f = ImgFileUpload.UploadedFiles[0];
            if (f.ContentType == "image/jpeg" || f.ContentType == "image/png" || f.ContentType == "image/jpg")
            {
                string CertName = txtCertificateName.Text;
                string error = null;

                if (string.IsNullOrWhiteSpace(CertName))
                {
                    error += Resources.LocalizedResource.Certificatenamecannotbeempty.ToString() + "<br />";
                }

                if (txtDateAcquired.DbSelectedDate == null)
                {
                    error += Resources.LocalizedResource.Dateacquiredcannotbeempty.ToString() + "<br />";
                }

                if (string.IsNullOrWhiteSpace(error))
                {


                    string uploadFolder;
                    var googleId = DataHelper.GetGoogleID();

                    if (HttpContext.Current.Request.Url.Host == "localhost")
                        uploadFolder = Request.PhysicalApplicationPath + "Media\\Uploads\\Certs\\" + googleId + "\\";
                    else
                        uploadFolder = HttpRuntime.AppDomainAppPath + "Media\\Uploads\\Certs\\" + googleId + "\\";

                    string directoryPath = Server.MapPath(string.Format("~/{0}/", "Media/Uploads/Certs/" + googleId));


                    if (!Directory.Exists(directoryPath))
                    {
                        Directory.CreateDirectory(directoryPath);
                    }


                    if (f.ContentLength < 1024000000)
                    {
                        Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                        string extension = Path.GetExtension(f.FileName);
                        f.SaveAs(uploadFolder + unixTimestamp.ToString() + f.GetExtension(), true);
                        
                        DataHelper.InsertCert(googleId, CertName, "", Convert.ToDateTime(txtDateAcquired.SelectedDate), null, unixTimestamp.ToString() + extension, true);
                    }

                    ClearForm();
                    GetAllIntCerts(null);
                    GetAllExtCerts();
                    RadWindowManager1.RadAlert(Resources.LocalizedResource.Certificatesuccessfullyuploaded.ToString(), 330, 180, Resources.LocalizedResource.Success.ToString(), "");
                }
                else
                {
                    
                    RadWindowManager1.RadAlert(error, 330, 180, Resources.LocalizedResource.Pleasefixthefollowingerrors.ToString(), "");
                }
            }
            else
            {
                RadWindowManager1.RadAlert(Resources.LocalizedResource.Errorfileformatpleaseselectanotherone.ToString(), 330, 180, Resources.LocalizedResource.InvalidFileFormat.ToString(), "");
            }
        }
        catch (Exception)
        {

            RadWindowManager1.RadAlert(Resources.LocalizedResource.ErrorinuploadingimagePleasecontactAdministrator.ToString(), 330, 180, Resources.LocalizedResource.SystemError.ToString(), "");
            throw;
        }


    }

    void ClearForm()
    {
        txtCertificateName.Text = "";
        txtDateAcquired.SelectedDate = null;
        ImgFileUpload.UploadedFiles.Clear();
    }

    void LoadCourseCategory()
    {
        cbCourseList.DataSource = DataHelper.GetAllCourseCategory();
        cbCourseList.DataTextField = "Name";
        cbCourseList.DataValueField = "ID";
        cbCourseList.DataBind();
    }

    protected void cbCourseList_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        GetAllIntCerts(Convert.ToInt32(cbCourseList.SelectedValue));
    }  


    #endregion
}