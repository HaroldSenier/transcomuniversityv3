﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MyProfile_CareerMap.ascx.cs" Inherits="UserControl_Pages_MyProfile_CareerMap" %>

 <div id="LearningMap">
                 <canvas
                   id="LearningMapC">
                  </canvas>
                </div>

<rad:RadScriptBlock ID="radScriptBlock1" runat="server" >
<script type="text/javascript">
    function LoadMap() {
        var count = 5;
        debugger;

        var arrCoordinates = [
                {
                    id: 0,
                    x: 724,
                    y: 252,
                    line: { x1: 724, y1: 252, x2: 602, y2: 193 },
                    imgSrc: "Media/Images/badges.png"
                },
                {
                    id: 1,
                    x: 602,
                    y: 193,
                    line: { x1: 602, y1: 193, x2: 449, y2: 166 },
                    imgSrc: "Media/Images/badges.png"
                },
                {
                    id: 2,
                    x: 449,
                    y: 166,
                    line: { x1: 449, y1: 166, x2: 250, y2: 120 },
                    imgSrc: "Media/Images/badges.png"
                },
                {
                    id: 3,
                    x: 250,
                    y: 120,
                    line: { x1: 250, y1: 120, x2: 400, y2: 82 },
                    imgSrc: "Media/Images/badges.png"
                },
                {
                    id: 4,
                    x: 400,
                    y: 82,
                    line: { x1: 400, y1: 82, x2: 630, y2: 74 },
                    imgSrc: "Media/Images/badges.png"
                },
                {
                    id: 5,
                    x: 630,
                    y: 74,
                    line: { x1: 630, y1: 74, x2: 778, y2: 68 },
                    imgSrc: "Media/Images/badges.png"
                },
                {
                    id: 6,
                    x: 778,
                    y: 68,
                    line: { x1: 778, y1: 68, x2: 690, y2: -17 },
                    imgSrc: "Media/Images/badges.png"
                },
                {
                    id: 7,
                    x: 690,
                    y: -17,
                    line: { x1: 690, y1: -17, x2: 510, y2: -30 },
                    imgSrc: "Media/Images/badges.png"
                },
                {
                    id: 8,
                    x: 510,
                    y: -30,
                    line: { x1: 510, y1: -30, x2: 350, y2: -50 },
                    imgSrc: "Media/Images/badges.png"
                },
                {
                    id: 9,
                    x: 350,
                    y: -50,
                    line: { x1: 350, y1: -50, x2: 500, y2: -110 },
                    imgSrc: "Media/Images/badges.png"
                },
                {
                    id: 10,
                    x: 500,
                    y: -110,
                    imgSrc: "Media/Images/badges.png"

                }
            ]

        var change = 150;
        var changex = 200;

        for (var i = 0; i <= count; i++) {
            if (i == count) {
                circle(arrCoordinates[10].x , arrCoordinates[10].y , arrCoordinates[10].imgSrc);
                //line(arrCoordinates[i].line.x1, arrCoordinates[i].line.y1, arrCoordinates[i].line.x2, arrCoordinates[i].line.y2);
            } else {
                circle(arrCoordinates[i].x, arrCoordinates[i].y, arrCoordinates[i-1].imgSrc);
                if (i + 1 != count)
                    line(arrCoordinates[i].line.x1 , arrCoordinates[i].line.y1, arrCoordinates[i].line.x2, arrCoordinates[i].line.y2 );
                else
                    line(arrCoordinates[i].line.x1 , arrCoordinates[i].line.y1 , 500 , -110 );
            }
        }

        var canvas = document.getElementById('LearningMapC')

        // Add event listener to canvas element
        canvas.addEventListener('click', function (event) {
            // get canvasXY of click
            canvasMouseX = parseInt(event.clientX - offsetX);
            canvasMouseY = parseInt((event.clientY + scrolltop) - offsetY);
            debugger;
            //alert(canvasMouseX + " ," + canvasMouseY)
            for (var i = 0; i <= count; i++) {
                var dx = canvasMouseX - arrCoordinates[i].x;
                var dy = canvasMouseY - arrCoordinates[i].y;

                if (i == count && count != 0) {
                    dx = canvasMouseX - arrCoordinates[10].x;
                    dy = canvasMouseY - arrCoordinates[10].y;
                }

                var isValid = (dx * dx + dy * dy) < (40 * 40);
                if (isValid) {
                    //alert("You clicked in the " + arrCoordinates[i].id);
                    if (count > 0) {
                        showCourseDetails(result[i - 1]);

                        tooltip.style.left = (dx) + "px";
                        tooltip.style.top = (dy - 40) + "px";
                        tooltipContext.clearRect(0, 0, tooltip.width, tooltip.height);
                        //                  tipCtx.rect(0,0,tipCanvas.width,tipCanvas.height);
                        tooltipContext.fillText("Title: " + result[i - 1].CourseTitle, 5, 15);
                        tooltipContext.fillText("Category: " + result[i - 1].CourseCategory, 5, 23);
                        tooltipContext.fillText("Subcategory: " + result[i - 1].CourseSubCategory, 5, 31);
                        tooltip.style.display = "block";
                    }
                }
                else
                    tooltip.style.display = "none";
            }
        });
    }

    function line(x1, y1, x2, y2) {
        var canvas = document.getElementById("LearningMapC");
        var ctx = canvas.getContext("2d");
        ctx.moveTo(x1, y1);
        ctx.lineTo(x2, y2);
        ctx.lineWidth = 7;
        ctx.strokeStyle = "#D1453C";
        ctx.stroke();

    }

    function circle(x, y, imgSrc) {
        var canvas = document.getElementById('LearningMapC')
        var ctx2 = canvas.getContext('2d');
        var canvasOffset = $("#LearningMapC").offset();
        _offsetX = canvasOffset.left;
        _offsetY = canvasOffset.top;
        //y += 65;
        var imageObj = new Image();
        imageObj.onload = function () {
            ctx2.save();
            ctx2.beginPath();
            ctx2.arc(x, y, 40, 0, 2 * Math.PI, true);
            ctx2.closePath();
            ctx2.clip();

            ctx2.drawImage(imageObj, x - 20, y - 20, 40, 40);

            ctx2.beginPath();
            ctx2.arc(x, y, 40, 0, 2 * Math.PI, true);
            ctx2.clip();
            ctx2.closePath();
            ctx2.restore();


        };
        imageObj.src = imgSrc;
    }
    </script>

</rad:RadScriptBlock>
