﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MyProfile_LearningPlan.ascx.cs"
    Inherits="UserControl_Pages_MyProfile_LearningPlan" %>
<%@ Register Src="~/UserControl/Pages/MyProfile_CareerMap.ascx" TagName="CarreerMap"
    TagPrefix="uc1" %>
<rad:RadScriptBlock ID="rsb1" runat="server">
    <script type="text/javascript">
        var mandatoryListView;
        var mandatoryGridView;
        var skillDevelopmentListView;
        var skillDevelopmentGridView;
        var recommendedListView;
        var recommendedGridView;


        Sys.Application.add_load(function () {
            $(".RadMultiPage").css({ "min-height": "1000px" });
            debugger;
        });

        function pageLoad() {

            //            $('.modal-child').on('show.bs.modal', function () {
            //                var modalParent = $(this).attr('data-modal-parent');
            //                $(modalParent).css('opacity', 0);
            //            });
            debugger;
            $('.modal-child').on('hidden.bs.modal', function () {
                var modalParent = $(this).attr('data-modal-parent');
                $(modalParent).css('opacity', 1);
            });
            initializeRecommendedCourse();

            //            initializeSkillDevelopmentCourse();
            //            initializeMandatoryCourse_PS();
            //            initializeMandatoryCourse_TW();
            //            initializeMandatoryCourse_RG();
            //            initializeMandatoryCourse_CT();

            $(".list_pager").each(function () {
                if ($(this).html().trim() == "Previous") {
                    $(this).addClass("fa fa-chevron-left pull-left relative-vr-left-center gray-arrow no-underline-hover")
                    $(this).html("");
                }
                if ($(this).html().trim() == "Next") {
                    $(this).addClass("fa fa-chevron-right pull-right relative-vr-right-center gray-arrow no-underline-hover")
                    $(this).html("");
                }
            });


        }

        //Mandatory Program Specific
        function showMandatoryLoading_PS() {
            $(".mc-container-ps .overlay").removeClass("display-none");
            $(".mc-container-ps .js-mc-loader").show();
        }

        function hideMandatoryLoading_PS() {
            $(".mc-container-ps .overlay").addClass("display-none");
            $(".mc-container-ps.js-mc-loader").hide();
        }

        //initialize mandatory Course
        function initializeMandatoryCourse_PS() {
            //this will fire the OnCommand event
            var mandatoryListView = $find("<%= lvMandatoryCourses_ps.ClientID%>");
            mandatoryListView.rebind();
            mandatoryListView.set_pageSize(5);
            mandatoryListView.page(0);



            $(".mcPager_ps .pagePrev").click(function (e) {
                showMandatoryLoading_PS();
                mandatoryListView.page(mandatoryListView.get_currentPageIndex() - 1);
            });

            $(".mcPager_ps .pageNext").click(function (e) {
                showMandatoryLoading_PS();
                mandatoryListView.page(mandatoryListView.get_currentPageIndex() + 1);
            });
        }

        function bindMandatoryCourseList_PS() {
            var mandatoryListView = $find("<%= lvMandatoryCourses_ps.ClientID %>");
            var mandatoryGridView = $find("<%= gridMandatoryCourses_ps.ClientID%>");
            var startRowIndex = mandatoryListView.get_currentPageIndex() * mandatoryListView.get_pageSize(),
            maximumRows = mandatoryListView.get_pageSize(),
            sortExpression = mandatoryListView.get_sortExpressions().toLinq();

            ///////////////////////////////////////////////////
            $.ajax({
                type: "POST",
                data: '{startRowIndex: ' + startRowIndex + ', maximumRows: ' + maximumRows + ', sortExpression: "' + sortExpression + '", user: "<%= HttpContext.Current.User.Identity.Name.Split('|')[0] %>", subcatID:' + 28 + '}',
                contentType: "application/json; charset=utf-8",
                url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "GetLearningPlanMandatoryCourse",
                dataType: 'json',
                success: function (res) {
                    var result = res.d.data;
                    var count = res.d.count;
                    console.log(res);
                    mandatoryListView.set_virtualItemCount(count);
                    if (count > 5) {
                        $(".mcPager_ps .pagePrev").removeClass("display-none");
                        $(".mcPager_ps .pageNext").removeClass("display-none");

                    }

                    ////////////
                    mandatoryListView.set_dataSource(result);

                    $(".mc-container-ps .js-mc-loader").hide();
                    mandatoryListView.dataBind();

                    mandatoryGridView.set_dataSource(result);
                    mandatoryGridView.dataBind();

                    hideMandatoryLoading_PS();

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var code = XMLHttpRequest.status;
                    if (code == "401")
                        window.location.reload();
                    else
                        console.log(code);
                }
            }).done(function () {
                initializeMandatoryCourse_TW();
                //            initializeMandatoryCourse_RG();
                //            initializeMandatoryCourse_CT();
            });

            ///////////////////////////////////////////////////
        }

        //Client Events mandatoryCourse

        function mcOnListViewDataBinding_ps(sender, args) {
            sender.set_selectedIndexes([]);
        }

        function mcOnListViewCommand_ps(sender, args) {
            args.set_cancel(true);
            bindMandatoryCourseList_PS();
        }

        //Mandatory Transcom Wide
        function showMandatoryLoading_TW() {
            $(".mc-container-tw .overlay").removeClass("display-none");
            $(".mc-container-tw .js-mc-loader").show();
        }

        function hideMandatoryLoading_TW() {
            $(".mc-container-tw .overlay").addClass("display-none");
            $(".mc-container-tw .js-mc-loader").hide();
        }

        //initialize mandatory Course
        function initializeMandatoryCourse_TW() {
            $ = $telerik.$;
            //this will fire the OnCommand event
            var mandatoryListView = $find("<%= lvMandatoryCourses_tw.ClientID%>");
            mandatoryListView.rebind();
            mandatoryListView.set_pageSize(5);
            mandatoryListView.page(0);



            $(".mcPager_tw .pagePrev").click(function (e) {
                showMandatoryLoading_TW();
                mandatoryListView.page(mandatoryListView.get_currentPageIndex() - 1);
            });

            $(".mcPager_tw .pageNext").click(function (e) {
                showMandatoryLoading_TW();
                mandatoryListView.page(mandatoryListView.get_currentPageIndex() + 1);
            });
        }

        function bindMandatoryCourseList_TW() {
            var mandatoryListView = $find("<%= lvMandatoryCourses_tw.ClientID %>");
            var mandatoryGridView = $find("<%= gridMandatoryCourses_tw.ClientID%>");
            var startRowIndex = mandatoryListView.get_currentPageIndex() * mandatoryListView.get_pageSize(),
            maximumRows = mandatoryListView.get_pageSize(),
            sortExpression = mandatoryListView.get_sortExpressions().toLinq();


            ///////////////////////////////////////////////////
            $.ajax({
                type: "POST",
                data: '{startRowIndex: ' + startRowIndex + ', maximumRows: ' + maximumRows + ', sortExpression: "' + sortExpression + '", user: "<%= HttpContext.Current.User.Identity.Name.Split('|')[0] %>", subcatID:' + 29 + '}',
                contentType: "application/json; charset=utf-8",
                url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "GetLearningPlanMandatoryCourse",
                dataType: 'json',
                success: function (res) {
                    var result = res.d.data;
                    var count = res.d.count;
                    console.log(res);
                    mandatoryListView.set_virtualItemCount(count);
                    if (count > 5) {
                        $(".mcPager_tw .pagePrev").removeClass("display-none");
                        $(".mcPager_tw .pageNext").removeClass("display-none");

                    }

                    ////////////
                    mandatoryListView.set_dataSource(result);

                    $(".mc-container-tw .js-mc-loader").hide();
                    mandatoryListView.dataBind();

                    mandatoryGridView.set_dataSource(result);
                    mandatoryGridView.dataBind();

                    hideMandatoryLoading_TW();

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var code = XMLHttpRequest.status;
                    if (code == "401")
                        window.location.reload();
                    else
                        console.log(code);
                }
            }).done(function () {
                initializeMandatoryCourse_RG();
                //            initializeMandatoryCourse_CT();
            });

            ///////////////////////////////////////////////////
        }

        //Client Events mandatoryCourse

        function mcOnListViewDataBinding_tw(sender, args) {
            sender.set_selectedIndexes([]);
        }

        function mcOnListViewCommand_tw(sender, args) {
            args.set_cancel(true);
            bindMandatoryCourseList_TW();
        }


        //Mandatory Regional COurses
        function showMandatoryLoading_RG() {
            $(".mc-container-rg .overlay").removeClass("display-none");
            $(".mc-container-rg .js-mc-loader").show();
        }

        function hideMandatoryLoading_RG() {
            $(".mc-container-rg .overlay").addClass("display-none");
            $(".mc-container-rg .js-mc-loader").hide();
        }

        //initialize mandatory Course
        function initializeMandatoryCourse_RG() {
            $ = $telerik.$;
            //this will fire the OnCommand event
            var mandatoryListView = $find("<%= lvMandatoryCourses_rg.ClientID%>");
            mandatoryListView.rebind();
            mandatoryListView.set_pageSize(5);
            mandatoryListView.page(0);

            $(".mcPager_rg .pagePrev").click(function (e) {
                showMandatoryLoading_RG();
                mandatoryListView.page(mandatoryListView.get_currentPageIndex() - 1);
            });

            $(".mcPager_rg .pageNext").click(function (e) {
                showMandatoryLoading_RG();
                mandatoryListView.page(mandatoryListView.get_currentPageIndex() + 1);
            });
        }

        function bindMandatoryCourseList_RG() {
            var mandatoryListView = $find("<%= lvMandatoryCourses_rg.ClientID %>");
            var mandatoryGridView = $find("<%= gridMandatoryCourses_rg.ClientID%>");
            var startRowIndex = mandatoryListView.get_currentPageIndex() * mandatoryListView.get_pageSize(),
            maximumRows = mandatoryListView.get_pageSize(),
            sortExpression = mandatoryListView.get_sortExpressions().toLinq();


            ///////////////////////////////////////////////////
            $.ajax({
                type: "POST",
                data: '{startRowIndex: ' + startRowIndex + ', maximumRows: ' + maximumRows + ', sortExpression: "' + sortExpression + '", user: "<%= HttpContext.Current.User.Identity.Name.Split('|')[0] %>", subcatID:' + 30 + '}',
                contentType: "application/json; charset=utf-8",
                url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "GetLearningPlanMandatoryCourse",
                dataType: 'json',
                success: function (res) {
                    var result = res.d.data;
                    var count = res.d.count;
                    console.log(res);
                    mandatoryListView.set_virtualItemCount(count);
                    if (count > 5) {
                        $(".mcPager .pagePrev").removeClass("display-none");
                        $(".mcPager .pageNext").removeClass("display-none");

                    }

                    ////////////
                    mandatoryListView.set_dataSource(result);

                    $(".mc-container-rg .js-mc-loader").hide();
                    mandatoryListView.dataBind();

                    mandatoryGridView.set_dataSource(result);
                    mandatoryGridView.dataBind();

                    hideMandatoryLoading_RG();

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var code = XMLHttpRequest.status;
                    if (code == "401")
                        window.location.reload();
                    else
                        console.log(code);
                }
            }).done(function () {
                initializeMandatoryCourse_CT();
            });

            ///////////////////////////////////////////////////
        }

        //Client Events mandatoryCourse

        function mcOnListViewDataBinding_rg(sender, args) {
            sender.set_selectedIndexes([]);
        }

        function mcOnListViewCommand_rg(sender, args) {
            args.set_cancel(true);
            bindMandatoryCourseList_RG();
        }

        //Mandatory Country Courses
        function showMandatoryLoading_CT() {
            $(".mc-container-ct .overlay").removeClass("display-none");
            $(".mc-container-ct .js-mc-loader").show();
        }

        function hideMandatoryLoading_CT() {
            $(".mc-container-ct .overlay").addClass("display-none");
            $(".mc-container-ct .js-mc-loader").hide();
        }

        //initialize mandatory Course
        function initializeMandatoryCourse_CT() {
            $ = $telerik.$;
            //this will fire the OnCommand event
            var mandatoryListView = $find("<%= lvMandatoryCourses_ct.ClientID%>");
            mandatoryListView.rebind();
            mandatoryListView.set_pageSize(5);
            mandatoryListView.page(0);



            $(".mcPager_ct .pagePrev").click(function (e) {
                showMandatoryLoading_CT();
                mandatoryListView.page(mandatoryListView.get_currentPageIndex() - 1);
            });

            $(".mcPager_ct .pageNext").click(function (e) {
                showMandatoryLoading_CT();
                mandatoryListView.page(mandatoryListView.get_currentPageIndex() + 1);
            });
        }

        function bindMandatoryCourseList_CT() {
            var mandatoryListView = $find("<%= lvMandatoryCourses_ct.ClientID %>");
            var mandatoryGridView = $find("<%= gridMandatoryCourses_ct.ClientID%>");
            var startRowIndex = mandatoryListView.get_currentPageIndex() * mandatoryListView.get_pageSize(),
            maximumRows = mandatoryListView.get_pageSize(),
            sortExpression = mandatoryListView.get_sortExpressions().toLinq();


            ///////////////////////////////////////////////////
            $.ajax({
                type: "POST",
                data: '{startRowIndex: ' + startRowIndex + ', maximumRows: ' + maximumRows + ', sortExpression: "' + sortExpression + '", user: "<%= HttpContext.Current.User.Identity.Name.Split('|')[0] %>", subcatID:' + 31 + '}',
                contentType: "application/json; charset=utf-8",
                url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "GetLearningPlanMandatoryCourse",
                dataType: 'json',
                success: function (res) {
                    var result = res.d.data;
                    var count = res.d.count;
                    console.log(res);
                    mandatoryListView.set_virtualItemCount(count);
                    if (count > 5) {
                        $(".mcPager_ct .pagePrev").removeClass("display-none");
                        $(".mcPager_ct .pageNext").removeClass("display-none");

                    }

                    ////////////
                    mandatoryListView.set_dataSource(result);

                    $(".mc-container-ct .js-mc-loader").hide();
                    mandatoryListView.dataBind();

                    mandatoryGridView.set_dataSource(result);
                    mandatoryGridView.dataBind();

                    hideMandatoryLoading_CT();

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var code = XMLHttpRequest.status;
                    if (code == "401")
                        window.location.reload();
                    else
                        console.log(code);
                }
            });

            ///////////////////////////////////////////////////
        }

        //Client Events mandatoryCourse

        function mcOnListViewDataBinding_ct(sender, args) {
            sender.set_selectedIndexes([]);
        }

        function mcOnListViewCommand_ct(sender, args) {
            args.set_cancel(true);
            bindMandatoryCourseList_CT();
        }



        //END MANDATORY

        //START SKILL DEV
        function showskillDevelopmentLoading() {
            $(".ps-container .overlay").removeClass("display-none");
            $(".js-ps-loader").show();
        }

        function hideskillDevelopmentLoading() {
            $(".ps-container .overlay").addClass("display-none");
            $(".js-ps-loader").hide();
        }

        function initializeSkillDevelopmentCourse() {
            $ = $telerik.$;
            //this will fire the OnCommand event
            skillDevelopmentListView = $find("<%= lvSkillDevelopmentCourses.ClientID%>");
            skillDevelopmentListView.rebind();
            skillDevelopmentListView.set_pageSize(5);
            skillDevelopmentListView.page(0);


            skillDevelopmentGridView = $find("<%= gridSkillDevelopmentCourses.ClientID %>");
            $(".psPager .pagePrev").click(function (e) {
                $(".ps-container overlay").show();
                showskillDevelopmentLoading();
                skillDevelopmentListView.page(skillDevelopmentListView.get_currentPageIndex() - 1);
            });

            $(".psPager .pageNext").click(function (e) {
                $(".ps-container overlay").show();
                showskillDevelopmentLoading();
                skillDevelopmentListView.page(skillDevelopmentListView.get_currentPageIndex() + 1);
            });

            $("#mandatoryListView").on("click", ".mc-item", function (e) {
                skillDevelopmentListView.toggleSelection($(this).index());
            });
        }

        function bindskillDevelopmentList() {
            var startRowIndex = skillDevelopmentListView.get_currentPageIndex() * skillDevelopmentListView.get_pageSize(),
            maximumRows = skillDevelopmentListView.get_pageSize(),
            sortExpression = skillDevelopmentListView.get_sortExpressions().toLinq();


            ///////////////////////////////////////////////////
            $.ajax({
                type: "POST",
                data: '{startRowIndex: ' + startRowIndex + ', maximumRows: ' + maximumRows + ', sortExpression: "' + sortExpression + '", user: "<%= HttpContext.Current.User.Identity.Name.Split('|')[0] %>"}',
                contentType: "application/json; charset=utf-8",
                url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "GetLearningPlanSkillDevelopment",
                dataType: 'json',
                success: function (res) {
                    var result = res.d.data;
                    var count = res.d.count;

                    skillDevelopmentListView.set_virtualItemCount(count);

                    if (count > 5) {
                        $(".psPager .pagePrev").removeClass("display-none");
                        $(".psPager .pageNext").removeClass("display-none");
                        $(".psViews").removeClass("display-none");
                    }

                    ////////////
                    skillDevelopmentListView.set_dataSource(result);
                    skillDevelopmentListView.dataBind();

                    skillDevelopmentGridView.set_dataSource(result);
                    skillDevelopmentGridView.dataBind();
                    $(".ps-container overlay").hide();
                    $(".js-ps-loader").hide();
                    hideskillDevelopmentLoading();

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var code = XMLHttpRequest.status;
                    if (code == "401")
                        window.location.reload();
                    else
                        alert(XMLHttpRequest.responseText);
                }
            }).done(function () {
                initializeMandatoryCourse_PS();
                //            initializeMandatoryCourse_TW();
                //            initializeMandatoryCourse_RG();
                //            initializeMandatoryCourse_CT();
            });

            ///////////////////////////////////////////////////
        }

        //Client Events mandatoryCourse

        function psOnListViewDataBinding(sender, args) {
            sender.set_selectedIndexes([]);
        }

        function psOnListViewCommand(sender, args) {
            args.set_cancel(true);
            bindskillDevelopmentList();
        }
        //END PROGRAM SPECIFIC

        //START LEADERSHIP RECOMMENDED COURSE
        function showRecommendedLoading() {
            $(".rc-container .overlay").removeClass("display-none");
            $(".js-rc-loader").show();
        }

        function hideRecommendedLoading() {
            $(".rc-container .overlay").addClass("display-none");
            $(".js-rc-loader").hide();
        }

        function initializeRecommendedCourse() {
            $ = $telerik.$;
            //this will fire the OnCommand event

            recommendedListView = $find("<%= lvRecommendedCourse.ClientID%>");
            recommendedListView.rebind();
            recommendedListView.set_pageSize(5);
            recommendedListView.page(0);

            recommendedGridView = $find("<%= gridRecommenedCourse.ClientID %>");
            $(".rcPager .pagePrev").click(function (e) {
                showRecommendedLoading();
                recommendedListView.page(recommendedListView.get_currentPageIndex() - 1);
            });

            $(".rcPager .pageNext").click(function (e) {
                showRecommendedLoading();
                $(".rc-container .overlay").show();
                recommendedListView.page(recommendedListView.get_currentPageIndex() + 1);
            });

            $("#recommendedListView").on("click", ".rc-item", function (e) {
                $(".rc-container .overlay").show();
                recommendedListView.toggleSelection($(this).index());
            });
        }

        function bindRecommendedList() {

            var startRowIndex = recommendedListView.get_currentPageIndex() * recommendedListView.get_pageSize(),
            maximumRows = recommendedListView.get_pageSize(),
            sortExpression = recommendedListView.get_sortExpressions().toLinq();
            debugger;
            ///////////////////////////////////////////////////
            $.ajax({
                type: "POST",
                data: '{startRowIndex: ' + startRowIndex + ', maximumRows: ' + maximumRows + ', sortExpression: "' + sortExpression + '", user: "<%= HttpContext.Current.User.Identity.Name.Split('|')[0] %>"}',
                contentType: "application/json; charset=utf-8",
                url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "GetLearningPlanLeadershipRecommended",
                dataType: 'json',
                success: function (res) {
                    console.log(res);
                    var result = res.d.data;
                    var count = res.d.count;

                    recommendedListView.set_virtualItemCount(count);

                    if (count > 5) {
                        $(".rcPager .pagePrev").removeClass("display-none");
                        $(".rcPager .pageNext").removeClass("display-none");
                        $(".rcViews").removeClass("display-none");
                    }

                    ////////////
                    recommendedListView.set_dataSource(result);
                    recommendedListView.dataBind();

                    recommendedGridView.set_dataSource(result);
                    recommendedGridView.dataBind();
                    $(".rc-container .overlay").hide();
                    $(".js-rc-loader").hide();
                    hideskillDevelopmentLoading();
                    console

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var code = XMLHttpRequest.status;
                    if (code == "401")
                        window.location.reload();
                    else
                        alert(XMLHttpRequest.responseText);
                }
            }).done(function () {
                initializeSkillDevelopmentCourse();
                //            initializeMandatoryCourse_PS();
                //            initializeMandatoryCourse_TW();
                //            initializeMandatoryCourse_RG();
                //            initializeMandatoryCourse_CT();
            });

            ///////////////////////////////////////////////////
        }

        //Client Events mandatoryCourse

        function rcOnListViewDataBinding(sender, args) {
            sender.set_selectedIndexes([]);
        }

        function rcOnListViewCommand(sender, args) {
            args.set_cancel(true);
            bindRecommendedList();
        }
        //END LEADERSHIP RECOMMENDED COURSE

        function initializeProgressBar() {
            $('.progressBarContainer').each(function (i, obj) {

                var hidValue = jQuery('[id=hidProgressVal]', this).val();

                jQuery('[id=spanval]', this).text(parseFloat(hidValue) + '%');

                var progressBarWidth = (hidValue * $('#progressBar').width()) / 100;
                //alert("hidValue" + hidValue + "pbar" + $('#progressBar').width());
                jQuery('[id=progress]', this).animate({ width: progressBarWidth }, 3000).html("&nbsp;");

                if (hidValue >= 0 && hidValue <= 50) {
                    jQuery('[id=progress]', this).addClass('red');
                }
                if (hidValue >= 50 && hidValue <= 75) {
                    jQuery('[id=progress]', this).addClass('yellow');
                }
                if (hidValue >= 75 && hidValue <= 100) {
                    jQuery('[id=progress]', this).addClass('green');
                }
                //                console.log("progressBarContainer");
                //                console.log("hidValue = " + hidValue);
                //                console.log("progressBarWidth = " + progressBarWidth);
                //                console.log("progressBar = " + $('#progressBar').width());


            });
        }

        function setStatusFontColor() {
            $('.statusContainer').each(function (i, obj) {
                var statusId = jQuery('[id=hidStatus]', this).val();
                //console.log(statusId);
                if (statusId == 1)
                //Pending
                    jQuery('[id=status]', this).css({ "color": "#0000CC" });
                if (statusId == 2)
                //Approved
                    jQuery('[id=status]', this).css({ "color": "#d8c91e" });
                else if (statusId == 3 || statusId == 4 || statusId == 7 || statusId == 8)
                //Declined, Cancelled, Overdue, Cancelled By Assigner, 
                    jQuery('[id=status]', this).css({ "color": "#E51C23" });
                else if (statusId == 6)
                //Completed
                    jQuery('[id=status]', this).css({ "color": "#259B24" });
                else if (statusId == 9)
                //InProgress
                    jQuery('[id=status]', this).css({ "color": "#C9AE1C" });
                else
                //Not Started
                    jQuery('[id=status]', this).css({ "color": "#000" });
            });
        }

        function btnSwitchView_Click() {
            if (!$("#skillDevelopmentGridView").hasClass("display-none")) {
                //switch to list view

                $("#skillDevelopmentGridView").addClass("display-none");
                $("#skillDevelopmentListView").removeClass("display-none");

                $("#recommendedCourseGridView").addClass("display-none");
                $("#recommendedCourseListView").removeClass("display-none");

                $("#mandatoryGridView_ps").addClass("display-none");
                $("#mandatoryListView_ps").removeClass("display-none");

                $("#mandatoryGridView_tw").addClass("display-none");
                $("#mandatoryListView_tw").removeClass("display-none");

                $("#mandatoryGridView_rg").addClass("display-none");
                $("#mandatoryListView_rg").removeClass("display-none");

                $("#mandatoryGridView_ct").addClass("display-none");
                $("#mandatoryListView_ct").removeClass("display-none");

                $(".switchClassGridView").removeClass("display-none");
                $(".switchClassListView").addClass("display-none");

                return false;
            } else {

                //Gridview
                $("#skillDevelopmentGridView").removeClass("display-none");
                $("#skillDevelopmentListView").addClass("display-none");

                $("#recommendedCourseGridView").removeClass("display-none");
                $("#recommendedCourseListView").addClass("display-none");

                $("#mandatoryGridView_ps").removeClass("display-none");
                $("#mandatoryListView_ps").addClass("display-none");

                $("#mandatoryGridView_tw").removeClass("display-none");
                $("#mandatoryListView_tw").addClass("display-none");

                $("#mandatoryGridView_rg").removeClass("display-none");
                $("#mandatoryListView_rg").addClass("display-none");

                $("#mandatoryGridView_ct").removeClass("display-none");
                $("#mandatoryListView_ct").addClass("display-none");

                $(".switchClassGridView").addClass("display-none");
                $(".switchClassListView").removeClass("display-none");

                return false;
            }

        }

        function bindData(courseType, courseID) {
            var count = 0;
            var result = null;
            var sp = "GetLearningPlanSkillDevelopment";
            var id = 0;
            if (courseID != null)
                id = courseID;
            debugger;
            if (courseType != null && courseType == "Leadership")
                sp = "GetLearningPlanLeadershipRecommended";
            var sortExpression = "";
            if (courseType == "Mandatory Courses") {
                $.ajax({
                    type: "POST",
                    data: '{startRowIndex: ' + 0 + ', maximumRows: ' + 10 + ', sortExpression: "' + sortExpression + '", user: "<%= HttpContext.Current.User.Identity.Name.Split('|')[0] %>", subcatID:' + id + '}',
                    contentType: "application/json; charset=utf-8",
                    url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "GetLearningPlanMandatoryCourse",
                    dataType: 'json',
                    success: function (res) {
                        result = res.d.data;
                        count = res.d.count;

                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        var code = XMLHttpRequest.status;
                        if (code == "401")
                            window.location.reload();
                        else
                            console.log(code);
                    }
                }).done(function () {
                    console.log(result);
                    loadMap(result, count);
                });

            }
            else {
                $.ajax({
                    type: "POST",
                    data: '{startRowIndex: ' + 0 + ', maximumRows: ' + 10 + ', sortExpression: "' + sortExpression + '", user: "<%= HttpContext.Current.User.Identity.Name.Split('|')[0] %>"}',
                    contentType: "application/json; charset=utf-8",
                    url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + sp,
                    dataType: 'json',
                    success: function (res) {
                        result = res.d.data;
                        count = res.d.count;

                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        var code = XMLHttpRequest.status;
                        if (code == "401")
                            window.location.reload();
                        else
                            alert(XMLHttpRequest.responseText);
                    }
                }).done(function () {
                    console.log(result);
                    loadMap(result, count);
                });
            }


        }

        function rcbCourseType_OnClientSelectedIndexChanged(sender, args) {
            var item = args.get_item();
            var combo = $find('<%= rcbMandatory.ClientID %>');

            if (item.get_text() == "Mandatory Courses") {
                $("#divrcbMandatory").removeClass("display-none");
            }
            else {
                $("#divrcbMandatory").addClass("display-none");
                combo.clearSelection();
                showPageLoading();
                bindData(item.get_text(), item.get_value());
            }
        }

        function rcbMandatory_OnClientSelectedIndexChanged(sender, args) {
            var item = args.get_item();
            showPageLoading();
            bindData("Mandatory Courses", item.get_value());
        }
        //        x1: 454, y1: 403,
        var result;
        var count = 0;
        var bilang = 0;
        var arrCoordinates = [
						 {
						     id: 0,
						     x: 435,
						     y: 365,
						     line: { x1: 454, y1: 403, x2: 335, y2: 370 },
						     imgSrc: "Media/Images/start-learner-map.png",
						     color: "#D1453C"

						 },
						 {
						     id: 1,
						     x: 335,
						     y: 370,
						     line: { x1: 335, y1: 370, x2: 180, y2: 318 },
						     imgSrc: "Media/Images/Logo-LearnerMapGS.png",
						     color: "#C5C5C5"
						 },
						 {
						     id: 2,
						     x: 180,
						     y: 318,
						     line: { x1: 180, y1: 318, x2: 34, y2: 262 },
						     imgSrc: "Media/Images/Logo-LearnerMapGS.png",
						     color: "#C5C5C5"
						 },
						 {
						     id: 3,
						     x: 34,
						     y: 262,
						     line: { x1: 34, y1: 262, x2: 149, y2: 218 },
						     imgSrc: "Media/Images/Logo-LearnerMapGS.png",
						     color: "#C5C5C5"
						 },
						 {
						     id: 4,
						     x: 149,
						     y: 218,
						     line: { x1: 149, y1: 218, x2: 360, y2: 219 },
						     imgSrc: "Media/Images/Logo-LearnerMapGS.png",
						     color: "#C5C5C5"
						 },
						 {
						     id: 5,
						     x: 360,
						     y: 219,
						     line: { x1: 360, y1: 219, x2: 515, y2: 209 },
						     imgSrc: "Media/Images/Logo-LearnerMapGS.png",
						     color: "#C5C5C5"
						 },
						 {
						     id: 6,
						     x: 515,
						     y: 209,
						     line: { x1: 515, y1: 209, x2: 429, y2: 134 },
						     imgSrc: "Media/Images/Logo-LearnerMapGS.png",
						     color: "#C5C5C5"
						 },
						 {
						     id: 7,
						     x: 429,
						     y: 134,
						     line: { x1: 429, y1: 134, x2: 275, y2: 128 },
						     imgSrc: "Media/Images/Logo-LearnerMapGS.png",
						     color: "#C5C5C5"
						 },
						 {
						     id: 8,
						     x: 275,
						     y: 128,
						     line: { x1: 275, y1: 128, x2: 123, y2: 96 },
						     imgSrc: "Media/Images/Logo-LearnerMapGS.png",
						     color: "#C5C5C5"
						 },
						 {
						     id: 9,
						     x: 123,
						     y: 96,
						     line: { x1: 123, y1: 96, x2: 272, y2: 44 },
						     imgSrc: "Media/Images/Logo-LearnerMapGS.png",
						     color: "#C5C5C5"
						 },
						 {
						     id: 10,
						     x: 272,
						     y: 44,
						     imgSrc: "Media/Images/Logo-LearnerMapGS.png",
						     color: "#C5C5C5"

						 }
					 ];

        var arrCoordinates1 = [];
        var canvas;
        var context;
        var tooltips;
        var points;
        var t = 1;
        // request mousemove events
        $("#LearningMapC").mousemove(function (e) {
            handleMouseMove(e);
        });


        function loadMap(ds, dscount) {

            if (dscount == 0)
                showToolTip(false);
            else
                showToolTip(true);

            //clearing of canvas
            canvas = document.getElementById("LearningMapC");
            context = canvas.getContext("2d");
            context.clearRect(0, 0, 570, 400);

            //setting tooltip
            tooltips = $("#divToolTip");

            if (dscount != null || ds != undefined) {
                result = ds;
            }

            count = 10;
            for (var i = 0; i <= count; i++) {
                if (result != null && i != 0 && i <= dscount) {
                    arrCoordinates[i].imgSrc = 'Media/Uploads/CourseImg/' + result[i - 1].CourseID + '/' + result[i - 1].CourseImage;
                    arrCoordinates[i].color = "#D1453C";
                }
                else if (i != 0) {
                    arrCoordinates[i].imgSrc = 'Media/Images/Logo-LearnerMapGS.png';
                    arrCoordinates[i].color = "#C5C5C5";
                }

                circle(arrCoordinates[i].x, arrCoordinates[i].y, arrCoordinates[i].imgSrc, arrCoordinates[i].color);

                if (i >= dscount)
                //arrCoordinates[i].color = "#1C1E20";
                    arrCoordinates[i].color = "#5B5B5B";
                else
                    arrCoordinates[i].color = "#D1453C";

                if (i < 10) {
                    line(arrCoordinates[i].line.x1, arrCoordinates[i].line.y1, arrCoordinates[i].line.x2, arrCoordinates[i].line.y2, arrCoordinates[i].color);
                    //arrCoordinates1.push({ x: arrCoordinates[i].line.x1, y: arrCoordinates[i].line.y1, imgSrc: arrCoordinates[i].imgSrc });					  
                }
                else {
                    line(arrCoordinates[i - 1].x1, arrCoordinates[i - 1].line.y1, arrCoordinates[i].x, arrCoordinates[i].y, arrCoordinates[i].color);
                    //arrCoordinates1.push({ x: arrCoordinates[i].x, y: arrCoordinates[i].y, imgSrc : arrCoordinates[i].imgSrc });						  
                }

            }
            hidePageLoading();
        }



        function handleMouseMove(e) {
            var canvasOffset = $("#LearningMapC").offset();
            var scrolltop = $(document).scrollTop();
            var offsetX = canvasOffset.left;
            var offsetY = canvasOffset.top;
            canvasMouseX = parseInt(event.clientX - offsetX);
            canvasMouseY = parseInt((event.clientY + scrolltop) - offsetY);
            var rating = $find("<%= rrCommentRating.ClientID %>");
            tooltips = $("#divToolTip");
            var isValid = false;
            for (var i = 0; i <= count; i++) {
                var dx = canvasMouseX - arrCoordinates[i].x;
                var dy = canvasMouseY - arrCoordinates[i].y;

                if (i == count && count != 0) {
                    dx = canvasMouseX - arrCoordinates[10].x;
                    dy = canvasMouseY - arrCoordinates[10].y;
                }
                if (i > 0) {
                    //circle button              
                    isValid = (dx * dx + dy * dy) < (40 * 40);
                    if (isValid) {
                        debugger;
                        rating.set_value(result[i - 1].Rating)
                        tooltips.empty();
                        dx = canvasMouseX - dx;
                        dy = canvasMouseY - dy;
                        tooltips.css("left", dx + "px");
                        tooltips.css("top", (dy - 70) + "px");
                        tooltips.append("<table class='table table-borderless' style='font-size: small;'> "
	                                           + " <tr> "
		                                        + "    <th class='col-md-4'>Title: </th> "
		                                        + "    <td class='col-md-8'>" + result[i - 1].CourseTitle + "</td>		"
	                                            + "</tr>"
	                                           + " <tr>"
		                                        + "    <th class='col-md-4'> Description: </th>"
		                                        + "    <td class='col-md-8'>" + result[i - 1].Description + "</td>		"
	                                           + " </tr>"
                                                + " <tr >"
		                                          + "  <th class='col-md-4'><i class='fa fa-clock-o' style='font-size: x-large; margin-right: 5px !important;'></i> : </th>"
		                                         + "   <td class='col-md-8'>" + result[i - 1].Duration + " min/s</td>		"
	                                           + " </tr>"
	                                          + "  <tr >"
		                                      + "      <th class='col-md-4'><i class='fa fa-calendar' style='font-size: x-large; margin-right: 5px !important;'> : </th>"
		                                      + "      <td class='col-md-8'> " + result[i - 1].DateAdded + " to " + result[i - 1].DueDate + "</td>		"
	                                          + "  </tr>"
	                                           + " <tr>"
		                                         + "   <th class='col-md-4'>Status: </th>"
		                                       + "     <td class='col-md-8'>" + result[i - 1].Status + "</td>		"
	                                            + "</tr>"
                                                 + "  <tr>"
		                                      + "      <th class='col-md-4'>You Rate This Course: </th>"
		                                      + "      <td id='tdRatings' class='col-md-8'> </td>		"
	                                          + "  </tr>"
                                            + " </table>");

                        $("#tdRatings").append(rating._element);
                        //tooltips.removeClass("display-none");

                        tooltips.show();
                        break;
                    }
                }
            }
            if (!isValid) {
                //tooltips.addClass("display-none");
                tooltips.hide();
            }
        }

        function line(x1, y1, x2, y2, color) {
            context.beginPath();
            context.moveTo(x1, y1);
            context.lineTo(x2, y2);
            context.lineWidth = 7;
            context.strokeStyle = color;
            context.closePath();
            context.stroke();
        }

        function circle(x, y, imgSrc, color) {
            debugger;
            var length = 0;
            var width = 0;
            var radius = 0;
            if (imgSrc == "Media/Images/start-learner-map.png") {
                radius = 200;
                length = 70;
                width = 80;
            }
            else {
                radius = 20;
                width = 40;
                length = 40;
            }
            var imageObj = new Image();
            //imgSrc = "img2.jpg";
            imageObj.onload = function () {
                context.save();
                context.strokeStyle = color;
                context.beginPath();
                context.arc(x, y, radius, 0, 2 * Math.PI, true);
                context.closePath();
                context.clip();

                context.drawImage(imageObj, x - 20, y - 20, width, length);

                context.beginPath();
                context.arc(x, y, radius, 0, 2 * Math.PI, true);

                context.clip();
                if (radius == 20)
                    context.stroke();
                context.closePath();
                context.restore();
            };

            imageObj.src = imgSrc;

            imageObj.onerror = function () {
                imageObj.src = 'Media/Uploads/CourseImg/No_image.jpg';
            }

        }

        function RectHouse(x, y, h, w) {
            context.beginPath();

            context.rect(x, y, w, h);

            var canvas1 = $("#LearningMapC");
            var canvasOffset = $("#LearningMapC").offset();
            var scrolltop = $(document).scrollTop();
            var offsetX = canvasOffset.left;
            var offsetY = canvasOffset.top;
            canvasMouseX = parseInt(x - offsetX);
            canvasMouseY = parseInt((y + scrolltop) - offsetY);

            var left = x, right = x + w;
            var top = y, bottom = y + h;
            canvas.addEventListener('click', function (event) {
                if (right >= canvasMouseX && left <= canvasMouseX && bottom >= canvasMouseY && top <= canvasMouseY) {
                    ShowModal();
                }
            });
            //context.stroke();                    
        }


        function ShowModal() {
            $('#btnShowChild').click();
        }

        function showToolTip(visib) {
            tooltips = $("#divToolTip");
            if (!visib)
                tooltips.addClass("display-none");
            else
                tooltips.removeClass("display-none");
        }

        function viewLearningMap() {
            debugger;
            $('#btnModal').click();
            // $('#exampleModal').on('shown.bs.modal', function () {
            $("#divToolTip").hide();
            canvas = document.getElementById("LearningMapC");
            context = canvas.getContext("2d");
            context.clearRect(0, 0, 1999, 1999);
            if (bilang == 0) {
                RectHouse(575, 410, 70, 120);
                bilang++;
            }

            //  });
            showToolTip(false);
            var combo = $find('<%= rcbCourseType.ClientID %>');
            combo.clearSelection();
        }

        function closeModalDialog() {
            $('#closeChild').click();
            $('#closeParent').click();
        }

    </script>
</rad:RadScriptBlock>
<rad:RadAjaxManager ID="RadAjaxManager1" runat="server">
    <AjaxSettings>
        <rad:AjaxSetting AjaxControlID="lvPagerProgramSpecific">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlProgramSpecificContainer" LoadingPanelID="localLoading" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="lvPagerTranscomWide">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlTranscomWideContainer" LoadingPanelID="localLoading" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="lvPagerRegional">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlRegionalContainer" LoadingPanelID="localLoading" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="lvPagerCountry">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlCountryContainer" LoadingPanelID="localLoading" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnSwitchToListView">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="RadAjaxPanel1" LoadingPanelID="localLoading" />
            </UpdatedControls>
        </rad:AjaxSetting>
    </AjaxSettings>
</rad:RadAjaxManager>
<div class="col-lg-12" style="padding-top: 30px;">
    <div class="row">
        <div class="pull-right">
            <rad:RadAjaxPanel ID="RadAjaxPanel1" runat="server">
                <asp:LinkButton ID="btnSwitchToListView" runat="server" OnClientClick="btnSwitchView_Click(); return false;"
                    Font-Underline="false" aria-hidden="true" ToolTip="Switch To List View">
                    <i id="switchClassListView" runat="server" class="fa fa-th-list fa-gray fa-md display-none switchClassListView">
                    </i><i id="switchClassGridView" runat="server" class="fa fa-th-large fa-gray fa-md switchClassGridView">
                    </i>
                </asp:LinkButton>
                <%--<asp:LinkButton ID="btnSwitchToGridView" runat="server" OnClientClick="btnSwitchView_Click(); return false;"
                    Font-Underline="false" aria-hidden="true" ToolTip="Switch To Grid View">
                    
                </asp:LinkButton>--%>
            </rad:RadAjaxPanel>
        </div>
        <button type="button" id="btnModalFake" class="btn btn-default border-lightgray btn-sm btn-flat btn-thin pull-right"
            style="margin-right: 20px;" onclick="viewLearningMap(); return false;">
            <span><i class="fa fa-crosshairs icon" aria-hidden="true"></i>
                <asp:Label runat="server" ID="Label1" Text="<%$ Resources:LocalizedResource, ViewMyLearningMap %>"></asp:Label></span>
        </button>
        <button type="button" id="btnModal" class="btn btn-default border-lightgray btn-sm btn-flat btn-thin pull-right display-none"
            style="margin-right: 20px;" data-toggle="modal" data-target="#exampleModal">
            <span><i class="fa fa-crosshairs icon" aria-hidden="true"></i>
                <asp:Label runat="server" ID="Label6" Text="<%$ Resources:LocalizedResource, ViewMyLearningMap %>"></asp:Label></span>
        </button>
        <div class="col-md-12 ten-px-paddings" style="padding-bottom: 30px">
            <div class="row">
                <div class="col-md-4 col-md-12">
                    <div class="bg-color-light-teal color-white font-bold five-px-padding">
                        <asp:Label runat="server" ID="Label7" Text="<%$ Resources:LocalizedResource, LeadershipRecommendedCourses %>"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="border-top-light-teal-md">
                        <div class="row">
                            <div class="col-md-12">
                                <br />
                                <div id="Div2" class="rc-container col-md-12 loadingPanel col-xs-12 no-padding" style="min-height: 300px;">
                                    <div class="overlay display-none">
                                    </div>
                                    <div class="loader js-rc-loader">
                                    </div>
                                    <rad:RadListView ID="lvRecommendedCourse" runat="server" AllowPaging="true" PageSize="5"
                                        AllowMultiFieldSorting="true" CssClass="is-list">
                                        <LayoutTemplate>
                                            <div id="recommendedCourseListView">
                                                <div class="rcPager">
                                                    <a class="pagePrev fa fa-chevron-left pull-left relative-vr-left-center gray-arrow no-underline-hover display-none"
                                                        href="javascript:void(0);" title="Go to previous page"></a><a class="pageNext fa fa-chevron-right pull-right relative-vr-right-center gray-arrow no-underline-hover display-none"
                                                            href="javascript:void(0);" title="Go to next page"></a>
                                                    <div id="rc-items" class="display-flow margin-side-15">
                                                    </div>
                                                </div>
                                            </div>
                                        </LayoutTemplate>
                                        <ClientSettings>
                                            <DataBinding ItemPlaceHolderID="rc-items">
                                                <ItemTemplate>
                                                    <div class="col-md-2 col-xs-12 no-padding" style="font-size:12px;margin:0 1.3%">
                                                        <a id="hllvRecommenedCourses" onclick='confirmLaunchCourse("#= EncryptedCourseID #");' style="color:Black;" class="no-underline-hover hover-pointer">
                                                        <div id="divRecommenedCourses" runat="server" class="ps-item rlvI">
                                                            <div class="photo-container col-md-12">
                                                                <image src='Media/Uploads/CourseImg/#= CourseID #/#= CourseImage #' onerror="this.src='Media/Uploads/CourseImg/No_image.jpg'" style="height:130px;width:190px;"></image>
                                                            </div>
                                                            <div class="col-md-12 ">
                                                            
                                                           
                                                                <div class="category font-bold text-trim text-teal">
                                                                    #= CourseTitle #
                                                                </div>
                                                                <div class="">
                                                                   Category : #= CourseCategory #
                                                        
                                                                </div>
                                                                <div class="">
                                                                 Sub Category : #= CourseSubcategory #
                                                       
                                                                </div>
                                                                <div class="">
                                                                 Date Added : #= DateAdded #
                                                                </div>
                                                                <div class="">
                                                                 Due Date : #= DueDate #
                                                                </div>
                                                                <div class="">
                                                                 Course Type : #= CourseType #
                                                                </div>
                                                                <div class="display-none">
                                                                 Progress : <div class='progressBarContainer'>
                                                                    <input type="hidden" id="hidProgressVal" value="#= Progress #" class="display-none"/>
                                                                    <div id='progressBar' style="width: 100%;border: 1px solid rgb(255, 255, 255);">
                                                                        <span id="spanval" style="line-height: 22px;" class="pull-right"></span>
                                                                        <div id="progress">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <script type="text/javascript">
                                                                    initializeProgressBar();
                                                                </script>
                                                                </div>
                                                                 <div class="">
                                                                    <div class="statusContainer display-inline-flex">
                                                                       Status : <p id="status" class="bold">#= Status #</p>
                                                                       <input type="hidden" id="hidStatus" value="#= StatusId #" class="display-none"/>
                                                                    </div>
                                                                    <script type="text/javascript">
                                                                        setStatusFontColor();
                                                                    </script>
                                                                </div>
                                                                <div class="clearfix">
                                                                </div>
                                                                 </div>
                                                        </div>
                                                        </a>
                                                        </div>
                                                </ItemTemplate>
                                                <EmptyDataTemplate>
                                                    No Recommended Leadership Course to display.
                                                </EmptyDataTemplate>
                                                <DataService EnableCaching="true" />
                                            </DataBinding>
                                            <ClientEvents OnCommand="rcOnListViewCommand" OnDataBinding="rcOnListViewDataBinding">
                                            </ClientEvents>
                                        </ClientSettings>
                                    </rad:RadListView>
                                    <rad:RadListView ID="gridRecommenedCourse" runat="server" RenderMode="Lightweight"
                                        CssClass="GridLess is-grid" AllowPaging="true" PageSize="5">
                                        <LayoutTemplate>
                                            <div id="recommendedCourseGridView" class="display-none">
                                                <table class="gridMainTable table table-bordered table-striped course-grid">
                                                    <thead>
                                                        <tr class="rlvHeader">
                                                            <th class="btn-teal">
                                                                Course Title
                                                            </th>
                                                            <th class="btn-teal">
                                                                Date Added
                                                            </th>
                                                            <th class="btn-teal">
                                                                Due Date
                                                            </th>
                                                            <th class="btn-teal">
                                                                Progress
                                                            </th>
                                                            <th class="btn-teal">
                                                                Status
                                                            </th>
                                                            <th class="btn-teal">
                                                                Course Type
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="gridrcitem">
                                                    </tbody>
                                                    <tfoot>
                                                    </tfoot>
                                                </table>
                                                <div class="rcPager margin-bottom-10px">
                                                    <a class="pagePrev fa fa-caret-left gray-arrow no-underline-hover display-none left-arrow-pager"
                                                        href="javascript:void(0);" title="Go to previous page"></a><a class="pageNext fa fa-caret-right gray-arrow no-underline-hover display-none right-arrow-pager"
                                                            href="javascript:void(0);" title="Go to next page"></a>
                                                </div>
                                            </div>
                                        </LayoutTemplate>
                                        <ClientSettings>
                                            <DataBinding ItemPlaceHolderID="gridrcitem">
                                                <ItemTemplate>
                                                <tr class="rlvI">
                                                    <td>
                                                        <a id="hlgridRecommended" onclick='confirmLaunchCourse("#= EncryptedCourseID #");' class="hover-pointer"> #= CourseTitle #</a>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label2" runat="server"> #= DateAdded #</asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label3" runat="server"> #= DueDate #</asp:Label>
                                                    </td>
                                                   <td class="display-none">
                                                        <asp:Label ID="Label4" runat="server">
                                                            <div class='progressBarContainer'>
                                                                <input type="hidden" id="hidProgressVal" value="#= Progress #" class="display-none"/>
                                                                <div id='progressBar' style="width: 100%;border: 1px solid rgb(255, 255, 255);">
                                                                    <span id="spanval" style="line-height: 22px;" class="pull-right"></span>
                                                                    <div id="progress">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <script type="text/javascript">
                                                                initializeProgressBar();
                                                            </script>
                                                        </asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label5" runat="server">
                                                            <div class="statusContainer display-inline-flex">
                                                            <p id="status" class="bold">#= Status #</p>
                                                            <input type="hidden" id="hidStatus" value="#= StatusId #" class="display-none"/>
                                                            </div>
                                                            <script type="text/javascript">
                                                                setStatusFontColor();
                                                            </script>
                                                        </asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label5" runat="server"> #= CourseType #</asp:Label>
                                                    </td>
                                                </tr>
                                                </ItemTemplate>
                                                <EmptyDataTemplate>
                                                No Recommended Course.
                                                </EmptyDataTemplate>
                                                <DataService EnableCaching="true" />
                                            </DataBinding>
                                        </ClientSettings>
                                    </rad:RadListView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <br />
        <div class="col-md-12 ten-px-paddings" style="padding-bottom: 30px">
            <div class="row">
                <div class="col-md-4 col-md-12">
                    <div class="bg-color-light-teal color-white font-bold five-px-padding">
                        <asp:Label runat="server" ID="Label8" Text="<%$ Resources:LocalizedResource, SkillDevelopmentCourses %>"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="border-top-light-teal-md">
                        <div class="row">
                            <div class="col-md-12">
                                <br />
                                <div id="Div1" class="ps-container col-md-12 loadingPanel" style="min-height: 300px;">
                                    <div class="overlay display-none">
                                    </div>
                                    <div class="loader js-ps-loader">
                                    </div>
                                    <rad:RadListView ID="lvSkillDevelopmentCourses" runat="server" AllowPaging="true"
                                        PageSize="5" AllowMultiFieldSorting="true" CssClass="is-list">
                                        <LayoutTemplate>
                                            <div id="skillDevelopmentListView">
                                                <div class="psPager">
                                                    <a class="pagePrev fa fa-chevron-left pull-left relative-vr-left-center gray-arrow no-underline-hover display-none"
                                                        href="javascript:void(0);" title="Go to previous page"></a><a class="pageNext fa fa-chevron-right pull-right relative-vr-right-center gray-arrow no-underline-hover display-none"
                                                            href="javascript:void(0);" title="Go to next page"></a>
                                                    <div id="ps-items" class="display-flow margin-side-15">
                                                    </div>
                                                </div>
                                            </div>
                                        </LayoutTemplate>
                                        <ClientSettings>
                                            <DataBinding ItemPlaceHolderID="ps-items">
                                                <ItemTemplate>
                                                    <div class="col-md-2" style="font-size:12px;margin:0 1.3%">
                                                        <a id="hllvSkillDevelopmentCourses" onclick='confirmLaunchCourse("#= EncryptedCourseID #");' style="color:Black;" class="no-underline-hover hover-pointer">
                                                        <div id="divSkillDevelopmentCourses" runat="server" class="ps-item rlvI">
                                                            <div class="photo-container">
                                                                <image src='Media/Uploads/CourseImg/#= CourseID #/#= CourseImage #' onerror="this.src='Media/Uploads/CourseImg/No_image.jpg'" style="height:130px;width:190px;"></image>
                                                            </div>
                                                                <div class="category font-bold text-trim text-teal">
                                                                 #= CourseTitle #
                                                                </div>
                                                                <div class="">
                                                                 Category : #= CourseCategory #
                                                        
                                                                </div>
                                                                <div class="">
                                                                 Sub Category : #= CourseSubcategory #
                                                       
                                                                </div>
                                                                <div class="">
                                                                 Date Added : #= DateAdded #
                                                                </div>
                                                                <div class="">
                                                                 Due Date : #= DueDate #
                                                                </div>
                                                                <div class="">
                                                                 Course Type : #= CourseType # 
                                                                </div>
                                                                <div class="display-none">
                                                                   Progress : <div class='progressBarContainer'>
                                                                    <input type="hidden" id="hidProgressVal" value="#= Progress #" class="display-none"/>
                                                                    <div id='progressBar' style="width: 100%;border: 1px solid rgb(255, 255, 255);">
                                                                        <span id="spanval" style="line-height: 22px;" class="pull-right"></span>
                                                                        <div id="progress">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <script type="text/javascript">
                                                                    initializeProgressBar();
                                                                </script>
                                                                </div>
                                                                 <div class="">
                                                                    <div class="statusContainer display-inline-flex">
                                                                       Status : <p id="status" class="bold">#= Status #</p>
                                                                       <input type="hidden" id="hidStatus" value="#= StatusId #" class="display-none"/>
                                                                    </div>
                                                                    <script type="text/javascript">
                                                                        setStatusFontColor();
                                                                    </script>
                                                                </div>
                                                                <div class="clearfix">
                                                                </div>
                                                        </div>
                                                        </a>
                                                        </div>
                                                </ItemTemplate>
                                                <EmptyDataTemplate>
                                                    No Skill Development Course to display.
                                                </EmptyDataTemplate>
                                                <DataService EnableCaching="true" />
                                            </DataBinding>
                                            <ClientEvents OnCommand="psOnListViewCommand" OnDataBinding="psOnListViewDataBinding">
                                            </ClientEvents>
                                        </ClientSettings>
                                    </rad:RadListView>
                                    <rad:RadListView ID="gridSkillDevelopmentCourses" runat="server" RenderMode="Lightweight"
                                        CssClass="GridLess is-grid" AllowPaging="true" PageSize="5">
                                        <LayoutTemplate>
                                            <div id="skillDevelopmentGridView" class="display-none">
                                                <table class="gridMainTable table table-bordered table-striped course-grid">
                                                    <thead>
                                                        <tr class="rlvHeader">
                                                            <th class="btn-teal">
                                                                Course Title
                                                            </th>
                                                            <th class="btn-teal">
                                                                Date Added
                                                            </th>
                                                            <th class="btn-teal">
                                                                Due Date
                                                            </th>
                                                            <th class="btn-teal">
                                                                Progress
                                                            </th>
                                                            <th class="btn-teal">
                                                                Status
                                                            </th>
                                                            <th class="btn-teal">
                                                                Course Type
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="gridpsitem">
                                                    </tbody>
                                                    <tfoot>
                                                    </tfoot>
                                                </table>
                                                <div class="psPager margin-bottom-10px">
                                                    <a class="pagePrev fa fa-caret-left gray-arrow no-underline-hover display-none left-arrow-pager"
                                                        href="javascript:void(0);" title="Go to previous page"></a><a class="pageNext fa fa-caret-right gray-arrow no-underline-hover display-none right-arrow-pager"
                                                            href="javascript:void(0);" title="Go to next page"></a>
                                                </div>
                                            </div>
                                        </LayoutTemplate>
                                        <ClientSettings>
                                            <DataBinding ItemPlaceHolderID="gridpsitem">
                                                <ItemTemplate>
                                    <tr class="rlvI">
                                        <td>
                                            <a id="hlgridskillDevelopment" onclick='confirmLaunchCourse("#= EncryptedCourseID #");' class="hover-pointer"> #= CourseTitle #</a>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label2" runat="server"> #= DateAdded #</asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label3" runat="server"> #= DueDate #</asp:Label>
                                        </td>
                                        <td class="display-none">
                                            <asp:Label ID="Label4" runat="server">
                                                <div class='progressBarContainer'>
                                                    <input type="hidden" id="hidProgressVal" value="#= Progress #" class="display-none"/>
                                                    <div id='progressBar' style="width: 100%;border: 1px solid rgb(255, 255, 255);">
                                                        <span id="spanval" style="line-height: 22px;" class="pull-right"></span>
                                                        <div id="progress">
                                                        </div>
                                                    </div>
                                                </div>
                                                <script type="text/javascript">
                                                    initializeProgressBar();
                                                </script>
                                            </asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label5" runat="server">
                                                <div class="statusContainer display-inline-flex">
                                                <p id="status" class="bold">#= Status #</p>
                                                <input type="hidden" id="hidStatus" value="#= StatusId #" class="display-none"/>
                                                </div>
                                                <script type="text/javascript">
                                                    setStatusFontColor();
                                                </script>
                                            </asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label5" runat="server"> #= CourseType #</asp:Label>
                                        </td>
                                    </tr>
                                                </ItemTemplate>
                                                <EmptyDataTemplate>
                                    No Program Specific Course.
                                                </EmptyDataTemplate>
                                                <DataService EnableCaching="true" />
                                            </DataBinding>
                                        </ClientSettings>
                                    </rad:RadListView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <br />
        <div class="col-md-12 ten-px-paddings" style="padding-bottom: 30px">
            <div class="row">
                <div class="col-md-4 col-md-12">
                    <div class="bg-color-light-teal color-white font-bold five-px-padding">
                        <asp:Label runat="server" ID="Label16" Text="<%$ Resources:LocalizedResource, MandatoryCourses %>"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="border-top-light-teal-md">
                        <div class="row">
                            <div class="col-md-12">
                                <br />
                                <div id="mc-container" class="mc-container col-md-12 loadingPanel" style="min-height: 300px;">
                                    <div class="course-container m-mandatory-course-nav" style="margin: 5px 0; height: 45vh">
                                        <ul class="nav nav-tabs tab-bottom-border black-font">
                                            <li class="active"><a data-toggle="tab" href="#tabProgramSpecific">
                                                <asp:Label runat="server" ID="Label9" Text="<%$ Resources:LocalizedResource, Programspecific %>"></asp:Label></a></li>
                                            <li><a data-toggle="tab" href="#tabTranscomWide">
                                                <asp:Label runat="server" ID="Label10" Text="<%$ Resources:LocalizedResource, TranscomDashWide %>"></asp:Label></a></li>
                                            <li><a data-toggle="tab" href="#tabRegional">
                                                <asp:Label runat="server" ID="Label11" Text="<%$ Resources:LocalizedResource, Regional %>"></asp:Label></a></li>
                                            <li><a data-toggle="tab" href="#tabCountry">
                                                <asp:Label runat="server" ID="Label12" Text="<%$ Resources:LocalizedResource, ByCountry %>"></asp:Label></a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <br />
                                            <div id="tabProgramSpecific" class="tab-pane fade in active mc-container-ps">
                                                <div class="overlay display-none">
                                                </div>
                                                <div class="loader js-mc-loader">
                                                </div>
                                                <rad:RadListView ID="lvMandatoryCourses_ps" runat="server" AllowPaging="true" PageSize="5"
                                                    AllowMultiFieldSorting="true" CssClass="is-list">
                                                    <LayoutTemplate>
                                                        <div id="mandatoryListView_ps">
                                                            <div class="mcPager_ps">
                                                                <a class="pagePrev fa fa-chevron-left pull-left relative-vr-left-center gray-arrow no-underline-hover display-none"
                                                                    href="javascript:void(0);" title="Go to previous page"></a><a class="pageNext fa fa-chevron-right pull-right relative-vr-right-center gray-arrow no-underline-hover display-none"
                                                                        href="javascript:void(0);" title="Go to next page"></a>
                                                                <div id="mc-items_ps" class="display-flow margin-side-15">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </LayoutTemplate>
                                                    <ClientSettings>
                                                        <DataBinding ItemPlaceHolderID="mc-items_ps">
                                                            <ItemTemplate>
                                                                <div class="col-md-2" style="font-size:12px;margin:0 1.3%">
                                                        <a id="hllvMandatoryCourses" onclick='confirmLaunchCourse("#= EncryptedCourseID #");'  style="color:Black;" class="no-underline-hover hover-pointer">
                                                        <div id="divMandatoryCourses" runat="server" class="mc-item_ps rlvI">
                                                            <div class="photo-container no-padding">
                                                                <image src='Media/Uploads/CourseImg/#= CourseID #/#= CourseImage #' onerror="this.src='Media/Uploads/CourseImg/No_image.jpg'" style="height:130px;width:100%"></image>
                                                            </div>
                                                                <div class="category font-bold text-trim text-teal">
                                                                    #= CourseTitle #
                                                                </div>
                                                                <div class="">
                                                                    Category : #= CourseCategory #
                                                        
                                                                </div>
                                                                <div class="">
                                                                    Sub Category : #= CourseSubcategory #
                                                       
                                                                </div>
                                                                <div class="">
                                                                    Date Added : #= DateAdded #
                                                                </div>
                                                                <div class="">
                                                                    Due Date  : #= DueDate #
                                                                </div>
                                                                <div class="">
                                                                    Course Type : #= CourseType #
                                                                </div>
                                                                <div class="display-none">
                                                                    Progress : <div class='progressBarContainer'>
                                                                    <input type="hidden" id="hidProgressVal" value="#= Progress #" class="display-none"/>
                                                                    <div id='progressBar' style="width: 100%;border: 1px solid rgb(255, 255, 255);">
                                                                        <span id="spanval" style="line-height: 22px;" class="pull-right"></span>
                                                                        <div id="progress">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <script type="text/javascript">
                                                                    initializeProgressBar();
                                                                </script>
                                                                </div>
                                                                    <div class="">
                                                                    <div class="statusContainer display-inline-flex">
                                                                        Status : <p id="status" class="bold">#= Status #</p>
                                                                        <input type="hidden" id="hidStatus" value="#= StatusId #" class="display-none"/>
                                                                    </div>
                                                                    <script type="text/javascript">
                                                                        setStatusFontColor();
                                                                    </script>
                                                                </div>
                                                                <div class="clearfix">
                                                                </div>
                                                        </div>
                                                        </a>
                                                     </div>
                                                            </ItemTemplate>
                                                            <EmptyDataTemplate>
                                                                No Mandatory Program Specific Course.
                                                            </EmptyDataTemplate>
                                                            <DataService EnableCaching="true" />
                                                        </DataBinding>
                                                        <ClientEvents OnCommand="mcOnListViewCommand_ps" OnDataBinding="mcOnListViewDataBinding_ps">
                                                        </ClientEvents>
                                                    </ClientSettings>
                                                </rad:RadListView>
                                                <rad:RadListView ID="gridMandatoryCourses_ps" runat="server" RenderMode="Lightweight"
                                                    CssClass="GridLess is-grid" AllowPaging="true" PageSize="5">
                                                    <LayoutTemplate>
                                                        <div id="mandatoryGridView_ps" class="display-none">
                                                            <table class="gridMainTable table table-bordered table-striped course-grid">
                                                                <thead>
                                                                    <tr class="rlvHeader">
                                                                        <th class="btn-teal">
                                                                            Course Title
                                                                        </th>
                                                                        <th class="btn-teal">
                                                                            Date Added
                                                                        </th>
                                                                        <th class="btn-teal">
                                                                            Due Date
                                                                        </th>
                                                                        <th class="btn-teal">
                                                                            Progress
                                                                        </th>
                                                                        <th class="btn-teal">
                                                                            Status
                                                                        </th>
                                                                        <th class="btn-teal">
                                                                            Course Type
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="gridmcitem_ps">
                                                                </tbody>
                                                                <tfoot>
                                                                </tfoot>
                                                            </table>
                                                            <div class="mcPager_ps margin-bottom-10px">
                                                                <a class="pagePrev fa fa-caret-left gray-arrow no-underline-hover display-none left-arrow-pager"
                                                                    href="javascript:void(0);" title="Go to previous page"></a><a class="pageNext fa fa-caret-right gray-arrow no-underline-hover display-none right-arrow-pager"
                                                                        href="javascript:void(0);" title="Go to next page"></a>
                                                            </div>
                                                        </div>
                                                    </LayoutTemplate>
                                                    <ClientSettings>
                                                        <DataBinding ItemPlaceHolderID="gridmcitem_ps">
                                                            <ItemTemplate>
                                                                <tr class="rlvI">
                                        <td>
                                            <a id="hlgridMandatoryCourses" onclick='confirmLaunchCourse("#= EncryptedCourseID #");' class="hover-pointer"> #= CourseTitle #</a>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label2" runat="server"> #= DateAdded #</asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label3" runat="server"> #= DueDate #</asp:Label>
                                        </td>
                                        <td class="display-none">
                                            <asp:Label ID="Label4" runat="server">
                                                <div class='progressBarContainer'>
                                                    <input type="hidden" id="hidProgressVal" value="#= Progress #" class="display-none"/>
                                                    <div id='progressBar' style="width: 100%;border: 1px solid rgb(255, 255, 255);">
                                                        <span id="spanval" style="line-height: 22px;" class="pull-right"></span>
                                                        <div id="progress">
                                                        </div>
                                                    </div>
                                                </div>
                                                <script type="text/javascript">
                                                    initializeProgressBar();
                                                </script>
                                            </asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label5" runat="server">
                                                <div class="statusContainer display-inline-flex">
                                                <p id="status" class="bold">#= Status #</p>
                                                <input type="hidden" id="hidStatus" value="#= StatusId #" class="display-none"/>
                                                </div>
                                                <script type="text/javascript">
                                                    setStatusFontColor();
                                                </script>
                                            </asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label5" runat="server"> #= CourseType #</asp:Label>
                                        </td>
                                    </tr>
                                                            </ItemTemplate>
                                                            <EmptyDataTemplate>
                                                               No Mandatory Program Specific Course.
                                                            </EmptyDataTemplate>
                                                            <DataService EnableCaching="true" />
                                                        </DataBinding>
                                                    </ClientSettings>
                                                </rad:RadListView>
                                            </div>
                                            <div id="tabTranscomWide" class="tab-pane fade mc-container-tw">
                                                <div class="overlay display-none">
                                                </div>
                                                <div class="loader js-mc-loader">
                                                </div>
                                                <asp:Panel runat="server" ID="pnlTranscomWideContainer" CssClass="container">
                                                    <rad:RadListView ID="lvMandatoryCourses_tw" runat="server" AllowPaging="true" PageSize="5"
                                                        AllowMultiFieldSorting="true" CssClass="is-list">
                                                        <LayoutTemplate>
                                                            <div id="mandatoryListView_tw">
                                                                <div class="mcPager_tw">
                                                                    <a class="pagePrev fa fa-chevron-left pull-left relative-vr-left-center gray-arrow no-underline-hover display-none"
                                                                        href="javascript:void(0);" title="Go to previous page"></a><a class="pageNext fa fa-chevron-right pull-right relative-vr-right-center gray-arrow no-underline-hover display-none"
                                                                            href="javascript:void(0);" title="Go to next page"></a>
                                                                    <div id="mc-items_tw" class="display-flow margin-side-15">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </LayoutTemplate>
                                                        <ClientSettings>
                                                            <DataBinding ItemPlaceHolderID="mc-items_tw">
                                                                <ItemTemplate>
                                                                <div class="col-md-2" style="font-size:12px;margin:0 1.3%">
                                                        <a id="hllvMandatoryCourses" onclick='confirmLaunchCourse("#= EncryptedCourseID #");'  style="color:Black;" class="no-underline-hover hover-pointer">
                                                        <div id="divMandatoryCourses" runat="server" class="mc-item rlvI">
                                                            <div class="photo-container no-padding">
                                                                <image src='Media/Uploads/CourseImg/#= CourseID #/#= CourseImage #' onerror="this.src='Media/Uploads/CourseImg/No_image.jpg'" style="height:130px;width:100%"></image>
                                                            </div>
                                                                <div class="category font-bold text-trim text-teal">
                                                                    #= CourseTitle #
                                                                </div>
                                                                <div class="">
                                                                    Category : #= CourseCategory #
                                                        
                                                                </div>
                                                                <div class="">
                                                                    Sub Category : #= CourseSubcategory #
                                                       
                                                                </div>
                                                                <div class="">
                                                                    Date Added : #= DateAdded #
                                                                </div>
                                                                <div class="">
                                                                    Due Date : #= DueDate #
                                                                </div>
                                                                <div class="">
                                                                    Course Type : #= CourseType #
                                                                </div>
                                                                <div class="display-none">
                                                                    Progress : <div class='progressBarContainer'>
                                                                    <input type="hidden" id="hidProgressVal" value="#= Progress #" class="display-none"/>
                                                                    <div id='progressBar' style="width: 100%;border: 1px solid rgb(255, 255, 255);">
                                                                        <span id="spanval" style="line-height: 22px;" class="pull-right"></span>
                                                                        <div id="progress">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <script type="text/javascript">
                                                                    initializeProgressBar();
                                                                </script>
                                                                </div>
                                                                    <div class="">
                                                                    <div class="statusContainer display-inline-flex">
                                                                        Status : <p id="status" class="bold">#= Status #</p>
                                                                        <input type="hidden" id="hidStatus" value="#= StatusId #" class="display-none"/>
                                                                    </div>
                                                                    <script type="text/javascript">
                                                                        setStatusFontColor();
                                                                    </script>
                                                                </div>
                                                                <div class="clearfix">
                                                                </div>
                                                        </div>
                                                        </a>
                                                     </div>
                                                                </ItemTemplate>
                                                                <EmptyDataTemplate>
                                                                No Mandatory Transcom Wide Course.
                                                                </EmptyDataTemplate>
                                                                <DataService EnableCaching="true" />
                                                            </DataBinding>
                                                            <ClientEvents OnCommand="mcOnListViewCommand_tw" OnDataBinding="mcOnListViewDataBinding_tw">
                                                            </ClientEvents>
                                                        </ClientSettings>
                                                    </rad:RadListView>
                                                    <rad:RadListView ID="gridMandatoryCourses_tw" runat="server" RenderMode="Lightweight"
                                                        CssClass="GridLess is-grid" AllowPaging="true" PageSize="5">
                                                        <LayoutTemplate>
                                                            <div id="mandatoryGridView_tw" class="display-none">
                                                                <table class="gridMainTable table table-bordered table-striped course-grid">
                                                                    <thead>
                                                                        <tr class="rlvHeader">
                                                                            <th class="btn-teal">
                                                                                Course Title
                                                                            </th>
                                                                            <th class="btn-teal">
                                                                                Date Added
                                                                            </th>
                                                                            <th class="btn-teal">
                                                                                Due Date
                                                                            </th>
                                                                            <th class="btn-teal">
                                                                                Progress
                                                                            </th>
                                                                            <th class="btn-teal">
                                                                                Status
                                                                            </th>
                                                                            <th class="btn-teal">
                                                                                Course Type
                                                                            </th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody id="gridmcitem_tw">
                                                                    </tbody>
                                                                    <tfoot>
                                                                    </tfoot>
                                                                </table>
                                                                <div class="mcPager_tw margin-bottom-10px">
                                                                    <a class="pagePrev fa fa-caret-left gray-arrow no-underline-hover display-none left-arrow-pager"
                                                                        href="javascript:void(0);" title="Go to previous page"></a><a class="pageNext fa fa-caret-right gray-arrow no-underline-hover display-none right-arrow-pager"
                                                                            href="javascript:void(0);" title="Go to next page"></a>
                                                                </div>
                                                            </div>
                                                        </LayoutTemplate>
                                                        <ClientSettings>
                                                            <DataBinding ItemPlaceHolderID="gridmcitem_tw">
                                                                <ItemTemplate>
                                                                <tr class="rlvI">
                                        <td>
                                            <a id="hlgridMandatoryCourses" onclick='confirmLaunchCourse("#= EncryptedCourseID #");' class="hover-pointer"> #= CourseTitle #</a>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label2" runat="server"> #= DateAdded #</asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label3" runat="server"> #= DueDate #</asp:Label>
                                        </td>
                                        <td class="display-none">
                                            <asp:Label ID="Label4" runat="server">
                                                <div class='progressBarContainer'>
                                                    <input type="hidden" id="hidProgressVal" value="#= Progress #" class="display-none"/>
                                                    <div id='progressBar' style="width: 100%;border: 1px solid rgb(255, 255, 255);">
                                                        <span id="spanval" style="line-height: 22px;" class="pull-right"></span>
                                                        <div id="progress">
                                                        </div>
                                                    </div>
                                                </div>
                                                <script type="text/javascript">
                                                    initializeProgressBar();
                                                </script>
                                            </asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label5" runat="server">
                                                <div class="statusContainer display-inline-flex">
                                                <p id="status" class="bold">#= Status #</p>
                                                <input type="hidden" id="hidStatus" value="#= StatusId #" class="display-none"/>
                                                </div>
                                                <script type="text/javascript">
                                                    setStatusFontColor();
                                                </script>
                                            </asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label5" runat="server"> #= CourseType #</asp:Label>
                                        </td>
                                    </tr>
                                                                </ItemTemplate>
                                                                <EmptyDataTemplate>
                                                                No Mandatory Transcom Wide Course.
                                                                </EmptyDataTemplate>
                                                                <DataService EnableCaching="true" />
                                                            </DataBinding>
                                                        </ClientSettings>
                                                    </rad:RadListView>
                                                </asp:Panel>
                                            </div>
                                            <div id="tabRegional" class="tab-pane fade mc-container-rg">
                                                <div class="overlay display-none">
                                                </div>
                                                <div class="loader js-mc-loader">
                                                </div>
                                                <asp:Panel runat="server" ID="pnlRegionalContainer" CssClass="container">
                                                    <rad:RadListView ID="lvMandatoryCourses_rg" runat="server" AllowPaging="true" PageSize="5"
                                                        AllowMultiFieldSorting="true" CssClass="is-list">
                                                        <LayoutTemplate>
                                                            <div id="mandatoryListView_rg">
                                                                <div class="mcPager_rg">
                                                                    <a class="pagePrev fa fa-chevron-left pull-left relative-vr-left-center gray-arrow no-underline-hover display-none"
                                                                        href="javascript:void(0);" title="Go to previous page"></a><a class="pageNext fa fa-chevron-right pull-right relative-vr-right-center gray-arrow no-underline-hover display-none"
                                                                            href="javascript:void(0);" title="Go to next page"></a>
                                                                    <div id="mc-items_rg" class="display-flow margin-side-15">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </LayoutTemplate>
                                                        <ClientSettings>
                                                            <DataBinding ItemPlaceHolderID="mc-items_rg">
                                                                <ItemTemplate>
                                                                <div class="col-md-2" style="font-size:12px;margin:0 1.3%">
                                                        <a id="hllvMandatoryCourses" onclick='confirmLaunchCourse("#= EncryptedCourseID #");'  style="color:Black;" class="no-underline-hover hover-pointer">
                                                        <div id="divMandatoryCourses" runat="server" class="mc-item rlvI">
                                                            <div class="photo-container no-padding">
                                                                <image src='Media/Uploads/CourseImg/#= CourseID #/#= CourseImage #' onerror="this.src='Media/Uploads/CourseImg/No_image.jpg'" style="height:130px;width:100%"></image>
                                                            </div>
                                                                <div class="category font-bold text-trim text-teal">
                                                                    #= CourseTitle #
                                                                </div>
                                                                <div class="">
                                                                    Category : #= CourseCategory #
                                                        
                                                                </div>
                                                                <div class="">
                                                                    Sub Category : #= CourseSubcategory #
                                                       
                                                                </div>
                                                                <div class="">
                                                                    Date Added : #= DateAdded #
                                                                </div>
                                                                <div class="">
                                                                    Due Date : #= DueDate #
                                                                </div>
                                                                <div class="">
                                                                    Course Type : #= CourseType #
                                                                </div>
                                                                <div class="display-none">
                                                                    Progress : <div class='progressBarContainer'>
                                                                    <input type="hidden" id="hidProgressVal" value="#= Progress #" class="display-none"/>
                                                                    <div id='progressBar' style="width: 100%;border: 1px solid rgb(255, 255, 255);">
                                                                        <span id="spanval" style="line-height: 22px;" class="pull-right"></span>
                                                                        <div id="progress">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <script type="text/javascript">
                                                                    initializeProgressBar();
                                                                </script>
                                                                </div>
                                                                    <div class="">
                                                                    <div class="statusContainer display-inline-flex">
                                                                        Status : <p id="status" class="bold">#= Status #</p>
                                                                        <input type="hidden" id="hidStatus" value="#= StatusId #" class="display-none"/>
                                                                    </div>
                                                                    <script type="text/javascript">
                                                                        setStatusFontColor();
                                                                    </script>
                                                                </div>
                                                                <div class="clearfix">
                                                                </div>
                                                        </div>
                                                        </a>
                                                     </div>
                                                                </ItemTemplate>
                                                                <EmptyDataTemplate>
                                                               No Mandatory Regional Course.
                                                                </EmptyDataTemplate>
                                                                <DataService EnableCaching="true" />
                                                            </DataBinding>
                                                            <ClientEvents OnCommand="mcOnListViewCommand_rg" OnDataBinding="mcOnListViewDataBinding_rg">
                                                            </ClientEvents>
                                                        </ClientSettings>
                                                    </rad:RadListView>
                                                    <rad:RadListView ID="gridMandatoryCourses_rg" runat="server" RenderMode="Lightweight"
                                                        CssClass="GridLess is-grid" AllowPaging="true" PageSize="5">
                                                        <LayoutTemplate>
                                                            <div id="mandatoryGridView_rg" class="display-none">
                                                                <table class="gridMainTable table table-bordered table-striped course-grid">
                                                                    <thead>
                                                                        <tr class="rlvHeader">
                                                                            <th class="btn-teal">
                                                                                Course Title
                                                                            </th>
                                                                            <th class="btn-teal">
                                                                                Date Added
                                                                            </th>
                                                                            <th class="btn-teal">
                                                                                Due Date
                                                                            </th>
                                                                            <th class="btn-teal">
                                                                                Progress
                                                                            </th>
                                                                            <th class="btn-teal">
                                                                                Status
                                                                            </th>
                                                                            <th class="btn-teal">
                                                                                Course Type
                                                                            </th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody id="gridmcitem_rg">
                                                                    </tbody>
                                                                    <tfoot>
                                                                    </tfoot>
                                                                </table>
                                                                <div class="mcPager_rg margin-bottom-10px">
                                                                    <a class="pagePrev fa fa-caret-left gray-arrow no-underline-hover display-none left-arrow-pager"
                                                                        href="javascript:void(0);" title="Go to previous page"></a><a class="pageNext fa fa-caret-right gray-arrow no-underline-hover display-none right-arrow-pager"
                                                                            href="javascript:void(0);" title="Go to next page"></a>
                                                                </div>
                                                            </div>
                                                        </LayoutTemplate>
                                                        <ClientSettings>
                                                            <DataBinding ItemPlaceHolderID="gridmcitem_rg">
                                                                <ItemTemplate>
                                                                <tr class="rlvI">
                                        <td>
                                            <a id="hlgridMandatoryCourses" onclick='confirmLaunchCourse("#= EncryptedCourseID #");' class="hover-pointer"> #= CourseTitle #</a>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label2" runat="server"> #= DateAdded #</asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label3" runat="server"> #= DueDate #</asp:Label>
                                        </td>
                                        <td class="display-none">
                                            <asp:Label ID="Label4" runat="server">
                                                <div class='progressBarContainer'>
                                                    <input type="hidden" id="hidProgressVal" value="#= Progress #" class="display-none"/>
                                                    <div id='progressBar' style="width: 100%;border: 1px solid rgb(255, 255, 255);">
                                                        <span id="spanval" style="line-height: 22px;" class="pull-right"></span>
                                                        <div id="progress">
                                                        </div>
                                                    </div>
                                                </div>
                                                <script type="text/javascript">
                                                    initializeProgressBar();
                                                </script>
                                            </asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label5" runat="server">
                                                <div class="statusContainer display-inline-flex">
                                                <p id="status" class="bold">#= Status #</p>
                                                <input type="hidden" id="hidStatus" value="#= StatusId #" class="display-none"/>
                                                </div>
                                                <script type="text/javascript">
                                                    setStatusFontColor();
                                                </script>
                                            </asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label5" runat="server"> #= CourseType #</asp:Label>
                                        </td>
                                    </tr>
                                                                </ItemTemplate>
                                                                <EmptyDataTemplate>
                                                                No Mandatory Regional Course.
                                                                </EmptyDataTemplate>
                                                                <DataService EnableCaching="true" />
                                                            </DataBinding>
                                                        </ClientSettings>
                                                    </rad:RadListView>
                                                </asp:Panel>
                                            </div>
                                            <div id="tabCountry" class="tab-pane fade mc-container-ct">
                                                <div class="overlay display-none">
                                                </div>
                                                <div class="loader js-mc-loader">
                                                </div>
                                                <asp:Panel runat="server" ID="pnlCountryContainer" CssClass="container">
                                                    <rad:RadListView ID="lvMandatoryCourses_ct" runat="server" AllowPaging="true" PageSize="5"
                                                        AllowMultiFieldSorting="true" CssClass="is-list">
                                                        <LayoutTemplate>
                                                            <div id="mandatoryListView_ct">
                                                                <div class="mcPager_ct">
                                                                    <a class="pagePrev fa fa-chevron-left pull-left relative-vr-left-center gray-arrow no-underline-hover display-none"
                                                                        href="javascript:void(0);" title="Go to previous page"></a><a class="pageNext fa fa-chevron-right pull-right relative-vr-right-center gray-arrow no-underline-hover display-none"
                                                                            href="javascript:void(0);" title="Go to next page"></a>
                                                                    <div id="mc-items_ct" class="display-flow margin-side-15">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </LayoutTemplate>
                                                        <ClientSettings>
                                                            <DataBinding ItemPlaceHolderID="mc-items_ct">
                                                                <ItemTemplate>
                                                                <div class="col-md-2" style="font-size:12px;margin:0 1.3%">
                                                        <a id="hllvMandatoryCourses" onclick='confirmLaunchCourse("#= EncryptedCourseID #");' style="color:Black;" class="no-underline-hover hover-pointer">
                                                        <div id="divMandatoryCourses" runat="server" class="mc-item rlvI">
                                                            <div class="photo-container no-padding">
                                                                <image src='Media/Uploads/CourseImg/#= CourseID #/#= CourseImage #' onerror="this.src='Media/Uploads/CourseImg/No_image.jpg'" style="height:130px;width:100%"></image>
                                                            </div>
                                                                <div class="category font-bold text-trim text-teal">
                                                                    #= CourseTitle #
                                                                </div>
                                                                <div class="">
                                                                    Category : #= CourseCategory #
                                                        
                                                                </div>
                                                                <div class="">
                                                                    Sub Category : #= CourseSubcategory #
                                                       
                                                                </div>
                                                                <div class="">
                                                                    Date Added : #= DateAdded #
                                                                </div>
                                                                <div class="">
                                                                    Due Date : #= DueDate #
                                                                </div>
                                                                <div class="">
                                                                    Course Type : #= CourseType #
                                                                </div>
                                                                <div class="display-none">
                                                                    Progress : <div class='progressBarContainer'>
                                                                    <input type="hidden" id="hidProgressVal" value="#= Progress #" class="display-none"/>
                                                                    <div id='progressBar' style="width: 100%;border: 1px solid rgb(255, 255, 255);">
                                                                        <span id="spanval" style="line-height: 22px;" class="pull-right"></span>
                                                                        <div id="progress">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <script type="text/javascript">
                                                                    initializeProgressBar();
                                                                </script>
                                                                </div>
                                                                    <div class="">
                                                                    <div class="statusContainer display-inline-flex">
                                                                        Status : <p id="status" class="bold">#= Status #</p>
                                                                        <input type="hidden" id="hidStatus" value="#= StatusId #" class="display-none"/>
                                                                    </div>
                                                                    <script type="text/javascript">
                                                                        setStatusFontColor();
                                                                    </script>
                                                                </div>
                                                                <div class="clearfix">
                                                                </div>
                                                        </div>
                                                        </a>
                                                     </div>
                                                                </ItemTemplate>
                                                                <EmptyDataTemplate>
                                                                No By Counry Mandatory Course to display.
                                                                </EmptyDataTemplate>
                                                                <DataService EnableCaching="true" />
                                                            </DataBinding>
                                                            <ClientEvents OnCommand="mcOnListViewCommand_ct" OnDataBinding="mcOnListViewDataBinding_ct">
                                                            </ClientEvents>
                                                        </ClientSettings>
                                                    </rad:RadListView>
                                                    <rad:RadListView ID="gridMandatoryCourses_ct" runat="server" RenderMode="Lightweight"
                                                        CssClass="GridLess is-grid" AllowPaging="true" PageSize="5">
                                                        <LayoutTemplate>
                                                            <div id="mandatoryGridView_ct" class="display-none">
                                                                <table class="gridMainTable table table-bordered table-striped course-grid">
                                                                    <thead>
                                                                        <tr class="rlvHeader">
                                                                            <th class="btn-teal">
                                                                                Course Title
                                                                            </th>
                                                                            <th class="btn-teal">
                                                                                Date Added
                                                                            </th>
                                                                            <th class="btn-teal">
                                                                                Due Date
                                                                            </th>
                                                                            <th class="btn-teal">
                                                                                Progress
                                                                            </th>
                                                                            <th class="btn-teal">
                                                                                Status
                                                                            </th>
                                                                            <th class="btn-teal">
                                                                                Course Type
                                                                            </th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody id="gridmcitem_ct">
                                                                    </tbody>
                                                                    <tfoot>
                                                                    </tfoot>
                                                                </table>
                                                                <div class="mcPager_ct margin-bottom-10px">
                                                                    <a class="pagePrev fa fa-caret-left gray-arrow no-underline-hover display-none left-arrow-pager"
                                                                        href="javascript:void(0);" title="Go to previous page"></a><a class="pageNext fa fa-caret-right gray-arrow no-underline-hover display-none right-arrow-pager"
                                                                            href="javascript:void(0);" title="Go to next page"></a>
                                                                </div>
                                                            </div>
                                                        </LayoutTemplate>
                                                        <ClientSettings>
                                                            <DataBinding ItemPlaceHolderID="gridmcitem_ct">
                                                                <ItemTemplate>
                                                                <tr class="rlvI">
                                        <td>
                                            <a id="hlgridMandatoryCourses" onclick='confirmLaunchCourse("#= EncryptedCourseID #");' class="hover-pointer"> #= CourseTitle #</a>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label2" runat="server"> #= DateAdded #</asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label3" runat="server"> #= DueDate #</asp:Label>
                                        </td>
                                        <td class="display-none">
                                            <asp:Label ID="Label4" runat="server">
                                                <div class='progressBarContainer'>
                                                    <input type="hidden" id="hidProgressVal" value="#= Progress #" class="display-none"/>
                                                    <div id='progressBar' style="width: 100%;border: 1px solid rgb(255, 255, 255);">
                                                        <span id="spanval" style="line-height: 22px;" class="pull-right"></span>
                                                        <div id="progress">
                                                        </div>
                                                    </div>
                                                </div>
                                                <script type="text/javascript">
                                                    initializeProgressBar();
                                                </script>
                                            </asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label5" runat="server">
                                                <div class="statusContainer display-inline-flex">
                                                <p id="status" class="bold">#= Status #</p>
                                                <input type="hidden" id="hidStatus" value="#= StatusId #" class="display-none"/>
                                                </div>
                                                <script type="text/javascript">
                                                    setStatusFontColor();
                                                </script>
                                            </asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label5" runat="server"> #= CourseType #</asp:Label>
                                        </td>
                                    </tr>
                                                                </ItemTemplate>
                                                                <EmptyDataTemplate>
                                                                No Mandatory Country Course.
                                                                </EmptyDataTemplate>
                                                                <DataService EnableCaching="true" />
                                                            </DataBinding>
                                                        </ClientSettings>
                                                    </rad:RadListView>
                                                </asp:Panel>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="width: 100% !important; max-width: 900px !important;">
            <div class="modal-header btn-teal" style="height: 35px !important; line-height: 0 !important;">
                <h5 class="modal-title" id="exampleModalLabel">
                    <asp:Label runat="server" ID="Label13" Text="<%$ Resources:LocalizedResource, LearningMap %>"></asp:Label></h5>
                <button type="button" class="close" onclick="closeModalDialog(); return false;" style="position: absolute;
                    right: 10px; top: 7px;">
                    <span aria-hidden="true">&times;</span>
                </button>
                <button type="button" class="close display-none" data-dismiss="modal" aria-label="Close"
                    style="position: absolute; right: 10px; top: 7px;" id="closeParent">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="pleasework" class="modal-body">
                <div>
                    <canvas id="LearningMapC" width="894px" height="500px"></canvas>
                    <div id="divToolTip" class="text-justify tooltip-learning">
                        <rad:RadRating RenderMode="Lightweight" ID="rrCommentRating" runat="server" ItemCount="5"
                            ClientIDMode="Static" SelectionMode="Continuous" Precision="Item" Orientation="Horizontal"
                            Enabled="false" CssClass="cr-rating">
                        </rad:RadRating>
                    </div>
                </div>
                <button type="button" data-toggle="modal" data-target="#modalDialogBox" class="display-none"
                    id="btnShowChild">
                </button>
                <div class="Click-Here">
                    <img src="Media/Images/click-here1.png" style="width: 100%; height: 100%;" />
                </div>
                <div class="cloud display-none">
                </div>
            </div>
        </div>
    </div>
</div>
<div id="modalDialogBox" class="modal modal-child" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true" data-modal-parent="#exampleModal" style="top: 120px !important;">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" style="width: 70% !important;">
            <div class="modal-header btn-teal" style="height: 35px !important; line-height: 0 !important;">
                <button type="button" class="close" data-dismiss="modal" style="position: absolute;
                    right: 10px; top: 7px;" id="closeChild">
                    &times;
                </button>
                <h5 class="modal-title">
                    <asp:Label runat="server" ID="Label14" Text="<%$ Resources:LocalizedResource, ChooseaCourse %>"></asp:Label>!</h5>
            </div>
            <div class="modal-body">
                <div class="col-md-12 display-inline-flex" style="padding-left: 30px; padding-top: 30px;">
                    <div>
                        <rad:RadComboBox runat="server" ID="rcbCourseType" OnClientSelectedIndexChanged="rcbCourseType_OnClientSelectedIndexChanged"
                            EmptyMessage="<%$ Resources:LocalizedResource, SelectCourse %>" AutoPostBack="false"
                            DataTextField="Category" DataValueField="CategoryID" DataSourceID="dsCourseType">
                        </rad:RadComboBox>
                    </div>
                    <div id="divrcbMandatory" class="display-none" style="left-margin: 15px !important;">
                        <rad:RadComboBox runat="server" ID="rcbMandatory" OnClientSelectedIndexChanged="rcbMandatory_OnClientSelectedIndexChanged"
                            EmptyMessage="<%$ Resources:LocalizedResource, SelectMandatory %>" AutoPostBack="false"
                            DataTextField="SubCategory" DataValueField="SubCategoryID" DataSourceID="dsSubCategory">
                        </rad:RadComboBox>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default display-none" data-dismiss="modal" aria-hidden="true">
                    <asp:Label runat="server" ID="Label15" Text="<%$ Resources:LocalizedResource, Cancel %>"></asp:Label></button>
            </div>
        </div>
    </div>
</div>
<div id="dataSource">
    <asp:SqlDataSource ID="dsCourseType" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection%>"
        SelectCommand="select * from tbl_transcomuniversity_lkp_category
                                                where Categoryid in (17,18,19)
                                                and HideFromList = 0"></asp:SqlDataSource>
    <asp:SqlDataSource ID="dsSubCategory" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection%>"
        SelectCommand="select * from tbl_transcomuniversity_lkp_subcategory
                where subcategoryid in (28,29,30,31)
                and HideFromList = 0"></asp:SqlDataSource>
</div>
