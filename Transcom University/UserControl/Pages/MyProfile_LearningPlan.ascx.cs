﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using Telerik.Web.UI;
using TranscomUniversityV3Model;
using System.Configuration;
using NuSkill.Business;
using System.Web.UI.HtmlControls;

public partial class UserControl_Pages_MyProfile_LearningPlan : System.Web.UI.UserControl
{
    //private static int CIM;
    //private static string EMAIL;
    //private static int bilang = 0;   

    protected void Page_Load(object sender, EventArgs e)
    {



        //CIM = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
        //EMAIL = DataHelper.getCurrentUserEmail();


        //BindProgramSpecific();
        //BindTranscomWide();
        //BindRegional();
        //BindCountry();

        //BindRepeater(17, EMAIL, rptLeadership);
        //BindRepeater(18, EMAIL, rptSkillDevelopment);
        //BindRepeaterMandatory(28, EMAIL, rptProgramSpecific);
        //BindRepeaterMandatory(29, EMAIL, rptTranscomWide);
        //BindRepeaterMandatory(30, EMAIL, rptRegional);
        //BindRepeaterMandatory(31, EMAIL, rptByCountry);

    }

    //protected void lkbtnLearningMap_Onclick(object sender, EventArgs e) {
    //    ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", DataHelper.callClientScript("showLearningMap"), true);
    //}


    #region oldCodes Learning Map
    //void BindRepeater(int category, string user, Repeater rpt)
    //{
    //    TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();
    //    int cim = DataHelper.GetCimByEmail(user);
    //    var result = db.pr_TranscomUniversity_Lkp_AssignedEnrolledCourses(cim, "", user)
    //                       .AsEnumerable()
    //                       .Select(m => new
    //                       {
    //                           CourseID = m.CourseId,
    //                           EncryptedCourseID = Utils.Encrypt(m.CourseId),
    //                           CourseTitle = m.Title,
    //                           CourseCategory = m.Category,
    //                           CategoryID = m.CategoryID,
    //                           CourseSubcategory = m.Subcategory,
    //                           SubcategoryID = m.SubcategoryID,
    //                           DateAdded = string.Format("{0: MM/dd/yyyy}", m.CreateDate),
    //                           DueDate = string.Format("{0: MM/dd/yyyy}", m.DueDate),
    //                           m.CourseType,
    //                           Progress = progressVal(m.CourseTypeID, m.HasTest, m.CourseLogin.ToString(), m.Passed, m.CourseId.ToString()),
    //                           m.CourseImage,
    //                           m.Status,
    //                           m.StatusId,
    //                       })
    //                       .Where(m => m.CategoryID == category)
    //                       .ToArray();

    //    var item = from a in result
    //               join b in db.tbl_TranscomUniversity_Cor_Course on a.CourseID equals b.CourseID
    //               select new
    //               {
    //                   a.CourseID,
    //                   a.EncryptedCourseID,
    //                   a.CourseTitle,
    //                   a.CourseCategory,
    //                   a.CategoryID,
    //                   a.CourseSubcategory,
    //                   a.SubcategoryID,
    //                   a.DateAdded,
    //                   a.DueDate,
    //                   a.CourseType,
    //                   a.Progress,
    //                   a.CourseImage,
    //                   a.StatusId,
    //                   a.Status,
    //                   CourseDescription = b.Description,
    //                   CourseDuration = b.Duration,
    //                   Rating = Convert.ToDecimal(ratings(Convert.ToInt32(a.CourseID), cim))
    //               };

    //    item.ToList();

    //    if (item.Count() > 0)
    //    {
    //        BindStep(rpt.ID.ToString(), item.Count() - 1);
    //        rpt.DataSource = item;
    //        rpt.DataBind();
    //    }
    //    else
    //    {
    //        if (rpt.ID.Contains("rptLeadership"))
    //            pnlLeadershipRoute.Attributes["class"] = "display-none";
    //        if (rpt.ID.Contains("rptSkillDevelopment"))
    //            pnlSkillDevelopmentRoute.Attributes["class"] = "display-none";

    //    }
    //}

    //void BindRepeaterMandatory(int subcatID, string user, Repeater rpt)
    //{

    //    TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();
    //    int cim = DataHelper.GetCimByEmail(user);
    //    var course = db.pr_TranscomUniversity_Lkp_AssignedEnrolledCourses(cim, string.Empty, user)
    //               .AsEnumerable()
    //               .Select(m => new
    //               {
    //                   CourseID = m.CourseId,
    //                   EncryptedCourseID = Utils.Encrypt(m.CourseId),
    //                   CourseTitle = m.Title,
    //                   CourseCategory = m.Category,
    //                   CategoryID = m.CategoryID,
    //                   CourseSubcategory = m.Subcategory,
    //                   SubcategoryID = m.SubcategoryID,
    //                   DateAdded = string.Format("{0: MM/dd/yyyy}", m.CreateDate),
    //                   DueDate = string.Format("{0: MM/dd/yyyy}", m.DueDate),
    //                   m.CourseType,
    //                   Progress = progressVal(m.CourseTypeID, m.HasTest, m.CourseLogin.ToString(), m.Passed, m.CourseId.ToString()),
    //                   m.CourseImage,
    //                   m.Status,
    //                   m.StatusId,
    //                   CourseDuration = 0
    //               })
    //               .Where(m => m.CategoryID == 19 && m.SubcategoryID == subcatID)
    //               .ToArray();

    //    var item = from a in course
    //               join b in db.tbl_TranscomUniversity_Cor_Course on a.CourseID equals b.CourseID
    //               select new
    //               {
    //                   a.CourseID,
    //                   a.EncryptedCourseID,
    //                   a.CourseTitle,
    //                   a.CourseCategory,
    //                   a.CategoryID,
    //                   a.CourseSubcategory,
    //                   a.SubcategoryID,
    //                   a.DateAdded,
    //                   a.DueDate,
    //                   a.CourseType,
    //                   a.Progress,
    //                   a.CourseImage,
    //                   a.StatusId,
    //                   a.Status,
    //                   CourseDescription = b.Description,
    //                   CourseDuration = b.Duration,
    //                   Rating = Convert.ToDecimal(ratings(Convert.ToInt32(a.CourseID), cim))
    //               };

    //    item.ToList();
        
    //    if (item.Count() > 0)
    //    {
    //        rpt.DataSource = item;
    //        rpt.DataBind();
    //    }

    //    if (item.Count() == 0)
    //    {
    //        if (rpt.ID.Contains("rptProgramSpecific"))
    //        {
    //            programSpecific.Attributes["class"] = "display-none";
    //            bilang++;
    //        }
    //        if (rpt.ID.Contains("rptTranscomWide"))
    //        {
    //            pnltranscomWide.Attributes["class"] = "display-none";
    //            bilang++;
    //        }
    //        if (rpt.ID.Contains("rptRegional"))
    //        {
    //            pnlRegional.Attributes["class"] = "display-none";
    //            bilang++;
    //        }
    //        if (rpt.ID.Contains("rptByCountry"))
    //        {
    //            pnlByCountry.Attributes["class"] = "display-none";
    //            bilang++;
    //        }
    //    }


    //    if (bilang == 4)
    //        divNo.Attributes["class"] = "display-block";
    //}

    //public static double progressVal(int? courseTypeID, int? hasTest, string login, int? passed, string courseId)
    //{
    //    double progressval = 0.00;

    //    if (courseTypeID == 1)
    //    {
    //        if (hasTest > 0) //COURSE WITH ASSESSMENT
    //        {
    //            if (login == "1")
    //            {
    //                progressval = 20;

    //                if (passed == 1)
    //                {
    //                    progressval = progressval + 80;
    //                }
    //            }
    //        }
    //        else //COURSE WO ASSESSMENT
    //        {
    //            if (login == "1")
    //            {
    //                progressval = 100;
    //            }
    //        }
    //    }

    //    else if (courseTypeID == 2)
    //    {

    //        var email = HttpContext.Current.User.Identity.Name.Split('|')[0];
    //        progressval = Convert.ToDouble(ScormDataHelper.GetOverallProgressByCourseID(email, Convert.ToInt32(courseId)));
    //    }

    //    return progressval;
    //}

    //void BindStep(string rpt, int count)
    //{
    //    if (rpt.Contains("rptLeadership"))
    //    {
    //        if (count == 0)
    //            dvStepLeadership.Attributes["style"] = "width: 125px;";
    //        else
    //            dvStepLeadership.Attributes["style"] = "width: " + (125 + (count * 185) + (count * 5)) + "px;";
    //    }

    //    if (rpt.Contains("rptSkillDevelopment"))
    //    {
    //        if (count == 0)
    //            dvSkillDev.Attributes["style"] = "width: 125px;";
    //        else
    //            dvSkillDev.Attributes["style"] = "width: " + (125 + (count * 185) + (count * 5)) + "px;";
    //    }

    //    if (rpt.Contains("rptProgramSpecific"))
    //    {
    //        if (count == 0)
    //            dvProgramSpecific.Attributes["style"] = "width: 125px;";
    //        else
    //            dvProgramSpecific.Attributes["style"] = "width: " + (125 + (count * 190)) + "px;";
    //    }

    //    if (rpt.Contains("rptTranscomWide"))
    //    {
    //        if (count == 0)
    //            dvTranscomWide.Attributes["style"] = "width: 125px;";
    //        else
    //            dvTranscomWide.Attributes["style"] = "width: " + (125 + (count * 190)) + "px;";
    //    }

    //    if (rpt.Contains("rptRegional"))
    //    {
    //        if (count == 0)
    //            dvRegional.Attributes["style"] = "width: 125px;";
    //        else
    //            dvRegional.Attributes["style"] = "width: " + (125 + (count * 190)) + "px;";
    //    }

    //    if (rpt.Contains("rptByCountry"))
    //    {
    //        if (count == 0)
    //            dvByCountry.Attributes["style"] = "width: 125px;";
    //        else
    //            dvByCountry.Attributes["style"] = "width: " + (125 + (count * 190)) + "px;";
    //    }

    //    //li.Attributes["class"] = "activated";

    //}

    //protected void myRepeaterUL_ItemDataBound(object sender, RepeaterItemEventArgs e)
    //{

    //    var hidCourse = (HiddenField)e.Item.FindControl("hidCourseID");
    //    var hidRating = (HiddenField)e.Item.FindControl("hidRating");
    //    var rating = (RadRating)e.Item.FindControl("rrCommentRating");
    //    rating.Value = Convert.ToDecimal(hidRating.Value.ToString());
    //    var rptRepeater = (RadToolTip)e.Item.FindControl("RadToolTip2");

    //    //var rgToolTip = (RadGrid)e.Item.FindControl("rgToolTip");
    //    var rptToolTip = (Repeater)e.Item.FindControl("rptResourcesList");
    //    var height = bindRadGrid(Convert.ToInt32(hidCourse.Value), rptToolTip);

    //    rptRepeater.Height = height;

    //}

    //public static double ratings(int courseID, int CIM)
    //{
    //    double ratings = 0.00;

    //    TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();


    //    var item = (from u in db.tbl_TranscomUniversity_Rlt_CourseReview
    //                where u.CourseID == courseID && u.EnrolleeCIM == CIM
    //                select u.Rating).FirstOrDefault().ToString();

    //    if (item.Count() == 0)
    //        ratings = 0.00;
    //    else
    //        ratings = Convert.ToDouble(item);

    //    return ratings;
    //}

    //public static int bindRadGrid(int courseID, Repeater radgrid1)
    //{
    //    int curriculumID;
    //    TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer();

    //    var item = (from u in db.tbl_scorm_cor_Course
    //                where u.CurriculumID == courseID
    //                select u.curriculumcourseID).FirstOrDefault().ToString();


    //    if (item.Count() == 0)
    //    {
    //        radgrid1.DataSource = null;
    //        radgrid1.DataBind();
    //        return 500;
    //    }
    //    else
    //    {
    //        curriculumID = Convert.ToInt32(item);


    //        var completed = "Completed";
    //        var ds = db.pr_Scorm_Cor_GetCurriculumPackages(courseID, curriculumID, 0)
    //                    .Select(a => new { a.ScoTitle, a.ScoType, a.curriculumid, a.curriculumcourseID, a.course, a.SectionName, completed })
    //                    .ToList();
    //        var height = 500;
    //        if (ds.Count() > 2)
    //            height = height + ((ds.Count() - 2) * 100);
           
    //        radgrid1.DataSource = ds;
    //        radgrid1.DataBind();

    //        return height;
    //    }

    //}

    #endregion

}