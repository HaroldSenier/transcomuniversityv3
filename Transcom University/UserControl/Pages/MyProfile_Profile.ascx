﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MyProfile_Profile.ascx.cs"
    Inherits="UserControl_Pages_MyProfile_Profile" %>
<rad:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript" language="javascript">
        Sys.Application.add_load(function () {
            $(".RadMultiPage").css({ "min-height": "1000px" });
        });

        $(document).ready(function () {
            debugger;
            var EditProfileStat = $('#contentPlaceHolderMain_EditProfileStatus').val();
            if (EditProfileStat == 'true') {
                $('.text-Chars').css('visibility', 'visible');
            } else { $('.text-Chars').css('visibility', 'hidden'); }
        });

        function VisibleElements() {
            debugger;
            $('#contentPlaceHolderMain_EditProfileStatus').val("true");
            $('.text-Chars').css('visibility', 'visible');
        }

        function HiddenElements() {
            debugger;
            $('#contentPlaceHolderMain_EditProfileStatus').val("");
            $('.text-Chars').css('visibility', 'visible');
        }

        //CHECK LENGTH GIVEN BY THE PARAMETER || MDQUERUBIN 12072018
        function CheckMaxLength(sender, Maxlength) {
            debugger;
            var length = $(sender).val().length;
            if (sender.value.length > Maxlength) {

                sender.value = sender.value.substr(0, Maxlength);
            }

            var length = Maxlength - length;
            var PrimaryEduc = $("#<%=txtPrimaryEduc.ClientID%>").attr("name");
            var SecondaryEduc = $("#<%=txtSecondaryEduc.ClientID%>").attr("name");
            var TertiaryEduc = $("#<%=txtTertiaryEduc.ClientID%>").attr("name");
            var Course = $("#<%=txtCourse.ClientID%>").attr("name");
            var PreviousJob = $("#<%=txtPreviousJob.ClientID%>").attr("name");
            var CompanyName = $("#<%=txtCompanyName.ClientID%>").attr("name");
            var JobDesc = $("#<%=txtJobDesc.ClientID%>").attr("name");

            if (sender.name == PrimaryEduc) {
                $("#<%=txtPrimaryEducChars.ClientID%>").text(length);
            }
            if (sender.name == SecondaryEduc) {
                $("#<%=txtSecondaryEducChars.ClientID%>").text(length);
            }
            if (sender.name == TertiaryEduc) {
                $("#<%=txtTertiaryEducChars.ClientID%>").text(length);
            }
            if (sender.name == Course) {
                $("#<%=txtCourseChars.ClientID%>").text(length);
            }
            if (sender.name == PreviousJob) {
                $("#<%=txtPreviousJobChars.ClientID%>").text(length);
            }
            if (sender.name == CompanyName) {
                $("#<%=txtCompanyNameChars.ClientID%>").text(length);
            }
            if (sender.name == JobDesc) {
                $("#<%=txtJobDescChars.ClientID%>").text(length);
            }

        }

        function dontAllowDotFn(sender, args) {
            var c = args.get_keyCode();
            console.log(c);
            if (c == 46 || c == 45)
                args.set_cancel(true);
        }
    </script>
</rad:RadScriptBlock>
<br />
<div class="col-md-12 no-padding" id="profileDetailsContainer">
    <div class="col-md-12 ten-px-paddings" style="padding-bottom: 30px">
        <div class="row">
            <div class="col-md-2 col-md-12">
                <div class="bg-color-light-teal color-white font-bold five-px-padding">
                    <asp:Label runat="server" ID="TProfile" Text= "<%$ Resources:LocalizedResource, TranscomProfile %>"></asp:Label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 border-top-light-teal-sm ">
            </div>
            <div class="col-md-12 row-margin-5">
                <div class="row">
                    <div class="col-md-2 col-xs-6">
                        <asp:Label runat="server" ID="supers" Text= "<%$ Resources:LocalizedResource, Supervisor %>"></asp:Label>:
                    </div>
                    <div class="col-md-10 col-xs-6 pull-left ">
                        <asp:Label ID="lblSupervisor" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 col-xs-6">
                         <asp:Label runat="server" ID="regi" Text= "<%$ Resources:LocalizedResource, Region %>"></asp:Label>:
                    </div>
                    <div class="col-md-10 col-xs-6 pull-left ">
                        <asp:Label ID="lblRegion" runat="server"></asp:Label>
                        <asp:DropDownList ID="cbRegion" runat="server" 
                            Width="350px"  CssClass="form-control">
                        </asp:DropDownList>
                   <%--     <asp:SqlDataSource ID="SqlDataSource_Region" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection%>"
                            SelectCommand="SELECT RegionID, Region FROM tbl_TranscomUniversity_Lkp_Region WHERE [HideFromList] = 0">
                        </asp:SqlDataSource>--%>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 col-xs-6">
                        <asp:Label runat="server" ID="Countstry" Text= "<%$ Resources:LocalizedResource, Country %>"></asp:Label>:
                    </div>
                    <div class="col-md-10 col-xs-6 pull-left ">
                        <asp:Label ID="lblCountry" runat="server"></asp:Label>
                        <asp:DropDownList ID="cbCountry" runat="server"
                            Width="350px" CssClass="form-control">
                        </asp:DropDownList>
                        <%--<asp:SqlDataSource ID="SqlDataSource_Country" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection%>"
                            SelectCommand="SELECT CountryID, Country FROM tbl_TranscomUniversity_Lkp_Country WHERE [HideFromList] = 0">
                        </asp:SqlDataSource>--%>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 col-xs-6">
                        <asp:Label runat="server" ID="deptment" Text= "<%$ Resources:LocalizedResource, Department %>"></asp:Label>:
                    </div>
                    <div class="col-md-10 col-xs-6 pull-left ">
                        <asp:Label ID="lblDepartment" runat="server"></asp:Label>
                        <asp:DropDownList ID="cbDepartment" runat="server"
                            Width="350px" CssClass="form-control">
                        </asp:DropDownList>
                       <%-- <asp:SqlDataSource ID="SqlDataSource_Department" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection%>"
                            SelectCommand="SELECT DepartmentID, Department FROM tbl_TranscomUniversity_Lkp_Department WHERE [HideFromList] = 0">
                        </asp:SqlDataSource>--%>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 col-xs-6">
                       <asp:Label runat="server" ID="Clientss" Text= "<%$ Resources:LocalizedResource, Client %>"></asp:Label> :
                    </div>
                    <div class="col-md-10 col-xs-6 pull-left ">
                        <asp:Label ID="lblClient" runat="server"></asp:Label>
                        <asp:DropDownList ID="cbClient" runat="server"
                            Width="350px" CssClass="form-control">
                        </asp:DropDownList>
                       <%-- <asp:SqlDataSource ID="SqlDataSource_Client" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection%>"
                            SelectCommand="SELECT ClientID, Client FROM tbl_TranscomUniversity_Lkp_Client WHERE [HideFromList] = 0">
                        </asp:SqlDataSource>--%>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 col-xs-6">
                         <asp:Label runat="server" ID="campa" Text= "<%$ Resources:LocalizedResource, Campaign %>"></asp:Label>:
                    </div>
                    <div class="col-md-10 col-xs-6 pull-left ">
                        <asp:Label ID="lblCampaign" runat="server"></asp:Label>
                        <asp:DropDownList ID="cbCampaign" runat="server" 
                            Width="350px" CssClass="form-control">
                        </asp:DropDownList>
                    <%--    <asp:SqlDataSource ID="SqlDataSource_Campaign" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection%>"
                            SelectCommand="SELECT CampaignID, Campaign FROM tbl_TranscomUniversity_Lkp_Campaign WHERE [HideFromList] = 0">
                        </asp:SqlDataSource>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 ten-px-paddings" style="padding-bottom: 30px">
        <div class="row">
            <div class="col-md-2 col-md-12">
                <div class="bg-color-light-teal color-white font-bold five-px-padding">
                    <asp:Label runat="server" ID="Personalinf" Text= "<%$ Resources:LocalizedResource, PersonalInformation %>"></asp:Label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 border-top-light-teal-sm ">
            </div>
            <div class="col-md-12 row-margin-5">
                <asp:Panel runat="server" ID="hiddens" Visible="false">
                    <asp:Label ID="lblFirstName" Text="text" runat="server" />
                    <asp:Label ID="lblLastName" Text="text" runat="server" />
                    <asp:Label ID="lblRole" Text="text" runat="server" />
                    <asp:Label ID="lblCompanySite" Text="text" runat="server" />
                    <asp:Label ID="lblStatus" Text="text" runat="server" />
                </asp:Panel>
                <div class="row">
                    <div class="col-md-2 col-xs-6">
                         <asp:Label runat="server" ID="Cimnumbe" Text= "<%$ Resources:LocalizedResource, CimNumber %>"></asp:Label>:
                    </div>
                    <div class="col-md-10 col-xs-6 pull-left ">
                        <asp:Label ID="lblCIMNumber" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 col-xs-6">
                        <asp:Label runat="server" ID="gends" Text= "<%$ Resources:LocalizedResource, Gender %>"></asp:Label>:
                    </div>
                    <div class="col-md-10 col-xs-6 pull-left ">
                        <asp:Label ID="lblGender" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 col-xs-6">
                         <asp:Label runat="server" ID="DOB" Text= "<%$ Resources:LocalizedResource, DateOfBirth %>"></asp:Label>:
                    </div>
                    <div class="col-md-10 col-xs-6 pull-left ">
                        <asp:Label ID="lblDateOfBirth" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 col-xs-6">
                       <asp:Label runat="server" ID="SDWT" Text= "<%$ Resources:LocalizedResource, StartDatewithTranscom %>"></asp:Label>: 
                    </div>
                    <div class="col-md-10 col-xs-6 pull-left ">
                        <asp:Label ID="lblStartDateWithTranscom" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 col-xs-6">
                        <asp:Label runat="server" ID="PL" Text= "<%$ Resources:LocalizedResource,  PrimaryLanguage%>"></asp:Label>:
                    </div>
                    <div class="col-md-10 col-xs-6 pull-left ">
                        <asp:Label ID="lblPrimaryLanguage" runat="server"></asp:Label>
                        <asp:DropDownList ID="cbPrimaryLanguage" runat="server" DataSourceID="SqlDataSource_Language"
                            Width="350px" DataValueField="LanguageID" DataTextField="Language" CssClass="form-control">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="SqlDataSource_Language" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection%>"
                            SelectCommand="SELECT LanguageID, Language FROM tbl_TranscomUniversity_Lkp_Language WHERE [HideFromList] = 0">
                        </asp:SqlDataSource>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 ten-px-paddings" style="padding-bottom: 30px">
        <div class="row">
            <div class="col-md-2 col-md-12">
                <div class="bg-color-light-teal color-white font-bold five-px-padding">
                   <asp:Label runat="server" ID="CI" Text= "<%$ Resources:LocalizedResource, ContactInformation%>"></asp:Label> 
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 border-top-light-teal-sm ">
            </div>
            <div class="col-md-12 row-margin-5">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-2">
                       <asp:Label runat="server" ID="MN" Text= "<%$ Resources:LocalizedResource, MobileNumber%>"></asp:Label>:
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-10 pull-left ">
                        <asp:Label ID="lblMobileNumber" runat="server"></asp:Label>
                        <rad:RadNumericTextBox Skin="Bootstrap" Type="Number" ID="txtMobileNumber" MaxLength="15" MinValue="0" ondragstart="return false;" ondrop="return false;"
                            Width="350px" CssClass="form-control btn-flat" runat="server" RenderMode="Lightweight">
                            <NumberFormat GroupSeparator="" DecimalDigits="0" AllowRounding="false" />
                            <ClientEvents OnKeyPress="dontAllowDotFn" />
                        </rad:RadNumericTextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-2">
                       <asp:Label runat="server" ID="LL" Text= "<%$ Resources:LocalizedResource, Landline%>"></asp:Label>:
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-10 pull-left ">
                        <asp:Label ID="lblLandline" runat="server"></asp:Label>
                        <rad:RadNumericTextBox Skin="Bootstrap" Type="Number" ID="txtLandline" MaxLength="11"
                            Width="350px" CssClass="form-control btn-flat" runat="server" RenderMode="Lightweight">
                            <NumberFormat GroupSeparator="" DecimalDigits="0" />
                            <ClientEvents OnKeyPress="dontAllowDotFn" />
                        </rad:RadNumericTextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-2">
                        <asp:Label runat="server" ID="TE" Text= "<%$ Resources:LocalizedResource, TranscomEmail%>"></asp:Label>:
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-10 pull-left ">
                        <asp:Label ID="lblTranscomEmail" runat="server"></asp:Label>
                        <asp:TextBox ID="txtTranscomEmail" runat="server" ReadOnly="true" CssClass="form-control"
                            Width="350px" MaxLength="50" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-2">
                       <asp:Label runat="server" ID="PE" Text= "<%$ Resources:LocalizedResource,  PersonalEmail%>"></asp:Label>:
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-10 pull-left ">
                        <asp:Label ID="lblPersonalEmail" runat="server"></asp:Label>
                        <asp:TextBox ID="txtPersonalEmail" runat="server" CssClass="form-control" Width="350px"
                            MaxLength="50"></asp:TextBox>
                        <%--TextMode="Email"--%>
                        <asp:RegularExpressionValidator ID="emailValidator" runat="server" Display="Dynamic"
                            ErrorMessage="Please, enter valid e-mail address" ValidationExpression="^[\w\.\-]+@[a-zA-Z0-9\-]+(\.[a-zA-Z0-9\-]{1,})*(\.[a-zA-Z]{2,3}){1,2}$"
                            ControlToValidate="txtPersonalEmail">
                        </asp:RegularExpressionValidator>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 ten-px-paddings" style="padding-bottom: 30px">
        <div class="row">
            <div class="col-md-2 col-md-12">
                <div class="bg-color-light-teal color-white font-bold five-px-padding">
                    <asp:Label runat="server" ID="Educ" Text= "<%$ Resources:LocalizedResource,  Education%>"></asp:Label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 border-top-light-teal-sm ">
            </div>
            <div class="col-xs-12 col-sm-12 row-margin-5">
                <div class="row" style="padding-bottom:15px;">
                    <div class="col-xs-12 col-sm-12 col-md-7 no-padding">
                        <div class="col-md-3" style="margin-right: 3%;">
                             <asp:Label runat="server" ID="PEduc" Text= "<%$ Resources:LocalizedResource, PrimaryEducation%>"></asp:Label>:
                        </div>
                        <div class="col-md-8 pull-left ">
                            <asp:Label ID="lblPrimaryEduc" runat="server"></asp:Label>
                            <asp:TextBox ID="txtPrimaryEduc" runat="server" CssClass="form-control" Width="350px" onkeyup="CheckMaxLength(this,50);" ondragstart="return false;" ondrop="return false;"
                                MaxLength="50"></asp:TextBox>
                            <label class="text-danger text-Chars">
                                    <span runat="server" id="txtPrimaryEducChars">50</span> <asp:Label runat="server" ID="CRmain" Text= "<%$ Resources:LocalizedResource, CharactersRemaining %>"></asp:Label></label>
                        </div>
                        
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-5 col-xs-12 padding-side-md">
                        <asp:Label runat="server" ID="percover" Text= "<%$ Resources:LocalizedResource, PeriodCovered %>"></asp:Label>: &nbsp;&nbsp;&nbsp;&nbsp; <asp:Label runat="server" ID="Fromss" Text= "<%$ Resources:LocalizedResource, From %>"></asp:Label>
                        <asp:Label ID="lblPrimaryPeriodFrom" runat="server"></asp:Label>
                        <rad:RadDatePicker ID="dpPrimaryPeriodFrom" runat="server" ShowPopupOnFocus="true"
                            RenderMode="Lightweight" Skin="Bootstrap" DateInput-ReadOnly="true">
                        </rad:RadDatePicker>
                        <asp:Label runat="server" ID="Toss" Text= "<%$ Resources:LocalizedResource, To %>"></asp:Label>
                        <asp:Label ID="lblPrimaryPeriodTo" runat="server"></asp:Label>
                        <rad:RadDatePicker ID="dpPrimaryPeriodTo" runat="server" RenderMode="Lightweight"
                            ShowPopupOnFocus="true" Skin="Bootstrap" DateInput-ReadOnly="true">
                        </rad:RadDatePicker>
                        <label class="text-danger">
                        <span>
                        <asp:CompareValidator ID="CompareValidator1" runat="server" ForeColor="#a94442" ControlToValidate="dpPrimaryPeriodFrom"
                            ControlToCompare="dpPrimaryPeriodTo" Operator="LessThan" Type="Date" ErrorMessage="Start date must be less than End date">
                        </asp:CompareValidator>
                        </span>
                        </label>
                    </div>
                </div>
                <div class="row" style="padding-bottom:15px;">
                    <div class="col-md-7 no-padding">
                        <div class="col-md-3" style="margin-right: 3%;">
                           <asp:Label runat="server" ID="SEduc" Text= "<%$ Resources:LocalizedResource, SecondaryEducation%>"></asp:Label>:
                        </div>
                        <div class="col-md-8 pull-left ">
                            <asp:Label ID="lblSecondaryEduc" runat="server"></asp:Label>
                            <asp:TextBox ID="txtSecondaryEduc" runat="server" CssClass="form-control" Width="350px" onkeyup="CheckMaxLength(this,50);" ondragstart="return false;" ondrop="return false;"
                                MaxLength="50"></asp:TextBox>
                            <label class="text-danger text-Chars">
                                    <span runat="server" id="txtSecondaryEducChars">50</span> <asp:Label runat="server" ID="Charmaining" Text= "<%$ Resources:LocalizedResource, CharactersRemaining %>"></asp:Label></label>
                        </div>
                    </div>
                    <div class="col-md-5 col-xs-12 padding-side-md">
                        <asp:Label runat="server" ID="coverperiod" Text= "<%$ Resources:LocalizedResource, PeriodCovered %>"></asp:Label>: &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label runat="server" ID="Fromsss" Text= "<%$ Resources:LocalizedResource, From %>"></asp:Label> 
                        <asp:Label ID="lblSecondaryPeriodFrom" runat="server"></asp:Label>
                        <rad:RadDatePicker ID="dpSecondaryPeriodFrom" runat="server" RenderMode="Lightweight"
                            ShowPopupOnFocus="true" Skin="Bootstrap" DateInput-ReadOnly="true">
                        </rad:RadDatePicker>
                       <asp:Label runat="server" ID="Toooos" Text= "<%$ Resources:LocalizedResource, To %>"></asp:Label> 
                        <asp:Label ID="lblSecondaryPeriodTo" runat="server"></asp:Label>
                        <rad:RadDatePicker ID="dpSecondaryPeriodTo" runat="server" RenderMode="Lightweight"
                            ShowPopupOnFocus="true" Skin="Bootstrap" DateInput-ReadOnly="true">
                        </rad:RadDatePicker>
                        <label class="text-danger">
                        <span>
                        <asp:CompareValidator ID="CompareValidator2" runat="server" ForeColor="#a94442" ControlToValidate="dpSecondaryPeriodFrom"
                            ControlToCompare="dpSecondaryPeriodTo" Operator="LessThan" Type="Date" ErrorMessage="Start date must be less than End date">
                        </asp:CompareValidator>
                        </span>
                        </label>
                    </div>
                </div>
                <div class="row" style="padding-bottom:15px;">
                    <div class="col-md-7 no-padding">
                        <div class="col-md-3" style="margin-right: 3%;">
                            <asp:Label runat="server" ID="TEduc" Text= "<%$ Resources:LocalizedResource, TertiaryEducation%>"></asp:Label>:
                        </div>
                        <div class="col-md-8 pull-left ">
                            <asp:Label ID="lblTertiaryEduc" runat="server"></asp:Label>
                            <asp:TextBox ID="txtTertiaryEduc" runat="server" CssClass="form-control" Width="350px" onkeyup="CheckMaxLength(this,50);" ondragstart="return false;" ondrop="return false;"
                                MaxLength="50"></asp:TextBox>
                            <label class="text-danger text-Chars">
                                    <span runat="server" id="txtTertiaryEducChars">50</span> <asp:Label runat="server" ID="charmains" Text= "<%$ Resources:LocalizedResource,  CharactersRemaining %>"></asp:Label></label>
                        </div>
                    </div>
                    <div class="col-md-5 col-xs-12  padding-side-md">
                        <asp:Label runat="server" ID="Coveperid" Text= "<%$ Resources:LocalizedResource, PeriodCovered %>"></asp:Label>: &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label runat="server" ID="framss" Text= "<%$ Resources:LocalizedResource, From %>"></asp:Label>
                        <asp:Label ID="lblTertiaryPeriodFrom" runat="server"></asp:Label>
                        <rad:RadDatePicker ID="dpTertiaryPeriodFrom" runat="server" RenderMode="Lightweight"
                            ShowPopupOnFocus="true" Skin="Bootstrap" DateInput-ReadOnly="true">
                        </rad:RadDatePicker>
                        <asp:Label runat="server" ID="tosssss" Text= "<%$ Resources:LocalizedResource, To %>"></asp:Label>
                        <asp:Label ID="lblTertiaryPeriodTo" runat="server"></asp:Label>
                        <rad:RadDatePicker ID="dpTertiaryPeriodTo" runat="server" RenderMode="Lightweight"
                            ShowPopupOnFocus="true" Skin="Bootstrap" DateInput-ReadOnly="true">
                        </rad:RadDatePicker>
                        <label class="text-danger">
                        <span>
                        <asp:CompareValidator ID="CompareValidator3" runat="server" ForeColor="#a94442" ControlToValidate="dpTertiaryPeriodFrom"
                            ControlToCompare="dpTertiaryPeriodTo" Operator="LessThan" Type="Date" ErrorMessage="Start date must be less than End date">
                        </asp:CompareValidator>
                        </span>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                       <asp:Label runat="server" ID="Course1" Text= "<%$ Resources:LocalizedResource, Course %>"></asp:Label> :
                    </div>
                    <div class="col-md-3 pull-left " style="margin-right: 3%;">
                        <asp:Label ID="lblCourse" runat="server"></asp:Label>
                        <asp:TextBox ID="txtCourse" runat="server" CssClass="form-control" Width="350px" onkeyup="CheckMaxLength(this,50);" ondragstart="return false;" ondrop="return false;"
                            MaxLength="50"></asp:TextBox>
                        <label class="text-danger text-Chars">
                                    <span runat="server" id="txtCourseChars">50</span> <asp:Label runat="server" ID="Chamaining" Text= "<%$ Resources:LocalizedResource,  CharactersRemaining %>"></asp:Label></label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 ten-px-paddings" style="padding-bottom: 30px">
        <div class="row">
            <div class="col-md-2 col-md-12">
                <div class="bg-color-light-teal color-white font-bold five-px-padding">
                   <asp:Label runat="server" ID="XPer" Text= "<%$ Resources:LocalizedResource, Experience %>"></asp:Label> 
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 border-top-light-teal-sm ">
            </div>
            <div class="col-md-12 row-margin-5">
                <div class="row" style="padding-bottom:15px;">
                    <div class="col-md-2 col-xs-6">
                        <asp:Label runat="server" ID="PPH" Text= "<%$ Resources:LocalizedResource, PreviousPositionHeld%>"></asp:Label>:
                    </div>
                    <div class="col-md-10 col-xs-6 pull-left ">
                        <asp:Label ID="lblPreviousJob" runat="server"></asp:Label>
                        <asp:TextBox ID="txtPreviousJob" runat="server" CssClass="form-control" Width="350px" onkeyup="CheckMaxLength(this,50);" ondragstart="return false;" ondrop="return false;"
                            MaxLength="50"></asp:TextBox>
                        <label class="text-danger text-Chars">
                                    <span runat="server" ID="txtPreviousJobChars">50</span> <asp:Label runat="server" ID="charaterssremainss" Text= "<%$ Resources:LocalizedResource, CharactersRemaining %>"></asp:Label></label>
                    </div>
                </div>
                <div class="row" style="padding-bottom:15px;">
                    <div class="col-md-2 col-xs-6">
                       <asp:Label runat="server" ID="CName" Text= "<%$ Resources:LocalizedResource,  CompanyName %>"></asp:Label>:
                    </div>
                    <div class="col-md-10 col-xs-6 pull-left ">
                        <asp:Label ID="lblCompanyName" runat="server"></asp:Label>
                        <asp:TextBox ID="txtCompanyName" runat="server" CssClass="form-control" Width="350px" onkeyup="CheckMaxLength(this,50);" ondragstart="return false;" ondrop="return false;"
                            MaxLength="50"></asp:TextBox>
                        <label class="text-danger text-Chars">
                                    <span runat="server" ID="txtCompanyNameChars">50</span> <asp:Label runat="server" ID="remainscharter" Text= "<%$ Resources:LocalizedResource, CharactersRemaining %>"></asp:Label></label>
                    </div>
                </div>
                <div class="row" style="padding-bottom:15px;">
                    <div class="col-md-2 col-xs-6">
                        <asp:Label runat="server" ID="JDescript" Text= "<%$ Resources:LocalizedResource, JobDescription %>"></asp:Label>:
                    </div>
                    <div class="col-md-10 col-xs-6 pull-left ">
                        <asp:Label ID="lblJobDesc" runat="server"></asp:Label>
                        <asp:TextBox ID="txtJobDesc" runat="server" CssClass="form-control" Width="350px" onkeyup="CheckMaxLength(this,100);" ondragstart="return false;" ondrop="return false;"
                            MaxLength="100"></asp:TextBox>
                        <label class="text-danger text-Chars">
                                    <span runat="server" ID="txtJobDescChars">100</span> <asp:Label runat="server" ID="remaincharsss" Text= "<%$ Resources:LocalizedResource, CharactersRemaining %>"></asp:Label></label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <rad:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true" RenderMode="Lightweight"
        Skin="Bootstrap">
    </rad:RadWindowManager>
    <div class="">
        <asp:Button ID="btnUpdateProfile" runat="server" CssClass="btn btn-teal" Text="Update Profile" OnClientClick="HiddenElements();"
            OnClick="btnUpdateProfile_Click" />
        <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-teal" Text="Cancel" OnClick="btnCancel_Click" OnClientClick="HiddenElements();" />
    </div>
    <asp:Button ID="btnHiddenEdit" runat="server" CssClass="display-none" OnClick="btnEditProfile_Click" OnClientClick="VisibleElements();"
        ClientIDMode="Static" />
    <br />
    <br />
    <br />
</div>
<br />
