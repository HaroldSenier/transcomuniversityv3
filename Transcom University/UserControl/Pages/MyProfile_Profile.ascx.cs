﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TranscomUniversityV3Model;
using Telerik.Web.UI;

public partial class UserControl_Pages_MyProfile_Profile : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            SetDetailsOnView();
            SetDetailsTrue();
        }
    }

    #region MyProfileSummary

    private static bool IsImage(HttpPostedFile file)
    {
        return ((file != null) && System.Text.RegularExpressions.Regex.IsMatch(file.ContentType, "image/\\S+") && (file.ContentLength > 0));
    }

    protected void btnEditProfile_Click(object sender, EventArgs e)
    {
        SetDetailsOnEdit();
        SetDetailsFalse();

        // RadTab rlTab = (this.Parent.Parent).FindControl("Mus").FindControl("pnlMainBadge").FindControl("pnlLvResult").FindControl("RadLV") as RadTab;

    }

    protected void btnUpdateProfile_Click(object sender, EventArgs e)
    {
        try
        {
            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                var googleId = DataHelper.GetGoogleID();

                //db.pr_TranscomUniversity_UpdateUsersInfo(googleId, Convert.ToInt32(lblCIMNumber.Text), lblFirstName.Text, lblLastName.Text,
                //  lblGender.Text == "Male" ? 1 : 2, DateTime.Parse(lblDateOfBirth.Text), DateTime.Parse(lblStartDateWithTranscom.Text),
                //  Convert.ToInt32(cbPrimaryLanguage.SelectedValue), txtMobileNumber.Text, txtLandline.Text, lblTranscomEmail.Text,
                //  txtPersonalEmail.Text, lblRole.Text, lblSupervisor.Text, Convert.ToInt32(cbRegion.SelectedValue), Convert.ToInt32(cbCountry.SelectedValue),
                //  Convert.ToInt32(cbDepartment.SelectedValue), Convert.ToInt32(cbClient.SelectedValue), Convert.ToInt32(cbCampaign.SelectedValue),
                //  txtPrimaryEduc.Text, dpPrimaryPeriodFrom.SelectedDate != null ? Convert.ToDateTime(dpPrimaryPeriodFrom.SelectedDate) : DateTime.Today,
                //  dpPrimaryPeriodTo.SelectedDate != null ? Convert.ToDateTime(dpPrimaryPeriodTo.SelectedDate) : DateTime.Today.AddDays(1), txtSecondaryEduc.Text,
                //  dpSecondaryPeriodFrom.SelectedDate != null ? Convert.ToDateTime(dpSecondaryPeriodFrom.SelectedDate) : DateTime.Today,
                //  dpSecondaryPeriodTo.SelectedDate != null ? Convert.ToDateTime(dpSecondaryPeriodTo.SelectedDate) : DateTime.Today.AddDays(1),
                //  txtTertiaryEduc.Text, dpTertiaryPeriodFrom.SelectedDate != null ? Convert.ToDateTime(dpTertiaryPeriodFrom.SelectedDate) : DateTime.Today,
                //  dpTertiaryPeriodTo.SelectedDate != null ? Convert.ToDateTime(dpTertiaryPeriodTo.SelectedDate) : DateTime.Today.AddDays(1),
                //  txtCourse.Text, txtPreviousJob.Text, txtCompanyName.Text, txtJobDesc.Text);

                db.pr_TranscomUniversity_UpdateUsersInfo(googleId, Convert.ToInt32(lblCIMNumber.Text), lblFirstName.Text, lblLastName.Text,
                    lblGender.Text == "Male" ? 1 : 2, DateTime.Parse(lblDateOfBirth.Text), DateTime.Parse(lblStartDateWithTranscom.Text),
                    Convert.ToInt32(cbPrimaryLanguage.SelectedValue), txtMobileNumber.Text, txtLandline.Text, txtTranscomEmail.Text,
                    txtPersonalEmail.Text, lblRole.Text, lblSupervisor.Text, 1, 1,
                    1, 1, 1,
                    txtPrimaryEduc.Text,
                    dpPrimaryPeriodFrom.SelectedDate != null ? Convert.ToDateTime(dpPrimaryPeriodFrom.SelectedDate) : dpPrimaryPeriodFrom.SelectedDate,
                    dpPrimaryPeriodTo.SelectedDate != null ? Convert.ToDateTime(dpPrimaryPeriodTo.SelectedDate) : dpPrimaryPeriodTo.SelectedDate,
                    txtSecondaryEduc.Text,
                    dpSecondaryPeriodFrom.SelectedDate != null ? Convert.ToDateTime(dpSecondaryPeriodFrom.SelectedDate) : dpSecondaryPeriodFrom.SelectedDate,
                    dpSecondaryPeriodTo.SelectedDate != null ? Convert.ToDateTime(dpSecondaryPeriodTo.SelectedDate) : dpSecondaryPeriodTo.SelectedDate,
                    txtTertiaryEduc.Text,
                    dpTertiaryPeriodFrom.SelectedDate != null ? Convert.ToDateTime(dpTertiaryPeriodFrom.SelectedDate) : dpTertiaryPeriodFrom.SelectedDate,
                    dpTertiaryPeriodTo.SelectedDate != null ? Convert.ToDateTime(dpTertiaryPeriodTo.SelectedDate) : dpTertiaryPeriodTo.SelectedDate,
                    txtCourse.Text, txtPreviousJob.Text, txtCompanyName.Text, txtJobDesc.Text);

                var PrimaryEducchars = 50 - txtPrimaryEduc.Text.Length;
                txtPrimaryEducChars.InnerText = PrimaryEducchars.ToString();

                var SecondaryEducchars = 50 - txtSecondaryEduc.Text.Length;
                txtSecondaryEducChars.InnerText = SecondaryEducchars.ToString();

                var TertiaryEducchars = 50 - txtTertiaryEduc.Text.Length;
                txtTertiaryEducChars.InnerText = TertiaryEducchars.ToString();

                var Coursechars = 50 - txtCourse.Text.Length;
                txtCourseChars.InnerText = Coursechars.ToString();

                var PreviousJobchars = 50 - txtPreviousJob.Text.Length;
                txtPreviousJobChars.InnerText = PreviousJobchars.ToString();

                var CompanyNamechars = 50 - txtCompanyName.Text.Length;
                txtCompanyNameChars.InnerText = CompanyNamechars.ToString();

                var JobDescchars = 100 - txtJobDesc.Text.Length;
                txtJobDescChars.InnerText = JobDescchars.ToString();
            }

            RadWindowManager1.RadAlert("Profile Successfully Updated.", 330, 180, "Profile Updated", "", "");
        }
        catch
        {
            RadWindowManager1.RadAlert("Error updating your profile. Please contact your System Administrator.", 330, 180, "Error Message", "");
        }

        SetDetailsClear();
        SetDetailsOnView();
        SetDetailsTrue();
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        SetDetailsOnView();
        SetDetailsTrue();
    }

    public void SetDetailsClear()
    {
        cbRegion.Items.Clear();
        cbCountry.Items.Clear();
        cbDepartment.Items.Clear();
        cbClient.Items.Clear();
        cbCampaign.Items.Clear();
        cbPrimaryLanguage.Items.Clear();
        txtMobileNumber.Text = "";
        txtLandline.Text = "";
        txtPersonalEmail.Text = "";
        txtPrimaryEduc.Text = "";
        dpPrimaryPeriodFrom.Clear();
        dpPrimaryPeriodTo.Clear();
        txtSecondaryEduc.Text = "";
        dpSecondaryPeriodFrom.Clear();
        dpSecondaryPeriodTo.Clear();
        txtTertiaryEduc.Text = "";
        dpSecondaryPeriodFrom.Clear();
        dpSecondaryPeriodTo.Clear();
        txtCourse.Text = "";
        txtPreviousJob.Text = "";
        txtCompanyName.Text = "";
        txtJobDesc.Text = "";
    }



    public void SetDetailsTrue()
    {
        lblRegion.Visible = true;
        lblCountry.Visible = true;
        lblDepartment.Visible = true;
        lblClient.Visible = true;
        lblCampaign.Visible = true;
        lblPrimaryLanguage.Visible = true;
        lblTranscomEmail.Visible = true;
        lblMobileNumber.Visible = true;
        lblLandline.Visible = true;
        lblPersonalEmail.Visible = true;
        lblPrimaryEduc.Visible = true;
        lblPrimaryPeriodFrom.Visible = true;
        lblPrimaryPeriodTo.Visible = true;
        lblSecondaryEduc.Visible = true;
        lblSecondaryPeriodFrom.Visible = true;
        lblSecondaryPeriodTo.Visible = true;
        lblTertiaryEduc.Visible = true;
        lblTertiaryPeriodFrom.Visible = true;
        lblTertiaryPeriodTo.Visible = true;
        lblCourse.Visible = true;
        lblPreviousJob.Visible = true;
        lblCompanyName.Visible = true;
        lblJobDesc.Visible = true;

        cbRegion.Visible = false;
        cbCountry.Visible = false;
        cbDepartment.Visible = false;
        cbClient.Visible = false;
        cbCampaign.Visible = false;
        cbPrimaryLanguage.Visible = false;
        txtTranscomEmail.Visible = false;
        txtMobileNumber.Visible = false;
        txtLandline.Visible = false;
        txtPersonalEmail.Visible = false;
        txtPrimaryEduc.Visible = false;
        dpPrimaryPeriodFrom.Visible = false;
        dpPrimaryPeriodTo.Visible = false;
        txtSecondaryEduc.Visible = false;
        dpSecondaryPeriodFrom.Visible = false;
        dpSecondaryPeriodTo.Visible = false;
        txtTertiaryEduc.Visible = false;
        dpTertiaryPeriodFrom.Visible = false;
        dpTertiaryPeriodTo.Visible = false;
        txtCourse.Visible = false;
        txtPreviousJob.Visible = false;
        txtCompanyName.Visible = false;
        txtJobDesc.Visible = false;
        btnUpdateProfile.Visible = false;
        btnCancel.Visible = false;
    }

    public void SetDetailsFalse()
    {
        lblRegion.Visible = true;
        lblCountry.Visible = true;
        lblDepartment.Visible = true;
        lblClient.Visible = true;
        lblCampaign.Visible = true;
        lblPrimaryLanguage.Visible = false;
        lblTranscomEmail.Visible = false;
        lblMobileNumber.Visible = false;
        lblLandline.Visible = false;
        lblPersonalEmail.Visible = false;
        lblPrimaryEduc.Visible = false;
        lblPrimaryPeriodFrom.Visible = false;
        lblPrimaryPeriodTo.Visible = false;
        lblSecondaryEduc.Visible = false;
        lblSecondaryPeriodFrom.Visible = false;
        lblSecondaryPeriodTo.Visible = false;
        lblTertiaryEduc.Visible = false;
        lblTertiaryPeriodFrom.Visible = false;
        lblTertiaryPeriodTo.Visible = false;
        lblCourse.Visible = false;
        lblPreviousJob.Visible = false;
        lblCompanyName.Visible = false;
        lblJobDesc.Visible = false;

        //cbRegion.Visible = true;
        //cbCountry.Visible = true;
        //cbDepartment.Visible = true;
        //cbClient.Visible = true;
        //cbCampaign.Visible = true;
        cbPrimaryLanguage.Visible = true;
        txtTranscomEmail.Visible = true;
        txtMobileNumber.Visible = true;
        txtLandline.Visible = true;
        txtPersonalEmail.Visible = true;
        txtPrimaryEduc.Visible = true;
        dpPrimaryPeriodFrom.Visible = true;
        dpPrimaryPeriodTo.Visible = true;
        txtSecondaryEduc.Visible = true;
        dpSecondaryPeriodFrom.Visible = true;
        dpSecondaryPeriodTo.Visible = true;
        txtTertiaryEduc.Visible = true;
        dpTertiaryPeriodFrom.Visible = true;
        dpTertiaryPeriodTo.Visible = true;
        txtCourse.Visible = true;
        txtPreviousJob.Visible = true;
        txtCompanyName.Visible = true;
        txtJobDesc.Visible = true;
        btnUpdateProfile.Visible = true;
        btnCancel.Visible = true;
    }

    public void SetDetailsOnView()
    {
        try
        {
            using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
            {
                var googleId = DataHelper.GetGoogleID();
                var userInfo = DataHelper.GetUsersInfo();
                var reportsTo = db.pr_TranscomUniversity_ReportsTo(googleId).ToList();

                lblFirstName.Text = userInfo[0].FirstName;
                lblLastName.Text = userInfo[0].LastName;
                lblRole.Text = userInfo[0].Role;
                lblCompanySite.Text = userInfo[0].CompanySite;
                lblStatus.Text = userInfo[0].SAP_STATUS;
                lblSupervisor.Text = reportsTo[0].Supervisor;
                lblRegion.Text = userInfo[0].Region;
                lblCountry.Text = userInfo[0].Country;
                lblDepartment.Text = userInfo[0].Department;
                lblClient.Text = userInfo[0].Client;
                lblCampaign.Text = userInfo[0].Campaign;
                lblCIMNumber.Text = ((decimal)userInfo[0].CIMNumber).ToString();
                lblGender.Text = userInfo[0].Gender == "1" ? "Male" : "Female";
                lblDateOfBirth.Text = userInfo[0].DateOfBirth.ToString("MMMM dd, yyyy");
                lblStartDateWithTranscom.Text = userInfo[0].StartDate.ToString("MMMM dd, yyyy");
                lblPrimaryLanguage.Text = userInfo[0].Language;
                lblMobileNumber.Text = userInfo[0].MobileNumber;
                lblLandline.Text = userInfo[0].Landline;
                lblTranscomEmail.Text = userInfo[0].TranscomEmail;
                lblPersonalEmail.Text = userInfo[0].PersonalEmail;
                lblPrimaryEduc.Text = userInfo[0].PrimaryEducation;
                lblPrimaryPeriodFrom.Text = userInfo[0].PrimaryPeriodFrom != null ? userInfo[0].PrimaryPeriodFrom.ToString("MMMM yyyy") : "";
                lblPrimaryPeriodTo.Text = userInfo[0].PrimaryPeriodTo != null ? userInfo[0].PrimaryPeriodTo.ToString("MMMM yyyy") : "";
                lblSecondaryEduc.Text = userInfo[0].SecondaryEducation;
                lblSecondaryPeriodFrom.Text = userInfo[0].SecondaryPeriodFrom != null ? userInfo[0].SecondaryPeriodFrom.ToString("MMMM yyyy") : "";
                lblSecondaryPeriodTo.Text = userInfo[0].SecondaryPeriodTo != null ? userInfo[0].SecondaryPeriodTo.ToString("MMMM yyyy") : "";
                lblTertiaryEduc.Text = userInfo[0].TertiaryEducation;
                lblTertiaryPeriodFrom.Text = userInfo[0].TertiaryPeriodFrom != null ? userInfo[0].TertiaryPeriodFrom.ToString("MMMM yyyy") : "";
                lblTertiaryPeriodTo.Text = userInfo[0].TertiaryPeriodTo != null ? userInfo[0].TertiaryPeriodTo.ToString("MMMM yyyy") : "";
                lblCourse.Text = userInfo[0].Course;
                lblPreviousJob.Text = userInfo[0].PreviousPosition;
                lblCompanyName.Text = userInfo[0].CompanyName;
                lblJobDesc.Text = userInfo[0].JobDescription;
            }
        }
        catch (Exception ex)
        {
            RadWindowManager1.RadAlert("Error loading your profile. Please contact your System Administrator." + ex.Message, 330, 180, "Error Message", "");
        }
    }

    public void SetDetailsOnEdit()
    {
        try
        {
            var googleId = DataHelper.GetGoogleID();
            var userInfo = DataHelper.GetUsersInfo();

            //int regionId = userInfo[0].RegionID != null ? userInfo[0].RegionID : 0;
            //int countryId = userInfo[0].CountryID != null ? userInfo[0].CountryID : 0;
            //int departmentId = userInfo[0].DepartmentID != null ? userInfo[0].DepartmentID : 0;
            //int clientId = userInfo[0].ClientID != null ? userInfo[0].ClientID : 0;
            //int campaignId = userInfo[0].CampaignID != null ? userInfo[0].CampaignID : 0;
            int languageId = userInfo[0].LanguageID != null ? userInfo[0].LanguageID : 0;
            //cbRegion.Enabled = false;
            //cbCountry.Enabled = false;
            //cbDepartment.Enabled = false;
            //cbClient.Enabled = false;
            //cbCampaign.Enabled = false; 
            //if (regionId == 0)
            //{
            //    cbRegion.Visible = true;                
            //}
            //else
            //{
            //    cbRegion.Items.Clear();
            //    cbRegion.DataBind();
            //    cbRegion.SelectedValue = regionId.ToString();
            //}

            //if (countryId == 0)
            //{
            //    cbCountry.Visible = true;               
            //}
            //else
            //{
            //    cbCountry.Items.Clear();
            //    cbCountry.DataBind();
            //    cbCountry.SelectedValue = countryId.ToString();
            //}

            //if (departmentId == 0)
            //{
            //    cbDepartment.Visible = true;
            //}
            //else
            //{
            //    cbDepartment.Items.Clear();
            //    cbDepartment.DataBind();
            //    cbDepartment.SelectedValue = departmentId.ToString();
            //}

            //if (clientId == 0)
            //{
            //    cbClient.Visible = true;
            //}
            //else
            //{
            //    cbClient.Items.Clear();
            //    cbClient.DataBind();
            //    cbClient.SelectedValue = clientId.ToString();
            //}

            //if (campaignId == 0)
            //{
            //    cbCampaign.Visible = true;
            //}
            //else
            //{
            //    cbCampaign.Items.Clear();
            //    cbCampaign.DataBind();
            //    cbCampaign.SelectedValue = campaignId.ToString();
            //}

            if (languageId == 0)
            {
                cbPrimaryLanguage.Visible = true;
            }
            else
            {
                cbPrimaryLanguage.Items.Clear();
                cbPrimaryLanguage.DataBind();
                cbPrimaryLanguage.SelectedValue = languageId.ToString();
            }

            txtTranscomEmail.Text = userInfo[0].TranscomEmail;
            txtMobileNumber.Text = userInfo[0].MobileNumber;
            txtLandline.Text = userInfo[0].Landline;
            txtPersonalEmail.Text = userInfo[0].PersonalEmail;
            txtPrimaryEduc.Text = userInfo[0].PrimaryEducation;
            dpPrimaryPeriodFrom.SelectedDate = userInfo[0].PrimaryPeriodFrom;
            dpPrimaryPeriodTo.SelectedDate = userInfo[0].PrimaryPeriodTo;
            txtSecondaryEduc.Text = userInfo[0].SecondaryEducation;
            dpSecondaryPeriodFrom.SelectedDate = userInfo[0].SecondaryPeriodFrom;
            dpSecondaryPeriodTo.SelectedDate = userInfo[0].SecondaryPeriodTo;
            txtTertiaryEduc.Text = userInfo[0].TertiaryEducation;
            dpTertiaryPeriodFrom.SelectedDate = userInfo[0].TertiaryPeriodFrom;
            dpTertiaryPeriodTo.SelectedDate = userInfo[0].TertiaryPeriodTo;
            txtCourse.Text = userInfo[0].Course;
            txtPreviousJob.Text = userInfo[0].PreviousPosition;
            txtCompanyName.Text = userInfo[0].CompanyName;
            txtJobDesc.Text = userInfo[0].JobDescription;

            var PrimaryEducchars = 50 - txtPrimaryEduc.Text.Length;
            txtPrimaryEducChars.InnerText = PrimaryEducchars.ToString();

            var SecondaryEducchars = 50 - txtSecondaryEduc.Text.Length;
            txtSecondaryEducChars.InnerText = SecondaryEducchars.ToString();

            var TertiaryEducchars = 50 - txtTertiaryEduc.Text.Length;
            txtTertiaryEducChars.InnerText = TertiaryEducchars.ToString();

            var Coursechars = 50 - txtCourse.Text.Length;
            txtCourseChars.InnerText = Coursechars.ToString();

            var PreviousJobchars = 50 - txtPreviousJob.Text.Length;
            txtPreviousJobChars.InnerText = PreviousJobchars.ToString();

            var CompanyNamechars = 50 - txtCompanyName.Text.Length;
            txtCompanyNameChars.InnerText = CompanyNamechars.ToString();

            var JobDescchars = 100 - txtJobDesc.Text.Length;
            txtJobDescChars.InnerText = JobDescchars.ToString();
        }
        catch (Exception e)
        {
            RadWindowManager1.RadAlert("Error loading your profile. Please contact your System Administrator." + e.Message, 330, 180, "Error Message", "");
        }
    }

    #endregion
}