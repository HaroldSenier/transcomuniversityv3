﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MyProfile_StarcAssessment.ascx.cs"
    Inherits="UserControl_Pages_MyProfile_StarcAssessment" %>
<rad:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">

        Sys.Application.add_load(function () {
            $(".RadMultiPage").css({ "min-height": "600px" });
            initializeDropDown();
        });

        function openDropDownFn() {
            $("#starcDateDropDown")[0].classList.toggle('show')
        }

        function initializeDropDown() {
            // Close the dropdown menu if the user clicks outside of it
            window.onclick = function (event) {
                if (!event.target.matches('.dropbtn')) {

                    var dropdowns = $(".dropdown-content");
                    var i;
                    for (i = 0; i < dropdowns.length; i++) {
                        var openDropdown = dropdowns[i];
                        if (openDropdown.classList.contains('show')) {
                            openDropdown.classList.remove('show');
                        }
                    }
                }
            }
        }


        $.fn.isInViewport = function () {
            var elementTop = $(this).offset().top;
            var elementBottom = elementTop + $(this).outerHeight();
            var viewportTop = $(window).scrollTop();
            var viewportBottom = viewportTop + $(window).height();
            return elementBottom > viewportTop && elementTop < viewportBottom;
        };

        function pageLoad() {

            loadOverAllStarc();
            loadStarcCircleProgress();
            countUpProgress();

            $(window).scroll(function () {
                if ($(".js-progressbar-starc").isInViewport()) {
                    $(".progress-bar").each(function () {
                        var progress = parseInt($(this).attr("aria-valuenow"));

                        $(this).css("cssText", "width: " + progress + "% !important;");

                        $(this).siblings('.starc-end-marker-icon').find('span').html(parseFloat((progress * 100) / 100).toFixed(2) + "%");
                        //$(this).animate({ width: progress }, 500);

                    });
                }
            })


        }

        function countUpProgress() {


            $('.countup').each(function () {
                $(this).prop('Counter', 0).animate({
                    Counter: parseFloat($(this).attr('data-count'))
                }, {
                    duration: 1500,
                    easing: 'swing',
                    step: function (now) {

                        $(this).text(Math.round(now * 100) / 100 + "%");

                    }
                });

            });


        }

        function loadOverAllStarc() {
            var totalScore = $("#hfTotalScore").val();
            $("#dashed-circle-progress .percentage")[0].setAttribute('data-count', totalScore);

            var percent = .75
            speed = 1.5,
            props = {
                x: 0
            };

            startCircularProgress(percent, speed);

            function startCircularProgress(percentage, speed) {
                TweenMax.to(props, speed, {
                    x: percentage,
                    ease: Power3.easeOut,
                    onUpdate: function () {
                        //console.log(props.x);
                        drawProgress(props.x);
                    }
                });
            }

            function clamp(n, min, max) {
                return Math.max(min, Math.min(max, n));
            };

            function drawProgress(percent) {

                if (isNaN(percent)) {
                    return;
                }

                percent = clamp(parseFloat(percent), 0, 1);

                // 360 loops back to 0, so keep it within 0 to < 360
                var angle = clamp(percent * 360, 0, 359.99999);
                var paddedRadius = 49 + 1;
                var radians = (angle * Math.PI / 180);
                var x = Math.sin(radians) * paddedRadius;
                var y = Math.cos(radians) * -paddedRadius;
                var mid = (angle > 180) ? 1 : 0;
                var pathData = 'M 0 0 v -%@ A %@ %@ 1 '.replace(/%@/gi, paddedRadius) +
                  mid + ' 1 ' +
                  x + ' ' +
                  y + ' z';

                var bar = document.getElementsByClassName('progress-radial-bar')[0];
                bar.setAttribute('d', pathData);
            };

        }

        function loadStarcCircleProgress() {
            $(".circle-progress").each(function (index) {
                var value = parseFloat(Math.round($(this).find('.hfValue').val() * 100) / 100).toFixed(2); ;
                var name = $(this).find('.hfName').val();
                $(this).find('.progressbar-marker').css({ 'transform': 'rotate(' + (360 * value / 100) + 'deg)' });
                $(this).find('.progressbar-line').css({ 'stroke-dashoffset': 304.777 * (1 - value / 100) + 'px' });
                $(this).find('.starcValue')[0].setAttribute('data-count', value);
                $(this).find('.starcValue').html(value + "%");
                $(this).find('.starcName').html(name);
            });
        }

        function showProgressToolTip(el) {
            $(el).siblings(".starc-end-marker-icon").removeClass('ihide');
        }

        function hideProgressToolTip(el) {
            $(el).siblings(".starc-end-marker-icon").addClass('ihide');
        }

    </script>
</rad:RadScriptBlock>
<div class="container starc">
    <br />
    <br />
    <div class="col-md-1">
        <div class="dropdown">
            <button onclick="openDropDownFn(); return false;" class="dropbtn btn btn-circle btn-md btn-gray btn-select-date">
                <i class="fa fa-bars" aria-hidden="true" ></i>
            </button>
            <div id="starcDateDropDown" class="dropdown-content">
                <a href="#">January 5, 2017</a> <a href="#">February 14 2016</a>
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <div id="dashed-circle-progress" style="position: relative; left: 50%; transform: translateX(-40%);">
            <svg class="progress-radial" width="210px" height="210px" viewbox="0 0 120 120" shape-rendering="geometricPrecision">
              <defs>
                <mask id="circle_mask" x="0" y="0" width="100" height="100" maskUnits="userSpaceOnUse">
                  <circle cx="50" cy="50" r="51" 
                          stroke-width="0" fill="black" opacity="1"/>
                  <circle id="bar" r="50" cx="50" cy="50" fill="transparent" 
                          stroke-dasharray="10" stroke-dashoffset="1000" stroke="white" stroke-width="15"></circle>
                  <circle class="progress-radial-mask-inner" 
                          cx="50" cy="50" r="40" stroke-width="0" fill="black" opacity="1"/>
                </mask>
              </defs>
              <g mask="url(#circle_mask)">
                <circle class="progress-radial-track" cx="50" cy="50" r="50"  opacity="1" fill="#b3b2d1"/>
                <path class="progress-radial-bar" transform="translate(50, 50)" 
                      d="M 0 0" fill="#020067">
                </path>
              </g>
               <text class="percentage countup" x="50%" y="50%" fill="#626262">82.00%
                </text>
                  <text class="ptext" x="50%" y="50%" fill="#626262">TOTAL SCORE
                </text>
            </svg>
            <asp:HiddenField ID="hfTotalScore" runat="server" Value="82.89" ClientIDMode="Static" />
        </div>
        <span class="bold">Leadership Level:</span>
        <asp:Literal ID="ltLevel" Text="Intermediate" runat="server" />
        <br />
        <br />
        <span class="bold">STARC Assessment:</span>
        <asp:Literal ID="ltAssessmentDate" Text="09/05/2018" runat="server" />
    </div>
    <div class="col-md-9">
        <span class="labelTitle">STARC ASSESSMENT OVERALL RESULTS</span>
        <br />
        <br />
        <div class="col-md-2 progress-container">
            <div class="circle-progress">
                <svg viewbox="-5 -5 110 110" width="120px">
                <circle class="progressbar-back" r="48.5" cx="50" cy ="50"/>
                <circle class="progressbar-line" r="48.5" cx="50" cy ="50" />
                <circle class="progressbar-marker" r="6" cx="50" cy="1.5" />
                <text class="starcValue countup" x="47%" y="40%" text-anchor="middle"  >00.00%</text>
                <text class="starcName" x="47%" y="60%" text-anchor="middle" >Leadership</text>
            </svg>
                <input type="hidden" value="77.68" class="hfValue" />
                <input type="hidden" value="Ability" class="hfName" />
            </div>
            <p class="description">
                The employee's proficiency in 7 Leadership Competencies, identified as key to successfully
                leading leading and managing the organization.
            </p>
        </div>
        <div class="col-md-2 progress-container">
            <div class="circle-progress">
                <svg viewbox="-5 -5 110 110" width="120px">
                <circle class="progressbar-back" r="48.5" cx="50" cy ="50"/>
                <circle class="progressbar-line" r="48.5" cx="50" cy ="50" />
                <circle class="progressbar-marker" r="6" cx="50" cy="1.5" />
                <text class="starcValue countup" x="47%" y="40%" text-anchor="middle" >00.00%</text>
                <text class="starcName" x="47%" y="60%" text-anchor="middle" >Leadership</text>
            </svg>
                <input type="hidden" value="77.68" class="hfValue" />
                <input type="hidden" value="Ability" class="hfName" />
            </div>
            <p class="description">
                The employee's proficiency in 7 Leadership Competencies, identified as key to successfully
                leading leading and managing the organization.
            </p>
        </div>
        <div class="col-md-2 progress-container">
            <div class="circle-progress">
                <svg viewbox="-5 -5 110 110" width="120px">
                <circle class="progressbar-back" r="48.5" cx="50" cy ="50"/>
                <circle class="progressbar-line" r="48.5" cx="50" cy ="50" />
                <circle class="progressbar-marker" r="6" cx="50" cy="1.5" />
                <text class="starcValue countup" x="47%" y="40%" text-anchor="middle" >00.00%</text>
                <text class="starcName" x="47%" y="60%" text-anchor="middle" >Leadership</text>
            </svg>
                <input type="hidden" value="77.68" class="hfValue" />
                <input type="hidden" value="Ability" class="hfName" />
            </div>
            <p class="description">
                The employee's proficiency in 7 Leadership Competencies, identified as key to successfully
                leading leading and managing the organization.
            </p>
        </div>
        <div class="col-md-2 progress-container">
            <div class="circle-progress">
                <svg viewbox="-5 -5 110 110" width="120px">
                <circle class="progressbar-back" r="48.5" cx="50" cy ="50"/>
                <circle class="progressbar-line" r="48.5" cx="50" cy ="50" />
                <circle class="progressbar-marker" r="6" cx="50" cy="1.5" />
                <text class="starcValue countup" x="47%" y="40%" text-anchor="middle" >00.00%</text>
                <text class="starcName" x="47%" y="60%" text-anchor="middle" >Leadership</text>
            </svg>
                <input type="hidden" value="77.68" class="hfValue" />
                <input type="hidden" value="Ability" class="hfName" />
            </div>
            <p class="description">
                The employee's proficiency in 7 Leadership Competencies, identified as key to successfully
                leading leading and managing the organization.
            </p>
        </div>
        <div class="col-md-2 progress-container">
            <div class="circle-progress">
                <svg viewbox="-5 -5 110 110" width="120px">
                <circle class="progressbar-back" r="48.5" cx="50" cy ="50"/>
                <circle class="progressbar-line" r="48.5" cx="50" cy ="50" />
                <circle class="progressbar-marker" r="6" cx="50" cy="1.5" />
                <text class="starcValue countup" x="47%" y="40%" text-anchor="middle" >00.00%</text>
                <text class="starcName" x="47%" y="60%" text-anchor="middle" >Leadership</text>
            </svg>
                <input type="hidden" value="77.68" class="hfValue" />
                <input type="hidden" value="Ability" class="hfName" />
            </div>
            <p class="description">
                The employee's proficiency in 7 Leadership Competencies, identified as key to successfully
                leading leading and managing the organization.
            </p>
        </div>
    </div>
    <div class="col-md-12 ten-px-paddings" style="padding-bottom: 30px; padding-top: 5%;">
        <div class="row">
            <div class="col-md-2 col-md-12">
                <div class="bg-color-light-teal color-white font-bold five-px-padding">
                    Strengths
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="border-top-light-teal">
                    <asp:SqlDataSource ID="dsStr" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection%>"
                        SelectCommand="exec pr_TranscomUniversity_GetStarcAssessment @CIM,  @isStrength">
                    </asp:SqlDataSource>
                    <asp:Repeater ID="rptStarcStrength" runat="server" DataSourceID="dsStr">
                        <ItemTemplate>
                            <div class="row" style="margin: 1% 0 0 0;">
                                <div class="col-md-2 font-capitalize">
                                    <span class="bold pull-right ">
                                        <%# Eval("Name") %></span>
                                </div>
                                <div class="col-md-4 no-padding js-progressbar-starc">
                                    <div class="progress" style="position: relative; top: 10px;">
                                        <div class="progress-bar _<%# Eval("AssessmentValue")%>" role="progressbar" aria-valuenow='<%# Eval("AssessmentValue")%>'
                                            aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                            <%-- <label class="pull-left font-uppercase" style="line-height: 30px;">
                                            &nbsp;&nbsp;<%# Eval("Name") %>&nbsp;&nbsp;</label>
                                        <label class="pull-right" style="line-height: 30px;">
                                            &nbsp;&nbsp;<%# Eval("AssessmentValue")%>%&nbsp;&nbsp;</label>--%>
                                        </div>
                                        <span id="progressEndMarker" class="progress-end-marker pointer" runat="server" onmouseover="showProgressToolTip(this);"
                                            onmouseout="hideProgressToolTip(this);"></span><i class="fa fa-map-marker starc-end-marker-icon ihide"
                                                aria-hidden="true"><span>00.00%</span></i>
                                    </div>
                                </div>
                                <div class="col-md-6 font-lowercase text-left">
                                    <%# Eval("Description") %>
                                </div>
                            </div>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="defaultItem" runat="server" Visible='<%# rptStarcStrength.Items.Count == 0 %>'
                                Text="No Strength Available" />
                        </FooterTemplate>
                    </asp:Repeater>
                    <div class="col-md-4 col-md-offset-2" style="display: flex; justify-content: space-between;
                        padding: 0; font-size: 10px; position: relative;">
                        <div>
                            00%</div>
                        <div>
                            10%</div>
                        <div>
                            20%</div>
                        <div>
                            30%</div>
                        <div>
                            40%</div>
                        <div>
                            50%</div>
                        <div>
                            60%</div>
                        <div>
                            70%</div>
                        <div>
                            80%</div>
                        <div>
                            90%</div>
                        <div>
                            100%</div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 ten-px-paddings" style="padding-bottom: 30px; padding-top: 5%;">
        <div class="row">
            <div class="col-md-2 col-md-12">
                <div class="bg-color-light-teal color-white font-bold five-px-padding">
                    Opportunities
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="border-top-light-teal">
                    <asp:SqlDataSource ID="dsOpp" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection%>"
                        SelectCommand="exec pr_TranscomUniversity_GetStarcAssessment @CIM,  @isStrength">
                    </asp:SqlDataSource>
                    <asp:Repeater ID="rptStarcOpportunity" runat="server" DataSourceID="dsOpp">
                        <ItemTemplate>
                            <div class="row" style="margin: 1% 0 0 0;">
                                <div class="col-md-2 font-capitalize">
                                    <span class="bold pull-right ">
                                        <%# Eval("Name") %></span>
                                </div>
                                <div class="col-md-4 no-padding js-progressbar-starc">
                                    <div class="progress" style="position: relative; top: 10px;">
                                        <div class="progress-bar" role="progressbar" aria-valuenow='<%# Eval("AssessmentValue")%>'
                                            aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                            <%--  <label class="pull-left font-uppercase" style="line-height: 30px;">
                                            &nbsp;&nbsp;<%# Eval("Name") %>&nbsp;&nbsp;</label>
                                        <label class="pull-right" style="line-height: 30px;">
                                            &nbsp;&nbsp;<%# Eval("AssessmentValue")%>%&nbsp;&nbsp;</label>--%>
                                        </div>
                                        <span id="progressEndMarker" class="progress-end-marker pointer" runat="server" onmouseover="showProgressToolTip(this);"
                                            onmouseout="hideProgressToolTip(this);"></span><i class="fa fa-map-marker starc-end-marker-icon ihide"
                                                aria-hidden="true"><span>00.00%</span></i>
                                    </div>
                                </div>
                                <div class="col-md-6 font-lowercase text-left">
                                    <%# Eval("Description") %>
                                </div>
                            </div>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="defaultItem" runat="server" Visible='<%# rptStarcOpportunity.Items.Count == 0 %>'
                                Text="No Opportunities Available" />
                        </FooterTemplate>
                    </asp:Repeater>
                    <div class="col-md-4 col-md-offset-2" style="display: flex; justify-content: space-between;
                        padding: 0; font-size: 10px; position: relative;">
                        <div>
                            00%</div>
                        <div>
                            10%</div>
                        <div>
                            20%</div>
                        <div>
                            30%</div>
                        <div>
                            40%</div>
                        <div>
                            50%</div>
                        <div>
                            60%</div>
                        <div>
                            70%</div>
                        <div>
                            80%</div>
                        <div>
                            90%</div>
                        <div>
                            100%</div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>
        </div>
        <br />
        <div class="col-md-12 text-center">
            <br />
            <span class="bold ">INSIGHTS AND RECOMMENDED ACTIONS</span>
            <br />
            <br />
            <rad:RadTextBox TextMode="MultiLine" Rows="5" Width="100%" CssClass="form-control btn-soft"
                EmptyMessage="INSIGHTS AND RECOMMENDED ACTIONS" runat="server" ReadOnly="true" RenderMode="Lightweight" Skin="Bootstrap">
            </rad:RadTextBox>
            <br />
            <br />
        </div>
    </div>
</div>
