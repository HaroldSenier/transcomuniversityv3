﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class UserControl_Pages_MyProfile_StarcAssessment : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string CIM = DataHelper.GetCurrentUserCIM();
        dsStr.SelectParameters.Add("CIM", DbType.Int32, CIM);
        dsStr.SelectParameters.Add("isStrength", DbType.Boolean, "true");
        dsOpp.SelectParameters.Add("CIM", DbType.Int32, CIM);
        dsOpp.SelectParameters.Add("isStrength", DbType.Boolean, "false");
    }
}