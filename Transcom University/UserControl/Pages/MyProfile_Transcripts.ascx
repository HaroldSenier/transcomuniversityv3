﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MyProfile_Transcripts.ascx.cs"
    Inherits="UserControl_Pages_MyProfile_Transcripts" %>
<rad:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript" language="javascript">
        Sys.Application.add_load(function () {
            $(".RadMultiPage").css({ "min-height": "400px" });
        });

        $(function () {
            $('.progressBarContainer').each(function (i, obj) {
                debugger;
                var hidValue = $('input[type=hidden]', this).val();
                //alert(parseFloat(hidValue) + '%');
                jQuery('[id=spanval]', this).text(parseFloat(hidValue) + '%');

                var progressBarWidth = hidValue * $('#progressBar').width() / 100;
                jQuery('[id=progress]', this).animate({ width: progressBarWidth }, 3000).html("&nbsp;");

                if (hidValue >= 0 && hidValue <= 50) {
                    jQuery('[id=progress]', this).addClass('red');
                }
                if (hidValue >= 50 && hidValue <= 75) {
                    jQuery('[id=progress]', this).addClass('yellow');
                }
                if (hidValue >= 75 && hidValue <= 100) {
                    jQuery('[id=progress]', this).addClass('green');
                }
            });
        });
    </script>
</rad:RadScriptBlock>
<br />
<div class="col-md-12">
    <rad:RadGrid ID="rgMyTranscripts" runat="server" RenderMode="Lightweight" OnNeedDataSource="rgMyTranscripts_NeedDataSource"
        AutoGenerateColumns="False" PageSize="10">
        <%--OnItemDataBound="rgMyTranscripts_ItemDataBound"--%>
        <ClientSettings EnableAlternatingItems="true" AllowColumnsReorder="true" EnableRowHoverStyle="true">
            <Selecting AllowRowSelect="true"></Selecting>
            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
            <Resizing AllowColumnResize="true" ResizeGridOnColumnResize="true" AllowResizeToFit="true" />
        </ClientSettings>
        <ItemStyle Wrap="false"></ItemStyle>
        <MasterTableView DataKeyNames="CourseID,  CourseName, DateCourseTaken" AllowSorting="true"
            AllowPaging="true" CommandItemDisplay="None" HeaderStyle-ForeColor="Teal">
            <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
            <Columns>
                <rad:GridBoundColumn UniqueName="CourseID" HeaderText="<%$ Resources:LocalizedResource, CourseId %>" DataField="CourseID"
                    AllowFiltering="False" Display="False" />
                <rad:GridBoundColumn UniqueName="CourseName" HeaderText="<%$ Resources:LocalizedResource, CourseName %>" DataField="CourseName"
                    FilterControlWidth="150" CurrentFilterFunction="Contains" AutoPostBackOnFilter="True"
                    ShowFilterIcon="False" FilterControlToolTip="Search by Course Name" />
                <rad:GridDateTimeColumn UniqueName="DateCourseTaken" HeaderText="<%$ Resources:LocalizedResource, DateCourseTaken %>"
                    DataField="DateCourseTaken" AllowFiltering="False" DataType="System.DateTime"
                    DataFormatString="{0:yyyy-MM-dd}" HeaderStyle-Width="200px" />
                <%--    <rad:GridTemplateColumn UniqueName="Progress" HeaderText="Progress" DataField="Progress"
                    AllowFiltering="false">
                    <ItemStyle HorizontalAlign="Left" />
                    <HeaderStyle Width="120" HorizontalAlign="Center" />
                    <ItemTemplate>
                        <div class='progressBarContainer'>
                            <asp:HiddenField ID="hidProgressVal" runat="server" Value="" />
                            <div id='progressBar'>
                                <span id="spanval"></span>
                                <div id="progress">
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </rad:GridTemplateColumn>
                <rad:GridTemplateColumn AllowFiltering="False" ReadOnly="True" UniqueName="ViewDetails"
                    HeaderText="Previous Scores">
                    <ItemTemplate>
                        <asp:LinkButton ID="btnPreviousScore" runat="server" ToolTip="View Previous Scores"
                            OnClientClick="PreviousScores()" OnClick="btnPreviousScore_Click" Text="View" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </rad:GridTemplateColumn>
                <rad:GridBoundColumn UniqueName="Score" HeaderText="Highest Score" DataField="Score"
                    AllowFiltering="False" />
                <rad:GridDateTimeColumn UniqueName="DateTestTaken" HeaderText="Date Test Taken"
                    DataField="DateTestTaken" AllowFiltering="False" DataType="System.DateTime" DataFormatString="{0:yyyy-MM-dd}" />--%>
            </Columns>

            <SortExpressions>
                <rad:GridSortExpression FieldName="DateCourseTaken" SortOrder="Descending" />
            </SortExpressions>
            <NoRecordsTemplate>
                &nbsp;
                <asp:Label Text="Transcript is empty." runat="server" />
            </NoRecordsTemplate>
        </MasterTableView>
        <ClientSettings EnableRowHoverStyle="True" AllowDragToGroup="false" AllowColumnsReorder="false" ReorderColumnsOnClient="false" >
            <Selecting AllowRowSelect="True" />
        </ClientSettings>
    </rad:RadGrid>
</div>
<br />
<br />
<rad:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <rad:AjaxSetting AjaxControlID="rgMyTranscripts">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="rgMyTranscripts" UpdatePanelRenderMode="Inline"
                    LoadingPanelID="localLoading" />
            </UpdatedControls>
        </rad:AjaxSetting>
    </AjaxSettings>
</rad:RadAjaxManagerProxy>
