﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

public partial class UserControl_Pages_MyProfile_Transcripts : System.Web.UI.UserControl
{
    public static string connection = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
    SqlConnection conn = new SqlConnection(connection);
    public SqlCommand cmd = new SqlCommand();
    static int CIM;

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Form.Attributes.Add("enctype", "multipart/form-data");
        //if (!Page.IsPostBack)
            CIM = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
    }

    protected void rgMyTranscripts_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        //var transcomEmail = HttpContext.Current.User.Identity.Name.Split('|')[0];

        //cmd.Connection = conn;
        //cmd.CommandType = CommandType.StoredProcedure;
        //cmd.Parameters.AddWithValue("@CurrentUser", transcomEmail);
        //cmd.CommandText = "pr_TranscomUniversity_MyTranscript";

        //DataSet dt = new DataSet();
        //SqlDataAdapter adp = new SqlDataAdapter();
        //adp.SelectCommand = cmd;
        //adp.Fill(dt);
        rgMyTranscripts.DataSource = DataHelper.MyTranscript(CIM);
        
    }

    //protected void rgMyTranscripts_ItemDataBound(object sender, GridItemEventArgs e)
    //{
    //    if (e.Item is GridDataItem)
    //    {
    //        var dataItem = e.Item as GridDataItem;
    //        var testCategoryId = dataItem.GetDataKeyValue("TestCategoryID").ToString();
    //        var dateCourseTaken = dataItem.GetDataKeyValue("DateCourseTaken").ToString();

    //        int? passed = 0;

    //        if (DBNull.Value.Equals(dataItem.GetDataKeyValue("Passed")))
    //            passed = 0;
    //        else
    //            passed = Convert.ToInt32(dataItem.GetDataKeyValue("Passed"));

    //        var btnPreviousScore = e.Item.FindControl("btnPreviousScore") as LinkButton;
    //        btnPreviousScore.Visible = Convert.ToUInt32(testCategoryId) > 0;

    //        var progressval = 0;
    //        if (testCategoryId != "0") //COURSE WITH ASSESSMENT
    //        {
    //            progressval = 20;

    //            if (Convert.ToInt32(passed) == 1)
    //                progressval = progressval + 80;
    //        }
    //        else //COURSE WO ASSESSMENT
    //        {
    //            if (!string.IsNullOrEmpty(dateCourseTaken))
    //                progressval = 100;
    //        }

    //        var hidProgressVal = (HiddenField)dataItem["Progress"].FindControl("hidProgressVal");
    //        hidProgressVal.Value = progressval.ToString();
    //    }
    //}
    //protected void btnPreviousScore_Click(object sender, EventArgs e)
    //{
    //    //var btn = (LinkButton)sender;
    //    //var item = btn.NamingContainer as GridDataItem;

    //    //var testCategoryId = item.GetDataKeyValue("TestCategoryID").ToString();
    //    //var courseName = item.GetDataKeyValue("CourseName").ToString();

    //    //var qryList = DataHelper.GetCourseHistoryByCategoryId(TwwId, Convert.ToInt32(testCategoryId));
    //    //if (qryList != null)
    //    //{
    //    //    lblCourseName.Text = courseName;
    //    //    gridPreviousScores.DataSource = qryList;
    //    //    gridPreviousScores.DataBind();
    //    //}
    //}
}