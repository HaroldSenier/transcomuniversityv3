﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NewsUserCtrl.ascx.cs"
    Inherits="NewsUserCtrl" %>
<div class="col-md-12 gray-border-no-pads ten-px-padding bg-color-light-gray">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-12">
                <div class="row">
                    <div class="table-responsive">
                        <rad:RadGrid ID="radNews" runat="server" AllowPaging="True" AllowSorting="True" GroupPanelPosition="Top"
                            ResolvedRenderMode="Classic" Skin="Bootstrap" CssClass="table table-condensed"
                            OnNeedDataSource="radNews_NeedDataSource" OnUpdateCommand="radNews_UpdateCommand"
                            OnItemDataBound="radNews_ItemDataBound" OnItemUpdated="radNews_ItemUpdated" OnInsertCommand="radNews_InsertCommand"
                            OnItemCommand="radNews_ItemCommand" OnPreRender="radNews_PreRender">
                            <MasterTableView CommandItemDisplay="Top" AutoGenerateColumns="false" DataKeyNames="id">
                                <CommandItemSettings ShowExportToExcelButton="false" AddNewRecordText="Add News"
                                    ShowExportToCsvButton="false" />
                                <EditFormSettings EditFormType="Template">
                                    <FormTemplate>
                                        <div class="col-md-12 col-md-6" style="margin: 10px;">
                                            <div class="row">
                                                <h3>
                                                    <%# (Container is GridEditFormInsertItem) ? "Create News" : "Update News" %></h3>
                                                <h5 class="font-bold">
                                                   <asp:Label runat="server" ID="NewsTitle" Text= "<%$ Resources:LocalizedResource,  NewsTitle %>"></asp:Label></h5>
                                                <asp:TextBox ID="TxtNewsTitle" runat="server" EmptyMessage="Please enter news title"
                                                    Text='<%# Eval("NewsTitle") %>' />
                                                <asp:RequiredFieldValidator ID="reqNewsTitle" runat="server" Text="Enter news title"
                                                    ControlToValidate="TxtNewsTitle"></asp:RequiredFieldValidator>
                                                <h5 class="font-bold">
                                                    <asp:Label runat="server" ID="categors" Text= "<%$ Resources:LocalizedResource, Category %>"></asp:Label></h5>
                                                <asp:Label runat="server" ID="CategoryID" Text='<%# Eval("CategoryID") %>' Visible="false" />
                                                <rad:RadComboBox ID="cboST" runat="server" EmptyMessage="Choose category" DataTextField="name"
                                                    DataValueField="id" AppendDataBoundItems="true" Sort="Ascending" DropDownAutoWidth="Enabled"
                                                    AllowCustomText="true" MarkFirstMatch="true" Skin="Bootstrap">
                                                    <Items>
                                                        <rad:RadComboBoxItem Text="" Value="" />
                                                    </Items>
                                                </rad:RadComboBox>
                                                <asp:RequiredFieldValidator ID="reqST" runat="server" Text="*" ControlToValidate="cboST"></asp:RequiredFieldValidator>
                                                <h5 class="font-bold">
                                                    <asp:Label runat="server" ID="contes" Text= "<%$ Resources:LocalizedResource, Content %>"></asp:Label></h5>
                                                <rad:RadEditor runat="server" Content='<%# Bind("NewContent")%>' ContentFilters="DefaultFilters,MakeUrlsAbsolute"
                                                    ID="EditorContent" Width="100%" Height="100%" DialogHandlerUrl="~/Telerik.Web.UI.DialogHandler.axd"
                                                    Skin="Bootstrap">
                                                    <Tools>
                                                        <rad:EditorToolGroup Tag="FileManagers">
                                                            <rad:EditorTool Name="ImageManager"></rad:EditorTool>
                                                            <rad:EditorTool Name="FlashManager"></rad:EditorTool>
                                                            <rad:EditorTool Name="MediaManager"></rad:EditorTool>
                                                            <rad:EditorTool Name="LinkManager"></rad:EditorTool>
                                                        </rad:EditorToolGroup>
                                                        <rad:EditorToolGroup>
                                                            <rad:EditorTool Name="InsertTable"></rad:EditorTool>
                                                            <rad:EditorTool Name="Bold"></rad:EditorTool>
                                                            <rad:EditorTool Name="Italic"></rad:EditorTool>
                                                            <rad:EditorTool Name="Underline"></rad:EditorTool>
                                                            <rad:EditorSeparator></rad:EditorSeparator>
                                                            <rad:EditorTool Name="ForeColor"></rad:EditorTool>
                                                            <rad:EditorTool Name="BackColor"></rad:EditorTool>
                                                            <rad:EditorSeparator></rad:EditorSeparator>
                                                            <rad:EditorTool Name="FontName"></rad:EditorTool>
                                                            <rad:EditorTool Name="RealFontSize"></rad:EditorTool>
                                                        </rad:EditorToolGroup>
                                                    </Tools>
                                                    <ImageManager ViewPaths="~/Media/Uploads/News" UploadPaths="~/Media/Uploads/News"
                                                        DeletePaths="~/Media/Uploads/News,~/Media/Uploads/News" />
                                                </rad:RadEditor>
                                                <asp:RequiredFieldValidator ID="reqNewsContent" runat="server" Text="Enter content"
                                                    ControlToValidate="EditorContent" Visible="false"></asp:RequiredFieldValidator>
                                                <h5 class="font-bold">
                                                  <asp:Label runat="server" ID="Inactive" Text= "<%$ Resources:LocalizedResource, Inactive %>"></asp:Label></h5>
                                                <asp:CheckBox ID="chkInactive" runat="server" Checked='<%# Eval("Inactive") == DBNull.Value ? false : Convert.ToBoolean(Eval("Inactive")) %>' />
                                                <asp:Label runat="server" ID="toggletoactivatedeactivate" Text= "<%$ Resources:LocalizedResource, toggletoactivatedeactivate %>"></asp:Label>
                                            </div>
                                            <div class="row">
                                                <div style="margin-top: 20px;">
                                                    <asp:Button ID="btnUpdate" CssClass="btn btn-teal" Text='<%# (Container is GridEditFormInsertItem) ? "Insert" : "Update" %>'
                                                        runat="server" CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'>
                                                    </asp:Button>&nbsp;
                                                    <asp:Button ID="btnCancel" CssClass="btn btn-teal" Text="<%$ Resources:LocalizedResource, Cancel %>" runat="server" CausesValidation="False"
                                                        CommandName="Cancel"></asp:Button>
                                                </div>
                                            </div>
                                        </div>
                                    </FormTemplate>
                                </EditFormSettings>
                                <Columns>
                                    <rad:GridEditCommandColumn>
                                    </rad:GridEditCommandColumn>
                                    <rad:GridBoundColumn UniqueName="NewsTitle" SortExpression="NewsTitle" DataField="NewsTitle"
                                        ColumnEditorID="txtNewsTitleaa" HeaderText="<%$ Resources:LocalizedResource, Title %>" HeaderStyle-Font-Bold="true">
                                    </rad:GridBoundColumn>
                                    <rad:GridBoundColumn UniqueName="category" SortExpression="category" DataField="category"
                                        ColumnEditorID="txtCategoryaaa" HeaderText="<%$ Resources:LocalizedResource, Category %>" HeaderStyle-Font-Bold="true">
                                    </rad:GridBoundColumn>
                                    <rad:GridBoundColumn UniqueName="NewsShortDesc" SortExpression="NewsShortDesc" DataField="NewsShortDesc"
                                        ColumnEditorID="txtNewsShortDesc" HeaderText="<%$ Resources:LocalizedResource, Preview %>" HeaderStyle-Font-Bold="true"
                                        Visible="false">
                                    </rad:GridBoundColumn>
                                    <rad:GridBoundColumn UniqueName="NewContent" SortExpression="NewsTitle" DataField="NewContent"
                                        ColumnEditorID="txtNewsContent" HeaderText="<%$ Resources:LocalizedResource, Content %>" HeaderStyle-Font-Bold="true"
                                        Visible="false">
                                    </rad:GridBoundColumn>
                                    <rad:GridBoundColumn UniqueName="Inactive" SortExpression="category" DataField="Inactive"
                                        ColumnEditorID="cbInactive" HeaderText="<%$ Resources:LocalizedResource, Inactive %>" HeaderStyle-Font-Bold="true"
                                        Visible="false">
                                    </rad:GridBoundColumn>
                                    <rad:GridDateTimeColumn UniqueName="CreatedOn" HeaderText="<%$ Resources:LocalizedResource, PublishedOn %>" DataField="PublishedOn"
                                        DataType="System.DateTime" DataFormatString="{0:MMM dd, yyyy HH:mm tt}" HeaderStyle-Font-Bold="true">
                                    </rad:GridDateTimeColumn>
                                    <rad:GridDateTimeColumn UniqueName="UpdatedOn" HeaderText="<%$ Resources:LocalizedResource, LastUpdated %>" DataField="UpdatedOn"
                                        DataType="System.DateTime" DataFormatString="{0:MMM dd, yyyy HH:mm tt}" HeaderStyle-Font-Bold="true">
                                    </rad:GridDateTimeColumn>
                                </Columns>
                            </MasterTableView>
                        </rad:RadGrid>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
