﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Collections;


public partial class NewsUserCtrl : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Telerik.Web.UI.RadSkinManager skinManager = new Telerik.Web.UI.RadSkinManager();
        skinManager.Skin = "Bootstrap";
    }

    void LoadNewsArticles()
    {
        radNews.DataSource = DataHelper.GetAllArticles();
        radNews.DataBind();
    }

    protected void radNews_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        if (e.RebindReason == Telerik.Web.UI.GridRebindReason.ExplicitRebind || e.RebindReason == Telerik.Web.UI.GridRebindReason.PostBackEvent)
        {
            try
            {
                radNews.DataSource = DataHelper.GetAllArticles();
            }
            catch (Exception ex)
            {
                radNews.Controls.Add(new LiteralControl(string.Format("<strong style='color: red'>Error fetching records: {0}</strong>", ex.Message)));
            }
        }
    }

    protected void radNews_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        if (e.Item is GridEditFormItem && e.Item.IsInEditMode)
        {
            GridEditableItem editItem = e.Item as GridEditableItem;
            //GridEditManager editMgr = editItem.EditManager;   

            RadComboBox cboST = (RadComboBox)editItem.FindControl("cboST");
            //DropDownList cboGraduated = (DropDownList)editItem.FindControl("DDGraduated");
            cboST.DataSource = DataHelper.GetNewsCategories();


            Label lblName = editItem.FindControl("CategoryID") as Label;
            cboST.SelectedValue = lblName.Text;
            cboST.DataBind();

            CheckBox cBox = editItem.FindControl("chkInactive") as CheckBox;
            cBox.InputAttributes["class"] = "js-switch";

        }
    }

    protected void radNews_ItemCreated(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        if (e.Item is GridEditableItem && e.Item.IsInEditMode)
        {
            if (!e.Item.OwnerTableView.IsItemInserted)
            {
                Button updateButton = (Button)e.Item.FindControl("UpdateButton");
                updateButton.Text = "Update";
            }
            else
            {
                Button insertButton = (Button)e.Item.FindControl("PerformInsertButton");
                insertButton.Text = "Save";
            }
            Button cancelButton = (Button)e.Item.FindControl("CancelButton");
            cancelButton.Text = "Cancel";
        }
    }

    protected void radNews_UpdateCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (e.Item is GridEditableItem && e.Item.IsInEditMode)
        {

            GridEditableItem editableItem = ((GridEditableItem)e.Item);
            //GridEditManager editMan = editableItem.EditManager;
            int NewsID = Convert.ToInt32(editableItem.GetDataKeyValue("id"));
            Hashtable newValues = new Hashtable();

            try
            {

                bool xx = ((CheckBox)editableItem.FindControl("chkInactive")).Checked ? true : false;
                DataHelper.UpdateNews(
                    NewsID
                    , ((TextBox)editableItem.FindControl("TxtNewsTitle")).Text
                    , ((RadEditor)editableItem.FindControl("EditorContent")).Content
                    , xx
                    , ""
                    , Convert.ToInt32(((RadComboBox)editableItem.FindControl("cboST")).SelectedValue)
                );

                radNews.Controls.Add(new LiteralControl("Record has been updated"));
            }
            catch (Exception ex)
            {
                radNews.Controls.Add(new LiteralControl(string.Format("Error updating Record: {0}", ex.Message)));
            }
        }
    }

    protected void radNews_ItemUpdated(object sender, GridUpdatedEventArgs e)
    {
        GridEditableItem editableItem = ((GridEditableItem)e.Item);
        Hashtable newValues = new Hashtable();
        e.Item.OwnerTableView.ExtractValuesFromItem(newValues, editableItem);

        //int parCode = Convert.ToInt32(editableItem.FindControl("ddtDisposition"));
        try
        {
            int NewsID = Convert.ToInt32(((GridDataItem)e.Item).GetDataKeyValue("id"));

            bool xx = ((CheckBox)editableItem["Inactive"].FindControl("chkInactive")).Checked ? true : false;
                
            DataHelper.UpdateNews(
                NewsID
                , ((TextBox)editableItem["NewsTitle"].FindControl("TxtNewsTitle")).Text
                , ((RadEditor)editableItem["NewContent"].FindControl("EditorContent")).Content, 
                xx,
                "", Convert.ToInt32(((RadComboBox)editableItem.FindControl("cboST")).SelectedValue));

            radNews.Controls.Add(new LiteralControl("Record has been updated"));

        }
        catch (Exception ex)
        {
            radNews.Controls.Add(new LiteralControl(string.Format("Error updating Record: {0}", ex.Message)));
        }
    }

    protected void radNews_InsertCommand(object sender, GridCommandEventArgs e)
    {
        GridEditableItem editableItem = ((GridEditableItem)e.Item);
        Hashtable newValues = new Hashtable();
        e.Item.OwnerTableView.ExtractValuesFromItem(newValues, editableItem);

        //int parCode = Convert.ToInt32(editableItem.FindControl("ddtDisposition"));
        try
        {
            //bool xx = ((CheckBox)editableItem.FindControl("chkInactive")).Checked ? true : false;
            DataHelper.InsertNews(
                ((TextBox)editableItem.FindControl("TxtNewsTitle")).Text
                , ((RadEditor)editableItem.FindControl("EditorContent")).Text
                , 1
                , ""
                , Convert.ToInt32(((RadComboBox)editableItem.FindControl("cboST")).SelectedValue)
            );

            radNews.Controls.Add(new LiteralControl("Record has been inserted"));
        }
        catch (Exception ex)
        {
            radNews.Controls.Add(new LiteralControl("Error: " + ex.Message));
        }
    }

    protected void radNews_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == RadGrid.InitInsertCommandName) //"Add new" button clicked
        {
            GridEditCommandColumn editColumn = (GridEditCommandColumn)radNews.MasterTableView.GetColumn("EditCommandColumn");
            editColumn.Visible = false;
        }
        else if (e.CommandName == RadGrid.RebindGridCommandName && e.Item.OwnerTableView.IsItemInserted)
        {
            e.Canceled = true;
        }
        else
        {
            GridEditCommandColumn editColumn = (GridEditCommandColumn)radNews.MasterTableView.GetColumn("EditCommandColumn");
            if (!editColumn.Visible)
                editColumn.Visible = true;
        }
    }

    protected void radNews_PreRender(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            radNews.Rebind();
        }
    }
}