﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OverdueCoursesUserCtrl.ascx.cs" Inherits="UserControl_Pages_OverdueCoursesUserCtrl" %>

<rad:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel2" runat="server" Transparency="25" IsSticky="true" CssClass="Loading myLoading" />
<rad:RadWindowManager ID="rwmOverdueResult" RenderMode="Lightweight" EnableShadow="true"
    VisibleOnPageLoad="false" Behaviors="Close, Move" DestroyOnClose="true" Modal="true"
    Opacity="99" runat="server" VisibleStatusbar="false" Skin="Bootstrap" Width="620px"
    Height="400px">
</rad:RadWindowManager>
<rad:RadCodeBlock ID="rcbOverdueCourses" runat="server" Visible="false">
    <script type="text/javascript">
        //listview variables
        var overdueListView;

        var overdueGridView;
        var arrOverdue;

        var virtualCount;
        var pageSize;
        var pageIndex;
        var defaultPageSize = 12;
        var oldPageSize;
        var isViewAll = false;

        function pageLoad() {
            initializeOverdueCourse();
        }

        //--Start overdueResult Binding

        //initialize pnlOverdueResultContainer Course
        function initializeOverdueCourse() {
            $ = $telerik.$;
            //this will fire the OnCommand event
            overdueListView = $find("<%= lvOverdueResult.ClientID%>");
            overdueListView.rebind();
            pageSize = defaultPageSize;
            overdueListView.set_pageSize(pageSize);
            overdueListView.page(0);

            overdueGridView = $find("<%= gridOverdueResult.ClientID%>");

            $(".mcPager .pagePrev").click(function (e) {
                overdueListView.page(overdueListView.get_currentPageIndex() - 1);
            });

            $(".mcPager .pageNext").click(function (e) {
                overdueListView.page(overdueListView.get_currentPageIndex() + 1);
            });

            $("#overdueListView").on("click", ".mc-item", function (e) {
                overdueListView.toggleSelection($(this).index());
            });
        }

        function bindOverdueCourseList() {
            overdueListView = $find("<%= lvOverdueResult.ClientID %>");

            var startRowIndex = overdueListView.get_currentPageIndex() * overdueListView.get_pageSize(),
                maximumRows = overdueListView.get_pageSize(),
                sortExpression = overdueListView.get_sortExpressions().toLinq();

            //make a call to get the data
            PageMethods.GetOverdueCoursesData(startRowIndex, maximumRows, sortExpression,
                function (result) {
                    var count = result.count;
                    var data = result.data;
                        console.log("hey");
                    
                    overdueListView.set_virtualItemCount(count);
                    if (count > 0) {
                        $(".mcPager .pagePrev").removeClass("display-none");
                        $(".mcPager .pageNext").removeClass("display-none");
                        $(".mcViews").removeClass("display-none");
                        $(".mcPager").removeClass("display-none");
                        $(".switchView").removeClass("display-none");
                        $(".ddlPageSize-container").removeClass("display-none");
                    }

                    pageSize = overdueListView.get_pageSize();
                    virtualCount = overdueListView.get_virtualItemCount();
                    overdueListView.set_dataSource(data);

                    console.log(virtualCount);
                    $(".js-mc-loader").hide();
                    overdueListView.dataBind();

                    overdueGridView.set_dataSource(data);
                    overdueGridView.dataBind();

                    if (isViewAll == true) {
                        $("#btnViewAllListEC").addClass("display-none");
                        $("#btnBackListEC").removeClass("display-none");

                        $("#btnViewAllGridTLC").addClass("display-none");
                        $("#btnBackGridTLC").removeClass("display-none");

                    }
                     else {
                        $("#btnViewAllListEC").removeClass("display-none");
                        $("#btnBackListEC").addClass("display-none");

                        $("#btnViewAllGridTLC").removeClass("display-none");
                        $("#btnBackGridTLC").addClass("display-none");
                    }

                    $(".myLoading").hide();
                    pageNumberGenerator();
                    console.log("just rebind");
                },
                function (error) {
                    var code = error._statusCode;
                    if (code == 401)
                        location.reload();
                    else if (code == 500)
                        radalert("Something went while submitting your report. Please try again.", 330, 180, "Error Message", "");
                });
        }

        //Client Events overdueCourses

        function ocOnListViewDataBinding(sender, args) {
            sender.set_selectedIndexes([]);
        }

        function ocOnListViewCommand(sender, args) {
            args.set_cancel(true);
            bindOverdueCourseList();
        }

        function btnSwitchView_Click() {
            if ($('#<%= pnllvOverdueResult.ClientID %>').hasClass("display-none")) {

                $("#<%= pnllvOverdueResult.ClientID %>").removeClass("display-none");
                $("#<%= pnlgridOverdueResult.ClientID %>").addClass("display-none");
                $("#<%= btnSwitchToGridView.ClientID %>").addClass("display-none");
                $("#<%= btnSwitchToListView.ClientID %>").removeClass("display-none");
                //switch to gridview

                console.log("to list");
            }
            else {
                //switch to list view

                $("#<%= pnllvOverdueResult.ClientID %>").addClass("display-none");
                $("#<%= pnlgridOverdueResult.ClientID %>").removeClass("display-none");
                $("#<%= btnSwitchToListView.ClientID %>").addClass("display-none");
                $("#<%= btnSwitchToGridView.ClientID %>").removeClass("display-none");
                console.log("to grid");
                var stored = sessionStorage.getItem('pnlOverdueResultContainerResult');
                console.log("tolist" + stored);

                overdueGridView.set_dataSource(arrOverdue);
                overdueGridView.dataBind();
            }

            return false;
        }

        function inGridView() {
            if ($('#<%= pnllvOverdueResult.ClientID %>').hasClass("display-none"))
                return false;
            else
                return true;
        }

        function sortpnlOverdueResultContainer() {
            overdueListView.get_sortExpressions().clear();
            overdueListView.get_sortExpressions().add('CourseTitle', "DESC");
            overdueListView.rebind();
        }

        function generatePageNumbers() {
            var maxPage = Math.ceil(virtualCount / pageSize);
            var pageVirtualCount = virtualCount;
            var pages = $(".js-page-numbers ul").html('');

            for (var i = 1; i <= maxPage; i++) {
                var pages = $(".js-page-numbers ul").html();
                var rp = i - 1;
                $(".js-page-numbers ul").html(pages + '<li class="page-number"> <a href="javascript:void(0);" onclick="gotoPage(' + rp + ');">' + i + '</a></li>');
            }
        }

        function gotoPage(pageNum) {
            $(".js-page-numbers ul li").removeClass("selected-page");
            overdueListView.page(pageNum);
            overdueGridView.page(pageNum);
        }

        function setPage() {
            pageIndex = overdueListView.get_currentPageIndex();
            var myPage = pageIndex + 1;
            $(".js-page-numbers ul li").removeClass("selected-page");
            $(".js-page-numbers ul li:nth-child(" + myPage + ")").addClass("selected-page");
            var itemCount = pageSize * (pageIndex + 1) <= virtualCount ? (pageSize * (pageIndex + 1)) : ((pageIndex * pageSize) + (virtualCount % pageSize));
            $(".lblShowNumber").html("Showing " + itemCount + " of " + virtualCount + " Overdue Courses");
        }

        function setDdlPagerValue(selectedPageSize) {
            if (selectedPageSize >= virtualCount) {
                $("#ddlpageSize").val(virtualCount);
            } else {
                $("#ddlpageSize").val(selectedPageSize);
            }
        }

        function changePageSize(pageSizer) {
            showLoading();
            isViewAll = false;
            var pageSizerID = pageSizer.id;
            pageSize = $("#" + pageSizerID).val();

            if (!$.isNumeric(pageSize)) {
                pageSize = defaultPageSize;
            }

            rebindPageSize(pageSize)
        }

        function pageNumberGenerator() {
            generatePageNumbers(); //generate page numbers depending on virtual count and pagesize
            setPage(); //set the numbers displayed in "n of n Take Later Courses"
        }

        function btnViewAllListEC_Click() {
            showLoading();
            console.log(virtualCount);
            isViewAll = true;
            oldPageSize = overdueListView.get_pageSize();
            rebindPageSize(virtualCount);

            return false;
        }

        function btnBackListEC_Click() {
            showLoading();
            isViewAll = false;
            rebindPageSize(oldPageSize);

            return false;
        }

        function rebindPageSize(ps) {
            overdueListView.set_pageSize(ps);
            overdueListView.page(0);
            overdueListView.rebind();

            overdueGridView.set_pageSize(ps);
            overdueGridView.page(0);
        }

        function showLoading() {
            var loading = $find("<%= RadAjaxLoadingPanel2.ClientID %>");
            loading.show("<%= pnllvOverdueResult.ClientID %>");
        }

        function setStatusFontColor() {
           $('.statusContainer').each(function (i, obj) {
               var status = jQuery('[id=hidStatus]', this).val();

               if (status == "Pending Approval")
                   jQuery('[id=status]', this).css({ "color": "#0000CC" });

               if (status == "Approved")
                   jQuery('[id=status]', this).css({ "color": "#d8c91e" });
               else if (status == "Declined" || status == "Cancelled (By Learner)" || status == "Overdue" || status == "Cancelled (By Assigner)")
                   jQuery('[id=status]', this).css({ "color": "#E51C23" });
               else if (status == "Completed")
                   jQuery('[id=status]', this).css({ "color": "#259B24" });
               else if (status == "In Progress")
                   jQuery('[id=status]', this).css({ "color": "#C9AE1C" });
               else
                   jQuery('[id=status]', this).css({ "color": "#000" });
           });
       }
    </script>
</rad:RadCodeBlock>
<asp:Panel ID="pnlOverdueResultContainer" runat="server" CssClass="enrolled-container search-container" Style="width: 100%; min-height: calc(100vh - 200px);">
    <div class="row">
        <div class="switchView display-none pull-left col-md-6 " style="line-height: 34px; left: 5%; padding-top: 0.5%;">
            <asp:LinkButton ID="btnSwitchToListView" runat="server" OnClientClick="btnSwitchView_Click(); return false;"
                Font-Underline="false" aria-hidden="true" ForeColor="Black" ToolTip="Switch to List View">
                <i id="switchClassListView" runat="server" class="fa fa-th-list "></i>
            </asp:LinkButton>
            <asp:LinkButton ID="btnSwitchToGridView" runat="server" OnClientClick="btnSwitchView_Click(); return false;"
                Font-Underline="false" aria-hidden="true" CssClass="display-none" ToolTip="Switch to Grid View">
                <i id="switchClassGridView" runat="server" class="fa fa-th "></i>
            </asp:LinkButton>
        </div>
        <div class="ddlPageSize-container display-none col-md-6" style="left: -5%; padding-top: 1%;">
            <div class="display-inline-flex pull-right">
                <div class="col-md-2">
                    <p style="line-height: 34px;">
                        Show
                    </p>
                </div>
                <div class="col-md-4">
                    <asp:DropDownList ID="ddlpageSize" runat="server" CssClass="form-control rounded-corner"
                        onchange="changePageSize(this); return false;" ClientIDMode="Static">
                    </asp:DropDownList>
                </div>
                <div class="col-md-6 no-padding">
                    <p style="line-height: 34px;">
                        item/s per page
                    </p>
                </div>
            </div>
        </div>
    </div>
    <asp:Panel ID="pnllvOverdueResult" runat="server" CssClass="enrolled-view">
        <div id="ec-containerListView" class="mc-container col-md-11" style="left: 3.4%">
            <div class="mcPager container display-none">
                <p class="lblShowNumber" class="pull-left" style="color: Teal;">
                </p>
                <div class="row-fluid mcViews">
                    <div class="pull-right">
                        <button id="btnViewAllListEC" onclick="btnViewAllListEC_Click(); return false;" class="btn-transparent"
                            style="line-height: 32px; color: Blue; text-decoration: underline;" title="View All">
                            View All
                        </button>
                        <button id="btnBackListEC" onclick="btnBackListEC_Click(); return false;" class="display-none btn-transparent"
                            style="line-height: 32px;" title="Back">
                            <i class="fa fa-long-arrow-left "></i>
                        </button>
                    </div>
                </div>
            </div>
            <div class="loader js-mc-loader">
            </div>
            <rad:RadListView ID="lvOverdueResult" runat="server" AllowPaging="true" PageSize="8"
                AllowMultiFieldSorting="true">
                <LayoutTemplate>
                    <div id="overdueListView" style="min-height: 950px;">
                        <div id="ec-items" class="display-flow">
                        </div>
                        <div class="mcPager container display-none">
                            <div class="js-page-numbers display-inline-flex takeLaterCourse-page" style="padding-top: 5%;">
                                <a class="pagePrev black-arrow fa fa-caret-left no-underline-hover" href="javascript:void(0);"
                                    title="Go to previous page"></a>
                                <ul class="nostyle display-inline-flex black-font">
                                </ul>
                                <a class="pageNext black-arrow fa fa-caret-right no-underline-hover" href="javascript:void(0);"
                                    title="Go to next page"></a>
                            </div>
                        </div>
                    </div>
                </LayoutTemplate>
                <ClientSettings>
                    <DataBinding ItemPlaceHolderID="ec-items">
                        <ItemTemplate>
                            <div class="col-md-3">
                                <a id="hllvOverdueResult" onclick='confirmLaunchCourse("#= EncryptedCourseID #");' style="color:Black;" title=" #= CourseTitle #">
                                    <div id="divOverdueResult" runat="server" class="mc-item lv rlvI">
                                        <div class="photo-container">
                                            <image src='Media/Uploads/CourseImg/#= CourseID #/#= CourseImage #' onerror="this.src='Media/Uploads/CourseImg/No_image.jpg'" style="height:130px; width:190px; filter: grayscale(80%) brightness(.8);">
                                            <div class="courseStatus">
                                                Overdue
                                            </div>
                                            <div class="transparent_btn">
                                                View
                                            </div>
                                            </image>
                                        </div>
                                        <div class="category font-bold  text-trim">
                                            #= CourseTitle #
                                        </div>
                                            <div class="category">
                                            #= CourseType #
                                        </div>
                                        <div class="category">
                                            #= CourseDuration #
                                        </div>
                                        <div class="category">
                                            #= CourseCategory #
                                        </div>
                                        <div class="category">
                                            #= CourseSubcategory #
                                        </div>
                                        <div class="clearfix">
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </ItemTemplate>
                        <EmptyDataTemplate>
                        <p>
                            &nbsp;&nbsp;&nbsp;No result found.
                        </p>
                        </EmptyDataTemplate>
                        <DataService EnableCaching="true" />
                    </DataBinding>
                    <ClientEvents OnCommand="ocOnListViewCommand" OnDataBinding="ocOnListViewDataBinding">
                    </ClientEvents>
                </ClientSettings>
            </rad:RadListView>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlgridOverdueResult" runat="server" CssClass="display-none enrolled-view">
        <div id="ec-containerGridView" class="mc-container col-md-11" style="left: 3.4%">
            <div class="loader js-mc-loader">
            </div>
            <div class="mcPager container display-none">
                <p class="lblShowNumber" class="pull-left" style="color: Teal;">
                </p>
                <div class="row-fluid mcViews">
                    <div class="pull-right">
                        <button id="btnViewAllGridEC" onclick="btnViewAllListEC_Click(); return false;" class="btn-transparent"
                            style="line-height: 32px; color: Blue; text-decoration: underline;" title="View All">
                            View All
                        </button>
                        <button id="btnBackGridEC" onclick="btnBackListEC_Click(); return false;" class="display-none btn-transparent"
                            style="line-height: 32px;" title="Back">
                            <i class="fa fa-long-arrow-left "></i>
                        </button>
                    </div>
                </div>
            </div>
            <rad:RadListView ID="gridOverdueResult" runat="server" AllowPaging="true" PageSize="8">
                <LayoutTemplate>
                    <div id="overdueGridView">
                        <table class="gridMainTable table table-bordered table-striped course-grid">
                            <thead>
                                <tr class="rlvHeader">
                                    <th class="btn-teal">
                                        Course Title
                                    </th>
                                    <th class="btn-teal">
                                        Description
                                    </th>
                                    <th class="btn-teal">
                                        Course Type
                                    </th>
                                    <th class="btn-teal">
                                        Duration
                                    </th>
                                    <th class="btn-teal">
                                        Status
                                    </th>
                                    <th class="btn-teal">
                                        Course Path
                                    </th>
                                </tr>
                            </thead>
                            <tbody id="gridmcitem">
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                        <div class="mcPager container display-none">
                            <div class="js-page-numbers display-inline-flex takeLaterCourse-page">
                                <a class="pagePrev black-arrow fa fa-caret-left no-underline-hover" href="javascript:void(0);" title="Go to previous page">
                                </a>
                                <ul class="nostyle display-inline-flex black-font">
                                </ul>
                                <a class="pageNext black-arrow fa fa-caret-right no-underline-hover" href="javascript:void(0);" title="Go to next page">
                                </a>
                            </div>
                        </div>
                    </div>
                </LayoutTemplate>
                <ClientSettings>
                    <DataBinding ItemPlaceHolderID="gridmcitem">
                        <ItemTemplate>
                        <tr class="rlvI">
                            <td>
                                <a id="hlgridOverdueResult" onclick='confirmLaunchCourse("#= EncryptedCourseID #");' title=" #= CourseTitle #" class="hover-pointer"> #= CourseTitle #</a>
                            </td>
                            <td>
                                <asp:Label ID="Label1" runat="server"> #= CourseDescription #</asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="Label2" runat="server"> #= CourseType #</asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="Label3" runat="server"> #= CourseDuration #</asp:Label>
                            </td>
                            <td>
                                <div class="statusContainer">
                                    <p id="status" class="bold">#= Status #</p>
                                    <input type="hidden" id="hidStatus" value="#= Status #" class="display-none"/>
                                </div>
                                <script type="text/javascript">
                                    setStatusFontColor();
                                </script>
                            </td>
                            <td>
                                <asp:Label ID="Label5" runat="server"> #= CourseCategory #/ #= CourseSubcategory #</asp:Label>
                            </td>
                        </tr>
                        </ItemTemplate>
                        <EmptyDataTemplate>
                            <p>
                               &nbsp;&nbsp;&nbsp; No result found.
                            </p>
                        </EmptyDataTemplate>
                        <DataService EnableCaching="true" />
                    </DataBinding>
                </ClientSettings>
            </rad:RadListView>
        </div>
    </asp:Panel>
</asp:Panel>
