﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PlaylistEditSlideDetailsUserCtrl.ascx.cs" Inherits="PlaylistEditSlideDetailsUserCtrl" %>

<%@ Register TagPrefix="AddPlaylistEditUserCtrlV3" TagName="PlaylistEditUserCtrlV3" Src="~/UserControl/Pages/PlaylistEditUserCtrl.ascx" %>

<div class="col-sm-11 col-md-12 margin-bottom-10px">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12 thirty-px-padding">
                    <div class="col-md-12 no-paddings">
                        <ul class="breadcrumb">
                            <li><a href="#"><asp:Label runat="server" ID="label1" Text=" <%$ Resources:LocalizedResource, AdminDashboard%>" ></asp:Label></a></li>
                            <li><a href="#"><asp:Label runat="server" ID="label2" Text=" <%$ Resources:LocalizedResource, HomepageLayout%>" ></asp:Label></a></li>
                            <li><a href="PlaylistManager.aspx"><asp:Label runat="server" ID="label3" Text=" <%$ Resources:LocalizedResource, ImageSlider%>" ></asp:Label></a></li>
                            <li><a href="?tab=edit"><asp:Label ID="PlaylistName" runat="server" CssClass="font-bold" /></a></li>
                            <li>Edit Image Text</li>
                        </ul>
                    </div>
                    <div class="col-md-12 gray-border-no-pads ten-px-padding bg-color-light-gray">
                        <div class="row row-eq-height">
                            <div class="col-md-4">
                               <asp:Image ID="ImgSlide" runat="server" CssClass="img-responsive" />
                            </div>
                            <asp:HiddenField ID="SlideID" runat="server" />
                            <asp:HiddenField ID="SlideImg" runat="server" />
                            <div class="col-md-8">
                               <asp:TextBox ID="TxtDescription" runat="server" TextMode="MultiLine" CssClass="col-md-12" Height="100%" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                            </div>
                            <div class="col-md-8 ten-px-padding">
                                <asp:Button ID="BtnUpdate" runat="server" Text="Update" 
                                    CssClass="btn btn-teal text-center font-bold" onclick="BtnUpdate_Click" />

                                <asp:Button ID="BtnCancel" runat="server" Text="<%$ Resources:LocalizedResource, Cancel%>" CssClass="btn btn-teal text-center font-bold" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>