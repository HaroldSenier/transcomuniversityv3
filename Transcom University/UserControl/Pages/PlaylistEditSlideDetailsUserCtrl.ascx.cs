﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class PlaylistEditSlideDetailsUserCtrl : System.Web.UI.UserControl
{
    PlaylistEditUserCtrl playlistEditUserCtrl;

    //PlaylistUserCtrl playlistUserCtrl;
    PlaylistEditSlideDetailsUserCtrl playlistEditSlideDetailsUserCtrl;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            LoadSlide();

            //playlistUserCtrl = (PlaylistUserCtrl)this.Page.Master.FindControl("contentPlaceHolderMain").FindControl("PlaylistUserCtrl1");
            playlistEditUserCtrl = (PlaylistEditUserCtrl)this.Page.Master.FindControl("contentPlaceHolderMain").FindControl("PlaylistEditUserCtrl1");
            playlistEditSlideDetailsUserCtrl = (PlaylistEditSlideDetailsUserCtrl)this.Page.Master.FindControl("contentPlaceHolderMain").FindControl("PlaylistEditSlideDetailsUserCtrl1");

            //playlistUserCtrl.Visible = Request.QueryString["tab"] == null ? true : false;
            playlistEditUserCtrl.Visible = Request.QueryString["tab"] != null && Request.QueryString["tab"].ToString() == "edit" && Request.QueryString["PlaylistID"] != null ? true : false;
            playlistEditSlideDetailsUserCtrl.Visible = Request.QueryString["tab"] != null && Request.QueryString["tab"].ToString() == "edit-slide" ? true : false;

            if (Request.QueryString["tab"] == "delete-slide" && Request.QueryString["SlideID"] != null)
            {
                DoDelete(Convert.ToInt32(Request.QueryString["SlideID"]), Convert.ToInt32(Request.QueryString["PlaylistID"]));
            }
        }
    }

    void LoadSlide()
    {
        if (Request.QueryString["SlideID"] != null)
        {
            DataSet ds = DataHelper.GetSelectedSlide(Convert.ToInt32(this.Request.QueryString["SlideID"]));

            SlideID.Value = Request.QueryString["SlideID"];
            PlaylistName.Text = ds.Tables[0].Rows[0]["PlaylistName"].ToString();
            SlideImg.Value = ds.Tables[0].Rows[0]["PlaylistImg"].ToString();

            ImgSlide.ImageUrl = "~/Media/Uploads/Sliders/" + ds.Tables[0].Rows[0]["PlaylistID"].ToString() + "/" + ds.Tables[0].Rows[0]["PlaylistImg"].ToString(); ;
            TxtDescription.Text = ds.Tables[0].Rows[0]["PlaylistDesc"].ToString();
        }
    }

    protected void BtnUpdate_Click(object sender, EventArgs e)
    {
        int SlideIDx = Convert.ToInt32(SlideID.Value);
        DataHelper.UpdateSlide(SlideIDx, SlideImg.Value.ToString(), TxtDescription.Text);
    }

    void DoDelete(int a, int b)
    {
        DataHelper.DeleteSlide(a);
        Response.Redirect("~/PlaylistManager.aspx?tab=edit&PlaylistID=" + b.ToString());
    }
}