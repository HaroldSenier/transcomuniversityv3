﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PlaylistEditUserCtrl.ascx.cs" Inherits="PlaylistEditUserCtrl" %>

<%@ Register TagPrefix="AddPlaylistUserCtrlV3" TagName="PlaylistUserCtrlV3" Src="~/UserControl/Pages/PlaylistUserCtrl.ascx" %>

<div class="col-sm-11 col-md-12 margin-bottom-10px">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12 thirty-px-padding">
                    <div class="col-md-12 no-paddings">
                        <ul class="breadcrumb">
                            <li><a href="#">Admin Dashboard</a></li>
                            <li><a href="#">Homepage Layout</a></li>
                            <li><a href="#">Image Slider</a></li>
                            <li>Edit Playlist</li>
                        </ul>
                    </div>
                    <div class="col-md-12 gray-border-no-pads ten-px-padding bg-color-light-gray">
                        <span class="pull-right">
                            <a href="?tab=add-slide&PlaylistID=<% Response.Write(Request.QueryString["PlaylistID"]); %>">Add slide</a>
                        </span>
                        <h4><asp:Label ID="PlaylistName" runat="server" CssClass="font-bold" /></h4>
                        <div class="row">
                                <asp:Repeater ID="rptSlides" runat="server">
                                    <ItemTemplate>
                                        <div class="col-md-2 col-md-4 col-xs-6 thumb">
                                            <div style="position: absolute; bottom: 22px; right: 20px;">
                                                <a href="?tab=delete-slide&SlideID=<%#Eval("ID") %>&PlaylistID=<%#Eval("PlaylistID") %>" onclick="return confirm('Are you sure you want to delete this slide?');"><span class="fa fa-times" style="font-size: 20px; color: red; font-weight: bold;"></span></a>
                                            </div>
                                            <a class="thumbnail" href="?tab=edit-slide&SlideID=<%# Eval("ID")%>">
                                                <asp:Image ID="imgSlide" runat="server" ImageUrl='<%#"~/Media/Uploads/Sliders/"+ Eval("PlaylistID")+"/"+Eval("PlaylistImg") %>'
                                                    AlternateText='<%#Eval("PlaylistDesc") %>' CssClass="img-responsive" />
                                            </a>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <div class="col-md-12">
                                <asp:Label ID="LblMsg" runat="server" Visible="false" CssClass="col-md-12 alert alert-warning" />
                                </div>

                            
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-lg-6 col-sm-6 col-12">
                                        <h4>Select image file</h4>
                                        <div class="input-group">
                                            <label class="input-group-btn">
                                                <span class="btn btn-teal">
                                                    <asp:Label runat="server" ID="label1" Text=" <%$ Resources:LocalizedResource, Browse%>" ></asp:Label>&hellip; <asp:FileUpload runat="server" ID="ImgFileUpload" CssClass="file-upload-display-none" />
                                                </span>
                                            </label>
                                            <input type="text" class="form-control" readonly />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-sm-6 col-12">
                                        <h4>Image file description</h4>
                                        <div class="input-group">
                                            <asp:TextBox ID="TxtDescription" runat="server" TextMode="MultiLine" Width="100%" Height="30%" CssClass="col-md-12 form-input" />
                                        </div>
                                    </div>
                                </div>
                                <asp:HiddenField ID="HDPlaylistID" runat="server" />
                                <div class="row">
                                    <div class="col-lg-6 col-sm-6 col-12">
                                        <h4> </h4>
                                        <div class="input-group">
                                            <asp:Button ID="BtnUpload" runat="server" Text="<%$ Resources:LocalizedResource, Upload%>" 
                                                CssClass="btn btn-primary" onclick="BtnUpload_Click" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>