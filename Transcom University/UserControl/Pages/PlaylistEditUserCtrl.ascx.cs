﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;

public partial class PlaylistEditUserCtrl : System.Web.UI.UserControl
{
    PlaylistEditUserCtrl playlistEditUserCtrl;

    PlaylistUserCtrl playlistUserCtrl;
    //PlaylistEditSlideDetailsUserCtrl playlistEditSlideDetailsUserCtrl;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (Request.QueryString["PlaylistID"] != null)
            {
                DataSet ds = DataHelper.GetPlaylist(Convert.ToInt32(this.Request.QueryString["PlaylistID"]));

                PlaylistName.Text = ds.Tables[0].Rows[0]["PlaylistName"].ToString();

                LoadSlides();
                HDPlaylistID.Value = Request.QueryString["PlaylistID"];
            }

            playlistUserCtrl = (PlaylistUserCtrl)this.Page.Master.FindControl("contentPlaceHolderMain").FindControl("PlaylistUserCtrl1");
            playlistEditUserCtrl = (PlaylistEditUserCtrl)this.Page.Master.FindControl("contentPlaceHolderMain").FindControl("PlaylistEditUserCtrl1");
            //playlistEditSlideDetailsUserCtrl = (PlaylistEditSlideDetailsUserCtrl)this.Page.Master.FindControl("contentPlaceHolderMain").FindControl("PlaylistEditSlideDetailsUserCtrl1");

            playlistUserCtrl.Visible = Request.QueryString["tab"] == null ? true : false;
            playlistEditUserCtrl.Visible = Request.QueryString["tab"] != null && Request.QueryString["tab"].ToString() == "edit" && Request.QueryString["PlaylistID"] != null ? true : false;
            //playlistEditSlideDetailsUserCtrl.Visible = Request.QueryString["tab"] != null && Request.QueryString["tab"].ToString() == "edit-slide" ? true : false;
        }
    }

    void LoadSlides()
    {
        DataSet ds = DataHelper.GetSelectedPlaylist(Convert.ToInt32(Request.QueryString["PlaylistID"]));

        if (ds.Tables[0].Rows.Count > 0)
        {

            rptSlides.DataSource = DataHelper.GetSelectedPlaylist(Convert.ToInt32(Request.QueryString["PlaylistID"]));
            rptSlides.DataBind();
        }
        else
        {
            LblMsg.Visible = true;
            LblMsg.Text = "No slides for this playlist";
        }
    }

    protected void BtnUpload_Click(object sender, EventArgs e)
    {
        if (ImgFileUpload.HasFile)
        {
            try
            {
                string uploadFolder;

                if (HttpContext.Current.Request.Url.Host == "localhost")
                    uploadFolder = Request.PhysicalApplicationPath + "Media\\Uploads\\Sliders\\" + HDPlaylistID.Value.ToString() + "\\";
                else
                    uploadFolder = HttpRuntime.AppDomainAppPath + "Media\\Uploads\\Sliders\\" + HDPlaylistID.Value.ToString() + "\\";

                string directoryPath = Server.MapPath(string.Format("~/{0}/", "Media/Uploads/Sliders/" + Request.QueryString["PlaylistID"]));

                if (!Directory.Exists(directoryPath))
                {
                    Directory.CreateDirectory(directoryPath);
                    if (ImgFileUpload.PostedFile.ContentType == "image/jpeg")
                    {
                        if (ImgFileUpload.PostedFile.ContentLength < 1024000000)
                        {

                            Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds + Convert.ToInt32(Request.QueryString["PlaylistID"]);
                            string extension = Path.GetExtension(ImgFileUpload.PostedFile.FileName);
                            //string newfilename = String.Format("{0}_{1}", "My_Prefix_", ImgFileUpload.FileName);
                            //string filename = Path.GetFileName(ImgFileUpload.FileName);

                            ImgFileUpload.SaveAs(uploadFolder + unixTimestamp.ToString() + extension);

                            DataHelper.AddSlide(
                                Convert.ToInt32(HDPlaylistID.Value)
                                , unixTimestamp.ToString() + extension
                                , TxtDescription.Text
                                , 1);

                            //StatusLabel.Text = "Upload status: File uploaded!";
                        }
                        //else
                    }

                }
                else
                {
                    if (ImgFileUpload.PostedFile.ContentType == "image/jpeg")
                    {
                        if (ImgFileUpload.PostedFile.ContentLength < 1024000000)
                        {
                            Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds + Convert.ToInt32(Request.QueryString["PlaylistID"]);
                            string extension = Path.GetExtension(ImgFileUpload.PostedFile.FileName);
                            //string newfilename = String.Format("{0}_{1}", "My_Prefix_", ImgFileUpload.FileName);
                            //string filename = Path.GetFileName(ImgFileUpload.FileName);

                            ImgFileUpload.SaveAs(uploadFolder + unixTimestamp.ToString() + extension);

                            DataHelper.AddSlide(
                                Convert.ToInt32(HDPlaylistID.Value)
                                , unixTimestamp.ToString() + extension
                                , TxtDescription.Text
                                , 1);


                            //StatusLabel.Text = "Upload status: File uploaded!";
                        }
                        //else
                    }
                    //else
                    //    StatusLabel.Text = "Upload status: Only JPEG files are accepted!";
                }

                Response.Redirect("~/PlaylistManager.aspx?tab=edit&PlaylistID=" + HDPlaylistID.Value.ToString());

            }
            catch (Exception ex)
            {
                //StatusLabel.Text = "Upload status: The file could not be uploaded. The following error occured: " + ex.Message;
            }
        }
    }
}