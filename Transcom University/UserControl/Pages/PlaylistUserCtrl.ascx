﻿<%@ Control Language="C#" AutoEventWireup="True" CodeFile="PlaylistUserCtrl.ascx.cs" Inherits="PlaylistUserCtrl" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<div class="col-sm-11 col-md-12 margin-bottom-10px">
    <div id="myModal" class="modal fade modal-fullscreen">
    <div class="modal-dialog">
        <div class="modal-content">
                <div class="modal-body">
                    <p><asp:Label runat="server" ID="label1" Text=" <%$ Resources:LocalizedResource, Loading%>" ></asp:Label>...</p>
                </div>
                <div class="modal-footer text-center">
                    
                    <a href="" class="btn btn-teal font-bold"><asp:Label runat="server" ID="label2" Text=" <%$ Resources:LocalizedResource, Edit%>" ></asp:Label></a>
                    <button type="button" class="btn btn-teal font-bold" data-dismiss="modal"><asp:Label runat="server" ID="label3" Text=" <%$ Resources:LocalizedResource, Close%>" ></asp:Label></button>
                </div>
        </div>
    </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12 thirty-px-padding">
                    <div class="col-md-12 no-paddings">
                        <ul class="breadcrumb">
                            <li><a href="#"><asp:Label runat="server" ID="label4" Text=" <%$ Resources:LocalizedResource, AdminDashboard%>" ></asp:Label></a></li>
                            <li><a href="#"><asp:Label runat="server" ID="label5" Text=" <%$ Resources:LocalizedResource, HomepageLayout%>" ></asp:Label></a></li>
                            <li><a href="#"><asp:Label runat="server" ID="label6" Text=" <%$ Resources:LocalizedResource, ImageSlider%>" ></asp:Label></a></li>
                            <li><asp:Label runat="server" ID="label7" Text=" <%$ Resources:LocalizedResource, SelectPlaylist%>" ></asp:Label></li>
                        </ul>
                    </div>
                    <div class="col-md-12 gray-border-no-pads ten-px-padding bg-color-light-gray">
                        <div class="row">
                            <div class="col-md-12 col-md-8">
                                <div class="col-md-12">
                                    <h5 class="font-bold">
                                        <asp:Label runat="server" ID="label8" Text=" <%$ Resources:LocalizedResource, CurrentPlaylist%>" ></asp:Label>:</h5>
                                    <div class="row">
                                        <asp:Repeater ID="rptActivePlaylist" runat="server">
                                            <ItemTemplate>
                                                <div class="col-xs-9">
                                                    <%#Eval("PlaylistName") %>
                                                </div>
                                                <div class="col-xs-3 links-holder">
                                                     <a href="../../PlaylistLoader.aspx?PlaylistID=<%#Eval("ID") %>" data-toggle="modal" data-target="#myModal" data-remote="false"><span class="fa fa-eye" style="font-size: 20px;"></span></a> 
                                                     <a href="?tab=edit&PlaylistID=<%#Eval("ID") %>"><span class="fa fa-pencil-square-o" style="font-size: 20px;"></span></a>
                                                     <span class="fa fa-times" style="font-size: 20px;
                                                            color: red; font-weight: bold;"></span>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <h5 class="font-bold">
                                        <asp:Label runat="server" ID="label9" Text=" <%$ Resources:LocalizedResource, OtherPlaylists%>" ></asp:Label>:</h5>

                                    <asp:Repeater ID="rptInactivePlaylist" runat="server">
                                        <ItemTemplate>
                                             <div class="row" style="margin-top: 3px;">
                                                <div class="col-xs-3">
                                                    <a href="../../PlaylistLoader.aspx?PlaylistID=<%#Eval("ID") %>"  class="btn btn-sm btn-teal font-bold" data-toggle="modal" data-target="#myModal" data-remote="false">Load Playlist</a>
                                                </div>
                                                <div class="col-xs-6">
                                                     <%#Eval("PlaylistName") %>
                                                </div>
                                                <div class="col-xs-3 links-holder">
                                                    <a href="../../PlaylistLoader.aspx?PlaylistID=<%#Eval("ID") %>"  data-toggle="modal" data-target="#myModal" data-remote="false"><span class="fa fa-eye" style="font-size: 20px;"></span></a>
                                                    <a href="?tab=edit&PlaylistID=<%#Eval("ID") %>"><span class="fa fa-pencil-square-o" style="font-size: 20px;"></span></a>
                                                    
                                                    <%# ((bool)Eval("Inactive") == true) ? "<span class='fa fa-check' style='font-size: 20px; color: green; font-weight: bold;'></span>" : "" %>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>

                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label for="playlist_name"><asp:Label runat="server" ID="label10" Text=" <%$ Resources:LocalizedResource, PlaylistName%>" ></asp:Label></label>
                                    <asp:TextBox ID="TxtPlaylistName" runat="server" CssClass="form-control"></asp:TextBox>
                                
                                </div>
                                <div class="form-group">
                                    <label for="playlist_name"></label>
                                    <asp:Button ID="BtnPlaylist" runat="server" CssClass="btn btn-primary" 
                                        Text="Add Playlist" onclick="BtnPlaylist_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
