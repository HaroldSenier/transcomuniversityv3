﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class PlaylistUserCtrl : System.Web.UI.UserControl
{
    //PlaylistEditUserCtrl playlistEditUserCtrl;

    PlaylistUserCtrl playlistUserCtrl;

    //PlaylistEditSlideDetailsUserCtrl playlistEditSlideDetailsUserCtrl;

    //FeaturedCoursesUserCtrl featuredCoursesUserCtrl;

    //NewsUserCtrl newsUserCtrl;

    protected void Page_Load(object sender, EventArgs e)
    {
        // DataSet dsUserInfo = DataHelper.GetAllPlaylists();

        LoadActivePlaylists();
        LoadInactivePlaylists();

        playlistUserCtrl = (PlaylistUserCtrl)this.Page.Master.FindControl("contentPlaceHolderMain").FindControl("PlaylistUserCtrl1");
        //playlistEditUserCtrl = (PlaylistEditUserCtrl)this.Page.Master.FindControl("contentPlaceHolderMain").FindControl("PlaylistEditUserCtrl1");
        //playlistEditSlideDetailsUserCtrl = (PlaylistEditSlideDetailsUserCtrl)this.Page.Master.FindControl("contentPlaceHolderMain").FindControl("PlaylistEditSlideDetailsUserCtrl1");
        //featuredCoursesUserCtrl = (FeaturedCoursesUserCtrl)this.Page.Master.FindControl("contentPlaceHolderMain").FindControl("FeaturedCoursesUserCtrl1");
        //newsUserCtrl = (NewsUserCtrl)this.Page.Master.FindControl("contentPlaceHolderMain").FindControl("NewsUserCtrl1");

        //playlistUserCtrl.Visible = Request.QueryString["tab"] == null ? true : false;
        //playlistEditUserCtrl.Visible = Request.QueryString["tab"] != null && Request.QueryString["tab"].ToString() == "edit" && Request.QueryString["PlaylistID"] != null ? true : false;
        //playlistEditSlideDetailsUserCtrl.Visible = Request.QueryString["tab"] != null && Request.QueryString["tab"].ToString() == "edit-slide" ? true : false;

        //featuredCoursesUserCtrl.Visible = Request.QueryString["tab"] != null && Request.QueryString["tab"].ToString() == "featured-courses" ? true : false;
        //newsUserCtrl.Visible = Request.QueryString["tab"] != null && Request.QueryString["tab"].ToString() == "news" ? true : false;
    }

    void LoadInactivePlaylists()
    {
        rptInactivePlaylist.DataSource = DataHelper.GetAllPlaylists(true);
        rptInactivePlaylist.DataBind();
    }

    void LoadActivePlaylists()
    {
        rptActivePlaylist.DataSource = DataHelper.GetAllPlaylists(false);
        rptActivePlaylist.DataBind();
    }

    protected void BtnPlaylist_Click(object sender, EventArgs e)
    {
        DataHelper.AddPlaylist(1, TxtPlaylistName.Text);
        Response.Redirect("~/PlaylistManager.aspx");
    }
}