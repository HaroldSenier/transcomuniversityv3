﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Collections;

public partial class UserControl_Pages_RecentlyAddedCourseSettings : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            loadDdl();
            loadPreviousSettings();
        }

    }

    void loadPreviousSettings()
    {
        var settings = DataHelper.getRecentlyAddedCourseSettings();

        if (settings != null)
        {
            bool isCreatedWithin = settings.EnableCreatedWithin;
            int createdWithin = settings.CreatedWithin == 0 ? 1 : settings.CreatedWithin;
            int createdWithinUnit = settings.CreatedWithinUnit == 0 ? 1 : settings.CreatedWithinUnit;
            bool isUpdatedWithin = settings.EnableUpdatedWithin;
            int updatedWithin = settings.UpdatedWithin == 0 ? 1 : settings.UpdatedWithin;
            int updatedWithinUnit = settings.UpdatedWithinUnit == 0 ? 1 : settings.UpdatedWithinUnit;
            bool isGrid = settings.DefaultView;
            int displayCount = settings.DisplayCount == 0 ? 1 : settings.DisplayCount;
            bool isManual = settings.ManualSelection;
            bool isVisible = settings.Visible;
            bool notifyAlert = settings.NotifyAlert;
            bool notifyEmail = settings.NotifyEmail;

            cbCreatedWithin.Checked = isCreatedWithin;
            cbUpdatedWithin.Checked = isUpdatedWithin;
            ddlCreatedWithin.SelectedIndex = ddlCreatedWithin.FindItemByValue(createdWithin.ToString()).Index;
            ddlCreatedWithinUnit.SelectedIndex = createdWithinUnit;
            ddlUpdatedWithin.SelectedIndex = ddlUpdatedWithin.FindItemByValue(updatedWithin.ToString()).Index;
            ddlUpdatedWithinUnit.SelectedIndex = updatedWithinUnit;
            cbGridView.Checked = isGrid == true ? true : false;
            cbListView.Checked = isGrid == false ? true : false;
            ddlDisplayCount.SelectedIndex = ddlDisplayCount.FindItemByValue(displayCount.ToString()).Index;
            cbManualSelection.Checked = isManual;
            cbNotifyAlert.Checked = notifyAlert;
            cbNotifyEmail.Checked = notifyEmail;
            cbVisible.Checked = isVisible;
        }
    }

    void loadDdl()
    {
        for (int i = 1; i <= 20; i++)
        {
            DropDownListItem itemDisplay = new DropDownListItem(i.ToString(), i.ToString());
            DropDownListItem itemLibrary = new DropDownListItem(i.ToString(), i.ToString());
            DropDownListItem itemPager = new DropDownListItem(i.ToString(), i.ToString());
            ddlDisplayCount.Items.Add(itemDisplay);
            ddlCourseLibrary.Items.Add(itemLibrary);

            ddlCourseCatalogPager.Items.Add(itemPager);
        }

        for (int i = 1; i <= 60; i++)
        {
            DropDownListItem itemCreated = new DropDownListItem(i.ToString(), i.ToString());
            DropDownListItem itemUpdated = new DropDownListItem(i.ToString(), i.ToString());

            ddlCreatedWithin.Items.Add(itemCreated);
            ddlUpdatedWithin.Items.Add(itemUpdated);

        }

        ArrayList itemsList = new ArrayList();
        itemsList.Add("Seconds");
        itemsList.Add("Minutes");
        itemsList.Add("Hours");
        itemsList.Add("Days");
        itemsList.Add("Weeks");
        itemsList.Add("Months");
        itemsList.Add("Years");

        ddlCreatedWithinUnit.DataSource = itemsList;
        ddlCreatedWithinUnit.DataBind();

        ddlUpdatedWithinUnit.DataSource = itemsList;
        ddlUpdatedWithinUnit.DataBind();

    }
}