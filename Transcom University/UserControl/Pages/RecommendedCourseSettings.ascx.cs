﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Collections;

public partial class UserControl_Pages_RecommendedCourseSettings : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            loadDdl();
            loadPreviousSettings();
        }

    }

    void loadPreviousSettings()
    {
        var settings = DataHelper.getRecommendedCourseSettings();

        if (settings != null)
        {
            bool assigned = settings.AssignedToLearner;
            bool similarCourse = settings.SimilarCourse;
            bool metTenureYears = settings.MetTenureYears;
            bool metRoleYears = settings.MetRoleYears;
            bool basedOnProfile = settings.BasedOnProfile;
            bool viewedCourses = settings.EnableCourseViewed;
            int viewCount = settings.ViewCount == 0 ? 1 : settings.ViewCount;
            bool isGrid = settings.DefaultView;
            int displayCount = settings.DisplayCount == 0 ? 1 : settings.DisplayCount;
            bool isManual = settings.ManualSelection;
            bool isVisible = settings.Visible;
            bool notifyAlert = settings.NotifyAlert;
            bool notifyEmail = settings.NotifyEmail;

            cbAssigned.Checked = assigned;
            cbSimilar.Checked = similarCourse;
            cbReqTenureYears.Checked = metTenureYears;
            cbReqRoleYears.Checked = metRoleYears;
            cbLearnersProfile.Checked = basedOnProfile;
            cbViewed.Checked = viewedCourses;
            ddlViewCount.SelectedIndex = ddlDisplayCount.FindItemByValue(viewCount.ToString()).Index;
            cbGridView.Checked = isGrid == true ? true : false;
            cbListView.Checked = isGrid == false ? true : false;
            ddlDisplayCount.SelectedIndex = ddlDisplayCount.FindItemByValue(displayCount.ToString()).Index;
            cbManualSelection.Checked = isManual;
            cbNotifyAlert.Checked = notifyAlert;
            cbNotifyEmail.Checked = notifyEmail;
            cbVisible.Checked = isVisible;
        }
    }

    void loadDdl()
    {
        for (int i = 1; i <= 20; i++)
        {
            DropDownListItem itemView = new DropDownListItem(i.ToString(), i.ToString());
            DropDownListItem itemDisplay = new DropDownListItem(i.ToString(), i.ToString());
            DropDownListItem itemLibrary = new DropDownListItem(i.ToString(), i.ToString());
            DropDownListItem itemPager = new DropDownListItem(i.ToString(), i.ToString());
            ddlViewCount.Items.Add(itemView);
            ddlDisplayCount.Items.Add(itemDisplay);
            ddlCourseLibrary.Items.Add(itemLibrary);
            ddlCourseCatalogPager.Items.Add(itemPager);
        }
    }

}