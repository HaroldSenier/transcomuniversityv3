﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReportsGraphCourseRatingUserCtrl.ascx.cs"
    Inherits="UserControl_Pages_ReportsGraphCourseRatingUserCtrl" %>
<rad:RadCodeBlock ID="rcbRating" runat="server">
    <script type="text/javascript">
        var $ = $telerik.$;

        function pageLoad() {
            highlightSelectedSubmenu();
        }

        function commentSetRating(rating, obj) {
            $(obj).val(rating);

        }

        function exportRadHtmlChart() {
            var course = $find('<%= rcbCourse.ClientID %>');
            var id;
            try {
                id = course.get_selectedItem().get_value();
                if (id >= 0) {
                    var hdnConfirmExportPDF = $("#<%=hdnConfirmExportPDF.ClientID%>").val();
                    var confirmExport = confirm(hdnConfirmExportPDF);
                    if (confirmExport == true) {
                        //$("#pnlComments").addClass("display-none");
                        //$find('<%=RadClientExportManager1.ClientID%>').exportPDF($("#pnlRatingContainer"));
                        // $("#pnlComments").removeClass("display-none");
                        $find('<%=RadClientExportManager1.ClientID%>').exportPDF($(".chart-container"));
                    }
                }
            } catch (e) {
                var hdnAlertSelectCourse = $("#<%=hdnAlertSelectCourse.ClientID%>").val();
                alert(hdnAlertSelectCourse);
            }
        }

        function exportToExcel() {
            var course = $find('<%= rcbCourse.ClientID %>');
            var id;
            try {
                id = course.get_selectedItem().get_value();
                if (id >= 0) {
                    var hdnConfirmExportExcel = $("#<%=hdnConfirmExportExcel.ClientID%>").val();
                    var confirmExport = confirm(hdnConfirmExportExcel);
                    if (confirmExport == true) {
                        $("#btnToExcel").click();
                    }
                }
            } catch (e) {
                var hdnAlertSelectCourse = $("#<%=hdnAlertSelectCourse.ClientID%>").val();
                alert(hdnAlertSelectCourse);
            }
        }
    </script>
</rad:RadCodeBlock>
<rad:RadClientExportManager runat="server" ID="RadClientExportManager1">
    <PdfSettings MarginRight="2mm" MarginBottom="2mm" MarginTop="2mm" MarginLeft="2mm" />
</rad:RadClientExportManager>
<rad:RadAjaxManagerProxy ID="ramRatings" runat="server">
    <AjaxSettings>
        <rad:AjaxSetting AjaxControlID="rcbCourse">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="rcbCourse" />
                <%--<rad:AjaxUpdatedControl ControlID="rgRatings" LoadingPanelID="localLoadingPanel" />--%>
                <rad:AjaxUpdatedControl ControlID="pnlRatingContainer" LoadingPanelID="localLoadingPanel" />
                <rad:AjaxUpdatedControl ControlID="pnlRatingGridContainer" />
                <rad:AjaxUpdatedControl ControlID="rdtStartDateTime" />
                <rad:AjaxUpdatedControl ControlID="rdtEndDateTime" />
                <rad:AjaxUpdatedControl ControlID="RadClientExportManager1" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="rdtStartDateTime">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="rdtStartDateTime" />
                <rad:AjaxUpdatedControl ControlID="pnlRatingContainer" LoadingPanelID="localLoadingPanel" />
                <rad:AjaxUpdatedControl ControlID="pnlRatingGridContainer" />
                <rad:AjaxUpdatedControl ControlID="RadClientExportManager1" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="rdtEndDateTime">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="rdtEndDateTime" />
                <rad:AjaxUpdatedControl ControlID="pnlRatingContainer" LoadingPanelID="localLoadingPanel" />
                <rad:AjaxUpdatedControl ControlID="pnlRatingGridContainer" />
                <rad:AjaxUpdatedControl ControlID="RadClientExportManager1" />
            </UpdatedControls>
        </rad:AjaxSetting>
    </AjaxSettings>
</rad:RadAjaxManagerProxy>
<div class="container border-lightgray dashboard-main-container">
    <div class="col-md-12 header">
        <asp:Label ID="lblcourseTitle" Text="<%$ Resources:LocalizedResource, CourseRatings %>" runat="server" CssClass="pull-left" />
        <asp:Button ID="btnToExcel" runat="server" OnClick="btnToExcel_Click" CssClass="display-none"
            ClientIDMode="Static" />
        <asp:LinkButton ID="fakeBtnToExcel" runat="server" OnClientClick="exportToExcel(); return false;">
            <i class="fa fa-file-excel-o pull-right convert-to" aria-hidden="true"></i>
        </asp:LinkButton>
        <asp:LinkButton RenderMode="Lightweight" runat="server" OnClientClick="exportRadHtmlChart(); return false;"
            Text="Export RadHtmlChart to PDF" CssClass="pull-right">
            <i class="fa fa-file-pdf-o pull-right convert-to" aria-hidden="true"></i>
        </asp:LinkButton>
    </div>
    <div class="col-md-12 no-paddings">
        <rad:RadComboBox RenderMode="Lightweight" ID="rcbCourse" runat="server" Height="250px"
            Width="300px" EmptyMessage="Select Course" DataSourceID="dsCourse" DataTextField="Title"
            DataValueField="CourseID" ToolTip="Select Course" OnSelectedIndexChanged="rcbCourse_SelectedIndexChanged"
            AutoPostBack="true" CssClass="btn-teal btn-flat pull-left">
            <DefaultItem Text="Select Course" />
        </rad:RadComboBox>
        <asp:SqlDataSource ID="dsCourse" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection%>"
            SelectCommand="SELECT CourseID, Title FROM tbl_TranscomUniversity_Cor_Course where HideFromList = 0 AND StartDate <= GETDATE() AND EndDate >= GETDATE()">
        </asp:SqlDataSource>
        <rad:RadDatePicker ID="rdtEndDateTime" runat="server" ShowPopupOnFocus="true" RenderMode="Lightweight"
            Width="250px" MaxDate="12-31-9999" ToolTip="Set End Date" OnSelectedDateChanged="rdtDate_OnSelectedDateChanged"
            AutoPostBack="true" CssClass="pull-right" DateInput-ValidationGroup="vgDates"
            DateInput-CausesValidation="true" Enabled="false">
        </rad:RadDatePicker>
        <rad:RadDatePicker ID="rdtStartDateTime" runat="server" ShowPopupOnFocus="true" RenderMode="Lightweight"
            Width="250px" MaxDate="12-31-9999" ToolTip="Set Start Date" OnSelectedDateChanged="rdtDate_OnSelectedDateChanged"
            AutoPostBack="true" CssClass="pull-right" DateInput-ValidationGroup="vgDates"
            DateInput-CausesValidation="true" Enabled="false">
        </rad:RadDatePicker>
    </div>
    <div class="col-md-12 no-paddings">
        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="rdtEndDateTime"
            ValidationGroup="vgDates" ControlToCompare="rdtStartDateTime" Operator="GreaterThan"
            ErrorMessage="End Date should be greater than Start Date" Display="Dynamic" ForeColor="Red"
            CssClass="pull-right" EnableClientScript="true"></asp:CompareValidator>
    </div>
    <div class="col-md-12 no-paddings">
        <asp:Panel ID="pnlRatingContainer" runat="server" CssClass="container no-padding"
            ClientIDMode="Static">
            <div class="col-md-12 chart-container">
                <rad:RadHtmlChart runat="server" CssClass="width-fill RadHtmlChart" Height="300px"
                    Visible="false" ID="RadHtmlChart1" PlotArea-XAxis-MinorGridLines-Visible="false"
                    PlotArea-XAxis-MajorGridLines-Visible="false" PlotArea-YAxis-MinorGridLines-Visible="false"
                    PlotArea-YAxis-MajorGridLines-Visible="false" PlotArea-Appearance-FillStyle-BackgroundColor="#c6d9f1"
                    RenderMode="Lightweight" Skin="Bootstrap">
                    <PlotArea>
                        <Series>
                        </Series>
                        <XAxis DataLabelsField="Rating">
                            <TitleAppearance Text="5-Star Rating System" RotationAngle="-90">
                                <TextStyle Bold="true" />
                            </TitleAppearance>
                            <Items>
                                <rad:AxisItem LabelText="5" />
                                <rad:AxisItem LabelText="4" />
                                <rad:AxisItem LabelText="3" />
                                <rad:AxisItem LabelText="2" />
                                <rad:AxisItem LabelText="1" />
                            </Items>
                        </XAxis>
                        <YAxis MinValue="0">
                            <TitleAppearance Text="# of Raters">
                                <TextStyle Bold="true" />
                            </TitleAppearance>
                        </YAxis>
                    </PlotArea>
                    <Legend>
                        <Appearance Visible="false">
                        </Appearance>
                    </Legend>
                    <ChartTitle Text="<%$ Resources:LocalizedResource, CourseTitle %>">
                        <Appearance>
                            <TextStyle Color="Teal" Bold="true" />
                        </Appearance>
                    </ChartTitle>
                </rad:RadHtmlChart>
                <asp:Label ID="lblAsOf" Text="Ratings as of march 28, 2018 as 0830H" CssClass="lbl-chart-subtitle"
                    runat="server" Visible="false"></asp:Label>
                <asp:Panel ID="emptyRatings" runat="server" BackColor="#c6d9f1" CssClass="width-fill"
                    Height="300px">
                    <center>
                        <h5 class="relative-vr-left-center relative-vr-right-center  ">
                            <asp:Label runat="server" ID="Label1" Text="<%$ Resources:LocalizedResource, PleaseSelectaCourse %>"></asp:Label>
                        </h5>
                    </center>
                </asp:Panel>
            </div>
            <div class="col-md-3 no-padding">
                <asp:Panel ID="pnlRatingSummary" runat="server" Visible="false" BackColor="#e8e8e8">
                    <div class="row">
                        <div class="col-md-12 no-padding">
                            <span class="text-center">
                                <h4>
                                    <asp:Label ID="lblSummaryCourseTitle" runat="server" ForeColor="Teal">
                                    </asp:Label>
                                </h4>
                            </span>
                        </div>
                        <div class="col-md-12 no-padding">
                            <span class="text-center">
                                <h2>
                                    <asp:Label ID="lblCourseStatRating" runat="server" Font-Bold="true">
                                                        5.5
                                    </asp:Label>
                                </h2>
                            </span>
                        </div>
                        <div class="col-md-12 no-padding">
                            <span class="text-center">
                                <rad:RadRating RenderMode="Lightweight" ID="rrCourseStatRating" runat="server" ItemCount="5"
                                    Value="4.5" SelectionMode="Continuous" Precision="Exact" Orientation="Horizontal"
                                    Enabled="false" CssClass="width-100 relative-hr-center">
                                </rad:RadRating>
                            </span>
                        </div>
                        <div class="col-md-12">
                            <span class="text-center">
                                <h4>
                                    <i class="fa fa-users"></i>&nbsp;
                                    <asp:Label ID="lblUserCount" runat="server">999,999</asp:Label>
                                    <asp:Label runat="server" ID="Label2" Text="<%$ Resources:LocalizedResource, TotalUsers %>"></asp:Label></h4>
                            </span>
                        </div>
                    </div>
                </asp:Panel>
            </div>
            <div class="col-md-9 no-padding">
                <asp:Panel ID="pnlComments" runat="server" Visible="false" CssClass="comments-container no-padding"
                    ClientIDMode="Static">
                    <h4 class="title">
                        <asp:Label runat="server" ID="Label3" Text="<%$ Resources:LocalizedResource, Comments %>"></asp:Label>
                    </h4>
                    <rad:RadListView ID="rdComments" runat="server">
                        <ItemTemplate>
                            <div class="cr-comment-container container">
                                <div class="row">
                                    <div class="col-xs-1">
                                        <asp:Image ID="uImg" runat="server" ImageUrl='<%# Eval("UserImageUrl", "{0}") %>'
                                            onerror="this.src='default-user.png'" CssClass="border-teal"
                                            AlternateText="User Image" />
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="row">
                                            <div class="col-xs-12 display-inline-flex">
                                                <asp:Label ID="lblName" runat="server" CssClass="cr-name"><%#Eval("Name")%></asp:Label>
                                                <rad:RadRating RenderMode="Lightweight" ID="rrCommentRating" runat="server" ItemCount="5"
                                                    SelectionMode="Continuous" Precision="Item" Orientation="Horizontal" Enabled="false"
                                                    DbValue='<%# DataBinder.Eval(Container.DataItem, "Rating") %>' CssClass="width-100">
                                                </rad:RadRating>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <p>
                                                    <%#Eval("Comment")%></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <p class="pull-right">
                                            <i>Date:
                                                <%#Eval("DateModified")%>
                                            </i>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </rad:RadListView>
                </asp:Panel>
            </div>
        </asp:Panel>
    </div>
    <div class="col-md-12 no-paddings">
        <asp:Panel ID="pnlRatingGridContainer" runat="server" CssClass="display-none">
            <rad:RadGrid ID="rgRating" runat="server" AutoGenerateColumns="false" OnItemDataBound="rgRating_OnItemDataBound">
                <MasterTableView DataKeyNames="Rating" ShowFooter="true">
                    <Columns>
                        <rad:GridBoundColumn UniqueName="Rating" DataField="Rating" HeaderText="<%$ Resources:LocalizedResource, Rating %>">
                        </rad:GridBoundColumn>
                        <rad:GridBoundColumn UniqueName="Count" DataField="Count" HeaderText="<%$ Resources:LocalizedResource, UserCount %>">
                        </rad:GridBoundColumn>
                    </Columns>
                </MasterTableView>
            </rad:RadGrid>
        </asp:Panel>
    </div>
</div>
<asp:HiddenField runat="server" ID="hdnAlertSelectCourse" Value="<%$ Resources:LocalizedResource, Pleaseselectcoursefirst %>" />
<asp:HiddenField runat="server" ID="hdnConfirmExportExcel" Value="<%$ Resources:LocalizedResource, AreyousureyouwanttoexportthistoExcelFile %>" />
<asp:HiddenField runat="server" ID="hdnConfirmExportPDF" Value="<%$ Resources:LocalizedResource, AreyousureyouwanttoexportthistoPDFFile %>" />
