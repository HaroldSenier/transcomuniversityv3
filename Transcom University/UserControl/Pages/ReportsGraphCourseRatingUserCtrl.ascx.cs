﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Drawing;
using Telerik.Charting;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Data;
using TranscomUniversityV3Model;
using System.Drawing.Drawing2D;

public partial class UserControl_Pages_ReportsGraphCourseRatingUserCtrl : System.Web.UI.UserControl
{
    private int selectedCourse;
    private string asOfDate;
    private DateTime? startDate;
    private DateTime? endDate;
    private string fileName;
    private int totalCount;

    private Color[] barColors = new Color[5]{
        Color.Green,
        Color.LightGreen,
        Color.Orange,
        Color.Yellow,
        Color.Red
    };

    protected void Page_Load(object sender, EventArgs e)
    {
        rcbCourse.Filter = RadComboBoxFilter.StartsWith;

    }

    protected void rcbCourse_SelectedIndexChanged(object sender, EventArgs e)
    {
        rdtEndDateTime.Enabled = true;
        rdtStartDateTime.Enabled = true;
        rdtStartDateTime.SelectedDate = null;
        rdtEndDateTime.SelectedDate = null;

        selectedCourse = Convert.ToInt32(rcbCourse.SelectedValue);
        loadAsOfLabel();
        loadRating(selectedCourse);
       
    }

    protected void rdtDate_OnSelectedDateChanged(object sender, EventArgs e)
    {
        if (rdtEndDateTime.SelectedDate != null && Page.IsValid)
        {
            selectedCourse = Convert.ToInt32(rcbCourse.SelectedValue);

            //DateTime? date1 = rdtEndDateTime.SelectedDate > DataHelper.serverDate() ? DataHelper.serverDate() : rdtEndDateTime.SelectedDate;
            loadAsOfLabel();
            loadRating(selectedCourse);
        }

    }

    protected void loadRating(int courseID)
    {
        startDate = rdtStartDateTime.SelectedDate;
        endDate = rdtEndDateTime.SelectedDate;
        var db = new TranscomUniversityV3ModelContainer();

        RadHtmlChart1.Visible = true;
        emptyRatings.Visible = false;
        lblAsOf.Visible = true;
        pnlRatingSummary.Visible = true;
        pnlComments.Visible = true;
        //set start/end dates, rating as of date, title

        var courseDetails = db.tbl_TranscomUniversity_Cor_Course.Where(c => c.CourseID == courseID).SingleOrDefault();
        RadHtmlChart1.ChartTitle.Text = courseDetails.Title;
        lblSummaryCourseTitle.Text = courseDetails.Title;
        rdtStartDateTime.SelectedDate = startDate != null ? startDate : courseDetails.StartDate;
        rdtEndDateTime.SelectedDate = endDate != null ? endDate : courseDetails.Enddate;
        startDate = startDate != null ? startDate : courseDetails.StartDate;
        endDate = endDate != null ? endDate : DataHelper.serverDate();
        //set exports filename
        fileName = courseDetails.Title + " " + asOfDate;
        RadClientExportManager1.PdfSettings.FileName = fileName;
        rgRating.ExportSettings.FileName = fileName;

        //set chart ratings details
        RadHtmlChart1.PlotArea.Series.Clear();

        var ratings = db.pr_TranscomUniversity_CourseRating(courseID, startDate, endDate);
        var max =  db.pr_TranscomUniversity_CourseRating(courseID, startDate, endDate).OrderByDescending(c => c.Count).FirstOrDefault();
        decimal? maxRating = Convert.ToDecimal(max.Count);
        BarSeries bs = new BarSeries();
        int i = 0;
        foreach (var item in ratings)
        {
            CategorySeriesItem ci = new CategorySeriesItem();
            ci.Y = item.Count;
            ci.BackgroundColor = barColors[i++];
            bs.SeriesItems.Add(ci);
        }
        bs.Spacing = 1;
        bs.Gap = .3;
        RadHtmlChart1.PlotArea.YAxis.MaxValue = maxRating + ((maxRating * 5) / 100);
        bs.TooltipsAppearance.Color = Color.Teal;
        RadHtmlChart1.PlotArea.Series.Add(bs);

        //load other panels
        loadComments(selectedCourse);
        loadRatingSummary(selectedCourse);
        loadRatingGrid(selectedCourse);

    }

    protected void loadAsOfLabel()
    {
         DateTime? date1;
         if (rdtEndDateTime.SelectedDate != null)
             date1 = rdtEndDateTime.SelectedDate > DataHelper.serverDate() ? DataHelper.serverDate() : rdtEndDateTime.SelectedDate;
         else
             date1 = DataHelper.serverDate();

        asOfDate = "Ratings as of " + string.Format("{0:MMMM dd, yyyy}", date1) + " at " + string.Format("{0:hh:mm tt}", date1);
        lblAsOf.Text = asOfDate;
    }

    protected void loadComments(int courseID)
    {
        var db = new TranscomUniversityV3ModelContainer();
        var courseComments = db.pr_TranscomUniversity_CourseCommentsFilterDate(courseID, startDate, endDate).ToList();

        if (courseComments != null)
            rdComments.DataSource = courseComments;
        rdComments.DataBind();
    }

    protected void loadRatingSummary(int courseID)
    {
        var db = new TranscomUniversityV3ModelContainer();
        var courseStats = db.pr_TranscomUniversity_CourseReviewDetailsFilterDate(courseID, startDate, endDate).ToList();

        double userCount = 0.0;
        try
        {
            userCount = Convert.ToInt32(string.Format("{0:n0}", courseStats[0].Users));
            lblUserCount.Text = string.Format("{0:n0}", courseStats[0].Users);
            rrCourseStatRating.Value = courseStats[0].Rating;
            lblCourseStatRating.Text = string.Format("{0:0.0}", courseStats[0].Rating);
            totalCount = Convert.ToInt32(userCount);

        }
        catch (Exception)
        {
            lblUserCount.Text = string.Format("{0:n0}", userCount);
            rrCourseStatRating.Value = 0;
            lblCourseStatRating.Text = string.Format("{0:0.0}", 0);
        }

    }

    protected void loadRatingGrid(int courseID) 
    {
        var db = new TranscomUniversityV3ModelContainer();
        var ratings = db.pr_TranscomUniversity_CourseRating(courseID, startDate, endDate)
                    .AsEnumerable()
                    .Select(c => new{
                        c.Count,
                        Rating = Convert.ToInt32(c.Rating) + " Star",
                    });
        rgRating.DataSource = ratings;
        rgRating.DataBind();
    }

    protected void btnToExcel_Click(object sender, EventArgs e)
    {
       
        rgRating.ExportSettings.ExportOnlyData = true;
        rgRating.ExportSettings.OpenInNewWindow = true;
        rgRating.MasterTableView.ExportToExcel();
    }

    int total;
    protected void rgRating_OnItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            GridDataItem dataItem = e.Item as GridDataItem;
            int fieldValue = int.Parse(dataItem["Count"].Text);
            total += fieldValue;
        }
        if (e.Item is GridFooterItem)
        {
            GridFooterItem footerItem = e.Item as GridFooterItem;
            footerItem["Count"].Text = Resources.LocalizedResource.TotalUsers.ToString() + ": " + total.ToString();
            
        }
    }
}