﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SettingsCBUserCtrl.ascx.cs"
    Inherits="SettingsCBUserCtrl" %>
<asp:Panel ID="pnlMainSettingsUC" runat="server" ClientIDMode="Static">
    <rad:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <rad:AjaxSetting AjaxControlID="LinkDC">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="LinkDC" />
                    <rad:AjaxUpdatedControl ControlID="CourseBuilderDetails1" />
                    <rad:AjaxUpdatedControl ControlID="CourseBuilderDripContent1" />
                    <rad:AjaxUpdatedControl ControlID="CourseBuilderNotifs1" />
                    <rad:AjaxUpdatedControl ControlID="CourseBuilderEnrol1" />
                    <rad:AjaxUpdatedControl ControlID="CourseBuilderWidgets1" />
                    <rad:AjaxUpdatedControl ControlID="CourseBuilderBadges1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="LinkButton1">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="LinkButton1" />
                    <rad:AjaxUpdatedControl ControlID="CourseBuilderDetails1" />
                    <rad:AjaxUpdatedControl ControlID="CourseBuilderDripContent1" />
                    <rad:AjaxUpdatedControl ControlID="CourseBuilderNotifs1" />
                    <rad:AjaxUpdatedControl ControlID="CourseBuilderEnrol1" />
                    <rad:AjaxUpdatedControl ControlID="CourseBuilderWidgets1" />
                    <rad:AjaxUpdatedControl ControlID="CourseBuilderBadges1" />
                </UpdatedControls>
            </rad:AjaxSetting>
         <%--   <rad:AjaxSetting AjaxControlID="LinkNotif">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="LinkNotif" />
                    <rad:AjaxUpdatedControl ControlID="CourseBuilderDetails1" />
                    <rad:AjaxUpdatedControl ControlID="CourseBuilderDripContent1" />
                    <rad:AjaxUpdatedControl ControlID="CourseBuilderNotifs1" />
                    <rad:AjaxUpdatedControl ControlID="CourseBuilderEnrol1" />
                    <rad:AjaxUpdatedControl ControlID="CourseBuilderWidgets1" />
                    <rad:AjaxUpdatedControl ControlID="CourseBuilderBadges1" />
                </UpdatedControls>
            </rad:AjaxSetting>--%>
            <rad:AjaxSetting AjaxControlID="LinkEnrol">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="LinkEnrol" />
                    <rad:AjaxUpdatedControl ControlID="CourseBuilderDetails1" />
                    <rad:AjaxUpdatedControl ControlID="CourseBuilderDripContent1" />
                    <rad:AjaxUpdatedControl ControlID="CourseBuilderNotifs1" />
                    <rad:AjaxUpdatedControl ControlID="CourseBuilderEnrol1" />
                    <rad:AjaxUpdatedControl ControlID="CourseBuilderWidgets1" />
                    <rad:AjaxUpdatedControl ControlID="CourseBuilderBadges1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="LinkWidgets">   
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="LinkWidgets" />
                    <rad:AjaxUpdatedControl ControlID="CourseBuilderDetails1" />
                    <rad:AjaxUpdatedControl ControlID="CourseBuilderDripContent1" />
                    <rad:AjaxUpdatedControl ControlID="CourseBuilderNotifs1" />
                    <rad:AjaxUpdatedControl ControlID="CourseBuilderEnrol1" />
                    <rad:AjaxUpdatedControl ControlID="CourseBuilderWidgets1" />
                    <rad:AjaxUpdatedControl ControlID="CourseBuilderBadges1" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="LinkBadges">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="LinkBadges" />
                    <rad:AjaxUpdatedControl ControlID="CourseBuilderDetails1" />
                    <rad:AjaxUpdatedControl ControlID="CourseBuilderDripContent1" />
                    <rad:AjaxUpdatedControl ControlID="CourseBuilderNotifs1" />
                    <rad:AjaxUpdatedControl ControlID="CourseBuilderEnrol1" />
                    <rad:AjaxUpdatedControl ControlID="CourseBuilderWidgets1" />
                    <rad:AjaxUpdatedControl ControlID="CourseBuilderBadges1" />
                </UpdatedControls>
            </rad:AjaxSetting>
        </AjaxSettings>
    </rad:RadAjaxManagerProxy>
    <div class="col-md-12">
        <asp:Panel ID="PanelHolder" runat="server">
            <div class="CourseBuilder-Settings">
                <div class="row">
                    <div class="col-md-12">
                        <h5>
                            <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> <asp:Label runat="server" ID="Label1" Text="<%$ Resources:LocalizedResource, CourseDetails %>"></asp:Label></asp:LinkButton></h5>
                    </div>
                    <div class="col-md-12">
                        <h5 style="text-transform: capitalize;">
                            <asp:LinkButton ID="LinkDC" runat="server" OnClick="LinkDC_Click">
                    <i class="fa fa-arrow-circle-right" aria-hidden="true"></i> <asp:Label runat="server" ID="Label2" Text="<%$ Resources:LocalizedResource, DripContent %>"></asp:Label></asp:LinkButton></h5>
                    </div>
                   <%-- <div class="col-md-12">
                        <h5 style="text-transform: capitalize;">
                            <asp:LinkButton ID="LinkNotif" runat="server" OnClick="LinkNotif_Click">
                    <i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Notifications</asp:LinkButton></h5>
                    </div>--%>
                    <div class="col-md-12">
                        <h5 style="text-transform: capitalize;">
                            <asp:LinkButton ID="LinkEnrol" runat="server" OnClick="LinkEnrol_Click">
                    <i class="fa fa-arrow-circle-right" aria-hidden="true"></i> <asp:Label runat="server" ID="Label3" Text="<%$ Resources:LocalizedResource, Enrollment %>"></asp:Label></asp:LinkButton>
                        </h5>
                    </div>
                    <div class="col-md-12">
                        <h5 style="text-transform: capitalize;">
                            <asp:LinkButton ID="LinkWidgets" runat="server" OnClick="LinkWidgets_Click">
                    <i class="fa fa-arrow-circle-right" aria-hidden="true"></i> <asp:Label runat="server" ID="Label4" Text="<%$ Resources:LocalizedResource, Widgets %>"></asp:Label></asp:LinkButton></h5>
                    </div>
                    <div class="col-md-12">
                        <h5 style="text-transform: capitalize;">
                            <asp:LinkButton ID="LinkBadges" runat="server" OnClick="LinkBadges_Click">
                    <i class="fa fa-arrow-circle-right" aria-hidden="true"></i> <asp:Label runat="server" ID="Label5" Text="<%$ Resources:LocalizedResource, BadgesCertifications %>"></asp:Label></asp:LinkButton></h5>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>
</asp:Panel>
