﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class SettingsCBUserCtrl : System.Web.UI.UserControl
{
    Control x, courseDetails, dripContent, notifs, enrol, widgets, badges;

    HtmlGenericControl y;

    protected void Page_Load(object sender, EventArgs e)
    {

        x = (this.Parent.Parent).FindControl("sidebar_changer");
        courseDetails = (this.Parent.Parent).FindControl("CourseBuilderDetails1");
        dripContent = (this.Parent.Parent).FindControl("CourseBuilderDripContent1");
        notifs = (this.Parent.Parent).FindControl("CourseBuilderNotifs1");
        enrol = (this.Parent.Parent).FindControl("CourseBuilderEnrol1");
        widgets = (this.Parent.Parent).FindControl("CourseBuilderWidgets1");
        badges = (this.Parent.Parent).FindControl("CourseBuilderBadges1");

        y = (HtmlGenericControl)(this.Parent.Parent).FindControl("content_changer");

        if (Request.QueryString["tab"] != null && Request.QueryString["tab"].ToString() == "CourseDetails")
        {
            y.Attributes.Add("class", "col-md-9");
            courseDetails.Visible = true;
            x.Visible = true;
        }
    }


    protected void LinkDC_Click(object sender, EventArgs e)
    {
        if ((this.Parent.Parent).FindControl("CourseBuilderDripContent1").Visible == false)
        {
            (this.Parent.Parent).FindControl("CourseBuilderDripContent1").Visible = true;
        }

        courseDetails.Visible = false;
        notifs.Visible = false;
        enrol.Visible = false;
        widgets.Visible = false;
        badges.Visible = false;


    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        if ((this.Parent.Parent).FindControl("CourseBuilderDetails1").Visible == false)
        {
            (this.Parent.Parent).FindControl("CourseBuilderDetails1").Visible = true;
        }
        dripContent.Visible = false;
        notifs.Visible = false;
        enrol.Visible = false;
        widgets.Visible = false;
        badges.Visible = false;
    }
    protected void LinkNotif_Click(object sender, EventArgs e)
    {
        if ((this.Parent.Parent).FindControl("CourseBuilderNotifs1").Visible == false)
        {
            (this.Parent.Parent).FindControl("CourseBuilderNotifs1").Visible = true;
        }

        dripContent.Visible = false;
        courseDetails.Visible = false;
        enrol.Visible = false;
        widgets.Visible = false;
        badges.Visible = false;
    }

    protected void LinkEnrol_Click(object sender, EventArgs e)
    {
        courseDetails.Visible = false;
        dripContent.Visible = false;
        notifs.Visible = false;
        enrol.Visible = true;
        widgets.Visible = false;
        badges.Visible = false;
    }

    protected void LinkWidgets_Click(object sender, EventArgs e)
    {
        courseDetails.Visible = false;
        dripContent.Visible = false;
        notifs.Visible = false;
        enrol.Visible = false;
        widgets.Visible = true;
        badges.Visible = false;
    }

    protected void LinkBadges_Click(object sender, EventArgs e)
    {
        courseDetails.Visible = false;
        dripContent.Visible = false;
        notifs.Visible = false;
        enrol.Visible = false;
        widgets.Visible = false;
        Panel pnlSACBadge = (Panel) badges.FindControl("pnlSACBadge");
        Panel pnlBadgeImages = (Panel) badges.FindControl("pnlBadgeImages");
        Panel pnlBadgeUpload = (Panel) badges.FindControl("pnlBadgeUpload");
        Panel Panel =  (Panel) badges.FindControl("Panel");

        pnlSACBadge.Visible = false;
        pnlBadgeImages.Visible = false;
        pnlBadgeUpload.Visible = false;
        Panel.Visible = true;
        badges.Visible = true;
    }
}