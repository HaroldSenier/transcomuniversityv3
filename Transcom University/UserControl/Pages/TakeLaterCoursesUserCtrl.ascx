﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TakeLaterCoursesUserCtrl.ascx.cs"
    Inherits="UserControl_Pages_TakeLaterCoursesUserCtrl" %>
<rad:RadCodeBlock ID="rcbTakeLaterCourse" runat="server">
    <script type="text/javascript">

        //listview variables
        var takeLaterListView;

        var takeLaterGridView;
        var arrtakeLater;

        var virtualCount;
        var pageSize;
        var pageIndex;
        var defaultPageSize = 12;
        var oldPageSize;
        var isViewAll = false;

        var selectCourseID;
        var selectedCourseTitle;

        function pageLoad() {
            initializetakeLaterCourse();
        }

        //--Start takeLaterResult Binding

        //initialize takeLater Course
        function initializetakeLaterCourse() {
            $ = $telerik.$;
            //this will fire the OnCommand event
            takeLaterListView = $find("<%= lvtakeLaterResult.ClientID%>");
            takeLaterListView.rebind();
            pageSize = defaultPageSize;
            takeLaterListView.set_pageSize(pageSize);
            takeLaterListView.page(0);

            takeLaterGridView = $find("<%= gridtakeLaterResult.ClientID%>");

            $(".mcPager .pagePrev").click(function (e) {

                takeLaterListView.page(takeLaterListView.get_currentPageIndex() - 1);

            });
            $(".mcPager .pageNext").click(function (e) {

                takeLaterListView.page(takeLaterListView.get_currentPageIndex() + 1);

            });

            $(".mcPager .pageFirst").click(function (e) {

                takeLaterListView.page(0);

            });
            $(".mcPager .pageLast").click(function (e) {
                var lastpage = (Math.ceil(takeLaterListView.get_virtualItemCount() / takeLaterListView.get_pageSize()));
                console.log(lastpage);
                takeLaterListView.page(lastpage - 1);

            });

            $("#takeLaterListView").on("click", ".mc-item", function (e) {

                takeLaterListView.toggleSelection($(this).index());

            });

        }

        function bindtakeLaterCourseList() {
            takeLaterListView = $find("<%= lvtakeLaterResult.ClientID %>");

            var startRowIndex = takeLaterListView.get_currentPageIndex() * takeLaterListView.get_pageSize(),
                maximumRows = takeLaterListView.get_pageSize(),
                sortExpression = takeLaterListView.get_sortExpressions().toLinq();

            //make a call to get the data
            PageMethods.GetSearchCourseData(startRowIndex, maximumRows, sortExpression,
                function (result) {

                    var count = result.count;
                    var data = result.data;
                    pageSize = takeLaterListView.get_pageSize();
                    virtualCount = count;
                    takeLaterListView.set_virtualItemCount(count);

                    takeLaterListView.set_dataSource(data);

                    //console.log(result);
                    $(".js-mc-loader").hide();
                    takeLaterListView.dataBind();

                    //show switch view button and page size dropdown
                    if (data.length > 0) {
                        $(".mcPager").removeClass("display-none");
                        $(".switchView").removeClass("display-none");
                        $(".ddlPageSize-container").removeClass("display-none");
                    }

                    arrtakeLater = result;
                    takeLaterGridView.set_dataSource(data);
                    takeLaterGridView.dataBind();

                    if (isViewAll == true) {
                        $("#btnViewAllListTLC").addClass("display-none");
                        $("#btnBackListTLC").removeClass("display-none");

                        $("#btnViewAllGridTLC").addClass("display-none");
                        $("#btnBackGridTLC").removeClass("display-none");

                    } else {
                        $("#btnViewAllListTLC").removeClass("display-none");
                        $("#btnBackListTLC").addClass("display-none");

                        $("#btnViewAllGridTLC").removeClass("display-none");
                        $("#btnBackGridTLC").addClass("display-none");

                    }
                    pageNumberGenerator();
                    console.log("just rebind");
                },
                function (error) {
                    var code = error._statusCode;
                    if (code == 401)
                        location.reload();
                    else if (code == 500)
                        radalert("Something went while submitting your report. Please try again.", 330, 180, "Error Message", "");
                });
        }

        //Client Events takeLaterCourse

        function mcOnListViewDataBinding(sender, args) {
            sender.set_selectedIndexes([]);
        }

        function mcOnListViewCommand(sender, args) {
            args.set_cancel(true);
            bindtakeLaterCourseList();
        }

        function btnSwitchView_Click() {
            if ($('#<%= pnlLvtakeLaterResult.ClientID %>').hasClass("display-none")) {

                $("#<%= pnlLvtakeLaterResult.ClientID %>").removeClass("display-none");
                $("#<%= pnlGridtakeLaterResult.ClientID %>").addClass("display-none");
                $("#<%= btnSwitchToGridView.ClientID %>").addClass("display-none");
                $("#<%= btnSwitchToListView.ClientID %>").removeClass("display-none");
                //switch to gridview

                console.log("to list");
            } else {
                //switch to list view

                $("#<%= pnlLvtakeLaterResult.ClientID %>").addClass("display-none");
                $("#<%= pnlGridtakeLaterResult.ClientID %>").removeClass("display-none");
                $("#<%= btnSwitchToListView.ClientID %>").addClass("display-none");
                $("#<%= btnSwitchToGridView.ClientID %>").removeClass("display-none");
                console.log("to grid");
                var stored = sessionStorage.getItem('takeLaterResult');
                console.log("tolist" + stored);

                takeLaterGridView.set_dataSource(arrtakeLater);
                takeLaterGridView.dataBind();
                //takeLaterGridView.page(0);


            }
            return false;
        }

        function inGridView() {
            if ($('#<%= pnlLvtakeLaterResult.ClientID %>').hasClass("display-none"))
                return false;
            else
                return true;
        }

        function sorttakeLater() {
            takeLaterListView.get_sortExpressions().clear();
            takeLaterListView.get_sortExpressions().add('CourseTitle', "DESC");
            takeLaterListView.rebind();
        }

        function settakeLaterTerm(id) {
            var qs = decodeURI('<%= Request.QueryString["q"] %>');
            $("#takeLateredTerm").html(qs);
        }

        function generatePageNumbers() {
            var maxPage = Math.ceil(virtualCount / pageSize);
            var pageVirtualCount = virtualCount;
            var pages = $(".js-page-numbers ul").html('');

            for (var i = 1; i <= maxPage; i++) {
                var pages = $(".js-page-numbers ul").html();
                var rp = i - 1;
                $(".js-page-numbers ul").html(pages + '<li class="page-number"> <a href="javascript:void(0);" onclick="gotoPage(' + rp + ');">' + i + '</a></li>');
            }
        }

        function gotoPage(pageNum) {
            $(".js-page-numbers ul li").removeClass("selected-page");
            takeLaterListView.page(pageNum);
            takeLaterGridView.page(pageNum);
        }

        function setPage() {
            pageIndex = takeLaterListView.get_currentPageIndex();
            var myPage = pageIndex + 1;
            $(".js-page-numbers ul li").removeClass("selected-page");
            $(".js-page-numbers ul li:nth-child(" + myPage + ")").addClass("selected-page");
            var itemCount = pageSize * (pageIndex + 1) <= virtualCount ? (pageSize * (pageIndex + 1)) : ((pageIndex * pageSize) + (virtualCount % pageSize));
            $(".lblShowNumber").html("Showing " + itemCount + " of " + virtualCount + " Take Later Courses");
        }

        function confirmLaunchCourseWithComments(courseId, title) {

            $('#divrdComments').hide();
            selectCourseID = courseId;
            selectedCourseTitle = title;
            $("#commentsCourseTitle").html(selectedCourseTitle);
            var viewComments = $("#<%=hfView.ClientID%>").val() + " " + $("#<%=hfComments.ClientID%>").val();
            var Launch = $("#<%=hfLaunch.ClientID%>").val();
            Telerik.Web.UI.RadWindowUtils.Localization =  
            { 
                "OK" : Launch, 
                "Cancel" : viewComments,
            };
            var RadConfirmWhatDoyouWantCourseMessage = $("#<%=RadConfirmWhatDoyouWantCourseMessage.ClientID%>").val();
            var RadConfirmWhatDoyouWantCourseMessageAction = $("#<%=RadConfirmWhatDoyouWantCourseMessageAction.ClientID%>").val();
            radconfirm(RadConfirmWhatDoyouWantCourseMessage, confirmLaunchTakeLaterFn, 330, 180, "", RadConfirmWhatDoyouWantCourseMessageAction);

            function confirmLaunchTakeLaterFn(arg) {
                if (arg) {
                
                    //window.location.href = "CourseRedirect.aspx?CourseID=" + selectCourseID;
                    var tz = $("#hfEncryptedOffset").val();
                    window.location.href = "CourseLauncher.aspx?CourseID=" + selectCourseID + "&tz=" + tz;
                } else if(arg == false) {
                     //$(".js-cm-loader").show();
                    rebindCommentsWindow();                    
                    $find('<%= rwComments.ClientID %>').show();
                    
                }
                console.log(arg);


            }
        }

       

        function setDdlPagerValue(selectedPageSize) {
            if (selectedPageSize >= virtualCount) {
                $("#ddlpageSize").val(virtualCount);
            } else {
                $("#ddlpageSize").val(selectedPageSize);
            }
        }

        function changePageSize(pageSizer) {
            isViewAll = false;
            var pageSizerID = pageSizer.id;
            pageSize = $("#" + pageSizerID).val();

            if (!$.isNumeric(pageSize)) {
                pageSize = defaultPageSize;
            }

            rebindPageSize(pageSize)
        }

        function pageNumberGenerator() {
            generatePageNumbers(); //generate page numbers depending on virtual count and pagesize
            setPage(); //set the numbers displayed in "n of n Take Later Courses"
        }

        function btnViewAllListTLC_Click() {
            console.log(virtualCount);
            isViewAll = true;
            oldPageSize = takeLaterListView.get_pageSize();
            rebindPageSize(virtualCount);


            //            $("#btnViewAllListTLC").addClass("display-none");
            //            $("#btnBackListTLC").removeClass("display-none");

            //            $("#btnViewAllGridTLC").addClass("display-none");
            //            $("#btnBackGridTLC").removeClass("display-none");

            //            if (takeLaterListView.get_pageSize() < virtualCount) {
            //                

            //            } else {
            //               

            //            }

            return false;
        }

        function btnBackListTLC_Click() {
            isViewAll = false;
            rebindPageSize(oldPageSize);
            //            $("#btnViewAllListTLC").removeClass("display-none");
            //            $("#btnBackListTLC").addClass("display-none");

            //            $("#btnViewAllGridTLC").removeClass("display-none");
            //            $("#btnBackGridTLC").addClass("display-none");
            return false;

        }


        function rebindPageSize(ps) {

            takeLaterListView.set_pageSize(ps);
            takeLaterListView.page(0);
            takeLaterListView.rebind();

            takeLaterGridView.set_pageSize(ps);
            takeLaterGridView.page(0);
            //setDdlPagerValue(ps);
        }

        function rebindCommentsWindow(){
            var commentList = $find("<%= rdComments.ClientID %>");    
            $(".js-cm-loader").show();               
            console.log(selectCourseID);
            PageMethods.getCourseComments(selectCourseID,
                function (result) {
                    commentList.set_dataSource(result);
                    commentList.dataBind();
                    console.log(result);
                    $(".js-cm-loader").hide();
                    
                    $(".js-comments-title").removeClass("display-none");                    
                    $('#divrdComments').show()
                    //$("#<%= rdComments.ClientID %>").removeClass("display-none");
                },
                function (error) {
                    var code = error._statusCode;
                    if (code == 401)
                        location.reload();
                    else if (code == 500)
                        var RadAlertSomethingWentWhileMessage = $("#<%=RadAlertSomethingWentWhileMessage.ClientID%>").val();
                        var RadAlertSomethingWentWhileError = $("#<%=RadAlertSomethingWentWhileError.ClientID%>").val();
                        radalert(RadAlertSomethingWentWhileMessage, 330, 180, RadAlertSomethingWentWhileError, "");
                });
        }

        function OnClientLoadRadrating(sender, args) {
            sender.set_value(3);
        }

        
        function SetRatingStar($star_rating) {
            return $star_rating.each(function () {
                if (parseInt($star_rating.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
                    return $(this).removeClass('fa-star-o').addClass('fa-star active');
                } else {
                    return $(this).removeClass('fa-star-o').addClass('fa-star inactive');
                }
            });
        }

    </script>
</rad:RadCodeBlock>
<rad:RadWindowManager ID="rwmtakeLaterResult" RenderMode="Lightweight" EnableShadow="true"
    VisibleOnPageLoad="false" Behaviors="Close, Move" DestroyOnClose="true" Modal="true"
    Opacity="99" runat="server" VisibleStatusbar="false" Skin="Bootstrap" Width="620px"
    Height="400px">
    <Windows>
        <rad:RadWindow ID="rwComments" runat="server" CssClass="tc-radwindow-3">
            
            <ContentTemplate>
                <div class="loader js-cm-loader">
                </div>
                <div id="divrdComments"> 
                <rad:RadListView ID="rdComments" runat="server" CssClass="display-none" ClientIDMode="Static">
                    <LayoutTemplate>
                        <div class="width-fill margin-bottom-10px display-none js-comments-title">
                            <div class="col-xs-8 col-xs-offset-2 no-padding width-fill">
                                <p id="commentsCourseTitle" class="teal">
                                </p>
                            </div>
                        </div>
                        <div id="cm-items">
                        </div>
                    </LayoutTemplate>
                    <ClientSettings>
                        <DataBinding ItemPlaceHolderID="cm-items">
                            <ItemTemplate>
                        <div class="cr-comment-container container width-fill">
                            <div class="row">
                                <div class="col-xs-1">
                                     <image id="uImg" src='#= UserImageUrl #' class="img-circle" ></image>
                                </div>
                                <div class="col-xs-8">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <asp:Label ID="lblName" runat="server" CssClass="cr-name">#= Name#</asp:Label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                        <div class="display-inline-flex">
                                            <div class="star-rating-#= EnrolleeCIM # star-rating">
                                                <span class="fa fa-star-o" data-rating="1"></span>
                                                <span class="fa fa-star-o" data-rating="2"></span>
                                                <span class="fa fa-star-o" data-rating="3"></span>
                                                <span class="fa fa-star-o" data-rating="4"></span>
                                                <span class="fa fa-star-o" data-rating="5"></span>
                                                <input type="hidden" name="whatever1" class="rating-value" value="#= Rating #">
                                            </div>
                                            <script type="text/javascript">
                                                var ecim = "#= EnrolleeCIM #";
                                                var star_rating = $('.star-rating-' + ecim + ' .fa');
                                                SetRatingStar(star_rating);
                                            </script>
                                        </div>
                                        <p class="pull-right">
                                            Date:
                                                #= FormattedDateModified #
                                        </p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <i>
                                                <p class="teal">
                                                    "#= Comment #"
                                                </p>
                                            </i>
                                        </div>
                                    </div>
                                </div>
                                </div>
                        </div>
                            </ItemTemplate>
                            <EmptyDataTemplate>
                                No Comment to display.
                            </EmptyDataTemplate>
                        </DataBinding>
                    </ClientSettings>
                </rad:RadListView>
                </div>
            </ContentTemplate>
        </rad:RadWindow>
    </Windows>
</rad:RadWindowManager>
<asp:Panel ID="takeLaterResultContainer" runat="server" CssClass="takeLater-container search-container"
    Style="width: 100%; min-height: calc(100vh - 200px);">
    <div class="row">
        <div class="switchView display-none pull-left col-md-6 " style="line-height: 34px;">
            <asp:LinkButton ID="btnSwitchToListView" runat="server" OnClientClick="btnSwitchView_Click(); return false;"
                Font-Underline="false" aria-hidden="true" ToolTip="Switch to List View">
                <i id="switchClassListView" runat="server" class="fa fa-th-list "></i>
            </asp:LinkButton>
            <asp:LinkButton ID="btnSwitchToGridView" runat="server" OnClientClick="btnSwitchView_Click(); return false;"
                Font-Underline="false" aria-hidden="true" CssClass="display-none" ToolTip="Switch to Grid View">
                <i id="switchClassGridView" runat="server" class="fa fa-th "></i>
            </asp:LinkButton>
        </div>
        <div class="ddlPageSize-container display-none col-md-6">
            <div class="display-inline-flex pull-right">
                <div class="col-md-2">
                    <p style="line-height: 34px;">
                        <asp:Label runat="server" ID="Label1" Text="<%$ Resources:LocalizedResource, Show %>"></asp:Label>
                    </p>
                </div>
                <div class="col-md-4">
                    <asp:DropDownList ID="ddlpageSize" runat="server" CssClass="form-control rounded-corner"
                        onchange="changePageSize(this); return false;" ClientIDMode="Static">
                    </asp:DropDownList>
                </div>
                <div class="col-md-6 no-padding">
                    <p style="line-height: 34px;">
                        <asp:Label runat="server" ID="Label6" Text="<%$ Resources:LocalizedResource, ItemsPerPage %>"></asp:Label>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <asp:Panel ID="pnlLvtakeLaterResult" runat="server" CssClass="takeLater-view">
        <div id="mc-container" class="mc-container">
            <div class="loader js-mc-loader">
            </div>
            <div class="mcPager container search-result display-none">
                <p class="lblShowNumber" class="pull-left">
                </p>
                <div class="row-fluid mcViews">
                    <div class="pull-right">
                        <button id="btnViewAllListTLC" onclick="btnViewAllListTLC_Click(); return false;"
                            class="gold btn-transparent" style="line-height: 32px;">
                            <asp:Label runat="server" ID="Label7" Text="<%$ Resources:LocalizedResource, ViewAll %>"></asp:Label></button>
                        <button id="btnBackListTLC" onclick="btnBackListTLC_Click(); return false;" class="display-none gold btn-transparent"
                            style="line-height: 32px;">
                            <i class="fa fa-long-arrow-left "></i>
                        </button>
                    </div>
                </div>
            </div>
            <rad:RadListView ID="lvtakeLaterResult" runat="server" AllowPaging="true" PageSize="8"
                AllowMultiFieldSorting="true">
                <LayoutTemplate>
                    <div id="takeLaterListView">
                        <div id="mc-items" class="display-flow">
                        </div>
                        <div class="mcPager container display-none">
                            <div class="js-page-numbers display-inline-flex takeLaterCourse-page">
                                <a class="pagePrev black-arrow fa fa-caret-left no-underline-hover" href="javascript:void(0);"
                                    title=" #= Resources:LocalizedResource, GoToPreviousPage # "></a>
                                <ul class="nostyle display-inline-flex black-font">
                                </ul>
                                <a class="pageNext black-arrow fa fa-caret-right no-underline-hover" href="javascript:void(0);"
                                    title=" #= Resources:LocalizedResource, GoToNextPage # "></a>
                            </div>
                        </div>
                    </div>
                </LayoutTemplate>
                <ClientSettings>
                    <DataBinding ItemPlaceHolderID="mc-items">
                        <ItemTemplate>
                                <div class="col-md-3">
                                <a id="hllvtakeLaterResult" onclick='confirmLaunchCourseWithComments("#= EncryptedCourseID #", "#= CourseTitle #");' style="color:Black;" title=" #= CourseTitle #">
                                <div id="divtakeLaterResult" runat="server" class="mc-item lv rlvI">
                                    <div class="photo-container">
                                        <image src='Media/Uploads/CourseImg/#= CourseID #/#= CourseImage #' onerror="this.src='Media/Uploads/CourseImg/No_image.jpg'" style="height:130px;width:190px;"></image>
                                    </div>
                                        <div class="category font-bold text-trim">
                                            #= CourseTitle #
                                        </div>
                                            <div class="category">
                                            #= CourseType #
                                        </div>
                                        <div class="category">
                                            #= CourseDuration #
                                        </div>
                                        <div class="category">
                                            #= CourseCategory #
                                        </div>
                                        <div class="category">
                                            #= CourseSubcategory #
                                        </div>
                                        <div class="clearfix">
                                        </div>
                                </div>
                                </a>
                                </div>
                        </ItemTemplate>
                        <EmptyDataTemplate>
                        <p>No Take Later Courses.
                        </p>
                        </EmptyDataTemplate>
                        <DataService EnableCaching="true" />
                    </DataBinding>
                    <ClientEvents OnCommand="mcOnListViewCommand" OnDataBinding="mcOnListViewDataBinding">
                    </ClientEvents>
                </ClientSettings>
            </rad:RadListView>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlGridtakeLaterResult" runat="server" CssClass="display-none takeLater-view">
        <rad:RadListView ID="gridtakeLaterResult" runat="server" AllowPaging="true" PageSize="8">
            <LayoutTemplate>
                <div id="takeLaterGridView">
                    <table class="gridMainTable table table-bordered table-striped course-grid">
                        <thead>
                            <tr class="rlvHeader">
                                <th class="btn-teal">                                    
                                    <asp:Label runat="server" ID="label1" Text=" <%$ Resources:LocalizedResource, Title%>" ></asp:Label>
                                </th>
                                <th class="btn-teal">
                                    <asp:Label runat="server" ID="label8" Text=" <%$ Resources:LocalizedResource, Description%>" ></asp:Label>
                                </th>
                                <th class="btn-teal">
                                    <asp:Label runat="server" ID="label9" Text=" <%$ Resources:LocalizedResource, Duration%>" ></asp:Label>
                                </th>
                                <th class="btn-teal">
                                    <asp:Label runat="server" ID="label10" Text=" <%$ Resources:LocalizedResource, LastUpdated%>" ></asp:Label>
                                </th>
                                <th class="btn-teal">
                                    <asp:Label runat="server" ID="label11" Text=" <%$ Resources:LocalizedResource, CoursePath%>" ></asp:Label>
                                </th>
                            </tr>
                        </thead>
                        <tbody id="gridmcitem">
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                    <div class="mcPager container display-none">
                        <div class="js-page-numbers display-inline-flex takeLaterCourse-page">
                            <a class="pagePrev black-arrow fa fa-caret-left no-underline-hover" href="javascript:void(0);"
                                title=" #= Resources:LocalizedResource, GoToPreviousPage # "></a>
                            <ul class="nostyle display-inline-flex black-font">
                            </ul>
                            <a class="pageNext black-arrow fa fa-caret-right no-underline-hover" href="javascript:void(0);"
                                title=" #= Resources:LocalizedResource, GoToNextPage # "></a>
                        </div>
                    </div>
                </div>
            </LayoutTemplate>
            <ClientSettings>
                <DataBinding ItemPlaceHolderID="gridmcitem">
                    <ItemTemplate>
                    <tr class="rlvI">
                        <td>
                            <a id="hlgridtakeLaterResult" onclick='confirmLaunchCourseWithComments("#= EncryptedCourseID #");' title=" #= CourseTitle #" class="hover-pointer"> #= CourseTitle #</a>
                        </td>
                        <td>
                            <asp:Label ID="Label2" runat="server"> #= CourseDescription #</asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="Label3" runat="server"> #= CourseDuration #</asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="Label4" runat="server"> #= DateLastModified #</asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="Label5" runat="server"> #= CoursePath #</asp:Label>
                        </td>
                    </tr>
                    </ItemTemplate>
                    <EmptyDataTemplate>
                        <p>No Take Later Courses.
                        </p>
                    </EmptyDataTemplate>
                    <DataService EnableCaching="true" />
                </DataBinding>
            </ClientSettings>
        </rad:RadListView>
    </asp:Panel>
</asp:Panel>
<asp:HiddenField runat="server" ID="RadAlertSomethingWentWhileMessage" Value="<%$ Resources:LocalizedResource, SomethingWentWhileSubmittingYourReportPleaseTryAgain %>" />
<asp:HiddenField runat="server" ID="RadAlertSomethingWentWhileError" Value="<%$ Resources:LocalizedResource, ErrorMessage %>" />
<asp:HiddenField runat="server" ID="RadConfirmWhatDoyouWantCourseMessage" Value="<%$ Resources:LocalizedResource, Whatdoyouwanttodowiththiscourse %>" />
<asp:HiddenField runat="server" ID="RadConfirmWhatDoyouWantCourseMessageAction" Value="<%$ Resources:LocalizedResource, SelectAction %>" />
<asp:HiddenField runat="server" ID="hfView" Value="<%$ Resources:LocalizedResource, View %>" />
<asp:HiddenField runat="server" ID="hfComments" Value="<%$ Resources:LocalizedResource, Comments %>" />
<asp:HiddenField runat="server" ID="hfLaunch" Value="<%$ Resources:LocalizedResource, Launch %>" />

