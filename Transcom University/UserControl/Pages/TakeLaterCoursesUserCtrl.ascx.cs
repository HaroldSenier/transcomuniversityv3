﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TranscomUniversityV3Model;

public partial class UserControl_Pages_TakeLaterCoursesUserCtrl : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        { 
            //populate ddl page size
            for (int i = 1; i <= 20; i++)
            {
                ListItem ddlItem = new ListItem();
                ddlItem.Text = i.ToString();
                ddlItem.Value = i.ToString();
                ddlpageSize.Items.Add(ddlItem);

            }
        }
    }
}
