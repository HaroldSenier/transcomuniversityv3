﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TestimonialSettingUserCtrl.ascx.cs"
    Inherits="UserControl_Pages_TestimonialSettingUserCtrl" %>
<rad:RadScriptBlock ID="rsbImageSlider" runat="server">
    <script type="text/javascript">
        function pageLoad() {

            if(!$(".tc-slick-carousel-testimonial").hasClass("slick-initialized")){
                 $(".tc-slick-carousel-testimonial").slick({
                        autoplay: false,
                        slidesToShow: 1,
                        slidesToScroll:1,
                        speed: 5000,
                        autoplaySpeed: 6000,
                        arrows: true,
                        dots:false,
                        vertical:true,
                        infinite: true,
                        draggable: false,
                    });
            }
            

             $("#testimonialCarousel").dblclick(function () {
                openTestimonialSettings();
            });

            bindCurrentComments(0);
        }

        function openTestimonialSettings(){
            bindOtherPlaylist(0);
            opentestimonialSettings();
        }

        function opentestimonialSettings() {
            $find("<%= rwTestimonialSettings.ClientID %>").show();
        }

         function closePlaylistSetting() {
            $find("<%= rwTestimonialSettings.ClientID %>").close();
        }


        

        function setDirectionActive(direction) {
           
            if($.isNumeric(direction) && (direction >=1 && direction <= 4)){
                $(".js-direction-selector i").removeClass("active");
                $("#<%= hfLoopDirection.ClientID %>").val(direction);
                    switch (direction) {
                        case 1:
                            $(".js-direction-selector #arrowLeft").addClass("active");
                            break;
                        case 2:
                            $(".js-direction-selector #arrowRight").addClass("active");
                            break;
                        case 3:
                            $(".js-direction-selector #arrowUp").addClass("active");
                            break;
                        case 4:
                            $(".js-direction-selector #arrowDown").addClass("active");
                            break;
                        default:
                            $(".js-direction-selector i").removeClass("active");
                    }
                    
            }else{
                
            }
            
            
        }

        function openPlaylistCreator(){
            $find("<%= rwPlaylistCreatorTestimonial.ClientID %>").show();
        }

        function closePlaylistCreator(){
            $find("<%= rwPlaylistCreatorTestimonial.ClientID %>").close();
        }

         function savePlaylistCreator() {
            var playlistName = $("#<%= txtPlaylistName.ClientID %>").val().trim();

           if (Page_ClientValidate("savePlaylistTestimonial")) {
                $.ajax({
                    type: "POST",
                    data: '{playlistName: "' + playlistName + '", user: "<%= HttpContext.Current.User.Identity.Name.Split('|')[0] %>"}',
                    contentType: "application/json; charset=utf-8",
                    url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "SaveNewTestimonialPlaylist",
                    dataType: 'json',
                
                    success: function (res) {
                        if(res.d.isSuccess == true){
                            //reload playlist list
                            closePlaylistCreator();
                            //showtestimonialSettingsLoading();
                            bindOtherPlaylist(2);
                            //message
                        }else{
                        var hndradalerterrorn = $("#<%=hndradalerterrorn.ClientID%>").val();
                        var hndradalerterror = $("#<%=hndradalerterror.ClientID%>").val();
                            radalert(hndradalerterrorn + res.d.message, 330, 180, hndradalerterror, "");
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        var code = XMLHttpRequest.status;
                        if (code == "401")
                            window.location.reload();
                        else
                            alert(XMLHttpRequest.responseText);
                    }
                });
         
           }else{
           var hndradalertplayname = $("#<%=hndradalertplayname.ClientID%>").val();
           var hndradalerterror = $("#<%=hndradalerterror.ClientID%>").val();
                radalert(hndradalertplayname, 330, 180, hndradalerterror, "");
           } 
        }

        function bindOtherPlaylist(operation) {
          
            var otherpl = $find("<%= lvOtherPlaylist.ClientID %>");
            var plselector = $find("<%= lvPlaylistSelector.ClientID %>");
            
            showtestimonialSettingsLoading();
             $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "GetOtherPlaylistTestimonial",
                data: '{optional : "1"}' ,
                dataType: 'json',
                success: function (res) {
                    var count = res.d.count;
                    var data = res.d.data;
                    otherpl.set_virtualItemCount(count);
                    otherpl.set_dataSource(data);
                    otherpl.dataBind();

                    plselector.set_virtualItemCount(count);
                    plselector.set_dataSource(data);
                    plselector.dataBind();

                    if(count > 0){
                        hidetestimonialSettingsLoading();
                        //radalert("Playlist successfully removed.", 330, 180, "Success", "");
                    }

                    if(operation == 1){//remove
                    var hndradalaertplayremove = $("#<%=hndradalaertplayremove.ClientID%>").val();
                    var hndradalertsuccess = $("#<%=hndradalertsuccess.ClientID%>").val();
                        radalert(hndradalaertplayremove, 330, 180, hndradalertsuccess, "");
                    }else if (operation == 2)
                    var hndradalertaddsuccess = $("#<%=hndradalertaddsuccess.ClientID%>").val();
                    var hndradalertsuccess = $("#<%=hndradalertsuccess.ClientID%>").val();
                       radalert(hndradalertaddsuccess, 330, 180, hndradalertsuccess, "");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var code = XMLHttpRequest.status;
                    if (code == "401")
                        window.location.reload();
                    else
                        alert(XMLHttpRequest.responseText);
                }
            });
        }



        function plOnListViewDataBinding(sender, args) {
            sender.set_selectedIndexes([]);
        }

        function plOnListViewCommand(sender, args) {
            args.set_cancel(true);
            bindOtherPlaylist(0);
        }


        //user comments
        function bindUserComments() {
          
            var commentlist = $find("<%= rdComments.ClientID %>");
            var sortIndex = playInterval = $find("<%= ddlCommentSort.ClientID %>").get_selectedItem().get_value();
            showtestimonialSettingsLoading();
             $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: '{sortIndex: ' + sortIndex + '}',
                url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "GetAllUserComments",
                dataType: 'json',
                success: function (res) {
                    var count = res.d.count;
                    var data = res.d.data;
                    commentlist.set_virtualItemCount(count);
                    commentlist.set_dataSource(data);
                    commentlist.dataBind();
                    if(count > 0){
                        //hidetestimonialSettingsLoading();
                        //radalert("Playlist successfully removed.", 330, 180, "Success", "");
                    }

//                    if(operation == 1)//remove
//                        radalert("Playlist Removed", 330, 180, "Success", "");
//                    else if (operation == 2)
//                       radalert("Playlist added successfully", 330, 180, "Success", "");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var code = XMLHttpRequest.status;
                    if (code == "401")
                        window.location.reload();
                    else
                        alert(XMLHttpRequest.responseText);
                }
            });
        }

        function cmOnListViewDataBinding(sender, args) {
            sender.set_selectedIndexes([]);
        }

        function cmOnListViewCommand(sender, args) {
            args.set_cancel(true);
            bindUserComments();
        }


        //current displayed comments
        function bindCurrentComments(operation) {
          
            var commentlist = $find("<%= lvCurrentPlaylist.ClientID %>");
            showCurrentPlaylistLoading();
             $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "GetCurrentTestimonialPlaylist",
                data: '{optional : "1"}' ,
                dataType: 'json',
                success: function (res) {
                    var count = res.d.count;
                    var data = res.d.data;
                    commentlist.set_virtualItemCount(count);
                    commentlist.set_dataSource(data);
                    commentlist.dataBind();
                    if(count > 0){
                        hideCurrentPlaylistLoading();
                        //radalert("Playlist successfully removed.", 330, 180, "Success", "");
                    }

                    if(operation == 1){//remove
                    var hndradalertremovecomment = $("#<%=hndradalertremovecomment.ClientID%>").val();
                    var hndradalertsuccess = $("#<%=hndradalertsuccess.ClientID%>").val();
                        radalert(hndradalertremovecomment, 330, 180, hndradalertsuccess, "");
                   } else if (operation == 2)
                   var hndradalertsuccaddcomment = $("#<%=hndradalertsuccaddcomment.ClientID%>").val();
                    var hndradalertsuccess = $("#<%=hndradalertsuccess.ClientID%>").val();
                       radalert(hndradalertsuccaddcomment, 330, 180, hndradalertsuccess, "");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var code = XMLHttpRequest.status;
                    if (code == "401")
                        window.location.reload();
                    else
                        alert(XMLHttpRequest.responseText);
                }
            });
        }

        function bindCurrentComments(operation) {
          
            var commentlist = $find("<%= lvCurrentPlaylist.ClientID %>");
            showCurrentPlaylistLoading();
             $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "GetCurrentTestimonialPlaylist",
                data: '{optional : "1"}' ,
                dataType: 'json',
                success: function (res) {
                    var count = res.d.count;
                    var data = res.d.data;
                    commentlist.set_virtualItemCount(count);
                    commentlist.set_dataSource(data);
                    commentlist.dataBind();
                    if(count > 0){
                        hideCurrentPlaylistLoading();
                        //radalert("Playlist successfully removed.", 330, 180, "Success", "");
                    }

                    if(operation == 1){//remove
                    var hndradalertremovecomment = $("#<%=hndradalertremovecomment.ClientID%>").val();
                    var hndradalertsuccess = $("#<%=hndradalertsuccess.ClientID%>").val();
                        radalert(hndradalertremovecomment, 330, 180, hndradalertsuccess, "");
                    }else if (operation == 2)
                    var hndradalertsuccaddcomment = $("#<%=hndradalertsuccaddcomment.ClientID%>").val();
                    var hndradalertsuccess = $("#<%=hndradalertsuccess.ClientID%>").val();
                       radalert(hndradalertsuccaddcomment, 330, 180, hndradalertsuccess, "");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var code = XMLHttpRequest.status;
                    if (code == "401")
                        window.location.reload();
                    else
                        alert(XMLHttpRequest.responseText);
                }
            });
        }

        function cpOnListViewDataBinding(sender, args) {
            sender.set_selectedIndexes([]);
        }

        function cpOnListViewCommand(sender, args) {
            args.set_cancel(true);
            bindCurrentComments(0);
        }
        
        

        function showtestimonialSettingsLoading() {
            var panel = $find("<%= localLoadingPanel.ClientID %>");
            panel.show("testimonialSettings");
        }

        function hidetestimonialSettingsLoading() {
            //radalert("Playlist successfully Uploaded.", 330, 180, "Success", "");
            var panel = $find("<%= localLoadingPanel.ClientID %>");
            panel.hide("testimonialSettings");

        }

        function saveTestimonialSettings() {
            //validate
            var maxImages;
            var playInterval;
            var loopDirection;
            var selectedPlaylistID;

            maxImages =  $find("<%= ddlMaxItem.ClientID %>").get_selectedItem().get_value();
            playInterval = $find("<%= ddlInterval.ClientID %>").get_selectedItem().get_value();
            loopDirection =  $('#<%= hfLoopDirection.ClientID %>').val().trim();
            selectedPlaylistID = $('#<%= hfSelectedPlaylistID.ClientID %>').val().trim();
            
//            console.log("maxImages = " + maxImages);
//            console.log("playInterval = " + playInterval);
//            console.log("isAutoplay = " + isAutoplay);
//            console.log("isOnclick = " + isOnclick);
//            console.log("videoDelay = " + videoDelay);
//            console.log("isVideoOnClick = " + isVideoOnClick);
//            console.log("loopDirection = " + loopDirection);
//            console.log("selectedPlaylistID = " + selectedPlaylistID);
            //save
            $.ajax({
                type: "POST",
                data: '{ maxImages: "' + maxImages + '", playInterval: "' + playInterval + '", loopDirection: "' + loopDirection+ '", selectedPlaylistID: "' + selectedPlaylistID + '", user: "<%= HttpContext.Current.User.Identity.Name.Split('|')[0] %>"}',
                contentType: "application/json; charset=utf-8",
                url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "SaveTestimonialSetting",
                dataType: 'json',
                success: function (res) {
                    console.log(res);
                    if(res.d.isSuccess == true){
                        //window.location.reload();
                        //bindCurrentPlaylist(1);
                        var hndradalertnewsaveset = $("#<%=hndradalertnewsaveset.ClientID%>").val();
                        var hndradalaertsaveds = $("#<%=hndradalaertsaveds.ClientID%>").val();
                         radalert(hndradalertnewsaveset, 330, 180, hndradalaertsaveds, "");
                    }
                    else
                    var hndradalerterror = $("#<%=hndradalerterror.ClientID%>").val();
                        radalert(res.d.message, 330, 180, hndradalerterror, "");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var code = XMLHttpRequest.status;
                    if (code == "401")
                        window.location.reload();
                    else
                        alert(XMLHttpRequest.responseText);
                }
            });

            $find("<%= rwTestimonialSettings.ClientID %>").close();
        }

        function btnPlayClick(playlistID, playlistName){
            
            $("#<%= lblCurrentPlaylist.ClientID %>").html(playlistName);
            $("#<%= hfSelectedPlaylistID.ClientID %>").val(playlistID);
            $("#<%= hfSelectedPlaylistName.ClientID %>").val(playlistName);
            
            bindCurrentComments(0);
            
        }

        function SetRatingStar($star_rating) {
            return $star_rating.each(function () {
                if (parseInt($star_rating.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
                    return $(this).removeClass('fa-star-o').addClass('fa-star active');
                } else {
                    return $(this).removeClass('fa-star-o').addClass('fa-star inactive');
                }
            });
        }

        
    function addToPlaylist(commentID){
         //show playlistList
        $("#<%= hfSelectedCommentID.ClientID %>").val(commentID);
        $find("<%= rwPlaylistSelector.ClientID %>").show();

    }

    function closePlaylistSelector(){
         $find("<%= rwPlaylistSelector.ClientID %>").close();
    }

    function btnSelectPlaylistClick(playlistID, playlistName){
    var hndradconfirmaddplay = $("#<%=hndradconfirmaddplay.ClientID%>").val();
    var hndradconfirmadd = $("#<%=hndradconfirmadd.ClientID%>").val();
         radconfirm(hndradconfirmaddplay + playlistName + " ?", callBackConfirmAddComment, 330, 180, "", hndradconfirmadd);
         var commentID =   $("#<%= hfSelectedCommentID.ClientID %>").val();
            function callBackConfirmAddComment(arg){
                if(arg){
                     $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        data: '{playlistID: "' + playlistID + '", commentID: "' + commentID + '", user: "<%= HttpContext.Current.User.Identity.Name.Split('|')[0] %>"}',
                        url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "AddCommentToTestimonial",
                        dataType: 'json',
                        success: function (res) {
                            var isSuccess = res.d.isSuccess;
                            if(isSuccess == true){
                                //bindOtherPlaylist(1);
                                var pl = $find("<%= lvCurrentPlaylist.ClientID %>");
                                 //pl.rebind();
                                 //radalert("Successfully Added Comment", 330, 180, "Success", "");
                                 bindCurrentComments(2);
                                 
                                
                            }else{
                            var hndradalerterror = $("#<%=hndradalerterror.ClientID%>").val();
                                radalert(res.d.message, 330, 180, hndradalerterror, "");
                            }
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            var code = XMLHttpRequest.status;
                            if (code == "401")
                                //window.location.reload();
                                console.log("error 401");
                            else
                                alert(XMLHttpRequest.responseText);
                        }
                    });
                }
            }
        }

        function showCurrentPlaylistLoading() {
            var panel = $find("<%= localLoadingPanel.ClientID %>");
            panel.show("pnlCurrentPlaylist");
        }

        function hideCurrentPlaylistLoading() {
            var panel = $find("<%= localLoadingPanel.ClientID %>");
            panel.hide("pnlCurrentPlaylist");
        }

    </script>
</rad:RadScriptBlock>
<rad:RadAjaxLoadingPanel ID="localLoadingPanel" runat="server" CssClass="Loading2"
    Transparency="25" />
<rad:RadAjaxManagerProxy ID="rmp" runat="server">
    <AjaxSettings>
        <rad:AjaxSetting AjaxControlID="ddlCommentSort">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="ddlCommentSort" LoadingPanelID="localLoadingPanel" />
                <rad:AjaxUpdatedControl ControlID="pnlComments" LoadingPanelID="localLoadingPanel" />
            </UpdatedControls>
        </rad:AjaxSetting>
    </AjaxSettings>
</rad:RadAjaxManagerProxy>
<rad:RadWindowManager ID="rwm" RenderMode="Lightweight" EnableShadow="true" VisibleOnPageLoad="false"
    Behaviors="Close, Move" DestroyOnClose="true" Modal="true" Opacity="99" runat="server"
    VisibleStatusbar="false" Skin="Bootstrap">
    <Windows>
        <rad:RadWindow ID="rwTestimonialSettings" runat="server" Title="" CssClass="tc-radwindow-3 font-size-normal"
            Width="985px" AutoSizeBehaviors="Height" Height="700px">
            <ContentTemplate>
                <rad:RadTabStrip RenderMode="Lightweight" runat="server" ID="RadTabStrip1" MultiPageID="RadMultiPage1"
                    SelectedIndex="0" Skin="Bootstrap">
                    <Tabs>
                        <rad:RadTab Text="ON DISPLAY" Width="200px">
                        </rad:RadTab>
                        <rad:RadTab Text="USER COMMENTS" Width="200px">
                        </rad:RadTab>
                    </Tabs>
                </rad:RadTabStrip>
                <rad:RadMultiPage runat="server" ID="RadMultiPage1" SelectedIndex="0" CssClass="outerMultiPage"
                    SkinID="Bootstrap" RenderMode="Lightweight">
                    <rad:RadPageView runat="server" ID="RadPageView1">
                        <div class="col-md-12 no-paddings" style="height: 330px; border: 1px solid lightgray;
                            overflow: auto;">
                            <asp:Panel ID="pnlCurrentPlaylist" runat="server" Style="height: 50vh; overflow: auto;">
                                <rad:RadListView ID="lvCurrentPlaylist" runat="server" AllowPaging="false" PageSize="999">
                                    <LayoutTemplate>
                                        <div id="currentPlaylist">
                                        </div>
                                    </LayoutTemplate>
                                    <ClientSettings DataBinding-DataService-EnableCaching="true">
                                        <DataBinding ItemPlaceHolderID="currentPlaylist">
                                            <ItemTemplate>
                                            <div class="col-md-8">
                                                <div class="col-xs-1 ">
                                                    <img src='../#= UserImageUrl #' onerror='this.src="default-user.png"'
                                                        class="img-circle" alt="Alternate Text" height="60px" width="60px" />
                                                </div>
                                                <div class="col-xs-8 ">
                                                    <div class="row">
                                                        <div class="col-xs-6">
                                                            <h5 class="bold">
                                                                <p class="teal">
                                                                    #= Title #
                                                            </h5>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-6">
                                                            <asp:Label ID="lblName" runat="server" CssClass="bold">#= Name #</asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 display-inline-flex">
                                                          <div class="display-inline-flex">
                                                            <div class="star-rating-#= UID # star-rating">
                                                                    <span class="fa fa-star-o" data-rating="1"></span>
                                                                    <span class="fa fa-star-o" data-rating="2"></span>
                                                                    <span class="fa fa-star-o" data-rating="3"></span>
                                                                    <span class="fa fa-star-o" data-rating="4"></span>
                                                                    <span class="fa fa-star-o" data-rating="5"></span>
                                                                    <input type="hidden" name="whatever1" class="rating-value" value="#= Rating #">
                                                                </div>
                                                                <script type="text/javascript">
                                                                    var star_rating = $('.star-rating-' + #= UID # + ' .fa');
                                                                    SetRatingStar(star_rating);
                                                                </script>
                                                            </div>
                                                            &nbsp;&nbsp;
                                                            <p class="pull-right">
                                                                <i>
                                                                   #= DateCreated # </i>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-6">
                                                            <p class="teal">
                                                              #= OriginalComment # 
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            </ItemTemplate>
                                            <EmptyDataTemplate>
                                                   <%--<asp:Label runat="server" ID="notesti" Text= "<%$ Resources:LocalizedResource, Notestimonialtoshow %>"></asp:Label>--%>
                                                   No testimonial to show.
                                            </EmptyDataTemplate>
                                        </DataBinding>
                                        <ClientEvents OnCommand="cpOnListViewCommand" OnDataBinding="cpOnListViewDataBinding">
                                        </ClientEvents>
                                    </ClientSettings>
                                </rad:RadListView>
                            </asp:Panel>
                        </div>
                        <div id="testimonialSettings" class="col-md-12 no-paddings" style="height: 230px;
                            overflow: auto;">
                            <h5>
                                <strong><asp:Label runat="server" ID="playset" Text= "<%$ Resources:LocalizedResource, PkaySettings %>"></asp:Label>:</strong>
                                <br />
                            </h5>
                            <table class="padding-md" width="50%">
                                <tr>
                                    <td>
                                        <asp:Label runat="server" ID="maxtesti" Text= "<%$ Resources:LocalizedResource, Maxtestimonies %>"></asp:Label>
                                    </td>
                                    <td>
                                        <rad:RadDropDownList ID="ddlMaxItem" runat="server" CssClass="soft bg-white" Width="50px">
                                        </rad:RadDropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server" ID="intplays" Text= "<%$ Resources:LocalizedResource, Imageplayinterval %>"></asp:Label>
                                    </td>
                                    <td>
                                        <rad:RadDropDownList ID="ddlInterval" runat="server" CssClass="soft bg-white" Width="50px">
                                        </rad:RadDropDownList>
                                        <asp:Label runat="server" ID="secs" Text= "<%$ Resources:LocalizedResource, Seconds %>"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server" ID="ctile1" Text= "<%$ Resources:LocalizedResource, Loopdirection %>"></asp:Label>
                                    </td>
                                    <td>
                                        <div class="js-direction-selector">
                                            <i id="arrowLeft" runat="server" class="fa fa-arrow-circle-left pointer" aria-hidden="true"
                                                onclick="setDirectionActive(1);" clientidmode="Static"></i>&nbsp;&nbsp;&nbsp;
                                            <i id="arrowRight" runat="server" class="fa fa-arrow-circle-right pointer" aria-hidden="true"
                                                onclick="setDirectionActive(2);" clientidmode="Static"></i>&nbsp;&nbsp;&nbsp;
                                            <i id="arrowUp" runat="server" class="fa fa-arrow-circle-up pointer" aria-hidden="true"
                                                onclick="setDirectionActive(3);" clientidmode="Static"></i>&nbsp;&nbsp;&nbsp;
                                            <i id="arrowDown" runat="server" class="fa fa-arrow-circle-down pointer" aria-hidden="true"
                                                onclick="setDirectionActive(4);" clientidmode="Static"></i>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <h5>
                                <strong><asp:Label runat="server" ID="listplay" Text= "<%$ Resources:LocalizedResource, Playlist %>"></asp:Label>:</strong>
                                <br />
                            </h5>
                            <table class="padding-md" width="40%">
                                <tr>
                                    <td>
                                        <p class="teal bold">
                                            <asp:Label runat="server" ID="Cplay" Text= "<%$ Resources:LocalizedResource, CurrentPlaylist %>"></asp:Label>:
                                        </p>
                                    </td>
                                    <td>
                                        <strong>
                                            <asp:Label ID="lblCurrentPlaylist" runat="server"><asp:Label runat="server" ID="nov2016" Text= "<%$ Resources:LocalizedResource, November2016FeaturedImages %>"></asp:Label></asp:Label></strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p class="teal bold">
                                            <asp:Label runat="server" ID="Oplays" Text= "<%$ Resources:LocalizedResource, OtherPlaylists %>"></asp:Label>:
                                        </p>
                                    </td>
                                </tr>
                            </table>
                            <div id="slideListContainer" height="100%" runat="server">
                                <rad:RadListView ID="lvOtherPlaylist" runat="server" AllowPaging="false" PageSize="999">
                                    <LayoutTemplate>
                                        <div id="otherPlaylist">
                                        </div>
                                    </LayoutTemplate>
                                    <ClientSettings DataBinding-DataService-EnableCaching="true">
                                        <DataBinding ItemPlaceHolderID="otherPlaylist">
                                            <ItemTemplate>
                                        <div class="col-md-2" style="margin: 5px 0;">
                                            #= PlaylistName #
                                        </div>
                                        <div class="col-md-8" style="margin: 5px 0;">
                                            <button id="btnPreview" class="btn btn-md btn-teal btn-oval">
                                               <%--<asp:Label runat="server" ID="prevs" Text= "<%$ Resources:LocalizedResource, Preview %>"></asp:Label> --%>
                                               Preview
                                            </button>
                                             &nbsp;&nbsp;&nbsp;
                                            <button id="btnPlay" class="btn btn-md btn-teal btn-oval" onclick='btnPlayClick("#= ID #", "#= PlaylistName #"); return false;'>
                                                <%--<asp:Label runat="server" ID="paly" Text= "<%$ Resources:LocalizedResource, Play %>"></asp:Label>--%>
                                                Play
                                            </button>
                                            &nbsp;&nbsp;&nbsp;
                                            <button id="btnRemovePl" class="btn btn-md btn-teal btn-oval" onclick='removePlaylist("#= ID #", " #= PlaylistName # "); return false'>
                                                <%--<asp:Label runat="server" ID="removs" Text= "<%$ Resources:LocalizedResource, Remove %>"></asp:Label>--%>
                                                Remove
                                            </button>
                                        </div>
                                            </ItemTemplate>
                                            <EmptyDataTemplate>
                                                No other playlist
                                                <%--<asp:Label runat="server" ID="noother" Text= "<%$ Resources:LocalizedResource, Nootherplaylist %>"></asp:Label>--%>
                                            </EmptyDataTemplate>
                                        </DataBinding>
                                        <ClientEvents OnCommand="plOnListViewCommand" OnDataBinding="plOnListViewDataBinding">
                                        </ClientEvents>
                                    </ClientSettings>
                                </rad:RadListView>
                            </div>
                            <asp:LinkButton ID="LinkButton1" Text="" runat="server" CssClass="btn btn-md btn-default"
                                OnClientClick="openPlaylistCreator(); return false;" Style="padding: 0px 15px;
                                display: inline-flex; align-items: center;">
                        <i class="fa fa-plus-circle" aria-hidden="true" style="font-size:30px;color:Gray"></i>&nbsp;
                        <asp:Label runat="server" ID="newcreate" Text= "<%$ Resources:LocalizedResource, CreateNew %>"></asp:Label>
                            </asp:LinkButton>
                        </div>
                    </rad:RadPageView>
                    <rad:RadPageView runat="server" ID="RadPageView2">
                        <div class="col-md-12 no-padding">
                            <div class="pull-right">
                                <asp:Label runat="server" ID="showcoms" Text= "<%$ Resources:LocalizedResource, Showcomments %>"></asp:Label> 
                                <rad:RadDropDownList ID="ddlCommentSort" runat="server" DefaultMessage="- Select -"
                                    OnClientSelectedIndexChanged="bindUserComments">
                                    <Items>
                                        <rad:DropDownListItem Value="0" Text="- Select -" runat="server" />
                                        <rad:DropDownListItem Value="1" Text="Within the last 12 hours" runat="server" />
                                        <rad:DropDownListItem Value="2" Text="Within the last 24 hours" runat="server" />
                                        <rad:DropDownListItem Value="3" Text="Within the last week" runat="server" />
                                        <rad:DropDownListItem Value="4" Text="Within the last month" runat="server" />
                                        <rad:DropDownListItem Value="5" Text="Within the last 6 month" runat="server" />
                                        <rad:DropDownListItem Value="6" Text="More than a year" runat="server" />
                                    </Items>
                                </rad:RadDropDownList>
                            </div>
                        </div>
                        <div class="col-md-12 no-padding">
                            <div class="col-md-4 pull-left form-group">
                                <input type="text" name="name" value=" " placeholder="Search..." class="form-control input-sm no-padding pull-left" /><i
                                    class="fa fa-search pull-right" aria-hidden="true"></i>
                            </div>
                            <div class="col-md-2">
                                <button class="btn btn-md btn-gray btn-soft width-fill pull-left">
                                    <asp:Label runat="server" ID="uses" Text= "<%$ Resources:LocalizedResource, users %>"></asp:Label> <i class="fa fa-caret-down" aria-hidden="true"></i>
                                </button>
                            </div>
                            <div class="col-md-2">
                                <button class="btn btn-md btn-gray btn-soft width-fill pull-left">
                                    <asp:Label runat="server" ID="Dat" Text= "<%$ Resources:LocalizedResource, Date %>"></asp:Label><i class="fa fa-caret-down" aria-hidden="true"></i></button>
                            </div>
                            <div class="col-md-2">
                                <button class="btn btn-md btn-gray btn-soft width-fill pull-left">
                                   <asp:Label runat="server" ID="Ranks" Text= "<%$ Resources:LocalizedResource, Ranking %>"></asp:Label><i class="fa fa-caret-down" aria-hidden="true"></i></button>
                            </div>
                        </div>
                        <div class="col-md-12 no-padding">
                            <div class="container no-padding">
                                <asp:Panel ID="pnlComments" runat="server" Style="height: 50vh; overflow: auto;">
                                    <rad:RadListView ID="rdComments" runat="server" AllowPaging="false" PageSize="999">
                                        <LayoutTemplate>
                                            <div id="userComments">
                                            </div>
                                        </LayoutTemplate>
                                        <ClientSettings DataBinding-DataService-EnableCaching="true">
                                            <DataBinding ItemPlaceHolderID="userComments">
                                                <ItemTemplate>
                                            <div class="col-md-8">
                                                <div class="col-xs-1 ">
                                                    <img src='../#= UserImageUrl #' onerror='this.src="default-user.png"'
                                                        class="img-circle" alt="Alternate Text" height="60px" width="60px" />
                                                </div>
                                                <div class="col-xs-8 ">
                                                    <div class="row">
                                                        <div class="col-xs-6">
                                                            <h5 class="bold">
                                                                <p class="teal">
                                                                    #= Title #
                                                            </h5>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-6">
                                                            <asp:Label ID="lblName" runat="server" CssClass="bold">#= Name #</asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 display-inline-flex">
                                                          <div class="display-inline-flex">
                                                            <div class="star-rating-#= UID # star-rating">
                                                                    <span class="fa fa-star-o" data-rating="1"></span>
                                                                    <span class="fa fa-star-o" data-rating="2"></span>
                                                                    <span class="fa fa-star-o" data-rating="3"></span>
                                                                    <span class="fa fa-star-o" data-rating="4"></span>
                                                                    <span class="fa fa-star-o" data-rating="5"></span>
                                                                    <input type="hidden" name="whatever1" class="rating-value" value="#= Rating #">
                                                                </div>
                                                                <script type="text/javascript">
                                                                    var star_rating = $('.star-rating-' + #= UID # + ' .fa');
                                                                    SetRatingStar(star_rating);
                                                                </script>
                                                            </div>
                                                            &nbsp;&nbsp;
                                                            <p class="pull-right">
                                                                <i>
                                                                   #= DateCreated # </i>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-6">
                                                            <p class="teal">
                                                              #= Comment #
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-1 pull-right">
                                                <button>
                                                    <i class="fa fa-thumbs-o-up fa-md fa-gray" aria-hidden="true"></i>
                                                </button>
                                                <button>
                                                    <i class="fa fa-flag-o fa-md fa-gray" aria-hidden="true"></i>
                                                </button>
                                                <button onclick='addToPlaylist("#= CommentID#"); return false;'>
                                                    <i class="fa fa-television fa-md fa-gray" aria-hidden="true"></i>
                                                    <button>
                                            </div>
                                                </ItemTemplate>
                                                <EmptyDataTemplate>
                                                    <%--<asp:Label runat="server" ID="nocmms" Text= "<%$ Resources:LocalizedResource, Nocommenttoshow %>"></asp:Label> --%>
                                                    No comment to show.
                                                </EmptyDataTemplate>
                                            </DataBinding>
                                            <ClientEvents OnCommand="cmOnListViewCommand" OnDataBinding="cmOnListViewDataBinding">
                                            </ClientEvents>
                                        </ClientSettings>
                                    </rad:RadListView>
                                </asp:Panel>
                            </div>
                        </div>
                    </rad:RadPageView>
                </rad:RadMultiPage>
                <div class="row bottom-center">
                    <asp:Button ID="Button18" Text="Cancel" runat="server" CssClass="btn btn-md btn-teal btn-flat"
                        OnClientClick="closePlaylistSetting(); return false" />
                    <asp:Button ID="Button19" Text="Save Changes" runat="server" CssClass="btn btn-md btn-teal btn-flat"
                        ValidationGroup="savePlaylist" OnClientClick="saveTestimonialSettings(); return false;" />
                    <%--OnClick="btnPlaylistCreatorSave_Click"--%>
                </div>
            </ContentTemplate>
        </rad:RadWindow>
    </Windows>
    <Windows>
        <rad:RadWindow ID="rwPlaylistCreatorTestimonial" runat="server" Title="" CssClass="tc-radwindow-3 font-size-normal"
            Width="300px" Height="170px">
            <ContentTemplate>
                <div class="col-md-11 form-group no-padding">
                    <asp:Label ID="lblPlayingPlaylist" runat="server"><strong>Playlist Name:</strong></asp:Label>
                    <asp:RequiredFieldValidator ID="rfvtxtPlaylistName" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPlaylistName" SetFocusOnError="True" Display="Dynamic"
                        ValidationGroup="savePlaylistTestimonial" ForeColor="Red" />
                    <asp:TextBox ID="txtPlaylistName" runat="server" CssClass="form-control input-md"
                        Height="27px" />
                </div>
                <div class="row pull-left">
                    <asp:Button ID="Button1" Text="Cancel" runat="server" CssClass="btn btn-md btn-teal btn-flat"
                        OnClientClick="closePlaylistCreator(); return false" />
                    <asp:Button ID="Button2" Text="Add" runat="server" CssClass="btn btn-md btn-teal btn-flat"
                        ValidationGroup="savePlaylistTestimonial" OnClientClick="savePlaylistCreator(); return false;" />
                    <%--OnClick="btnPlaylistCreatorSave_Click"--%>
                </div>
            </ContentTemplate>
        </rad:RadWindow>
    </Windows>
    <Windows>
        <rad:RadWindow ID="rwPlaylistSelector" runat="server" Title="" CssClass="tc-radwindow-3 font-size-normal"
            Width="300px" Height="300px">
            <ContentTemplate>
                <div class="col-md-11 form-group no-padding" style="height: 300px; overflow: auto;">
                    <rad:RadListView ID="lvPlaylistSelector" runat="server" AllowPaging="false" PageSize="999">
                        <LayoutTemplate>
                            <div id="playlistSelector" class="border-gray">
                            </div>
                        </LayoutTemplate>
                        <ClientSettings DataBinding-DataService-EnableCaching="true">
                            <DataBinding ItemPlaceHolderID="playlistSelector">
                                <ItemTemplate>
                                        <div class="col-md-12 no-padding" style="margin: 5px 0;">
                                           <button id="btnSelectPlaylist" class="btn btn-md btn-gray btn-flat btn width-fill" onclick='btnSelectPlaylistClick("#= ID #", "#= PlaylistName #"); return false;'>
                                                #= PlaylistName #
                                            </button>
                                </ItemTemplate>
                                <EmptyDataTemplate>
                                        No other playlist
                                </EmptyDataTemplate>
                            </DataBinding>
                            <ClientEvents OnCommand="plOnListViewCommand" OnDataBinding="plOnListViewDataBinding">
                            </ClientEvents>
                        </ClientSettings>
                    </rad:RadListView>
                </div>
                <div class="row bottom-center">
                    <asp:Button ID="Button3" Text="Close" runat="server" CssClass="btn btn-md btn-teal btn-flat"
                        OnClientClick="closePlaylistSelector(); return false" />
                    <%--OnClick="btnPlaylistCreatorSave_Click"--%>
                </div>
            </ContentTemplate>
        </rad:RadWindow>
    </Windows>
</rad:RadWindowManager>
<asp:Panel ID="pnlCatalogSettings" runat="server" CssClass="region">
    <div class="wrapper-main-content">
        <div class="inner-content col-md-6">
            <h5>
                <strong>TESTIMONIALS SECTION</strong>
            </h5>
            <p>
                These are the comments left by Users after rating a course. These comments appear
                and loop on the Sign-in / Home Page (signed out state) and User's Home Page (signed
                in state)
            </p>
        </div>
        <div class="inner-content col-md-5">
            <h5>
                <p class="teal bold">
                    Testimonial Current playlist
                </p>
            </h5>
            <div id="main-content" style="width: 32vw; height: 40vh" class="bottom-margin-60vh">
                <div id="testimonialCarousel" class="tc-slick-carousel-testimonial gray-border hover-pointer"
                    style="position: relative; left: 50%; transform: translateX(-50%);">
                    <div class="item" style="margin-bottom: 30px;">
                        <div class="col-md-3">
                            <img src="../default-user.png" alt="Alternate Text" class="img-circle border-gray-with-padding pull-left"
                                width="120px" height="120px" />
                            <h5>
                                <strong>Name:</strong> 1
                            </h5>
                            <h5>
                              <asp:Label runat="server" ID="rol" Text= "<%$ Resources:LocalizedResource, Role %>"></asp:Label>:
                            </h5>
                            <h5>
                               <asp:Label runat="server" ID="dept" Text= "<%$ Resources:LocalizedResource, Department %>"></asp:Label>:
                            </h5>
                            <h5>
                               <asp:Label runat="server" ID="progr" Text= "<%$ Resources:LocalizedResource, Program %>"></asp:Label>:
                            </h5>
                        </div>
                        <div class="col-md-8 col-md-offset-1">
                            <p class="testimonial-comment teal">
                                Duis justo ex, venenatis id lobortis ac, maximus at lectus.
                            </p>
                        </div>
                    </div>
                    <div class="item" style="margin-bottom: 30px;">
                        <div class="col-md-3">
                            <img src="../default-user.png" alt="Alternate Text" class="img-circle border-md-gray pull-left"
                                width="115px" height="115px" />
                            <h5>
                                <strong><asp:Label runat="server" ID="Names" Text= "<%$ Resources:LocalizedResource, Name %>"></asp:Label>:</strong> 2
                            </h5>
                            <h5>
                               <asp:Label runat="server" ID="Rolesss" Text= "<%$ Resources:LocalizedResource, Role %>"></asp:Label> :
                            </h5>
                            <h5>
                               <asp:Label runat="server" ID="depsst" Text= "<%$ Resources:LocalizedResource, Department %>"></asp:Label> :
                            </h5>
                            <h5>
                               <asp:Label runat="server" ID="progrsss" Text= "<%$ Resources:LocalizedResource, Program %>"></asp:Label> :
                            </h5>
                        </div>
                        <div class="col-md-8 col-md-offset-1">
                            <p class="testimonial-comment teal">
                                Duis justo ex, venenatis id lobortis ac, maximus at lectus.
                            </p>
                        </div>
                    </div>
                    <div class="item" style="margin-bottom: 30px;">
                        <div class="col-md-3">
                            <img src="../default-user.png" alt="Alternate Text" class="img-circle border-md-gray pull-left"
                                width="115px" height="115px" />
                            <h5>
                                <strong><asp:Label runat="server" ID="namesss1" Text= "<%$ Resources:LocalizedResource, Name %>"></asp:Label>:</strong> 3
                            </h5>
                            <h5>
                               <asp:Label runat="server" ID="Rolessses" Text= "<%$ Resources:LocalizedResource, Role %>"></asp:Label> :
                            </h5>
                            <h5>
                               <asp:Label runat="server" ID="departmentssas" Text= "<%$ Resources:LocalizedResource, Department %>"></asp:Label> :
                            </h5>
                            <h5>
                                <asp:Label runat="server" ID="programssssaa" Text= "<%$ Resources:LocalizedResource, Program %>"></asp:Label>:
                            </h5>
                        </div>
                        <div class="col-md-8 col-md-offset-1">
                            <p class="testimonial-comment teal">
                                Duis justo ex, venenatis id lobortis ac, maximus at lectus.
                            </p>
                        </div>
                    </div>
                    <!-- Carousel nav -->
                    <%--<a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
                    <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>--%>
                </div>
                <p class="">
                    Double click on the slider to edit and configure the play settings
                </p>
            </div>
        </div>
    </div>
</asp:Panel>
<asp:HiddenField ID="hfSelectedPlaylistID" runat="server" />
<asp:HiddenField ID="hfSelectedPlaylistName" runat="server" />
<asp:HiddenField ID="hfLoopDirection" runat="server" />
<asp:HiddenField ID="hfSelectedCommentID" runat="server" />
<asp:HiddenField runat="server" ID ="hndradalerterrorn" Value="ERROR \n" /> 
<asp:HiddenField runat="server" ID ="hndradalerterror" Value="Error" /> 
<asp:HiddenField runat="server" ID ="hndradalertplayname" Value="Playlist Name is Not Valid. Please try again." /> 
<asp:HiddenField runat="server" ID ="hndradalaertplayremove" Value="Playlist Removed" />
<asp:HiddenField runat="server" ID ="hndradalertaddsuccess" Value="Playlist added successfully" />  
<asp:HiddenField runat="server" ID ="hndradalertsuccess" Value="Success" /> 
<asp:HiddenField runat="server" ID ="hndradalertremovecomment" Value="Comment Removed" /> 
<asp:HiddenField runat="server" ID ="hndradalertsuccaddcomment" Value="Comment successfully Added." /> 
<asp:HiddenField runat="server" ID ="hndradalertnewsaveset" Value="New settings have been saved." /> 
<asp:HiddenField runat="server" ID ="hndradalaertsaveds" Value="Saved." /> 
<asp:HiddenField runat="server" ID ="hndradconfirmaddplay" Value="Are you sure you want to add this Comment in the playlist" /> 
<asp:HiddenField runat="server" ID ="hndradconfirmadd" Value="Confirm Add" /> 

