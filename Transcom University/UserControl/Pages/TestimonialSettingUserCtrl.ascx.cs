﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TranscomUniversityV3Model;
using Telerik.Web.UI;

public partial class UserControl_Pages_TestimonialSettingUserCtrl : System.Web.UI.UserControl
{
    private int commentsOrder = 1;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            loadDropDownDefault();
            loadPreviousSettings();
            //loadComments(commentsOrder);
        }
    }

    void loadPreviousSettings()
    {
        var settings = DataHelper.getTestimonialSettings();

        int maxTestimonies;
        int imageInterval;
        int loopDirection;
        int selectedTestimonialPlaylist;

        try
        {
            if (settings == null)
            {
                maxTestimonies = 1;
                imageInterval = 1;
                loopDirection = 1;
                selectedTestimonialPlaylist = 0;

            }
            else
            {
                maxTestimonies = settings.MaxItems;
                imageInterval = settings.ImagePlayInterval;
                loopDirection = settings.LoopDirection;
                selectedTestimonialPlaylist = settings.SelectedTestimonialID;
            }

            ddlMaxItem.SelectedIndex = ddlMaxItem.FindItemByValue(maxTestimonies.ToString()).Index;
            ddlInterval.SelectedIndex = ddlInterval.FindItemByValue(imageInterval.ToString()).Index;

            hfLoopDirection.Value = loopDirection.ToString();

            if (loopDirection == 1)
            {
                arrowLeft.Attributes.Add("class", "fa fa-arrow-circle-left pointer active");
            }
            else if (loopDirection == 2)
            {
                arrowRight.Attributes.Add("class", "fa fa-arrow-circle-right pointer active");
            }
            else if (loopDirection == 3)
            {
                arrowUp.Attributes.Add("class", "fa fa-arrow-circle-up pointer active");
            }
            else
            {
                arrowDown.Attributes.Add("class", "fa fa-arrow-circle-down pointer active");
            }

            var playlistDetails = DataHelper.getTestimonialPlaylistDetail(selectedTestimonialPlaylist);
            string playlistName = "No Playlist Selected";
            if (playlistDetails != null)
                playlistName = playlistDetails.Title;
            hfSelectedPlaylistName.Value = playlistName;
            lblCurrentPlaylist.Text = playlistName;
            hfSelectedPlaylistID.Value = Utils.Encrypt(selectedTestimonialPlaylist);

        }
        catch (Exception e)
        {
            rwm.RadAlert(e.Message, 330, 180, "Error", "");
        }
    }

    void loadDropDownDefault()
    {
        //20 items
        for (int i = 1; i <= 20; i++)
        {
            DropDownListItem item = new DropDownListItem(i.ToString(), i.ToString());
            ddlMaxItem.Items.Add(item);
            DropDownListItem item2 = new DropDownListItem(i.ToString(), i.ToString());
        }
        //60 items
        for (int i = 1; i <= 60; i++)
        {
            DropDownListItem item = new DropDownListItem(i.ToString(), i.ToString());
            ddlInterval.Items.Add(item);
        }
    }

    void loadComments(int index) {
 
        using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        {
            try
            {   
                dynamic comments = null;
                if(index == 1){
                    DateTime dt1 = DateTime.Now.AddHours(-12);
                    comments = db.vw_TranscomUniversity_CourseComments
                                    .Where(c => c.DateCreated > dt1)
                                    .ToList();
                    
                }else if(index == 2){
                    DateTime dt1 = DateTime.Now.AddHours(-24);
                    comments = db.vw_TranscomUniversity_CourseComments
                                    .Where(c => c.DateCreated > dt1)
                                    .ToList();
                  
                }else if(index == 3){
                    DateTime dt1 = DateTime.Now.AddDays(-7);
                    comments = db.vw_TranscomUniversity_CourseComments
                                    .Where(c => c.DateCreated > dt1)
                                    .ToList();
                   
                }else if(index == 4){
                   DateTime dt1 = DateTime.Now.AddMonths(-1);
                    comments = db.vw_TranscomUniversity_CourseComments
                                    .Where(c => c.DateCreated > dt1)
                                    .ToList();
                   
                }else if(index == 5){
                   DateTime dt1 = DateTime.Now.AddMonths(-6);
                    comments = db.vw_TranscomUniversity_CourseComments
                                    .Where(c => c.DateCreated > dt1)
                                    .ToList();
                    
                }else if(index == 6){
                   DateTime dt1 = DateTime.Now.AddYears(-1);
                    comments = db.vw_TranscomUniversity_CourseComments
                                    .Where(c => c.DateCreated > dt1)
                                    .ToList();
                }

                rdComments.DataSource = comments;
                rdComments.DataBind();
            }
            catch(Exception e)
            {
                rwm.RadAlert(e.Message, 330, 180, "Error", "");
            }
        }
        
    }

    protected void ddlCommentSort_IndexChanged(object sender, DropDownListEventArgs e)
    {

        commentsOrder = e.Index;

        loadComments(commentsOrder);

    }
}