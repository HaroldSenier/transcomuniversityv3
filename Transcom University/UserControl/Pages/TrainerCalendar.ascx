﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TrainerCalendar.ascx.cs"
    Inherits="TrainerCalendar" %>

<%@ Register TagName="EventDetails" TagPrefix="ucEventDetails" Src="~/UserControl/Pages/CalendarEventDetailsUserCtrl.ascx" %>

<rad:RadCodeBlock ID="rcbCalendar" runat="server" >
    <script type="text/javascript">
        var calendarID = 0;
        function backToCalendar() {
            $("#<%= pnlEventDetails.ClientID %>").addClass("display-none");
            $("#<%= pnlCalendar.ClientID %>").removeClass("display-none");
        }

        function MyCalendar_OnAppointmentClick(sender, args) {
            console.log(sender);
            console.log(args);
            var apt =  args.get_appointment();
            calendarID = apt.get_id();
            loadEventDetails();
            
        }

        function showEventDetails() {
            $("#<%= pnlEventDetails.ClientID %>").removeClass("display-none");
            $("#<%= pnlCalendar.ClientID %>").addClass("display-none");
        }
        function loadEventDetails() {
            clearDetails();
            var tz = $("#hfEncryptedOffset").val();
            PageMethods.GetCalendarEventDetails(calendarID, tz,
                function (result) {
                    //console.log(result);

                    if (result != null) {
                        $("#lblTitle").html(result.Title);
                        $("#subjectTitle").html(result.Subject);
                        $("#lblMonth").html(result.CalendarMonth);
                        $("#lblDay").html(result.CalendarDay);
                        $("#lblWhen").html(result.EventWhen);
                        $("#lblTime").html(result.EventTime);
                        $("#lblLocation").html(result.Location);
                        $("#lblTrainer").html(result.TrainerName);
                        $("#lblDesc").html(result.Description);
                        $("#lblAvailableSeats").html(result.AvailableSeats);
                        $("#lblWaitListed").html(result.WaitListed);
                        showEventDetails();
                    } else {
                        var hndradalertnoshowevent = $("#<%=hndradalertnoshowevent.ClientID%>").val();
                        var hndradalerterrormess = $("#<%=hndradalerterrormess.ClientID%>").val();
                        radalert(hndradalertnoshowevent, 330, 180, hndradalerterrormess, "");
                    }


                },
                function (error) {
                    var code = error._statusCode;
                    if (code == 401)
                        location.reload();
                    else if (code == 500)
                        var hndradalertwentwrongsomething = $("#<%=hndradalertwentwrongsomething.ClientID%>").val();
                        var hndradalerterrormess = $("#<%=hndradalerterrormess.ClientID%>").val();
                        radalert(hndradalertwentwrongsomething, 330, 180, hndradalerterrormess, "");
                });
            }

            function clearDetails() {
                $("#lblTitle").html("");
                $("#subjectTitle").html("");
                $("#lblMonth").html("");
                $("#lblDay").html("");
                $("#lblWhen").html("");
                $("#lblTime").html("");
                $("#lblLocation").html("");
                $("#lblTrainer").html("");
                $("#lblDesc").html("");
                $("#lblAvailableSeats").html("");
                $("#lblWaitListed").html("");
            }

    </script>
</rad:RadCodeBlock>
<rad:RadAjaxLoadingPanel ID="localLoadingPanel" runat="server" CssClass="Loading2"
    MinDisplayTime="1000" Transparency="25" />
<rad:RadAjaxManagerProxy ID="rmpCalendar" runat="server">
    <AjaxSettings>
        <rad:AjaxSetting AjaxControlID="MyCalendar">
            <UpdatedControls>
                <%--<rad:AjaxUpdatedControl ControlID="MyCalendar" LoadingPanelID="localLoadingPanel" />--%>
                <rad:AjaxUpdatedControl ControlID="pnlCalendar" LoadingPanelID="localLoadingPanel" />
                <rad:AjaxUpdatedControl ControlID="pnlEventDetails" LoadingPanelID="localLoadingPanel" />
            </UpdatedControls>
        </rad:AjaxSetting>
    </AjaxSettings>
</rad:RadAjaxManagerProxy>
<%--   OnClientAppointmentWebServiceInserting="OnClientAppointmentWebServiceInserting"
    OnClientNavigationComplete="OnClientNavigationComplete" OnClientAppointmentsPopulating="OnClientAppointmentsPopulating"--%>

<div class="container calendar-container height-fit">

<asp:Panel runat="server" ID="pnlCalendar">
    <rad:RadScheduler RenderMode="Lightweight" runat="server" ID="MyCalendar" Height="100%"
        ForeColor="Black" OnClientAppointmentClick="MyCalendar_OnAppointmentClick" RowHeight="30px"
        OverflowBehavior="Expand" SelectedView="WeekView" ShowFooter="false" DayStartTime="00:00:00"
        DayEndTime="23:59:00" FirstDayOfWeek="Sunday" LastDayOfWeek="Saturday" EnableDescriptionField="true"
        AppointmentStyleMode="Default" Skin="Metro" AllowInsert="false" AllowDelete="false" AllowEdit="false" BackColor="Transparent" >
        <AppointmentTemplate>
            <div class="calendarAppointment">
                <div class="appointmentHeader">
                    <asp:Panel ID="RecurrencePanel" CssClass="rsAptRecurrence" runat="server" Visible="false">
                    </asp:Panel>
                    <asp:Panel ID="RecurrenceExceptionPanel" CssClass="rsAptRecurrenceException" runat="server"
                        Visible="false">
                    </asp:Panel>
                    <asp:Panel ID="ReminderPanel" CssClass="rsAptReminder" runat="server" Visible="false">
                    </asp:Panel>
                </div>
                <div>
                    <%# string.Format("{0: hh:mm tt}", Eval("Start")) + " - " + string.Format("{0: hh:mm tt}", Eval("End")) %>
                    <%# Container.Appointment.Resources.GetResourceByType("Type").Text%>
                    <%# Eval("Subject") %>
                </div>
            </div>
        </AppointmentTemplate>
        <AdvancedForm Modal="true"></AdvancedForm>
        <TimelineView UserSelectable="false"></TimelineView>
        <AgendaView UserSelectable="true" />
        <ResourceTypes>
            <rad:ResourceType KeyField="EventTypeID" Name="Type" TextField="EventType" ForeignKeyField="EventTypeID">
            </rad:ResourceType>
        </ResourceTypes>
        <ResourceStyles>
            <rad:ResourceStyleMapping Type="Type" Key="1" ApplyCssClass="eventtype-violet"></rad:ResourceStyleMapping>
            <rad:ResourceStyleMapping Type="Type" Key="2" ApplyCssClass="eventtype-green"></rad:ResourceStyleMapping>
            <rad:ResourceStyleMapping Type="Type" Key="3" ApplyCssClass="eventtype-gray"></rad:ResourceStyleMapping>
        </ResourceStyles>
        <%-- <TimeSlotContextMenuSettings EnableDefault="true"/>--%>
        <%--<AppointmentContextMenuSettings EnableDefault="true" />--%>
        <%--    <Localization HeaderWeek="Work week" />--%>
    </rad:RadScheduler>
    
   <asp:Label runat="server" ID="codesevents" Text= "<%$ Resources:LocalizedResource, EventCodes %>"></asp:Label>:
    <br />
    <div class="row display-inline-flex">
        
        &nbsp;&nbsp;&nbsp;&nbsp;

        <div class="fill-purple box-sm">
        </div>
        &nbsp;<asp:Label runat="server" ID="sesavail" Text= "<%$ Resources:LocalizedResource, AvailableSessions %>"></asp:Label>&nbsp;&nbsp;&nbsp;
        <div class="fill-green box-sm">
        </div>
        &nbsp;<asp:Label runat="server" ID="regses" Text= "<%$ Resources:LocalizedResource, RegisteredSessions %>"></asp:Label>&nbsp;&nbsp;&nbsp;
        <div class="fill-gray box-sm">
        </div>
        &nbsp;<asp:Label runat="server" ID="concses" Text= "<%$ Resources:LocalizedResource, ConcludedSessions %>"></asp:Label>&nbsp;&nbsp;&nbsp;
    </div>
    <asp:SqlDataSource ID="EventTypeDS" runat="server" ProviderName="System.Data.SqlClient"
        ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM [ref_TranscomUniversity_CalendarEventType]">
    </asp:SqlDataSource>
</asp:Panel>


<asp:Panel runat="server" ID="pnlEventDetails" CssClass="display-none bottom-margin-60vh">
<asp:Panel ID="pnlDetails" runat="server">
    <h3 class="text-center bold" id="subjectTitle">
      <asp:Label runat="server" ID="SubjectTitle" Text= "<%$ Resources:LocalizedResource, SubjectTitle %>"></asp:Label>
    </h3>
    <br />
    <br />
    <br />
    <div class="row">
        <div class="col-md-3 col-md-offset-1">
            <p id="lblTitle" class="teal"></p>
        </div>
    </div>
    <br />
    <div class="row">
        <div class="col-md-3 col-md-offset-1">
            <%--<asp:Image ID="Image1" ImageUrl="default-user.png" runat="server" />--%>
            <div class="bigCalendarDate">
                <div class="innerDiv">
                    <div class="monthHolder">
                        <label id="lblMonth"></label>
                    </div>
                    <div class="dayHolder">
                        <label id="lblDay"></label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-3">
                   <asp:Label runat="server" ID="whens" Text= "<%$ Resources:LocalizedResource, When %>"></asp:Label>:
                </div>
                <div class="col-md-9">
                    <label id="lblWhen"></label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <asp:Label runat="server" ID="timess" Text= "<%$ Resources:LocalizedResource, Time %>"></asp:Label>:
                </div>
                <div class="col-md-9">
                    <label id="lblTime"></label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <asp:Label runat="server" ID="localtion" Text= "<%$ Resources:LocalizedResource, Location %>"></asp:Label>:
                </div>
                <div class="col-md-9">
                    <label id="lblLocation"></label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <asp:Label runat="server" ID="trains" Text= "<%$ Resources:LocalizedResource, Trainer %>"></asp:Label>:
                </div>
                <div class="col-md-9">
                    <label id="lblTrainer"></label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <asp:Label runat="server" ID="coursedesc" Text= "<%$ Resources:LocalizedResource, CourseDescription %>"></asp:Label>:
                </div>
                <div class="col-md-9">
                     <label id="lblDesc"></label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                   <asp:Label runat="server" ID="seatavail" Text= "<%$ Resources:LocalizedResource, AvailableSeats %>"></asp:Label>:
                </div>
                <div class="col-md-9">
                    <label id="lblAvailableSeats"></label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                   <asp:Label runat="server" ID="listwait" Text= "<%$ Resources:LocalizedResource, Waitlisted %>"></asp:Label>:
                </div>
                <div class="col-md-9">
                    <label id="lblWaitListed"></label>
                </div>
            </div>
        </div>
    </div>
    <hr />
</asp:Panel>

 <%--   <ucEventDetails:EventDetails ID="EventDetails1" runat="server" />--%>
    <asp:Button ID="btnBackToCalendar" OnClientClick="backToCalendar(); return false;" Text="Back to Calendar" runat="server" CssClass="btn btn-sm btn-teal pull-right" />
</asp:Panel>

</div>
<asp:HiddenField runat="server" ID ="hndradalertnoshowevent" Value="No event details to show!" /> 
<asp:HiddenField runat="server" ID ="hndradalerterrormess" Value="Error Message" /> 
<asp:HiddenField runat="server" ID ="hndradalertwentwrongsomething" Value="Something went wrong while viewing event. Please try again." /> 
