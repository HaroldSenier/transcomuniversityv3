﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using TranscomUniversityV3Model;
using System.Drawing;

public partial class TrainerCalendar : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //Appointment am = new Appointment();
            //am.Subject = "Test";
            //am.Start = DateTime.Now;
            //am.End = DateTime.Now.AddHours(3.0);
            //am.ID = "test1";

            //RadScheduler1.DataKeyField = am.Subject;
            //RadScheduler1.DataSubjectField = "";
            //RadScheduler1.DataStartField = "";
            //RadScheduler1.DataEndField = "";
            //RadScheduler1.InsertAppointment(am);

            var db = new TranscomUniversityV3ModelContainer();
            var Cim = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
            //int? Cim = 95759;
            //Cim = 95759;
            //var calendar = db.vw_TranscomUniversity_MyCalendar
            //                .Where(c => c.UserCim == Cim)
            //                .ToList();
            var calendar = db.pr_TranscomUniversity_Cor_Calendar_Trainer(Cim).ToList();
            var eventType = db.ref_TranscomUniversity_CalendarEventType.ToList();


            string tz = Request.QueryString["tz"];

            int decryptTz = Convert.ToInt32(Utils.Decrypt(tz));

            var offset = new TimeSpan(decryptTz / 60, decryptTz % 60, 0);

            foreach (TranscomUniversityV3Model.pr_TranscomUniversity_Cor_Calendar_Trainer_Result c in calendar)
            {
                c.CreateDate = (c.CreateDate != null) ? (DateTime)c.CreateDate.Value - offset : c.CreateDate;
                c.EndTime = (c.EndTime != null) ? (DateTime)c.EndTime.Value - offset : c.EndTime;
                c.StartTime = (c.StartTime != null) ? (DateTime)c.StartTime.Value - offset : c.StartTime;
                c.updateDate = (c.updateDate != null) ? (DateTime)c.updateDate.Value - offset : c.updateDate;
            }

            //ResourceTypeCollection resCollection = new ResourceTypeCollection();
            //ResourceType resType = new ResourceType("Name");
            //resType.ForeignKeyField = "EventTypeID";
            //resType.KeyField = "Id";
            //resType.TextField = "EventType";
            //resType.DataSource = eventType;
            //resCollection.Add(resType);
            //MyCalendar.Resources.Add(resCollection);
            MyCalendar.ResourceTypes[0].DataSource = eventType;
            MyCalendar.SelectedDate = DataHelper.serverDate() - offset;
            MyCalendar.DataKeyField = "CalendarID";
            MyCalendar.DataSubjectField = "Subject";
            MyCalendar.DataStartField = "StartTime";
            MyCalendar.DataEndField = "EndTime";
            //MyCalendar.TimeZoneOffset = offset;
            MyCalendar.DataSource = calendar;
            MyCalendar.DataBind();
        }
        
    }

    protected void MyCalendar_OnAppointmentClick(object sender, SchedulerEventArgs e) 
    {
       // Response.Redirect("MyLearningPlan.aspx");
        var key = e.Appointment.ID;
        pnlCalendar.CssClass = "display-none";
        pnlEventDetails.CssClass = "";
    }

}