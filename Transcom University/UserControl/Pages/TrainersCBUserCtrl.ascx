﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TrainersCBUserCtrl.ascx.cs"
    Inherits="TrainersCBUserCtrl" %>
<asp:Panel ID="pnlMainTrainerUC" runat="server" ClientIDMode="Static">
    <rad:RadScriptBlock ID="RadScriptBlock1" runat="server">
        <script type="text/javascript">
            function assignState(e, trainerCim, trainerName) {
                $("#" + "<%= hfTrainerCIM.ClientID %>").val(trainerCim);
                $("#" + "<%= hfTrainerName.ClientID %>").val(trainerName);

                var radwindow = $find('<%=rwAssignState.ClientID %>');
                radwindow.show();
            }

            function setRadWindowTitle(sender) {
                var hfValue = $("#" + "<%= hfTrainerName.ClientID %>").val();

                if (hfValue) {
                    sender.set_title(hfValue);
                }
                else {
                    sender.set_title("Name");
                }
            }

            function insertTrainerSuccess() {
                $find('<%=rwAssignState.ClientID %>').close();
                var hndradalerttrainersuc = $("#<%=hndradalerttrainersuc.ClientID%>").val();
                var hndradalertsuccess = $("#<%=hndradalertsuccess.ClientID%>").val();
                radalert(hndradalerttrainersuc, 330, 180, hndradalertsuccess, "");
            }

            function removeTrainer(e, trainerCim, trainerName) {
                var callBackFunction = function (shouldSubmit) {
                    if (shouldSubmit) {
                        $("#<%= btnRemoveTrainer.ClientID %>").trigger('click');
                        

                    }
                };

                $("#" + "<%= hfTrainerCIM.ClientID %>").val(trainerCim);
                $("#" + "<%= hfTrainerName.ClientID %>").val(trainerName);
                var radconfirmsurewantto = $("#<%=radconfirmsurewantto.ClientID%>").val();
                var radconfirmfirm = $("#<%=radconfirmfirm.ClientID%>").val();
                radconfirm(radconfirmsurewantto + trainerName + " ?", callBackFunction, 330, 180, null, radconfirmfirm);
                return false;

            }
            
        </script>
    </rad:RadScriptBlock>
    <rad:RadAjaxManagerProxy ID="RadAjaxManagerProxy2" runat="server">
        <AjaxSettings>
            <%--<rad:AjaxSetting AjaxControlID="gridTrainers">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="gridTrainers" />
                </UpdatedControls>
            </rad:AjaxSetting>--%>
            <rad:AjaxSetting AjaxControlID="btnAssignState">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlAssignState" LoadingPanelID="RadAjaxLoadingPanel2" />
                    <rad:AjaxUpdatedControl ControlID="ltTrainers" />
                    <rad:AjaxUpdatedControl ControlID="gridTrainers" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnRemoveTrainer">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="gridTrainers" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <rad:AjaxUpdatedControl ControlID="ltTrainers" />
                </UpdatedControls>
            </rad:AjaxSetting>
        </AjaxSettings>
    </rad:RadAjaxManagerProxy>
    <rad:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true" RenderMode="Lightweight"
        Skin="Bootstrap">
        <Windows>
            <rad:RadWindow ID="rwAssignState" runat="server" RenderMode="Lightweight" Width="450px"
                Height="300px" Behaviors="Move, Close" Modal="true" OnClientShow="setRadWindowTitle"
                CssClass="tc-radwindow-1 height-inherit" VisibleStatusbar="false">
                <ContentTemplate>
                    <asp:Panel ID="pnlAssignState" runat="server">
                        <div class="col-md-6">
                            <asp:Label runat="server" ID="SelectAssignState" Text= "<%$ Resources:LocalizedResource, SelectAssignState %>"></asp:Label>
                        </div>
                        <br />
                        <div id="divRadio" runat="server" style="margin-left: 5%; font-size: small;">
                            <asp:RadioButtonList ID="rblAssignState" runat="server">
                                <asp:ListItem Value="1" Text="<%$ Resources:LocalizedResource, LeadTrainer %>r"></asp:ListItem>
                                <asp:ListItem Value="2" Text="<%$ Resources:LocalizedResource, BackupTrainer %>"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                        <p align="center" style="margin-top: 10px; margin-bottom: 10px">
                            - or -</p>
                        <div class="col-md-6">
                            <asp:Label runat="server" ID="transtrain" Text= "<%$ Resources:LocalizedResource, TransferTrainer %>"></asp:Label>
                        </div>
                        <br />
                        <div id="divCombo" runat="server" style="margin-left: 5%;">
                            <rad:RadComboBox ID="rcbTransferTrainer" runat="server" DataSourceID="dsTransferTrainer"
                                DataValueField="CourseID" DataTextField="Title">
                            </rad:RadComboBox>
                            <asp:SqlDataSource ID="dsTransferTrainer" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
                                SelectCommand="SELECT 0 AS CourseID, '' AS Title UNION ALL SELECT CourseID, Title FROM tbl_TranscomUniversity_Cor_Course WHERE EndDate >= GETDATE() AND HideFromList = 0 ORDER BY 2 ASC"
                                SelectCommandType="Text" />
                        </div>
                        <br />
                        <div class="form-group" style="margin-left: 80%;">
                            <asp:Button ID="btnAssignState" runat="server" OnClick="btnAssignState_Click" CssClass="btn btn-md btn-teal btn-flat"
                                Text="<%$ Resources:LocalizedResource, Assigned %>" />
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </rad:RadWindow>
        </Windows>
    </rad:RadWindowManager>
    <div class="col-md-12">
        <div class="CourseBuilder-Settings">
            <div class="row">
                <div class="container">
                    <span>
                        <asp:Label runat="server" ID="lblNumberOfGroup" ForeColor="Teal" Font-Bold="true"
                            Font-Size="Small" CssClass="display-inline-flex"><asp:Label runat="server" ID="theare" Text= "<%$ Resources:LocalizedResource, ThereAre %>"></asp:Label>&nbsp;
                            <asp:Literal ID="ltTrainers" runat="server"></asp:Literal>&nbsp;<asp:Label runat="server" ID="desigcour" Text= "<%$ Resources:LocalizedResource, DesignatedTrainersToThisCourse %>"></asp:Label> </asp:Label></span>
                </div>
                <rad:RadGrid ID="gridTrainers" runat="server" RenderMode="Lightweight" AutoGenerateColumns="false"
                    PageSize="10" OnNeedDataSource="gridTrainers_NeedDataSource" CssClass="GridLess"
                    Height="500px">
                    <PagerStyle Mode="NextPrev" AlwaysVisible="true" />
                    <ClientSettings EnableAlternatingItems="true">
                        <Selecting AllowRowSelect="true"></Selecting>
                        <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                        <Resizing AllowColumnResize="true" ResizeGridOnColumnResize="true" AllowResizeToFit="true" />
                    </ClientSettings>
                    <ItemStyle Wrap="false"></ItemStyle>
                    <MasterTableView DataKeyNames="TrainerCIM" AllowSorting="true" AllowPaging="true"
                        CommandItemDisplay="None" HeaderStyle-ForeColor="Teal">
                        <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                        <Columns>
                            <rad:GridTemplateColumn HeaderText="#" HeaderStyle-Width="5%">
                                <ItemTemplate>
                                    <%# Container.ItemIndex+1 %>
                                </ItemTemplate>
                            </rad:GridTemplateColumn>
                            <rad:GridBoundColumn DataField="TrainerCIM" HeaderText="<%$ Resources:LocalizedResource, CimNumber %>" UniqueName="TrainerCIM">
                            </rad:GridBoundColumn>
                            <rad:GridBoundColumn DataField="Name" HeaderText="<%$ Resources:LocalizedResource, Name %>" UniqueName="Name">
                            </rad:GridBoundColumn>
                            <rad:GridBoundColumn DataField="Campaign" HeaderText="<%$ Resources:LocalizedResource, Campaign %>" UniqueName="Campaign">
                            </rad:GridBoundColumn>
                            <rad:GridBoundColumn DataField="Supervisor" HeaderText="<%$ Resources:LocalizedResource, Supervisor %>" UniqueName="Supervisor">
                            </rad:GridBoundColumn>
                            <rad:GridBoundColumn DataField="AssignState" HeaderText="<%$ Resources:LocalizedResource, AssignState %>" UniqueName="AssignState">
                            </rad:GridBoundColumn>
                            <rad:GridTemplateColumn HeaderStyle-Width="8%" HeaderText="<%$ Resources:LocalizedResource, Action %>" HeaderStyle-HorizontalAlign="Center" >
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnAssignState" runat="server" OnClientClick='<%# "assignState(event," + Eval("TrainerCIM") + ",\""+  Eval("Name") + "\")" %>' CssClass="pull-left">
                                    <i class="fa fa-user" title="Assign State" aria-hidden="true"></i>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnFakeRemoveTrainer" runat="server" OnClientClick='<%# "removeTrainer(event," + Eval("TrainerCIM") + ",\""+  Eval("Name") + "\"); return false;" %>' CssClass="pull-right">
                                        <i class="fa fa-close" title="Remove Trainer" aria-hidden="true"></i>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </rad:GridTemplateColumn>
                        </Columns>
                    </MasterTableView>
                </rad:RadGrid>
                <br />
            </div>
        </div>
    </div>
    <asp:Button ID="btnRemoveTrainer" runat="server" OnClick="btnRemoveTrainer_Click"
        CssClass="display-none"></asp:Button>
    <asp:HiddenField ID="hfTrainerCIM" runat="server" />
    <asp:HiddenField ID="hfTrainerName" runat="server" />
</asp:Panel>

<asp:HiddenField runat="server" ID ="hndradalerttrainersuc" Value="Trainer Successfully Transferred." /> 
<asp:HiddenField runat="server" ID ="hndradalertsuccess" Value="Success" /> 
<asp:HiddenField runat="server" ID ="radconfirmsurewantto" Value="Are you sure you want to remove" /> 
<asp:HiddenField runat="server" ID ="radconfirmfirm" Value="Confirm" /> 
