﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using TranscomUniversityV3Model;

public partial class TrainersCBUserCtrl : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
       
            Page.Form.Attributes.Add("enctype", "multipart/form-data");

            if (!(System.Web.Security.Roles.IsUserInRole(HttpContext.Current.User.Identity.Name.Split('|')[0], "Admin") || System.Web.Security.Roles.IsUserInRole(HttpContext.Current.User.Identity.Name.Split('|')[0], "Trainer")))
                Response.Redirect("~/Unauthorized.aspx");

            //gridTrainers.Rebind();
           
    }

    protected void gridTrainers_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        var courseId = Convert.ToInt32(DataHelper.Decrypt(Request.QueryString["CourseID"]));
  
            gridTrainers.DataSource = DataHelper.GetAllTrainers(courseId);
            GetTrainersCount();       
    }

    protected void btnAssignState_Click(object sender, EventArgs e)
    {
        int _actionID = DataHelper.getLastLogID() + 1;
        string _userIP = DataHelper.GetIPAddress();
        string _userID = DataHelper.GetCurrentUserCIM();
        double _duration = 0;

        DateTime startTime;
        DateTime endTime;

        try
        {
            int targetCourseID = Convert.ToInt32(rcbTransferTrainer.SelectedValue);
            var courseId = Convert.ToInt32(DataHelper.Decrypt(Request.QueryString["CourseID"]));
            var trainerCim = Convert.ToInt32(hfTrainerCIM.Value);

            if (rblAssignState.SelectedValue != "" && rcbTransferTrainer.SelectedItem.Text == "")
            {
               

                var db = new TranscomUniversityV3ModelContainer();
                var dts = db.CreateQuery<DateTime>("CurrentDateTime() ");
                startTime = dts.AsEnumerable().First();

                db.pr_TranscomUniversity_UpdateTrainerAssignState(Convert.ToInt32(courseId), Convert.ToInt32(trainerCim), Convert.ToInt32(rblAssignState.SelectedValue));

                string _action = "Assigned " + DataHelper.getUserName(trainerCim) + " as " + getAssignState(Convert.ToInt32(rblAssignState.SelectedValue));
                var dte = db.CreateQuery<DateTime>("CurrentDateTime() ");

                endTime = dte.AsEnumerable().First();
                _duration = (endTime - startTime).TotalMilliseconds;

                int l_actionID = DataHelper.getLastLogID() + 1;
                DataHelper.logCourse(courseId, l_actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);

                rblAssignState.ClearSelection();
                gridTrainers.Rebind();
                GetTrainersCount();

                RadWindowManager1.RadAlert("Trainer successfully assigned.", 330, 180, "System Message", "");
            }
            else if (rcbTransferTrainer.SelectedItem.Text != "" && rblAssignState.SelectedValue == "")
            {
                var db = new TranscomUniversityV3ModelContainer();
                pr_TranscomUniversity_TransferTrainerAssignState_Result retVal = db.pr_TranscomUniversity_TransferTrainerAssignState(courseId, Convert.ToInt32(trainerCim), targetCourseID).SingleOrDefault();

                if (retVal.RESPONSE == "2")
                {
                    RadWindowManager1.RadAlert("Trainer already exists in the target course", 330, 180, "System Message", "");
                    return;
                }
                else if (retVal.RESPONSE == "1")
                {
                    rcbTransferTrainer.ClearSelection();
                    rblAssignState.ClearSelection();
                    gridTrainers.Rebind();
                    GetTrainersCount();

                    RadScriptManager.RegisterStartupScript(Page, Page.GetType(), "insertTrainerSuccess", Utils.callClientScript("insertTrainerSuccess"), true);
                }
                else
                {
                    RadWindowManager1.RadAlert("Failed. Please Check if the Trainer is added in the UserInfo", 330, 180, "Error Message", "");
                    return;
                }
                //if (DataHelper.isTrainer(targetCourseID, trainerCim))
                //{
                //    RadWindowManager1.RadAlert("Trainer already exists in the target course", 330, 180, "System Message", "");
                //    return;
                //}
                //else
                //{
                //    var db = new TranscomUniversityV3ModelContainer();
                //    db.pr_TranscomUniversity_TransferTrainerAssignState(targetCourseID, Convert.ToInt32(trainerCim), courseId);

                //    rcbTransferTrainer.ClearSelection();
                //    rblAssignState.ClearSelection();
                //    gridTrainers.Rebind();
                //    GetTrainersCount();


                //    RadScriptManager.RegisterStartupScript(Page, Page.GetType(), "insertTrainerSuccess", Utils.callClientScript("insertTrainerSuccess"), true);
                //    //RadWindowManager1.RadAlert("Trainer successfully transfered.", 330, 180, "System Message", "");
                //}

              
               
            }
            else
            {
                rcbTransferTrainer.ClearSelection();
                rblAssignState.ClearSelection();

                RadWindowManager1.RadAlert("Please select one state only for this user.", 330, 180, "System Message", "");
            }
        }
        catch
        {
            RadWindowManager1.RadAlert("Error assigning state. Please contact your System Administrator.", 330, 180, "Error Message", "");
        }
    }

    protected void btnRemoveTrainer_Click(object sender, EventArgs e)
    {
        int _actionID = DataHelper.getLastLogID() + 1;
        string _userIP = DataHelper.GetIPAddress();
        string _userID = DataHelper.GetCurrentUserCIM();
        double _duration = 0;

        DateTime startTime;
        DateTime endTime;

        try
        {
            var courseId = Convert.ToInt32(DataHelper.Decrypt(Request.QueryString["CourseID"]));
            var trainerCim = Convert.ToInt32(hfTrainerCIM.Value);
            string trainerName = hfTrainerName.Value.Trim();
           

                var db = new TranscomUniversityV3ModelContainer();
                var dts = db.CreateQuery<DateTime>("CurrentDateTime() ");
                startTime = dts.AsEnumerable().First();

                db.pr_TranscomUniversity_RemoveCourseTrainer(Convert.ToInt32(trainerCim), Convert.ToInt32(courseId), Convert.ToInt32(_userID));

                string _action = "Removed Trainer " + trainerName + ".";
                var dte = db.CreateQuery<DateTime>("CurrentDateTime() ");

                endTime = dte.AsEnumerable().First();
                _duration = (endTime - startTime).TotalMilliseconds;

                int l_actionID = DataHelper.getLastLogID() + 1;
                DataHelper.logCourse(courseId, l_actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);

                gridTrainers.Rebind();
                GetTrainersCount();


                RadWindowManager1.RadAlert("Trainer " + trainerName + " was successfully removed.", 330, 180, "Success", "");
           
        }
        catch
        {
            RadWindowManager1.RadAlert("Error assigning state. Please contact your System Administrator.", 330, 180, "Error", "");
        }
    }


    

    public void GetTrainersCount()
    {
        var db = new TranscomUniversityV3ModelContainer();
        var courseId = Convert.ToInt32(DataHelper.Decrypt(Request.QueryString["CourseID"]));

        var countTrainers = (from t in db.vw_TranscomUniversity_CourseTrainers
                             where t.CourseID == courseId
                             select t).Count().ToString();

        ltTrainers.Text = countTrainers;
    }

    private string getAssignState(int id)
    {
        string state = "";
        switch (id)
        {
            case 0:
                state = " ";
                break;
            case 1:
                state = "Lead Trainer";
                break;
            case 2:
                state = "Backup Trainer";
                break;
            default:
                state = "";
                break;
        }

        return state;
    }
}