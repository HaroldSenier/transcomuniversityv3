﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TrendingCourseSettings.ascx.cs"
    Inherits="UserControl_Pages_TrendingCourseSettings" %>
<rad:RadScriptBlock ID="rsbTrendingCourse" runat="server">
    <script type="text/javascript">

        //vars
        var COURSECATALOG_LISTVIEW;
        var COURSECATALOG_GRIDVIEW;

        var COURSELIBRARY_LISTVIEW;
        var COURSELIBRARY_GRIDVIEW;

        var COURSELIBRARY_DATA;
        var COURSELIBRARY_COUNT;
        var COURSELIBRARY_PAGESIZE;
        var COURSELIBRARY_PAGEINDEX;

        var COURSECATALOG_DATA;
        var COURSECATALOG_COUNT;
        var COURSECATALOG_PAGESIZE;
        var COURSECATALOG_PAGEINDEX;

        var SELECTEDCOURSES_TOADD = [];

        var ISCOURSECATALOGLISTVIEW = false;
        var ISCOURSELIBRARYLISTVIEW = false;

        var COURSECATALOG_LIST_CACHE;
        var COURSECATALOG_GRID_CACHE;

        var SETTINGSCHANGED;


        function pageLoad() {
            initializeSwitchery();
            initializeAccordion();

            SETTINGSCHANGED = false;
            highlightSelectedSubmenu();
        }

        function initializeSwitchery() {
            var elems2 = Array.prototype.slice.call(document.querySelectorAll('.js-switch-transparent'));

            elems2.forEach(function (html) {
                new Switchery(html, {
                    size: 'small',
                    color: 'none',
                    secondaryColor: 'none', //off
                    jackColor: '#09A8A4', //on
                    jackSecondaryColor: '#bbb' //off
                });
            });
            $(".js-switch-transparent").addClass("border-gray");
        }

        function initializeAccordion() {
            $(function () {
                $("#MCListAccordion")
                .accordion({
                    header: "> div > div"
                })
                .sortable({
                    handle: "div",
                    stop: function (event, ui) {
                        ui.item.children("div").triggerHandler("focusout");
                        var i = 1;
                        $("#MCListAccordion").find('input[name*=hfIndex]').each(function (index) {
                            var newIndex = $(this).val(i);
                            i++;
                        });
                    }
                });
            });

            $(function () {
                $("#MCGridAccordion")
                .accordion({
                    header: "> tr > td"
                })
                .sortable({
                    handle: "td",
                    stop: function (event, ui) {
                        ui.item.children("td").triggerHandler("focusout");
                        var i = 1;
                        $("#MCGridAccordion").find('input[name*=hfIndex]').each(function (index) {
                            var newIndex = $(this).val(i);
                            i++;
                        });
                    }
                });

            });
        }

        function openCourseCatalog() {
            if (SETTINGSCHANGED == true || COURSECATALOG_COUNT == 0) {

                COURSECATALOG_LISTVIEW.page(0);
                COURSECATALOG_LISTVIEW.rebind();
                SETTINGSCHANGED = false;
            }

            initializeCourseCatalogListView();
            $find("<%= rwPreviewCourseCatalog.ClientID %>").show();
        }

        function closeCourseCatalog() {
            $find("<%= rwPreviewCourseCatalog.ClientID %>").close();
        }

        function initializeCourseCatalogListView() {
            COURSECATALOG_PAGESIZE = 12;
            COURSECATALOG_LISTVIEW = $find("<%= lvCourseCatalog.ClientID%>");
            COURSECATALOG_LISTVIEW.rebind();
            COURSECATALOG_LISTVIEW.set_pageSize(COURSECATALOG_PAGESIZE);
            COURSECATALOG_LISTVIEW.page(0);

            $(".mcPager .pagePrev").click(function (e) {
                COURSECATALOG_LISTVIEW.page(COURSECATALOG_LISTVIEW.get_currentPageIndex() - 1);
            });

            $(".mcPager .pageNext").click(function (e) {
                COURSECATALOG_LISTVIEW.page(COURSECATALOG_LISTVIEW.get_currentPageIndex() + 1);
            });
        }

        function bindCourseCatalog() {
            showLoading("pnlCourseCatalogContainer");
            COURSECATALOG_LISTVIEW = $find("<%= lvCourseCatalog.ClientID %>");
            COURSECATALOG_GRIDVIEW = $find("<%= gvCourseCatalog.ClientID %>");
            var startRowIndex = COURSECATALOG_LISTVIEW.get_currentPageIndex() * COURSECATALOG_LISTVIEW.get_pageSize(),
                maximumRows = COURSECATALOG_LISTVIEW.get_pageSize(),
                sortExpression = COURSECATALOG_LISTVIEW.get_sortExpressions().toLinq();

            $.ajax({
                type: "POST",
                data: "{startRowIndex: " + startRowIndex + ", maximumRows: " + maximumRows + ", sortExpression: '" + sortExpression + "'}",
                contentType: "application/json; charset=utf-8",
                url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "GetTrendingCourseList",
                dataType: 'json',
                success: function (res) {
                    COURSECATALOG_DATA = res.d.data;
                    COURSECATALOG_COUNT = res.d.count;

                    COURSECATALOG_PAGESIZE = COURSECATALOG_LISTVIEW.get_pageSize();
                    COURSECATALOG_LISTVIEW.set_virtualItemCount(COURSECATALOG_COUNT);
                    COURSECATALOG_LISTVIEW.set_dataSource(COURSECATALOG_DATA);
                    COURSECATALOG_LISTVIEW.dataBind();

                    COURSECATALOG_GRIDVIEW.set_virtualItemCount(COURSECATALOG_COUNT);
                    COURSECATALOG_GRIDVIEW.set_dataSource(COURSECATALOG_DATA);
                    COURSECATALOG_GRIDVIEW.dataBind();

                    if (COURSECATALOG_COUNT > 0)
                        $(".mcPager").removeClass("display-none");

                    courseCatalogGeneratePageNumbers();
                    setCourseCatalogPage();
                    //remove delete classes to refresh styles after loading items
                    removeDeleteClasses();
                    hideLoading();

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var code = XMLHttpRequest.status;
                    if (code == "401")
                        window.location.reload();
                    else
                        alert(XMLHttpRequest.responseText);
                }
            });
        }

        function ccOnListViewDataBinding(sender, args) {
            sender.set_selectedIndexes([]);
        }

        function ccOnListViewCommand(sender, args) {
            args.set_cancel(true);
            bindCourseCatalog();
        }

        function saveCourseCatalogOrder() {
            var newIndexes = {};
            var newData = [];
            var newItemsPerPage = $find("<%= ddlCourseCatalogPager.ClientID %>").get_selectedItem().get_value();
            try {
                $("#ccMainWrapper").find(".item.move .js-hiddenfields").each(function (index, value) {
                    var txtIndex = $(value).find("input[name*=hfIndex]");
                    var txtId = $(value).find("input[name*=hfCourseID]");
                    var index = $(txtIndex).val();
                    var courseId = $(txtId).val();
                    var data = {
                        "index": index,
                        "courseid": courseId
                    }
                    newData.push(data);
                });
            } catch (e) {
                newData = [];
                var hndradalertsomewentwrong = $("#<%=hndradalertsomewentwrong.ClientID%>").val();
                var hndradalerterror = $("#<%=hndradalerterror.ClientID%>").val();
                radalert(hndradalertsomewentwrong + e.Message, 330, 180, hndradalerterror, "");
            }

            newIndexes = { "courses": newData, "items": newItemsPerPage };

            if (newData.length > 0) {
                $.ajax({
                    type: "POST",
                    data: "{ jsonData: '" + JSON.stringify(newIndexes) + "'}",
                    contentType: "application/json; charset=utf-8",
                    url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "UpdateTrendingCourseOrder",
                    dataType: 'json',
                    success: function (res) {
                        var result = res.d.isSuccess;
                        var message = res.d.message;
                        if (result == true) {
                            var hndradalertsuccsaved = $("#<%=hndradalertsuccsaved.ClientID%>").val();
                            var hndradalertsuccess = $("#<%=hndradalertsuccess.ClientID%>").val();
                            radalert(hndradalertsuccsaved, 330, 180, hndradalertsuccess, "");
                            bindCourseCatalog();
                            COURSECATALOG_LIST_CACHE = null;
                            COURSECATALOG_GRID_CACHE = null;
                        } else {
                            var hndradalertsomewentwrong = $("#<%=hndradalertsomewentwrong.ClientID%>").val();
                            var hndradalerterror = $("#<%=hndradalerterror.ClientID%>").val();
                            radalert(hndradalertsomewentwrong + message, 330, 180, hndradalerterror, "");
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        var code = XMLHttpRequest.status;
                        if (code == "401")
                            window.location.reload();
                        else
                            alert(XMLHttpRequest.responseText);
                    }
                });
            } else {
                var hndradalertselectfirst = $("#<%=hndradalertselectfirst.ClientID%>").val();
                var hndradalerterror = $("#<%=hndradalerterror.ClientID%>").val();
                radalert(hndradalertselectfirst, 330, 180, hndradalerterror, "");
            }
        }

        function deleteSelectedCourse() {
            var hndradconfirmdeletethis = $("#<%=hndradconfirmdeletethis.ClientID%>").val();
            var hndradconfirmconfirmdel = $("#<%=hndradconfirmconfirmdel.ClientID%>").val();
            radconfirm(hndradconfirmdeletethis, confirmDeleteCourseCatalogFn, 330, 180, "", hndradconfirmconfirmdel);

            function confirmDeleteCourseCatalogFn(arg) {
                if (arg) {
                    var deleteCourseData = {};
                    var newData = [];
                    try {
                        if (!ISCOURSECATALOGLISTVIEW) {
                            $("#ccMainWrapper").find(".item.delete.selected").each(function (index, value) {
                                var txtId = $(value).find("input[name*=hfCourseID]");
                                var courseId = $(txtId).val();
                                newData.push(courseId);
                            });
                        } else {
                            $("#ccGridWrapper").find(".item.delete.selected").each(function (index, value) {
                                var txtId = $(value).find("input[name*=hfCourseID]");
                                var courseId = $(txtId).val();
                                newData.push(courseId);
                            });
                        }

                        deleteCourseData = { "courses": newData };
                        if (newData.length > 0) {
                            $.ajax({
                                type: "POST",
                                data: "{ selectedCourses: '" + JSON.stringify(deleteCourseData) + "'}",
                                contentType: "application/json; charset=utf-8",
                                url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "DeleteTrendingCourse",
                                dataType: 'json',
                                success: function (res) {
                                    var result = res.d.isSuccess;
                                    var message = res.d.message;
                                    if (result == true) {
                                        var hndradalertcoursedeleted = $("#<%=hndradalertcoursedeleted.ClientID%>").val();
                                        var hndradalertsuccess = $("#<%=hndradalertsuccess.ClientID%>").val();
                                        radalert(hndradalertcoursedeleted, 330, 180, hndradalertsuccess, "");
                                        bindCourseCatalog();
                                    } else {
                                        var hndradalertsomewentwrong = $("#<%=hndradalertsomewentwrong.ClientID%>").val();
                                        var hndradalerterror = $("#<%=hndradalerterror.ClientID%>").val();
                                        radalert(hndradalertsomewentwrong + message, 330, 180, hndradalerterror, "");
                                    }
                                },
                                error: function (XMLHttpRequest, textStatus, errorThrown) {
                                    var code = XMLHttpRequest.status;
                                    if (code == "401")
                                        window.location.reload();
                                    else
                                        alert(XMLHttpRequest.responseText);
                                }
                            });
                        } else {
                            var hndradalertselectfirst = $("#<%=hndradalertselectfirst.ClientID%>").val();
                            var hndradalerterror = $("#<%=hndradalerterror.ClientID%>").val();
                            radalert(hndradalertselectfirst, 330, 180, hndradalerterror, "");
                        }
                    } catch (Error) {
                        newData = [];
                        console.log(Error);
                        var hndradalertsomewenterror = $("#<%=hndradalertsomewenterror.ClientID%>").val();
                        var hndradalerterror = $("#<%=hndradalerterror.ClientID%>").val();
                        radalert(hndradalertsomewenterror + Error.Message, 330, 180, hndradalerterror, "");
                    }
                } else {
                    return false;
                }
            }
        }

        function DefaultViewChanged(el) {
            var id = $(el).attr("id");
            if (id == "cbGridView")
                $('#cbListView').prop('checked', false);
            else
                $('#cbGridView').prop('checked', false);
        }

        function setCourseCatalogPage() {
            COURSECATALOG_PAGEINDEX = COURSECATALOG_LISTVIEW.get_currentPageIndex();
            COURSECATALOG_COUNT = COURSECATALOG_LISTVIEW.get_virtualItemCount();
            COURSECATALOG_PAGESIZE = COURSECATALOG_LISTVIEW.get_pageSize();
            var myPage = COURSECATALOG_PAGEINDEX + 1;
            var startNum = myPage == 1 ? 1 : ((myPage * COURSECATALOG_PAGESIZE) > COURSECATALOG_COUNT ? COURSECATALOG_COUNT : (myPage * COURSECATALOG_PAGESIZE));
            $(".js-page-numbers.coursecatalog ul li").removeClass("selected-page");
            $(".js-page-numbers.coursecatalog ul li:nth-child(" + myPage + ")").addClass("selected-page");
            var itemCount = COURSECATALOG_PAGESIZE * (myPage) <= COURSECATALOG_COUNT ? (COURSECATALOG_PAGESIZE * (myPage)) : ((COURSECATALOG_PAGEINDEX * COURSECATALOG_PAGESIZE) + (COURSECATALOG_COUNT % COURSECATALOG_PAGESIZE));
            var itemCountIndex = myPage == 1 ? (COURSECATALOG_COUNT == 0 ? 0 : 1) : ((COURSECATALOG_PAGEINDEX * COURSECATALOG_PAGESIZE) > COURSECATALOG_COUNT ? COURSECATALOG_COUNT : (COURSECATALOG_PAGEINDEX * COURSECATALOG_PAGESIZE) + 1);
            $(".lblCourseCatalogShowNumber").html("Showing " + itemCountIndex + " - " + itemCount + " of " + COURSECATALOG_COUNT);
        }

        function changeCourseCatalogPageSize(sender, args) {
            COURSECATALOG_LISTVIEW = $find("<%= lvCourseCatalog.ClientID %>");
            var ps = $find("<%= ddlCourseCatalogPager.ClientID %>").get_selectedItem().get_value();
            COURSECATALOG_PAGESIZE = ps;
            COURSECATALOG_PAGEINDEX = 0;

            var defaultPageSize = COURSECATALOG_LISTVIEW.get_pageSize();
            if (!$.isNumeric(ps)) {
                COURSECATALOG_PAGESIZE = defaultPageSize;
            }
            COURSECATALOG_LISTVIEW.set_pageSize(COURSECATALOG_PAGESIZE);
            COURSECATALOG_LISTVIEW.page(COURSECATALOG_PAGEINDEX);
            COURSECATALOG_LISTVIEW.rebind();
        }

        function deleteCourseCatalogItem(sender) {
            //remove move
            //add delete
            var btnDelete = $("#btnDelete");
            if (!(btnDelete.hasClass("deleting"))) {
                addDeleteClasses();
            } else {
                removeDeleteClasses();
            }
        }

        function addDeleteClasses() {
            var btnDelete = $("#btnDelete");
            btnDelete.addClass("deleting");
            $("#MCListAccordion").sortable("disable");

            $("#MCListAccordion").find(".item").each(function (index, value) {
                $(value).removeClass("move");
                $(value).addClass("delete");
            });
            $("#MCGridAccordion").find(".item").each(function (index, value) {
                $(value).removeClass("move");
                $(value).addClass("delete");
            });
            $("#btnSaveChanges").addClass("display-none");
            $("#btnDeleteCourse").removeClass("display-none");
        }

        function removeDeleteClasses() {
            var btnDelete = $("#btnDelete");
            $("#btnSaveChanges").removeClass("display-none");
            $("#btnDeleteCourse").addClass("display-none");
            $("#MCListAccordion").find(".item").each(function (index, value) {
                $(value).addClass("move");
                $(value).removeClass("delete");
                $(value).removeClass("selected");
            });
            $("#MCGridAccordion").find(".item").each(function (index, value) {
                $(value).addClass("move");
                $(value).removeClass("delete");
                $(value).removeClass("selected");
            });
            btnDelete.removeClass("deleting");
            $("#MCListAccordion").sortable("enable");
        }

        function clickCourseCatalogItem(sender) {
            if ($(sender).hasClass("selected")) {
                $(sender).removeClass("selected");
            } else if ($(sender).hasClass("delete")) {
                $(sender).addClass("selected");
            }
        }

        function courseCatalogGeneratePageNumbers() {
            var maxPage = Math.ceil(COURSECATALOG_COUNT / COURSECATALOG_PAGESIZE);
            //var maxPageToDisplay = maxPage > 5 ? 5 : (virtualCount % pageSize > 1 ? maxPage : maxPage);
            var pageVirtualCount = COURSECATALOG_COUNT;
            var pages = $(".js-page-numbers.coursecatalog ul").html('');
            //end = end > maxPage ? maxPage : end;
            for (var i = 1; i <= maxPage; i++) {
                var pages = $(".js-page-numbers.coursecatalog ul").html();
                var rp = i - 1;
                $(".js-page-numbers.coursecatalog ul").html(pages + '<li class="page-number"> <a href="javascript:void(0);" onclick="gotoPageCourseCatalog(' + rp + ');">' + i + '</a></li>');
            }
        }

        function btnSwitchViewCourseLibray_Click() {
            if ($('#<%= pnlCourseLibraryListView.ClientID %>').hasClass("display-none")) {
                ISCOURSELIBRARYLISTVIEW = true;
                $("#<%= pnlCourseLibraryListView.ClientID %>").removeClass("display-none");
                $("#<%= pnlGridCourseLibrary.ClientID %>").addClass("display-none");
                $("#<%= lbtnSwitchToGridCourseLibrary.ClientID %>").addClass("display-none");
                $("#<%= lbtnSwitchToListCourseLibrary.ClientID %>").removeClass("display-none");
            } else {
                ISCOURSELIBRARYLISTVIEW = false;
                $("#<%= pnlCourseLibraryListView.ClientID %>").addClass("display-none");
                $("#<%= pnlGridCourseLibrary.ClientID %>").removeClass("display-none");
                $("#<%= lbtnSwitchToListCourseLibrary.ClientID %>").addClass("display-none");
                $("#<%= lbtnSwitchToGridCourseLibrary.ClientID %>").removeClass("display-none");
            }
            setCourseLibrarySelected();
            return false;
        }

        function gotoPageCourseCatalog(pageNum) {
            $(".js-page-numbers.coursecatalog ul li").removeClass("selected-page");

            COURSECATALOG_LISTVIEW.page(pageNum);
            COURSECATALOG_LISTVIEW.page(pageNum);
        }

        function btnSwitchViewCourseCatalog_Click() {
            if (COURSECATALOG_LIST_CACHE == null || COURSECATALOG_GRID_CACHE == null) {
                COURSECATALOG_LIST_CACHE = $("#MCListAccordion").html();
                COURSECATALOG_GRID_CACHE = $("#MCGridAccordion").html();
            }

            if ($('#<%= pnlCourseCatalogListView.ClientID %>').hasClass("display-none")) {
                removeDeleteClasses();
                ISCOURSECATALOGLISTVIEW = false;
                $("#MCListAccordion").html(COURSECATALOG_LIST_CACHE);
                $("#<%= pnlCourseCatalogListView.ClientID %>").removeClass("display-none");
                $("#<%= pnlCourseCatalogGridView.ClientID %>").addClass("display-none");
                $("#<%= lbtnSwitchToGridViewCourseCatalog.ClientID %>").addClass("display-none");
                $("#<%= lbtnSwitchToListViewCourseCatalog.ClientID %>").removeClass("display-none");
            } else {
                removeDeleteClasses();
                ISCOURSECATALOGLISTVIEW = true;
                $("#MCGridAccordion").html(COURSECATALOG_GRID_CACHE);
                $("#<%= pnlCourseCatalogListView.ClientID %>").addClass("display-none");
                $("#<%= pnlCourseCatalogGridView.ClientID %>").removeClass("display-none");
                $("#<%= lbtnSwitchToListViewCourseCatalog.ClientID %>").addClass("display-none");
                $("#<%= lbtnSwitchToGridViewCourseCatalog.ClientID %>").removeClass("display-none");
            }
            return false;
        }

        //COURSE LIBRARY
        function openCourseLibrary() {
            SELECTEDCOURSES_TOADD = [];
            initializeCourseLibraryView();
            $find("<%= rwCourseLibrary.ClientID %>").show();
        }

        function closeCourseLibrary() {
            $find("<%= rwCourseLibrary.ClientID %>").close();
        }

        function initializeCourseLibraryView() {
            COURSELIBRARY_LISTVIEW = $find("<%= lvCourseLibrary.ClientID%>");
            COURSELIBRARY_GRIDVIEW = $find("<%= gridCourseLibrary.ClientID%>");
            COURSELIBRARY_LISTVIEW.rebind();
            COURSELIBRARY_PAGESIZE = 12;
            COURSELIBRARY_LISTVIEW.set_pageSize(COURSELIBRARY_PAGESIZE);
            COURSELIBRARY_LISTVIEW.page(0);

            $(".clPager .pagePrev").click(function (e) {

                COURSELIBRARY_LISTVIEW.page(COURSELIBRARY_LISTVIEW.get_currentPageIndex() - 1);

            });

            $(".clPager .pageNext").click(function (e) {

                COURSELIBRARY_LISTVIEW.page(COURSELIBRARY_LISTVIEW.get_currentPageIndex() + 1);

            });
        }

        function bindCourseLibrary() {
            COURSELIBRARY_LISTVIEW = $find("<%= lvCourseLibrary.ClientID %>");
            var startRowIndex = COURSELIBRARY_LISTVIEW.get_currentPageIndex() * COURSELIBRARY_LISTVIEW.get_pageSize(),
                maximumRows = COURSELIBRARY_LISTVIEW.get_pageSize(),
                sortExpression = COURSELIBRARY_LISTVIEW.get_sortExpressions().toLinq();

            $.ajax({
                type: "POST",
                data: "{startRowIndex: " + startRowIndex + ", maximumRows: " + maximumRows + ", sortExpression: '" + sortExpression + "'}",
                contentType: "application/json; charset=utf-8",
                url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "GetTrendingCourseLibraryList",
                dataType: 'json',
                success: function (res) {
                    //console.log(res);
                    COURSELIBRARY_DATA = res.d.data;
                    COURSELIBRARY_COUNT = res.d.count;
                    COURSELIBRARY_PAGESIZE = COURSELIBRARY_LISTVIEW.get_pageSize();
                    COURSELIBRARY_LISTVIEW.set_virtualItemCount(COURSELIBRARY_COUNT);
                    COURSELIBRARY_LISTVIEW.set_dataSource(COURSELIBRARY_DATA);
                    COURSELIBRARY_LISTVIEW.dataBind();

                    COURSELIBRARY_GRIDVIEW.set_dataSource(COURSELIBRARY_DATA);
                    COURSELIBRARY_GRIDVIEW.dataBind();

                    if (COURSELIBRARY_COUNT > 0) {
                        $(".clPager").removeClass("display-none");
                        $(".lblCourseLibraryShowNumber").removeClass("display-none");
                    }

                    courseLibraryGeneratePageNumbers();
                    setCourseLibraryPage();

                    setCourseLibrarySelected();
                    removeDeleteClasses();

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var code = XMLHttpRequest.status;
                    if (code == "401")
                        window.location.reload();
                    else
                        alert(XMLHttpRequest.responseText);
                }
            });
        }

        function courseLibraryGeneratePageNumbers() {
            var maxPage = Math.ceil(COURSELIBRARY_COUNT / COURSELIBRARY_PAGESIZE);
            //var maxPageToDisplay = maxPage > 5 ? 5 : (virtualCount % pageSize > 1 ? maxPage : maxPage);
            var pageVirtualCount = COURSELIBRARY_COUNT;
            var pages = $(".js-page-numbers.courselibrary ul").html('');
            //end = end > maxPage ? maxPage : end;
            for (var i = 1; i <= maxPage; i++) {
                var pages = $(".js-page-numbers.courselibrary ul").html();
                var rp = i - 1;
                $(".js-page-numbers.courselibrary ul").html(pages + '<li class="page-number"> <a href="javascript:void(0);" onclick="gotoPageCourseLibrary(' + rp + ');">' + i + '</a></li>');
            }
        }

        function setCourseLibraryPage() {
            COURSELIBRARY_PAGEINDEX = COURSELIBRARY_LISTVIEW.get_currentPageIndex();
            COURSELIBRARY_COUNT = COURSELIBRARY_LISTVIEW.get_virtualItemCount();
            COURSELIBRARY_PAGESIZE = COURSELIBRARY_LISTVIEW.get_pageSize();
            var myPage = COURSELIBRARY_PAGEINDEX + 1;
            var startNum = myPage == 1 ? (COURSELIBRARY_COUNT == 0 ? 0 : 1) : ((myPage * COURSELIBRARY_PAGESIZE) > COURSELIBRARY_COUNT ? COURSELIBRARY_COUNT : (myPage * COURSELIBRARY_PAGESIZE));
            $(".js-page-numbers.courselibrary ul li").removeClass("selected-page");
            $(".js-page-numbers.courselibrary ul li:nth-child(" + myPage + ")").addClass("selected-page");
            var itemCount = COURSELIBRARY_PAGESIZE * (myPage) <= COURSELIBRARY_COUNT ? (COURSELIBRARY_PAGESIZE * (myPage)) : ((COURSELIBRARY_PAGEINDEX * COURSELIBRARY_PAGESIZE) + (COURSELIBRARY_COUNT % COURSELIBRARY_PAGESIZE));
            $(".lblCourseLibraryShowNumber").html("Showing " + itemCount + " of " + COURSELIBRARY_COUNT);
        }

        function clOnListViewDataBinding(sender, args) {
            sender.set_selectedIndexes([]);
        }

        function clOnListViewCommand(sender, args) {
            args.set_cancel(true);
            bindCourseLibrary();
        }

        function selectCourseToAdd(sender) {
            var courseId = $(sender).find("[name=hfCourseID]").val();

            if ($(sender).hasClass("selected")) {
                $(sender).removeClass("selected");

                if (SELECTEDCOURSES_TOADD.indexOf(courseId.toString()) > -1) {
                    var index = SELECTEDCOURSES_TOADD.indexOf(courseId);
                    if (index > -1) {
                        SELECTEDCOURSES_TOADD.splice(index, 1);
                    }
                }
            }
            else {
                $(sender).addClass("selected");

                if (SELECTEDCOURSES_TOADD.indexOf(courseId.toString()) < 0) {
                    SELECTEDCOURSES_TOADD.push(courseId.toString());
                }
            }
        }

        function setCourseLibrarySelected() {
            if (ISCOURSELIBRARYLISTVIEW) {
                $("#CourseLibraryView .item").removeClass("selected");
                $("#CourseLibraryView input[name=hfCourseID]").each(function (index, value) {
                    var cid = $(value).val();
                    if (SELECTEDCOURSES_TOADD.indexOf(cid) >= 0) {
                        $(value).parent().addClass("selected");
                    }
                });
            } else {
                $("#CourseLibraryGridView .item").removeClass("selected");
                $("#CourseLibraryGridView input[name=hfCourseID]").each(function (index, value) {
                    var cid = $(value).val();
                    if (SELECTEDCOURSES_TOADD.indexOf(cid) >= 0) {
                        $(value).parent().addClass("selected");
                    }
                });
            }
        }

        function saveSelectedCourse() {
            var coursesData = { "courses": SELECTEDCOURSES_TOADD };

            if (SELECTEDCOURSES_TOADD.length > 0) {
                $.ajax({
                    type: "POST",
                    data: "{selectedCourses: '" + JSON.stringify(coursesData) + "'}",
                    contentType: "application/json; charset=utf-8",
                    url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "SaveSelectedTrendingCourses",
                    dataType: 'json',
                    success: function (res) {
                        var result = res.d.isSuccess;
                        var message = res.d.message;
                        if (result == true) {
                            closeCourseLibrary();
                            var hndradalertcourseaddsuc = $("#<%=hndradalertcourseaddsuc.ClientID%>").val();
                            var hndradalertsuccess = $("#<%=hndradalertsuccess.ClientID%>").val();
                            radalert(hndradalertcourseaddsuc, 330, 180, hndradalertsuccess, "");
                            bindCourseLibrary();
                            bindCourseCatalog();
                        } else {
                            hndradalerterror
                            radalert(hndradalertsomewentwrong, 330, 180, hndradalerterror, "");
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        var code = XMLHttpRequest.status;
                        if (code == "401")
                            window.location.reload();
                        else
                            alert(XMLHttpRequest.responseText);
                    }
                });
            } else {
                var hndradalertselectfirst = $("#<%=hndradalertselectfirst.ClientID%>").val();
                var hndradalerterror = $("#<%=hndradalerterror.ClientID%>").val();
                radalert(hndradalertselectfirst, 330, 180, hndradalerterror, "");
            }
        }

        function changeCourseLibraryPageSize(sender, args) {
            var ps = $find("<%= ddlCourseLibrary.ClientID %>").get_selectedItem().get_value();
            COURSELIBRARY_PAGESIZE = ps;
            COURSELIBRARY_PAGEINDEX = 0;

            var defaultPageSize = COURSELIBRARY_LISTVIEW.get_pageSize();
            if (!$.isNumeric(ps)) {
                COURSELIBRARY_PAGESIZE = defaultPageSize;
            }

            COURSELIBRARY_LISTVIEW.set_pageSize(COURSELIBRARY_PAGESIZE);
            COURSELIBRARY_LISTVIEW.page(COURSELIBRARY_PAGEINDEX);
            COURSELIBRARY_LISTVIEW.rebind();
        }

        function gotoPageCourseLibrary(pageNum) {
            $(".js-page-numbers.courselibrary ul li").removeClass("selected-page");

            COURSELIBRARY_LISTVIEW.page(pageNum);
            COURSELIBRARY_LISTVIEW.page(pageNum);
        }

        //---
        function showLoading(controlToLoad) {
            var panel = $find("<%= localLoadingPanel.ClientID %>");
            panel.show(controlToLoad);
        }

        function hideLoading() {
            $(".Loading2").hide();
        }


        //update trending course settings
        function UpdateSettings() {

            var isGridView = $('#<%= cbGridView.ClientID %>').is(':checked') == true ? true : false;
            var displayCount = $find("<%= ddlDisplayCount.ClientID %>").get_selectedItem().get_value();
            var isManual = $('#<%= cbManualSelection.ClientID %>').is(':checked');
            var isVisible = $('#<%= cbVisible.ClientID %>').is(':checked');

            var onMostView = $('#<%= cbMostViews.ClientID %>').is(':checked');
            var mostViewCount = $find("<%= ddlTopMostViews.ClientID %>").get_selectedItem().get_value();
            var onRating = $('#<%= cbMostRatings.ClientID %>').is(':checked');
            var mostRatingCount = $find('<%= ddlTopMostRating.ClientID %>').get_selectedItem().get_value();
            var onRatingWithin = $('#<%= cbRating.ClientID %>').is(':checked');
            var ratingWithin = $find('<%= ddlRating.ClientID %>').get_selectedItem().get_index();
            var onCompleted = $("#<%= cbCompletedCourse.ClientID %>").is(':checked');
            var onMostEnrollee = $('#<%= cbMostEnrollee.ClientID %>').is(':checked');
            var mostEnrolleeCount = $find('<%= ddlTopMostEnrollee.ClientID %>').get_selectedItem().get_value();
            var onLastAdded = $('#<%= cbLatestAdded.ClientID %>').is(':checked');
            var lastAddedNumber = $find("<%= ddlLatestAddedNumber.ClientID %>").get_selectedItem().get_value();
            var lastAddedUnit = $find('<%= ddlLatestAddedUnit.ClientID %>').get_selectedItem().get_index();
            var onLastModified = $('#<%= cbLatestModified.ClientID %>').is(':checked');
            var lastModifiedNumber = $find("<%= ddlLatestModifiedNumber.ClientID %>").get_selectedItem().get_value();
            var lastModifiedUnit = $find('<%= ddlLatestModifiedUnit.ClientID %>').get_selectedItem().get_index();

            //test
            //console.log("defaultView = " + defaultView);
            //console.log("displayCount = " + displayCount);
            //console.log("isManual = " + isManual);
            //console.log("isVisible = " + isVisible);
            $.ajax({
                type: "POST",
                data: "{ IsGridView: '" + isGridView + "', " +
                 "DisplayCount: '" + displayCount + "', " +
                 "ManualSelection: '" + isManual + "', " +
                 "EnableMostView: '" + onMostView + "', " +
                 "MostViewCount: '" + mostViewCount + "', " +
                 "EnableMostRating: '" + onRating + "', " +
                 "MostRatingCount: '" + mostRatingCount + "', " +
                 "EnableRatingWithin: '" + onRatingWithin + "', " +
                 "RatingWithin: '" + ratingWithin + "', " +
                 "Completed: '" + onCompleted + "', " +
                 "EnableMostEnrollee: '" + onMostEnrollee + "', " +
                 "MostEnrolleeCount: '" + mostEnrolleeCount + "', " +
                 "EnableAddedLast: '" + onLastAdded + "', " +
                 "LastAddInMillisecond: '" + lastAddedNumber + "', " +
                 "LastAddUnit: '" + lastAddedUnit + "', " +
                 "EnableLastModified: '" + onLastModified + "', " +
                 "LastModifiedInMillisecond: '" + lastModifiedNumber + "', " +
                 "LastModifiedUnit: '" + lastModifiedUnit + "', " +
                 "Visible: '" + isVisible + "'}",
                contentType: "application/json; charset=utf-8",
                url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "SaveTrendingCourseSetting",
                dataType: 'json',
                success: function (msg) {
                    //console.log(bitVal);
                    settingsChanged = true;
                    radalert("New settings have been saved.", 330, 180, "Saved", "");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {

                    if (code == "401")
                        window.location.reload();
                    else {
                        alert(XMLHttpRequest.responseText);
                        var code = XMLHttpRequest.status;
                    }
                }
            });

            return false;

        }

    </script>
</rad:RadScriptBlock>
<rad:RadAjaxLoadingPanel ID="localLoadingPanel" runat="server" CssClass="Loading2"
    Transparency="25" />
<rad:RadWindowManager ID="rwmCourseCatalog" RenderMode="Lightweight" EnableShadow="true"
    VisibleOnPageLoad="false" Behaviors="Close, Move" DestroyOnClose="true" Modal="true"
    Opacity="99" runat="server" VisibleStatusbar="false" Skin="Bootstrap">
    <Windows>
        <rad:RadWindow ID="rwPreviewCourseCatalog" runat="server" Title="" CssClass="tc-radwindow-3 font-size-normal"
            Width="950px" AutoSizeBehaviors="Height" Height="750px">
            <ContentTemplate>
                <asp:Panel ID="pnlPreviewContent" runat="server" class="mc-preview-container">
                    <div class="row no-margin header-content border-box">
                        <div class="col-md-4 pull-left no-paddings">
                            <asp:LinkButton ID="lbtnSwitchToListViewCourseCatalog" runat="server" OnClientClick="btnSwitchViewCourseCatalog_Click(); return false;"
                                Font-Underline="false" aria-hidden="true" ToolTip="Switch to List View">
                                <i id="switchClassListView" runat="server" class="fa fa-th-list fa-md fa-gray ">
                                </i>
                            </asp:LinkButton>
                            <asp:LinkButton ID="lbtnSwitchToGridViewCourseCatalog" runat="server" OnClientClick="btnSwitchViewCourseCatalog_Click(); return false;"
                                Font-Underline="false" aria-hidden="true" CssClass="display-none" ToolTip="Switch to Grid View">
                                <i id="switchClassGridView" runat="server" class="fa fa-th-large  fa-md fa-gray ">
                                </i>
                            </asp:LinkButton>
                        </div>
                        <div class="col-md-4">
                            <p class="text-center lblCourseCatalogShowNumber">
                               <asp:Label runat="server" ID="Showing" Text= "<%$ Resources:LocalizedResource, Showing %>"></asp:Label>1-9 of 29
                            </p>
                        </div>
                        <div class="col-md-4 no-paddings">
                            <div class="col-md-4 pull-right no-margin no-paddings">
                                <rad:RadDropDownList ID="ddlCourseCatalogPager" runat="server" CssClass="soft bg-white"
                                    Width="100px" ClientIDMode="Static" OnClientSelectedIndexChanged="changeCourseCatalogPageSize">
                                </rad:RadDropDownList>
                            </div>
                            <div class="col-md-8 no-paddings">
                                <p class="pull-right">
                                   <asp:Label runat="server" ID="Showipp" Text= "<%$ Resources:LocalizedResource, ShowItemsPerPage %>"></asp:Label>  &nbsp;&nbsp;
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="mcPager container search-result no-paddings">
                        <p class="pull-left" style="line-height: 34px;">
                            &nbsp;&nbsp;
                            <asp:Label runat="server" ID="TrendingCourse" Text= "<%$ Resources:LocalizedResource,TrendingCourses%>"> </asp:Label>
                        </p>
                    </div>
                    <div class="row no-margin padding-vertical-md settings-button">
                        <div class="pull-right">
                            <asp:LinkButton ID="btnAdd" Text="text" runat="server" OnClientClick="openCourseLibrary(); return false;">
                                <i class="fa fa-plus-circle fa-md fa-gray" aria-hidden="true"></i>
                            </asp:LinkButton>
                            <asp:LinkButton ID="btnDelete" Text="text" runat="server" OnClientClick="deleteCourseCatalogItem(); return false;"
                                ClientIDMode="Static">
                                <i class="fa fa-trash-o fa-md fa-gray" aria-hidden="true"></i>
                            </asp:LinkButton>
                        </div>
                    </div>
                    <asp:Panel ID="pnlCourseCatalogListView" runat="server" ClientIDMode="Static">
                        <div id="pnlCourseCatalogContainer" class="main-content border-box">
                            <rad:RadListView ID="lvCourseCatalog" runat="server" AllowPaging="true" AllowMultiFieldSorting="true"
                                PageSize="12">
                                <LayoutTemplate>
                                    <div id="ccMainWrapper">
                                        <div id="mc-items" class="display-flow pull-left width-fill">
                                            <div id="MCListAccordion">
                                            </div>
                                        </div>
                                    </div>
                                </LayoutTemplate>
                                <ClientSettings>
                                    <DataBinding ItemPlaceHolderID="MCListAccordion">
                                        <ItemTemplate>
                                        <div class="col-md-3 move item" onclick="clickCourseCatalogItem(this);">
                                            <div class="image-wrapper container no-paddings">
                                                <div class="overlay-move overlay-item">
                                                    <i class="fa fa-arrows-alt fa-md fa-white" aria-hidden="true"></i>
                                                </div>
                                                    <div class="overlay-delete overlay-item">
                                                    <i class="fa fa-trash-o fa-md fa-white" aria-hidden="true"></i>
                                                </div>
                                                <image src='Media/Uploads/CourseImg/#= CourseID #/#= CourseImage #' onerror="this.src='Media/Uploads/CourseImg/No_image.jpg'" />
                                            </div>
                                            <div class="course-title font-bold teal">
                                                <p class="teal">
                                                <strong>#= CourseTitle #</strong>
                                                </p>
                                            </div>
                                            <div class="line-gray"></div>
                                            <div class="description font-bold">
                                                #= CourseDescription #
                                            </div>
                                            <div class="js-hiddenfields">
                                                <input type="number" name="hfIndex" value='#= OrderNumber #' class="display-none" />
                                                <input type="text" name="hfCourseID" value='#= EncryptedCourseID #' class="display-none" />
                                            </div>
                                        </div>
                                        </ItemTemplate>
                                        <EmptyDataTemplate>
                                    <p>
                                    No Trending Course Selected
                                    </p>
                                        </EmptyDataTemplate>
                                        <DataService EnableCaching="true" />
                                    </DataBinding>
                                    <ClientEvents OnCommand="ccOnListViewCommand" OnDataBinding="ccOnListViewDataBinding">
                                    </ClientEvents>
                                </ClientSettings>
                            </rad:RadListView>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="pnlCourseCatalogGridView" runat="server" CssClass="display-none" ClientIDMode="Static">
                        <rad:RadListView ID="gvCourseCatalog" runat="server" AllowPaging="true" PageSize="8">
                            <LayoutTemplate>
                                <div id="ccGridWrapper">
                                    <table class="gridMainTable table table-bordered table-striped course-grid">
                                        <thead>
                                            <tr class="rlvHeader">
                                                <th class="btn-teal">
                                                   <asp:Label runat="server" ID="titlesss" Text= "<%$ Resources:LocalizedResource, Title %>"></asp:Label> 
                                                </th>
                                                <th class="btn-teal">
                                                    <asp:Label runat="server" ID="desccc" Text= "<%$ Resources:LocalizedResource, Description %>"></asp:Label>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody id="MCGridAccordion">
                                        </tbody>
                                        <tfoot>
                                        </tfoot>
                                    </table>
                                </div>
                            </LayoutTemplate>
                            <ClientSettings>
                                <DataBinding ItemPlaceHolderID="MCGridAccordion">
                                    <ItemTemplate>
                                        <tr class="rlvI move item" title=" #= CourseTitle #" onclick='clickCourseCatalogItem(this);'>
                                            <td>
                                                <i class="fa fa-trash-o fa-md fa-gray pull-left" aria-hidden="true"></i>
                                                &nbsp;&nbsp;&nbsp;
                                                #= CourseTitle #
                                            </td>
                                            <td>
                                                &nbsp;&nbsp;&nbsp;
                                                #= CourseDescription #
                                            </td>
                                            <input type="hidden" name="hfCourseID" value="#= EncryptedCourseID #" />
                                        </tr>
                                    </ItemTemplate>
                                    <EmptyDataTemplate>
                                    <p>
                                    No Result Found
                                    </p>
                                    </EmptyDataTemplate>
                                    <DataService EnableCaching="true" />
                                </DataBinding>
                            </ClientSettings>
                        </rad:RadListView>
                    </asp:Panel>
                    <div class="mcPager pager-bottom container display-none">
                        <div class="js-page-numbers coursecatalog display-inline-flex" style="padding-top: 5%;">
                            <a class="pagePrev black-arrow fa fa-chevron-left no-underline-hover" href="javascript:void(0);"
                                title="Go to previous page"></a>
                            <ul class="nostyle display-inline-flex black-font">
                            </ul>
                            <a class="pageNext black-arrow fa fa-chevron-right no-underline-hover" href="javascript:void(0);"
                                title="Go to next page"></a>
                        </div>
                    </div>
                </asp:Panel>
                <div class="row bottom-center">
                    <asp:Button ID="btnCancel" Text="Cancel" runat="server" CssClass="btn btn-md btn-teal btn-flat"
                        OnClientClick="closeCourseCatalog(); return false" />
                    <asp:Button ID="btnSaveChanges" Text="Save changes" runat="server" CssClass="btn btn-md btn-teal btn-flat"
                        OnClientClick="saveCourseCatalogOrder(); return false;" ClientIDMode="Static" />
                    <asp:Button ID="btnDeleteCourse" Text="Delete" runat="server" CssClass="btn btn-md btn-teal btn-flat diplay-none"
                        OnClientClick="deleteSelectedCourse(); return false;" ClientIDMode="Static" />
                </div>
            </ContentTemplate>
        </rad:RadWindow>
        <rad:RadWindow ID="rwCourseLibrary" runat="server" Title="" CssClass="tc-radwindow-3 font-size-normal"
            Width="950px" AutoSizeBehaviors="Height" Height="750px">
            <ContentTemplate>
                <asp:Panel ID="pnlCourseLibrary" runat="server">
                    <div class="container no-paddings">
                        <p class="teal text-center bold" style="line-height: 34px;">
                           <asp:Label runat="server" ID="Clib" Text= "<%$ Resources:LocalizedResource,  CourseLibrary %>"></asp:Label>
                        </p>
                    </div>
                    <div class="row no-margin header-content border-box">
                        <div class="col-md-4 pull-left no-paddings">
                            <asp:LinkButton ID="lbtnSwitchToGridCourseLibrary" runat="server" OnClientClick="btnSwitchViewCourseLibray_Click(); return false;"
                                Font-Underline="false" aria-hidden="true" ToolTip="Switch to List View">
                                <i id="I1" runat="server" class="fa fa-th-list fa-md fa-gray "></i>
                            </asp:LinkButton>
                            <asp:LinkButton ID="lbtnSwitchToListCourseLibrary" runat="server" OnClientClick="btnSwitchViewCourseLibray_Click(); return false;"
                                Font-Underline="false" aria-hidden="true" CssClass="display-none" ToolTip="Switch to Grid View">
                                <i id="I2" runat="server" class="fa fa-th-large fa-md fa-gray "></i>
                            </asp:LinkButton>
                        </div>
                        <div class="col-md-4 text-center">
                            <p>
                                <p class="lblCourseLibraryShowNumber">
                                </p>
                            </p>
                        </div>
                        <div class="col-md-4">
                            <div class="col-md-3 pull-right no-margin no-paddings">
                                <rad:RadDropDownList ID="ddlCourseLibrary" runat="server" CssClass="soft bg-white"
                                    Width="50px" ClientIDMode="Static" OnClientSelectedIndexChanged="changeCourseLibraryPageSize">
                                </rad:RadDropDownList>
                            </div>
                            <div class="col-md-8 no-margin no-paddings">
                                <p class="pull-right">
                                    <asp:Label runat="server" ID="showitems" Text= "<%$ Resources:LocalizedResource, ShowingItemsPerPage %>"></asp:Label>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="main-content border-box">
                        <asp:Panel ID="pnlCourseLibraryListView" runat="server">
                            <rad:RadListView ID="lvCourseLibrary" runat="server" AllowPaging="true" AllowMultiFieldSorting="true"
                                PageSize="12">
                                <LayoutTemplate>
                                    <div id="CourseLibraryView">
                                        <div id="cl-items" class="display-flow pull-left width-fill">
                                            <div id="CourseLibraryListItemContainer">
                                            </div>
                                        </div>
                                    </div>
                                </LayoutTemplate>
                                <ClientSettings>
                                    <DataBinding ItemPlaceHolderID="CourseLibraryListItemContainer">
                                        <ItemTemplate>
                                        <div class="col-md-3 no-paddings no-margin item pointer " onclick="selectCourseToAdd(this);" id="i#= EncryptedCourseID #" >
                                            <div class="image-wrapper container no-paddings text-center">
                                                <i class="fa fa-check-circle-o fa-md fa-white" aria-hidden="true"></i>
                                                <h5 class="show-on-hover center-both white">
                                                    #= CourseTitle #</h5>
                                                <image src='Media/Uploads/CourseImg/#= CourseID #/#= CourseImage #' onerror="this.src='Media/Uploads/CourseImg/No_image.jpg'" class="img-fit" />
                                            </div>
                                            <input type="hidden" name="hfCourseID" value="#= EncryptedCourseID #" />
                                        </div>
                                        </ItemTemplate>
                                        <EmptyDataTemplate>
                                            <p>
                                            No Course To Display
                                            </p>
                                        </EmptyDataTemplate>
                                        <DataService EnableCaching="true" />
                                    </DataBinding>
                                    <ClientEvents OnCommand="clOnListViewCommand" OnDataBinding="clOnListViewDataBinding">
                                    </ClientEvents>
                                </ClientSettings>
                            </rad:RadListView>
                        </asp:Panel>
                        <asp:Panel ID="pnlGridCourseLibrary" runat="server" CssClass="display-none search-view">
                            <rad:RadListView ID="gridCourseLibrary" runat="server" AllowPaging="true" PageSize="8">
                                <LayoutTemplate>
                                    <div id="CourseLibraryGridView">
                                        <div class="clPager container search-result display-none">
                                            <p class="lblShowNumber" class="pull-left">
                                            </p>
                                        </div>
                                        <table class="gridMainTable table table-bordered table-striped course-grid">
                                            <thead>
                                                <tr class="rlvHeader">
                                                    <th class="btn-teal">
                                                     <asp:Label runat="server" ID="titlesssss" Text= "<%$ Resources:LocalizedResource, Title %>"></asp:Label>   
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody id="CourseLibraryGridItemContainer">
                                            </tbody>
                                            <tfoot>
                                            </tfoot>
                                        </table>
                                    </div>
                                </LayoutTemplate>
                                <ClientSettings>
                                    <DataBinding ItemPlaceHolderID="CourseLibraryGridItemContainer">
                                        <ItemTemplate>
                                        <tr class="rlvI hover-pointer item" title=" #= CourseTitle #" onclick='selectCourseToAdd(this);'>
                                            <td>
                                                <i class="fa fa-check-circle-o fa-md fa-white pull-left" aria-hidden="true"></i>
                                                &nbsp;&nbsp;&nbsp;
                                                #= CourseTitle #
                                               
                                            </td>
                                             <input type="hidden" name="hfCourseID" value="#= EncryptedCourseID #" />
                                        </tr>
                                        </ItemTemplate>
                                        <EmptyDataTemplate>
                                    <p>
                                    NoResultFound
                                    </p>
                                        </EmptyDataTemplate>
                                        <DataService EnableCaching="true" />
                                    </DataBinding>
                                </ClientSettings>
                            </rad:RadListView>
                        </asp:Panel>
                    </div>
                </asp:Panel>
                <div class="clPager pager-bottom container display-none no-paddings">
                    <div class="js-page-numbers courselibrary display-inline-flex" style="padding-top: 5%;">
                        <a class="pagePrev black-arrow fa fa-chevron-left no-underline-hover" href="javascript:void(0);"
                            title="Go to previous page"></a>
                        <ul class="nostyle display-inline-flex black-font">
                        </ul>
                        <a class="pageNext black-arrow fa fa-chevron-right no-underline-hover" href="javascript:void(0);"
                            title="Go to next page"></a>
                    </div>
                </div>
                <div class="row bottom-center">
                    <asp:Button ID="Button3" Text="Cancel" runat="server" CssClass="btn btn-md btn-teal btn-flat"
                        OnClientClick="closeCourseLibrary(); return false" />
                    <asp:Button ID="Button4" Text="Add" runat="server" CssClass="btn btn-md btn-teal btn-flat"
                        OnClientClick="saveSelectedCourse(); return false;" />
                </div>
            </ContentTemplate>
        </rad:RadWindow>
    </Windows>
</rad:RadWindowManager>
<asp:Panel ID="pnlCatalogSettings" runat="server" CssClass="region">
    <div class="wrapper-main-content">
        <div class="inner-content col-md-6">
            <h5>
              <asp:Label runat="server" ID="criterias" Text= "<%$ Resources:LocalizedResource, CriteriaSelection %>"></asp:Label>  
            </h5>
            <p>
                <asp:Label runat="server" ID="SelectthecriteriatodisplayacourseundertheTrendingCoursescatalogdisplay" Text= "<%$ Resources:LocalizedResource, SelectthecriteriatodisplayacourseundertheTrendingCoursescatalogdisplay %>"></asp:Label>
            </p>
        </div>
        <div class="inner-content col-md-5">
            <h5>
               <asp:Label runat="server" ID="filtercri" Text= "<%$ Resources:LocalizedResource, CriteriaFilters %>"></asp:Label>
            </h5>
            <table class="tbl-padding-md">
                <tr>
                    <td>
                        <input type="checkbox" id="cbMostViews" runat="server" class="js-switch-transparent " />
                    </td>
                    <td>
                      <asp:Label runat="server" ID="mostviews" Text= "<%$ Resources:LocalizedResource, CoursesWithTheMostNumberOfViews %>"></asp:Label>
                    </td>
                    <td>
                       <asp:Label runat="server" ID="topget" Text= "<%$ Resources:LocalizedResource,  GetTop %>"></asp:Label>
                    </td>
                    <td>
                        <rad:RadDropDownList ID="ddlTopMostViews" runat="server" CssClass="soft bg-white"
                            Width="50px">
                        </rad:RadDropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="checkbox" id="cbMostRatings" runat="server" class="js-switch-transparent " />
                    </td>
                    <td>
                       <asp:Label runat="server" ID="ctile1" Text= "<%$ Resources:LocalizedResource, CourseTitle %>"></asp:Label> Courses with the most number of ratings
                    </td>
                    <td>
                        <asp:Label runat="server" ID="topsget" Text= "<%$ Resources:LocalizedResource, GetTop %>"></asp:Label>
                    </td>
                    <td>
                        <rad:RadDropDownList ID="ddlTopMostRating" runat="server" CssClass="soft bg-white"
                            Width="50px">
                        </rad:RadDropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="checkbox" id="cbRating" runat="server" class="js-switch-transparent " />
                    </td>
                    <td>
                        <asp:Label runat="server" ID="Crate" Text= "<%$ Resources:LocalizedResource, CourseRatings %>"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="ranks" Text= "<%$ Resources:LocalizedResource, Ranked %>"></asp:Label>
                    </td>
                    <td>
                        <rad:RadDropDownList ID="ddlRating" runat="server" CssClass="soft bg-white" Width="70px">
                        </rad:RadDropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="checkbox" id="cbCompletedCourse" runat="server" class="js-switch-transparent " />
                    </td>
                    <td>
                      <asp:Label runat="server" ID="hundredpercent" Text= "<%$ Resources:LocalizedResource, CourseWith100PercentCompletionRate %>"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="checkbox" id="cbMostEnrollee" runat="server" class="js-switch-transparent " />
                    </td>
                    <td>
                       <asp:Label runat="server" ID="mostenroll" Text= "<%$ Resources:LocalizedResource, CoursesWithTheMostNumberOfEnrollees %>"></asp:Label>
                    </td>
                    <td>
                     <asp:Label runat="server" ID="GetTop" Text= "<%$ Resources:LocalizedResource, GetTop %>"></asp:Label>
                    </td>
                    <td>
                       
                        <rad:RadDropDownList ID="ddlTopMostEnrollee" runat="server" CssClass="soft bg-white"
                            Width="50px">
                        </rad:RadDropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="checkbox" id="cbLatestAdded" runat="server" class="js-switch-transparent " />
                    </td>
                    <td>
                       <asp:Label runat="server" ID="lastadd" Text= "<%$ Resources:LocalizedResource, CoursesAddedWithinTheLast %>"></asp:Label>
                    </td>
                    <td>
                        <rad:RadDropDownList ID="ddlLatestAddedNumber" runat="server" CssClass="soft bg-white"
                            Width="50px">
                        </rad:RadDropDownList>
                    </td>
                    <td>
                        <rad:RadDropDownList ID="ddlLatestAddedUnit" runat="server" CssClass="soft bg-white"
                            Width="100px">
                        </rad:RadDropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="checkbox" id="cbLatestModified" runat="server" class="js-switch-transparent " />
                    </td>
                    <td>
                    <asp:Label runat="server" ID="lastwithin" Text= "<%$ Resources:LocalizedResource, CoursesModifiedWithinTheLast %>"></asp:Label>
                    </td>
                    <td>
                        <rad:RadDropDownList ID="ddlLatestModifiedNumber" runat="server" CssClass="soft bg-white"
                            Width="50px">
                        </rad:RadDropDownList>
                    </td>
                    <td>
                        <rad:RadDropDownList ID="ddlLatestModifiedUnit" runat="server" CssClass="soft bg-white"
                            Width="100px">
                        </rad:RadDropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                       <asp:Label runat="server" ID="defview" Text= "<%$ Resources:LocalizedResource, SetDefaultView %>"></asp:Label>
                    </td>
                    <td>
                        <asp:CheckBox ID="cbGridView" Text="Grid" runat="server" ClientIDMode="Static" onclick="DefaultViewChanged(this);"
                            CssClass="js-cbox-defaultview" />
                    </td>
                    <td>
                        <asp:CheckBox ID="cbListView" Text="List" runat="server" ClientIDMode="Static" onclick="DefaultViewChanged(this);"
                            CssClass="js-cbox-defaultview" />
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                      <asp:Label runat="server" ID="displ" Text= "<%$ Resources:LocalizedResource, Display %>"></asp:Label>
                    </td>
                    <td>
                        <rad:RadDropDownList ID="ddlDisplayCount" runat="server" CssClass="soft bg-white"
                            Width="50px">
                        </rad:RadDropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="checkbox" id="cbManualSelection" runat="server" class="js-switch-transparent " />
                    </td>
                    <td>
                       <asp:Label runat="server" ID="enablesec" Text= "<%$ Resources:LocalizedResource, EnableManualSelection %>"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="checkbox" id="cbVisible" runat="server" class="js-switch-transparent" />
                    </td>
                    <td>
                       <asp:Label runat="server" ID="visithomes" Text= "<%$ Resources:LocalizedResource, VisibleOnHomepageSigninPage %>"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="row btn-group-center">
        <asp:Button ID="Button1" Text="Preview" runat="server" CssClass="btn btn-md btn-teal btn-flat"
            OnClientClick="openCourseCatalog(); return false" />
        <asp:Button ID="Button2" Text="Save changes" runat="server" CssClass="btn btn-md btn-teal btn-flat"
            OnClientClick="UpdateSettings(); return false;" />
    </div>
</asp:Panel>
<asp:HiddenField runat="server" ID ="hndradalerterror" Value="Error" /> 
<asp:HiddenField runat="server" ID ="hndradalertsomewenterror" Value="Something went wrong. Error : " /> 
<asp:HiddenField runat="server" ID ="hndradalertsomewentwrong" Value="Something went wrong. Please try again" /> 
<asp:HiddenField runat="server" ID ="hndradalertsuccsaved" Value="Course successfully saved" /> 
<asp:HiddenField runat="server" ID ="hndradalertcoursedeleted" Value="Course successfully deleted." /> 
<asp:HiddenField runat="server" ID ="hndradalertsuccess" Value="Success" /> 
<asp:HiddenField runat="server" ID ="hndradalertselectfirst" Value="Please select course first." /> 
<asp:HiddenField runat="server" ID ="hndradconfirmdeletethis" Value="Are you sure you want to delete this courses?" /> 
<asp:HiddenField runat="server" ID ="hndradconfirmconfirmdel" Value="Confirm Delete" /> 
<asp:HiddenField runat="server" ID ="hndradalertcourseaddsuc" Value="Course successfully added." /> 