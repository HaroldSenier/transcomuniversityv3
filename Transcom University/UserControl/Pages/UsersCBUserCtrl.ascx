﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UsersCBUserCtrl.ascx.cs"
    Inherits="UsersCBUserCtrl" %>
<asp:Panel ID="pnlMainUserUC" runat="server" ClientIDMode="Static">
    <rad:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <rad:AjaxSetting AjaxControlID="btnApproveDenyEnrollee">
                <UpdatedControls>
                    <%--  <rad:AjaxUpdatedControl ControlID="RadMultiPage2" />--%>
                    <rad:AjaxUpdatedControl ControlID="gridPendingEnrollee" />
                    <rad:AjaxUpdatedControl ControlID="gridApprovedEnrollee" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="rcmApproveEnrollee">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage2" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnRemoveEnrollee">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="RadMultiPage2" />
                    <rad:AjaxUpdatedControl ControlID="gridPendingEnrollee" />
                    <rad:AjaxUpdatedControl ControlID="gridApprovedEnrollee" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="gridPendingEnrollee">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="gridPendingEnrollee" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="gridApprovedEnrollee">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="gridApprovedEnrollee" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnAssignUser">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlAssignToAnotherCourse" LoadingPanelID="RadAjaxLoadingPanel2" />
                    <rad:AjaxUpdatedControl ControlID="gridApprovedEnrollee" />
                    <rad:AjaxUpdatedControl ControlID="ltApprovedEnrollee" />
                    <rad:AjaxUpdatedControl ControlID="usrgTransactionSummary" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="btnAssignANewCourse">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="pnlAssignANewCourse" LoadingPanelID="RadAjaxLoadingPanel2" />
                    <rad:AjaxUpdatedControl ControlID="gridApprovedEnrollee" />
                    <rad:AjaxUpdatedControl ControlID="ltApprovedEnrollee" />
                    <rad:AjaxUpdatedControl ControlID="usrgTransactionSummary" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="gridUserAllCourseList">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="gridUserAllCourseList" LoadingPanelID="RadAjaxLoadingPanel2" />
                </UpdatedControls>
            </rad:AjaxSetting>
            <rad:AjaxSetting AjaxControlID="gridAssignANewCourse">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="gridAssignANewCourse" LoadingPanelID="RadAjaxLoadingPanel2" />
                </UpdatedControls>
            </rad:AjaxSetting>
        </AjaxSettings>
    </rad:RadAjaxManagerProxy>
    <rad:RadScriptBlock ID="RadScriptBlock1" runat="server">
        <script type="text/javascript">
            function approveDenyChange(enrolleeCim) {
                $("#" + "<%= hfEnrolleeCIM.ClientID %>").val(enrolleeCim);
                $("#approveDeny").modal("toggle");
                return false;
            }

            function showContextMenu(e, enrolleeCim, enrolleeName) {
                $("#" + "<%= hfEnrolleeCIM.ClientID %>").val(enrolleeCim);
                $("#" + "<%= hfEnrolleeName.ClientID %>").val(enrolleeName);

                var contextMenu = $find("<%= rcmApproveEnrollee.ClientID %>");

                if ((!e.relatedTarget) || (!$telerik.isDescendantOrSelf(contextMenu.get_element(), e.relatedTarget))) {
                    contextMenu.show(e);
                }
                $telerik.cancelRawEvent(e);
            }

            function removeEnrollee() {
                $("#removeEnrollee").modal("toggle");
                console.log("remove");
            }

            function openCouseList() {
                var radwindow = $find('<%=rwAssignToAnotherCourse.ClientID %>');
                radwindow.show();
            }

            function setRadWindowTitle(sender) {
                var hfValue = $("#" + "<%= hfEnrolleeName.ClientID %>").val();

                if (hfValue) {
                    sender.set_title(hfValue);
                }
                else {
                    sender.set_title("Name");
                }
            }

            var selectedCourseItems = [];

            function courseRowSelecting(sender, args) {
                if ($("#hfAssignOption").val() == "2") {
                    clearSelectedUserItems();
                }
            }

            function courseRowSelected(sender, args) {
                debugger;
                var id = args.getDataKeyValue("CourseID");

                if (!selectedCourseItems.includes(id)) {
                    selectedCourseItems.push(id);
                }
                setSelectedUserItems();

            }

            function courseRowDeselected(sender, args) {

                var id = args.getDataKeyValue("CourseID");

                if (selectedCourseItems.includes(id)) {
                    selectedCourseItems.splice(selectedCourseItems.indexOf(id), 1);
                }
                setSelectedUserItems();


            }

            function courseRowCreated(sender, args) {
                var id = args.getDataKeyValue("CourseID");
                if (selectedCourseItems.includes(id) && selectedCourseItems.length > 0) {
                    args.get_gridDataItem().set_selected(true);
                }
            }

            function setSelectedUserItems() {
                var elements = selectedCourseItems;
                var outputStr = "";
                for (var i = 0; i < elements.length; i++) {
                    outputStr += elements[i] + ",";
                }
                $("#" + "<%= hfSelectedCourses.ClientID %>").val(outputStr);
            }

            function clearSelectedUserItems() {
                selectedCourseItems = [];
                var gridanother = $find("<%= gridUserAllCourseList.ClientID %>");
                gridanother.clearSelectedItems();
            }

            function usshowTransaction() {
                $find('<%=rwAssignToAnotherCourse.ClientID %>').close();
                var radwindow = $find('<%=usrdTransactionSummary.ClientID %>');
                radwindow.show();
            }

            function usbtnTransactionsList_Click() {
                var radwindow = $find('<%=usrdTransactionSummary.ClientID %>');
                radwindow.close();
            }

            function rcmApproveEnrollee_Click(sender, eventArgs) {
                var itemValue = eventArgs.get_item().get_value();

                if (itemValue == "1")
                    removeEnrollee();
                else if (itemValue == "2") {
                    $("#" + "<%= btnAssignUser.ClientID %>").val("Assign To Another Course")
                    $("#hfAssignOption").val("2");
                    openCouseList();
                }
                else if (itemValue == "3") {
                    $("#" + "<%= btnAssignUser.ClientID %>").val("Assign To New Course")
                    $("#hfAssignOption").val("3");
                    openCouseList();
                }
            }
        </script>
    </rad:RadScriptBlock>
    <asp:HiddenField ID="hfEnrolleeCIM" runat="server" />
    <asp:HiddenField ID="hfEnrolleeName" runat="server" />
    <rad:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true" RenderMode="Lightweight"
        Skin="Bootstrap" VisibleStatusbar="false">
        <Windows>
            <rad:RadWindow ID="rwAssignToAnotherCourse" runat="server" RenderMode="Lightweight"
                Modal="true" Skin="Bootstrap" Behaviors="Move, Close" OnClientShow="setRadWindowTitle"
                CssClass="tc-radwindow-1 height-inherit" Height="530px" Width="850px" OnClientBeforeShow="clearSelectedUserItems"
                VisibleStatusbar="false">
                <ContentTemplate>
                    <asp:Panel ID="pnlAssignToAnotherCourse" runat="server">
                        <rad:RadGrid ID="gridUserAllCourseList" runat="server" RenderMode="Lightweight" AutoGenerateColumns="false"
                            PageSize="6" OnNeedDataSource="gridUserAllCourseList_NeedDataSource" CssClass="GridLess"
                            AllowMultiRowSelection="true">
                            <PagerStyle Mode="NextPrev" AlwaysVisible="true" />
                            <ClientSettings EnableAlternatingItems="true">
                                <Selecting AllowRowSelect="true"></Selecting>
                                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                <Resizing AllowColumnResize="true" ResizeGridOnColumnResize="true" AllowResizeToFit="true" />
                                <ClientEvents OnRowSelected="courseRowSelected" OnRowDeselected="courseRowDeselected"
                                    OnRowSelecting="courseRowSelecting" OnRowCreated="courseRowCreated" />
                            </ClientSettings>
                            <ItemStyle Wrap="false"></ItemStyle>
                            <MasterTableView DataKeyNames="CourseID" ClientDataKeyNames="CourseID" AllowSorting="true"
                                AllowPaging="true" CommandItemDisplay="None" HeaderStyle-ForeColor="Teal">
                                <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                <Columns>
                                    <rad:GridClientSelectColumn UniqueName="chkAssignToAnotherCourse">
                                    </rad:GridClientSelectColumn>
                                    <rad:GridBoundColumn UniqueName="CourseID" HeaderText="<%$ Resources:LocalizedResource, CourseId %>" DataField="CourseID"
                                        ReadOnly="true">
                                    </rad:GridBoundColumn>
                                    <rad:GridBoundColumn UniqueName="Title" HeaderText="<%$ Resources:LocalizedResource, CourseName %>" DataField="Title"
                                        ReadOnly="true">
                                    </rad:GridBoundColumn>
                                    <rad:GridBoundColumn UniqueName="Description" HeaderText="<%$ Resources:LocalizedResource, Description %>" DataField="Description"
                                        ReadOnly="true">
                                    </rad:GridBoundColumn>
                                </Columns>
                            </MasterTableView>
                        </rad:RadGrid>
                        <br />
                        <div style="margin-left: 75%;">
                            <asp:Button ID="btnAssignUser" runat="server" CssClass="btn btn-md btn-teal btn-flat"
                                Text="<%$ Resources:LocalizedResource, AssignToAnotherCourse %>" OnClick="btnAssignUser_Click" />
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </rad:RadWindow>
        </Windows>
        <%-- <Windows>
            <rad:RadWindow ID="rwAssignANewCourse" runat="server" RenderMode="Lightweight" Modal="true"
                Skin="Bootstrap" Behaviors="Move, Close" OnClientShow="setRadWindowTitle" CssClass="tc-radwindow-1 height-inherit"
                Height="530px" Width="850px" VisibleStatusbar="false" OnClientBeforeShow="clearSelectedUserItems">
                <ContentTemplate>
                    <asp:Panel ID="pnlAssignANewCourse" runat="server">
                        <rad:RadGrid ID="gridAssignANewCourse" runat="server" RenderMode="Lightweight" AutoGenerateColumns="false"
                            PageSize="6" OnNeedDataSource="gridAssignANewCourse_NeedDataSource" CssClass="GridLess"
                            AllowMultiRowSelection="false">
                            <PagerStyle Mode="NextPrev" AlwaysVisible="true" />
                            <ClientSettings EnableAlternatingItems="true">
                                <Selecting AllowRowSelect="true"></Selecting>
                                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                <Resizing AllowColumnResize="true" ResizeGridOnColumnResize="true" AllowResizeToFit="true" />
                                <ClientEvents OnRowSelected="courseRowSelected" OnRowDeselected="courseRowDeselected"
                                    OnRowCreated="courseRowCreated" />
                            </ClientSettings>
                            <ItemStyle Wrap="false"></ItemStyle>
                            <MasterTableView DataKeyNames="CourseID" ClientDataKeyNames="CourseID" AllowSorting="true" AllowPaging="true" CommandItemDisplay="None"
                                HeaderStyle-ForeColor="Teal">
                                <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                <Columns>
                                    <rad:GridClientSelectColumn UniqueName="chkAssignANewCourse">
                                    </rad:GridClientSelectColumn>
                                    <rad:GridBoundColumn UniqueName="CourseID" HeaderText="COURSEID" DataField="CourseID"
                                        Display="false">
                                    </rad:GridBoundColumn>
                                    <rad:GridBoundColumn UniqueName="Title" HeaderText="COURSE NAME" DataField="Title"
                                        ReadOnly="true">
                                    </rad:GridBoundColumn>
                                    <rad:GridBoundColumn UniqueName="Description" HeaderText="DESCRIPTION" DataField="Description"
                                        ReadOnly="true">
                                    </rad:GridBoundColumn>
                                </Columns>
                            </MasterTableView>
                        </rad:RadGrid>
                        <br />
                        <div style="margin-left: 75%;">
                            <asp:Button ID="btnAssignANewCourse" runat="server" CssClass="btn btn-md btn-teal btn-flat"
                                Text="Assign A New Course" OnClick="btnAssignANewCourse_Click" />
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </rad:RadWindow>
        </Windows>--%>
        <Windows>
            <rad:RadWindow RenderMode="Lightweight" ID="usrdTransactionSummary" Behaviors="Close,Move"
                VisibleOnPageLoad="false" runat="server" CssClass="tc-radwindow-1" Height="330px"
                Title="Result Summary">
                <ContentTemplate>
                    <asp:Panel ID="pnlTransactionList" runat="server">
                        <rad:RadGrid RenderMode="Lightweight" ID="usrgTransactionSummary" runat="server"
                            AllowSorting="True" CssClass="GridLess" AllowMultiRowSelection="true" AllowPaging="true"
                            OnItemDataBound="rgTransactionSummary_OnItemDataBound">
                            <MasterTableView AutoGenerateColumns="false" TableLayout="Auto" HeaderStyle-ForeColor="Teal"
                                DataKeyNames="Id" ClientDataKeyNames="Id">
                                <Columns>
                                    <rad:GridTemplateColumn HeaderText="#">
                                        <ItemTemplate>
                                            <%#Container.ItemIndex+1 %></ItemTemplate>
                                    </rad:GridTemplateColumn>
                                    <rad:GridBoundColumn DataField="Name" HeaderText="<%$ Resources:LocalizedResource, CourseTitle %>" UniqueName="Name">
                                    </rad:GridBoundColumn>
                                    <rad:GridBoundColumn DataField="Status" HeaderText="<%$ Resources:LocalizedResource, Status %>" UniqueName="Status">
                                    </rad:GridBoundColumn>
                                </Columns>
                            </MasterTableView>
                        </rad:RadGrid>
                    </asp:Panel>
                    <button id="usbtnTransactionsList" onclick="usbtnTransactionsList_Click(); return false;"
                        runat="server" title="Ok" class="btn tc-btn-md btn-teal btn-flat bottom-center">
                        <asp:Label runat="server" ID="Ok" Text= "<%$ Resources:LocalizedResource, Ok %>"></asp:Label></button>
                </ContentTemplate>
            </rad:RadWindow>
        </Windows>
    </rad:RadWindowManager>
    <div class="col-md-12">
        <div class="CourseBuilder-Settings">
            <div class="row">
                <div class="container">
                    <asp:Label runat="server" ID="lblNumberOfGroup" ForeColor="Teal" Font-Bold="true"
                        Font-Size="Small" CssClass="display-inline-flex"><asp:Label runat="server" ID="ThereAre" Text= "<%$ Resources:LocalizedResource, ThereAre %>"></asp:Label>&nbsp;<asp:Literal ID="ltPendingEnrollee" runat="server">&nbsp;</asp:Literal>
                        <asp:Label runat="server" ID="pendingenrollmentstothiscourse" Text= "<%$ Resources:LocalizedResource, pendingenrollmentstothiscourse %>"></asp:Label> </asp:Label>
                </div>
                <rad:RadGrid ID="gridPendingEnrollee" runat="server" RenderMode="Lightweight" AutoGenerateColumns="false"
                    PageSize="6" OnNeedDataSource="gridPendingEnrollee_NeedDataSource" CssClass="GridLess">
                    <PagerStyle Mode="NextPrev" AlwaysVisible="true" />
                    <ClientSettings EnableAlternatingItems="true">
                        <Selecting AllowRowSelect="true"></Selecting>
                        <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                        <Resizing AllowColumnResize="true" ResizeGridOnColumnResize="true" AllowResizeToFit="true" />
                    </ClientSettings>
                    <ItemStyle Wrap="false"></ItemStyle>
                    <MasterTableView DataKeyNames="EnrolleeCIM" AllowSorting="true" AllowPaging="true"
                        CommandItemDisplay="None" HeaderStyle-ForeColor="Teal">
                        <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                        <Columns>
                            <rad:GridTemplateColumn HeaderText="#" HeaderStyle-Width="5%">
                                <ItemTemplate>
                                    <%# Container.ItemIndex+1 %>
                                </ItemTemplate>
                            </rad:GridTemplateColumn>
                            <rad:GridBoundColumn DataField="EnrolleeCIM" HeaderText="<%$ Resources:LocalizedResource, CimNumber %>" UniqueName="EnrolleeCIM">
                            </rad:GridBoundColumn>
                            <rad:GridBoundColumn DataField="Name" HeaderText="<%$ Resources:LocalizedResource, Name %>" UniqueName="Name">
                            </rad:GridBoundColumn>
                            <rad:GridBoundColumn DataField="Campaign" HeaderText="<%$ Resources:LocalizedResource, Account %>" UniqueName="Campaign">
                            </rad:GridBoundColumn>
                            <rad:GridBoundColumn DataField="Supervisor" HeaderText="<%$ Resources:LocalizedResource, Supervisor %>" UniqueName="Supervisor">
                            </rad:GridBoundColumn>
                            <rad:GridTemplateColumn HeaderText="<%$ Resources:LocalizedResource, Action %>">
                                <ItemTemplate>
                                    <button class="btn btn-md btn-transparent" onclick='<%# "approveDenyChange(" + Eval("EnrolleeCIM") + "); return false;" %>'
                                        title="Approve Enrollee">
                                        <i class="fa fa-check"></i>
                                    </button>
                                </ItemTemplate>
                            </rad:GridTemplateColumn>

                        </Columns>
                    </MasterTableView>
                </rad:RadGrid>
            </div>
            <br />
            <br />
            <div class="row">
                <div class="container">
                    <asp:Label runat="server" ID="Label1" ForeColor="Teal" Font-Bold="true" Font-Size="Small">
                        <asp:Label runat="server" ID="ThereAreeee" Text= "<%$ Resources:LocalizedResource, ThereAre %>"></asp:Label>
                        <asp:Literal ID="ltApprovedEnrollee" runat="server"></asp:Literal>
                        <asp:Label runat="server" ID="approvedenrollmentstothiscourseee" Text= "<%$ Resources:LocalizedResource, approvedenrollmentstothiscourse %>"></asp:Label> </asp:Label>
                </div>
                <rad:RadGrid ID="gridApprovedEnrollee" runat="server" RenderMode="Lightweight" AutoGenerateColumns="false"
                    PageSize="6" OnNeedDataSource="gridApprovedEnrollee_NeedDataSource" CssClass="GridLess">
                    <PagerStyle Mode="NextPrev" AlwaysVisible="true" />
                    <ClientSettings EnableAlternatingItems="true">
                        <Selecting AllowRowSelect="true"></Selecting>
                        <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                        <Resizing AllowColumnResize="true" ResizeGridOnColumnResize="true" AllowResizeToFit="true" />
                    </ClientSettings>
                    <ItemStyle Wrap="false"></ItemStyle>
                    <MasterTableView DataKeyNames="EnrolleeCIM" AllowSorting="true" AllowPaging="true"
                        CommandItemDisplay="None" HeaderStyle-ForeColor="Teal">
                        <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                        <Columns>
                            <rad:GridTemplateColumn HeaderText="#" HeaderStyle-Width="5%">
                                <ItemTemplate>
                                    <%# Container.ItemIndex+1 %>
                                </ItemTemplate>
                            </rad:GridTemplateColumn>
                            <rad:GridBoundColumn DataField="EnrolleeCIM" HeaderText="<%$ Resources:LocalizedResource, CimNumber %>" UniqueName="EnrolleeCIM">
                            </rad:GridBoundColumn>
                            <rad:GridBoundColumn DataField="Name" HeaderText="<%$ Resources:LocalizedResource, Name %>" UniqueName="Name">
                            </rad:GridBoundColumn>
                            <rad:GridBoundColumn DataField="Campaign" HeaderText="<%$ Resources:LocalizedResource, Account %>" UniqueName="Campaign">
                            </rad:GridBoundColumn>
                            <rad:GridBoundColumn DataField="Supervisor" HeaderText="<%$ Resources:LocalizedResource, Supervisor %>" UniqueName="Supervisor">
                            </rad:GridBoundColumn>
                            <rad:GridTemplateColumn>
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnDetails" runat="server" OnClientClick='<%# "showContextMenu(event," + Eval("EnrolleeCIM") + ",\""+  Eval("Name") + "\")" %>'>
                                    <i class="fa fa-tasks icon-tasks" title="Menu" aria-hidden="true"></i>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </rad:GridTemplateColumn>
                        </Columns>
                    </MasterTableView>
                </rad:RadGrid>
            </div>
        </div>
    </div>
    <rad:RadContextMenu ID="rcmApproveEnrollee" runat="server" RenderMode="Lightweight"
        OnClientItemClicked="rcmApproveEnrollee_Click" EnableShadows="true" CssClass="RadContextMenu1"
        EnableScreenBoundaryDetection="false">
        <Items>
            <rad:RadMenuItem Text="<%$ Resources:LocalizedResource, Remove %>" Value="1" />
            <rad:RadMenuItem Text="<%$ Resources:LocalizedResource, AssignToAnotherCourse %>" Value="2" />
            <rad:RadMenuItem Text="<%$ Resources:LocalizedResource, AssignANewCourse %>" Value="3" />
        </Items>
    </rad:RadContextMenu>
    <div id="approveDeny" class="modal fade success-popup" tabindex="1041" role="dialog"
        aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="H4">
                        <asp:Label runat="server" ID="ApproveDeny" Text= "<%$ Resources:LocalizedResource, ApproveDeny %>"></asp:Label>
                    </h4>
                </div>
                <div class="modal-body text-center">
                    <p class="lead">
                        <asp:Label runat="server" ID="ApproveEnrollee" Text= "<%$ Resources:LocalizedResource, ApproveEnrollee %>"></asp:Label>
                    </p>
                </div>
                <div class="modal-footer">
                    <asp:LinkButton ID="btnDenyEnrollee" runat="server" OnClick="btnDenyEnrollee_Click"
                            Text="Deny" CssClass="btn btn-danger"></asp:LinkButton>
                           <%-- <%$ Resources:LocalizedResource, Deny %>--%>

                    <i data-toggle="modal" data-target="#approveDeny">
                        <asp:LinkButton ID="btnApproveDenyEnrollee" runat="server" OnClick="btnApproveDeny_Click"
                            Text="<%$ Resources:LocalizedResource, Approved %>" CssClass="btn btn-info"></asp:LinkButton>
                    </i>
                </div>
            </div>
        </div>
    </div>
    <div id="removeEnrollee" class="modal fade success-popup" tabindex="-1" role="dialog"
        aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="H1">
                       <asp:Label runat="server" ID="remmov" Text= "<%$ Resources:LocalizedResource,  Remove %>"></asp:Label>
                    </h4>
                </div>
                <div class="modal-body text-center">
                    <p class="lead">
                        <asp:Label runat="server" ID="RemoveEnrolleee" Text= "<%$ Resources:LocalizedResource, RemoveEnrollee %>"></asp:Label>
                    </p>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-danger" data-toggle="modal" data-target="#removeEnrollee"><asp:Label runat="server" ID="Cancellll" Text= "<%$ Resources:LocalizedResource, Cancel %>"></asp:Label>
                    </a><i data-toggle="modal" data-target="#removeEnrollee">
                        <asp:LinkButton ID="btnRemoveEnrollee" runat="server" OnClick="btnRemoveEnrollee_Click"
                            Text="<%$ Resources:LocalizedResource, OK %>" CssClass="btn btn-info"></asp:LinkButton>
                    </i>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hfSelectedCourses" runat="server" />
    <asp:HiddenField ID="hfAssignOption" runat="server" ClientIDMode="Static" />
</asp:Panel>
