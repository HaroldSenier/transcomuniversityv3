﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Web.UI.HtmlControls;
using TranscomUniversityV3Model;
using System.Security.Cryptography;
using System.IO;
using System.Text;
using System.Drawing;
using TransactionNames;
using System.Web.UI.WebControls.WebParts;

public partial class UsersCBUserCtrl : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

        Page.Form.Attributes.Add("enctype", "multipart/form-data");

        if (!(System.Web.Security.Roles.IsUserInRole(HttpContext.Current.User.Identity.Name.Split('|')[0], "Admin") || System.Web.Security.Roles.IsUserInRole(HttpContext.Current.User.Identity.Name.Split('|')[0], "Trainer")))
            Response.Redirect("~/Unauthorized.aspx");

        gridPendingEnrollee.Rebind();
        gridApprovedEnrollee.Rebind();
        GetUsersCount();

    }

    protected void gridPendingEnrollee_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        var courseId = Convert.ToInt32(DataHelper.Decrypt(Request.QueryString["CourseID"]));
        //var pendingEnrollee = 

        //if (pendingEnrollee != null)
        gridPendingEnrollee.DataSource = DataHelper.GetPendingEnrollee(courseId);
    }

    protected void populateEnrollee()
    {
        var courseId = Convert.ToInt32(DataHelper.Decrypt(Request.QueryString["CourseID"]));
        var pendingEnrollee = DataHelper.GetPendingEnrollee(courseId);
        var approvedEnrollee = DataHelper.GetApprovedEnrollee(courseId);

        if (pendingEnrollee != null)
            gridPendingEnrollee.DataSource = pendingEnrollee;

        if (approvedEnrollee != null)
            gridApprovedEnrollee.DataSource = approvedEnrollee;

        gridPendingEnrollee.DataBind();
        gridApprovedEnrollee.DataBind();
    }

    protected void gridApprovedEnrollee_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
       var courseId = Convert.ToInt32(DataHelper.Decrypt(Request.QueryString["CourseID"]));
        //var approvedEnrollee = 

        //if (approvedEnrollee != null)
        gridApprovedEnrollee.DataSource = DataHelper.GetApprovedEnrollee(courseId);
    }

    protected void gridUserAllCourseList_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        var assignToAnotherCourse = DataHelper.GetAllActiveCourses();

        if (assignToAnotherCourse != null)
            gridUserAllCourseList.DataSource = assignToAnotherCourse;
    }

    //protected void gridAssignANewCourse_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    //{
    //    var assignANewCourse = DataHelper.GetAllActiveCourses();

    //    if (assignANewCourse != null)
    //        gridAssignANewCourse.DataSource = assignANewCourse;
    //}

    protected void btnApproveDeny_Click(object sender, EventArgs e)
    {
        try
        {
            int _actionID = DataHelper.getLastLogID() + 1;
            string _userIP = DataHelper.GetIPAddress();
            string _userID = DataHelper.GetCurrentUserCIM();
            double _duration = 0;

            DateTime startTime;
            DateTime endTime;

            var courseId = Convert.ToInt32(DataHelper.Decrypt(Request.QueryString["CourseID"]));
            var enrolleeCim = Convert.ToInt32(hfEnrolleeCIM.Value);
            var delegator = DataHelper.GetCurrentUserCIM();

            var db = new TranscomUniversityV3ModelContainer();

            var dts = db.CreateQuery<DateTime>("CurrentDateTime() ");
            startTime = dts.AsEnumerable().First();

            db.pr_TranscomUniversity_UpdateEnrollmentStatus(Convert.ToInt32(courseId), Convert.ToInt32(enrolleeCim), 2, Convert.ToInt32(delegator), Convert.ToDateTime(DateTime.UtcNow));

            string _action = "Approved User " + DataHelper.getUserName(enrolleeCim);
            var dte = db.CreateQuery<DateTime>("CurrentDateTime() ");

            endTime = dte.AsEnumerable().First();
            _duration = (endTime - startTime).TotalMilliseconds;

            int l_actionID = DataHelper.getLastLogID() + 1;
            DataHelper.logCourse(courseId, l_actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);

            //gridPendingEnrollee.Rebind();
            //gridApprovedEnrollee.Rebind();
            populateEnrollee();
            GetUsersCount();
        }
        catch
        {
            RadWindowManager1.RadAlert("Error approving enrollee. Please contact your System Administrator.", 330, 180, "Error Message", "");
        }
    }

    protected void btnDenyEnrollee_Click(object sender, EventArgs e)
    {
        try
        {
            int _actionID = DataHelper.getLastLogID() + 1;
            string _userIP = DataHelper.GetIPAddress();
            string _userID = DataHelper.GetCurrentUserCIM();
            double _duration = 0;

            DateTime startTime;
            DateTime endTime;

            var courseId = Convert.ToInt32(DataHelper.Decrypt(Request.QueryString["CourseID"]));
            var enrolleeCim = Convert.ToInt32(hfEnrolleeCIM.Value);
            var delegator = DataHelper.GetCurrentUserCIM();

            var db = new TranscomUniversityV3ModelContainer();

            var dts = db.CreateQuery<DateTime>("CurrentDateTime() ");
            startTime = dts.AsEnumerable().First();

            db.pr_TranscomUniversity_UpdateEnrollmentStatus(Convert.ToInt32(courseId), Convert.ToInt32(enrolleeCim), 3, Convert.ToInt32(delegator), Convert.ToDateTime(DateTime.UtcNow));

            string _action = "Denied User " + DataHelper.getUserName(enrolleeCim);
            var dte = db.CreateQuery<DateTime>("CurrentDateTime() ");

            endTime = dte.AsEnumerable().First();
            _duration = (endTime - startTime).TotalMilliseconds;

            int l_actionID = DataHelper.getLastLogID() + 1;
            DataHelper.logCourse(courseId, l_actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);

            //gridPendingEnrollee.Rebind();
            //gridApprovedEnrollee.Rebind();
            populateEnrollee();
            GetUsersCount();
        }
        catch
        {
            RadWindowManager1.RadAlert("Error approving enrollee. Please contact your System Administrator.", 330, 180, "Error Message", "");
        }
    }

    #region Old Codes
    //protected void rcmApproveEnrollee_Click(object sender, RadMenuEventArgs e)
    //{
    //    var itemVal = e.Item.Value;

    //    if (itemVal == "1")
    //        ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", DataHelper.callClientScript("removeEnrollee"), true);
    //      //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "key", DataHelper.callClientScript("removeEnrollee"), true);

    //    if (itemVal == "2")
    //        ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", DataHelper.callClientScript("assignToAnotherCourse"), true);
    //        //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "key", DataHelper.callClientScript("assignToAnotherCourse"), true);

    //    if (itemVal == "3")
    //        ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", DataHelper.callClientScript("assignANewCourse"), true);
    //        //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "key", DataHelper.callClientScript("assignANewCourse"), true);
    //}
    #endregion

    protected void btnRemoveEnrollee_Click(object sender, EventArgs e)
    {
        try
        {
            var courseId = Convert.ToInt32(DataHelper.Decrypt(Request.QueryString["CourseID"]));
            var enrolleeCim = Convert.ToInt32(hfEnrolleeCIM.Value);

            int _actionID = DataHelper.getLastLogID() + 1;
            string _userIP = DataHelper.GetIPAddress();
            string _userID = DataHelper.GetCurrentUserCIM();
            double _duration = 0;

            DateTime startTime;
            DateTime endTime;

            var db = new TranscomUniversityV3ModelContainer();
            var dts = db.CreateQuery<DateTime>("CurrentDateTime() ");

            startTime = dts.AsEnumerable().First();
            int successCount = 0;

            db.pr_TranscomUniversity_UpdateEnrollmentInactivity(courseId, enrolleeCim);
            db.SaveChanges();
            successCount++;

            if (successCount > 0)
            {
                var dte = db.CreateQuery<DateTime>("CurrentDateTime() ");
                endTime = dte.AsEnumerable().First();
                _duration = (endTime - startTime).TotalMilliseconds;
                string _action = "Removed User " + DataHelper.getUserName(enrolleeCim);

                DataHelper.logCourse(courseId, _actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);
                //gridPendingEnrollee.Rebind();
                //gridApprovedEnrollee.Rebind();
                populateEnrollee();

                RadWindowManager1.RadAlert("User was successfully removed", 330, 180, "Success Message", "");
            }

            GetUsersCount();
        }
        catch
        {
            RadWindowManager1.RadAlert("Error removing enrollee. Please contact your System Administrator.", 330, 180, "Error Message", "");
        }
    }

    private void AssignToAnotherCourse()
    {
        try
        {
            int _courseID = -1;
            int courseID_Origin = Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"]));
            int _actionID = DataHelper.getLastLogID() + 1;
            string _userIP = DataHelper.GetIPAddress();
            string _userID = DataHelper.GetCurrentUserCIM();
            double _duration = 0;

            DateTime startTime;
            DateTime endTime;

            int successCount = 0;
            string stat;

            List<Transaction> CourseList = new List<Transaction>();

            string courseList = hfSelectedCourses.Value;
            string[] data = courseList.Split(',');

            var enrolleeCim = Convert.ToInt32(hfEnrolleeCIM.Value);
            var delegator = DataHelper.GetCurrentUserCIM();

            if (data.Length > 1)
            {
                var db = new TranscomUniversityV3ModelContainer();

                var dts = db.CreateQuery<DateTime>("CurrentDateTime() ");
                startTime = dts.AsEnumerable().First();

                for (int i = 0; i < (data.Length - 1); i++)
                {
                    _courseID = Convert.ToInt32(data[i]);

                    //pr_TranscomUniversity_InsertEnrollmentUser_Result _res = db.pr_TranscomUniversity_InsertEnrollmentUser(Convert.ToInt32(enrolleeCim), Convert.ToInt32(_courseID), Convert.ToInt32(delegator)).SingleOrDefault();
                    pr_TranscomUniversity_UpdateEnrollmentUser_Result _res = db.pr_TranscomUniversity_UpdateEnrollmentUser(Convert.ToInt32(enrolleeCim), Convert.ToInt32(_courseID), Convert.ToInt32(courseID_Origin)).SingleOrDefault();

                    if (_res.Msg == "1")
                    {
                        stat = "Successful";
                        successCount++;
                        string _action = "Added " + DataHelper.getUserName(enrolleeCim);
                        var dte = db.CreateQuery<DateTime>("CurrentDateTime() ");

                        endTime = dte.AsEnumerable().First();
                        _duration = (endTime - startTime).TotalMilliseconds;

                        int l_actionID = DataHelper.getLastLogID() + 1;
                        DataHelper.logCourse(_courseID, l_actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);
                    }
                    else if (_res.Msg == "2")
                    {

                        stat = "Duplicate";
                    }
                    else
                    {

                        stat = "Fail";
                    }

                    Transaction course = new Transaction
                    {
                        Id = Convert.ToInt32(data[i]),
                        Name = _res.Title,
                        Status = stat
                    };

                    CourseList.Add(course);
                }
                if (successCount > 0)
                {
                    string _action = successCount > 1 ? "Assigned " + DataHelper.getUserName(enrolleeCim) + " to " + successCount + " Courses" : "Assigned " + DataHelper.getUserName(enrolleeCim) + " to " + DataHelper.getCourseName(_courseID);
                    if (successCount > 0)
                    {
                        var dte = db.CreateQuery<DateTime>("CurrentDateTime() ");
                        endTime = dte.AsEnumerable().First();
                        _duration = (endTime - startTime).TotalMilliseconds;
                        _actionID = DataHelper.getLastLogID() + 1;
                        DataHelper.logCourse(courseID_Origin, _actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);
                    }
                }

                gridUserAllCourseList.Rebind();
                //gridApprovedEnrollee.Rebind();
                GetUsersCount();

                usrgTransactionSummary.DataSource = CourseList;
                usrgTransactionSummary.DataBind();

                gridApprovedEnrollee.DataSource = DataHelper.GetApprovedEnrollee(courseID_Origin);
                gridApprovedEnrollee.DataBind();

                ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", Utils.callClientScript("usshowTransaction"), true);
            }
            else
            {
                RadWindowManager1.RadAlert("Please select atleast one(1) course to assign.", 330, 180, "System Message", "");
            }
        }
        catch
        {
            RadWindowManager1.RadAlert("Error assigning to another course(s). Please contact your System Administrator.", 330, 180, "Error Message", "");
        }
    }

    protected void btnAssignUser_Click(object sender, EventArgs e)
    {

        int assignID = 0;
        try
        {
            assignID = Convert.ToInt32(hfAssignOption.Value);
        }
        catch (Exception)
        {
            RadWindowManager1.RadAlert("Invalid Assigning please refresh the page and try again.", 330, 180, "System Message", "");
            return;
        }

        if (assignID == 2)//assign to another
        {
            AssignToAnotherCourse();
        }
        else if (assignID == 3)
        {
            AssignToNewCourse();
        }
        else
        {
            RadWindowManager1.RadAlert("Invalid in assigning user please refresh the page and try again.", 330, 180, "System Message", "");
            return;
        }

    }

    private void AssignToNewCourse()
    {
        try
        {
            int _actionID = DataHelper.getLastLogID() + 1;
            string _userIP = DataHelper.GetIPAddress();
            string _userID = DataHelper.GetCurrentUserCIM();
            double _duration = 0;

            DateTime startTime;
            DateTime endTime;

            int successCount = 0;
            List<Transaction> CourseList = new List<Transaction>();

            int count = 0;
            var enrolleeCim = Convert.ToInt32(hfEnrolleeCIM.Value);
            var currentCourseId = Convert.ToInt32(DataHelper.Decrypt(Request.QueryString["CourseID"]));

            var db = new TranscomUniversityV3ModelContainer();

            var dts = db.CreateQuery<DateTime>("CurrentDateTime() ");
            startTime = dts.AsEnumerable().First();

            var courseId = "";
            string stat = "";

            string courseList = hfSelectedCourses.Value;
            string[] data = courseList.Split(',');

            var delegator = DataHelper.GetCurrentUserCIM();
            if (data.Length <= 1)
            {
                RadWindowManager1.RadAlert("Please select one(1) course to assign.", 330, 180, "System Message", "");
                return;
            }

            if (data.Length > 1)
            {
                for (int i = 0; i < (data.Length - 1); i++)
                {
                    courseId = data[i];
                    pr_TranscomUniversity_InsertEnrollmentUser_Result retVal = db.pr_TranscomUniversity_InsertEnrollmentUser(Convert.ToInt32(enrolleeCim), Convert.ToInt32(courseId), Convert.ToInt32(delegator)).SingleOrDefault();
                    //pr_TranscomUniversity_UpdateEnrollmentUser_Result retVal = db.pr_TranscomUniversity_UpdateEnrollmentUser(Convert.ToInt32(enrolleeCim), Convert.ToInt32(courseId), Convert.ToInt32(currentCourseId)).SingleOrDefault();

                    if (retVal.Msg == "1")
                    {
                        successCount++;
                        string _action = "Added " + DataHelper.getUserName(Convert.ToInt32(enrolleeCim));
                        var dte = db.CreateQuery<DateTime>("CurrentDateTime() ");

                        endTime = dte.AsEnumerable().First();
                        _duration = (endTime - startTime).TotalMilliseconds;

                        int l_actionID = DataHelper.getLastLogID() + 1;
                        DataHelper.logCourse(Convert.ToInt32(courseId), l_actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);
                        stat = "Successful";
                    }
                    else if (retVal.Msg == "2")
                    {
                        //RadWindowManager1.RadAlert("User already assigned to this course.", 330, 180, "Error Message", "");
                        stat = "Duplicate";
                    }
                    else
                    {
                        stat = "Fail";
                        //RadWindowManager1.RadAlert("This User can only be assigned to a new course via the Groups Tab", 330, 180, "Error Message", "");
                    }
                    Transaction course = new Transaction
                    {
                        Id = Convert.ToInt32(count),
                        Name = DataHelper.getCourseName(Convert.ToInt32(courseId)),
                        Status = stat
                    };

                    CourseList.Add(course);
                    count++;
                }

            }

            if (successCount > 0)
            {
                string _action = successCount > 1 ? "Assigned " + DataHelper.getUserName(Convert.ToInt32(enrolleeCim)) + " to " + successCount + " Courses" : "Assigned " + DataHelper.getUserName(Convert.ToInt32(enrolleeCim)) + " to " + DataHelper.getCourseName(Convert.ToInt16(courseId));

                var dte = db.CreateQuery<DateTime>("CurrentDateTime() ");
                endTime = dte.AsEnumerable().First();

                _duration = (endTime - startTime).TotalMilliseconds;
                _actionID = DataHelper.getLastLogID() + 1;

                DataHelper.logCourse(currentCourseId, _actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);


                //RadWindowManager1.RadAlert("User Successfully assigned to this course.", 330, 180, "Error Message", "");
            }

            usrgTransactionSummary.DataSource = CourseList;
            usrgTransactionSummary.DataBind();
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "key", Utils.callClientScript("usshowTransaction"), true);



           
            GetUsersCount();
        }
        catch
        {
            RadWindowManager1.RadAlert("Error assigning to new course. Please contact your System Administrator.", 330, 180, "Error Message", "");
        }
    }

    public void GetUsersCount()
    {
        var db = new TranscomUniversityV3ModelContainer();
        var courseId = Convert.ToInt32(DataHelper.Decrypt(Request.QueryString["CourseID"]));

        var countPendingEnrollee = (from c in db.vw_TranscomUniversity_PendingEnrollments
                                    where c.CourseID == courseId
                                    select c).Count().ToString();

        var countApprovedEnrollee = (from c in db.vw_TranscomUniversity_ApprovedEnrollments
                                     where c.CourseID == courseId
                                     select c).Count().ToString();

        ltPendingEnrollee.Text = countPendingEnrollee;
        ltApprovedEnrollee.Text = countApprovedEnrollee;
    }

    protected void rgTransactionSummary_OnItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            GridDataItem dataBoundItem = e.Item as GridDataItem;

            if (dataBoundItem["Status"].Text == "Duplicate")
            {
                dataBoundItem["Status"].ForeColor = Color.DarkGray;
            }
            else if (dataBoundItem["Status"].Text == "Fail")
            {
                dataBoundItem["Status"].ForeColor = Color.Red;
            }
            else
            {
                dataBoundItem["Status"].ForeColor = Color.YellowGreen;
            }
        }
    }
}
