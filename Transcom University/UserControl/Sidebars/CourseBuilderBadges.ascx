﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CourseBuilderBadges.ascx.cs"
    Inherits="CourseBuilderBadges" %>
<style>
    #drop_zone
    {
        margin: 10px 0;
        width: 40%;
        min-height: 150px;
        text-align: center;
        text-transform: uppercase;
        font-weight: bold;
        border: 8px dashed #898;
        height: 160px;
    }
    
    .RadInput .riTextBox {
        height: 30px;
    }
</style>
<div id="pnlContent" class="contentSidebarContainer courseSidebarContainer content-tab bg-color-dark-gray"
    style="font-size: inherit;">
    <div class="row" style="height: inherit;">
        <div class="col-md-3 tabButtonGroup">
            <div class="tablinks" id="btnCourseUpload" style="font-size: 12px;">
                <button id="btnUpload" runat="server" title="Upload" onclick="openSideContent(event, 'ctUpload', this); return false"
                    style="width: 74.66px">
                    <h3 style="color: white;">       
                        <image src="Media/Images/badges.png" style="width: 40px; length: 40px"></image>
                    </h3>
                    <asp:Label runat="server" Text="<%$ Resources:LocalizedResource, Badges %>"></asp:Label>
                </button>
                <%--<asp:LinkButton runat="server" ID="btnUploadText" ClientIDMode="Static" OnClientClick="openSideContent(event, 'ctUpload', this); return false" style="width: 74.66px">
                    <asp:Label ID="Label15" runat="server" Text="<%$ Resources:LocalizedResource, Badges %>"></asp:Label>
                </asp:LinkButton>--%>
            </div>
            <div class="tablinks" id="btnCourseCreate" style="font-size: 12px;">
                <button id="btnCreate" runat="server" title="Create" onclick="openSideContent(event, 'ctCreate', this); return false"
                    style="width: 74.66px">
                    <h3 style="color: white;">
                        <%-- <i class="fa fa-envelope-o"></i>--%>
                        <image src="Media/Images/certificates.png" style="width: 40px; length: 40px"></image>
                    </h3>
                    <asp:Label ID="Label1" runat="server" Text="<%$ Resources:LocalizedResource, Certificates %>"></asp:Label>
                </button>
            </div>
        </div>
        <div class="col-md-9" style="padding: 0 !important;">
            <div class="contentDetails" style="padding-top: 0 !important;">
                <div id="ctIntro" class="tabcontent active text-center" style="display: block; font-size: 14px;">
                    <div class="row">
                        <div class="col-md-12 text-center contentDetails">
                            <asp:Label runat="server" Text="<%$ Resources: LocalizedResource, Selectanawardtoadd %>" />
                            <br />
                            <asp:Label ID="Label15" runat="server" Text="<%$ Resources: LocalizedResource, tothiscourse %>" />
                        </div>
                    </div>
                </div>
                <div id="ctUpload" class="tabcontent">
                    <div class="row">
                        <div class="col-md-12">
                            <asp:Panel ID="Panel" runat="server" CssClass="contentDetails">
                                <small class="color-white">
                                    <asp:Label ID="Label2" runat="server" Text="<%$ Resources:LocalizedResource, SelectAction %>"></asp:Label>
                                </small>
                                <center>
                                    <p>
                                        <asp:Button ID="btnSetAwardCondition" runat="server" Font-Size="Small" CssClass="btn btn-teal no-border-radius margin-top-10px"
                                            Text="<%$ Resources:LocalizedResource, SetAwardingConditions %>" Width="150" OnClick="btnSetAwardCondition_Click" /></p>
                                    <p>
                                        <asp:Button ID="btnGoToBadgeLib" runat="server" Font-Size="Small" CssClass="btn btn-teal no-border-radius margin-top-10px"
                                            Text="<%$ Resources:LocalizedResource, GoToBadgeLibrary %>" Width="150" OnClick="Button1_Click" /></p>
                                    <p>
                                        <asp:Button ID="btnUploadBadge" runat="server" Font-Size="Small" CssClass="btn btn-teal no-border-radius margin-top-10px"
                                            Text="<%$ Resources:LocalizedResource, UploadBadges %>" Width="150" OnClick="Button2_Click" /></p>
                                </center>
                            </asp:Panel>
                            <asp:Panel ID="pnlSACBadge" runat="server" Visible="false" CssClass="margin-top-10px">
                                <asp:LinkButton ID="LinkBtnBack" runat="server" OnClick="LinkBtnBack_Click"><h3><i class="fa fa-arrow-circle-left fa-large" style="color: #fff;"></i></h3></asp:LinkButton>
                                <small class="color-white">
                                    <asp:Label ID="Label3" runat="server" Text="<%$ Resources:LocalizedResource, SetAwardingConditions %>"></asp:Label>
                                </small>
                                <center class="fake-select">
                                    <asp:DropDownList ID="ddSelect" runat="server" CssClass="color-black">
                                    </asp:DropDownList>
                                    <asp:Button ID="btnBadgeSetAwarCond" runat="server" Font-Size="Small" CssClass="btn btn-teal no-border-radius margin-top-10px"
                                        Text="<%$ Resources:LocalizedResource, SelectBadge %>" Width="150" OnClick="Button3_Click" />
                                </center>
                            </asp:Panel>
                            <asp:Panel ID="pnlBadgeImages" runat="server" Visible="false">
                                <center style="margin-top: 20px;">
                                    <asp:Image ID="ImgBadge" runat="server" Width="100" Height="100" BorderColor="White"
                                        BorderWidth="1" CssClass="img-responsive" /></center>
                                <br />
                                <center>
                                    <asp:DropDownList ID="ddlBadgeCriteria" runat="server" CssClass="color-black" OnSelectedIndexChanged="ddlBadgeCriteria_SelectedIndexChanged"
                                        AutoPostBack="true">
                                        <asp:ListItem Value="1" Text="<%$ Resources: LocalizedResource, Criteria1 %>"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="<%$ Resources: LocalizedResource, Criteria2 %>"></asp:ListItem>
                                    </asp:DropDownList>
                                </center>
                            </asp:Panel>
                            <asp:Panel ID="pnlBadgeLib" runat="server" Visible="false" CssClass="margin-top-10px">
                                <asp:LinkButton ID="lbtnBackBadgeLib" runat="server" OnClick="LinkButton1_Click"><h3><i class="fa fa-arrow-circle-left fa-large" style="color: #fff;"></i></h3></asp:LinkButton>
                                <center>
                                    <small class="color-white text-center">
                                        <asp:Label ID="Label4" runat="server" Text="<%$ Resources:LocalizedResource, BadgeLibrary %>"></asp:Label>
                                    </small>
                                </center>
                            </asp:Panel>
                            <asp:Panel ID="pnlBadgeUpload" runat="server" Visible="false" CssClass="margin-top-10px">
                                <asp:LinkButton ID="lbtnBackBadgeUpload" OnClick="LinkBtnBack_Click" runat="server"
                                    Style="z-index: 999999;"><h3><i class="fa fa-arrow-circle-left fa-large" style="color: #fff;"></i></h3></asp:LinkButton>
                                <table>
                                    <tr>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                                <center>
                                    <small class="color-white text-center">
                                        <asp:Label ID="Label5" runat="server" Text="<%$ Resources:LocalizedResource, UploadBadges %>"></asp:Label>
                                    </small>
                                </center>
                                <div id="managecontrol2" style="width: 90%; padding-top: 10%;" align="center">
                                    <div id="Div1">                                     
                                        <rad:RadTextBox ID="rtbBadgeName" runat="server" RenderMode="Lightweight" EmptyMessage="<%$ Resources:LocalizedResource, BadgeName %>" MaxLength="50" onkeyup="CheckMaxLength(this,50,'Badge');"
                                            ToolTip="<%$ Resources: LocalizedResource, TypeBadgeName %>" ondragstart="return false;" ondrop="return false;" Skin="Bootstrap" Width="200px" Height="30px">
                                        </rad:RadTextBox>
                                        <%--<label class="text-right btn-teal input-sm">
                                        <span runat="server" id="txtBadgeChars">50</span> CharactersRemaining</label> --%>
                                        <label class="text-right btn-teal input-sm">
                                            <span runat="server" id="txtBadgeChars">50 
                                            </span>
                                            <asp:Label ID="Label6" runat="server" Text="<%$ Resources:LocalizedResource, CharactersRemaining %>"></asp:Label>
                                        </label>
                                        <rad:RadTextBox ID="rbtDescription" runat="server" RenderMode="Lightweight" EmptyMessage="<%$ Resources:LocalizedResource, Description %>"
                                            ToolTip="<%$ Resources: LocalizedResource, SelectSection %>" CssClass="margin-top-10px "  Skin="Bootstrap" Width="200px" Height="30px">
                                        </rad:RadTextBox>
                                        <p>
                                            <br />
                                        </p>
                                        <div class="row">
                                        </div>
                                        <center>
                                            <div>
                                                <div class="row img-dropzone-md bgimg-dropzone-md">
                                                    <div class="dropzone-empty" style="padding-top: 2em;">
                                                        <asp:Image ID="Image2" ImageUrl="~/Media/Images/icon-upload.png" runat="server" AlternateText="Upload Image"
                                                            Height="2.875em" Width="2.875em" />
                                                        <br />
                                                        <p>
                                                            <asp:Label ID="Label7" runat="server" Text="<%$ Resources:LocalizedResource, DragImageFileHere %>"></asp:Label>
                                                        </p>
                                                    </div>
                                                    <rad:RadAsyncUpload ID="BadgeImage" runat="server" MultipleFileSelection="Disabled"
                                                        ViewStateMode="Enabled" MaxFileInputsCount="1" ControlObjectsVisibility="None"
                                                        Width="100%" HideFileInput="false" ToolTip="<%$ Resources:LocalizedResource, SelectCourseImage %>" CssClass="CourseImage display-none"
                                                        DropZones=".bgimg-dropzone-md" ForeColor="#ddd" OnClientFilesUploaded="fileUploadedBadge"
                                                        OnClientFileSelected="fileSelectedBadge" OnFileUploaded="upload_FileUploaded"
                                                        AllowedFileExtensions="jpeg,jpg,gif,png">
                                                    </rad:RadAsyncUpload>
                                                    <div id="DropzonePreviewBadge" class="dropzone-preview" onmouseover="dropzoneEnter()"
                                                        onmouseout="dropzoneExit()" runat="server">
                                                        <rad:RadBinaryImage ID="badgeImagePreview" runat="server" ClientIDMode="Static" />
                                                        <div class="box-fluid tc-overlay">
                                                            <asp:LinkButton ID="btnRemovePreviewImg" runat="server" ToolTip="<%$ Resources:LocalizedResource, RemoveImage %>" OnClick="btnRemovePreviewImg_Click"
                                                                OnClientClick="showImageDropzonePreviewLoading();" CssClass="display-none">
                                                                    <i class="fa fa-times-circle-o previewClose" style="color: white; position: absolute;
                                                                    top: 50%; left: 50%; transform: translate(-50%,-50%); font-size: 5em; cursor: pointer;">
                                                                    </i>
                                                            </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row center-all" style="padding-top: .5em; position: relative; top: 50%;
                                                    transform: translate(0, 20%);">
                                                    or
                                                </div>
                                                <asp:Button ID="Button4" runat="server" Font-Size="Small" CssClass="btn btn-teal no-border-radius margin-top-10px"
                                                    Text="<%$ Resources:LocalizedResource, SelectFile %>" OnClientClick="javascript:openFileUploaderBadge(); return false;"
                                                    CausesValidation="false" Width="180px" />
                                                <%--<rad:RadButton ID="Button4" Text="Select File" runat="server" Font-Size="Small" CssClass="btn btn-teal no-border-radius margin-top-10px"
                                                    OnClientClicking="openFileUploaderBadge" />--%>
                                            </div>
                                        </center>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9" style="padding: 0 !important;">
            <div class="contentDetails" style="padding-top: 0 !important;">
                <div id="ctCreate" class="tabcontent">
                    <asp:Panel ID="pnlCertificates" runat="server" CssClass="contentDetails">
                        <small class="color-white">
                            <asp:Label ID="Label8" runat="server" Text="<%$ Resources:LocalizedResource, SelectAction %>"></asp:Label>
                        </small>
                        <center>
                            <p>
                                <asp:Button ID="btnCertAwarding" runat="server" Font-Size="Small" CssClass="btn btn-teal no-border-radius margin-top-10px"
                                    Text="<%$ Resources:LocalizedResource, SetAwardingConditions %>" Width="150" OnClick="btnCertSetAwardCondition_Click" /></p>
                            <p>
                                <asp:Button ID="btnCertLibrary" runat="server" Font-Size="Small" CssClass="btn btn-teal no-border-radius margin-top-10px"
                                    Text="<%$ Resources:LocalizedResource, CertificatesLibrary %>" Width="150" OnClick="btnCertLibrary_Click" /></p>
                            <p>
                                <asp:Button ID="btnCertUpload" runat="server" Font-Size="Small" CssClass="btn btn-teal no-border-radius margin-top-10px"
                                    Text="<%$ Resources:LocalizedResource, UploadCertificates %>" Width="150" OnClick="btnCertUpload_Click" /></p>
                        </center>
                    </asp:Panel>
                    <asp:Panel ID="pnlSAC" runat="server" Visible="false" CssClass="margin-top-10px">
                        <asp:LinkButton ID="lbtnBackSAC" runat="server" OnClick="LinkBtnCertBack_Click"><h3><i class="fa fa-arrow-circle-left fa-large" style="color: #fff;"></i></h3></asp:LinkButton>
                        <small class="color-white">
                            <asp:Label ID="Label9" runat="server" Text="<%$ Resources:LocalizedResource, SetAwardingConditions %>"></asp:Label>
                        </small>
                        <center class="fake-select">
                            <asp:DropDownList ID="ddlCertAwarding" runat="server" CssClass="color-black">
                            </asp:DropDownList>
                            <asp:Button ID="btnSetAwarding" runat="server" Font-Size="Small" CssClass="btn btn-teal no-border-radius margin-top-10px"
                                Text="<%$ Resources:LocalizedResource, SelectCertificates %>" Width="150" OnClick="btnSetAwarding_Click" />
                        </center>
                    </asp:Panel>
                    <asp:Panel ID="pnlSacImages" runat="server" Visible="false">
                        <center style="margin-top: 20px;">
                            <asp:Image ID="imgCert" runat="server" Width="100" Height="100" BorderColor="White"
                                BorderWidth="1" CssClass="img-responsive" /></center>
                        <br />
                        <center>
                            <asp:DropDownList ID="ddlCertCriteria" runat="server" CssClass="color-black" OnSelectedIndexChanged="ddlCertCriteria_SelectedIndexChanged"
                                AutoPostBack="true">
                                <asp:ListItem Value="1" Text="Criteria 1"></asp:ListItem>
                                <asp:ListItem Value="2" Text="Criteria 2"></asp:ListItem>
                            </asp:DropDownList>
                        </center>
                    </asp:Panel>
                    <asp:Panel ID="pnlCertLibrary" runat="server" Visible="false" CssClass="margin-top-10px">
                        <asp:LinkButton ID="lbtnBackCretLib" runat="server" OnClick="lbtnBackCertLib_Click"><h3><i class="fa fa-arrow-circle-left fa-large" style="color: #fff;"></i></h3></asp:LinkButton>
                        <center>
                            <small class="color-white text-center">
                                <asp:Label ID="Label10" runat="server" Text="<%$ Resources:LocalizedResource, CertificatesLibrary %>"></asp:Label>
                            </small>
                        </center>
                    </asp:Panel>
                    <asp:Panel ID="pnlCertUpload" runat="server" Visible="false" CssClass="margin-top-10px">
                        <asp:LinkButton ID="lbtnBackCertLib" OnClick="LinkBtnCertBack_Click" runat="server"
                            Style="z-index: 999999;"><h3><i class="fa fa-arrow-circle-left fa-large" style="color: #fff;"></i></h3></asp:LinkButton>
                        <table>
                            <tr>
                                <td>
                                </td>
                            </tr>
                        </table>
                        <center>
                            <small class="color-white text-center">
                                <asp:Label ID="Label11" runat="server" Text="<%$ Resources:LocalizedResource, UploadCertificates %>"></asp:Label>
                            </small>
                        </center>
                        <div id="Div2" style="width: 90%; padding-top: 10%;" align="center">
                            <div id="Div3">
                                <rad:RadTextBox ID="rtbCertificateName" runat="server" RenderMode="Lightweight" EmptyMessage="<%$ Resources:LocalizedResource, CertificateName %>" MaxLength="50" onkeyup="CheckMaxLength(this,50,'Cert');"
                                    ToolTip="<%$ Resources:LocalizedResource, TypeCertName %>" CssClass="" ondragstart="return false;" ondrop="return false;"  Skin="Bootstrap" Width="200px" Height="30px">
                                </rad:RadTextBox>
                                <%--<label class="text-right btn-teal input-sm">
                                        <span runat="server" id="txtCertChars">50</span> characters remaining</label> --%>
                                <label class="text-right btn-teal input-sm">
                                    <span runat="server" id="txtCertChars">50 
                                    </span>
                                    <asp:Label ID="Label12" runat="server" Text="<%$ Resources:LocalizedResource, CharactersRemaining %>"></asp:Label>
                                </label>
                                <rad:RadTextBox ID="rtbCertDesc" runat="server" RenderMode="Lightweight" EmptyMessage="<%$ Resources:LocalizedResource, Description %>"
                                    ToolTip="Select Section" CssClass=" margin-top-10px "  Skin="Bootstrap" Width="200px" Height="30px">
                                </rad:RadTextBox>
                                <p>
                                    <br />
                                </p>
                                <div class="row">
                                </div>
                                <center>
                                    <div>
                                        <div class="row img-dropzone-md certimg-dropzone-md">
                                            <div class="dropzone-empty" style="padding-top: 2em;">
                                                <asp:Image ID="Image1" ImageUrl="~/Media/Images/icon-upload.png" runat="server" AlternateText="Upload Image"
                                                    Height="2.875em" Width="2.875em" />
                                                <br />
                                                <p>
                                                    <asp:Label ID="Label13" runat="server" Text="<%$ Resources:LocalizedResource, DragImageFileHere %>"></asp:Label>
                                                </p>
                                            </div>
                                            <rad:RadAsyncUpload ID="CertificateImage" runat="server" MultipleFileSelection="Disabled"
                                                ViewStateMode="Enabled" MaxFileInputsCount="1" ControlObjectsVisibility="None"
                                                Width="100%" HideFileInput="false" ToolTip="Select Certificate Image" CssClass="CourseImage display-none"
                                                DropZones=".certimg-dropzone-md" ForeColor="#ddd" OnClientFilesUploaded="fileUploadedCert"
                                                OnClientFileSelected="fileSelectedCert" OnFileUploaded="upload_FileUploadedCert"
                                                AllowedFileExtensions="jpeg,jpg,gif,png">
                                            </rad:RadAsyncUpload>
                                            <div id="DropzonePreview" class="dropzone-preview" onmouseover="dropzoneEnterCert()"
                                                onmouseout="dropzoneExitCert()" runat="server">
                                                <rad:RadBinaryImage ID="certImagePreview" runat="server" ClientIDMode="Static" />
                                                <div class="box-fluid tc-overlay">
                                                    <asp:LinkButton ID="btnRemovePreviewImgCert" runat="server" ToolTip="Remove Image"
                                                        OnClick="btnRemovePreviewImgCert_Click" OnClientClick="showImageDropzonePreviewLoadingCert();"
                                                        CssClass="hidden-lg hidden-md hidden-s hidden-xs">
                                                                    <i class="fa fa-times-circle-o previewClose" style="color: white; position: absolute;
                                                                    top: 50%; left: 50%; transform: translate(-50%,-50%); font-size: 5em; cursor: pointer;">
                                                                    </i>
                                                    </asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row center-all" style="padding-top: .5em; position: relative; top: 50%;
                                            transform: translate(0, 20%);">
                                            <asp:Label ID="Label14" runat="server" Text="<%$ Resources:LocalizedResource, Or %>"></asp:Label>
                                        </div>
                                        <asp:Button ID="RadButton1" runat="server" Font-Size="Small" CssClass="btn btn-teal no-border-radius margin-top-10px"
                                            Text="<%$ Resources:LocalizedResource, SelectFile %>" OnClientClick="javascript:openFileUploaderCert(); return false;"
                                            CausesValidation="false" Width="180px" />
                                        <%-- <rad:RadButton ID="RadButton1" Text="Select File" runat="server"  Font-Size="Small" CssClass="btn btn-teal no-border-radius margin-top-10px"
                                    OnClientClicking="openFileUploaderCert" />--%>
                                    </div>
                                </center>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </div>
        </div>
    </div>
</div>
<rad:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true" RenderMode="Lightweight"
    Skin="Bootstrap">
    <Windows>
    </Windows>
</rad:RadWindowManager>
<rad:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <rad:AjaxSetting AjaxControlID="btnSetAwardCondition">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="btnSetAwardCondition" />
                <rad:AjaxUpdatedControl ControlID="Panel" />
                <rad:AjaxUpdatedControl ControlID="pnlSACBadge" />
                <rad:AjaxUpdatedControl ControlID="pnlBadgeImages" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnBadgeSetAwarCond">
            <UpdatedControls>
                <%--<rad:AjaxUpdatedControl ControlID="btnBadgeSetAwarCond" />--%>
                <rad:AjaxUpdatedControl ControlID="Panel" />
                <rad:AjaxUpdatedControl ControlID="pnlSACBadge" />
                <rad:AjaxUpdatedControl ControlID="pnlBadgeImages" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnGoToBadgeLib">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="btnGoToBadgeLib" />
                <rad:AjaxUpdatedControl ControlID="Panel" />
                <rad:AjaxUpdatedControl ControlID="pnlSACBadge" />
                <rad:AjaxUpdatedControl ControlID="pnlBadgeImages" />
                <rad:AjaxUpdatedControl ControlID="pnlBadgeLib" />
                <rad:AjaxUpdatedControl ControlID="BadgeUserCtrl1" />
                <rad:AjaxUpdatedControl ControlID="SettingsCBUserCtrl1" />
                <rad:AjaxUpdatedControl ControlID="RadLV" />
                <rad:AjaxUpdatedControl ControlID="RadGV" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="lbtnBackBadgeLib">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="lbtnBackBadgeLib" />
                <rad:AjaxUpdatedControl ControlID="Panel" />
                <rad:AjaxUpdatedControl ControlID="pnlSACBadge" />
                <rad:AjaxUpdatedControl ControlID="pnlBadgeImages" />
                <rad:AjaxUpdatedControl ControlID="pnlBadgeLib" />
                <rad:AjaxUpdatedControl ControlID="BadgeUserCtrl1" />
                <rad:AjaxUpdatedControl ControlID="SettingsCBUserCtrl1" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnUploadBadge">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="btnUploadBadge" />
                <rad:AjaxUpdatedControl ControlID="Panel" />
                <rad:AjaxUpdatedControl ControlID="pnlSACBadge" />
                <rad:AjaxUpdatedControl ControlID="pnlBadgeImages" />
                <rad:AjaxUpdatedControl ControlID="pnlBadgeLib" />
                <rad:AjaxUpdatedControl ControlID="pnlBadgeUpload" />
                <rad:AjaxUpdatedControl ControlID="BadgeUserCtrl1" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="lbtnBackBadgeUpload">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="lbtnBackBadgeUpload" />
                <rad:AjaxUpdatedControl ControlID="Panel" />
                <rad:AjaxUpdatedControl ControlID="pnlSACBadge" />
                <rad:AjaxUpdatedControl ControlID="pnlBadgeImages" />
                <rad:AjaxUpdatedControl ControlID="pnlBadgeLib" />
                <rad:AjaxUpdatedControl ControlID="pnlBadgeUpload" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="LinkBtnBack">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="LinkBtnBack" />
                <rad:AjaxUpdatedControl ControlID="Panel" />
                <rad:AjaxUpdatedControl ControlID="pnlSACBadge" />
                <rad:AjaxUpdatedControl ControlID="pnlBadgeImages" />
                <%--  <rad:AjaxUpdatedControl ControlID="pnlBadgeLib" />
                <rad:AjaxUpdatedControl ControlID="pnlBadgeUpload" />
                <rad:AjaxUpdatedControl ControlID="BadgeUserCtrl1" />
                <rad:AjaxUpdatedControl ControlID="SettingsCBUserCtrl1" />
                <rad:AjaxUpdatedControl ControlID="btnUploadBadge" />--%>
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="ddlBadgeCriteria">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="ddlBadgeCriteria" />
                <rad:AjaxUpdatedControl ControlID="Panel" />
                <rad:AjaxUpdatedControl ControlID="pnlSACBadge" />
                <rad:AjaxUpdatedControl ControlID="pnlBadgeImages" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="lbtnBackCretLib">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlCertificates" />
                <rad:AjaxUpdatedControl ControlID="pnlSAC" />
                <rad:AjaxUpdatedControl ControlID="pnlSacImages" />
                <rad:AjaxUpdatedControl ControlID="pnlCertLibrary" />
                <rad:AjaxUpdatedControl ControlID="pnlCertUpload" />
                <rad:AjaxUpdatedControl ControlID="BadgeUserCtrl1" />
                <rad:AjaxUpdatedControl ControlID="SettingsCBUserCtrl1" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnCertAwarding">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="btnCertAwarding" />
                <rad:AjaxUpdatedControl ControlID="pnlCertificates" />
                <rad:AjaxUpdatedControl ControlID="pnlSAC" />
                <rad:AjaxUpdatedControl ControlID="pnlSacImages" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnSetAwarding">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="btnSetAwarding" />
                <rad:AjaxUpdatedControl ControlID="pnlCertificates" />
                <rad:AjaxUpdatedControl ControlID="pnlSAC" />
                <rad:AjaxUpdatedControl ControlID="pnlSacImages" />
                <rad:AjaxUpdatedControl ControlID="Panel" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnCertUpload">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="btnCertUpload" />
                <rad:AjaxUpdatedControl ControlID="Panel" />
                <rad:AjaxUpdatedControl ControlID="pnlSAC" />
                <rad:AjaxUpdatedControl ControlID="pnlSacImages" />
                <rad:AjaxUpdatedControl ControlID="pnlCertLibrary" />
                <rad:AjaxUpdatedControl ControlID="pnlCertUpload" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="ddlCertCriteria">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="ddlCertCriteria" />
                <rad:AjaxUpdatedControl ControlID="btnSetAwarding" />
                <rad:AjaxUpdatedControl ControlID="pnlCertificates" />
                <rad:AjaxUpdatedControl ControlID="pnlSAC" />
                <rad:AjaxUpdatedControl ControlID="pnlSacImages" />
                <rad:AjaxUpdatedControl ControlID="SettingsCBUserCtrl1" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnCertLibrary">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="btnCertLibrary" />
                <rad:AjaxUpdatedControl ControlID="pnlCertificates" />
                <rad:AjaxUpdatedControl ControlID="pnlCertLibrary" />
                <rad:AjaxUpdatedControl ControlID="SettingsCBUserCtrl1" />
                <rad:AjaxUpdatedControl ControlID="BadgeUserCtrl1" />
                <rad:AjaxUpdatedControl ControlID="RadLV" />
                <rad:AjaxUpdatedControl ControlID="RadGV" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnCertUpload">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="btnCertUpload" />
                <rad:AjaxUpdatedControl ControlID="pnlCertificates" />
                <rad:AjaxUpdatedControl ControlID="pnlCertUpload" />
                <rad:AjaxUpdatedControl ControlID="Panel" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="lbtnBackSAC">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="lbtnBackSAC" />
                <rad:AjaxUpdatedControl ControlID="pnlCertificates" />
                <rad:AjaxUpdatedControl ControlID="pnlSAC" />
                <rad:AjaxUpdatedControl ControlID="pnlSacImages" />
                <rad:AjaxUpdatedControl ControlID="Panel" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="lbtnBackCertLib">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="lbtnBackCertLib" />
                <rad:AjaxUpdatedControl ControlID="pnlCertificates" />
                <rad:AjaxUpdatedControl ControlID="pnlCertUpload" />
                <rad:AjaxUpdatedControl ControlID="Panel" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnRemovePreviewImg">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="badgeImagePreview" LoadingPanelID="localLoadingPanel" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnRemovePreviewImgCert">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="certImagePreview" LoadingPanelID="localLoadingPanel" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="RadAjaxManager1">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="BadgeImage" LoadingPanelID="noLoadingPanel" />
                <rad:AjaxUpdatedControl ControlID="badgeImagePreview" LoadingPanelID="localLoadingPanel" />
                <rad:AjaxUpdatedControl ControlID="CertificateImage" LoadingPanelID="noLoadingPanel" />
                <rad:AjaxUpdatedControl ControlID="certImagePreview" LoadingPanelID="localLoadingPanel" />
                <rad:AjaxUpdatedControl ControlID="Panel" LoadingPanelID="localLoadingPanel" />
                <rad:AjaxUpdatedControl ControlID="pnlCertificates" LoadingPanelID="localLoadingPanel" />
                <rad:AjaxUpdatedControl ControlID="pnlBadgeUpload" LoadingPanelID="localLoadingPanel" />
                <rad:AjaxUpdatedControl ControlID="pnlCertUpload" LoadingPanelID="localLoadingPanel" />
                <rad:AjaxUpdatedControl ControlID="rtbCertificateName" />
                <rad:AjaxUpdatedControl ControlID="rtbCertDesc" />
                <rad:AjaxUpdatedControl ControlID="rbtDescription" />
                <rad:AjaxUpdatedControl ControlID="rtbBadgeName" />
            </UpdatedControls>
        </rad:AjaxSetting>
    </AjaxSettings>
</rad:RadAjaxManagerProxy>

<asp:HiddenField runat="server" ID="hfBadgeFileError" Value="<%$ Resources:LocalizedResource, InvalidFileTypeAllowedFilesareJPGJPEGPNG %>" />

<rad:RadScriptBlock ID="rsbbb1" runat="server">
    <script type="text/javascript">
    
        function openSideContent(evt, actionName, obj) {
            var btnName = $(obj).parent().attr('id');

            $('.tabcontent').css({ 'display': 'none' });
            $('.tablinks').removeClass('active');
            $('.tabcontent').removeClass('active');
            $('.tab-action').removeClass('active');
            $('#' + btnName).addClass('active');
            $('#' + actionName).css({ 'display': 'block' }).addClass('active');


        }

        function createAction(action, obj) {
            var obj_id = $(obj).attr('id');

            $('.tab-action').removeClass('active');
            $('#' + obj_id).addClass('active');

            if (action == '1') {
            }
        }
    </script>
    <script type="text/javascript">
        //badges upload
        function fileUploadedBadge(sender, args) {
            var upload = $find("<%= BadgeImage.ClientID %>");
            var inputs = upload.getUploadedFiles();

            if (!upload.isExtensionValid(inputs[0]))
                alert("extension is invalid!");
           
            var radManager = $find('<%= RadAjaxManager.GetCurrent(Page).ClientID %>');
            radManager.ajaxRequest();
            
            setTimeout(function () {
                hideAllLocalLoading();
            }, 3000);

        }

        function fileSelectedBadge(sender, args) {
            debugger;
            var badgeName = $find("<%= rtbBadgeName.ClientID%>");
            var errorMessage = $("#<%= hfBadgeFileError.ClientID %>").val();
            if (badgeName.get_editValue() != "") {
                showImageDropzonePreviewLoading();
                var fileExtention = args.get_fileName().substring(args.get_fileName().lastIndexOf('.') + 1, args.get_fileName().length);
                if (args.get_fileName().lastIndexOf('.') != -1) {//this checks if the extension is correct
                    if (sender.get_allowedFileExtensions().toLowerCase().indexOf(fileExtention.toLowerCase()) == -1) {
                        $("#" + "<%= badgeImagePreview.ClientID%>").attr('src', "").hide();
                        removePreviewImage();
                        radalert(errorMessage, 330, 180, "Error", "");
                    }
                    else {
                        console.log("File Supported.");
                    }
                }
                else {
                    console.log("not correct extension.");

                }
            }
            else {
                hideAllLocalLoading();
                radalert("Please Input BadgeName Before uploading Image", 330, 180, "Error", "");
            }

        }

        function showImageDropzonePreviewLoading() {
            var panel = $find("<%= localLoadingPanel.ClientID %>");
            panel.show("badgeImagePreview");
        }

        function hideAllLocalLoading() {
            $(".Loading2").hide();
            console.log("hide loading");
        }

        function dropzoneEnter() {

            if ($("#" + "<%= badgeImagePreview.ClientID %>").is('[src]')) {
                $(".tc-overlay").css({ "display": "block", "opacity": "1" });
            }
        }

        function dropzoneExit() {
            var a = $("#" + "<%= badgeImagePreview.ClientID %>").attr('src');
            $(".tc-overlay").css({ "display": "none", "opacity": "0" });

        }

        function removePreviewImage() {
            try {
                var upload = $find("<%= BadgeImage.ClientID %>");
                upload.deleteAllFileInputs();
                hideAllLocalLoading();
            } catch (e) {

            }
        }

        function openFileUploaderBadge() {
            //            sender.set_autoPostBack(false);
            var uploader = $("#<%= BadgeImage.ClientID %> .ruFileInput");
            uploader.click();
        }
		
		
		
        
        
    
    </script>
    <script type="text/javascript">
        //cetificates upload
        function fileUploadedCert(sender, args) {
            var upload = $find("<%= CertificateImage.ClientID %>");
            var inputs = upload.getUploadedFiles();

            if (!upload.isExtensionValid(inputs[0]))
                alert("extension is invalid!");


            var radManager = $find('<%= RadAjaxManager.GetCurrent(Page).ClientID %>');
            radManager.ajaxRequest();


            setTimeout(function () {
                hideAllLocalLoading();
            }, 3000);

        }

        function fileSelectedCert(sender, args) {
            var certName = $find("<%= rtbCertificateName.ClientID%>");
            if (certName.get_displayValue() != "- Certificate Name -") {
             showImageDropzonePreviewLoadingCert();
             var fileExtention = args.get_fileName().substring(args.get_fileName().lastIndexOf('.') + 1, args.get_fileName().length);
             if (args.get_fileName().lastIndexOf('.') != -1) {//this checks if the extension is correct
                 if (sender.get_allowedFileExtensions().toLowerCase().indexOf(fileExtention.toLowerCase()) == -1) {
                     $("#" + "<%= certImagePreview.ClientID%>").attr('src', "").hide();
                     removePreviewImageCert();
                     radalert("Invalid File Type.<br> Allowed Files are JPG, GIF, PNG.", 330, 180, "Error", "");
                 }
                 else {
                     console.log("File Supported.");
                 }
             }
             else {
                 console.log("not correct extension.");

             }
         }
         else {
             hideAllLocalLoading();
             radalert("Please Input Certificate Name Before uploading Image", 330, 180, "Error", "");
         }

        }

        function showImageDropzonePreviewLoadingCert() {
            var panel = $find("<%= localLoadingPanel.ClientID %>");
            panel.show("certImagePreview");
        }

        function hideAllLocalLoading() {
            $(".Loading2").hide();
            console.log("hide loading");
        }

        function dropzoneEnterCert() {

            if ($("#" + "<%= certImagePreview.ClientID %>").is('[src]')) {
                $(".tc-overlay").css({ "display": "block", "opacity": "1" });
            }
        }

        function dropzoneExitCert() {
            var a = $("#" + "<%= certImagePreview.ClientID %>").attr('src');
            $(".tc-overlay").css({ "display": "none", "opacity": "0" });

        }

        function removePreviewImageCert() {
            try {
                var upload = $find("<%= CertificateImage.ClientID %>");
                upload.deleteAllFileInputs();
                hideAllLocalLoading();
            } catch (e) {

            }
        }

        function openFileUploaderCert() {
            //            sender.set_autoPostBack(false);
            var uploader = $("#<%= CertificateImage.ClientID %> .ruFileInput");
            uploader.click();
        }

        //CHECK LENGTH GIVEN BY THE PARAMETER
        function CheckMaxLength(sender, Maxlength, Proc) {
            var length = $(sender).val().length;
            if (sender.value.length > Maxlength) {
                sender.value = sender.value.substr(0, Maxlength);
            }

            var length = Maxlength - length;
            if (Proc == 'Badge')
                $('#<%= txtBadgeChars.ClientID %>').text(length);
            else
                $('#<%= txtCertChars.ClientID %>').text(length);

        }     
		
		
		
		
    </script>
</rad:RadScriptBlock>
<rad:RadAjaxLoadingPanel ID="localLoadingPanel" runat="server" CssClass="Loading2"
    Transparency="25" />
<rad:RadAjaxLoadingPanel ID="noLoadingPanel" runat="server" CssClass="Loading-Empty" />
