﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;


public partial class CourseBuilderBadges : System.Web.UI.UserControl
{
    private static string global_badgeImage;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            var courseID = Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"].ToString()));
            ddSelect.DataSource = null;
            ddSelect.DataBind();
            ddlCertAwarding.DataSource = null;
            ddlCertAwarding.DataBind();

            ddSelect.DataSource = DataHelper.GetAllBadges(1, courseID);
            ddSelect.DataTextField = "BadgeName";
            ddSelect.DataValueField = "ID";
            ddSelect.DataBind();

            ddlCertAwarding.DataSource = DataHelper.GetAllBadges(2, courseID);
            ddlCertAwarding.DataTextField = "BadgeName";
            ddlCertAwarding.DataValueField = "ID";
            ddlCertAwarding.DataBind();

        }
    }
    protected void Button3_Click(object sender, EventArgs e)
    {
        pnlBadgeImages.Visible = true;
        var fileName = Request.QueryString["CourseID"].ToString();
        var courseID = Convert.ToInt32(Utils.Decrypt(fileName));
        int id = Convert.ToInt32(ddSelect.SelectedValue);
        DataSet ds = DataHelper.GetBadge(id, 1, courseID);

        ImgBadge.ImageUrl = "~/Media/Uploads/Badges/" + fileName + "/" + ds.Tables[0].Rows[0]["BadgeImgBig"].ToString();
    }
    protected void btnSetAwardCondition_Click(object sender, EventArgs e)
    {
        pnlSACBadge.Visible = true;
        Panel.Visible = false;
        btnBadgeSetAwarCond.Visible = true;
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        pnlSACBadge.Visible = false;
        Panel.Visible = false;
        pnlBadgeImages.Visible = false;
        pnlBadgeLib.Visible = true;

        Session["SessionFileType"] = 1;

        LoadData(1);
        HideControls(true, false);
    }
    protected void LinkBtnBack_Click(object sender, EventArgs e)
    {
        pnlSACBadge.Visible = false;
        pnlBadgeImages.Visible = false;
        pnlBadgeUpload.Visible = false;
        Panel.Visible = true;

        HideControls(false, true);
    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        Panel.Visible = true;
        pnlSACBadge.Visible = false;
        pnlBadgeImages.Visible = false;
        pnlBadgeLib.Visible = false;
        pnlBadgeUpload.Visible = false;

        HideControls(false, true);
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        pnlSACBadge.Visible = false;
        pnlBadgeImages.Visible = false;
        pnlBadgeLib.Visible = false;
        Panel.Visible = false;
        pnlBadgeUpload.Visible = true;

        HideControls(false, true);

    }
    protected void HideControls(bool badge, bool cbUser)
    {
        (this.Parent.Parent).FindControl("BadgeUserCtrl1").Visible = badge;
        (this.Parent.Parent).FindControl("SettingsCBUserCtrl1").Visible = cbUser;
    }
    protected void upload_FileUploaded(object sender, FileUploadedEventArgs e)
    {
        if (rtbBadgeName.Text.Trim() != "")
        {
            if (e.IsValid)
            {
                var courseId = Convert.ToInt32(DataHelper.Decrypt(Request.QueryString["CourseID"]));
                var FileName = badgeCertsCourse(e.File, "Badges");
                //badgeImagePreview.ImageUrl = "~/Media/Uploads/Badges/" + global_badgeImage;
                var ans = DataHelper.isFolderName(rtbBadgeName.Text.Trim(), courseId, 1, "image");
                if (FileName != "error" && ans)
                {
                    var currentCIM = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
                    DataHelper.insertBadges(courseId, rtbBadgeName.Text.Trim(), rbtDescription.Text.Trim(), FileName, null, currentCIM, 1);
                    rbtDescription.Text = string.Empty;
                    rtbBadgeName.Text = string.Empty;
                    RadWindowManager1.RadAlert("  Badges Successfully Uploaded", 330, 180, "Message", "");
                }
                else
                    RadWindowManager1.RadAlert("  Duplicate Badge Name ", 330, 180, "Error", "");
            }
            else
            {
                BadgeImage.UploadedFiles.Clear();
                //RadWindowManager1.RadAlert("Invalid File. Allowed Files (jpeg,jpg,gif,png)", null, null, "Error Message", "");
            }
        }

    }
    protected void upload_FileUploadedCert(object sender, FileUploadedEventArgs e)
    {
        if (rtbCertificateName.Text.Trim() != "")
        {
            if (e.IsValid)
            {
                var courseId = Convert.ToInt32(DataHelper.Decrypt(Request.QueryString["CourseID"]));
                var FileName = badgeCertsCourse(e.File, "Certs");
                var ans = DataHelper.isFolderName(rtbCertificateName.Text.Trim(), courseId, 2, "image");
                //certImagePreview.ImageUrl = "~/Media/Uploads/Certs/" + global_badgeImage;
                if (FileName != "error" && ans)
                {
                    var currentCIM = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
                    DataHelper.insertBadges(courseId, rtbCertificateName.Text.Trim(), rtbCertDesc.Text.Trim(), FileName, null, currentCIM, 2);
                    rtbCertificateName.Text = string.Empty;
                    rtbCertDesc.Text = string.Empty;
                    RadWindowManager1.RadAlert("  Certificate Successfully Uploaded", 330, 180, "Message", "");
                }
                else                
                    RadWindowManager1.RadAlert("  Duplicate Certificate Name ", 330, 180, "Error", "");
                
            }
            else
            {
                BadgeImage.UploadedFiles.Clear();
            }
        }

    }
    protected string badgeCertsCourse(UploadedFile f, string Type)
    {
        try
        {

            string error = null;
            var ImageName = Request.QueryString["CourseID"];
            var courseID = Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"]));
            ImageName = ImageName.Replace(" ", "");
            ImageName = ImageName.Replace("/", "");
            if (string.IsNullOrWhiteSpace(error))
            {
                string uploadFolder;

                if (HttpContext.Current.Request.Url.Host == "localhost")
                    uploadFolder = Request.PhysicalApplicationPath + "Media\\Uploads\\" + Type + "\\" + ImageName;
                else
                    uploadFolder = HttpRuntime.AppDomainAppPath + "Media\\Uploads\\" + Type + "\\" + ImageName;

                //string directoryPath = Server.MapPath("") + "/Media/Uploads/" +Type +"/" + ImageName;

                if (!Directory.Exists(uploadFolder))
                    Directory.CreateDirectory(uploadFolder);

                //if (f.ContentType == "image/jpeg" || f.ContentType == "image/png" || f.ContentType == "image/jpg")
                //{
                if (f.ContentLength < 1024000000)
                {
                    Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                    string extension = Path.GetExtension(f.FileName);
                    //f.SaveAs(uploadFolder + , true);

                    //DataHelper.InsertCert(googleId, CertName, "", Convert.ToDateTime(txtDateAcquired.SelectedDate), null, unixTimestamp.ToString() + extension, true);
                    //var fileName = DataHelper.getFileID() + "_" + Convert.ToString(courseID) + "_" + f.FileName.Replace(" ","");
                    var fileName = unixTimestamp.ToString() + f.GetExtension();
                    f.SaveAs(uploadFolder + "/"+ fileName, true);
                    global_badgeImage = fileName;
                }
                else
                {
                    global_badgeImage = "error";
                    RadWindowManager1.RadAlert("  File size is too large, please select another Image!", 330, 180, "Image too Large", "");
                }
                //}
                //else
                //{
                //    global_badgeImage = "error";
                //    RadWindowManager1.RadAlert("Error file format please select another one!", 330, 180, "Invalid File Format", "");
                //}
            }
        }
        catch
        {
            global_badgeImage = "error";
        }
        return global_badgeImage;
    }
    protected void btnRemovePreviewImg_Click(object sender, EventArgs e)
    {

        badgeImagePreview.DataValue = null;
        global_badgeImage = "No_image.jpg";
        badgeImagePreview.ImageUrl = "";
        //deleteImageInServer(newCourseID);
        //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", Utils.callClientScript("hideAllLocalLoading"), true);

    }
    protected void btnRemovePreviewImgCert_Click(object sender, EventArgs e)
    {

        certImagePreview.DataValue = null;
        global_badgeImage = "No_image.jpg";
        certImagePreview.ImageUrl = "";
        //deleteImageInServer(newCourseID);
        //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", Utils.callClientScript("hideAllLocalLoading"), true);

    }
    protected void ddlBadgeCriteria_SelectedIndexChanged(object sender, EventArgs e)
    {
        var criteriaID = ddlBadgeCriteria.SelectedValue.ToString();
        var badgeID = ddSelect.SelectedValue.ToString();
        var currentCIM = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
        DataHelper.insertAwardingConditions(Convert.ToInt32(badgeID), Convert.ToInt32(criteriaID), currentCIM, 1);
    }
    protected void LoadData(int FileType)
    {
        var fileName = Request.QueryString["CourseID"].ToString();
        fileName = fileName.Replace(" ", "");
        fileName = fileName.Replace("/", "");
        var courseID = Convert.ToInt32(Utils.Decrypt(fileName));
        RadListView rlBadge = (this.Parent.Parent).FindControl("BadgeUserCtrl1").FindControl("pnlMainBadge").FindControl("pnlLvResult").FindControl("RadLV") as RadListView;
        RadGrid rgBadge = (this.Parent.Parent).FindControl("BadgeUserCtrl1").FindControl("pnlMainBadge").FindControl("pnlGridResult").FindControl("RadGV") as RadGrid;
        Label BadgeMsg = (this.Parent.Parent).FindControl("BadgeUserCtrl1").FindControl("BadgeMsg") as Label;
        HiddenField hidFileType = (this.Parent.Parent).FindControl("BadgeUserCtrl1").FindControl("hidFileType") as HiddenField;
        var ds = DataHelper.GetFolderAndBadges(FileType, courseID);
        var path = string.Empty;
        foreach (DataRow dr in ds.Tables[0].Rows)
        {
            if (dr["Fname"].ToString() == "FOLDER")
                path = "Badges";
            else
            {
                path = FileType == 1 ? "Badges" : "Certs";
                path = path + "/" + fileName;
            }
            dr["IconImg"] = path + "/" + dr["IconImg"].ToString();
        }
        rgBadge.DataSource = ds;
        rgBadge.DataBind();
        rlBadge.DataSource = ds;
        rlBadge.DataBind();
        hidFileType.Value = Convert.ToString(FileType);
        BadgeMsg.Text = "There are " + ds.Tables[0].Rows.Count.ToString() + " items in this library";

    }
    protected void ddlCertCriteria_SelectedIndexChanged(object sender, EventArgs e)
    {
        var criteriaID = ddlCertCriteria.SelectedValue.ToString();
        var badgeID = ddlCertAwarding.SelectedValue.ToString();
        var currentCIM = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
        DataHelper.insertAwardingConditions(Convert.ToInt32(badgeID), Convert.ToInt32(criteriaID), currentCIM, 2);
    }
    protected void btnApplyConditions_onclick(object sender, EventArgs e)
    { }
    protected void btnCertSetAwardCondition_Click(object sender, EventArgs e)
    {
        btnSetAwarding.Visible = true;
        //HidePanelsCert(certs,SAC,SacImages,CertLibrary,CertUpload)
        HidePanelsCert(false, true, false, false, false);
    }
    protected void btnSetAwarding_Click(object sender, EventArgs e)
    {
        try
        {
            var fileName = Request.QueryString["CourseID"].ToString();
            pnlSacImages.Visible = true;
            var courseID = Convert.ToInt32(Utils.Decrypt(fileName));
            int id = Convert.ToInt32(ddlCertAwarding.SelectedValue);
            DataSet ds = DataHelper.GetBadge(id, 2, courseID);

            imgCert.ImageUrl = "~/Media/Uploads/Certs/" + fileName + "/" + ds.Tables[0].Rows[0]["BadgeImgBig"].ToString();
        }
        catch
        {
            imgCert.ImageUrl = "~/Media/Images/noimage.png";
        }
    }
    protected void lbtnBackCertLib_Click(object sender, EventArgs e)
    {
        HidePanelsCert(true, false, false, false, false);
        HideControls(false, true);

    }
    protected void btnCertLibrary_Click(object sender, EventArgs e)
    {
        Session["SessionFileType"] = 2;
        LoadData(2);
        HidePanelsCert(false, false, false, true, false);
        HideControls(true, false);
    }
    protected void LinkBtnCertBack_Click(object sender, EventArgs e)
    {
        HidePanelsCert(true, false, false, false, false);
        HideControls(false, true);
    }
    protected void btnCertUpload_Click(object sender, EventArgs e)
    {
        HidePanelsCert(false, false, false, false, true);
    }
    protected void HidePanelsCert(bool certs, bool SAC, bool SacImages, bool CertLibrary, bool CertUpload)
    {
        pnlSAC.Visible = SAC;
        pnlSacImages.Visible = SacImages;
        pnlCertLibrary.Visible = CertLibrary;
        pnlCertUpload.Visible = CertUpload;
        pnlCertificates.Visible = certs;
    }
}
