﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CourseBuilderDetails.ascx.cs"
    Inherits="CourseBuilderDetails" %>
<rad:RadAjaxLoadingPanel ID="fullPageLoading" runat="server" Transparency="25" IsSticky="true"
    CssClass="Loading" />
<rad:RadAjaxLoadingPanel ID="localLoading" runat="server" MinDisplayTime="1000" CssClass="Loading2"
    Transparency="25" />
<rad:RadAjaxLoadingPanel ID="noLoadingPanel" runat="server" CssClass="Loading-Empty"
    Transparency="25" />
<rad:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <rad:AjaxSetting AjaxControlID="btnEditCourseDetails">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlCourseDetails" UpdatePanelRenderMode="Inline" />
                <rad:AjaxUpdatedControl ControlID="pnlEditCourseDetails" UpdatePanelRenderMode="Inline" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="lbtnClose">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlCourseDetails" UpdatePanelRenderMode="Inline" />
                <rad:AjaxUpdatedControl ControlID="pnlEditCourseDetails" UpdatePanelRenderMode="Inline" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnRemoveImage">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlCourseDetails" UpdatePanelRenderMode="Inline" />
                <rad:AjaxUpdatedControl ControlID="pnlEditCourseDetails" UpdatePanelRenderMode="Inline" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="lbtnSaveButton">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlCourseDetails" UpdatePanelRenderMode="Inline" />
                <rad:AjaxUpdatedControl ControlID="pnlEditCourseDetails" UpdatePanelRenderMode="Inline" />
                <rad:AjaxUpdatedControl ControlID="imagePreviewContainer" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="rcbCategory">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="ddtSubcategory" LoadingPanelID="localLoading" />
                <rad:AjaxUpdatedControl ControlID="pnlLevelContainer" LoadingPanelID="noLoadingPanel" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="rcbReqCourse">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="ddtReqSubcategory" UpdatePanelRenderMode="Inline"
                    LoadingPanelID="localLoading" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="RadAjaxManagerProxy1">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="CourseImage" LoadingPanelID="noLoadingPanel" />
                <rad:AjaxUpdatedControl ControlID="courseImagePreview" LoadingPanelID="noLoadingPanel" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnSelectCourse">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="btnSelectCourse" UpdatePanelRenderMode="Inline"
                    LoadingPanelID="noLoadingPanel" />
                <rad:AjaxUpdatedControl ControlID="lbAvailableCourses" UpdatePanelRenderMode="Inline"
                    LoadingPanelID="localLoading" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="AddCourses">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="AddCourses" UpdatePanelRenderMode="Inline" LoadingPanelID="noLoadingPanel" />
                <rad:AjaxUpdatedControl ControlID="lbAssignedCourses" UpdatePanelRenderMode="Inline"
                    LoadingPanelID="localLoading" />
            </UpdatedControls>
        </rad:AjaxSetting>
    </AjaxSettings>
</rad:RadAjaxManagerProxy>
<div class="sideContent bg-color-light-gray ten-px-padding">
    <asp:Panel ID="pnlCourseDetails" runat="server" CssClass="course-details container">
        <div class="row">
            <div class="col-md-12 no-padding">
                <span class="pull-right">
                    <h4>
                        <asp:LinkButton ID="btnEditCourseDetails" Text="text" runat="server" OnClick="btnEditCourseDetails_Click">
                         <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        </asp:LinkButton>
                    </h4>
                </span>
                <h5 class="font-bold">
                    <asp:Label runat="server" Text="<%$ Resources: LocalizedResource, CourseDetails %>" /> </h5>
                <table class="table-padding-md table table-borderless" border="0" style="font-size: 13px;">
                    <tr>
                        <td style="font-size: 12px; font-weight: bold;">
                            <asp:Label ID="Label2" runat="server" Text="<%$ Resources: LocalizedResource, CourseTitle %>" />
                        </td>
                        <td>
                            <asp:Label ID="LblCourseTitle" AutoSize="False" runat="server" Width="150px" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 12px; font-weight: bold;">
                            <asp:Label ID="Label3" runat="server" Text="<%$ Resources: LocalizedResource, Category %>" />
                        </td>
                        <td>
                            <asp:Label ID="LblCategory" runat="server" CssClass="font-12px" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 12px; font-weight: bold;">
                            <asp:Label ID="Label4" runat="server" Text="<%$ Resources: LocalizedResource, Subcategory %>" />
                        </td>
                        <td>
                            <asp:Label ID="LblSubCat" runat="server" CssClass="font-12px" />
                        </td>
                    </tr>
                    <tr runat="server" id="rowjoblevel">
                        <td style="font-size: 12px; font-weight: bold;">
                            <asp:Label ID="Label5" runat="server" Text="<%$ Resources: LocalizedResource, JobLevel %>" />
                        </td>
                        <td>
                            <asp:Label ID="lblJobLevel" runat="server" CssClass="font-12px" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 12px; font-weight: bold;">
                            <asp:Label ID="Label6" runat="server" Text="<%$ Resources: LocalizedResource, CourseId %>" />
                        </td>
                        <td>
                            <asp:Label ID="LblCourseID" runat="server" CssClass="font-12px" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 12px; font-weight: bold;">
                            <asp:Label ID="Label7" runat="server" Text="<%$ Resources: LocalizedResource, Description %>" />
                        </td>
                        <td>
                            <%--<asp:Label ID="LblDesc" runat="server" CssClass="font-12px" />--%>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:TextBox runat="server" ID="LblDesc" TextMode="MultiLine" Height="70px" CssClass="font-12px"
                                MaxLength="1500" Style="resize: none;" Enabled="false" Width="100%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 12px; font-weight: bold;">
                            <asp:Label ID="Label8" runat="server" Text="<%$ Resources: LocalizedResource, DateCreated %>" />
                        </td>
                        <td>
                            <asp:Label ID="LblCreateDate" runat="server" CssClass="font-12px" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 12px; font-weight: bold;">
                            <asp:Label ID="Label9" runat="server" Text="<%$ Resources: LocalizedResource, StartDatetime %>" />
                        </td>
                        <td>
                            <asp:Label ID="LblStartDate" runat="server" CssClass="font-12px" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 12px; font-weight: bold;">
                            <asp:Label ID="Label10" runat="server" Text="<%$ Resources: LocalizedResource, EndDatetime %>" />
                        </td>
                        <td>
                            <asp:Label ID="LblEndDate" runat="server" CssClass="font-12px" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 12px; font-weight: bold;">
                            <asp:Label ID="Label11" runat="server" Text="<%$ Resources: LocalizedResource, Duration %>" />
                        </td>
                        <td>
                            <asp:Label ID="LblDuration" runat="server" CssClass="font-12px" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 12px; font-weight: bold;">
                            <asp:Label ID="Label12" runat="server" Text="<%$ Resources: LocalizedResource, Author %>" />
                        </td>
                        <td>
                            <asp:Label ID="LblAuthor" runat="server" CssClass="font-12px" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 12px; font-weight: bold;">
                            <asp:Label ID="Label13" runat="server" Text="<%$ Resources: LocalizedResource, DepartmentOwner %>" />
                        </td>
                        <td>
                            <asp:Label ID="LblDepartmentOwner" runat="server" CssClass="font-12px" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 12px; font-weight: bold;">
                            <asp:Label ID="Label14" runat="server" Text="<%$ Resources: LocalizedResource, RequiredProgram %>" />
                        </td>
                        <td>
                            <asp:Label ID="lblRequiredProgram1" runat="server" CssClass="font-12px" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 12px; font-weight: bold;">
                            <asp:Label ID="Label15" runat="server" Text="<%$ Resources: LocalizedResource, RequiredCourse %>" />
                        </td>
                        <td>
                            <ul class="list-unstyled">
                                <asp:Repeater ID="rptRequiredCourse" runat="server">
                                    <ItemTemplate>
                                        <li>
                                            <asp:Label ID="Label1" Text='<%# Eval("title") %>' runat="server" />
                                        </li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 12px; font-weight: bold;">
                            <asp:Label ID="Label16" runat="server" Text="<%$ Resources: LocalizedResource, RequiredOfYearsInTenure %>" />
                        </td>
                        <td>
                            <asp:Label ID="LblRequiredTenureYrs" runat="server" CssClass="font-12px" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 12px; font-weight: bold;">
                            <asp:Label ID="Label17" runat="server" Text="<%$ Resources: LocalizedResource, RequiredOfYearsInRole %>" />
                        </td>
                        <td>
                            <asp:Label ID="LblRequiredRoleYrs" runat="server" CssClass="font-12px" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 12px; font-weight: bold;">
                            <asp:Label ID="Label18" runat="server" Text="<%$ Resources: LocalizedResource, AccessType %>" />
                        </td>
                        <td>
                            <asp:Label ID="LblAccessType" runat="server" CssClass="font-12px" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 12px; font-weight: bold;">
                            <asp:Label ID="Label19" runat="server" Text="<%$ Resources: LocalizedResource, CourseImage %>" />
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="font-bold" colspan="2">
                            <asp:Image ID="ImgCourse" runat="server" Width="250" CssClass="img-responsive margin-auto"
                                onerror="this.src='Media/Uploads/CourseImg/No_image.jpg'" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlEditCourseDetails" runat="server" CssClass="course-details table table-borderless"
        Visible="false">
        <div class="row">
            <div class="container" id="createCourseContainer" style="font-size: 12px;">
                <form id="frmAddCourse">
                <div class="col-md-12" style="padding-bottom: 20px;">
                    <span class="pull-right">
                        <h4>
                            <asp:LinkButton ID="lbtnSave" Text="text" runat="server" OnClientClick="saveCourseDetails(); return false;">
                         <i class="fa fa-save" aria-hidden="true" style="font-size: 20px; color: Black !important;"></i>
                            </asp:LinkButton>
                            <asp:LinkButton ID="lbtnClose" Text="text" runat="server" OnClick="btnCloseCourseDetails_Click"
                                Style="margin-left: 10px;">
                         <i class="fa fa-close" aria-hidden="true" style="font-size: 20px; color: Black !important;"></i>
                            </asp:LinkButton>
                        </h4>
                    </span>
                    <h5 class="font-bold pull-left">
                        <asp:Label ID="Label20" runat="server" Text="<%$ Resources: LocalizedResource, CourseDetails %>" />
                        <asp:Label ID="lblInvalidError" Visible="false" Font-Italic="true" Text="<%$ Resources: LocalizedResource, Pleaseenterallrequiredfields %>"
                            ForeColor="Red" runat="server"></asp:Label>
                    </h5>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-sm-12 create-course-col-1">
                        <div class="row">
                            <div class="col-md-4">
                                <asp:RequiredFieldValidator ID="rfvTxtCourseTitle" runat="server" ErrorMessage="*"
                                    ControlToValidate="txtCourseTitle" SetFocusOnError="True" Display="Dynamic" ValidationGroup="addCourse"
                                    ForeColor="Red" CssClass="pull-left displayerror" />
                                <asp:Label ID="Label21" runat="server" Text="<%$ Resources: LocalizedResource, CourseTitle %>" />
                            </div>
                            <div class="col-md-7 no-padding pull-left form-group">
                                <asp:TextBox ID="txtCourseTitle" runat="server" CssClass="form-control" ToolTip="^<%$ Resources: LocalizedResource, EnterCourseTitle %>"
                                    MaxLength="250" ViewStateMode="Enabled" onkeyup="CheckMaxLength(this,250);"></asp:TextBox>
                                <asp:HiddenField runat="server" ID="hfCourseType" Value="" />
                                <label class="text-danger text-right">
                                    <span runat="server" id="txtCourseTitleChars">250</span> 
                                    <asp:Label ID="Label22" runat="server" Text="<%$ Resources: LocalizedResource, CharactersRemaining %>" />
                                </label>
                            </div>
                            <div class="col-md-1">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                                    ControlToValidate="rcbCategory" SetFocusOnError="True" Display="Dynamic" ValidationGroup="addCourse"
                                    ForeColor="Red" CssClass="pull-left displayerror" />
                                <asp:Label ID="Label23" runat="server" Text="<%$ Resources: LocalizedResource, Category %>" />
                            </div>
                            <div class="col-md-7 no-padding pull-left form-group">
                                <rad:RadComboBox ID="rcbCategory" runat="server" DefaultValue="0" AutoPostBack="true"
                                    DataTextField="Category" DataValueField="CategoryID" RenderMode="Lightweight"
                                    Height="250px" EmptyMessage="- Select -" ToolTip="Select Course Category" CssClass="width-fill"
                                    OnSelectedIndexChanged="rcbCategory_SelectedIndexChanged">
                                </rad:RadComboBox>
                            </div>
                            <div class="col-md-1">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*"
                                    ControlToValidate="ddtSubcategory" SetFocusOnError="True" Display="Dynamic" CssClass="displayerror pull-left"
                                    ValidationGroup="addCourse" ForeColor="Red" />
                                <asp:Label ID="Label24" runat="server" Text="<%$ Resources: LocalizedResource, Subcategory %>" />
                            </div>
                            <div class="col-md-7 no-padding pull-left form-group">
                                <rad:RadDropDownTree ID="ddtSubcategory" runat="server" DataTextField="Subcategory"
                                    DataValueField="SubcategoryId" DataFieldID="SubcategoryId" DataFieldParentID="ParentSubcategoryId"
                                    TextMode="Default" RenderMode="Lightweight" ViewStateMode="Enabled" ToolTip="<%$ Resources: LocalizedResource, SelectCourseSubcategory %>"
                                    CssClass="width-fill" Enabled="false" DefaultMessage="- Select -">
                                    <DropDownSettings CloseDropDownOnSelection="true" />
                                </rad:RadDropDownTree>
                            </div>
                            <div class="col-md-1">
                            </div>
                        </div>
                        <asp:Panel ID="pnlLevelContainer" runat="server" CssClass="row" Visible="false">
                            <div class="col-md-4">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                                    ControlToValidate="rcbLevel" SetFocusOnError="True" Display="Dynamic" CssClass="displayerror pull-left"
                                    ValidationGroup="addCourse" ForeColor="Red" />
                                <asp:Label ID="Label46" runat="server" Text="<%$ Resources: LocalizedResource, Level%>" />
                            </div>
                            <div class="col-md-7 no-padding pull-left form-group">
                                <rad:RadComboBox RenderMode="Lightweight" ID="rcbLevel" runat="server" CheckBoxes="true"
                                    CssClass="width-fill" DefaultMessage="<%$ Resources: LocalizedResource, SelectLevel %>" ToolTip="<%$ Resources: LocalizedResource, SelectJobLevelthatcantakethecourse %>">
                                    <Items>
                                        <rad:RadComboBoxItem Text="1" Value="1" />
                                        <rad:RadComboBoxItem Text="2" Value="2" />
                                        <rad:RadComboBoxItem Text="3" Value="3" />
                                        <rad:RadComboBoxItem Text="4" Value="4" />
                                        <rad:RadComboBoxItem Text="5" Value="5" />
                                    </Items>
                                </rad:RadComboBox>
                            </div>
                            <div class="col-md-1">
                            </div>
                        </asp:Panel>
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <asp:Label ID="Label25" runat="server" Text="<%$ Resources: LocalizedResource, Description %>" />
                                <div class="row" style="margin-top: 5px;">
                                    <div class="col-md-12">
                                        <asp:Label ID="lblDescription" runat="server"></asp:Label>
                                        <asp:TextBox ID="txtDescription" runat="server" CssClass="form-control resize-none rounded-corner"
                                            TextMode="MultiLine" Rows="4" ToolTip="<%$ Resources: LocalizedResource, EnterCourseDescription %>" onkeyup="CheckMaxLength(this,1500);"
                                            MaxLength="1500" ondragstart="return false;" ondrop="return false;" placeholder="<%$ Resources: LocalizedResource, ThisCourse %>"></asp:TextBox>
                                        <label class="text-danger text-right">
                                            <span runat="server" id="txtDescriptionChars">1500</span> 
                                            <asp:Label ID="Label26" runat="server" Text="<%$ Resources: LocalizedResource, CharactersRemaining %>" />
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <asp:Label ID="lblDateCreated" Visible="false" Text="*" ForeColor="Red" runat="server"></asp:Label>
                                <asp:Label ID="Label27" runat="server" Text="<%$ Resources: LocalizedResource, DateCreated %>" />
                            </div>
                            <div class="col-md-7 no-padding pull-left form-group">
                                <asp:TextBox ID="txtDateCreated" runat="server" CssClass="form-control" MaxLength="11"
                                    ReadOnly="true" ValidationGroup="addCourse" ToolTip="<%$ Resources: LocalizedResource, DateCreated %>" Text="- Auto pop -"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                    ForeColor="Red" ControlToValidate="dpStartDateTime" SetFocusOnError="True" Display="Dynamic"
                                    CssClass="displayerror pull-left" ValidationGroup="addCourse" Text="*" />
                                <asp:Label ID="Label28" runat="server" Text="<%$ Resources: LocalizedResource, StartDatetime %>" />
                            </div>
                            <div class="col-md-7 no-padding pull-left form-group">
                                <rad:RadDateTimePicker ID="dpStartDateTime" runat="server" RenderMode="Lightweight"
                                    DateInput-ValidationGroup="addCourse" ToolTip="<%$ Resources: LocalizedResource, SetStartDate %>" CssClass="left-calendar-icon width-fill">
                                    <TimeView ID="TimeView1" runat="server" StartTime="00:00" EndTime="23:59" OnClientTimeSelecting="timeSelecting" />
                                    <ClientEvents OnDateSelected="startDateSelected" />
                                    <DateInput ID="DateInput1" runat="server">
                                    </DateInput>
                                </rad:RadDateTimePicker>
                            </div>
                            <div class="col-md-1">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <asp:Label ID="Label29" runat="server" Text="<%$ Resources: LocalizedResource, EndDatetime %>" />
                            </div>
                            <div class="col-md-7 no-padding pull-left form-group">
                                <rad:RadDateTimePicker ID="dpEndDateTime" runat="server" RenderMode="Lightweight"
                                    DateInput-ValidationGroup="addCourse, dateValid" ToolTip="<%$ Resources: LocalizedResource, SetEndDateTime %>"
                                    CssClass="left-calendar-icon width-fill">
                                    <TimeView ID="TimeView2" runat="server" StartTime="00:00" EndTime="23:59" OnClientTimeSelecting="timeSelecting" />
                                    <ClientEvents OnDateSelected="endDateSelected" />
                                    <DateInput ID="DateInput2" runat="server">
                                    </DateInput>
                                </rad:RadDateTimePicker>
                                <asp:CustomValidator ID="cvDate" runat="server" ErrorMessage="<%$ Resources: LocalizedResource, TheEnddateshouldbegreaterthanStartDate %>"
                                    SetFocusOnError="true" Display="Dynamic" ForeColor="Red" ControlToValidate="dpEndDateTime"
                                    ClientValidationFunction="validateStartEndDate" ValidationGroup="dateValid"></asp:CustomValidator>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <asp:Label ID="Label30" runat="server" Text="<%$ Resources: LocalizedResource, Duration %>" />
                            </div>
                            <div class="col-md-3 no-padding pull-left form-group" style="display: inline;">
                                <%--<asp:DropDownList ID="ddlDuration" runat="server" CssClass="form-control width-fill"
                                    ToolTip="Set Course Duration" Width="100px" >
                                </asp:DropDownList>--%>
                                <rad:RadNumericTextBox runat="server" ID="rntbDuration" CssClass="form-control width-fill"
                                    MaxLength="3" MinValue="0" MaxValue="999" ToolTip="<%$ Resources: LocalizedResource, SetCourseDuration %>" Width="100px"
                                    Style="padding-left: 10px;">
                                    <NumberFormat GroupSeparator="" DecimalDigits="0" />
                                </rad:RadNumericTextBox>
                            </div>
                            <div class="col-md-3">
                                <asp:Label ID="Label44" runat="server" Text="<%$ Resources: LocalizedResource, minutes %>" />
                            </div>
                        </div>
                    </div>
                    <%--Col 1 End--%>
                    <div class="col-lg-12 col-sm-12 create-course-col-1">
                        <div class="row">
                            <div class="col-md-4">
                                <asp:Label ID="Label31" runat="server" Text="<%$ Resources: LocalizedResource, Author %>" />
                            </div>
                            <div class="col-md-7 no-padding pull-left form-group">
                                <asp:TextBox ID="txtAuthor" runat="server" CssClass="form-control" ToolTip="<%$ Resources: LocalizedResource, AuthorName %>"
                                    MaxLength="100"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <asp:Label ID="Label32" runat="server" Text="<%$ Resources: LocalizedResource, DepartmentOwner %>" />
                            </div>
                            <div class="col-md-7 no-padding pull-left form-group">
                                <asp:Label ID="lblDepartment" runat="server"></asp:Label>
                                <rad:RadComboBox RenderMode="Lightweight" ID="rcbDepartment" runat="server" DataTextField="Department"
                                    DataValueField="Department" EmptyMessage="<%$ Resources: LocalizedResource, TypeNameOrSelect %>" ToolTip="<%$ Resources: LocalizedResource, SelectDepartment %>"
                                    CssClass="width-fill">
                                    <DefaultItem Text="<%$ Resources: LocalizedResource, SelectDepartment %>" Value="0" />
                                </rad:RadComboBox>
                                <%-- <asp:SqlDataSource ID="dsDepartment" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection%>"
                                    SelectCommand="SELECT DepartmentID, Department FROM [tbl_TranscomUniversity_Lkp_Department] WHERE [HideFromList] = 0">
                                </asp:SqlDataSource>--%>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <asp:Label ID="Label33" runat="server" Text="<%$ Resources: LocalizedResource, RequiredProgram %>" />
                            </div>
                            <div class="col-md-7 no-padding pull-left form-group">
                                <asp:Label ID="lblRequiredProgram" runat="server"></asp:Label>
                                <rad:RadComboBox RenderMode="Lightweight" ID="rcbReqProgram" runat="server" DataTextField="Client"
                                    DataValueField="ClientID" EmptyMessage="<%$ Resources: LocalizedResource, SelectProgram %>" ToolTip="<%$ Resources: LocalizedResource, SelectProgram %>"
                                    CssClass="width-fill" OnClientBlur="OnClientBlurHandler" MarkFirstMatch="true"
                                    AllowCustomText="false" EnableItemCaching="true" Height="250px">
                                </rad:RadComboBox>
                                <%-- EnableAutomaticLoadOnDemand="True" ItemsPerRequest="10" EnableVirtualScrolling="true"--%>
                                <%--  <asp:SqlDataSource ID="dsProgram" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection%>"
                                    SelectCommand="select * from tbl_TranscomUniversity_Lkp_Client where HideFromList=0">
                                </asp:SqlDataSource>--%>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <asp:Label ID="Label34" runat="server" Text="<%$ Resources: LocalizedResource, RequiredCourse %>" />
                            </div>
                            <div class="col-md-7 no-padding pull-left form-group">
                                <asp:Label ID="lblReqCourse" runat="server"></asp:Label>
                                <asp:HiddenField runat="server" ID="hdnReqSubcategory" />
                                <rad:RadComboBox ID="rcbReqCourse" runat="server" RenderMode="Lightweight" AutoPostBack="true"
                                    EmptyMessage="<%$ Resources: LocalizedResource, SelectCategory %>" OnSelectedIndexChanged="rcbReqCourse_IndexChanged"
                                    OnClientSelectedIndexChanged="rcbReqCourse_OnClientSelectedIndexChanged" ToolTip="<%$ Resources: LocalizedResource, SelectCategory %>"
                                    CssClass="width-fill">
                                </rad:RadComboBox>
                                <div id="panelSubReqCategory">
                                    <br />
                                    <rad:RadDropDownTree ID="ddtReqSubcategory" runat="server" DataTextField="Subcategory"
                                        DataValueField="SubcategoryId" DataFieldID="SubcategoryId" DataFieldParentID="ParentSubcategoryId"
                                        TextMode="Default" RenderMode="Lightweight" OnClientBlur="OnClientBlurHandler"
                                        ToolTip="<%$ Resources: LocalizedResource, SelectSubcategory %>" CssClass="width-fill" DefaultMessage="<%$ Resources: LocalizedResource, SelectSubcategory %>">
                                        <DropDownSettings CloseDropDownOnSelection="true" />
                                    </rad:RadDropDownTree>
                                </div>
                                <div id="panelSelectCourse">
                                    <br />
                                    <rad:RadButton ID="btnSelectCourse" runat="server" Text="<%$ Resources: LocalizedResource, SelectCourse %>" OnClientClicking="validateCategoryAndSub"
                                        OnClick="btnSelectCourse_Click" RenderMode="Lightweight" Skin="Bootstrap">
                                    </rad:RadButton>
                                </div>
                                <div id="panelAssignedCourse">
                                    <br />
                                    <rad:RadListBox ID="lbAssignedCourses" runat="server" Height="100px" Width="100%"
                                        RenderMode="Lightweight" EmptyMessage="<%$ Resources: LocalizedResource, NoSelectedCourses %>" AllowDelete="true" AutoPostBackOnTransfer="true"
                                        ToolTip="<%$ Resources: LocalizedResource, SelectRequiredCourses %>">
                                        <ButtonSettings ShowDelete="true" ShowReorder="false" HorizontalAlign="Right" VerticalAlign="Top" />
                                    </rad:RadListBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <asp:Label ID="Label35" runat="server" Text="<%$ Resources: LocalizedResource, RequiredYearsOfTenure %>" />
                            </div>
                            <div class="col-md-3 no-padding pull-left form-group">
                                <asp:DropDownList ID="ddlTenureYears" runat="server" CssClass="form-control width-fill"
                                    ToolTip="<%$ Resources: LocalizedResource, SelectYearsofTenure %>" Style="margin-left: -25%;">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <asp:Label ID="Label36" runat="server" Text="<%$ Resources: LocalizedResource, RequiredYearsInRole %>" />
                            </div>
                            <div class="col-md-3 no-padding pull-left form-group">
                                <asp:DropDownList ID="ddlRoleYears" runat="server" CssClass="form-control width-fill"
                                    ToolTip="<%$ Resources: LocalizedResource, SelectYearsofRole %>" Style="margin-left: -25%;">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <asp:Label ID="Label37" runat="server" Text="<%$ Resources: LocalizedResource, AccessType %>" />
                            </div>
                            <div class="col-md-7 no-padding pull-left form-group">
                                <asp:DropDownList ID="ddlAccessType" runat="server" CssClass="form-control" ToolTip="Set Access Type">
                                    <asp:ListItem Value="Internal" Text="<%$ Resources: LocalizedResource, Internal %>"></asp:ListItem>
                                    <asp:ListItem Value="External" Text="<%$ Resources: LocalizedResource, External %>"></asp:ListItem>
                                    <asp:ListItem Value="Both" Text="<%$ Resources: LocalizedResource, Both %>"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="lblAccessMode" Visible="false" Text="*" ForeColor="Red" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <asp:Label ID="Label38" runat="server" Text="<%$ Resources: LocalizedResource, CourseImage %>" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row img-dropzone-md">
                                    <div class="dropzone-empty" style="padding-top: 2em;">
                                        <asp:Image ID="Image4" ImageUrl="~/Media/Images/icon-upload.png" runat="server" AlternateText="Upload Image"
                                            Height="2.875em" Widt h="2.875em" />
                                        <br />
                                        <p>
                                            <asp:Label ID="Label39" runat="server" Text="<%$ Resources: LocalizedResource, DragImageFileHere %>" />
                                        </p>
                                    </div>
                                    <rad:RadAsyncUpload ID="CourseImage" runat="server" MultipleFileSelection="Disabled"
                                        ViewStateMode="Enabled" MaxFileInputsCount="1" ControlObjectsVisibility="None"
                                        Width="100%" HideFileInput="false" ToolTip="<%$ Resources: LocalizedResource, SelectCourseImage %>" CssClass="CourseImage display-none"
                                        DropZones=".img-dropzone-md" ForeColor="#ddd" OnClientFilesUploaded="fileUploaded"
                                        OnClientFileSelected="fileSelected" OnFileUploaded="upload_FileUploaded" AllowedFileExtensions="jpeg,jpg,gif,png">
                                        <%-- <FileFilters>
                                                                <rad:FileFilter Description="Images(jpeg;jpg;gif;png)" Extensions="jpeg,jpg,gif,png" />
                                                            </FileFilters>--%>
                                    </rad:RadAsyncUpload>
                                    <div id="DropzonePreview" class="dropzone-preview" onmouseover="dropzoneEnter()"
                                        onmouseout="dropzoneExit()" runat="server">
                                        <rad:RadBinaryImage ID="courseImagePreview" runat="server" CssClass="box-fluid" ClientIDMode="Static" />
                                        <div class="box-fluid tc-overlay">
                                            <button onclick="removeImage(); return false;" title="Remove Image">
                                                <%--<asp:LinkButton ID="btnRemovePreviewImg" runat="server" ToolTip="Remove Image" OnClick="btnRemovePreviewImg_Click"
                                                OnClientClick="showImageDropzonePreviewLoading();">--%>
                                                <i class="fa fa-times-circle-o previewClose" style="color: white; position: absolute;
                                                    top: 50%; left: 50%; transform: translate(-50%,-50%); font-size: 5em; cursor: pointer;"
                                                    title="Remove Image"></i>
                                                <%-- </asp:LinkButton>--%>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="margin-top-10px col-md-12">
                                    <button class="btn btn-sm btn-teal pull-left col-md-5" onclick="openFileUploader(); return false;"
                                        style="margin-right: 20px; margin-left: 10px;">
                                        <asp:Label ID="Label47" runat="server" Text="<%$ Resources: LocalizedResource, ReplaceImage %>" /></button>
                                    <button class="btn btn-sm btn-teal pull-left col-md-5" onclick="removeImage(); return false;">
                                        <asp:Label ID="Label40" runat="server" Text="<%$ Resources: LocalizedResource, RemoveImage %>" /></button>
                                </div>
                            </div>
                            <%--Col 2 End--%>
                        </div>
                    </div>
                </form>
                <div id="RemoveImage" class="modal fade success-popup" tabindex="-1" role="dialog"
                    aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title" id="H1">
                                    <asp:Label ID="Label41" runat="server" Text="<%$ Resources: LocalizedResource, Remove %>" />
                                </h4>
                            </div>
                            <div class="modal-body text-center">
                                <p class="lead">
                                    <asp:Label ID="Label48" runat="server" Text="<%$ Resources: LocalizedResource, RemoveImage %>" />?
                                </p>
                            </div>
                            <div class="modal-footer">
                                <a class="btn btn-danger" data-toggle="modal" data-target="#RemoveImage"><asp:Label ID="Label42" runat="server" Text="<%$ Resources: LocalizedResource, Cancel %>" />
                                </a><i data-toggle="modal" data-target="#RemoveImage">
                                    <asp:LinkButton ID="btnRemoveImage" runat="server" OnClick="btnRemovePreviewImg_Click"
                                        Text="<%$ Resources: LocalizedResource, Ok %>" CssClass="btn btn-info"></asp:LinkButton>
                                </i>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="SaveDetails" class="modal fade success-popup" tabindex="-1" role="dialog"
                    aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title" id="H2">
                                    <asp:Label ID="Label45" runat="server" Text="<%$ Resources: LocalizedResource, SaveOnly %>" />
                                </h4>
                            </div>
                            <div class="modal-body text-center">
                                <p class="lead">
                                    <asp:Label ID="Label49" runat="server" Text="<%$ Resources: LocalizedResource, SavetheChanges %>" />
                                </p>
                            </div>
                            <div class="modal-footer">
                                <a class="btn btn-danger" data-toggle="modal" data-target="#SaveDetails"><asp:Label ID="Label43" runat="server" Text="<%$ Resources: LocalizedResource, Cancel %>" />
                                </a><i data-toggle="modal" data-target="#SaveDetails">
                                    <asp:LinkButton ID="lbtnSaveButton" runat="server" OnClick="btnSaveCourseDetails_Click"
                                        ValidationGroup="addCourse, dateValid" Text="<%$ Resources: LocalizedResource, Ok %>" CssClass="btn btn-info"></asp:LinkButton>
                                </i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <rad:RadWindowManager ID="cbram" RenderMode="Lightweight" EnableShadow="true" Skin="Bootstrap"
        Modal="true" VisibleOnPageLoad="false" Behaviors="Close, Move" DestroyOnClose="true"
        Opacity="99" runat="server" Width="800" Height="400px">
        <Windows>
            <rad:RadWindow ReloadOnShow="true" RenderMode="Lightweight" ID="radCourseWindow"
                VisibleOnPageLoad="false" Title="Select Course" runat="server" CssClass="top-9999"
                VisibleStatusbar="false">
                <ContentTemplate>
                    <asp:Panel ID="Panel2" runat="server" ClientIDMode="Static">
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="selectCourseContainer top-9999">
                                            <rad:RadListBox ID="lbAvailableCourses" runat="server" Height="275px" Width="100%"
                                                EmptyMessage="<%$ Resources: LocalizedResource, NoAvailableCourses %>" SelectionMode="Multiple" AllowTransfer="true"
                                                TransferToID="lbAssignedCourses" TransferMode="Move" CheckBoxes="true">
                                                <ButtonSettings ShowTransfer="false" ShowTransferAll="false" />
                                            </rad:RadListBox>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td style="padding-top: 10px;">
                                        <rad:RadButton ID="AddCourses" runat="server" Text="Save" CssClass="btn btn-teal btn-md pull-right"
                                            OnClick="AddCourses_Click" RenderMode="Lightweight" Visible="true">
                                        </rad:RadButton>
                                        <%-- OnClick="btnAddCourses_Click" --%>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </asp:Panel>
                </ContentTemplate>
            </rad:RadWindow>
        </Windows>
    </rad:RadWindowManager>

    <asp:HiddenField ID="hfInvalidFileTypeAllowedFilesareJPGJPEGPNG" runat="server" Value="<%$ Resources: LocalizedResource, InvalidFileTypeAllowedFilesareJPGJPEGPNG %>" />
    <asp:HiddenField ID="hfError" runat="server" Value="<%$ Resources: LocalizedResource, Error %>" />
    <asp:HiddenField ID="hfSomethingwentwrongPleasereloadthepage" runat="server" Value="<%$ Resources: LocalizedResource, SomethingwentwrongPleasereloadthepage %>" />
    <asp:HiddenField ID="hfextensionisinvalid" runat="server" Value="<%$ Resources: LocalizedResource, extensionisinvalid %>" />

</div>
<rad:RadScriptBlock ID="radScriptBlock1" runat="server">
    <script type="text/javascript">

//        function pageLoad() {
//            debugger;

//            var ctCreatedDate = $("#<%= LblCreateDate.ClientID %>");
//            var ctStartDate = $("#<%= LblStartDate.ClientID %>");
//            var ctEndDate = $("#<%= LblEndDate.ClientID %>");

//            var txtCreatedDate = convertTimeZone(ctCreatedDate.text());
//            var txtStartDate = convertTimeZone(ctStartDate.text());
//            var txtEndDate = convertTimeZone(ctEndDate.text());

//            ctCreatedDate.text(moment(txtCreatedDate).format("MM/DD/YYYY"));
//            ctStartDate.text(moment(txtStartDate).format("MM/DD/YYYY - hh:mm A"));
//            ctEndDate.text(moment(txtEndDate).format("MM/DD/YYYY - hh:mm A"));
//        }

        function closePopup() {
            var radwindow = $find('<%=radCourseWindow.ClientID %>');
            console.log("RAD WINDOW CLOSE");
            radwindow.close();
        }

        //CHECK LENGTH GIVEN BY THE PARAMETER || MDQUERUBIN 12072018
        function CheckMaxLength(sender, Maxlength) {
            var length = $(sender).val().length;
            if (sender.value.length > Maxlength) {

                sender.value = sender.value.substr(0, Maxlength);
            }

            var length = Maxlength - length;
            var Description = $("#<%=txtDescription.ClientID%>").attr("name");
            var CourseTitle = $("#<%=txtCourseTitle.ClientID%>").attr("name");

            if (sender.name == Description) {
                $("#<%=txtDescriptionChars.ClientID%>").text(length);
            }
            if (sender.name == CourseTitle) {
                $("#<%=txtCourseTitleChars.ClientID%>").text(length);
            }

        }

        function OnClientBlurHandler(sender, eventArgs) {
            var textInTheCombo = sender.get_text();
            var item = sender.findItemByText(textInTheCombo);
            $("#<%=hdnReqSubcategory.ClientID%>").val(item)
            //if there is no item with that text
            if (!item) {
                sender.set_text("");
                setTimeout(function () {
                    var inputElement = sender.get_inputDomElement();
                    inputElement.focus();
                    var empty = sender.get_emptyMessage();
                    sender.set_text(empty);
                    //inputElement.style.backgroundColor = "red";
                }, 20);
            }
        }

        function reqSubcategoryIndexChange(sender, args) {
            var panel = $("#panelSelectCourse");
            setTimeout(function () {
                panel.show();
            }, 200);
        }

        function rcbReqCourse_OnClientSelectedIndexChanged() {
            debugger;
            try {
                var rcbReqCourse = $find("<%= rcbReqCourse.ClientID %>");
                var ddtReqSubcategory = $find("<%= ddtReqSubcategory.ClientID %>");
                var value = parseInt(rcbReqCourse.get_value());
                console.log(value);
                var size = rcbReqCourse.get_items().get_count();
                debugger;

                if (value <= 0 && ddtReqSubcategory.get_entries().get_count() > 0) {
                    ddtReqSubcategory.get_entries().clear();
                }
            } catch (e) {
                console.log(e);
                alert($("#<%= hfSomethingwentwrongPleasereloadthepage.ClientID %>").val());
            }
        }

        function AddCourses_OnClientClick() {

            var assignedCourses = $find("<%= lbAssignedCourses.ClientID %>");
            var availableCourses = $find("<%= lbAvailableCourses.ClientID %>");
            var selectedItems = availableCourses.get_checkedItems();


            selectedItems.forEach(function (item) {
                availableCourses.transferItem(item, availableCourses, assignedCourses);

            });


            var panel = $find("<%= localLoading.ClientID %>");
            panel.show("Panel2");
            setTimeout(function () {

                $("#panelAssignedCourse").show();
                var courses = $find("<%= radCourseWindow.ClientID %>");
                $(".Loading2").hide();
                courses.close();
            }, 800);
            return false;
        }

        function openFileUploader() {
            $telerik.$("#<%= CourseImage.ClientID %> .ruFileInput").click();
        }


        function removeImage() {
            $("#RemoveImage").modal("toggle");
            console.log("remove");
        }

        function saveCourseDetails() {
            if (Page_ClientValidate("addCourse") && Page_ClientValidate("dateValid")) {
                //console.log("validated");
                $("#SaveDetails").modal("toggle");

            }

        }


        function fileUploaded(sender, args) {
            //                alert("s");
            var upload = $find("<%= CourseImage.ClientID %>");
            var inputs = upload.getUploadedFiles();

            if (!upload.isExtensionValid(inputs[0]))
                alert($("#<%= hfextensionisinvalid.ClientID %>").val());

            var radManager = $find('<%= RadAjaxManager.GetCurrent(Page).ClientID %>');
            radManager.ajaxRequest();

            setTimeout(function () {
                hideAllLocalLoading();
            }, 3000);

        }

        function fileSelected(sender, args) {

            showImageDropzonePreviewLoading();
            var fileExtention = args.get_fileName().substring(args.get_fileName().lastIndexOf('.') + 1, args.get_fileName().length);
            if (args.get_fileName().lastIndexOf('.') != -1) {//this checks if the extension is correct
                if (sender.get_allowedFileExtensions().toLowerCase().indexOf(fileExtention.toLowerCase()) == -1) {
                    $("#" + "<%= courseImagePreview.ClientID%>").attr('src', "").hide();
                    removePreviewImage();
                    radalert($("#<%= hfInvalidFileTypeAllowedFilesareJPGJPEGPNG.ClientID %>").val(), 330, 180, $("#<%= hfError.ClientID %>").val(), "");
                }
                else {
                    console.log("File Supported.");
                }
            }
            else {
                console.log("not correct extension.");

            }

        }

        function showImageDropzonePreviewLoading() {
            //debugger;
            var panel = $find("<%= fullPageLoading.ClientID %>");
            panel.show("courseImagePreview");
        }

        function hideAllLocalLoading() {

            $(".Loading2").hide();
            $(".Loading").hide();
            console.log("hide loading");
        }

        function dropzoneEnter() {

            if ($("#" + "<%= courseImagePreview.ClientID %>").is('[src]')) {
                $(".tc-overlay").css({ "display": "block", "opacity": "1" });
            }
        }

        function dropzoneExit() {
            var a = $("#" + "<%= courseImagePreview.ClientID %>").attr('src');
            $(".tc-overlay").css({ "display": "none", "opacity": "0" });

        }

        function removePreviewImage() {
            try {
                var upload = $find("<%= CourseImage.ClientID %>");
                upload.deleteAllFileInputs();
                $(".Loading").hide();
            } catch (e) {
            }
        }

        function validateStartEndDate(sender, args) {
            //debugger;
            var startDate = $find("<%= dpStartDateTime.ClientID %>").get_selectedDate();
            var endDate = $find("<%= dpEndDateTime.ClientID %>").get_selectedDate();
            endDate = endDate == null ? new Date(2099, 12, 30, 23, 0, 0) : endDate;
            var valid = (endDate > startDate);
            args.IsValid = valid;
            if (valid) {
                var dpEndDateTime = $find("<%= dpEndDateTime.ClientID %>");
                dpEndDateTime.get_dateInput().focus();
            }
        }

        function openCourseWindow() {
            debugger;
            var radwindow = $find('<%=radCourseWindow.ClientID %>');
            radwindow.show();
        }

        function ClosePrerequisiteForm() {
            var window = $find('<%=radCourseWindow.ClientID %>');
            window.close();
        }

        function validateCategoryAndSub(sender, args) {
            debugger;
            var c = $find("<%= rcbReqCourse.ClientID %>").get_value();
            var sc = $find("<%= ddtReqSubcategory.ClientID %>").get_selectedValue();

            if (c == 0 || c == "-1") {
                args.set_cancel(true);
                $find("<%= rcbReqCourse.ClientID %>").get_inputDomElement().focus();

                return;
            }
            else if(sc== "" || sc == 0){
                args.set_cancel(true);
                $find("<%= rcbReqCourse.ClientID %>").get_inputDomElement().focus();

                return;
            }
            else {
                openCourseWindow();
                //                sender.click();
                //                if (Telerik.Web.Browser.ff) //work around a FireFox issue with form submission, see http://www.telerik.com/support/kb/aspnet-ajax/window/details/form-will-not-submit-after-radconfirm-in-firefox
                //                    sender.get_element().click();
                args.set_cancel(false);
            }
            //            else if (c == "" || sc == "") {
            //                $find("<%= rcbReqCourse.ClientID %>").get_inputDomElement().focus();
            //                $find("<%= ddtReqSubcategory.ClientID %>").get_inputDomElement().focus();
            //            } 

        }
    </script>
    <%--additional for time picker--%>
    <script type="text/javascript">
        // using a variable to prevent infinite loop
        var isTimeSet = false;
        function timeSelecting(sender, args) {
            isTimeSet = true;
        }

        function startDateSelected(sender, args) {
            if (args.get_newDate() && !isTimeSet) {
                args.set_cancel(true);
                isTimeSet = true;
                sender.get_timeView().setTime(00, 0, 0, 0);
            }
            if (isTimeSet) {
                isTimeSet = false;
            }
        }

        function endDateSelected(sender, args) {
            if (args.get_newDate() && !isTimeSet) {
                args.set_cancel(true);
                isTimeSet = true;
                sender.get_timeView().setTime(23, 0, 0, 0);
            }
            if (isTimeSet) {
                isTimeSet = false;
            }
        }

        function valueChanging(sender, args) {
            if (args.get_newValue().indexOf(":") > 0) {
                isTimeSet = true;
            }
        }
    </script>
</rad:RadScriptBlock>
