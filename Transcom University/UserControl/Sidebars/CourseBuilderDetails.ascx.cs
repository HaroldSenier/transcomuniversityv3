﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using Telerik.Web.UI;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Text;
using Telerik.Charting;
using System.Security.Cryptography;
using TranscomUniversityV3Model;

public partial class CourseBuilderDetails : System.Web.UI.UserControl
{
    private static string global_courseImage;
    private static int COURSEID;
    private static SessionCourseDetails cd;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //var courseId = Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"]));
            COURSEID = Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"]));
            if (!IsPostBack)
            {
                ViewState["courseDetails"] = null;
                cd = new SessionCourseDetails();
                LoadDetails(COURSEID);
            }
        }
        catch
        {
            throw;
        }
    }

    protected void LoadDetails(int courseID)
    {
        if (ViewState["courseDetails"] == null)
        {

            DataSet ds = DataHelper.GetCourseDetails(courseID);
            cd = new SessionCourseDetails();

            cd.courseID = courseID;
            cd.courseType = Convert.ToInt32(ds.Tables[0].Rows[0]["CourseTypeID"]);
            cd.title = ds.Tables[0].Rows[0]["Title"].ToString();
            cd.categoryName = ds.Tables[0].Rows[0]["CategoryName"].ToString();
            cd.subcategoryName = ds.Tables[0].Rows[0]["Subcategory"].ToString();
            cd.categoryID = Convert.ToInt32(ds.Tables[0].Rows[0]["CategoryID"]);
            cd.subcategoryID = Convert.ToInt32(ds.Tables[0].Rows[0]["SubcategoryID"]);
            cd.description = ds.Tables[0].Rows[0]["Description"].ToString();
            cd.publishedDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["DateTimePublished"].ToString());
            cd.startDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["StartDate"].ToString());
            cd.endDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["EndDate"].ToString());
            cd.duration = Convert.ToInt32(ds.Tables[0].Rows[0]["Duration"]);
            cd.author = ds.Tables[0].Rows[0]["AuthorName"].ToString();
            cd.department = ds.Tables[0].Rows[0]["DeptOwnership"].ToString();
            cd.reqProgramID = Convert.ToInt32(ds.Tables[0].Rows[0]["RequiredProgram"]);
            cd.reqProgram = ds.Tables[0].Rows[0]["RequiredProgramName"].ToString();
            //cd.reqCourseID = 
            //cd.reqCourses = ds.Tables[0].Rows[0]["RequiredCourses"].ToString();
            cd.reqTenure = Convert.ToInt32(ds.Tables[0].Rows[0]["YearsInTenure"]);
            cd.reqRole = Convert.ToInt32(ds.Tables[0].Rows[0]["YearsInRole"]);
            cd.accessType = ds.Tables[0].Rows[0]["AccessMode"].ToString();
            cd.jobLevel = ds.Tables[0].Rows[0]["JobLevels"].ToString();
            cd.courseImage = ds.Tables[0].Rows[0]["CourseImage"].ToString();

            ViewState["courseDetails"] = cd;
        }
        else
            cd = (SessionCourseDetails)ViewState["courseDetails"];


        //View Mode
        LblCourseTitle.Text = fixCourseTitle(cd.title);
        LblCategory.Text = cd.categoryName;
        LblSubCat.Text = cd.subcategoryName;
        LblCourseID.Text = cd.courseID.ToString();
        LblDesc.Text = cd.description;
        LblCreateDate.Text = string.Format("{0:MM/dd/yyyy}", cd.publishedDate);
        //LblStartDate.Text = string.Format("{0:MM/dd/yyyy - hh:mm tt}", cd.startDate);
        //LblEndDate.Text = string.Format("{0:MM/dd/yyyy - hh:mm tt}", cd.endDate);

        LblStartDate.Text = cd.startDate.ToString();
        LblEndDate.Text = cd.endDate.ToString();

        LblDuration.Text = cd.duration.ToString();
        LblAuthor.Text = cd.author == "" ? "N/A" : cd.author;
        LblDepartmentOwner.Text = cd.department == "0" ? "N/A" : cd.department;
        //LblRequiredCourse.Text = cd.reqCourses;
        LblRequiredTenureYrs.Text = cd.reqTenure.ToString();
        LblRequiredRoleYrs.Text = cd.reqRole.ToString();
        LblAccessType.Text = cd.accessType;
        lblRequiredProgram1.Text = cd.reqProgramID == 0 ? "N/A" : cd.reqProgram;
        lblJobLevel.Text = cd.jobLevel == "1,2,3,4,5" ? Resources.LocalizedResource.OpenforAll.ToString() : cd.jobLevel;

        if (cd.categoryID != 17)
        {
            rowjoblevel.Visible = false;
            lblJobLevel.Visible = false;

        }

        //var db = new TranscomUniversityV3ModelContainer();
        //var courseImage = (from c in db.tbl_TranscomUniversity_Cor_Course
        //                   where c.CourseID == courseId
        //                   select c.CourseImage).FirstOrDefault().ToString();
        global_courseImage = cd.courseImage;
        ImgCourse.ImageUrl = string.Format("~/Media/Uploads/CourseImg/{0}/{1}", cd.courseID, cd.courseImage);
        //global_courseImage = ds.Tables[0].Rows[0]["CourseImage"].ToString();

        //LoadSubcategories(Convert.ToInt32(ds.Tables[0].Rows[0]["CategoryID"].ToString()));

        rptRequiredCourse.DataSource = DataHelper.GetRequiredCourses(cd.courseID);
        rptRequiredCourse.DataBind();
        //Image4.ImageUrl = string.Format("~/Media/Uploads/CourseImg/{0}/{1}", courseID, ds.Tables[0].Rows[0]["CourseImage"].ToString());

    }

    private string fixCourseTitle(string courseTitle)
    {
        var ssCourseTitle = string.Empty;
        string[] words = courseTitle.Split(' ');
        if (words.Count() == 1 && courseTitle.Length > 20)
        {
            double leng = Convert.ToDouble(courseTitle.Length) / 20;
            var value = Math.Ceiling(leng);
            var a = 0;
            var b = 20;
            for (var i = 1; i <= value; i++)
            {
                if (courseTitle.Length < b * i)
                    b = (courseTitle.Length - 1) - (b * (i - 1));
                ssCourseTitle += courseTitle.Substring(a, b) + " ";
                a = (20 * i) + 1;
            }
        }

        else
        {
            ssCourseTitle = courseTitle;
        }
        return ssCourseTitle;
    }

    protected void loadCourseEditDetails()
    {
        //Edit Mode
        try
        {
            loadCategory();
            bindDepartments();
            bindPrograms();

            if (cd == null)
                cd = new SessionCourseDetails();

            cd = (SessionCourseDetails)ViewState["courseDetails"];

            LoadSubcategories(cd.categoryID);
            txtCourseTitle.Text = cd.title;
            //rcbCategory.Text = ds.Tables[0].Rows[0]["CategoryName"].ToString();
            rcbCategory.SelectedIndex = rcbCategory.Items.FindItemByValue(cd.categoryID.ToString()).Index;

            //ddtSubcategory.SelectedValue = ds.Tables[0].Rows[0]["SubcategoryID"].ToString();
            ddtSubcategory.SelectedText = cd.subcategoryName;
            txtDescription.Text = cd.description;

            var Descriptionchars = 1500 - txtDescription.Text.Length;
            txtDescriptionChars.InnerText = Descriptionchars.ToString();

            var chars = 250 - txtCourseTitle.Text.Length;
            txtCourseTitleChars.InnerText = chars.ToString();

            txtDateCreated.Text = string.Format("{0:MM/dd/yyyy}", cd.publishedDate);
            dpStartDateTime.SelectedDate = Convert.ToDateTime(cd.startDate);
            dpEndDateTime.SelectedDate = Convert.ToDateTime(cd.endDate);
            rntbDuration.Text = cd.duration.ToString();
            txtAuthor.Text = cd.author;

            rcbDepartment.SelectedIndex = rcbDepartment.Items.FindItemByValue(cd.department) == null ? 0 : rcbDepartment.Items.FindItemByValue(cd.department).Index;
            rcbReqProgram.SelectedValue = cd.reqProgramID.ToString();
            //rcbReqCourse.SelectedIndex = rcbReqCourse.Items.FindItemIndexByText(cd.reqCourseID.ToString()) == 0 ? 0 : rcbReqCourse.Items.FindItemIndexByText(cd.reqCourseID.ToString());
            ddlTenureYears.Text = cd.reqTenure.ToString();
            ddlRoleYears.Text = cd.reqRole.ToString();
            ddlAccessType.Text = cd.accessType;
            hfCourseType.Value = cd.courseType.ToString();
            courseImagePreview.ImageUrl = string.Format("~/Media/Uploads/CourseImg/{0}/{1}", cd.courseID, cd.courseImage);


            IList<RadListBoxItem> assignedList = lbAssignedCourses.Items;
            IList<RadListBoxItem> availableList = lbAvailableCourses.Items;
            List<vw_Transcomuniversity_RequiredCoures> reqCourseList = new List<vw_Transcomuniversity_RequiredCoures>(DataHelper.GetRequiredCourses(cd.courseID));

            foreach (vw_Transcomuniversity_RequiredCoures c in reqCourseList)
            {

                RadListBoxItem item = new RadListBoxItem();
                item.Value = c.RequiredCourseID.ToString();
                item.Text = c.title;
                lbAssignedCourses.Items.Add(item);
            }

            if (cd.categoryID == 17)
            {
                string[] joblevel = cd.jobLevel.Split(',');

                foreach (RadComboBoxItem item in rcbLevel.Items)
                {
                    if (joblevel.Contains(item.Value))
                    {
                        item.Checked = true;
                    }
                }
                pnlLevelContainer.Visible = true;

            }
            else
                pnlLevelContainer.Visible = false;

        }
        catch (Exception)
        {

            throw;
        }


    }

    protected void btnCloseCourseDetails_Click(object sender, EventArgs e)
    {
        clearCourseInput();
        LoadDetails(COURSEID);
        pnlCourseDetails.Visible = true;
        pnlEditCourseDetails.Visible = false;
    }

    protected void btnEditCourseDetails_Click(object sender, EventArgs e)
    {
        pnlCourseDetails.Visible = false;
        pnlEditCourseDetails.Visible = true;
        loadCourseEditDetails();
    }

    protected void btnRemoveImage_OnClick(object sender, EventArgs e)
    {

        ImgCourse.ImageUrl = string.Format("..Media/Images/noimage.png");
        global_courseImage = null;


        //deleteImageInServer(COURSEID); MADQUERUBIN 12112018

    }

    void loadCategory()
    {
        rcbCategory.DataSource = null;
        rcbCategory.DataBind();
        var ds = DataHelper.GetAllCategories();
        rcbCategory.DataSource = ds;
        rcbCategory.DataBind();

        //populate Tenure Years
        for (int i = 0; i <= 20; i++)
        {
            ListItem item = new ListItem();
            item.Value = i.ToString();
            item.Text = i.ToString();
            ddlTenureYears.Items.Add(item);
            ddlRoleYears.Items.Add(item);
        }

        //populate rcbReqCourse
        rcbReqCourse.DataSource = null;
        rcbReqCourse.DataBind();

        rcbReqCourse.DataTextField = "Category";
        rcbReqCourse.DataValueField = "CategoryID";
        //rcbReqCourse.AppendDataBoundItems = true; MADQUERUBIN 12112018
        rcbReqCourse.DataSource = DataHelper.GetAllCategories();
        rcbReqCourse.Items.Insert(0, new RadComboBoxItem(Resources.LocalizedResource.SelectCategory.ToString(), "-1"));
        rcbReqCourse.DataBind();



    }

    protected void btnSaveCourseDetails_Click(object sender, EventArgs e)
    {
        int len2 = ddtSubcategory.SelectedValue.Length;
        if (len2 > 1)
        {
            ddtSubcategory.SelectedValue = ddtSubcategory.SelectedValue.Substring(len2 - 1, len2 - 2);
        }
        else if (len2 == 1)
        {
            ddtSubcategory.SelectedValue = ddtSubcategory.SelectedValue;
        }
        if (dpStartDateTime.SelectedDate != null)
        {
            if (Page.IsValid)
            {
                DateTime defaultMax = new DateTime(2099, 12, 1, 23, 00, 00);
                int dateValid = DateTime.Compare(Convert.ToDateTime(dpStartDateTime.SelectedDate), dpEndDateTime.SelectedDate == null ? defaultMax : Convert.ToDateTime(dpEndDateTime.SelectedDate));

                if (dateValid < 0)
                {
                    var courseImage = global_courseImage == null ? "No_image.jpg" : global_courseImage;
                    if (courseImage != "error" || courseImage != "No_image.jpg")
                    {
                        int courseTypeID = Convert.ToInt32(hfCourseType.Value.ToString());
                        string courseTitle = txtCourseTitle.Text.Trim();
                        int categoryID = Convert.ToInt32(rcbCategory.SelectedValue == "" ? "0" : rcbCategory.SelectedValue);
                        int subCategoryID = Convert.ToInt32(ddtSubcategory.SelectedValue == "" ? "0" : ddtSubcategory.SelectedValue);
                        string description = txtDescription.Text.Trim();
                        DateTime dateCreated = Convert.ToDateTime(txtDateCreated.Text);
                        DateTime startDate = Convert.ToDateTime(dpStartDateTime.SelectedDate);
                        DateTime endDate = dpEndDateTime.SelectedDate.ToString() == "" ? defaultMax : Convert.ToDateTime(dpEndDateTime.SelectedDate);
                        int duration = Convert.ToInt32(rntbDuration.Text == "" ? "0" : rntbDuration.Text);
                        string AuthorCim = txtAuthor.Text.Trim();
                        string department = rcbDepartment.SelectedValue == "" ? "0" : rcbDepartment.SelectedValue;
                        int reqProgramID = Convert.ToInt32(rcbReqProgram.SelectedValue == "" ? "0" : rcbReqProgram.SelectedValue);
                        string reqProgram = rcbReqProgram.Text;

                        //MADQUERUBIN 12112018
                        int reqCourseID = Convert.ToInt32(rcbReqCourse.SelectedValue == "" ? "0" : rcbReqCourse.SelectedValue);
                        string reqSubcategory = hdnReqSubcategory.Value;
                        //MADQUERUBIN 12112018

                        string level = "";

                        var levelList = rcbLevel.CheckedItems;
                        level = string.Join(",", levelList.Select(l => l.Value));

                        if (rcbReqCourse.SelectedValue == "-1" || rcbReqCourse.SelectedValue == "")
                        {
                            reqCourseID = 0;
                        }
                        else
                        {
                            reqCourseID = Convert.ToInt32(rcbReqCourse.SelectedValue);
                        }

                        int yearsInTenure = Convert.ToInt32(ddlTenureYears.SelectedValue);
                        int yearsInRole = Convert.ToInt32(ddlRoleYears.SelectedValue);

                        int publishedBy = Convert.ToInt32(DataHelper.GetCurrentUserCIM());
                        string accessMode = ddlAccessType.SelectedValue;

                        //DataHelper.InsertCourse(courseTitle, categoryID, subCategoryID, description, dateCreated, startDate, endDate, publishedBy, duration, AuthorCim, department, reqProgramID, yearsInTenure, yearsInRole, accessMode, courseImage, courseTypeID, level);
                        DataHelper.UpdateCourse(COURSEID, courseTitle, categoryID, subCategoryID, description, dateCreated, startDate, endDate, publishedBy, duration, AuthorCim, department, reqProgramID, yearsInTenure, yearsInRole, accessMode, courseImage, courseTypeID, level);
                        IList<RadListBoxItem> collection = lbAssignedCourses.Items;
                        DataHelper.HideAllRequiredCourse(COURSEID);

                        if (collection.Count > 0)
                        {

                            foreach (RadListBoxItem item in collection)
                            {
                                DataHelper.UpdateRequiredCourse(COURSEID, Convert.ToInt32(item.Value));
                            }

                        }
                        //string script = "function f(){openSuccessModal(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
                        //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, true);
                        // RadScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", Utils.callClientScript("redirectToCourseBuilder", Utils.Encrypt(COURSEID)), true);
                        //Response.Redirect("CourseBuilder.aspx?CourseID=" + );

                        //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", Utils.callClientScript("updateCourseImage",  "Media/Uploads/CourseImg/" + COURSEID + "/" +global_courseImage), true);


                        //cd = new CourseDetails();

                        //save to viewstate new details
                        cd.courseID = COURSEID;
                        cd.courseType = courseTypeID;
                        cd.title = courseTitle;
                        cd.categoryName = rcbCategory.Text;
                        cd.subcategoryName = ddtSubcategory.SelectedText;
                        cd.categoryID = categoryID;
                        cd.subcategoryID = subCategoryID;
                        cd.description = description;
                        //cd.publishedDate = cd
                        cd.startDate = startDate;
                        cd.endDate = endDate;
                        cd.duration = duration;
                        cd.author = AuthorCim;
                        cd.department = department;
                        cd.reqProgramID = reqProgramID;
                        cd.reqProgram = reqProgram;

                        //MADQUERUBIN 12112018
                        cd.reqCourses = reqCourseID.ToString();
                        cd.reqSubcategory = reqSubcategory;
                        //MADQUERUBIN 12112018

                        cd.reqTenure = yearsInTenure;
                        cd.reqRole = yearsInRole;
                        cd.accessType = accessMode;
                        cd.jobLevel = level;
                        cd.courseImage = courseImage;

                        ViewState["courseDetails"] = cd;

                        /////////////////////////
                        LoadDetails(COURSEID);
                        pnlCourseDetails.Visible = true;
                        pnlEditCourseDetails.Visible = false;
                        string ImageUrl = string.Format("Media/Uploads/CourseImg/{0}/{1}", COURSEID, HttpUtility.HtmlEncode(courseImage));
                        ImgCourse.ImageUrl = string.Format("~/Media/Uploads/CourseImg/{0}/{1}", cd.courseID, cd.courseImage);


                        HtmlGenericControl imagePreviewContainer = (HtmlGenericControl)this.Parent.FindControl("imagePreviewContainer");
                        Label CourseTitle = (Label)this.Parent.FindControl("CourseTitle");
                        //LblCourseTitle.Text = courseTitle;
                        CourseTitle.Text = courseTitle;
                        if (courseImage != "No_image.jpg")
                            imagePreviewContainer.Attributes["style"] = "background-image: url('" + ImageUrl + "'); margin:40px; height: 250px; width: 350px;";
                        else
                            imagePreviewContainer.Attributes["style"] = "background-image: url('" + "Media/Uploads/CourseImg/No_image.jpg" + "');  margin:40px; height: 250px; width: 350px;";

                        lbAssignedCourses.Items.Clear();

                        cbram.RadAlert(Resources.LocalizedResource.Coursedetailssuccessfullyedited.ToString(), 330, 180, Resources.LocalizedResource.Success.ToString(), "");


                    }
                    else
                    {
                        cbram.RadAlert(Resources.LocalizedResource.CourseImageisRequired.ToString(), 330, 180, Resources.LocalizedResource.Error.ToString(), "");
                    }
                }
                else
                {
                    cbram.RadAlert(Resources.LocalizedResource.TheEnddateshouldbegreaterthanStartDate.ToString(), 330, 180, Resources.LocalizedResource.Error.ToString(), "");
                    //string script = "function f(){openErrorDateModal(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
                    //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, true);
                }
            }
        }
        else
        {

            cbram.RadAlert(Resources.LocalizedResource.StartDateisnotdefined.ToString(), 330, 180, Resources.LocalizedResource.Error.ToString(), "");
            //string script = "function f(){noStartDateError(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, true);
        }
    }


    void clearCourseInput()
    {
        txtCourseTitle.Text = "";
        rcbCategory.ClearSelection();
        ddtSubcategory.Entries.Clear();
        txtDescription.Text = "";
        dpStartDateTime.SelectedDate = null;
        dpEndDateTime.SelectedDate = null;
        //ddlDuration.SelectedIndex = 0;
        rntbDuration.Text = string.Empty;
        //rcbAuthor.ClearSelection();
        txtAuthor.Text = "";
        rcbDepartment.ClearSelection();
        rcbReqProgram.ClearSelection();
        rcbReqProgram.Text = "";
        rcbReqCourse.ClearSelection();
        rcbReqCourse.SelectedIndex = 0;
        ddtReqSubcategory.Entries.Clear();
        lbAvailableCourses.Items.Clear();
        lbAssignedCourses.Items.Clear();
        ddlRoleYears.SelectedIndex = 0;
        ddlTenureYears.SelectedIndex = 0;
        ddlAccessType.SelectedIndex = 0;
        CourseImage.UploadedFiles.Clear();
        rcbLevel.ClearCheckedItems();
        rcbLevel.ClearSelection();
    }

    protected string imageCourse(int courseID, UploadedFile f)
    {

        string error = null;

        if (string.IsNullOrWhiteSpace(error))
        {
            string uploadFolder;
            var userCim = DataHelper.GetCurrentUserCIM();

            if (HttpContext.Current.Request.Url.Host == "localhost")
                uploadFolder = Request.PhysicalApplicationPath + "Media\\Uploads\\CourseImg\\" + courseID + "\\";
            else
                uploadFolder = HttpRuntime.AppDomainAppPath + "Media\\Uploads\\CourseImg\\" + courseID + "\\";

            string directoryPath = Server.MapPath(string.Format("~/{0}/", "Media/Uploads/CourseImg/" + courseID + "\\"));

            if (!Directory.Exists(directoryPath))
                Directory.CreateDirectory(directoryPath);

            if (f.ContentType == "image/jpeg" || f.ContentType == "image/png" || f.ContentType == "image/jpg")
            {
                if (f.ContentLength < 1024000000)
                {
                    Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                    string extension = Path.GetExtension(f.FileName);
                    f.SaveAs(uploadFolder + f.FileName, true);
                    //f.SaveAs(uploadFolder + unixTimestamp.ToString() + extension, true);
                    //global_courseImage = unixTimestamp.ToString() + extension;
                    global_courseImage = f.FileName;
                }
                else
                {
                    global_courseImage = "error";
                    cbram.RadAlert(Resources.LocalizedResource.Filesizeistoolargepleaseselectanotherone.ToString(), 330, 180, Resources.LocalizedResource.ImagetooLarge.ToString(), "");
                }
            }
            else
            {
                global_courseImage = "error";
                cbram.RadAlert(Resources.LocalizedResource.Errorfileformatpleaseselectanotherone.ToString(), 330, 180, Resources.LocalizedResource.InvalidFileFormat.ToString(), "");
            }
        }

        return global_courseImage;
    }

    protected void rcbCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddtSubcategory.Enabled = true;
        ddtSubcategory.Entries.Clear();
        int selectCategoryValue = Convert.ToInt32(rcbCategory.SelectedValue);
        LoadSubcategories(selectCategoryValue);

        if (selectCategoryValue == 17)
            pnlLevelContainer.Visible = true;
        else
        {
            pnlLevelContainer.Visible = false;
            rcbLevel.ClearCheckedItems();
        }

    }

    protected void LoadSubcategories(int categoryID)
    {
        ddtSubcategory.Enabled = true;
        ddtSubcategory.DataTextField = "Subcategory";
        ddtSubcategory.DataValueField = "SubcategoryID";
        ddtSubcategory.DataSource = DataHelper.GetSubcategory(categoryID);
        ddtSubcategory.DefaultMessage = Resources.LocalizedResource.SelectSubcategory.ToString();
        try
        {

            ddtSubcategory.DataBind();
        }
        catch (ArgumentOutOfRangeException aore)
        {
            cbram.RadAlert(Resources.LocalizedResource.SomethingwentwrongPleasereloadthepageandtryagain.ToString(), 330, 180, Resources.LocalizedResource.Error.ToString(), "");


        }
    }

    private static bool IsImage(HttpPostedFile file)
    {
        return ((file != null) && System.Text.RegularExpressions.Regex.IsMatch(file.ContentType, "image/\\S+") && (file.ContentLength > 0));
    }

    protected void LoadReqSubcategories(int categoryID)
    {
        ddtReqSubcategory.DataSource = DataHelper.GetSubcategory(categoryID);
        ddtReqSubcategory.DefaultMessage = Resources.LocalizedResource.SelectSubcategory.ToString();
        ddtReqSubcategory.DefaultValue = "-1";
        try
        {
            ddtReqSubcategory.DataBind();
        }
        catch (ArgumentOutOfRangeException aore)
        {

        }

    }

    protected void rcbReqCourse_IndexChanged(object sender, EventArgs e)
    {

        if (rcbReqCourse.SelectedValue == "-1")
        {
            //string script = "function f(){hideRequiredCoursePanel(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, true);
            lbAvailableCourses.Items.Clear();
            //lbAssignedCourses.Items.Clear();
            ddtReqSubcategory.Entries.Clear();
        }
        else
        {
            //string script = "function f(){showPanel('panelSubReqCategory'); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, true);
            ddtReqSubcategory.Entries.Clear();
            int selectCategoryValue = Convert.ToInt32(rcbReqCourse.SelectedValue);
            LoadReqSubcategories(selectCategoryValue);
        }

    }

    protected void btnSelectCourse_Click(object sender, EventArgs e)
    {
        //load data on rad window
        //show rad window
        int categoryid = 0;
        int subcategoryid = 0;
        try
        {
            categoryid = Convert.ToInt32(rcbReqCourse.SelectedValue);
            string[] d = ddtReqSubcategory.SelectedValue.Split(',');

            if (ddtReqSubcategory.SelectedValue.Contains(','))
                subcategoryid = Convert.ToInt32(d[d.Count() - 1]);
            else// if (len == 1)
                subcategoryid = Convert.ToInt32(ddtReqSubcategory.SelectedValue);


        }
        catch (Exception error)
        {
            categoryid = 0;
            subcategoryid = 0;
        }
        finally
        {

            lbAvailableCourses.DataValueField = "CourseID";
            lbAvailableCourses.DataTextField = "Title";
            lbAvailableCourses.DataSource = DataHelper.GetFilterCourses(categoryid, subcategoryid);
            lbAvailableCourses.DataBind();

        }
        IList<RadListBoxItem> assignedList = lbAssignedCourses.Items;
        IList<RadListBoxItem> availableList = lbAvailableCourses.Items;
        foreach (RadListBoxItem a in assignedList)
        {
            foreach (RadListBoxItem b in availableList)
            {
                if (a.Text == b.Text)
                {
                    b.Checked = true;
                }
            }
        }
        //string script = "function f(){openCourseWindow(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";

        ////RadScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", Utils.callClientScript("openCourseWindow"), true);
        ////ScriptManager.RegisterStartupScript(this, this.GetType(), "key", Utils.callClientScript("openCourseWindow"), true);

    }

    //protected void btnAddCourses_Click(object sender, EventArgs e)
    //{
    //    IList<RadListBoxItem> collection = lbAvailableCourses.CheckedItems;

    //    if (collection.Count > 0)
    //    {
    //        foreach (RadListBoxItem item in collection)
    //        {
    //            lbAvailableCourses.Transfer(item, lbAvailableCourses, lbAssignedCourses);
    //        }
    //        string script = "function f(){showPanel('panelAssignedCourse'); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
    //        RadScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, true);
    //    }
    //    else
    //    {
    //        string script = "function f(){hidePanel('panelAssignedCourse'); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
    //        RadScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, true);
    //    }
    //    ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox", "ClosePrerequisiteForm()", true);
    //}

    //MADQUERUBIN 12112018
    protected void AddCourses_Click(object sender, EventArgs e)
    {
        //lbAssignedCourses.Items.Clear();

        IList<RadListBoxItem> collection = lbAvailableCourses.CheckedItems;
        IList<RadListBoxItem> assigncollection = lbAssignedCourses.Items;


        var searchFor = new List<string>();

        foreach (RadListBoxItem assignitem in assigncollection)
        {
            searchFor.Add(assignitem.Text);
        }


        if (collection.Count > 0)
        {
            foreach (RadListBoxItem item in collection)
            {
                if (assigncollection.Count <= 0)
                {
                    lbAvailableCourses.Transfer(item, lbAvailableCourses, lbAssignedCourses);
                }
                else
                {
                    bool containsAnySearchString = searchFor.Any(word => item.Text.Contains(word));

                    if (containsAnySearchString == false)
                    {
                        lbAvailableCourses.Transfer(item, lbAvailableCourses, lbAssignedCourses);
                    }
                }
                    
            }

        }

        ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "closePopup1", DataHelper.callClientScript("closePopup"), true);
       
    }
    //MADQUERUBIN 12112018
    protected void DateValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = (args.Value == "");
    }

    protected void upload_FileUploaded(object sender, FileUploadedEventArgs e)
    {

        if (e.IsValid)
        {
            imageCourse(COURSEID, e.File);
            courseImagePreview.ImageUrl = "~/Media/Uploads/CourseImg/" + COURSEID + "/" + global_courseImage;
        }
        else
        {
            CourseImage.UploadedFiles.Clear();
            cbram.RadAlert(Resources.LocalizedResource.InvalidFileTypeAllowedFilesareJPGJPEGPNG.ToString(), null, null, Resources.LocalizedResource.Error.ToString(), "");

        }

    }

    protected void btnRemovePreviewImg_Click(object sender, EventArgs e)
    {

        //courseImagePreview.DataValue = null;
        global_courseImage = "No_image.jpg";
        courseImagePreview.ImageUrl = "";
        //deleteImageInServer(COURSEID);
        RadScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", Utils.callClientScript("hideAllLocalLoading"), true);

    }

    private void deleteImageInServer(int courseID)
    {
        try
        {
            var folderPath = Server.MapPath("Media/Uploads/CourseImg/" + courseID + "\\");
            System.IO.DirectoryInfo folderInfo = new DirectoryInfo(folderPath);

            foreach (FileInfo file in folderInfo.GetFiles())
            {
                if (file.Name != global_courseImage)
                    file.Delete();
            }
        }
        catch { }

    }

    protected void bindDepartments()
    {


        rcbDepartment.DataSource = DataHelper.GetDepartments();
        rcbDepartment.DataBind();
    }

    protected void bindPrograms()
    {

        rcbReqProgram.DataSource = DataHelper.GetPrograms();
        rcbReqProgram.DataBind();
    }


    [Serializable]
    public class SessionCourseDetails
    {
        public int courseID { get; set; }
        public int courseType { get; set; }
        public string title { get; set; }
        public string categoryName { get; set; }
        public string subcategoryName { get; set; }
        public int categoryID { get; set; }
        public int subcategoryID { get; set; }
        public string description { get; set; }
        public DateTime publishedDate { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public int duration { get; set; }
        public string author { get; set; }
        public string department { get; set; }
        public string reqProgram { get; set; }
        public int reqProgramID { get; set; }
        public int reqCourseID { get; set; }
        public int reqTenure { get; set; }
        public int reqRole { get; set; }
        public string reqCourses { get; set; }
        public string accessType { get; set; }
        public string courseImage { get; set; }
        public string jobLevel { get; set; }
        public string reqSubcategory { get; set; }
    }

}

