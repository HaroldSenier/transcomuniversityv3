﻿﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CourseBuilderDripContent.ascx.cs"
    Inherits="DripContent" %>
<rad:RadScriptBlock ID="ctRadScriptBlock1" runat="server">
    <script type="text/javascript">

        (function (global, undefined) {
            var demo = {};

            function setCustomPosition(sender, args) {
                sender.moveTo(sender.getWindowBounds().x, 280);
            }


            function showDialogInitially() {
                var wnd = getModalWindow();
                wnd.show();
                Sys.Application.remove_load(showDialogInitially);
            }

            //Sys.Application.add_load(showDialogInitially);

            function getModalWindow() { return $find(demo.modalWindowID); }

            function checkboxCheck(x) {
                var isChecked = $(x).is(":checked");

                if (isChecked) {
                    //$('#x').click();
                    var wnd = getModalWindow();
                    wnd.show();
                    var ddContent = $find("<%= ddContent.ClientID %>");
                    ddContent.clearSelection();
                    ddContent.set_emptyMessage("- Select a Content -");

                    $("#<%= StartDate.ClientID %>").val("");
                    $("#<%= EndDate.ClientID %>").val("");
                } else {
                    //alert("no I am not checked");
                }
            }

            global.$modalWindowDemo = demo;
            global.setCustomPosition = setCustomPosition;
            global.checkboxCheck = checkboxCheck;

        })(window);

        $modalWindowDemo.modalWindowID = "<%=modalPopup.ClientID %>";

        function closePopup() {
            var modal = $find("<%=modalPopup.ClientID %>")
            modal.close();
        }

        $(document).ready(function () {

            var elems2 = Array.prototype.slice.call(document.querySelectorAll('.js-switch2'));
            elems2.forEach(function (html) {
                new Switchery(html, {
                    size: 'small',
                    color: '#FFFFFF',
                    secondaryColor: '#FFFFFF', //off
                    jackColor: '#09A8A4', //on
                    jackSecondaryColor: '#BBBBBB' //off
                });
            });

        });

        function validateStartEndDate(sender, args) {
            var startDate = $("#<%= StartDate.ClientID %>").val();
            var endDate = $("#<%= EndDate.ClientID %>").val();
            var valid = (endDate > startDate);
            args.IsValid = valid;
            if (valid) {
                endDate.focus();
            }
        }

        function confirmInput(sender, args) {
            var startDate = $("#<%= StartDate.ClientID %>").val();
            var endDate = $("#<%= EndDate.ClientID %>").val();
            var ddContent = $find("<%= ddContent.ClientID %>");

            var isValid = false;
            if (ddContent.get_text() == "- Select a Content -") {
                radalert("Please Select a Content", 330, 180, "Error", "");
            }
            else if(startDate == "")
                radalert("Please input valid start date", 330, 180, "Invalid Date", "");
            else if (endDate == "")
                radalert("Please input valid end date", 330, 180, "Invalid Date", "");
            else if(endDate > startDate){
                isValid = true;
            }
            else {
                radalert("Invalid input date. End Date should be greater than Start Date", 330, 180, "Invalid Date", "");
            }

            if (isValid)
                sender.click();
            else
                args.set_cancel(true);

        }

    </script>
</rad:RadScriptBlock>
<rad:RadAjaxManagerProxy runat="server" ID="RadAjaxManager1">
    <AjaxSettings>
        <rad:AjaxSetting AjaxControlID="rbSubmitChanges">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="modalPopup" />
                <rad:AjaxUpdatedControl ControlID="loadingPanelWrapper" LoadingPanelID="ralpConfiguration" />
                <rad:AjaxUpdatedControl ControlID="BtnProceed" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="BtnProceed">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="BtnProceed" />
                <rad:AjaxUpdatedControl ControlID="RadGridContentDrip" />
            </UpdatedControls>
        </rad:AjaxSetting>
    </AjaxSettings>
</rad:RadAjaxManagerProxy>
<rad:RadWindowManager ID="rwm" runat="server" RenderMode="Lightweight" Skin="Bootstrap">
    <Windows>
        <rad:RadWindow RenderMode="Lightweight" ID="modalPopup" runat="server" Modal="true"
            OffsetElementID="main" OnClientShow="setCustomPosition" Style="z-index: 10;"
            Skin="Bootstrap" Behaviors="Close" Title="Select content/s" MinWidth="500px"
            MinHeight="500px">
            <ContentTemplate>
                <table width="100%">
                    <tr>
                        <td>
                            <small>Select content</small>
                        </td>
                        <td>
                        </td>
                        <td>
                            <small>Select preset schedule</small>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px;" valign="top">
                            <rad:RadComboBox  ID="ddContent" runat="server" CssClass="input-sm" Skin="Bootstrap" RenderMode="Lightweight" EmptyMessage="- Select a Content -"
                                Width="150"/>
                           <%-- <asp:DropDownList ID="ddContent" runat="server" CssClass="form-control input-sm"
                                Width="150">
                            </asp:DropDownList>--%>
                        </td>
                        <td>
                        </td>
                        <td style="padding: 5px;">
                            <table>
                                <tr>
                                    <td>
                                        <small>Start:</small>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="StartDate" runat="server" placeholder="From" type="date" CssClass="form-control input-sm pull-left" style="width:80%" required></asp:TextBox>
                                        <%--<span class="pull-right" style="color:red">*</span>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <small>End:</small>
                                    </td>
                                    <td style="padding-top: 5px;">
                                        <asp:TextBox ID="EndDate" runat="server" placeholder="From" type="date" CssClass="form-control input-sm pull-left" style="width:80%" required></asp:TextBox>
                                     <%--   <span class="displayerror">*</span>--%>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="right" style="padding: 10px;">
                            <button id="btnCanceleno" class="btn-teal font-white no-border-radius btn" onclick="closePopup()"
                                type="button">
                                Cancel</button>&nbsp;
                        </td>
                        <td>
                            <rad:RadButton ID="BtnProceed" runat="server" OnClientClicking="confirmInput" OnClick="BtnProceed_Click"
                                CssClass="btn btn-teal font-white no-border-radius" Text="Proceed" RenderMode="Lightweight" Skin="Bootstrap">
                            </rad:RadButton>
                            <%--<asp:Button ID="BtnProceed" runat="server" CssClass="btn btn-teal font-white no-border-radius"
                                Text="Proceed" OnClick="BtnProceed_Click" />--%>
                        </td>
                    </tr>
                </table>
                <rad:RadGrid ID="RadGridContentDrip" runat="server" Skin="Bootstrap">
                    <MasterTableView CommandItemDisplay="None" GridLines="None" AllowPaging="True" PageSize="20"
                        AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="ID">
                        <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                        <Columns>
                            <rad:GridBoundColumn UniqueName="ScoTitle" HeaderText="Title" DataField="ScoTitle"
                                HeaderStyle-Font-Bold="true">
                            </rad:GridBoundColumn>
                            <rad:GridDateTimeColumn UniqueName="Starts" HeaderText="From" DataField="PresetStartDate"
                                DataType="System.DateTime" DataFormatString="{0:dd MMMM yyyy}" HeaderStyle-Font-Bold="true">
                            </rad:GridDateTimeColumn>
                            <rad:GridDateTimeColumn UniqueName="Ends" HeaderText="Ends" DataField="PresetEndDate"
                                DataType="System.DateTime" DataFormatString="{0:dd MMMM yyyy}" HeaderStyle-Font-Bold="true">
                            </rad:GridDateTimeColumn>
                        </Columns>
                    </MasterTableView>
                </rad:RadGrid>
            </ContentTemplate>
        </rad:RadWindow>
    </Windows>
</rad:RadWindowManager>
<div class="sideContent bg-color-light-gray ten-px-padding">
    <div class="row">
        <div class="course-details">
            <div class="col-md-12">
                <h5 class="font-bold">
                    <span id="lblToolTip">DRIP CONTENT <i class="fa fa-info-circle"></i></span>
                </h5>
                <rad:RadToolTip RenderMode="Lightweight" runat="server" ID="RadToolTip1" Width="200px"
                    ShowEvent="onmouseover" RelativeTo="Element" Animation="Resize" TargetControlID="lblToolTip"
                    IsClientID="true" HideEvent="LeaveTargetAndToolTip" Position="TopRight" Text="Once Enabled Users with the appropriate roles will be able to preset a schedule  on selected course contents.">
                </rad:RadToolTip>
                <span style="font-size: 12px;">This feature determines what and when content/s will
                    have a ''scheduled delivery''; unlocks courses, modules and sections on a preset
                    date and time.
                    <br />
                    <br />
                    Use the toggle switch below to activate the feature </span>
                <div class="row" style="margin-top: 20px;">
                    <div class="col-md-12">
                        <div class="text-center">
                            <input type="checkbox" id="ChkBoxDripContent" class="js-switch2" runat="server" onchange="checkboxCheck(this)"
                                data-toggle="modal" data-target="#smallShoes" />
                            <!--<asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>-->
                            <button type="button" id="x" data-toggle="modal" data-target="#smallShoes" style="display: none;"
                                data-backdrop="static" data-keyboard="false">
                                Fire</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
