﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using TranscomUniversityV3Model;
using Telerik.Web.UI;

public partial class DripContent : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["CourseID"] != null)
        {

            //LoadDropDownList();
            GetDripContent();
            //RadDatePicker1.DatePopupButton.Attributes.Add("onclick", "PopupAbove(event, '" + RadDatePicker1.ClientID + "');return false;");
        }
    }

    void LoadDropDownList()
    {

        try
        {
             //var db = new TranscomUniversityV3ModelContainer();
            // var ds = pr_Scorm_Cor_GetCurriculumPackages();
            //DataSet ds = DataHelper.GetCurriculumCourse(Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"])));
            //DataSet dsx = DataHelper.GetCurriculumPackages(Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"])), Convert.ToInt32(ds.Tables[0].Rows[0]["CurriculumCourseID"]), 0);

            //string IDs = "IDS: " + Convert.ToInt32(ds.Tables[0].Rows[0]["CurriculumCourseID"]);



            /*foreach (DataTable table in dsx.Tables)
            {
                foreach (DataRow dr in table.Rows)
                {
                
                }
            }*/

            //ddContent.DataSource = DataHelper.GetCurriculumPackages(Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"])), Convert.ToInt32(ds.Tables[0].Rows[0]["CurriculumCourseID"]), 0);
            //foreach (DataRow dr in ds.Tables[0].Rows)
            //{
            //    dr["FileName"] = dr["FileName"] + "." + dr["FileType"];
            //}

            //ddContent.DataSource = ds;
            //ddContent.DataTextField = "FileName";
            //ddContent.DataValueField = "scoID";
            //ddContent.DataBind();
        }
        catch (IndexOutOfRangeException iore)
        {
            ddContent.DataSource = null;
            ddContent.DataBind();

        }


    }

    protected void BtnProceed_Click(object sender, EventArgs e)
    {
        int selectID = Convert.ToInt32(ddContent.SelectedValue);
        var courseID = Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"]));
        DataSet dsy = DataHelper.GetDripContent(selectID,courseID);
        
        //RadWindowManager rwm = new RadWindowManager();
        if (dsy.Tables[0].Rows.Count > 0)
        {
            rwm.RadAlert("Duplicate Content", 330, 180, "Error", "");
        }
        else
        {
            DataHelper.InsertDripContent(selectID,
                Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"])),
                Convert.ToDateTime(StartDate.Text),
                Convert.ToDateTime(EndDate.Text),
                false);

            //StartDate.Text = string.Empty;
            //EndDate.Text = string.Empty;
            //ddContent.Text = "";
            GetDripContent();
            rwm.RadAlert("Content Added to Drip", 330, 180, "Success", "");
        }
    }


    void GetDripContent()
    {
        RadGridContentDrip.DataSource = DataHelper.GetDripContents(Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"])));
        RadGridContentDrip.DataBind();
    }


}