﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CourseBuilderEnrol.ascx.cs"
    Inherits="CourseBuilderEnrol" %>
<rad:RadScriptBlock ID="ctRadScriptBlock1" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            reInitSwitchery(0);


        });

        function reInitSwitchery(type) {
            var elems2;
            if (type == 0)
                elems2 = Array.prototype.slice.call(document.querySelectorAll('.js-switch2'));
            else
                elems2 = Array.prototype.slice.call(document.querySelectorAll('.js-switch2.js-enrollment'));

            elems2.forEach(function (html) {
                new Switchery(html, {
                    size: 'small',
                    color: '#FFFFFF',
                    secondaryColor: '#FFFFFF', //off
                    jackColor: '#09A8A4', //on
                    jackSecondaryColor: '#BBBBBB' //off
                });
            });
        }

    </script>
</rad:RadScriptBlock>
<div id="pnlContent" class="contentSidebarContainer courseSidebarContainer content-tab bg-color-dark-gray"
    style="font-size: inherit;">
    <div class="row" style="height: inherit;">
        <div class="col-md-3 tabButtonGroup" style="color: white">
            <div class="text-center active" id="btnCourseUpload" style="font-size: 12px;">
                <h2 style="color: white !important;">
                    <i class="fa fa-graduation-cap"></i>
                </h2>
                Enrollment
            </div>
        </div>
        <div class="col-md-9" style="padding: 0 !important;">
            <div class="contentDetails">
                <div id="ctIntro" class="tabcontent active" style="display: block; color: #fff;">
                    <h5 class="color-white" style="padding: 5px;">
                        Enrollment features</h5>
                    <table style="color: #fff; font-size: 13px;">
                        <%--  <tr>
                            <asp:HiddenField ID="hdnCourseID" runat="server" />
                            <td colspan="2" style="padding: 5px">
                                <input type="checkbox" id="ChkAllowLearners" runat="server" class="js-switch2" onchange="checkBoxChange(this, 'AllowLearners')" />
                                Allow Learners to enroll
                            </td>
                        </tr>--%>
                        <tr>
                            <asp:HiddenField ID="hdnCourseID" runat="server" />
                            <td colspan="2" style="padding: 5px">
                                <input type="checkbox" id="ChkWithApproval" runat="server" class="js-switch2 js-enrollment" onchange="checkBoxChange2(this, 'WithApproval', 'js-enrollment')" />
                                With approval
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding: 5px">
                                <input type="checkbox" id="ChkOpenEnrollment" runat="server" class="js-switch2 js-enrollment" onchange="checkBoxChange2(this, 'IsOpen', 'js-enrollment')" />
                                Open enrollment
                            </td>
                        </tr>
                    </table>
                   <%-- <h5 class="color-white" style="padding: 5px;">
                        Enrollment actions</h5>
                    <table style="color: #fff; font-size: 13px;">
                        <tr>
                            <td colspan="2" style="padding: 5px">
                                <input type="checkbox" id="ChkNotifyTrainers" runat="server" class="js-switch2" onchange="checkBoxChange(this, 'NotifyTrainers')" />
                                Notify Trainers
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding: 5px">
                                <input type="checkbox" id="ChkNotifySupervisor" runat="server" class="js-switch2"
                                    onchange="checkBoxChange(this, 'NotifySupervisors')" />
                                Notify Supervisors
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding: 5px">
                                <input type="checkbox" id="ChkNotifyManagers" runat="server" class="js-switch2" onchange="checkBoxChange(this, 'NotifyManagers')" />
                                Notify Managers
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding: 5px">
                                <input type="checkbox" id="ChkNotifyAdmins" runat="server" class="js-switch2" onchange="checkBoxChange(this, 'NotifyAdmins')" />
                                Notify Admins
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding: 5px">
                                <input type="checkbox" id="ChkUnenrollments" runat="server" class="js-switch2" onchange="checkBoxChange(this, 'Unenrollments')" />
                                Apply to Unenrollments
                            </td>
                        </tr>
                    </table>--%>
                </div>
            </div>
        </div>
    </div>
</div>
<rad:RadScriptBlock ID="radScript" runat="server">
    <script type="text/javascript">

        function checkBoxChange(check, nameVal) {
            var courseID = $("#<%=hdnCourseID.ClientID %>").val();

            var actPage = nameVal;
            var isChecked = $(check).is(":checked");


            var bitVal;
            if (isChecked) {
                bitVal = 1;
            } else {
                bitVal = 0;
            }

            $.ajax({
                type: "POST",
                data: "{ CourseID: '" + courseID + "', BitVal: '" + bitVal + "', ActPage: '" + actPage + "'}",
                contentType: "application/json; charset=utf-8",
                url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "SaveCourseSetting2",
                dataType: 'json',
                success: function (msg) {
                    console.log(bitVal);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(XMLHttpRequest.responseText);
                }
            });

            return false;
        }

        function checkBoxChange2(check, nameVal, withClass) {
            
            var courseID = $("#<%=hdnCourseID.ClientID %>").val();

            var actPage = nameVal;
            var isChecked = $(check).is(":checked");
            //$('.js-switch2.' + withClass).click();

            debugger;
            $(".js-switch2." + withClass).siblings().remove();
            $(".js-switch2." + withClass).prop("checked", false);
            if (isChecked)
                $(check).prop("checked", true);
            reInitSwitchery(1);
            
            var bitVal;
            if (isChecked) {
                bitVal = 1;
            } else {
                bitVal = 0;
            }

            $.ajax({
                type: "POST",
                data: "{ CourseID: '" + courseID + "', BitVal: '" + bitVal + "', ActPage: '" + actPage + "'}",
                contentType: "application/json; charset=utf-8",
                url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "SaveCourseSetting2",
                dataType: 'json',
                success: function (msg) {
                    console.log(bitVal);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(XMLHttpRequest.responseText);
                }
            });

            return false;
        }

    </script>
</rad:RadScriptBlock>
