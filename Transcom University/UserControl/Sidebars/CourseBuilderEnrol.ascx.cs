﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;

public partial class CourseBuilderEnrol : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            foreach (Control control in this.Controls)
            {
                if (control is CheckBox)
                {
                    ((CheckBox)control).InputAttributes["class"] = "js-switch2";
                }
            }

            hdnCourseID.Value = Request.QueryString["CourseID"].ToString();

            DataSet ds = DataHelper.GetCourseSetting(Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"])));

            //ChkAllowLearners.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["AllowLearnersToEnroll"]) == true ? true : false;
            ChkOpenEnrollment.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["IsOpen"]) == true ? true : false;
            ChkWithApproval.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["WithApproval"]) == true ? true : false;
            //ChkNotifyTrainers.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["NotifyTrainers"]) == true ? true : false;
            //ChkNotifySupervisor.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["NotifySupervisor"]) == true ? true : false;
            //ChkNotifyManagers.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["NotifyManagers"]) == true ? true : false;
            //ChkNotifyAdmins.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["NotifyAdmin"]) == true ? true : false;
            //ChkUnenrollments.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["ApplyUnenrollments"]) == true ? true : false;
        }
        }
    
}