﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CourseBuilderNotifs.ascx.cs"
    Inherits="CourseBuilderNotifs" %>
<%@ Register Src="NotifContentInit.ascx" TagName="NotifContentInit" TagPrefix="uc1" %>
<%@ Register Src="NotifAlertBox.ascx" TagName="NotifAlertBox" TagPrefix="uc2" %>
<%@ Register Src="NotifEmail.ascx" TagName="NotifEmail" TagPrefix="uc3" %>
<rad:RadScriptBlock ID="ctRadScriptBlock1" runat="server">
   <script type="text/javascript">

       function pageLoad() {
       }
       $(document).ready(function () {

           var elems2 = Array.prototype.slice.call(document.querySelectorAll('.js-switch2'));

           elems2.forEach(function (html) {
               new Switchery(html, {
                   size: 'small',
                   color: '#FFFFFF',
                   secondaryColor: '#FFFFFF', //off
                   jackColor: '#09A8A4', //on
                   jackSecondaryColor: '#BBBBBB' //off
               });
           });

       });

        function openSideContent(evt, actionName, obj) {
            var btnName = $(obj).parent().attr('id');

            $('.tabcontent').css({ 'display': 'none' });
            $('.tablinks').removeClass('active');
            $('.tabcontent').removeClass('active');
            $('.tab-action').removeClass('active');
            $('#' + btnName).addClass('active');
            $('#' + actionName).css({ 'display': 'block' }).addClass('active');
        }

        function createAction(action, obj) {
            var obj_id = $(obj).attr('id');

            $('.tab-action').removeClass('active');
            $('#' + obj_id).addClass('active');

            if (action == '1') {
            }
        }


        function onCheckChangeNotif(chkbox, nameVal) {
            var courseID = $("#<%=hdnCourseID.ClientID %>").val();

            var actPage = nameVal;
            var isChecked = $(chkbox).is(":checked");


            var bitVal;
            if (isChecked) {
                bitVal = 1;
            } else {
                bitVal = 0;
            }

            $.ajax({
                type: "POST",
                data: "{ CourseID: '" + courseID + "', BitVal: '" + bitVal + "', ActPage: '" + actPage + "'}",
                contentType: "application/json; charset=utf-8",
                url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "SaveCourseSettingNotif",
                dataType: 'json',
                success: function (msg) {
                    //console.log(bitVal);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(XMLHttpRequest.responseText);
                }
            });

            return false;
        }

    </script>
</rad:RadScriptBlock>
<div id="pnlContent" class="contentSidebarContainer courseSidebarContainer content-tab bg-color-dark-gray"
    style="font-size: inherit;">
    <div class="row" style="height: inherit;">
        <div class="col-md-3 tabButtonGroup">
            <div class="tablinks" id="btnCourseUpload">
                <button id="btnUpload" runat="server" title="Upload" onclick="openSideContent(event, 'ctUpload', this); return false">
                    <h3 style="color: white;">
                        <i class="fa fa-bell-o"></i>
                    </h3>
                    Alert Box
                </button>
            </div>
            <div class="tablinks" id="btnCourseCreate">
                <button id="btnCreate" runat="server" title="Create" onclick="openSideContent(event, 'ctCreate', this); return false">
                    <h3 style="color: white;">
                        <i class="fa fa-envelope-o"></i>
                    </h3>
                    Email
                </button>
            </div>
        </div>
        <div class="col-md-9" style="padding: 0 !important;">
            <div class="contentDetails">
                <div id="ctIntro" class="tabcontent active text-center" style="display: block; font-size: 14px;">
                    <div class="row">
                        <div class="col-md-12 text-center" style="margin-top: 40px;">
                            Configure Alert Box feature for
                            <br />
                            on-site activity notifications
                        </div>
                        <div class="col-md-12 text-center" style="margin-top: 70px;">
                            Configure Alert Box feature for
                            <br />
                            on-the-go activity notifications
                        </div>
                    </div>
                </div>
                <div id="ctUpload" class="tabcontent">
                    <div class="row">
                        <div class="col-md-12">
                            <h5 class="color-white">
                                Alert Box feature for</h5>
                            <table style="color: #fff; font-size: 13px;">
                                <tr>
                                    <td colspan="2" style="padding: 5px">
                                        <input type="checkbox" id="ChkEnrolRequest" runat="server" class="js-switch2" onchange="onCheckChangeNotif(this, 'EnrolRequest')" />
                                        Enrollment requests
                                    </td>
                                </tr>
                                <tr><asp:HiddenField ID="hdnCourseID" runat="server" />
                                    <td style="padding: 5px">
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                    <td>
                                        <input type="checkbox" id="ChkTrainers" runat="server" class="js-switch2" onchange="onCheckChangeNotif(this, 'Trainers')" />
                                        Trainers
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 5px">
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                    <td>
                                        <input type="checkbox" id="ChkSupervisors" runat="server" class="js-switch2" onchange="onCheckChangeNotif(this, 'Supervisors')" />
                                        Supervisors
                                    </td>
                                </tr>
                                <tr valign="middle">
                                    <td style="padding: 5px">
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                    <td>
                                        <input type="checkbox" id="ChkManagers" runat="server" class="js-switch2" onchange="onCheckChangeNotif(this, 'Managers')" />
                                        Managers
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="padding: 5px">
                                        <input type="checkbox" id="ChkCourseAvailability" runat="server" class="js-switch2" onchange="onCheckChangeNotif(this, 'CourseAvailability')" />
                                        Course Availability
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="padding: 5px">
                                        <input type="checkbox" id="ChkAssigments" runat="server" class="js-switch2" onchange="onCheckChangeNotif(this, 'ClassAssignments')" />
                                        Class Assignments
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="padding: 5px">
                                        <input type="checkbox" id="ChkOverdue" runat="server" class="js-switch2" onchange="onCheckChangeNotif(this, 'OverdueCourses')" />
                                        Overdue Courses
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="padding: 5px">
                                        <input type="checkbox" id="ChkForum" runat="server" class="js-switch2" onchange="onCheckChangeNotif(this, 'Forums')" />
                                        Forums
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div id="ctCreate" class="tabcontent">
                    <h5 class="color-white">
                        Email feature for</h5>
                    <table style="color: #fff; font-size: 13px;">
                        <tr>
                            <td colspan="2" style="padding: 5px">
                                <input type="checkbox" id="CheckBox1" runat="server" class="js-switch2" onchange="onCheckChangeNotif(this, 'EnrolRequest1')" />
                                Enrollment requests
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                &nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                            <td>
                                <input type="checkbox" id="CheckBox2" runat="server" class="js-switch2" onchange="onCheckChangeNotif(this, 'Trainers1')" />
                                Trainers
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                &nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                            <td>
                                <input type="checkbox" id="CheckBox3" runat="server" class="js-switch2" onchange="onCheckChangeNotif(this, 'Supervisors1')" />
                                Supervisors
                            </td>
                        </tr>
                        <tr valign="middle">
                            <td style="padding: 5px">
                                &nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                            <td>
                                <input type="checkbox" id="CheckBox4" runat="server" class="js-switch2" onchange="onCheckChangeNotif(this, 'Managers1')" />
                                Managers
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding: 5px">
                                <input type="checkbox" id="CheckBox5" runat="server" class="js-switch2" onchange="onCheckChangeNotif(this, 'CourseAvailability1')" />
                                Course Availability
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding: 5px">
                                <input type="checkbox" id="CheckBox6" runat="server" class="js-switch2" onchange="onCheckChangeNotif(this, 'ClassAssignments1')" />
                                Class Assignments
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding: 5px">
                                <input type="checkbox" id="CheckBox7" runat="server" class="js-switch2" onchange="onCheckChangeNotif(this, 'OverdueCourses1')" />
                                Overdue Courses
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding: 5px">
                                <input type="checkbox" id="CheckBox8" runat="server" class="js-switch2" onchange="onCheckChangeNotif(this, 'Forums1')" />
                                Forums
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
