﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CourseBuilderNotifs : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        hdnCourseID.Value = Request.QueryString["CourseID"];

        foreach (Control control in this.Controls)
        {
            if (control is CheckBox)
            {
                ((CheckBox)control).InputAttributes["class"] = "js-switch2";
            }
        }

        DataSet ds = DataHelper.GetCourseSettingNotif(Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"])));

        if (ds.Tables[0].Rows.Count > 0)
        {
            ChkEnrolRequest.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["AlertEnrollmentReq"]) == true ? true : false;
            ChkTrainers.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["AlertTrainers"]) == true ? true : false;
            ChkSupervisors.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["AlertSupervisors"]) == true ? true : false;
            ChkManagers.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["AlertManagers"]) == true ? true : false;
            ChkCourseAvailability.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["AlertCourseAvailability"]) == true ? true : false;
            ChkAssigments.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["AlertClassAssignments"]) == true ? true : false;
            ChkOverdue.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["AlertOverdueCourses"]) == true ? true : false;
            ChkForum.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["AlertForums"]) == true ? true : false;

            CheckBox1.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["EmailEnrollmentReq"]) == true ? true : false;
            CheckBox2.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["EmailTrainers"]) == true ? true : false;
            CheckBox3.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["EmailSupervisors"]) == true ? true : false;
            CheckBox4.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["EmailManagers"]) == true ? true : false;
            CheckBox5.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["EmailCourseAvailability"]) == true ? true : false;
            CheckBox6.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["EmailClassAssignments"]) == true ? true : false;
            CheckBox7.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["EmailOverdueCourses"]) == true ? true : false;
            CheckBox8.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["EmailForums"]) == true ? true : false;
        }
    }
}