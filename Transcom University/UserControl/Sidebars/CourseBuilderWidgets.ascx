﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CourseBuilderWidgets.ascx.cs"
    Inherits="CourseBuilderWidgets" %>

<rad:RadScriptBlock ID="ctRadScriptBlock1" runat="server">
    <script type="text/javascript">         

        function pageLoad() {
        }
            $(document).ready(function () {

            var elems2 = Array.prototype.slice.call(document.querySelectorAll('.js-switch2'));

            elems2.forEach(function (html) {
                new Switchery(html, {
                    size: 'small',
                    color: '#FFFFFF',
                    secondaryColor: '#FFFFFF', //off
                    jackColor: '#09A8A4', //on
                    jackSecondaryColor: '#BBBBBB' //off
                });
            });

        });

        function openSideContent(evt, actionName, obj) {
            var btnName = $(obj).parent().attr('id');

            $('.tabcontent').css({ 'display': 'none' });
            $('.tablinks').removeClass('active');
            $('.tabcontent').removeClass('active');
            $('.tab-action').removeClass('active');
            $('#' + btnName).addClass('active');
            $('#' + actionName).css({ 'display': 'block' }).addClass('active');
        }

        function createAction(action, obj) {
            var obj_id = $(obj).attr('id');

            $('.tab-action').removeClass('active');
            $('#' + obj_id).addClass('active');

            if (action == '1') {
            }
        }
    </script>
</rad:RadScriptBlock>
<div id="pnlContent" class="contentSidebarContainer courseSidebarContainer content-tab bg-color-dark-gray"
    style="font-size: inherit;">
    <div class="row" style="height: inherit;">
        <div class="col-md-3 tabButtonGroup">
            <div class="tablinks" id="btnCourseUpload">
                <button id="btnUpload" runat="server" title="Upload" onclick="openSideContent(event, 'ctUpload', this); return false">
                    <h3 style="color: white;">
                        <i class="fa fa-comments-o"></i>
                    </h3>
                    Forums
                </button>
            </div>
            <div class="tablinks" id="btnCourseCreate">
                <button id="btnCreate" runat="server" title="Create" onclick="openSideContent(event, 'ctCreate', this); return false">
                    <h3 style="color: white;">
                        <i class="fa fa-files-o"></i>
                    </h3>
                    Similar Courses
                </button>
            </div>
            <div class="tablinks" id="btnCourseViews">
                <button id="Button1" runat="server" title="Create" onclick="openSideContent(event, 'ctCourseViews', this); return false">
                    <h3 style="color: white;">
                        <i class="fa fa-file-text"></i>
                    </h3>
                    Course Views
                </button>
            </div>
        </div>
        <div class="col-md-9" style="padding: 5px !important;">
            <div class="contentDetails">
                <div id="ctIntro" class="tabcontent active text-center" style="display: block; font-size: 14px;">
                    <div class="row">
                        <div class="col-md-12 text-center" style="margin-top: 40px;">
                            Select &amp; configure widgets
                        </div>
                    </div>
                </div>
                <asp:HiddenField ID="hdnCourseID" runat="server" />
                <div id="ctUpload" class="tabcontent">
                    <div class="row">
                        <div class="col-md-12">
                            <h5 class="color-white" style="padding: 5px;">
                                Forum setting:</h5>

                            <table style="color: #fff; font-size: 13px;">
                                <tr>
                                    <td colspan="2" style="padding: 5px">
                                        <input type="checkbox" id="ChkAddWidget" runat="server" class="js-switch2" onchange="checkBoxChange(this, 'Forum')" />
                                        Add widget
                                    </td>
                                </tr>
                            </table>
                            <table class="ForumSettingComplete"  style="color: #fff; font-size: 13px;">
                                
                                <tr>
                                    <td style="padding: 5px">
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                    <td>
                                        <input type="checkbox" id="ChkAllUsers" runat="server" class="js-switch2 js-forum-access" onchange="checkBoxChangeForum2(this, 'UsersCanParticipate', 'js-forum-access')" />
                                        All users can participate
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 5px">
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                    <td>
                                        <input type="checkbox" id="ChkEnrolledOnly" runat="server" class="js-switch2 js-forum-access" onchange="checkBoxChangeForum2(this, 'EnrolledOnly', 'js-forum-access')" />
                                        Enrolled users only
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="padding: 5px">
                                        <input type="checkbox" id="ChkAllowFileUpload" runat="server" class="js-switch2" onchange="checkBoxChangeForum(this, 'AllowUpload')" />
                                        Allow file upload
                                    </td>
                                </tr>
                                <%--<tr>
                                    <td style="padding: 5px">
                                        &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                    <td>
                                        <asp:Button Text="View accepted file types" runat="server" CssClass="btn btn-sm btn-teal text-center"  />
                                        <a href="#" class="btn btn-sm btn-teal text-center" style="color: #fff;">View accepted file types</a>
                                        <br />
                                    </td>
                                </tr>--%>
                                <tr>
                                    <td>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                    <td style="padding: 5px">
                                        <select id="ComSizeUpload" runat="server" class="form-control input-sm" style="width: 100px;" onchange="SetComSizeUpload(this, 'UploadSize')">
                                            
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                    <td>
                                        <small>set max upload size</small>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                    <td style="padding: 5px">
                                        <select id="ComSizeChar" runat="server" class="form-control input-sm"  style="width: 100px;" onchange="SetComSizeUpload(this, 'CharSize')">
                                            <option value="140">140</option>
                                            <option value="280">280</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="2000">2000</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                    <td>
                                        <small>set max characters</small>
                                    </td>
                                </tr>
                               <%-- <tr>
                                    <td colspan="2" style="padding: 5px">
                                        <input type="checkbox" id="ChkCanJoinMultiForums" runat="server" class="js-switch2" onchange="checkBoxChangeForum(this, 'CanJoinMultiForums')" />
                                        Can join multiple forums
                                    </td>
                                </tr>--%>
                                <%--<tr>
                                    <td colspan="2" style="padding: 5px">
                                        <input type="checkbox" id="ChkLimitToClass" runat="server" class="js-switch2" onchange="checkBoxChangeForum(this, 'LimitToClass')" />
                                        Limit to participant's class
                                    </td>
                                </tr>--%>
                            </table>
                        </div>
                    </div>
                </div>
                <div id="ctCreate" class="tabcontent">
                    <h5 class="color-white" style="padding: 7px;">
                        Similar Courses setting:</h5>
                    <table style="color: #fff; font-size: 13px;">
                        <tr>
                            <td colspan="2" style="padding: 7px">
                                <input type="checkbox" id="ChkAddWidgetCourse" runat="server" class="js-switch2" onchange="checkBoxChange(this, 'SimilarCourse')" />
                                Add Widget
                            </td>
                        </tr>
                    </table>
                    <table style="color: #fff; font-size: 13px;" class="SimilarSettingComplete" >
                        <tr>
                            <td style="padding: 7px">
                                <input type="checkbox" id="Checkbox4" runat="server" class="js-switch2" onchange="checkBoxChangeSimilarCourse(this, 'SameCategory')" />
                            </td>
                            <td>
                                Courses under the same Categories
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 7px">
                                <input type="checkbox" id="Checkbox5" runat="server" class="js-switch2" onchange="checkBoxChangeSimilarCourse(this, 'SameSubCategory')" />
                            </td>
                            <td>
                                Courses under the same Sub-categories
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 7px">
                                <input type="checkbox" id="Checkbox6" runat="server" class="js-switch2" onchange="checkBoxChangeSimilarCourse(this, 'SameAuthor')" />
                            </td>
                            <td>
                                Courses under the same Author
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 7px">
                                <input type="checkbox" id="Checkbox7" runat="server" class="js-switch2" onchange="checkBoxChangeSimilarCourse(this, 'SameDept')" />
                            </td>
                            <td>
                                Courses under the same Department
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 7px">
                                <input type="checkbox" id="Checkbox8" runat="server" class="js-switch2" onchange="checkBoxChangeSimilarCourse(this, 'BundleCourse')" />
                            </td>
                            <td>
                                Bundles Courses
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 7px">
                                <input type="checkbox" id="Checkbox9" runat="server" class="js-switch2" onchange="checkBoxChangeSimilarCourse(this, 'SameProgram')" />
                            </td>
                            <td>
                                Courses under the same Program
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 7px">
                                <input type="checkbox" id="Checkbox10" runat="server" class="js-switch2" onchange="checkBoxChangeSimilarCourse(this, 'SameCatalogue')" />
                            </td>
                            <td>
                                Courses under the same Catalogue Category
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 7px">
                                <input type="checkbox" id="Checkbox11" runat="server" class="js-switch2" onchange="checkBoxChangeSimilarCourse(this, 'SameClassType')" />
                            </td>
                            <td>
                                Courses under the same Course / Class Type
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 7px">
                                <input type="checkbox" id="Checkbox12" runat="server" class="js-switch2" onchange="checkBoxChangeSimilarCourse(this, 'SaveAverageRating')" />
                            </td>
                            <td>
                                Same Averaged Rating (Real-time view)
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="ctCourseViews" class="tabcontent">
                    <h5 class="color-white" style="padding: 7px;">
                        Course view setting:</h5>
                    <table style="color: #fff; font-size: 13px;">
                        <tr>
                            <td colspan="2" style="padding: 7px">
                                <input type="checkbox" id="ChkCourseView" runat="server" class="js-switch2" onchange="checkBoxChange(this, 'CourseView')" />
                                Add Widget
                            </td>
                        </tr>
                    </table>
                    <table style="color: #fff; font-size: 13px;" class="CourseViewComplete">
                        <tr>
                            <td style="padding: 7px">
                                <input type="checkbox" id="Checkbox1" runat="server" class="js-switch2" onchange="checkBoxChangeCourseView(this, 'ClickTitle')" />
                            </td>
                            <td>
                                Clicking the Course Title or Image
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 7px">
                                <input type="checkbox" id="Checkbox2" runat="server" class="js-switch2" onchange="checkBoxChangeCourseView(this, 'CountUnique')" />
                            </td>
                            <td>
                                Count of Unique IDs only regardless of visits
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 7px">
                                <input type="checkbox" id="Checkbox3" runat="server" class="js-switch2" onchange="checkBoxChangeCourseView(this, 'CountVisit')" />
                            </td>
                            <td>
                                Count of the visits regarless of the IDs
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<rad:RadScriptBlock ID="radScript" runat="server">
<script type="text/javascript">

    Sys.Application.add_load(function () {
        (function (d, $) {
            var $somebox = $('#ChkAddWidget'),
        $mainbox = $('.ForumSettingComplete');

            var toggleMain = function () {
                $mainbox.slideToggle();
            };
            if ($somebox.is(':checked')) {
                toggleMain();
            }
            $somebox.on('click', toggleMain);
        })(document, jQuery);
    });

    function SetComSizeUpload(select, label) {
        var courseID = $("#<%=hdnCourseID.ClientID %>").val();
        var SelectedVal = $(select).val();
        var Labeling = label;

        //alert(SelectedVal);

        $.ajax({
            type: "POST",
            data: "{ CourseID: '" + courseID + "', BitVal: '" + SelectedVal + "', ActPage: '" + Labeling + "'}",
            contentType: "application/json; charset=utf-8",
            url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "SaveForumSetting2",
            dataType: 'json',
            success: function (msg) {
                //console.log(bitVal);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert(XMLHttpRequest.responseText);
            }
        });

        return false;
    }

    /*Sys.Application.add_load(function () {

        $(document).ready(function () {
            var isChecked = $("#ChkAddWidget").is(":checked");

            if (isChecked) {
                $(".ForumSettingComplete").show();
            } else {
                $(".ForumSettingComplete").hide();
            }
        });
    });*/

    function checkBoxChange(check, nameVal) {
        var courseID = $("#<%=hdnCourseID.ClientID %>").val();

        var actPage = nameVal;
        var isChecked = $(check).is(":checked");


        var bitVal;
        if (isChecked) {
            bitVal = 1;
        } else {
            bitVal = 0;
        }

        if (bitVal == 1 && nameVal == "Forum") {
            $(".ForumSettingComplete").show();
        } else {
            $(".ForumSettingComplete").hide();
        }

        if (bitVal == 1 && nameVal == "SimilarCourse") {
            $(".SimilarSettingComplete").show();
        } else {
            $(".SimilarSettingComplete").hide();
        }

        if (bitVal == 1 && nameVal == "CourseView") {
            $(".CourseViewComplete").show();
        } else {
            $(".CourseViewComplete").hide();
        }
        

        $.ajax({
            type: "POST",
            data: "{ CourseID: '" + courseID + "', BitVal: '" + bitVal + "', ActPage: '" + actPage + "'}",
            contentType: "application/json; charset=utf-8",
            url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "SaveCourseSetting2",
            dataType: 'json',
            success: function (msg) {
                //console.log(bitVal);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert(XMLHttpRequest.responseText);
            }
        });

        return false;
    }

    function checkBoxChangeForum(check, nameVal) {
        var courseID = $("#<%=hdnCourseID.ClientID %>").val();

        var actPage = nameVal;
        var isChecked = $(check).is(":checked");

        var bitVal;
        if (isChecked) {
            bitVal = 1;
        } else {
            bitVal = 0;
        }

        $.ajax({
            type: "POST",
            data: "{ CourseID: '" + courseID + "', BitVal: '" + bitVal + "', ActPage: '" + actPage + "'}",
            contentType: "application/json; charset=utf-8",
            url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "SaveForumSetting",
            dataType: 'json',
            success: function (msg) {
                //console.log(bitVal);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert(XMLHttpRequest.responseText);
            }
        });

        return false;
    }

    function checkBoxChangeSimilarCourse(check, nameVal) {
        var courseID = $("#<%=hdnCourseID.ClientID %>").val();

        var actPage = nameVal;
        var isChecked = $(check).is(":checked");

        var bitVal;
        if (isChecked) {
            bitVal = 1;
        } else {
            bitVal = 0;
        }

        $.ajax({
            type: "POST",
            data: "{ CourseID: '" + courseID + "', BitVal: '" + bitVal + "', ActPage: '" + actPage + "'}",
            contentType: "application/json; charset=utf-8",
            url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "SaveSimilarCourseSetting",
            dataType: 'json',
            success: function (msg) {
                //console.log(bitVal);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert(XMLHttpRequest.responseText);
            }
        });

        return false;
    }

    function checkBoxChangeForum2(check, nameVal, withClass) {
        var actPage = nameVal;
        var isChecked = $(check).is(":checked");
        //$('.js-switch2.' + withClass).click();

        debugger;
        $(".js-switch2." + withClass).siblings().remove();
        $(".js-switch2." + withClass).prop("checked", false);
        if (isChecked)
            $(check).prop("checked", true);

        reInitSwitchery(1);

        var bitVal;
        if (isChecked) {
            bitVal = 1;
        } else {
            bitVal = 0;
        }

        var courseID = $("#<%=hdnCourseID.ClientID %>").val();

        var actPage = nameVal;
        var isChecked = $(check).is(":checked");

        var bitVal;
        if (isChecked) {
            bitVal = 1;
        } else {
            bitVal = 0;
        }

        $.ajax({
            type: "POST",
            data: "{ CourseID: '" + courseID + "', BitVal: '" + bitVal + "', ActPage: '" + actPage + "'}",
            contentType: "application/json; charset=utf-8",
            url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "SaveForumSetting",
            dataType: 'json',
            success: function (msg) {
                //console.log(bitVal);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert(XMLHttpRequest.responseText);
            }
        });

        return false;

    }

    function reInitSwitchery(type) {
        var elems2;
        if (type == 0)
            elems2 = Array.prototype.slice.call(document.querySelectorAll('.js-switch2'));
        else
            elems2 = Array.prototype.slice.call(document.querySelectorAll('.js-switch2.js-forum-access'));

        elems2.forEach(function (html) {
            new Switchery(html, {
                size: 'small',
                color: '#FFFFFF',
                secondaryColor: '#FFFFFF', //off
                jackColor: '#09A8A4', //on
                jackSecondaryColor: '#BBBBBB' //off
            });
        });
    }


</script>
</rad:RadScriptBlock>
