﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CourseBuilderWidgets : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            foreach (Control control in this.Controls)
            {
                if (control is CheckBox)
                {
                    ((CheckBox)control).InputAttributes["class"] = "js-switch2";
                }
            }
        

        hdnCourseID.Value = Request.QueryString["CourseID"].ToString();
        DataSet ds = DataHelper.GetCourseSetting(Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"])));

        ChkAddWidget.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["ForumWidget"]) == true ? true : false;
        ChkAddWidgetCourse.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["SimilarCourseWidget"]) == true ? true : false;
        ChkCourseView.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["CourseViewWidget"]) == true ? true : false;

        DataSet dsx, dsy, dsz;

        LoadComboBox();
        LoadCombo2();


        if (ds.Tables[0].Rows.Count > 0)
        {
            dsx = DataHelper.GetForumSetting(Convert.ToInt32(ds.Tables[0].Rows[0]["CourseSettingID"]));

            if (dsx.Tables[0].Rows.Count > 0)
            {
                ChkAllUsers.Checked = Convert.ToBoolean(dsx.Tables[0].Rows[0]["UsersCanParticipate"]) == true ? true : false;

                ChkEnrolledOnly.Checked = Convert.ToBoolean(dsx.Tables[0].Rows[0]["EnrolledUsersOnly"]) == true ? true : false;
                ChkAllowFileUpload.Checked = Convert.ToBoolean(dsx.Tables[0].Rows[0]["AllowFileUpload"]) == true ? true : false; 
                //ChkCanJoinMultiForums.Checked = Convert.ToBoolean(dsx.Tables[0].Rows[0]["CanJoinMultipleForums"]) == true ? true : false;
                //ChkLimitToClass.Checked = Convert.ToBoolean(dsx.Tables[0].Rows[0]["LimitToClass"]) == true ? true : false;
                
                if(Convert.ToInt32(dsx.Tables[0].Rows[0]["MaxUploadSize"].ToString()) > 0)
                    ComSizeUpload.Items.FindByValue(dsx.Tables[0].Rows[0]["MaxUploadSize"].ToString()).Selected = true;

                if (Convert.ToInt32(dsx.Tables[0].Rows[0]["MaxChars"].ToString()) > 0)
                    ComSizeChar.Items.FindByValue(dsx.Tables[0].Rows[0]["MaxChars"].ToString()).Selected = true;

            }

            dsy = DataHelper.GetSimilarCourseSetting(Convert.ToInt32(ds.Tables[0].Rows[0]["CourseSettingID"]));

            if (dsy.Tables[0].Rows.Count > 0)
            {
                Checkbox4.Checked = Convert.ToBoolean(dsy.Tables[0].Rows[0]["SameCategory"]) == true ? true : false;
                Checkbox5.Checked = Convert.ToBoolean(dsy.Tables[0].Rows[0]["SameSubCategory"]) == true ? true : false;
                Checkbox6.Checked = Convert.ToBoolean(dsy.Tables[0].Rows[0]["SameAuthor"]) == true ? true : false;
                Checkbox7.Checked = Convert.ToBoolean(dsy.Tables[0].Rows[0]["SameDepartment"]) == true ? true : false;
                Checkbox8.Checked = Convert.ToBoolean(dsy.Tables[0].Rows[0]["BundledCourse"]) == true ? true : false;
                Checkbox9.Checked = Convert.ToBoolean(dsy.Tables[0].Rows[0]["SameProgram"]) == true ? true : false;
                Checkbox10.Checked = Convert.ToBoolean(dsy.Tables[0].Rows[0]["SameCatalogueCategory"]) == true ? true : false;
                Checkbox11.Checked = Convert.ToBoolean(dsy.Tables[0].Rows[0]["SameCourseType"]) == true ? true : false;
                Checkbox12.Checked = Convert.ToBoolean(dsy.Tables[0].Rows[0]["SameRating"]) == true ? true : false;
            }


            dsz = DataHelper.GetCourseViewSetting(Convert.ToInt32(ds.Tables[0].Rows[0]["CourseSettingID"]));

            if (dsz.Tables[0].Rows.Count > 0)
            {
                Checkbox1.Checked = Convert.ToBoolean(dsz.Tables[0].Rows[0]["ClickTitleOrImage"]) == true ? true : false;
                Checkbox2.Checked = Convert.ToBoolean(dsz.Tables[0].Rows[0]["CountOfUniqueIDVisits"]) == true ? true : false;
                Checkbox3.Checked = Convert.ToBoolean(dsz.Tables[0].Rows[0]["CountOfIDVisits"]) == true ? true : false;
            }


        }

        }




        /*LinkForums.NavigateUrl = "~/CourseBuilder.aspx?CourseID=" + Request.QueryString["CourseID"] + "&tab=Widgets&w=forums";
        LinkSimilarCourse.NavigateUrl = "~/CourseBuilder.aspx?CourseID=" + Request.QueryString["CourseID"] + "&tab=Widgets&w=similar";
        LinkCourseViews.NavigateUrl = "~/CourseBuilder.aspx?CourseID=" + Request.QueryString["CourseID"] + "&tab=Widgets&w=views";
        if (Request.QueryString["tab"] != null && Request.QueryString["tab"] == "Widgets")
        {
            WidgetsForum1.Visible = false;
            WidgetsInit1.Visible = true;
            WidgetsSimilarCourse1.Visible = false;
            WidgetsCourseViews1.Visible = false;
        }

        if (Request.QueryString["tab"] != null && Request.QueryString["tab"] == "Widgets" && Request.QueryString["w"] == "forums")
        {
            WidgetsForum1.Visible = true;
            WidgetsInit1.Visible = false;
            WidgetsSimilarCourse1.Visible = false;
            WidgetsCourseViews1.Visible = false;
        }

        if (Request.QueryString["tab"] != null && Request.QueryString["tab"] == "Widgets" && Request.QueryString["w"] == "similar")
        {
            WidgetsForum1.Visible = false;
            WidgetsInit1.Visible = false;
            WidgetsSimilarCourse1.Visible = true;
            WidgetsCourseViews1.Visible = false;
        }

        if (Request.QueryString["tab"] != null && Request.QueryString["tab"] == "Widgets" && Request.QueryString["w"] == "views")
        {
            WidgetsForum1.Visible = false;
            WidgetsInit1.Visible = false;
            WidgetsSimilarCourse1.Visible = false;
            WidgetsCourseViews1.Visible = true;
        }*/
    }

    void LoadComboBox()
    {
        ComSizeUpload.DataSource = DataHelper.GetSizeUpload();
        ComSizeUpload.DataTextField = "Desc";
        ComSizeUpload.DataValueField = "Size";
        ComSizeUpload.DataBind();

    }

    void LoadCombo2()
    {
        ComSizeChar.DataSource = DataHelper.GetSizeChar();
        ComSizeChar.DataTextField = "Desc";
        ComSizeChar.DataValueField = "Size";
        ComSizeChar.DataBind();

    }

   
}