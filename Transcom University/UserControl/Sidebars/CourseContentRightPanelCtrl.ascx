﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CourseContentRightPanelCtrl.ascx.cs"
    Inherits="CourseContentRightPanelCtrl" %>
<%@ Register Src="~/UserControl/Pages/Assessment/editexamcontrol.ascx" TagName="ExamUC"
    TagPrefix="ucex" %>
<%@ Register Src="~/UserControl/Pages/Assessment/editexamquestioncontrol.ascx" TagName="ExamQuestionUC"
    TagPrefix="uceq" %>
<%@ Register Src="~/UserControl/Pages/Assessment/examstatuscontrol.ascx" TagName="ExamStatUC"
    TagPrefix="uces" %>
<%@ Register Src="~/UserControl/Pages/Assessment/exams.ascx" TagName="ExamsUC" TagPrefix="ucexams" %>
<rad:RadAjaxManagerProxy ID="RadAjaxManagerProxy2" runat="server">
    <AjaxSettings>
        <rad:AjaxSetting AjaxControlID="btnAddSection">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="btnAddSection" LoadingPanelID="fullPageLoader" />
                <rad:AjaxUpdatedControl ControlID="pnlCourseOrderTrue" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="rcbSCOType">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="rcbSCOType" LoadingPanelID="noLoadingPanel" />
                <rad:AjaxUpdatedControl ControlID="rcbCourseSections" LoadingPanelID="RadAjaxLoadingPanel2" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="rcbResourceTypeBrowse">
            <UpdatedControls>
                <%--<rad:AjaxUpdatedControl ControlID="rcbResourceTypeBrowse" LoadingPanelID="noLoadingPanel" />
                <rad:AjaxUpdatedControl ControlID="rcbSectionBrowse" LoadingPanelID="RadAjaxLoadingPanel2" />--%>
                <rad:AjaxUpdatedControl ControlID="pnlBrowse" LoadingPanelID="RadAjaxLoadingPanel2" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="rgAllResources">
            <UpdatedControls>
                <%--          <rad:AjaxUpdatedControl ControlID="rgAllResources" LoadingPanelID="RadAjaxLoadingPanel2" />--%>
                <rad:AjaxUpdatedControl ControlID="pnlBrowse" LoadingPanelID="RadAjaxLoadingPanel2" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnAddFiles">
            <UpdatedControls>
                <%--<rad:AjaxUpdatedControl ControlID="btnAddFiles" LoadingPanelID="noLoadingPanel" />--%>
                <rad:AjaxUpdatedControl ControlID="pnlBrowse" LoadingPanelID="RadAjaxLoadingPanel2" />
                <rad:AjaxUpdatedControl ControlID="bfrgTransactionSummary" LoadingPanelID="noLoadingPanel" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="rcbCourse">
            <UpdatedControls>
                <%--  <rad:AjaxUpdatedControl ControlID="rcbCourse" LoadingPanelID="noLoadingPanel" />
                <rad:AjaxUpdatedControl ControlID="rgAllResources" LoadingPanelID="RadAjaxLoadingPanel2" />--%>
                <rad:AjaxUpdatedControl ControlID="pnlBrowse" LoadingPanelID="RadAjaxLoadingPanel2" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnContinue">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlCreateAssessment" LoadingPanelID="fullPageLoader" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="ddlItemtypes">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="ddlItemtypes" LoadingPanelID="noLoadingPanel" />
                <rad:AjaxUpdatedControl ControlID="pnlQuestionTypeContainer" LoadingPanelID="fullPageLoader" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="lnkEditExam">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="lnkEditExam" LoadingPanelID="fullPageLoader" />
                <rad:AjaxUpdatedControl ControlID="pnlCreateAssessment" LoadingPanelID="fullPageLoader" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnAddItem">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlCreateAssessment" LoadingPanelID="fullPageLoader" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnContinueAddQuestion">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlCreateAssessment" LoadingPanelID="fullPageLoader" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="lbtnEditExam">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlCreateAssessment" LoadingPanelID="fullPageLoader" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="lnkEdit">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlCreateAssessment" LoadingPanelID="fullPageLoader" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="lbtnFakeDelete">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlCreateAssessment" LoadingPanelID="fullPageLoader" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="lbtnAddNewExam">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlCreateAssessment" LoadingPanelID="fullPageLoader" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnReturnToExamList">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlCreateAssessment" LoadingPanelID="fullPageLoader" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnCancelAddQuestion">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlCreateAssessment" LoadingPanelID="fullPageLoader" />
                <rad:AjaxUpdatedControl ControlID="pnlQuestionBuilderContainer" LoadingPanelID="fullPageLoader" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnConfirmDeleteQuestionItem">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlCreateAssessment" LoadingPanelID="fullPageLoader" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnConfirmEditQuestionItem">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlCreateAssessment"  LoadingPanelID="fullPageLoader" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="rcbResourceTypeAssessment">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlCreateAssessment" LoadingPanelID="fullPageLoader" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="rcbCreateType">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlCreateAssessment" LoadingPanelID="fullPageLoader" />
                <rad:AjaxUpdatedControl ControlID="txtPassingGrade" LoadingPanelID="fullPageLoader" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <%--  <rad:AjaxSetting AjaxControlID="gvGridItems">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlCreateAssessment" LoadingPanelID="fullPageLoader" />
            </UpdatedControls>
        </rad:AjaxSetting>--%>
    </AjaxSettings>
</rad:RadAjaxManagerProxy>
<rad:RadScriptBlock ID="ctRadScriptBlock1" runat="server">
    <script type="text/javascript">
        function loadMainPage() {
            alert("Load Main Content");
            clientRefreshResources();
        }

        function clearRcbSection() {
            $find("<%= rcbCourseSections.ClientID %>").clearItems();
            $find("<%= rcbCourseSections.ClientID %>").clearSelection();
            clientRefreshResources();
        }

        function openSideContent(evt, actionName, obj) {
            var btnName = $(obj).parent().attr('id');

            $('.tabcontent').css({ 'display': 'none' });
            $('.tablinks').removeClass('active');
            $('.tabcontent').removeClass('active');
            $('.tab-action').removeClass('active');
            $('#' + btnName).addClass('active');
            $('#' + actionName).css({ 'display': 'block' }).addClass('active');

            if (actionName == "ctUpload")
                $find("<%= rcbSCOType.ClientID %>").clearSelection();
            if (actionName == "ctBrowse") {
                openResourceBrowser();
            }

        }

        function createAction(action, obj) {
            var obj_id = $(obj).attr('id');

            $('.tab-action').removeClass('active');
            $('#' + obj_id).addClass('active');

            if (action == '1') {//Section
                $("#<%= txtSectionName.ClientID %>").val("");
                $find("<%= cbAddSectionResourceType.ClientID %>").clearSelection();
                $find("<%= CreateSectionWindow.ClientID %>").show();
            }
            else if (action == '2') {//ILT
                $find("<%= TrainingTypeWindow.ClientID %>").show();
            }
            else if (action == '3') {//ASSESSMENT
                $find("<%= CreateAssessmentWindow.ClientID %>").show();
            }
        }
        var ISFILEVALID = true;

        function isRcbSCOTypeNotEmpty() {
            debugger;
            var v = $find("<%= rcbSCOType.ClientID %>").get_value();
            if (v == "") {
//                var input = $find("<%= rcbSCOType.ClientID %>").get_inputDomElement();
                //                input.focus();
                
                return false;
            }
            else
                return true;
        }
        function OpenFileChooser() {

            if (isRcbSCOTypeNotEmpty()) {
                
                View_UploadWindow1();
            } else {
                radalert("Please select the Resource Type from the dropdown.", 330, 180, "Invalid Resource Type", "");
            }
                
        }

        function setCourseTrainingType(trainingID, trainingName) {
            var courseID = '<%= Request.QueryString["CourseID"] %>';
            //            console.log(courseID);
            //            console.log(trainingID);
            //            console.log(trainingName);
            showPageLoading();
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "UpdateCourseTrainingType",
                data: "{courseID :'" + courseID + "', trainingTypeID : " + trainingID + "}",
                dataType: 'json',
                success: function (res) {
                    $(".Loading").hide();
                    if (res.d.isSuccess)
                        radalert("Course Type was Successfully updated to " + trainingName, 330, 180, "Success", "");
                    else
                        radalert(res.d.message, 330, 180, "Error", "");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var code = XMLHttpRequest.status;
                    if (code == "401")
                        window.location.reload();
                    else
                        alert(XMLHttpRequest.responseText);
                }
            });
            $find("<%= TrainingTypeWindow.ClientID %>").close();
        }
        function showPageLoading() {
            var panel = $("#<%= fullPageLoader.ClientID %>");
            panel.show().css({ "opacity": .8 });
        }

        function hidePageLoading() {
            $(".Loading").hide();
        }

        function validationFailed(upload, args) {
            //debugger;
            $(".Loading").hide();
            enableUploadButton(false);
            ISFILEVALID = false;
            var v = $find("<%= rcbSCOType.ClientID %>").get_value();
            if (v == "") {

                //radalert("Please select the Resource Type from the dropdown.", 330, 180, "Invalid Resource Type", "");

                upload.set_enabled(true);
                var input = $find("<%= rcbSCOType.ClientID %>").get_inputDomElement();
                input.focus();
            } else {
            
                radalert("Selected File is not supported please select another one.", 330, 180, "Invalid Filetype", "");
                upload.set_enabled(true);
                console.log("File Validation Failed");
            }
            deleteCourseUploaderFiles();
            $telerik.$(".ruRemove", args.get_row()).click();
            $(".course-dropzone .dropfiles-text").show();
            $(".course-dropzone .droppedfiles-text").html("").addClass("display-none");
        }

        function closeCreateSectionWindow(refreshResource) {
            if (refreshResource == "1") {
                debugger;
                clientRefreshResources();
                $find("<%= CreateSectionWindow.ClientID %>").close();
            } else
                $find("<%= CreateSectionWindow.ClientID %>").close();
        }

        function closeBrowseFilesWindow() {
            $find("<%= BrowseResourceWindow.ClientID %>").close();
        }

        function ConfirmAddSection() {
            var sectionName = $("#<%= txtSectionName.ClientID %>").val();
            var type = $find("<%= cbAddSectionResourceType.ClientID %>").get_value();

            if (type == "") {
                radalert("Please Select Resource Type! ", 330, 180, "Invalid Resource Type", "");
                $find("<%= cbAddSectionResourceType.ClientID %>").get_inputDomElement().focus();
            } else if (sectionName == "") {
                radalert("Section Name is empty", 330, 180, "Invalid Resource Name", "");
                $("#<%= txtSectionName.ClientID %>").focus();
            } else {
                radconfirm("Are you sure you want to add section \"" + sectionName + "\"", confirmAddSectionFn, 330, 180, "", "Confirm Add");
                var v = $find("<%= rcbSCOType.ClientID %>").get_value();

            }

        }

        function confirmAddSectionFn(arg) {
            if (arg) {
                $("#<%= btnAddSection.ClientID %>").click();
            } else {
                return false;
            }
        }


        function openResourceBrowser() {
            clearBrowseFiles();
            $find("<%= BrowseResourceWindow.ClientID %>").show();
        }
        function closeResourceBrowser() {
            $find("<%= BrowseResourceWindow.ClientID %>").close();
        }
        //Browse File Selection
        var selectedItems = [];

        function gtCourseRowSelected1(sender, args) {
            var id = args.getDataKeyValue("fileID");

            if (!selectedItems.includes(id)) {
                selectedItems.push(id);
            }
            setSelectedItems1();

        }

        function gtCourseRowDeselected1(sender, args) {
            var id = args.getDataKeyValue("fileID");

            if (selectedItems.includes(id)) {
                selectedItems.splice(selectedItems.indexOf(id), 1);
            }
            setSelectedItems1();
        }

        function gtCourseRowCreated1(sender, args) {
            var id = args.getDataKeyValue("fileID");
            if (selectedItems.includes(id) && selectedItems.length > 0) {
                args.get_gridDataItem().set_selected(true);
            }
        }

        function setSelectedItems1() {
            var elements = selectedItems;
            var outputStr = "";
            for (var i = 0; i < elements.length; i++) {
                outputStr += elements[i] + ",";

            }
            console.log(outputStr);
            $("#" + "<%= hfSelectedFiles.ClientID %>").val(outputStr);
        }

        function clearSelectedItems1() {
            selectedItems = [];
            $("#" + "<%= hfSelectedFiles.ClientID %>").val("");
            var grid = $find("<%= rgAllResources.ClientID %>");
            grid.clearSelectedItems();
        }

        function showTransaction() {

            var radwindow = $find('<%=bfrdTransactionSummary.ClientID %>');
            radwindow.show();
        }

        function closeTransaction() {

            closeResourceBrowser();
            var radwindow = $find('<%= bfrdTransactionSummary.ClientID %>');
            radwindow.close();
            clientRefreshResources();


        }

        function disableSelectCourse() {
            $("#<%= rcbCourse.ClientID %>").prop('disabled', true);
        }

        function clearBrowseFiles() {
            $find("<%= rcbSectionBrowse.ClientID %>").clearItems();
            $find("<%= rcbSectionBrowse.ClientID %>").clearSelection();

            $find("<%= rcbResourceTypeBrowse.ClientID %>").clearSelection();

            clearSelectedItems1();
        }

        function closeLocalLoading() {

            $(".Loading2").hide();
        }

    </script>
</rad:RadScriptBlock>
<rad:RadAjaxLoadingPanel ID="fullPageLoader" runat="server" Transparency="25" IsSticky="true"
    CssClass="Loading" />
<div id="pnlContent" class="contentSidebarContainer courseSidebarContainer content-tab bg-color-dark-gray"
    style="text-align: center; font-size: inherit;">
    <div class="row" style="height: inherit;">
        <div class="col-md-3 tabButtonGroup">
            <div class="tablinks" id="btnCourseUpload">
                <button id="btnUpload" runat="server" title="Upload" onclick="openSideContent(event, 'ctUpload', this); return false">
                    <img src="Media/Images/upload-icon.png" alt="Upload" />
                    Upload
                </button>
            </div>
            <div class="tablinks" id="btnCourseCreate">
                <button id="btnCreate" runat="server" title="Create" onclick="openSideContent(event, 'ctCreate', this); return false">
                    <img src="Media/Images/create-icon.png" alt="Create" />
                    Create
                </button>
            </div>
            <div class="tablinks" id="btnCourseSearch">
                <button id="btnBrowse" runat="server" title="Browse" onclick="openSideContent(event, 'ctBrowse', this); return false">
                    <img src="Media/Images/search-icon.png" alt="Search" />
                    Browse
                </button>
            </div>
        </div>
        <div class="col-md-9" style="padding: 0 !important; width: calc(75% - 15px);">
            <div class="contentDetails" id="SideContentContainer" runat="server">
                <div id="ctIntro" class="tabcontent active" style="display: block;">
                    <p style="padding-top: 5em; color: #fff;">
                        Select a Course Builder icon</p>
                </div>
                <div id="ctUpload" class="tabcontent">
                    <rad:RadComboBox ID="rcbSCOType" runat="server" DefaultValue="0" AutoPostBack="true"
                        RenderMode="Lightweight" MaxHeight="115px" EmptyMessage="- Select Resource Type -"
                        ToolTip="Select Resource Type" CssClass="width-fill" OnSelectedIndexChanged="rcbSCOType_SelectedIndexChanged">
                    </rad:RadComboBox>
                    <%-- <asp:SqlDataSource ID="dsScoType" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
                        SelectCommand="select scotypeid, scotype, maxallowed from tbl_scorm_lkp_scotype where inactive = 0 order by ordinal">
                    </asp:SqlDataSource>--%>
                    <rad:RadComboBox ID="rcbCourseSections" runat="server" DefaultValue="0" RenderMode="Lightweight"
                        DataValueField="SectionID" DataTextField="SectionName" MaxHeight="150px" EmptyMessage="- Select Section Name-"
                        ToolTip="Select Section" CssClass="width-fill margin-top-10px ">
                    </rad:RadComboBox>
                    <p>
                        <br />
                    </p>
                    <div class="course-dropzone">
                        <div class="dropfiles-text">
                            <img src="Media/Images/upload-icon.png" width="45px" alt="Upload" class="invert" />
                            <br />
                            Drop files here
                        </div>
                        <div class="droppedfiles-text" class="display-none">
                        </div>
                        <rad:RadProgressArea Font-Size="X-Small" RenderMode="Lightweight" runat="server" CssClass="progress-area display-none"
                            ID="RadProgressArea1" ProgressIndicators="TransferSpeed,TotalProgressPercent,TotalProgressBar,TimeEstimated,TimeElapsed,CurrentFileName" />
                    </div>
                    <p>
                        <br />
                        or
                        <br />
                        <br />
                    </p>
                    <asp:Label ID="AddButtonSenderID" runat="server" Text="0" Visible="false"></asp:Label>
                    <%--OnClick="btnChooseAFile_Click"--%>
                    <asp:LinkButton ID="btnChooseAFile" runat="server" CssClass="tab-action" ToolTip="Choose a file"
                        OnClientClick="OpenFileChooser(); return false;">
                        <div class="course-choosefile">
                            <i class="fa fa-files-o"></i>
                            <p>
                                Choose a file
                            </p>
                        </div>
                    </asp:LinkButton>
                </div>
                <div id="ctCreate" class="tabcontent">
                    <div class="row" style="margin-bottom: 15px; margin-left: 10px; margin-right: 10px;">
                        <div class="col-md-6" style="text-align: center">
                            <asp:LinkButton ID="btnCreateSection" runat="server" CssClass="tab-action" OnClientClick="createAction(1, this); return false"
                                ToolTip="Section">
                                <img src="Media/Images/create-section-icon.png" alt="Create Section" />
                                <br />
                                Section
                            </asp:LinkButton>
                        </div>
                        <div class="col-md-6" style="text-align: center">
                            <asp:LinkButton ID="btnCreateILT" runat="server" CssClass="tab-action" OnClientClick="createAction(2, this); return false"
                                ToolTip="ILT">
                                <img src="Media/Images/create-ilt-icon.png" alt="Create Section" />
                                <br />
                                ILT
                            </asp:LinkButton>
                        </div>
                    </div>
                    <div class="row" style="margin-bottom: 15px; margin-left: 10px; margin-right: 10px;">
                        <div class="col-md-6" style="text-align: center">
                            <asp:LinkButton ID="btnCreateAssessment" runat="server" CssClass="tab-action" OnClientClick="createAction(3, this); return false"
                                ToolTip="Assessment/Survey">
                                <img src="Media/Images/create-assessment-icon.png" alt="Create Section" />
                                Assessment/ Survey
                            </asp:LinkButton>
                        </div>
                        <div class="col-md-6" style="text-align: center">
                            <asp:LinkButton ID="btnCreateEmbedVideo" runat="server" CssClass="tab-action" OnClientClick="createAction(5, this); return false"
                                ToolTip="Embed Video">
                                <img src="Media/Images/create-embedvideo-icon.png" alt="Create Section" />
                                Embed Video
                            </asp:LinkButton>
                        </div>
                    </div>
                    <div class="row" style="margin-bottom: 15px; margin-left: 10px; margin-right: 10px;">
                        <div class="col-md-6" style="text-align: center">
                            <asp:LinkButton ID="btnCreateSurvey" runat="server" CssClass="tab-action display-none"
                                OnClientClick="createAction(4, this); return false" ToolTip="Survey">
                                <img src="Media/Images/create-survey-icon.png" alt="Create Section" />
                                Survey
                            </asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div id="ctBrowse" class="tabcontent">
                    <h3>
                        Browse</h3>
                    <p>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<rad:RadGrid runat="server" ID="gridPackage" AutoGenerateColumns="False" CssClass="RadGridPackage"
    Skin="Bootstrap" AllowPaging="False" AllowSorting="False" AllowFilteringByColumn="False"
    Visible="false">
    <MasterTableView DataKeyNames="ScoId,ScoPackageId,ScoTypeId,ScoType,ScoTitle,CourseTypeID,curriculumcourseID"
        Name="MasterTable" AutoGenerateColumns="False" GridLines="None" ShowHeader="False"
        TableLayout="Auto">
        <Columns>
            <rad:GridButtonColumn ConfirmDialogType="RadWindow" ConfirmTitle="Delete" ButtonType="ImageButton"
                CommandName="Delete" Text="Delete" UniqueName="DeleteColumn" ConfirmText="Are you sure you want to cancel this SCO?"
                ImageUrl="~/Media/Images/deletesmall.png" ConfirmDialogHeight="180" ConfirmDialogWidth="350">
                <HeaderStyle Width="50px" />
            </rad:GridButtonColumn>
            <rad:GridBoundColumn UniqueName="ScoTypeId" DataField="ScoTypeId" HeaderText="ScoTypeId"
                Display="False" />
            <rad:GridBoundColumn UniqueName="ScoType" DataField="ScoType" HeaderText="ScoType"
                Display="False" />
            <rad:GridBoundColumn UniqueName="ScoTitle" DataField="ScoTitle" HeaderText="ScoTitle" />
        </Columns>
        <GroupByExpressions>
            <rad:GridGroupByExpression>
                <SelectFields>
                    <rad:GridGroupByField FieldAlias="&nbsp;" FieldName="ScoType" HeaderValueSeparator="&nbsp;" />
                </SelectFields>
                <GroupByFields>
                    <rad:GridGroupByField FieldName="ScoTypeId" SortOrder="Ascending" />
                </GroupByFields>
            </rad:GridGroupByExpression>
        </GroupByExpressions>
    </MasterTableView>
    <ClientSettings EnableRowHoverStyle="True">
        <Selecting AllowRowSelect="True" />
    </ClientSettings>
</rad:RadGrid>
<rad:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true" RenderMode="Lightweight"
    Skin="Bootstrap" VisibleStatusbar="false">
    <Windows>
        <rad:RadWindow ID="TrainingTypeWindow" runat="server" Title="Select Training Type"
            Behaviors="Close, Move" DestroyOnClose="True" ShowContentDuringLoad="True" VisibleStatusbar="False"
            Modal="True" ReloadOnShow="True" Width="300px" Height="250px" CssClass="tc-radwindow-simple">
            <ContentTemplate>
                <rad:RadListView ID="lvIltType" runat="server">
                    <ItemTemplate>
                        <div class="col-md-12 no-padding" style="margin: 3% 0;">
                            <asp:Button ID="Button1" Text='<%#Eval("CourseTrainingType")%>' CssClass="btn btn-md btn-flat btn-teal width-fill"
                                runat="server" OnClientClick='<%# "setCourseTrainingType(" + Eval("ID") + ",\"" + Eval("CourseTrainingType") + "\"); return false;"%>' />
                        </div>
                    </ItemTemplate>
                </rad:RadListView>
            </ContentTemplate>
        </rad:RadWindow>
        <rad:RadWindow ID="CreateSectionWindow" runat="server" Title="Create Section" Behaviors="Close, Move"
            DestroyOnClose="True" ShowContentDuringLoad="True" VisibleStatusbar="False" Modal="True"
            ReloadOnShow="True" Width="500px" Height="250px" CssClass="tc-radwindow-simple">
            <ContentTemplate>
                <div class="col-md-12 no-padding" style="margin: 3% 0;">
                    Resource Type:
                    <rad:RadComboBox ID="cbAddSectionResourceType" runat="server" DefaultValue="0" RenderMode="Lightweight"
                        DataSourceID="dsResourceType" DataValueField="scotypeid" DataTextField="scotype"
                        Height="150px" EmptyMessage="- Select -" ToolTip="Select Sco Type" CssClass="width-fill">
                    </rad:RadComboBox>
                    <asp:SqlDataSource ID="dsResourceType" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
                        SelectCommand="select * from vw_transcomuniversity_ResourceType order by ScoTypeID ASC">
                    </asp:SqlDataSource>
                </div>
                <asp:TextBox runat="server" ID="txtSectionName" placeholder="Type Section Name ..."
                    CssClass="form-control input-violet width-fill" />
                <div class="text-center margin-top-10px margin-bottom-10px ">
                    <asp:Button ID="btnAddSection" runat="server" CssClass="display-none" OnClick="btnAddSectionClicked" />
                    <asp:Button ID="fakeBtnAddSection" Text="Add Section" runat="server" CssClass="btn btn-md btn-teal btn-flat"
                        OnClientClick="ConfirmAddSection(); return false;" />
                    <asp:Button ID="btnCancelAddSection" Text="Cancel" runat="server" CssClass="btn btn-md btn-teal btn-flat"
                        OnClientClick="closeCreateSectionWindow(); return false;" />
                </div>
            </ContentTemplate>
        </rad:RadWindow>
        <rad:RadWindow ID="BrowseResourceWindow" runat="server" Title="Browse Resources"
            Behaviors="Close" DestroyOnClose="True" ShowContentDuringLoad="True" VisibleStatusbar="False"
            Modal="True" ReloadOnShow="True" Width="850px" Height="550px" CssClass="tc-radwindow-simple tc-radwindow nomove"
            OnClientClose="closeLocalLoading">
            <ContentTemplate>
                <asp:Panel ID="pnlBrowse" runat="server" CssClass="container no-padding">
                    <table width="100%">
                        <tbody>
                            <tr>
                                <td>
                                    <div class="col-lg-4">
                                        Resource Type:
                                        <rad:RadComboBox ID="rcbResourceTypeBrowse" runat="server" DefaultValue="0" RenderMode="Lightweight"
                                            DataSourceID="dsResourceTypeBrowse" DataValueField="scotypeid" DataTextField="scotype"
                                            AutoPostBack="true" MaxHeight="150px" EmptyMessage="- Select Resource Type -"
                                            OnClientSelectedIndexChanged="disableSelectCourse" ToolTip="Select Sco Type"
                                            CssClass="width-fill" OnSelectedIndexChanged="rcbResourceTypeBrowse_IndexChanged">
                                        </rad:RadComboBox>
                                        <asp:SqlDataSource ID="dsResourceTypeBrowse" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
                                            SelectCommand="select * from vw_transcomuniversity_ResourceType order by ScoTypeID ASC">
                                        </asp:SqlDataSource>
                                    </div>
                                    <div class="col-lg-4">
                                        Section:
                                        <rad:RadComboBox ID="rcbSectionBrowse" runat="server" DefaultValue="0" RenderMode="Lightweight"
                                            DataValueField="SectionID" DataTextField="SectionName" MaxHeight="150px" EmptyMessage="- Select Section -"
                                            ToolTip="Select Section" CssClass="width-fill">
                                        </rad:RadComboBox>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="col-lg-4">
                                        <rad:RadComboBox RenderMode="Lightweight" ID="rcbCourse" runat="server" DataTextField="Title"
                                            DataValueField="CourseID" EmptyMessage="-Select Course-" DataSourceID="dsCourses"
                                            EnableAutomaticLoadOnDemand="True" ItemsPerRequest="10" EnableVirtualScrolling="true"
                                            ToolTip="Select Course" CssClass="width-fill" AllowCustomText="false" EnableItemCaching="true"
                                            Height="250px" OnSelectedIndexChanged="rcbCourse_SelectedIndexChanged" AutoPostBack="true">
                                            <DefaultItem Text="-Select Course-" Value="0" />
                                        </rad:RadComboBox>
                                        <asp:SqlDataSource ID="dsCourses" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection%>"
                                            SelectCommand="select CourseID, Title from tbl_transcomuniversity_cor_course where HideFromList=0">
                                        </asp:SqlDataSource>
                                        <br />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <rad:RadGrid RenderMode="Lightweight" ID="rgAllResources" runat="server" AllowSorting="True"
                                        Width="800px" CssClass="GridLess nowrap-header" AllowMultiRowSelection="True"
                                        AllowPaging="true" OnNeedDataSource="rgAllResources_OnNeedDataSource">
                                        <MasterTableView AutoGenerateColumns="false" TableLayout="Auto" HeaderStyle-ForeColor="Teal"
                                            DataKeyNames="fileID" ClientDataKeyNames="fileID">
                                            <Columns>
                                                <rad:GridBoundColumn DataField="RowNumber" HeaderText="#" UniqueName="RowNumber">
                                                </rad:GridBoundColumn>
                                                <rad:GridClientSelectColumn UniqueName="cboxSco">
                                                </rad:GridClientSelectColumn>
                                                <rad:GridBoundColumn DataField="FileName" HeaderText="FILENAME" UniqueName="FileName">
                                                </rad:GridBoundColumn>
                                                <rad:GridBoundColumn DataField="FileType" HeaderText="FILETYPE" UniqueName="FileType">
                                                </rad:GridBoundColumn>
                                                <rad:GridBoundColumn DataField="UploadDate" HeaderText="DATE UPLOADED" UniqueName="UploadDate">
                                                </rad:GridBoundColumn>
                                                <rad:GridBoundColumn DataField="UploadedBy" HeaderText="UPLOADED BY" UniqueName="UploadedBy">
                                                </rad:GridBoundColumn>
                                            </Columns>
                                        </MasterTableView>
                                        <ClientSettings>
                                            <Selecting AllowRowSelect="true" />
                                            <Resizing AllowResizeToFit="true" ResizeGridOnColumnResize="false" />
                                            <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="true">
                                            </Scrolling>
                                            <ClientEvents OnRowSelected="gtCourseRowSelected1" OnRowDeselected="gtCourseRowDeselected1"
                                                OnRowCreated="gtCourseRowCreated1" />
                                        </ClientSettings>
                                    </rad:RadGrid>
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td style="padding-top: 10px;">
                                    <div class="relative-hr-center display-inline-flex">
                                        <asp:Button ID="btnAddFiles" Text="Add File" runat="server" CssClass="btn btn-md btn-teal btn-flat margin-side-10"
                                            OnClick="AddSelectedFiles" />
                                        <asp:Button ID="Button4" Text="Cancel" runat="server" CssClass="btn btn-md btn-teal btn-flat margin-side-10"
                                            OnClientClick="closeBrowseFilesWindow(); return false;" />
                                    </div>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </asp:Panel>
            </ContentTemplate>
        </rad:RadWindow>
        <rad:RadWindow RenderMode="Lightweight" ID="bfrdTransactionSummary" Behaviors="Close"
            VisibleOnPageLoad="false" runat="server" CssClass="tc-radwindow-1 window-summary "
            Width="800px" Title="Result Summary" Modal="true" OnClientClose="closeTransaction">
            <ContentTemplate>
                <asp:Panel ID="pnlTransactionList" runat="server" Height="400px" ScrollBars="Vertical">
                    <rad:RadGrid RenderMode="Lightweight" ID="bfrgTransactionSummary" runat="server"
                        AllowSorting="True" CssClass="GridLess" AllowMultiRowSelection="true" AllowPaging="true"
                        OnItemDataBound="bfrgTransactionSummary_OnItemDataBound">
                        <MasterTableView AutoGenerateColumns="false" TableLayout="Auto" HeaderStyle-ForeColor="Teal"
                            DataKeyNames="Id" ClientDataKeyNames="Id">
                            <Columns>
                                <rad:GridTemplateColumn HeaderText="#">
                                    <ItemTemplate>
                                        <%#Container.ItemIndex+1 %></ItemTemplate>
                                </rad:GridTemplateColumn>
                                <rad:GridBoundColumn DataField="Name" HeaderText="FILE NAME" UniqueName="GroupName">
                                </rad:GridBoundColumn>
                                <rad:GridBoundColumn DataField="Status" HeaderText="STATUS" UniqueName="Status">
                                </rad:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                    </rad:RadGrid>
                </asp:Panel>
                <asp:Button ID="bfbtnTransactionsList" Text="Close" runat="server" CssClass="btn tc-btn-md btn-teal btn-flat bottom-center"
                    OnClientClick="closeTransaction(); return false;" />
                <%-- <asp:LinkButton ID="gtbtnTransactionsList" OnClientClick="closeTransaction(); return false;"
                    runat="server" Text="Close" ToolTip="Close" CssClass="btn tc-btn-md btn-teal btn-flat bottom-center"></asp:LinkButton>--%>
            </ContentTemplate>
        </rad:RadWindow>
        <rad:RadWindow ID="CreateAssessmentWindow" runat="server" Title="Create Assessment/Survey"
            Behaviors="Close" DestroyOnClose="True" ShowContentDuringLoad="false" VisibleStatusbar="False"
            Modal="True" ReloadOnShow="True" Width="950px" Height="550px" CssClass="tc-radwindow-simple tc-radwindow nomove">
            <%--OnUnload="ReloadCreateAssessment"--%>
            <ContentTemplate>
                <asp:Panel ID="pnlCreateAssessment" runat="server" CssClass="container no-padding">
                    <asp:Panel runat="server" ID="pnlAssessmentBuilderContainer" CssClass="container no-padding">
                        <ucex:ExamUC ID="assessmentBuilder" runat="server" Visible="false" />
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlQuestionBuilderContainer" CssClass="container no-padding">
                        <uceq:ExamQuestionUC ID="questionBuilder" runat="server" Visible="false" />
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlStatusContainer" CssClass="container no-padding">
                        <uces:ExamStatUC ID="testStatus" runat="server" Visible="false" />
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlExamsContainer" CssClass="container no-padding">
                        <ucexams:ExamsUC ID="examsList" runat="server" Visible="true" />
                    </asp:Panel>
                    <%-- <ul class="nav nav-tabs">
                              <li class="active"><a data-toggle="tab" href="#assessment">Assement</a></li>
                              <li><a data-toggle="tab" href="#questions">Questions</a></li>
                            </ul>

                            <div class="tab-content">
                              <div id="assessment" class="tab-pane fade in active">
                                
                              </div>
                              <div id="questions" class="tab-pane fade">
                               
                              </div>
                            </div>
                    --%>
                </asp:Panel>
            </ContentTemplate>
        </rad:RadWindow>
    </Windows>
</rad:RadWindowManager>
<asp:HiddenField ID="hfSelectedFiles" runat="server" />
