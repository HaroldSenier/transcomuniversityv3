﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web.UI;
using System.Web;
using System.Web.UI.WebControls;
using Microsoft.LearningComponents;
using Microsoft.LearningComponents.Storage;
using DotNetSCORM.LearningAPI;
using System.Data.SqlClient;
using Telerik.Web.UI;
using Schema = DotNetSCORM.LearningAPI.Schema;
using System.IO;
using System.Web.Hosting;
using System.Security.Cryptography;
using System.Text;
using TranscomUniversityV3Model;
using System.Linq;
using System.Reflection;
using TransactionNames;
using System.Drawing;

public partial class CourseContentRightPanelCtrl : DotNetSCORMControlBase
{
    private static SqlCommand _ocmd;

    private static SqlConnection _oconn;

    private static void Dbconn(string connStr)
    {
        _oconn = new SqlConnection
        {
            ConnectionString = ConfigurationManager.ConnectionStrings[connStr].ConnectionString
        };
        _oconn.Open();
    }

    public DataTable DtPackage1
    {
        get
        {
            return ViewState["DtPackage1"] as DataTable;
        }
        set
        {
            ViewState["DtPackage1"] = value;
        }
    }

    //public void CreatePackageDataTableColumns()
    //{
    //    DtPackage1 = new DataTable();
    //    var column = new DataColumn
    //    {
    //        DataType = Type.GetType("System.Int32"),
    //        ColumnName = "ScoId",
    //        AutoIncrement = true,
    //        AutoIncrementSeed = 1,
    //        AutoIncrementStep = 1
    //    };
    //    DtPackage1.Columns.Add(column);
    //    DtPackage1.Columns.Add("ScoPackageId", typeof(int));
    //    DtPackage1.Columns.Add("ScoTitle", typeof(String));
    //    DtPackage1.Columns.Add("ScoTypeId", typeof(int));
    //    DtPackage1.Columns.Add("ScoType", typeof(String));
    //    DtPackage1.Columns.Add("CourseTypeID", typeof(int));
    //    DtPackage1.Columns.Add("curriculumcourseID", typeof(int));
    //}

    protected string Decrypt(string cipherText)
    {
        string EncryptionKey = "TRNSCMV32017111";
        cipherText = cipherText.Replace(" ", "+");
        byte[] cipherBytes = Convert.FromBase64String(cipherText);

        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(cipherBytes, 0, cipherBytes.Length);
                    cs.Close();
                }
                cipherText = Encoding.Unicode.GetString(ms.ToArray());
            }
        }
        return cipherText;
    }

    public void GetDBPackages(int CurriculumID, int CurriculumCourseID)
    {
        var db = new TranscomUniversityV3ModelContainer();
        var dt1 = db.pr_Scorm_Cor_GetCurriculumPackages(CurriculumID, CurriculumCourseID, 0)
                .AsEnumerable()
                .Select(p => new
                {
                    ScoId = p.scoid,
                    ScoPackageId = p.ScoPackageId,
                    ScoTitle = p.ScoTitle,
                    ScoTypeId = p.ScoTypeId,
                    ScoType = p.ScoType,
                    CourseTypeID = p.CourseTypeID,
                    curriculumcourseID = p.curriculumcourseID,
                    CourseOrder = p.CourseOrder,
                    p.SectionID,
                    SectionName = p.SectionName == null ? "NotSection" : p.SectionName
                })
                .ToList();

        gridPackage.DataSource = dt1;
        gridPackage.DataBind();

        UserControl content = (UserControl)(this.Parent.Parent).FindControl("Content");
        RadListView lvCourseReorderTrue = (RadListView)content.FindControl("lvCourseReorderTrue");

        RadListView lvCourseReorderFalse = (RadListView)content.FindControl("lvCourseReorderFalse");
        Button btnReorderSequence = (Button)content.FindControl("btnReorderSequence");

        if (dt1.Count == 0)
            btnReorderSequence.Visible = false;
        else if (dt1.Count > 0)
            btnReorderSequence.Visible = true;

        lvCourseReorderTrue.DataSource = dt1;
        lvCourseReorderFalse.DataSource = dt1;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (!Page.IsPostBack)
        {
            int courseID = Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"]));
            int testCount = DataHelper.GetTestCount(courseID);

            loadTrainingTypeWindow();
            loadUploadResourceType(DataHelper.GetCourseType(courseID));

            string courseName = DataHelper.getCourseName(courseID);
            CreateSectionWindow.Title = courseName + " - " + "Create Section";
            BrowseResourceWindow.Title = courseName + " - " + "Browse Files";


            //SessionManager.SessionTestCategoryID = testCategoryID;
            SessionManager.SessionUsername = DataHelper.GetCurrentUserCIM();

            if (testCount > 0)
            {
                assessmentBuilder.Visible = false;
                questionBuilder.Visible = false;
                testStatus.Visible = false;
                examsList.Visible = true;
            }
            else
            {
                assessmentBuilder.Visible = true;
                questionBuilder.Visible = false;
                testStatus.Visible = false;
                examsList.Visible = false;
            }
           
        }
        assessmentBuilder.EventReloadQuestionDetails += new UserControl_Pages_Assessment_editexamcontrol.customHandler(questionBuilderUC_questionBuilderUCReloadQuestion);
        assessmentBuilder.EventAddQuestion += new UserControl_Pages_Assessment_editexamcontrol.customHandler(questionBuilderUC_questionBuilderUCAddQuestion);
        
        
       
    }

    protected void btnChooseAFile_Click(object sender, EventArgs e)
    {
        if (rcbSCOType.SelectedValue != "")
        {
            RadScriptManager.RegisterStartupScript(Page, Page.GetType(), "View_UploadWindow1", Utils.callClientScript("View_UploadWindow1"), true);
        }
        else
            rcbSCOType.Focus();
    }

    //protected void RadAsyncUpload1_FileUploaded(object sender, FileUploadedEventArgs e)
    //{
    //    if (e.IsValid)
    //    {
    //        try
    //        {
    //            CreatePackageDataTableColumns();
    //            foreach (GridDataItem row in gridPackage.Items)
    //            {
    //                var ScoTypeId = (Int32)row.GetDataKeyValue("ScoTypeId");
    //                var ScoPackageId = (Int32)row.GetDataKeyValue("ScoPackageId");
    //                var ScoType = (string)row.GetDataKeyValue("ScoType");
    //                var ScoTitle = (string)row.GetDataKeyValue("ScoTitle");
    //                var CourseTypeID = (Int32)row.GetDataKeyValue("CourseTypeID");
    //                var curriculumcourseID = (Int32)row.GetDataKeyValue("curriculumcourseID");

    //                DataRow dr = DtPackage1.NewRow();
    //                dr["ScoPackageId"] = ScoPackageId;
    //                dr["ScoTitle"] = ScoTitle;
    //                dr["ScoType"] = ScoType;
    //                dr["ScoTypeId"] = ScoTypeId;
    //                dr["CourseTypeID"] = CourseTypeID;
    //                dr["curriculumcourseID"] = curriculumcourseID;
    //                DtPackage1.Rows.Add(dr);
    //            }

    //            if (rcbSCOType.Text.In("Trainer Resources", "Additional Resources"))
    //            {
    //                string completePath;
    //                string uniqName = null;

    //                foreach (UploadedFile file in RadAsyncUpload1.UploadedFiles)
    //                {
    //                    string FolderPath = ConfigurationManager.AppSettings["OtherResourcesDirectoryPath"];

    //                    uniqName = DateTime.Now.ToString("yyyyMMddHHmmss") + file.GetName();
    //                    completePath = Server.MapPath(FolderPath + "\\" + uniqName);

    //                    file.SaveAs(completePath);
    //                }

    //                DataRow drPackage = DtPackage1.NewRow();
    //                drPackage["ScoPackageId"] = 0;
    //                drPackage["ScoTitle"] = uniqName;
    //                drPackage["ScoType"] = rcbSCOType.SelectedItem.Text;
    //                drPackage["ScoTypeId"] = rcbSCOType.SelectedValue;
    //                drPackage["CourseTypeID"] = 1;
    //                drPackage["curriculumcourseID"] = "1";

    //                DtPackage1.Rows.Add(drPackage);
    //                gridPackage.DataSource = DtPackage1;
    //                gridPackage.DataBind();
    //            }
    //            else
    //            {
    //                DotNetSCORM.LearningAPI.LStoreUserInfo currentUser = GetCurrentUserInfo();
    //                PackageItemIdentifier packageId;

    //                using (PackageReader packageReader = PackageReader.Create(RadAsyncUpload1.UploadedFiles[0].InputStream))
    //                {
    //                    AddPackageResult result = PStore.AddPackage(packageReader, new PackageEnforcement(false, false, false));
    //                    packageId = result.PackageId;
    //                }

    //                LearningStoreJob job = LStore.CreateJob();
    //                Dictionary<string, object> properties = new Dictionary<string, object>();
    //                properties[Schema.PackageItem.Owner] = currentUser.Id;
    //                properties[Schema.PackageItem.FileName] = RadAsyncUpload1.UploadedFiles[0].FileName;
    //                properties[Schema.PackageItem.UploadDateTime] = DateTime.Now;

    //                job.UpdateItem(packageId, properties);
    //                job.Execute();
    //                job = LStore.CreateJob();
    //                RequestMyTraining(job, packageId);
    //                job.Execute<DataTable>();

    //                //get package title
    //                string PackageTitle;
    //                Dbconn("DefaultConnection");
    //                _ocmd = new SqlCommand("pr_Scorm_Cor_GetPackageDetails", _oconn)
    //                {
    //                    CommandType = CommandType.StoredProcedure
    //                };
    //                _ocmd.Parameters.AddWithValue("@ScoPackageId", packageId.ToString().Replace("ItemType:PackageItem, Key:", ""));
    //                _ocmd.Parameters.Add("@Title", SqlDbType.VarChar, -1);
    //                _ocmd.Parameters["@Title"].Direction = ParameterDirection.Output;
    //                _ocmd.ExecuteReader();
    //                _oconn.Close();
    //                PackageTitle = _ocmd.Parameters["@Title"].Value.ToString();

    //                DataRow drPackage = DtPackage1.NewRow();
    //                drPackage["ScoPackageId"] = packageId.ToString().Replace("ItemType:PackageItem, Key:", "");
    //                drPackage["ScoTitle"] = PackageTitle;
    //                drPackage["ScoType"] = rcbSCOType.SelectedItem.Text;
    //                drPackage["ScoTypeId"] = rcbSCOType.SelectedValue;
    //                drPackage["CourseTypeID"] = 2;
    //                drPackage["curriculumcourseID"] = "1";

    //                DtPackage1.Rows.Add(drPackage);
    //                gridPackage.DataSource = DtPackage1;
    //                gridPackage.DataBind();
    //                gridPackage.Visible = true;
    //            }

    //            //SaveSCODetails
    //            var currentUserCim = DataHelper.GetCurrentUserCIM();
    //            var courseId = Convert.ToInt32(Decrypt(Request.QueryString["CourseID"]));

    //            var db = new TranscomUniversityV3ModelContainer();
    //            var courseTitle = (from c in db.tbl_TranscomUniversity_Cor_Course
    //                                where c.CourseID == courseId
    //                                select c.Title).FirstOrDefault().ToString();
    //            var courseDesc = (from c in db.tbl_TranscomUniversity_Cor_Course
    //                                where c.CourseID == courseId
    //                                select c.Description).FirstOrDefault().ToString();
    //            int ccId = 0;

    //            pr_TranscomUniversity_ScoDetailChecker_Result retVal = db.pr_TranscomUniversity_ScoDetailChecker(courseId).SingleOrDefault();

    //            if (retVal.Msg == "0") //Not Exist
    //            {
    //                Dbconn("DefaultConnection");
    //                _ocmd = new SqlCommand("pr_Scorm_Sav_CourseDetails", _oconn)
    //                {
    //                    CommandType = CommandType.StoredProcedure
    //                };
    //                _ocmd.Parameters.AddWithValue("@CourseID", courseId);
    //                _ocmd.Parameters.AddWithValue("@Course", courseTitle != null ? courseTitle : "");
    //                _ocmd.Parameters.AddWithValue("@Description", courseDesc != null ? courseDesc : "");
    //                _ocmd.Parameters.Add("@curriculumcourseid", SqlDbType.Int);
    //                _ocmd.Parameters["@curriculumcourseid"].Direction = ParameterDirection.Output;

    //                _ocmd.ExecuteReader();
    //                _oconn.Close();
    //                ccId = Convert.ToInt32(_ocmd.Parameters["@curriculumcourseid"].Value.ToString());
    //            }

    //            int a;
    //            a = 0;

    //            pr_TranscomUniversity_lkp_GetScormPackage_Result rslt = db.pr_TranscomUniversity_lkp_GetScormPackage("", courseId).SingleOrDefault();
    //            int? curriculumCourseId = rslt != null ? rslt.CurriculumCourseID : ccId;

    //            if (gridPackage.Items.Count != 0)
    //            {
    //                foreach (GridDataItem row in gridPackage.Items)
    //                {
    //                    a += 1;

    //                    var scoTypeId = (Int32)row.GetDataKeyValue("ScoTypeId");
    //                    var scoPackageId = (Int32)row.GetDataKeyValue("ScoPackageId");
    //                    var courseTypeID = (Int32)row.GetDataKeyValue("CourseTypeID");
    //                    var scoTitle = (string)row.GetDataKeyValue("ScoTitle");

    //                    Dbconn("DefaultConnection");
    //                    _ocmd = new SqlCommand("pr_Scorm_SaveUpdate_ScoDetails", _oconn)
    //                    {
    //                        CommandType = CommandType.StoredProcedure
    //                    };
    //                    _ocmd.Parameters.AddWithValue("@ScoTypeId", scoTypeId);
    //                    _ocmd.Parameters.AddWithValue("@ScoPackageId", scoPackageId);
    //                    _ocmd.Parameters.AddWithValue("@CourseTypeID", courseTypeID);
    //                    _ocmd.Parameters.AddWithValue("@ScoTitle", scoTitle);
    //                    _ocmd.Parameters.AddWithValue("@CreatedBy", Convert.ToInt32(currentUserCim));
    //                    _ocmd.Parameters.AddWithValue("@curriculumcourseid", curriculumCourseId);
    //                    _ocmd.ExecuteReader();
    //                    _oconn.Close();
    //                }
    //            }

    //            gridPackage.DataSource = new string[] { };
    //            gridPackage.DataBind();

    //            gridPackage.Visible = false;
    //            rcbSCOType.ClearSelection();
    //            CreatePackageDataTableColumns();
    //            GetDBPackages(courseId, Convert.ToInt32(curriculumCourseId));

    //            RadWindowManager1.RadAlert("Course successfully added.", 330, 180, "System Message", "");
    //        }
    //        catch (Exception ex)
    //        {
    //            RadWindowManager1.RadAlert("Error Uploading SCORM Package. Please contact your System Administrator.", 330, 180, "Error Message", "");
    //        }
    //    }
    //    else
    //    {
    //        RadWindowManager1.RadAlert("Invalid File Format", 330, 180, "Error Message", "");
    //    }
    //}

    private void loadTrainingTypeWindow()
    {

        var d = DataHelper.getCourseTrainingType();

        lvIltType.DataSource = d;
        lvIltType.DataBind();
    }

    protected void btnAddSectionClicked(object sender, EventArgs e)
    {
        try
        {

            if (Page.IsValid)
            {
                int courseID = Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"]));
                int resourceTypeID = Convert.ToInt32(cbAddSectionResourceType.SelectedValue);
                string sectionName = txtSectionName.Text;

                var db = new TranscomUniversityV3ModelContainer();
                pr_transcomuniversity_AddSection_Result data = db.pr_transcomuniversity_AddSection(resourceTypeID, courseID, sectionName).SingleOrDefault();

                if (data.res == 0)
                {
                    RadWindowManager1.RadAlert("Section Name Already Exists!", 330, 180, "Duplicate Section Name", "");
                }
                else if (data.res == 1)
                {
                    txtSectionName.Text = "";
                    cbAddSectionResourceType.ClearSelection();
                    RadScriptManager.RegisterStartupScript(Page, Page.GetType(), "closeCreateSectionWindow", Utils.callClientScript("closeCreateSectionWindow", "1"), true);
                    RadWindowManager1.RadAlert("Section successfully added!", 330, 180, "Success", "");
                }

                if (resourceTypeID == 1)
                {
                    UserControl content = (UserControl)(this.Parent.Parent).FindControl("Content");
                    RadListView lvCourseDefault_Learner = (RadListView)content.FindControl("lvCourseDefault_Learner");
                    lvCourseDefault_Learner.Rebind();
                }
                else if (resourceTypeID == 5)
                {
                    UserControl content = (UserControl)(this.Parent.Parent).FindControl("Content");
                    RadListView lvCourseDefault_Additional = (RadListView)content.FindControl("lvCourseDefault_Additional");
                    lvCourseDefault_Additional.Rebind();
                }
                else {
                    UserControl content = (UserControl)(this.Parent.Parent).FindControl("Content");
                    RadListView lvCourseDefault_Trainer = (RadListView)content.FindControl("lvCourseDefault_Trainer");
                    lvCourseDefault_Trainer.Rebind();
                }

                
            }
        }
        catch (Exception ex)
        {
            RadWindowManager1.RadAlert(ex.Message, 330, 180, "Error", "");
        }

    }

    protected void rcbSCOType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            rcbCourseSections.ClearSelection();
            rcbCourseSections.Items.Clear();
            int resourceTypeID = Convert.ToInt32(rcbSCOType.SelectedValue);
            int courseid = Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseiD"]));
            var db = new TranscomUniversityV3ModelContainer();
            var sections = db.tbl_transcomuniversity_rlt_CourseSections
                                .Where(s => s.CourseID == courseid && s.HideFromList == false && s.ResourceTypeID == resourceTypeID)
                                .ToList();


            rcbCourseSections.DataSource = sections;
            rcbCourseSections.DataBind();
        }
        catch (ArgumentOutOfRangeException aore)
        {
            RadWindowManager1.RadAlert("Something went wrong. Please reload the page and try again.", 330, 180, "Error", "");


        }
    }

    protected void rcbResourceTypeBrowse_IndexChanged(object sender, EventArgs e)
    {
        try
        {

            rcbSectionBrowse.ClearSelection();
            rcbSectionBrowse.Items.Clear();
            int resourceTypeID = Convert.ToInt32(rcbResourceTypeBrowse.SelectedValue);
            int courseid = Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseiD"]));
            var db = new TranscomUniversityV3ModelContainer();
            var sections = db.tbl_transcomuniversity_rlt_CourseSections
                                .Where(s => s.CourseID == courseid && s.HideFromList == false && s.ResourceTypeID == resourceTypeID)
                                .ToList();


            rcbSectionBrowse.DataSource = sections;
            rcbSectionBrowse.DataBind();
        }
        catch (ArgumentOutOfRangeException aore)
        {
            RadWindowManager1.RadAlert("Something went wrong. Please reload the page and try again.", 330, 180, "Error", "");


        }
    }

    private void loadUploadResourceType(int? courseType)
    {
        var db = new TranscomUniversityV3ModelContainer();

        if (courseType == 1) //NON SCORM Course
        {
            var rs = db.vw_transcomuniversity_ResourceType.OrderBy(r => r.ScoTypeId).ToList();
            rcbSCOType.DataTextField = "ScoType";
            rcbSCOType.DataValueField = "ScoTypeID";
            rcbSCOType.DataSource = rs;
            rcbSCOType.DataBind();
        }
        else
        {
            var rs = db.tbl_Scorm_Lkp_ScoType.Where(r => r.Inactive == 0).OrderBy(r => r.Ordinal).ToList();
            rcbSCOType.DataTextField = "ScoType";
            rcbSCOType.DataValueField = "ScoTypeID";
            rcbSCOType.DataSource = rs;
            rcbSCOType.DataBind();
        }
       
    }

    protected void rgAllResources_OnNeedDataSource(object sender, EventArgs e)
    {
        int selectedCourseID = 0;
        try
        {
            selectedCourseID = Convert.ToInt32(rcbCourse.SelectedValue);
        }
        catch (FormatException fe)
        {
            selectedCourseID = 0;
        }

        var allResources = DataHelper.GetAllResources(selectedCourseID);


        rgAllResources.DataSource = allResources;

    }

    protected void AddSelectedFiles(object sender, EventArgs e)
    {

        string courseList = hfSelectedFiles.Value;
        string[] data = courseList.Split(',');

        int _actionID = DataHelper.getLastLogID() + 1;
        string _userIP = DataHelper.GetIPAddress();
        string _userID = DataHelper.GetCurrentUserCIM();
        double _duration = 0;

        DateTime startTime;
        DateTime endTime;

        int successCount = 0;

        List<Transaction> GroupList = new List<Transaction>();
        string stat = "";
        string _action = "";

        try
        {
            if (rcbResourceTypeBrowse.SelectedValue != "")
            {
                if (data.Length > 1)
                {
                    pr_transcomuniversity_InsertFile_Result res = null;
                    using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
                    {
                        var dts = db.CreateQuery<DateTime>("CurrentDateTime() ");
                        startTime = dts.AsEnumerable().First();
                        //tobe inserted
                        int fileID = -1;
                        int scoTypeID = Convert.ToInt32(rcbResourceTypeBrowse.SelectedValue);
                        int sectionID = Convert.ToInt32(rcbSectionBrowse.SelectedValue == "" ? "0" : rcbSectionBrowse.SelectedValue);
                        int courseID = Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"]));
                        int addedBy = Convert.ToInt32(DataHelper.GetCurrentUserCIM());

                        for (int i = 0; i < (data.Length - 1); i++)
                        {
                            fileID = Convert.ToInt32(data[i]);
                            res = db.pr_transcomuniversity_InsertFile(fileID, scoTypeID, sectionID, courseID, addedBy).SingleOrDefault();

                            if (res.res == 1)
                            {
                                stat = "Successful";
                                successCount++;

                            }
                            else if (res.res == 2)
                            {
                                stat = "Duplicate";
                            }
                            else
                            {
                                stat = "Fail";
                            }

                            Transaction group = new Transaction
                            {
                                Id = Convert.ToInt32(data[i]),
                                Name = res.Title,
                                Status = stat
                            };

                            GroupList.Add(group);
                        }

                        _action = successCount > 1 ? "Added " + successCount + " Files" : "Added File " + res.Title;

                        if (successCount > 0)
                        {
                            var dte = db.CreateQuery<DateTime>("CurrentDateTime() ");
                            endTime = dte.AsEnumerable().First();
                            _duration = (endTime - startTime).TotalMilliseconds;

                            DataHelper.logCourse(courseID, _actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);
                        }
                    }



                    if (data.Length > 1)
                    {
                        bfrgTransactionSummary.DataSource = GroupList;
                        bfrgTransactionSummary.DataBind();

                        RadScriptManager.RegisterStartupScript(Page, Page.GetType(), "showTransactionRightPanel", Utils.callClientScript("showTransaction"), true);
                    }

                }
                else
                {
                    RadWindowManager1.RadAlert("No file was selected!", 330, 180, "Error Message", "");
                }
            }
            else
            {
                RadWindowManager1.RadAlert("Please Select Resource Type", 330, 180, "Invalid Resource Type", "");
            }
        }
        catch (Exception ex)
        {
            RadWindowManager1.RadAlert("Error In Adding file. Please contact your System Administrator." + ex.Message, 330, 180, "Error Message", "");
        }
    }

    protected void bfrgTransactionSummary_OnItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            GridDataItem dataBoundItem = e.Item as GridDataItem;

            if (dataBoundItem["Status"].Text == "Duplicate")
            {
                dataBoundItem["Status"].ForeColor = Color.DarkGray;
            }
            else if (dataBoundItem["Status"].Text == "Fail")
            {
                dataBoundItem["Status"].ForeColor = Color.Red;
            }
            else
            {
                dataBoundItem["Status"].ForeColor = Color.YellowGreen;
            }
        }
    }

    protected void rcbCourse_SelectedIndexChanged(object sender, EventArgs e)
    {
        rgAllResources.Rebind();
    }

    bool questionBuilderUC_questionBuilderUCReloadQuestion(object sender)
    {
        questionBuilder.reloadQuestionDetails();
        return true;
    }

    protected void ReloadCreateAssessment(object sender, EventArgs e)
    {
        assessmentBuilder.Visible = false;
        questionBuilder.Visible = false;
        testStatus.Visible = false;
        examsList.Visible = true;
    }

    bool questionBuilderUC_questionBuilderUCAddQuestion(object sender)
    {
        questionBuilder.clearForm();
        return true;
    }
  

}