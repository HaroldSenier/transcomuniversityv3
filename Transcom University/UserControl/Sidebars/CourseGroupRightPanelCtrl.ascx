﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CourseGroupRightPanelCtrl.ascx.cs" Inherits="CourseGroupRightPanelCtrl" %>

<%@ Register TagPrefix="AddCourseGroupTabCtrlV3" TagName="CourseGroupTabCtrlV3" Src="~/UserControl/Pages/CourseGroupTabCtrl.ascx" %>

<rad:RadAjaxManagerProxy ID="rmpGroup" runat="server">
    <AjaxSettings>
        <rad:AjaxSetting AjaxControlID="btnViewList">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="rgViewGroupList" />
                <rad:AjaxUpdatedControl ControlID="pnlGroupList" LoadingPanelID="localLoadingPanel" />
            </UpdatedControls>
        </rad:AjaxSetting>
    </AjaxSettings>
</rad:RadAjaxManagerProxy>
<rad:RadWindowManager ID="rwmGroup" RenderMode="Lightweight" EnableShadow="true"
    Modal="true" VisibleOnPageLoad="false" Behaviors="Close, Move" Opacity="99" runat="server"
    VisibleStatusbar="false">
    <Windows>
        <rad:RadWindow RenderMode="Lightweight" ID="rdGroupList" Behaviors="Close,Move" VisibleOnPageLoad="false"
            runat="server" CssClass="tc-radwindow-1 height-inherit rdGroupList" Title="<%$ Resources:LocalizedResource, GroupsList%>"
            Width="800" Height="350px" OnClientClose="hideMembersListLoad" OnClientBeforeShow="clearSelectedItems">
            <ContentTemplate>
                <asp:Panel ID="pnlGroupList" runat="server">
                    <table width="100%">
                        <tbody>
                            <tr>
                                <td>
                                    <rad:RadGrid RenderMode="Lightweight" ID="rgViewGroupList" runat="server" AllowSorting="True"
                                        CssClass="GridLess" AllowMultiRowSelection="true" AllowPaging="true">
                                        <MasterTableView AutoGenerateColumns="false" TableLayout="Auto" HeaderStyle-ForeColor="Teal"
                                            DataKeyNames="GroupID" ClientDataKeyNames="GroupID">
                                            <Columns>
                                                <rad:GridClientSelectColumn UniqueName="cboxGroup">
                                                </rad:GridClientSelectColumn>
                                                <rad:GridBoundColumn DataField="GroupName" HeaderText="<%$ Resources:LocalizedResource, GroupBatchName%>" UniqueName="GroupName">
                                                </rad:GridBoundColumn>
                                                <rad:GridBoundColumn DataField="Account" HeaderText="<%$ Resources:LocalizedResource, Account%>" UniqueName="Account">
                                                </rad:GridBoundColumn>
                                                <rad:GridBoundColumn DataField="Site" HeaderText="<%$ Resources:LocalizedResource, Site%>" UniqueName="Site">
                                                </rad:GridBoundColumn>
                                                <rad:GridBoundColumn DataField="Members" HeaderText="<%$ Resources:LocalizedResource, Members%>" UniqueName="Members">
                                                </rad:GridBoundColumn>
                                            </Columns>
                                        </MasterTableView>
                                        <ClientSettings>
                                            <Selecting AllowRowSelect="true" />
                                            <Resizing AllowResizeToFit="true" ResizeGridOnColumnResize="false" />
                                            <Scrolling AllowScroll="true" UseStaticHeaders="false" ScrollHeight="" />
                                            <ClientEvents OnRowSelected="gtGroupRowSelected" OnRowDeselected="gtGroupRowDeselected"
                                                OnRowCreated="gtGroupRowCreated" />
                                        </ClientSettings>
                                    </rad:RadGrid>
                                </td>
                            </tr>
                        </tbody>
                        <tfoot class="footer-bottom">
                            <tr>
                                <td style="padding-top: 10px;">
                                    <asp:LinkButton ID="btnAddGroup" OnClick="btn_AddGroup_Click" ToolTip="Add Selected Group/s"
                                        runat="server" Text="<%$ Resources:LocalizedResource, Add%>" CssClass="btn tc-btn-md btn-teal btn-flat pull-right "></asp:LinkButton>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </asp:Panel>
            </ContentTemplate>
        </rad:RadWindow>
        <rad:RadWindow RenderMode="Lightweight" ID="rdTransactionSummary" Behaviors="Close,Move"
            VisibleOnPageLoad="false" runat="server" CssClass="tc-radwindow-1" Height="330px"
            Title="<%$ Resources:LocalizedResource, ResultSummary%>">
            <ContentTemplate>
                <asp:Panel ID="pnlTransactionList" runat="server">
                    <rad:RadGrid RenderMode="Lightweight" ID="rgTransactionSummary" runat="server" AllowSorting="True"
                        CssClass="GridLess" AllowMultiRowSelection="true" AllowPaging="true" OnItemDataBound="rgTransactionSummary_OnItemDataBound">
                        <MasterTableView AutoGenerateColumns="false" TableLayout="Auto" HeaderStyle-ForeColor="Teal"
                            DataKeyNames="Id" ClientDataKeyNames="Id">
                            <Columns>
                                <rad:GridTemplateColumn HeaderText="#">
                                    <ItemTemplate>
                                        <%#Container.ItemIndex+1 %></ItemTemplate>
                                </rad:GridTemplateColumn>
                                <rad:GridBoundColumn DataField="Name" HeaderText="<%$ Resources:LocalizedResource, GroupBatchName%>" UniqueName="Name">
                                </rad:GridBoundColumn>
                                <rad:GridBoundColumn DataField="Status" HeaderText="<%$ Resources:LocalizedResource, Status%>" UniqueName="Status">
                                </rad:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                    </rad:RadGrid>
                </asp:Panel>
                <Button id="btnTransactionsList" onclick="btnTransactionsList_Click(); return false;" runat="server" 
                    title="Close" class="btn tc-btn-md btn-teal btn-flat bottom-center"><asp:Label runat="server" ID="label1" Text=" <%$ Resources:LocalizedResource, Close%>" ></asp:Label></Button>
            </ContentTemplate>
        </rad:RadWindow>
    </Windows>
</rad:RadWindowManager>
<div id="pnlGroup" class="groupSidebarContainer bg-color-light-gray" style="height: 135vh;">
    <div class="row" style="text-align: center; font-size: inherit;">
        <h5 style="font-weight: bold;">
            Add Group/s
        </h5>
        <asp:LinkButton ID="btnViewList" OnClick="btnViewList_Click" ToolTip="View All Groups"
            Text="View List" CssClass="btn btn-teal btn-flat tc-btn-md" runat="server"></asp:LinkButton>
    </div>
</div>
<asp:HiddenField ID="hfSelectedGroup" runat="server" />
<rad:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        function groupOpenGroupList() {
            var radwindow = $find('<%=rdGroupList.ClientID %>');
            radwindow.show();
        }
        function hideMembersListLoad() {
            $(".Loading2").hide();
            clearSelectedItems();
        }

        var selectedGroupItems = [];

        function gtGroupRowSelected(sender, args) {
            var id = args.getDataKeyValue("GroupID");

            if (!selectedGroupItems.includes(id)) {
                selectedGroupItems.push(id);
            }
            setSelectedItems();

        }

        function gtGroupRowDeselected(sender, args) {
            var id = args.getDataKeyValue("GroupID");

            if (selectedGroupItems.includes(id)) {
                selectedGroupItems.splice(selectedGroupItems.indexOf(id), 1);
            }
            setSelectedItems();
        }

        function gtGroupRowCreated(sender, args) {
            var id = args.getDataKeyValue("GroupID");
            if (selectedGroupItems.includes(id) && selectedGroupItems.length > 0) {
                args.get_gridDataItem().set_selected(true);
            }
        }

        function setSelectedItems() {
            var elements = selectedGroupItems;
            var outputStr = "";
            for (var i = 0; i < elements.length; i++) {
                outputStr += elements[i] + ",";

            }
            $("#" + "<%= hfSelectedGroup.ClientID %>").val(outputStr);
        }

        function clearSelectedItems() {
            selectedGroupItems = [];
            $("#" + "<%= hfSelectedGroup.ClientID %>").val("");
            var grid = $find("<%= rgViewGroupList.ClientID %>");
            grid.clearSelectedItems();
        }

        function showTransaction() {
            var radwindow = $find('<%=rdTransactionSummary.ClientID %>');
            radwindow.show();
        }

        function btnTransactionsList_Click() {
            var radwindow = $find('<%=rdTransactionSummary.ClientID %>');
            radwindow.close();
        }
    </script>
</rad:RadScriptBlock>
