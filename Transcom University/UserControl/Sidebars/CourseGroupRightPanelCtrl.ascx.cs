﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using TranscomUniversityV3Model;
using System.Drawing;
using TransactionNames;

public partial class CourseGroupRightPanelCtrl : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            rgViewGroupList.DataSource = DataHelper.getAllGroup();
            rgViewGroupList.DataBind();
        }
    }

    protected void btnViewList_Click(object sender, EventArgs e)
    {
        rgViewGroupList.DataSource = DataHelper.getAllGroup();
        rgViewGroupList.DataBind();
        rgViewGroupList.SelectedIndexes.Clear();
        RadScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", Utils.callClientScript("groupOpenGroupList"), true);
    }

    protected void btn_AddGroup_Click(object sender, EventArgs e)
    {
        int _courseID = Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"]));

        string courseList = hfSelectedGroup.Value;
        string[] data = courseList.Split(',');

        int _actionID = DataHelper.getLastLogID() + 1;
        string _userIP = DataHelper.GetIPAddress();
        string _userID = DataHelper.GetCurrentUserCIM();
        double _duration = 0;

        DateTime startTime;
        DateTime endTime;

        int successCount = 0;

        List<Transaction> GroupList = new List<Transaction>();
        string stat = "";
        string _action = "";

        try
        {
            if (data.Length > 1)
            {
                pr_TranscomUniversity_InsertGroup_Result res = null; ;
                using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
                {
                    var dts = db.CreateQuery<DateTime>("CurrentDateTime() ");
                    startTime = dts.AsEnumerable().First();
                    int _groupID = -1;

                    for (int i = 0; i < (data.Length - 1); i++)
                    {
                        _groupID = Convert.ToInt32(data[i]);
                        res = db.pr_TranscomUniversity_InsertGroup(_courseID, _groupID).SingleOrDefault();

                        if (res.Msg == "1")
                        {
                            stat = "Successful";
                            successCount++;

                        }
                        else if (res.Msg == "2")
                        {
                            stat = "Duplicate";
                        }
                        else
                        {
                            stat = "Fail";
                        }

                        Transaction group = new Transaction
                        {
                            Id = Convert.ToInt32(data[i]),
                            Name = res.GroupName,
                            Status = stat
                        };

                        GroupList.Add(group);
                    }

                    _action = successCount > 1 ? "Added " + successCount + " Groups" : "Added " + DataHelper.getGroupName(_groupID);

                    if (successCount > 0)
                    {
                        var dte = db.CreateQuery<DateTime>("CurrentDateTime() ");
                        endTime = dte.AsEnumerable().First();
                        _duration = (endTime - startTime).TotalMilliseconds;

                        DataHelper.logCourse(_courseID, _actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);
                    }
                }
                CourseGroupTabCtrl tabCtrl = (CourseGroupTabCtrl)(this.Parent.Parent).FindControl("gtcourseGroup");

                RadGrid rgGroup = (RadGrid)tabCtrl.FindControl("rgCourseGroup");
                rgGroup.Rebind();

                if (data.Length > 1)
                {
                    rgTransactionSummary.DataSource = GroupList;
                    rgTransactionSummary.DataBind();

                    RadScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", Utils.callClientScript("showTransaction"), true);
                }

              
            }
            else
            {
                rwmGroup.RadAlert("No group was selected!", 330, 180, "Error Message", "");
            }
        }
        catch
        {
            rwmGroup.RadAlert("Error In Adding Group. Please contact your System Administrator.", 330, 180, "Error Message", "");
        }
    }

    protected void rgTransactionSummary_OnItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            GridDataItem dataBoundItem = e.Item as GridDataItem;

            if (dataBoundItem["Status"].Text == "Duplicate")
            {
                dataBoundItem["Status"].ForeColor = Color.DarkGray;
            }
            else if (dataBoundItem["Status"].Text == "Fail")
            {
                dataBoundItem["Status"].ForeColor = Color.Red;
            }
            else
            {
                dataBoundItem["Status"].ForeColor = Color.YellowGreen;
            }
        }
    }
}