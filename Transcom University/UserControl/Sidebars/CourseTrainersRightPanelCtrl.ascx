﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CourseTrainersRightPanelCtrl.ascx.cs"
    Inherits="CourseTrainersRightPanelCtrl" %>
<%@ Register TagPrefix="AddTrainersCBUserCtrlV3" TagName="TrainersCBUserCtrlV3" Src="~/UserControl/Pages/TrainersCBUserCtrl.ascx" %>
<rad:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        function keyPress(sender, args) {
            var text = sender.get_value() + args.get_keyCharacter();
            if (!text.match('^[0-9]+$'))
                args.set_cancel(true);
        }

        function viewTrainersList() {
//            try {
//                debugger;
//                var CIMNumber = $('#ctl00_contentPlaceHolderMain_gtTrainersPanel_rwViewTrainersList_C_gridViewTrainersList_ctl00_ctl02_ctl02_FilterTextBox_CIMNumber').val();
//                var TextBox_Name = $('#ctl00_contentPlaceHolderMain_gtTrainersPanel_rwViewTrainersList_C_gridViewTrainersList_ctl00_ctl02_ctl02_FilterTextBox_Name').val();

//                $('#ctl00_contentPlaceHolderMain_gtTrainersPanel_rwViewTrainersList_C_gridViewTrainersList_ctl00_ctl02_ctl02_FilterTextBox_CIMNumber').val('');
//                $('#ctl00_contentPlaceHolderMain_gtTrainersPanel_rwViewTrainersList_C_gridViewTrainersList_ctl00_ctl02_ctl02_FilterTextBox_Name').val('');

//                if (CIMNumber != "" || CIMNumber == null) {
//                    $find("ctl00_contentPlaceHolderMain_gtTrainersPanel_rwViewTrainersList_C_gridViewTrainersList_ctl00")._filterNoDelay("ctl00_contentPlaceHolderMain_gtTrainersPanel_rwViewTrainersList_C_gridViewTrainersList_ctl00_ctl02_ctl02_FilterTextBox_CIMNumber", "CIMNumber");
//                }
//                if (TextBox_Name != "" || TextBox_Name == null) {
//                    $find("ctl00_contentPlaceHolderMain_gtTrainersPanel_rwViewTrainersList_C_gridViewTrainersList_ctl00")._filterNoDelay("ctl00_contentPlaceHolderMain_gtTrainersPanel_rwViewTrainersList_C_gridViewTrainersList_ctl00_ctl02_ctl02_FilterTextBox_Name", "Name");
//                }
//            }
//            catch (err) { }
//            finally {
//                var radwindow = $find('<%=rwViewTrainersList.ClientID %>');
//                radwindow.show();
//            }

            var radwindow = $find('<%=rwViewTrainersList.ClientID %>');
            radwindow.show();
        }

        var selectedTrainerItems = [];

        function trainerRowSelected(sender, args) {
            var id = args.getDataKeyValue("CIMNumber");

            if (!selectedTrainerItems.includes(id)) {
                selectedTrainerItems.push(id);
            }
            setSelectedTrainerItems();
        }

        function trainerRowDeselected(sender, args) {
            var id = args.getDataKeyValue("CIMNumber");

            if (selectedTrainerItems.includes(id)) {
                selectedTrainerItems.splice(selectedTrainerItems.indexOf(id), 1);
            }
            setSelectedTrainerItems();
        }

        function trainerRowCreated(sender, args) {
            var id = args.getDataKeyValue("CIMNumber");
            if (selectedTrainerItems.includes(id) && selectedTrainerItems.length > 0) {
                args.get_gridDataItem().set_selected(true);
            }
        }

        function setSelectedTrainerItems() {
            var elements = selectedTrainerItems;
            var outputStr = "";
            for (var i = 0; i < elements.length; i++) {
                outputStr += elements[i] + ",";
            }
            $("#" + "<%= hfSelectedTrainer.ClientID %>").val(outputStr);
        }

        function clearSelectedTrainerItems() {
            selectedTrainerItems = [];
            var grid = $find("<%= gridViewTrainersList.ClientID %>");
            grid.clearSelectedItems();

        }

        function confirmSuccess() {
            $("#confirmSuccess").modal("toggle");
        }
        function ctshowTransaction() {
            $find('<%=rwViewTrainersList.ClientID %>').close();
            var radwindow = $find('<%=ctrdTransactionSummary.ClientID %>');
            radwindow.show();
        }

        function ctbtnTransactionsList_Click() {
            var radwindow = $find('<%=ctrdTransactionSummary.ClientID %>');
            radwindow.close();
        }

        function openFileTrainerWindow() {
            //$("#fakeFilenameUser").html("");
            $find("<%= upTrainerList.ClientID %>").deleteAllFileInputs();
            $telerik.$(".trainerUploader .ruFileInput").click();
        }

        function fileSelectedTrainer(sender, args) {
            $("#fakeFilenameTrainer").html(args.get_fileName());
            $("#fakeFilenameTrainer").attr('title', args.get_fileName());
            var fileExtention = args.get_fileName().substring(args.get_fileName().lastIndexOf('.') + 1, args.get_fileName().length);
            if (args.get_fileName().lastIndexOf('.') != -1) {//this checks if the extension is correct
                if (sender.get_allowedFileExtensions().toLowerCase().indexOf(fileExtention.toLowerCase()) == -1) {
                    radalert("Please upload a .csv file.", 330, 180, $('#<%= hfError.ClientID %>').val(), "");
                    sender.deleteAllFileInputs();
                }
                else {
                    console.log("File Supported.");
                }
            }
            else {
                sender.deleteAllFileInputs();
                radalert("Please upload a .csv file", 330, 180, $('#<%= hfError.ClientID %>').val(), "");
            }
        }
    </script>
</rad:RadScriptBlock>
<rad:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <rad:AjaxSetting AjaxControlID="btnInviteTrainer">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="RadMultiPage2" />
                <rad:AjaxUpdatedControl ControlID="ulBreadcrumb" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="gridViewTrainersList">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="gridViewTrainersList" LoadingPanelID="RadAjaxLoadingPanel2" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnAssignMultipleTrainers">
            <UpdatedControls>
                <%--
              <rad:AjaxUpdatedControl ControlID="btnAssignMultipleTrainers" />--%>
                <rad:AjaxUpdatedControl ControlID="pnlViewTrainersList" LoadingPanelID="RadAjaxLoadingPanel1" />
                <rad:AjaxUpdatedControl ControlID="ltTrainers" />
                <rad:AjaxUpdatedControl ControlID="gridTrainers" />
                <rad:AjaxUpdatedControl ControlID="ctrgTransactionSummary" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnUploadBulkTrainers">
            <UpdatedControls>
                <%-- <rad:AjaxUpdatedControl ControlID="btnUploadBulkTrainers" />--%>
                <rad:AjaxUpdatedControl ControlID="fakeFilenameTrainer" />
                <rad:AjaxUpdatedControl ControlID="upTrainerList" LoadingPanelID="RadAjaxLoadingPanel1" />
                <rad:AjaxUpdatedControl ControlID="ltTrainers" />
                <rad:AjaxUpdatedControl ControlID="gridTrainers" />
                <rad:AjaxUpdatedControl ControlID="ctrgTransactionSummary" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnViewTrainersList">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="btnViewTrainersList" />
                <rad:AjaxUpdatedControl ControlID="gridViewTrainersList" LoadingPanelID="RadAjaxLoadingPanel2" />
            </UpdatedControls>
        </rad:AjaxSetting>
    </AjaxSettings>
</rad:RadAjaxManagerProxy>
<rad:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true" RenderMode="Lightweight"
    Skin="Bootstrap" Modal="true" VisibleStatusbar="false">
    <Windows>
        <rad:RadWindow ID="rwViewTrainersList" runat="server" RenderMode="Lightweight" Modal="true"
            Skin="Bootstrap" Behaviors="Move, Close, Resize" Title="<%$ Resources:LocalizedResource, SelectMultipleTrainers%>"
            CssClass="tc-radwindow-1 height-inherit" Height="600px" Width="950px" OnClientBeforeShow="clearSelectedTrainerItems"
            VisibleStatusbar="false">
            <ContentTemplate>
                <asp:Panel ID="pnlViewTrainersList" runat="server">
                    <rad:RadGrid ID="gridViewTrainersList" runat="server" RenderMode="Lightweight" AutoGenerateColumns="false"
                        PageSize="10" DataSourceID="dsViewTrainersList" CssClass="GridLess" AllowMultiRowSelection="true"
                        AllowFilteringByColumn="true">
                        <GroupingSettings CaseSensitive="false" />
                        <PagerStyle Mode="NextPrev" AlwaysVisible="true" />
                        <ClientSettings EnableAlternatingItems="true">
                            <Selecting AllowRowSelect="true"></Selecting>
                            <Resizing AllowColumnResize="true" ResizeGridOnColumnResize="true" AllowResizeToFit="true" />
                            <ClientEvents OnRowSelected="trainerRowSelected" OnRowDeselected="trainerRowDeselected"
                                OnRowCreated="trainerRowCreated" />
                        </ClientSettings>
                        <ItemStyle Wrap="false"></ItemStyle>
                        <MasterTableView DataKeyNames="CIMNumber" ClientDataKeyNames="CIMNumber" AllowSorting="true"
                            AllowPaging="true" CommandItemDisplay="None" HeaderStyle-ForeColor="Teal" AllowFilteringByColumn="true">
                            <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                            <Columns>
                                <rad:GridClientSelectColumn UniqueName="chkMultipleTrainers" HeaderText="Select">
                                </rad:GridClientSelectColumn>
                                <rad:GridBoundColumn UniqueName="CIMNumber" HeaderText="<%$ Resources:LocalizedResource, CimNumber%>" DataField="CIMNumber"
                                    AllowFiltering="true" DataType="System.String" FilterControlWidth="120px" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false" AutoPostBackOnFilter="True">
                                </rad:GridBoundColumn>
                                <rad:GridBoundColumn UniqueName="Name" HeaderText="<%$ Resources:LocalizedResource, Name%>" DataField="Name" AllowFiltering="true"
                                    FilterControlWidth="250px" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                                    AutoPostBackOnFilter="True">
                                </rad:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                    </rad:RadGrid>
                    <asp:SqlDataSource ID="dsViewTrainersList" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
                        SelectCommand="pr_TranscomUniversity_AllEmployees" SelectCommandType="StoredProcedure" />
                    <br />
                    <div style="margin-left: 75%;">
                        <asp:Button ID="btnAssignMultipleTrainers" runat="server" OnClick="btnAssignMultipleTrainers_Click"
                            Text="Assign Trainer(s) to this Course" CssClass="btn btn-md btn-teal button-flat" />
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </rad:RadWindow>
        <rad:RadWindow RenderMode="Lightweight" ID="ctrdTransactionSummary" Behaviors="Close,Move"
            VisibleOnPageLoad="false" runat="server" CssClass="tc-radwindow-1" Height="330px"
            Title="<%$ Resources:LocalizedResource, ResultSummary%>">
            <ContentTemplate>
                <asp:Panel ID="pnlTransactionList" runat="server">
                    <rad:RadGrid RenderMode="Lightweight" ID="ctrgTransactionSummary" runat="server"
                        AllowSorting="True" CssClass="GridLess" AllowMultiRowSelection="true" AllowPaging="true"
                        OnItemDataBound="rgTransactionSummary_OnItemDataBound">
                        <MasterTableView AutoGenerateColumns="false" TableLayout="Auto" HeaderStyle-ForeColor="Teal"
                            DataKeyNames="Id" ClientDataKeyNames="Id">
                            <Columns>
                                <rad:GridTemplateColumn HeaderText="#">
                                    <ItemTemplate>
                                        <%#Container.ItemIndex+1 %></ItemTemplate>
                                </rad:GridTemplateColumn>
                                <rad:GridBoundColumn DataField="Name" HeaderText="<%$ Resources:LocalizedResource, Name%>" UniqueName="Name">
                                </rad:GridBoundColumn>
                                <rad:GridBoundColumn DataField="Status" HeaderText="<%$ Resources:LocalizedResource, Status%>" UniqueName="Status">
                                </rad:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                    </rad:RadGrid>
                </asp:Panel>
                <button id="ctbtnTransactionsList" onclick="ctbtnTransactionsList_Click(); return false;"
                    runat="server" title="Close" class="btn tc-btn-md btn-teal button-flat bottom-center">
                    Ok</button>
            </ContentTemplate>
        </rad:RadWindow>
    </Windows>
</rad:RadWindowManager>
<div id="pnlTrainers" class="groupSidebarContainer bg-color-light-gray" style="height: 135vh">
    <div class="row" style="text-align: center; font-size: inherit;">
        <h5 style="font-weight: bold;">
            Invite Trainers
        </h5>
        <br />
        <div class="input-group">
            <rad:RadTextBox ID="txtInviteTrainer" runat="server" Skin="Bootstrap" MaxLength="10"
                EmptyMessage="Type CIM" Width="200px">
                <ClientEvents OnKeyPress="keyPress" />
            </rad:RadTextBox>
            <div class="input-group-btn">
                <asp:LinkButton ID="btnInviteTrainer" runat="server" OnClick="btnInviteTrainer_Click"
                    Font-Underline="false">
                    <i id="i1" runat="server" class="icon fa fa-plus-square" style="color: Black; font-size: 25px;">
                    </i>
                </asp:LinkButton>
            </div>
        </div>
        <br />
        <h5 style="font-weight: bold;">
            or
        </h5>
        <br />
        <h5 style="font-weight: bold;">
            <asp:Label runat="server" ID="label1" Text=" <%$ Resources:LocalizedResource, Select%>" ></asp:Label> &nbsp; <asp:Label runat="server" ID="label2" Text=" <%$ Resources:LocalizedResource, Trainers%>" ></asp:Label>
        </h5>
        <br />
        <%--<button id="btnViewTrainersList" onclick="viewTrainersList(); return false;"  runat="server" class="btn btn-md btn-teal button-flat" class="display-none">View List</button>--%>
        <asp:LinkButton ID="btnViewTrainersList" runat="server" Text="View List"  OnClick="btnViewTrainersList_Click"
            CssClass="btn btn-md btn-teal button-flat"></asp:LinkButton>
        <br />
        <h5 style="font-weight: bold;">
            <asp:Label runat="server" ID="label3" Text=" <%$ Resources:LocalizedResource, Or%>" ></asp:Label>
        </h5>
        <br />
        <h5 style="font-weight: bold;">
            Upload bulk Trainers
        </h5>
        <br />
        <div class="uploader-container inline-flex">
            <asp:Button ID="Button1" Text="<%$ Resources:LocalizedResource, ChooseFile%>" runat="server" OnClientClick="openFileTrainerWindow(); return false;"
                Height="25px" CssClass="pull-left" />
            <asp:Label ID="fakeFilenameTrainer" Text="" runat="server" ClientIDMode="Static"
                Style="float: right; text-overflow: ellipsis; overflow: hidden; width: 60%;" />
            &nbsp;&nbsp;&nbsp;
            <rad:RadAsyncUpload ID="upTrainerList" runat="server" MultipleFileSelection="Disabled"
                ViewStateMode="Enabled" MaxFileInputsCount="1" ControlObjectsVisibility="None"
                Width="100%" ToolTip="Select File to Upload" OnClientFileSelected="fileSelectedTrainer"
                AllowedFileExtensions=".csv" CssClass="trainerUploader display-none">
            </rad:RadAsyncUpload>
            <br />
            <asp:LinkButton ID="btnUploadBulkTrainers" runat="server" Text="Upload Bulk Trainer"
                OnClick="btnUploadBulkTrainers_Click" CssClass="btn btn-md btn-teal button-flat"></asp:LinkButton>
        </div>
    </div>
</div>
<asp:HiddenField ID="hfSelectedTrainer" runat="server" />
<asp:HiddenField runat="server" ID="hfError" Value="<%$ Resources:LocalizedResource, Error%>" />
