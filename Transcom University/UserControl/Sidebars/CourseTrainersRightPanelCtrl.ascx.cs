﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.IO;
using System.Text;
using TranscomUniversityV3Model;
using Telerik.Web.UI;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Drawing;
using TransactionNames;

public partial class CourseTrainersRightPanelCtrl : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Form.Attributes.Add("enctype", "multipart/form-data");
    }

    protected void btnInviteTrainer_Click(object sender, EventArgs e)
    {
        string _userIP = DataHelper.GetIPAddress();
        string _userID = DataHelper.GetCurrentUserCIM();
        double _duration = 0;

        DateTime startTime;
        DateTime endTime;

        try
        {
            if (txtInviteTrainer.Text == string.Empty)
            {
                RadWindowManager1.RadAlert("Please specify Trainer CIM Number.", 330, 180, "System Message", "");
                return;
            }
            else if (!DataHelper.isCIMValid(txtInviteTrainer.Text.Trim()))
            {
                RadWindowManager1.RadAlert("The CIM you entered is not valid or the user is not a enrolled in Transcom University", 330, 180, "System Message", "");
                return;
            }

            var courseId = Convert.ToInt32(Decrypt(Request.QueryString["CourseID"]));
            var delegator = DataHelper.GetCurrentUserCIM();

            var db = new TranscomUniversityV3ModelContainer();

            var dts = db.CreateQuery<DateTime>("CurrentDateTime() ");
            startTime = dts.AsEnumerable().First();

            pr_TranscomUniversity_InsertTrainer_Result retVal = db.pr_TranscomUniversity_InsertTrainer(Convert.ToInt32(courseId), Convert.ToInt32(txtInviteTrainer.Text.Trim()),0, Convert.ToInt32(delegator)).SingleOrDefault();

            if (retVal.Msg == "0")
            {
                string _action = "Added Trainer " + DataHelper.getUserName(Convert.ToInt32(txtInviteTrainer.Text.Trim()));
                var dte = db.CreateQuery<DateTime>("CurrentDateTime() ");

                endTime = dte.AsEnumerable().First();
                _duration = (endTime - startTime).TotalMilliseconds;

                int l_actionID = DataHelper.getLastLogID() + 1;
                DataHelper.logCourse(courseId, l_actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);

                RadWindowManager1.RadAlert("Trainer successfully assigned.", 330, 180, "System Message", "");

                TrainersCBUserCtrl ctrl = (TrainersCBUserCtrl)(this.Parent.Parent).FindControl("gtTrainers");
                RadGrid grid = (RadGrid)ctrl.FindControl("gridTrainers");

                txtInviteTrainer.Text = string.Empty;
                grid.Rebind();
                GetTrainersCount();
            }

            if (retVal.Msg == "1")
            {
                RadWindowManager1.RadAlert("Trainer already assigned in this course.", 330, 180, "System Message", "");

                txtInviteTrainer.Text = string.Empty;
            }
        }
        catch
        {
            RadWindowManager1.RadAlert("Error adding trainer. Please contact your System Administrator.", 330, 180, "Error Message", "");
        }
    }

    protected void btnViewTrainersList_Click(object sender, EventArgs e)
    {
        foreach (GridColumn column in gridViewTrainersList.MasterTableView.OwnerGrid.Columns)
        {
            column.CurrentFilterFunction = GridKnownFunction.NoFilter;
            column.CurrentFilterValue = string.Empty;
        }
        gridViewTrainersList.MasterTableView.FilterExpression = string.Empty;
        gridViewTrainersList.Rebind();

        RadScriptManager.RegisterStartupScript(Page, Page.GetType(), "viewTrainersList", DataHelper.callClientScript("viewTrainersList"), true);
    }

    protected void btnAssignMultipleTrainers_Click(object sender, EventArgs e)
    {
        try
        {
            string _userIP = DataHelper.GetIPAddress();
            string _userID = DataHelper.GetCurrentUserCIM();
            double _duration = 0;

            DateTime startTime;
            DateTime endTime;

            string stat = "";
            int successCount = 0;

            List<Transaction> TrainerList = new List<Transaction>();

            var db = new TranscomUniversityV3ModelContainer();

            string trainerList = hfSelectedTrainer.Value;
            string[] data = trainerList.Split(',');

            var courseId = Convert.ToInt32(Decrypt(Request.QueryString["CourseID"]));
            var delegator = DataHelper.GetCurrentUserCIM();

            if (data.Length > 1)
            {
                var dts = db.CreateQuery<DateTime>("CurrentDateTime() ");
                startTime = dts.AsEnumerable().First();

                for (int i = 0; i < (data.Length - 1); i++)
                {
                    int trainerCim = Convert.ToInt32(data[i]);

                    pr_TranscomUniversity_InsertMultipleTrainers_Result _res = db.pr_TranscomUniversity_InsertMultipleTrainers(Convert.ToInt32(trainerCim), Convert.ToInt32(courseId), Convert.ToInt32(delegator)).SingleOrDefault();

                    if (_res.Msg == "1")
                    {
                        stat = "Successful";
                        successCount++;
                    }
                    else if (_res.Msg == "2")
                    {
                        stat = "Duplicate";
                    }
                    else
                    {
                        stat = "Fail";
                    }

                    Transaction course = new Transaction
                    {
                        Id = Convert.ToInt32(data[i]),
                        Name = DataHelper.getUserName(trainerCim),
                        Status = stat
                    };

                    TrainerList.Add(course);
                }

                TrainersCBUserCtrl ctrl = (TrainersCBUserCtrl)(this.Parent.Parent).FindControl("gtTrainers");
                RadGrid grid = (RadGrid)ctrl.FindControl("gridTrainers");

                gridViewTrainersList.Rebind();
                grid.DataSource = DataHelper.GetAllTrainers(courseId);
                grid.DataBind();
                GetTrainersCount();

                ctrgTransactionSummary.DataSource = TrainerList;
                ctrgTransactionSummary.DataBind();

                string _action = "Added " + successCount + " Trainer ";
                var dte = db.CreateQuery<DateTime>("CurrentDateTime() ");

                endTime = dte.AsEnumerable().First();
                _duration = (endTime - startTime).TotalMilliseconds;

                int l_actionID = DataHelper.getLastLogID() + 1;
                DataHelper.logCourse(courseId, l_actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);

                RadScriptManager.RegisterStartupScript(Page, Page.GetType(), "ctshowTransaction", Utils.callClientScript("ctshowTransaction"), true);
            }
            else
            {
                RadWindowManager1.RadAlert("Please select atleast one(1) Trainer.", 330, 180, "System Message", "");
            }
        }
        catch
        {
            RadWindowManager1.RadAlert("Error on assigning multiple trainer(s). Please contact your System Administrator.", 330, 180, "Error Message", "");
        }
    }

    protected void btnUploadBulkTrainers_Click(object sender, EventArgs e)
    {
        string _userIP = DataHelper.GetIPAddress();
        string _userID = DataHelper.GetCurrentUserCIM();
        double _duration = 0;

        DateTime startTime;
        DateTime endTime;

        try
        {
           
            if (upTrainerList.UploadedFiles.Count == 0 || upTrainerList.UploadedFiles[0].GetExtension() != ".csv")
            {
                upTrainerList.UploadedFiles.Clear();
                RadWindowManager1.RadAlert("Please select a .CSV file to upload.", 330, 180, "System Message", "");
                return;
            }

            UploadedFile f = upTrainerList.UploadedFiles[0];
            var db = new TranscomUniversityV3ModelContainer();
            var dts = db.CreateQuery<DateTime>("CurrentDateTime() ");
            startTime = dts.AsEnumerable().First();

            string csvPath = Server.MapPath("~/Files/BulkTrainers/") + f.FileName;
            f.SaveAs(csvPath);

            string csvData = File.ReadAllText(csvPath);
            List<Transaction> TrainerList = new List<Transaction>();
            int ui = 0;
            int successCount = 0;
            int CourseID = Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"]));


            foreach (string row in csvData.Split('\n'))
            {
                string stat = "";
                if (ui == 0 && !string.IsNullOrEmpty(row))
                {
                    string[] filecolum = row.Split(',');
                    if (filecolum[0].Trim().ToLower() != "trainercim" || filecolum[1].Trim().ToLower() != "assignstate" || filecolum[2].Trim().ToLower() != "approvedenyby")
                    {
                        RadWindowManager1.RadAlert("CSV File does not contain correct Columns. Please select another.", 330, 180, "System Message", "");
                        return;
                    }
                }
                else if (!string.IsNullOrEmpty(row) && ui > 0)
                {
                    string[] cells = row.Split(',');

                    int TrainerCIM = Convert.ToInt32(cells[0]);
                    int AssignState = Convert.ToInt32(cells[1]);
                    int ApproveDenyBy = Convert.ToInt32(cells[2]);

                    pr_TranscomUniversity_InsertTrainer_Result retVal = db.pr_TranscomUniversity_InsertTrainer(CourseID, TrainerCIM, AssignState, ApproveDenyBy).SingleOrDefault();
                    if (retVal.Msg == "0")
                    {
                        stat = "Successful";
                        successCount++;
                    }
                    else if (retVal.Msg == "1")
                    {
                        stat = "Duplicate";
                    }
                    else
                    {
                        stat = "Fail";
                    }

                    Transaction user = new Transaction
                    {
                        Id = Convert.ToInt32(ui),
                        Name = DataHelper.getUserName(TrainerCIM),
                        Status = stat
                    };

                    TrainerList.Add(user);
                }
                ui++;
            }

            if (successCount > 0)
            {
                TrainersCBUserCtrl ctrl = (TrainersCBUserCtrl)(this.Parent.Parent).FindControl("gtTrainers");
                RadGrid grid = (RadGrid)ctrl.FindControl("gridTrainers");

                gridViewTrainersList.Rebind();
                grid.DataSource = DataHelper.GetAllTrainers(CourseID);
                grid.DataBind();
                GetTrainersCount();

                string _action = "Uploaded " + f.FileName + " Trainer List";
                var dte = db.CreateQuery<DateTime>("CurrentDateTime() ");

                endTime = dte.AsEnumerable().First();
                _duration = (endTime - startTime).TotalMilliseconds;

                int l_actionID = DataHelper.getLastLogID() + 1;
                DataHelper.logCourse(CourseID, l_actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);
                fakeFilenameTrainer.Text = "";
            }

            ctrgTransactionSummary.DataSource = TrainerList;
            ctrgTransactionSummary.DataBind();
            RadScriptManager.RegisterStartupScript(Page, Page.GetType(), "ctshowTransaction1", Utils.callClientScript("ctshowTransaction"), true);

        }
        catch (ArgumentException ex)
        {
            RadWindowManager1.RadAlert("Invalid file. The file does not match the required columns.", 330, 180, "Error Message", "");
        }
        catch (Exception ex)
        {
            RadWindowManager1.RadAlert("Error uploading trainer. Please notify Administrator. " + ex.Message, 330, 180, "Error Message", "");
        }
    }

    protected string Decrypt(string cipherText)
    {
        string EncryptionKey = "TRNSCMV32017111";
        cipherText = cipherText.Replace(" ", "+");
        byte[] cipherBytes = Convert.FromBase64String(cipherText);

        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(cipherBytes, 0, cipherBytes.Length);
                    cs.Close();
                }
                cipherText = Encoding.Unicode.GetString(ms.ToArray());
            }
        }
        return cipherText;
    }

    public void GetTrainersCount()
    {
        var db = new TranscomUniversityV3ModelContainer();
        var courseId = Convert.ToInt32(DataHelper.Decrypt(Request.QueryString["CourseID"]));

        var countTrainers = (from t in db.vw_TranscomUniversity_CourseTrainers
                             where t.CourseID == courseId
                             select t).Count().ToString();

        TrainersCBUserCtrl ctrl = (TrainersCBUserCtrl)(this.Parent.Parent).FindControl("gtTrainers");
        Literal ltTrainers = (Literal)ctrl.FindControl("ltTrainers");

        ltTrainers.Text = countTrainers;
    }

    protected void rgTransactionSummary_OnItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        //Is it a GridDataItem
        if (e.Item is GridDataItem)
        {
            //Get the instance of the right type
            GridDataItem dataBoundItem = e.Item as GridDataItem;

            //Check the formatting condition
            if (dataBoundItem["Status"].Text == "Duplicate")
            {
                dataBoundItem["Status"].ForeColor = Color.DarkGray;
            }
            else if (dataBoundItem["Status"].Text == "Fail")
            {
                dataBoundItem["Status"].ForeColor = Color.Red;
            }
            else
            {
                dataBoundItem["Status"].ForeColor = Color.YellowGreen;
            }
        }
    }   
}