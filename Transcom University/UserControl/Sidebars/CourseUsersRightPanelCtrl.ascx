﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CourseUsersRightPanelCtrl.ascx.cs"
    Inherits="CourseUsersRightPanelCtrl" %>
<%@ Register TagPrefix="AddUsersCBUserCtrlV3" TagName="UsersCBUserCtrlV3" Src="~/UserControl/Pages/UsersCBUserCtrl.ascx" %>
<rad:RadScriptBlock ID="RadScriptBlock1" runat="server">
    <script type="text/javascript">
        function keyPress(sender, args) {
            var text = sender.get_value() + args.get_keyCharacter();
            if (!text.match('^[0-9]+$'))
                args.set_cancel(true);
        }

        function viewUsersList() {
//            try {
//                debugger;
//                var CIMNumber = $('#ctl00_contentPlaceHolderMain_gtUsersPanel_rwViewUsersList_C_gridViewUsersList_ctl00_ctl02_ctl02_FilterTextBox_CIMNumber').val();
//                var TextBox_Name = $('#ctl00_contentPlaceHolderMain_gtUsersPanel_rwViewUsersList_C_gridViewUsersList_ctl00_ctl02_ctl02_FilterTextBox_Name').val();

//                $('#ctl00_contentPlaceHolderMain_gtUsersPanel_rwViewUsersList_C_gridViewUsersList_ctl00_ctl02_ctl02_FilterTextBox_CIMNumber').val('');
//                $('#ctl00_contentPlaceHolderMain_gtUsersPanel_rwViewUsersList_C_gridViewUsersList_ctl00_ctl02_ctl02_FilterTextBox_Name').val('');

//                if (CIMNumber != "" || CIMNumber == null) {
//                    $find("ctl00_contentPlaceHolderMain_gtUsersPanel_rwViewUsersList_C_gridViewUsersList_ctl00")._filterNoDelay("ctl00_contentPlaceHolderMain_gtUsersPanel_rwViewUsersList_C_gridViewUsersList_ctl00_ctl02_ctl02_FilterTextBox_CIMNumber", "CIMNumber");
//                }
//                if (TextBox_Name != "" || TextBox_Name == null) {
//                    $find("ctl00_contentPlaceHolderMain_gtUsersPanel_rwViewUsersList_C_gridViewUsersList_ctl00")._filterNoDelay("ctl00_contentPlaceHolderMain_gtUsersPanel_rwViewUsersList_C_gridViewUsersList_ctl00_ctl02_ctl02_FilterTextBox_Name", "Name");
//                }
//            }
//            catch (err) { }
//            finally {
//                var radwindow = $find('<%=rwViewUsersList.ClientID %>');
//                radwindow.show();
//            }

            var radwindow = $find('<%=rwViewUsersList.ClientID %>');
            radwindow.show();
        }

        var selectedUserItems = [];

        function userRowSelected(sender, args) {
            //debugger;
            var id = args.getDataKeyValue("CIMNumber");

            if (!selectedUserItems.includes(id)) {
                selectedUserItems.push(id);
            }
            setSelectedItems();
        }

        function userRowDeselected(sender, args) {
            var id = args.getDataKeyValue("CIMNumber");

            if (selectedUserItems.includes(id)) {
                selectedUserItems.splice(selectedUserItems.indexOf(id), 1);
            }
            setSelectedItems();
        }

        function userRowCreated(sender, args) {
            //debugger;
            var id = args.getDataKeyValue("CIMNumber");
            if (selectedUserItems.includes(id) && selectedUserItems.length > 0) {
                args.get_gridDataItem().set_selected(true);
            }
        }

        function setSelectedItems() {
            //debugger;
            var elements = selectedUserItems;
            var outputStr = "";
            for (var i = 0; i < elements.length; i++) {
                outputStr += elements[i] + ",";
            }
            $("#" + "<%= hfSelectedUser.ClientID %>").val(outputStr);
        }

        function clearSelectedItems() {
            selectedUserItems = [];
            var grid = $find("<%= gridViewUsersList.ClientID %>");
            grid.clearSelectedItems();
        }
        function urshowTransaction() {
            $find("<%= rwViewUsersList.ClientID %>").close();
            var radwindow = $find('<%=urrdTransactionSummary.ClientID %>');
            radwindow.show();
        }

        function urbtnTransactionsList_Click() {
            var radwindow = $find('<%=urrdTransactionSummary.ClientID %>');
            radwindow.close();
        }

        function openFileWindow() {
            //$("#fakeFilenameUser").html("");
            $find("<%= upUserList.ClientID %>").deleteAllFileInputs();
            $telerik.$(".userUploader .ruFileInput").click();
        }

        function fileSelectedUser(sender, args) {
            $("#fakeFilenameUser").html(args.get_fileName());
            $("#fakeFilenameUser").attr('title', args.get_fileName());
            var fileExtention = args.get_fileName().substring(args.get_fileName().lastIndexOf('.') + 1, args.get_fileName().length);
            if (args.get_fileName().lastIndexOf('.') != -1) {//this checks if the extension is correct
                if (sender.get_allowedFileExtensions().toLowerCase().indexOf(fileExtention.toLowerCase()) == -1) {
                    radalert("Invalid File Type.<br> Allowed Files is .csv", 330, 180, $('#<%= hfError.ClientID %>').val(), "");
                    sender.deleteAllFileInputs();
                }
                else {
                    console.log("File Supported.");
                }
            }
            else {
                sender.deleteAllFileInputs();
                radalert("Invalid File Type.<br> Allowed Files is .csv", 330, 180, $('#<%= hfError.ClientID %>').val(), "");
            }
        }
    </script>
</rad:RadScriptBlock>
<rad:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <rad:AjaxSetting AjaxControlID="btnEnrollUser">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="RadMultiPage2" />
                <rad:AjaxUpdatedControl ControlID="ulBreadcrumb" />
                <rad:AjaxUpdatedControl ControlID="btnEnrollUser" />
                <rad:AjaxUpdatedControl ControlID="gridApprovedEnrollee" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <%--   <rad:AjaxSetting AjaxControlID="btnViewUsersList">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="btnViewUsersList" />
            </UpdatedControls>
        </rad:AjaxSetting>--%>
        <rad:AjaxSetting AjaxControlID="gridViewUsersList">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="gridViewUsersList" LoadingPanelID="RadAjaxLoadingPanel2" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnAssignMultipleUsers">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="pnlViewUsersList" LoadingPanelID="RadAjaxLoadingPanel2" />
                <rad:AjaxUpdatedControl ControlID="ltApprovedEnrollee" UpdatePanelRenderMode="Inline" />
                <rad:AjaxUpdatedControl ControlID="gridApprovedEnrollee" />
                <rad:AjaxUpdatedControl ControlID="urrgTransactionSummary" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnUploadBulkUsers">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="btnUploadBulkUsers" />
                <rad:AjaxUpdatedControl ControlID="fakeFilenameUser" />
                <rad:AjaxUpdatedControl ControlID="upUserList" LoadingPanelID="RadAjaxLoadingPanel1" />
                  <rad:AjaxUpdatedControl ControlID="ltApprovedEnrollee" UpdatePanelRenderMode="Inline" />
                 <rad:AjaxUpdatedControl ControlID="gridApprovedEnrollee" />
                <rad:AjaxUpdatedControl ControlID="urrgTransactionSummary" />
            </UpdatedControls>
        </rad:AjaxSetting>
        <rad:AjaxSetting AjaxControlID="btnViewUsersList">
            <UpdatedControls>
                <rad:AjaxUpdatedControl ControlID="btnViewUsersList" />
                <rad:AjaxUpdatedControl ControlID="gridViewUsersList" LoadingPanelID="RadAjaxLoadingPanel2" />
            </UpdatedControls>
        </rad:AjaxSetting>
    </AjaxSettings>
</rad:RadAjaxManagerProxy>
<rad:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true" RenderMode="Lightweight"
    Skin="Bootstrap" Modal="true" VisibleStatusbar="false">
    <Windows>
        <rad:RadWindow ID="rwViewUsersList" runat="server" RenderMode="Lightweight" Modal="true"
            Skin="Bootstrap" Behaviors="Move, Close, Resize" Title="<%$ Resources:LocalizedResource, SelectMultipleUsers%>"
            CssClass="tc-radwindow-1 height-inherit" Height="600px" Width="950px" OnClientBeforeShow="clearSelectedItems"
            VisibleStatusbar="false">
            <ContentTemplate>
                <asp:Panel ID="pnlViewUsersList" runat="server">
                    <rad:RadGrid ID="gridViewUsersList" runat="server" RenderMode="Lightweight" AutoGenerateColumns="false"
                        PageSize="10" DataSourceID="dsViewUsersList" CssClass="GridLess" AllowMultiRowSelection="true"
                        AllowFilteringByColumn="true">
                        <GroupingSettings CaseSensitive="false" />
                        <PagerStyle Mode="NextPrev" AlwaysVisible="true" />
                        <ClientSettings EnableAlternatingItems="true">
                            <Selecting AllowRowSelect="true"></Selecting>
                            <Resizing AllowColumnResize="true" ResizeGridOnColumnResize="true" AllowResizeToFit="true" />
                            <ClientEvents OnRowSelected="userRowSelected" OnRowDeselected="userRowDeselected"
                                OnRowCreated="userRowCreated" />
                        </ClientSettings>
                        <ItemStyle Wrap="false"></ItemStyle>
                        <MasterTableView DataKeyNames="CIMNumber" ClientDataKeyNames="CIMNumber" AllowSorting="true"
                            AllowPaging="true" CommandItemDisplay="None" HeaderStyle-ForeColor="Teal" AllowFilteringByColumn="true">
                            <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                            <Columns>
                                <rad:GridClientSelectColumn UniqueName="chkMultipleUsers" HeaderText="<%$ Resources:LocalizedResource, Select%>">
                                </rad:GridClientSelectColumn>
                                <rad:GridBoundColumn UniqueName="CIMNumber" HeaderText="<%$ Resources:LocalizedResource, CimNumber%>" DataField="CIMNumber"
                                    AllowFiltering="true" DataType="System.String" FilterControlWidth="120px" CurrentFilterFunction="Contains"
                                    ShowFilterIcon="false" AutoPostBackOnFilter="True">
                                </rad:GridBoundColumn>
                                <rad:GridBoundColumn UniqueName="Name" HeaderText="<%$ Resources:LocalizedResource, Name%>" DataField="Name" AllowFiltering="true"
                                    FilterControlWidth="250px" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                                    AutoPostBackOnFilter="True">
                                </rad:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                    </rad:RadGrid>
                    <asp:SqlDataSource ID="dsViewUsersList" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
                        SelectCommand="pr_TranscomUniversity_AllEmployees" SelectCommandType="StoredProcedure" />
                    <br />
                    <div style="margin-left: 75%;">
                        <asp:Button ID="btnAssignMultipleUsers" runat="server" OnClick="btnAssignMultipleUsers_Click"
                            Text="<%$ Resources:LocalizedResource, AssignUsersToThisCourse%>" CssClass="btn btn-md btn-teal button-flat" />
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </rad:RadWindow>
        <rad:RadWindow RenderMode="Lightweight" ID="urrdTransactionSummary" Behaviors="Close,Move"
            VisibleOnPageLoad="false" runat="server" CssClass="tc-radwindow-1" Height="330px"
            Title="<%$ Resources:LocalizedResource, ResultSummary%>">
            <ContentTemplate>
                <asp:Panel ID="pnlTransactionList" runat="server">
                    <rad:RadGrid RenderMode="Lightweight" ID="urrgTransactionSummary" runat="server"
                        AllowSorting="True" CssClass="GridLess" AllowMultiRowSelection="true" AllowPaging="true"
                        OnItemDataBound="rgTransactionSummary_OnItemDataBound">
                        <MasterTableView AutoGenerateColumns="false" TableLayout="Auto" HeaderStyle-ForeColor="Teal"
                            DataKeyNames="Id" ClientDataKeyNames="Id">
                            <Columns>
                                <rad:GridTemplateColumn HeaderText="#">
                                    <ItemTemplate>
                                        <%#Container.ItemIndex+1 %></ItemTemplate>
                                </rad:GridTemplateColumn>
                                <rad:GridBoundColumn DataField="Name" HeaderText="<%$ Resources:LocalizedResource, Name%>" UniqueName="Name">
                                </rad:GridBoundColumn>
                                <rad:GridBoundColumn DataField="Status" HeaderText="<%$ Resources:LocalizedResource, Status%>" UniqueName="Status">
                                </rad:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                    </rad:RadGrid>
                </asp:Panel>
                <button id="urbtnTransactionsList" onclick="urbtnTransactionsList_Click(); return false;"
                    runat="server" title="Close" class="btn tc-btn-md btn-teal button-flat bottom-center">
                    <asp:Label runat="server" ID="label1" Text=" <%$ Resources:LocalizedResource, Ok%>" ></asp:Label></button>
                <%-- <asp:LinkButton ID="gtbtnTransactionsList" OnClientClick="gtbtnTransactionsList_Click(); return false;"
                    runat="server" Text="Close" ToolTip="Close" CssClass="btn tc-btn-md btn-teal button-flat bottom-center"></asp:LinkButton>--%>
            </ContentTemplate>
        </rad:RadWindow>
    </Windows>
</rad:RadWindowManager>
<div id="pnlUsers" class="groupSidebarContainer bg-color-light-gray" style="height: 175vh">
    <div class="row" style="text-align: center; font-size: inherit;">
        <h5 style="font-weight: bold;">
            <asp:Label runat="server" ID="label2" Text=" <%$ Resources:LocalizedResource, EnrollIndividualUsers%>" ></asp:Label>
        </h5>
        <br />
        <div class="input-group">
            <rad:RadTextBox ID="txtEnrollUser" runat="server" Skin="Bootstrap" MaxLength="10"
                EmptyMessage="<%$ Resources:LocalizedResource, TypeCim%>" Width="200px">
                <ClientEvents OnKeyPress="keyPress" />
            </rad:RadTextBox>
            <div class="input-group-btn">
                <asp:LinkButton ID="btnEnrollUser" runat="server" OnClick="btnEnrollUser_Click" Font-Underline="false">
                    <i id="i1" runat="server" class="icon fa fa-plus-square" style="color: Black; font-size: 25px;">
                    </i>
                </asp:LinkButton>
            </div>
        </div>
        <br />
        <h5 style="font-weight: bold;">
            or
        </h5>
        <br />
        <h5 style="font-weight: bold;">
            <asp:Label runat="server" ID="label3" Text=" <%$ Resources:LocalizedResource, SelectMultipleUsers%>" ></asp:Label>
        </h5>
        <br />
        <asp:LinkButton ID="btnViewUsersList" runat="server" Text="View List" OnClick="btnViewUsersList_Click" CssClass="btn btn-md btn-teal button-flat"></asp:LinkButton>
        <br />
        <h5 style="font-weight: bold;">
            <asp:Label runat="server" ID="label4" Text=" <%$ Resources:LocalizedResource, Or%>" ></asp:Label>
        </h5>
        <br />
        <h5 style="font-weight: bold;">
            <asp:Label runat="server" ID="label5" Text=" <%$ Resources:LocalizedResource, UploadBulkUsers%>" ></asp:Label>
        </h5>
        <br />
        <%--  <asp:FileUpload ID="fuUsers" runat="server" ToolTip="Upload Bulk Users" CssClass="btn btn-default btn-file"
            Width="220px" />
        <rad:RadAsyncUpload ID="RadAsyncUpload1" runat="server" InitialFileInputsCount="1"
            MaxFileInputsCount="1" CssClass="noProgressBar" Localization-Select="Select File"
            ViewStateMode="Enabled" ControlObjectsVisibility="None" AllowedFileExtensions=".csv" />--%>
        <div class="uploader-container inline-flex">
         <asp:Button ID="Button1" Text="<%$ Resources:LocalizedResource, ChooseFile%>" runat="server" OnClientClick="openFileWindow(); return false;"
            Height="25px" CssClass="pull-left" />

            <asp:Label ID="fakeFilenameUser" Text="" runat="server" ClientIDMode="Static" style=" float: right;text-overflow: ellipsis;overflow: hidden;width: 60%;"  />
        &nbsp;&nbsp;&nbsp;
        <rad:RadAsyncUpload ID="upUserList" runat="server" MultipleFileSelection="Disabled"
            ViewStateMode="Enabled" MaxFileInputsCount="1" ControlObjectsVisibility="None"
            Width="100%" ToolTip="Select File to Upload" OnClientFileSelected="fileSelectedUser"
            AllowedFileExtensions=".csv" CssClass="userUploader display-none">
        </rad:RadAsyncUpload>
        </div>
       
        <br />
        <asp:LinkButton ID="btnUploadBulkUsers" runat="server" Text="<%$ Resources:LocalizedResource, UploadBulkUsers%>" OnClick="btnUploadBulkUsers_Click"
            CssClass="btn btn-md btn-teal button-flat"></asp:LinkButton>
    </div>
</div>
<asp:HiddenField ID="hfSelectedUser" runat="server" />
   <asp:HiddenField runat="server" ID="hfError" Value="<%$ Resources:LocalizedResource, Error%>" />
