﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TranscomUniversityV3Model;
using System.Security.Cryptography;
using System.IO;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using Telerik.Web.UI;
using System.Drawing;
using TransactionNames;
using System.Web.UI.WebControls.WebParts;

public partial class CourseUsersRightPanelCtrl : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Form.Attributes.Add("enctype", "multipart/form-data");
    }

    protected void btnEnrollUser_Click(object sender, EventArgs e)
    {
        string _userIP = DataHelper.GetIPAddress();
        string _userID = DataHelper.GetCurrentUserCIM();
        double _duration = 0;

        DateTime startTime;
        DateTime endTime;

        try
        {

            if (txtEnrollUser.Text == string.Empty)
            {
                RadWindowManager1.RadAlert("Please specify Enrollee CIM Number.", 330, 180, "System Message", "");
                return;
            }
            else if (!DataHelper.isCIMValid(txtEnrollUser.Text.Trim()))
            {
                RadWindowManager1.RadAlert("The CIM you entered is not valid or the user is not a enrolled in Transcom University", 330, 180, "System Message", "");
                return;
            }

            int enrolleeCIM = Convert.ToInt32(txtEnrollUser.Text.Trim());
            var courseId = Convert.ToInt32(Decrypt(Request.QueryString["CourseID"]));
            var delegator = DataHelper.GetCurrentUserCIM();

            var db = new TranscomUniversityV3ModelContainer();


            var dts = db.CreateQuery<DateTime>("CurrentDateTime() ");
            startTime = dts.AsEnumerable().First();


            pr_TranscomUniversity_InsertEnrollee_Result retVal = db.pr_TranscomUniversity_InsertEnrollee(enrolleeCIM, Convert.ToInt32(courseId), Convert.ToInt32(delegator)).SingleOrDefault();

            if (retVal.Msg == "0")
            {
                db.pr_TranscomUniversity_InsertEnrollee(Convert.ToInt32(txtEnrollUser.Text.Trim()), Convert.ToInt32(courseId), Convert.ToInt32(delegator));


                string _action = "Added User " + DataHelper.getUserName(Convert.ToInt32(txtEnrollUser.Text.Trim()));
                var dte = db.CreateQuery<DateTime>("CurrentDateTime() ");

                endTime = dte.AsEnumerable().First();
                _duration = (endTime - startTime).TotalMilliseconds;

                int l_actionID = DataHelper.getLastLogID() + 1;
                DataHelper.logCourse(courseId, l_actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);

                RadWindowManager1.RadAlert("User successfully enrolled.", 330, 180, "System Message", "");

                UsersCBUserCtrl ctrl = (UsersCBUserCtrl)(this.Parent.Parent).FindControl("gtUsers");
                RadGrid grid = (RadGrid)ctrl.FindControl("gridApprovedEnrollee");
                txtEnrollUser.Text = string.Empty;
                //.Rebind();
                GetUsersCount();

                //binding the gridview
                var approvedEnrollee = DataHelper.GetApprovedEnrollee(courseId);
                if (approvedEnrollee != null)
                {
                    grid.DataSource = approvedEnrollee;
                    grid.DataBind();
                }

            }

            if (retVal.Msg == "1")
            {
                RadWindowManager1.RadAlert("User already enrolled in this course.", 330, 180, "System Message", "");

                txtEnrollUser.Text = string.Empty;
            }
        }
        catch
        {
            RadWindowManager1.RadAlert("Error adding enrollee. Please contact your System Administrator.", 330, 180, "Error Message", "");
        }
    }

    protected void btnViewUsersList_Click(object sender, EventArgs e)
    {
        foreach (GridColumn column in gridViewUsersList.MasterTableView.OwnerGrid.Columns)
        {
            column.CurrentFilterFunction = GridKnownFunction.NoFilter;
            column.CurrentFilterValue = string.Empty;
        }
        gridViewUsersList.MasterTableView.FilterExpression = string.Empty;
        gridViewUsersList.Rebind();

        RadScriptManager.RegisterStartupScript(Page, typeof(Page), "key", DataHelper.callClientScript("viewUsersList"), true);
    }

    protected void btnAssignMultipleUsers_Click(object sender, EventArgs e)
    {
        try
        {
            string _userIP = DataHelper.GetIPAddress();
            string _userID = DataHelper.GetCurrentUserCIM();
            double _duration = 0;

            DateTime startTime;
            DateTime endTime;

            string stat = "";
            int successCount = 0;

            List<Transaction> UserList = new List<Transaction>();

            var db = new TranscomUniversityV3ModelContainer();

            string userList = hfSelectedUser.Value;
            string[] data = userList.Split(',');

            var courseId = Convert.ToInt32(Decrypt(Request.QueryString["CourseID"]));
            var delegator = DataHelper.GetCurrentUserCIM();
            int enrolleeCim = -1;

            if (data.Length > 1)
            {
                var dts = db.CreateQuery<DateTime>("CurrentDateTime() ");
                startTime = dts.AsEnumerable().First();

                for (int i = 0; i < (data.Length - 1); i++)
                {
                    enrolleeCim = Convert.ToInt32(data[i]);

                    pr_TranscomUniversity_InsertEnrollmentUser_Result _res = db.pr_TranscomUniversity_InsertEnrollmentUser(Convert.ToInt32(enrolleeCim), Convert.ToInt32(courseId), Convert.ToInt32(delegator)).SingleOrDefault();

                    if (_res.Msg == "1")
                    {
                        stat = "Successful";
                        successCount++;
                    }
                    else if (_res.Msg == "2")
                    {
                        stat = "Duplicate";
                    }
                    else
                    {
                        stat = "Fail";
                    }

                    Transaction course = new Transaction
                    {
                        Id = Convert.ToInt32(data[i]),
                        Name = DataHelper.getUserName(enrolleeCim),
                        Status = stat
                    };

                    UserList.Add(course);
                }

                if (successCount > 0)
                {
                    string _action = successCount > 1 ? "Added " + successCount + " Users" : "Added " + DataHelper.getUserName(enrolleeCim);
                    var dte = db.CreateQuery<DateTime>("CurrentDateTime() ");

                    endTime = dte.AsEnumerable().First();
                    _duration = (endTime - startTime).TotalMilliseconds;

                    int l_actionID = DataHelper.getLastLogID() + 1;
                    DataHelper.logCourse(courseId, l_actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);
                }

                UsersCBUserCtrl ctrl = (UsersCBUserCtrl)(this.Parent.Parent).FindControl("gtUsers");
                RadGrid grid = (RadGrid)ctrl.FindControl("gridApprovedEnrollee");

                gridViewUsersList.Rebind();

                grid.DataSource = DataHelper.GetApprovedEnrollee(courseId);
                grid.DataBind();
                GetUsersCount();

                urrgTransactionSummary.DataSource = UserList;
                urrgTransactionSummary.DataBind();

                RadScriptManager.RegisterStartupScript(Page, Page.GetType(), "urshowTransaction1", Utils.callClientScript("urshowTransaction"), true);
            }
            else
            {
                RadWindowManager1.RadAlert("Please select atleast one(1) User.", 330, 180, "System Message", "");
            }
        }
        catch
        {
            RadWindowManager1.RadAlert("Error assigning to another course(s). Please contact your System Administrator.", 330, 180, "Error Message", "");
        }
    }

    protected void btnUploadBulkUsers_Click(object sender, EventArgs e)
    {
        try
        {

            if (upUserList.UploadedFiles.Count == 0 || upUserList.UploadedFiles[0].GetExtension() != ".csv")
            {
                upUserList.UploadedFiles.Clear();
                RadWindowManager1.RadAlert("Please select a .CSV file to upload.", 330, 180, "System Message", "");
                return;
            }

            //declare logger variable
            string _userIP = DataHelper.GetIPAddress();
            string _userID = DataHelper.GetCurrentUserCIM();
            double _duration = 0;
            DateTime startTime;
            DateTime endTime;
            var db = new TranscomUniversityV3ModelContainer();
            var dts = db.CreateQuery<DateTime>("CurrentDateTime() ");
            startTime = dts.AsEnumerable().First();

            //save excel to server
            UploadedFile f = upUserList.UploadedFiles[0];
            string csvPath = Server.MapPath("~/Files/BulkUsers/") + f.FileName;
            f.SaveAs(csvPath);

            List<Transaction> UserList = new List<Transaction>();
            int ui = 0;
            int successCount = 0;
            string csvData = File.ReadAllText(csvPath);
            int CourseID = Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"]));

            foreach (string row in csvData.Split('\n'))
            {
                string stat = "";
                if (ui == 0 && !string.IsNullOrEmpty(row))
                {
                    string[] filecolum = row.Split(',');
                    if (filecolum[0].Trim().ToLower() != "enrolleecim" || filecolum[1].Trim().ToLower() != "approvedenyby")
                    {
                        RadWindowManager1.RadAlert("CSV File does not contain correct Columns. Please select another.", 330, 180, "System Message", "");
                        return;
                    }
                }
                else if (!string.IsNullOrEmpty(row) && ui > 0)
                {
                    string[] cells = row.Split(',');
                   
                    int EnrolleeCIM = Convert.ToInt32(cells[0]);
                    int ApproveDenyBy = Convert.ToInt32(cells[1]);

                    pr_TranscomUniversity_InsertEnrollmentUser_Result _res = db.pr_TranscomUniversity_InsertEnrollmentUser(EnrolleeCIM, CourseID, ApproveDenyBy).SingleOrDefault();

                    if (_res.Msg == "1")
                    {
                        stat = "Successful";
                        successCount++;
                    }
                    else if (_res.Msg == "2")
                    {
                        stat = "Duplicate";
                    }
                    else
                    {
                        stat = "Fail";
                    }


                    Transaction user = new Transaction
                    {
                        Id = Convert.ToInt32(ui),
                        Name = DataHelper.getUserName(EnrolleeCIM),
                        Status = stat
                    };

                    UserList.Add(user);

                }
                ui++;
            }

            if (successCount > 0)
            {
                //rebind list
                UsersCBUserCtrl ctrl = (UsersCBUserCtrl)(this.Parent.Parent).FindControl("gtUsers");
                RadGrid grid = (RadGrid)ctrl.FindControl("gridApprovedEnrollee");

                gridViewUsersList.Rebind();

                grid.DataSource = DataHelper.GetApprovedEnrollee(CourseID);
                grid.DataBind();
                GetUsersCount();

                //log action
                string _action = "Uploaded " + f.FileName + " User List";
                var dte = db.CreateQuery<DateTime>("CurrentDateTime()");
                endTime = dte.AsEnumerable().First();
                _duration = (endTime - startTime).TotalMilliseconds;
                int l_actionID = DataHelper.getLastLogID() + 1;
                DataHelper.logCourse(CourseID, l_actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);

                //reset filename
                fakeFilenameUser.Text = "";
            }
           

            urrgTransactionSummary.DataSource = UserList;
            urrgTransactionSummary.DataBind();

            RadScriptManager.RegisterStartupScript(Page, Page.GetType(), "urshowTransaction", Utils.callClientScript("urshowTransaction"), true);

            //string consString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            //using (SqlConnection con = new SqlConnection(consString))
            //{
            //    using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
            //    {
            //        sqlBulkCopy.DestinationTableName = "dbo.tbl_TranscomUniversity_Cor_CourseEnrollment";
            //        sqlBulkCopy.ColumnMappings.Add("CourseID", "CourseID");
            //        sqlBulkCopy.ColumnMappings.Add("EnrolleeCIM", "EnrolleeCIM");
            //        sqlBulkCopy.ColumnMappings.Add("SupervisorCIM", "SupervisorCIM");
            //        sqlBulkCopy.ColumnMappings.Add("EnrollmentStatus", "EnrollmentStatus");
            //        sqlBulkCopy.ColumnMappings.Add("ApproveDenyBy", "ApproveDenyBy");

            //        con.Open();
            //        sqlBulkCopy.WriteToServer(dt);

            //        con.Close();

            //        UsersCBUserCtrl ctrl = (UsersCBUserCtrl)(this.Parent.Parent).FindControl("gtUsers");
            //        RadGrid grid = (RadGrid)ctrl.FindControl("gridApprovedEnrollee");

            //        grid.Rebind();
            //        GetUsersCount();

            //        string _action = "Uploaded " + f.FileName + " User List";
            //        var dte = db.CreateQuery<DateTime>("CurrentDateTime()");

            //        endTime = dte.AsEnumerable().First();
            //        _duration = (endTime - startTime).TotalMilliseconds;

            //        int l_actionID = DataHelper.getLastLogID() + 1;
            //        DataHelper.logCourse(courseId, l_actionID, Convert.ToInt32(_duration), _userID, _action, _userIP);
            //        fakeFilenameUser.Text = "";
            //        RadWindowManager1.RadAlert("User(s) successfully enrolled.", 330, 180, "System Message", "");
            //    }
            //}
        }
        catch (ArgumentException ex)
        {
            RadWindowManager1.RadAlert("Invalid file. The file does not match the required columns.", 330, 180, "Error Message", "");
        }
        catch (Exception ex)
        {
            RadWindowManager1.RadAlert("Error uploading user. Please notify Administrator. ", 330, 180, "Error Message", "");
        }
    }

    protected string Decrypt(string cipherText)
    {
        string EncryptionKey = "TRNSCMV32017111";
        cipherText = cipherText.Replace(" ", "+");
        byte[] cipherBytes = Convert.FromBase64String(cipherText);

        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(cipherBytes, 0, cipherBytes.Length);
                    cs.Close();
                }
                cipherText = Encoding.Unicode.GetString(ms.ToArray());
            }
        }
        return cipherText;
    }

    public void GetUsersCount()
    {
        var db = new TranscomUniversityV3ModelContainer();
        var courseId = Convert.ToInt32(DataHelper.Decrypt(Request.QueryString["CourseID"]));

        var countPendingEnrollee = (from c in db.vw_TranscomUniversity_PendingEnrollments
                                    where c.CourseID == courseId
                                    select c).Count().ToString();

        var countApprovedEnrollee = (from c in db.vw_TranscomUniversity_ApprovedEnrollments
                                     where c.CourseID == courseId
                                     select c).Count().ToString();

        UsersCBUserCtrl ctrl = (UsersCBUserCtrl)(this.Parent.Parent).FindControl("gtUsers");
        Literal ltPendingEnrollee = (Literal)ctrl.FindControl("ltPendingEnrollee");
        Literal ltApprovedEnrollee = (Literal)ctrl.FindControl("ltApprovedEnrollee");

        ltPendingEnrollee.Text = countPendingEnrollee;
        ltApprovedEnrollee.Text = countApprovedEnrollee;
    }

    protected void rgTransactionSummary_OnItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            GridDataItem dataBoundItem = e.Item as GridDataItem;

            if (dataBoundItem["Status"].Text == "Duplicate")
            {
                dataBoundItem["Status"].ForeColor = Color.DarkGray;
            }
            else if (dataBoundItem["Status"].Text == "Fail")
            {
                dataBoundItem["Status"].ForeColor = Color.Red;
            }
            else
            {
                dataBoundItem["Status"].ForeColor = Color.YellowGreen;
            }
        }

    }

}
