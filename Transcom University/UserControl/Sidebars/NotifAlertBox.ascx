﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NotifAlertBox.ascx.cs" Inherits="NotifAlertBox" %>

<h5 class="color-white" style="padding: 5px;">
    <asp:Label runat="server" ID="label1" Text=" <%$ Resources:LocalizedResource, AlertBoxFeatureFor%>" ></asp:Label></h5> 
<table style="color: #fff; font-size: 13px;">
    <tr>
        <td colspan="2" style="padding: 5px">
            <asp:CheckBox ID="ChkEnrolRequest" runat="server" />
            <asp:Label runat="server" ID="label2" Text=" <%$ Resources:LocalizedResource, EnrollmentRequests%>" ></asp:Label>
        </td>
    </tr>
    <tr>
        <td style="padding: 5px">
            &nbsp;&nbsp;&nbsp;&nbsp;
        </td>
        <td>
            <asp:CheckBox ID="ChkTrainers" runat="server" />
            <asp:Label runat="server" ID="label3" Text=" <%$ Resources:LocalizedResource, Trainers%>" ></asp:Label>
        </td>
    </tr>
    <tr>
        <td style="padding: 5px">
            &nbsp;&nbsp;&nbsp;&nbsp;
        </td>
        <td>
            <asp:CheckBox ID="ChkSupervisors" runat="server" />
            <asp:Label runat="server" ID="label4" Text=" <%$ Resources:LocalizedResource, Supervisor%>" ></asp:Label>
        </td>
    </tr>
    <tr valign="middle">
        <td style="padding: 5px">
            &nbsp;&nbsp;&nbsp;&nbsp;
        </td>
        <td>
            <asp:CheckBox ID="ChkManagers" runat="server" />
            <asp:Label runat="server" ID="label5" Text=" <%$ Resources:LocalizedResource, Managers%>" ></asp:Label>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="padding: 5px">
            <asp:CheckBox ID="ChkCourseAvailability" runat="server" />
            <asp:Label runat="server" ID="label6" Text=" <%$ Resources:LocalizedResource, CourseAvailability%>" ></asp:Label>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="padding: 5px">
            <asp:CheckBox ID="ChkAssigments" runat="server" />
           <asp:Label runat="server" ID="label7" Text=" <%$ Resources:LocalizedResource, ClassAssignments%>" ></asp:Label>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="padding: 5px">
            <asp:CheckBox ID="ChkOverdue" runat="server" />
            <asp:Label runat="server" ID="label8" Text=" <%$ Resources:LocalizedResource, OverdueCourses%>" ></asp:Label>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="padding: 5px">
            <asp:CheckBox ID="ChkForum" runat="server" />
            <asp:Label runat="server" ID="label9" Text=" <%$ Resources:LocalizedResource, Forums%>" ></asp:Label>
        </td>
    </tr>
</table>
