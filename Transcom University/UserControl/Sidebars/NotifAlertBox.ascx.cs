﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class NotifAlertBox : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        foreach (Control control in this.Controls)
        {
            if (control is CheckBox)
            {
                ((CheckBox)control).InputAttributes["class"] = "js-switch2";
            }
        }
    }
}