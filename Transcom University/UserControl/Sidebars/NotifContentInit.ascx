﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NotifContentInit.ascx.cs"
    Inherits="NotifContentInit" %>
<div id="pnlContent" class="contentSidebarContainer courseSidebarContainer content-tab bg-color-dark-gray"
    style="text-align: center; font-size: inherit;">
    <div class="row" style="height: inherit;">
        <div class="col-md-3 tabButtonGroup">
            <div class="tablinks" id="btnCourseUpload">
                <button id="btnUpload" runat="server" title="Upload" onclick="openSideContent(event, 'ctUpload', this); return false">
                    <img src="Media/Images/upload-icon.png" alt="Upload" />
                    <asp:Label runat="server" ID="label1" Text=" <%$ Resources:LocalizedResource, Upload%>" ></asp:Label>
                </button>
            </div>
            <div class="tablinks" id="btnCourseCreate">
                <button id="btnCreate" runat="server" title="Create" onclick="openSideContent(event, 'ctCreate', this); return false">
                    <img src="Media/Images/create-icon.png" alt="Create" />
                    <asp:Label runat="server" ID="label2" Text=" <%$ Resources:LocalizedResource, Create%>" ></asp:Label>
                </button>
            </div>
            <div class="tablinks" id="btnCourseSearch">
                <button id="btnBrowse" runat="server" title="Browse" onclick="openSideContent(event, 'ctSearch', this); return false">
                    <img src="Media/Images/search-icon.png" alt="Search" />
                    <asp:Label runat="server" ID="label3" Text=" <%$ Resources:LocalizedResource, Browse%>" ></asp:Label>
                </button>
            </div>
        </div>
        <div class="col-md-9" style="padding: 0 !important; width: calc(75% - 15px);">
            <div class="contentDetails">
                <div id="ctIntro" class="tabcontent active" style="display: block;">
                    <p style="padding-top: 5em; color: #fff;">
                        Select a Course Builder icon</p>
                </div>
            </div>
        </div>
    </div>
</div>
