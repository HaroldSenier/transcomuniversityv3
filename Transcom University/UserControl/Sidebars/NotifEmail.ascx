﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NotifEmail.ascx.cs" Inherits="NotifEmail" %>

<h5 class="color-white" style="padding: 5px;">
   <asp:Label runat="server" ID="emailfor" Text= "<%$ Resources:LocalizedResource, EmailFeatureFor %>"></asp:Label> </h5> 
<table style="color: #fff; font-size: 13px;">
    <tr>
        <td colspan="2" style="padding: 5px">
            <asp:CheckBox ID="ChkEnrolRequest" runat="server" />
            <asp:Label runat="server" ID="Ereqs" Text= "<%$ Resources:LocalizedResource, EnrollmentRequests %>"></asp:Label>
        </td>
    </tr>
    <tr>
        <td style="padding: 5px">
            &nbsp;&nbsp;&nbsp;&nbsp;
        </td>
        <td>
            <asp:CheckBox ID="ChkTrainers" runat="server" />
            <asp:Label runat="server" ID="traines" Text= "<%$ Resources:LocalizedResource, Trainers %>"></asp:Label>
        </td>
    </tr>
    <tr>
        <td style="padding: 5px">
            &nbsp;&nbsp;&nbsp;&nbsp;
        </td>
        <td>
            <asp:CheckBox ID="ChkSupervisors" runat="server" />
            <asp:Label runat="server" ID="supss" Text= "<%$ Resources:LocalizedResource, Supervisor %>"></asp:Label>
        </td>
    </tr>
    <tr valign="middle">
        <td style="padding: 5px">
            &nbsp;&nbsp;&nbsp;&nbsp;
        </td>
        <td>
            <asp:CheckBox ID="ChkManagers" runat="server" />
            <asp:Label runat="server" ID="manss" Text= "<%$ Resources:LocalizedResource, Managers %>"></asp:Label>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="padding: 5px">
            <asp:CheckBox ID="ChkCourseAvailability" runat="server" />
           <asp:Label runat="server" ID="Label1" Text= "<%$ Resources:LocalizedResource, CourseTitle %>"></asp:Label> Course Availability
        </td>
    </tr>
    <tr>
        <td colspan="2" style="padding: 5px">
            <asp:CheckBox ID="ChkAssigments" runat="server" />
           <asp:Label runat="server" ID="Cas" Text= "<%$ Resources:LocalizedResource, ClassAssignments %>"></asp:Label> 
        </td>
    </tr>
    <tr>
        <td colspan="2" style="padding: 5px">
            <asp:CheckBox ID="ChkOverdue" runat="server" />
           <asp:Label runat="server" ID="OCours" Text= "<%$ Resources:LocalizedResource, OverdueCourses %>"></asp:Label> 
        </td>
    </tr>
    <tr>
        <td colspan="2" style="padding: 5px">
            <asp:CheckBox ID="ChkForum" runat="server" />
           <asp:Label runat="server" ID="forsm" Text= "<%$ Resources:LocalizedResource, Forums %>"></asp:Label> 
        </td>
    </tr>
</table>
