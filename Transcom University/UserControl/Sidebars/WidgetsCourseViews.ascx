﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WidgetsCourseViews.ascx.cs" Inherits="WidgetsCourseViews" %>

    <h5 class="color-white" style="padding: 7px;">
   <asp:Label runat="server" ID="viewcoursett" Text= "<%$ Resources:LocalizedResource, CourseViewSetting %>"></asp:Label> :</h5> 
<table style="color: #fff; font-size: 13px;">
    <tr>
        <td colspan="2" style="padding: 7px">
            <asp:CheckBox ID="ChkEnrolRequest" runat="server" />
           <asp:Label runat="server" ID="adwidg" Text= "<%$ Resources:LocalizedResource, AddWidget %>"></asp:Label> 
        </td>
    </tr>
</table>
<table style="color: #fff; font-size: 13px;">
    <tr>
        <td style="padding: 7px">
            <asp:CheckBox ID="CheckBox1" runat="server" />
        </td>
        <td>
          <asp:Label runat="server" ID="Clickimagecourse" Text= "<%$ Resources:LocalizedResource, ClickingTheCourseTitleOrImage %>"></asp:Label> 
        </td>
    </tr>
    <tr>
        <td style="padding: 7px">
            <asp:CheckBox ID="CheckBox2" runat="server" />
        </td>
        <td>
           <asp:Label runat="server" ID="visitiduniq" Text= "<%$ Resources:LocalizedResource, CountOfUniqueIdsOnlyRegardlessOfVisits %>"></asp:Label> 
        </td>
    </tr>
    <tr>
        <td style="padding: 7px">
            <asp:CheckBox ID="CheckBox3" runat="server" />
        </td>
        <td>
           <asp:Label runat="server" ID="visitregards" Text= "<%$ Resources:LocalizedResource, CountOfTheVisitsRegardlessOfTheIds %>"></asp:Label> 
        </td>
    </tr>
</table>
