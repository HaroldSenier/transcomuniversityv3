﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WidgetsForum.ascx.cs" Inherits="WidgetsForum" %>

<h5 class="color-white" style="padding: 5px;">
   <asp:Label runat="server" ID="forset" Text= "<%$ Resources:LocalizedResource, ForumSetting %>"></asp:Label> :</h5>


    <input type="hidden" id="hdnCourseID" value="<% Response.Write(Request.QueryString["CourseID"]); %>" /> 
<table style="color: #fff; font-size: 13px;">
    <tr>
        <td colspan="2" style="padding: 5px">
            <asp:CheckBox ID="ChkAddWidget" runat="server" ClientIDMode="Static" />
            <asp:Label runat="server" ID="addwig" Text= "<%$ Resources:LocalizedResource, AddWidget %>"></asp:Label>
        </td>
    </tr>
    <tr>
        <td style="padding: 5px">
            &nbsp;&nbsp;&nbsp;&nbsp;
        </td>
        <td>
            <asp:CheckBox ID="ChkAllUsers" runat="server" />
           <asp:Label runat="server" ID="alusers" Text= "<%$ Resources:LocalizedResource, AllUsersCanParticipate %>"></asp:Label> 
        </td>
    </tr>
    <tr>
        <td style="padding: 5px">
            &nbsp;&nbsp;&nbsp;&nbsp;
        </td>
        <td>
            <asp:CheckBox ID="ChkEnrolledOnly" runat="server" />
          <asp:Label runat="server" ID="enusers" Text= "<%$ Resources:LocalizedResource, EnrolledUsersOnly %>"></asp:Label>  
        </td>
    </tr>
    <tr>
        <td colspan="2" style="padding: 5px">
            <asp:CheckBox ID="ChkAllowFileUpload" runat="server" />
           <asp:Label runat="server" ID="allowfiles" Text= "<%$ Resources:LocalizedResource, AllowFileUpload %>"></asp:Label> 
        </td>
    </tr>
    <tr>
        <td style="padding: 5px">
            &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
        </td>
        <td>
            <a href="#" class="btn btn-sm btn-teal text-center"><asp:Label runat="server" ID="viewaccepts" Text= "<%$ Resources:LocalizedResource, ViewAcceptedFileTypes %>"></asp:Label></a> <br />

        </td>
    </tr>
    <tr>
        <td>
            &nbsp;&nbsp;&nbsp;&nbsp;
        </td>
        <td style="padding: 5px">
            <select class="form-control input-sm">
                <option>20 MB</option>
            </select>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;&nbsp;&nbsp;&nbsp;
        </td>
        <td>
        <small><asp:Label runat="server" ID="setsizemax" Text= "<%$ Resources:LocalizedResource, SetMaxUploadSize %>"></asp:Label></small>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;&nbsp;&nbsp;&nbsp;
        </td>
        <td style="padding: 5px">
            <select class="form-control input-sm">
                <option>20 MB</option>
            </select>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;&nbsp;&nbsp;&nbsp;
        </td>
        <td>
        <small><asp:Label runat="server" ID="charmax" Text= "<%$ Resources:LocalizedResource, SetMaxCharacters %>"></asp:Label></small>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="padding: 5px">
            <asp:CheckBox ID="CheckBox1" runat="server" />
            <asp:Label runat="server" ID="joinmulti" Text= "<%$ Resources:LocalizedResource, CanJoinMultipleForums %>"></asp:Label>
        </td>
    </tr>
     <tr>
        <td colspan="2" style="padding: 5px">
            <asp:CheckBox ID="CheckBox2" runat="server" />
            <asp:Label runat="server" ID="limitclasspart" Text= "<%$ Resources:LocalizedResource, LimitToParticipantsClass %>"></asp:Label>
        </td>
    </tr>
</table>
<rad:RadScriptBlock ID="radScript" runat="server">
<script type="text/javascript">
    Sys.Application.add_load(function () {
        $(document).on("change", $('#<%=ChkAddWidget.ClientID%>'), function () {
            //alert("CheckBox Changed.");

            var isChecked = $("#<%=ChkAddWidget.ClientID%>").is(":checked");

            var bitVal;
            if (isChecked) {
                bitVal = 1;
            } else {
                bitVal = 0;
            }

            var courseID = $("#hdnCourseID").val();

            $.ajax({
                type: "POST",
                data: "{ CourseID: '" + courseID + "', BitVal: '" + bitVal + "'}",
                contentType: "application/json; charset=utf-8",
                url: ($(location).attr('hostname') == "localhost" ? "../ApiService.asmx/" : "/TranscomUniversityV3/ApiService.asmx/") + "SaveCourseSetting",
                dataType: 'json',
                success: function (msg) {

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(XMLHttpRequest.responseText);
                }
            });
            //}
        });
    });
</script>

</rad:RadScriptBlock>