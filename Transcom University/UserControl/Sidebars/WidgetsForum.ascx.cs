﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;


public partial class WidgetsForum : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DataSet ds = DataHelper.GetCourseSetting(Convert.ToInt32(Utils.Decrypt(Request.QueryString["CourseID"])));

        if (ds.Tables[0].Rows.Count > 0)
        {
            ChkAddWidget.Checked = Convert.ToInt32(ds.Tables[0].Rows[0]["ForumWidget"]) == 1 ? true : false;
        }

        foreach (Control control in this.Controls)
        {
            if (control is CheckBox)
            {
                ((CheckBox)control).InputAttributes["class"] = "js-switch2";
            }
        }
    }
}