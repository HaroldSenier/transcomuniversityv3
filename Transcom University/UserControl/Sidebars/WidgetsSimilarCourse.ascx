﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WidgetsSimilarCourse.ascx.cs" Inherits="WidgetsSimilarCourse" %>

<h5 class="color-white" style="padding: 7px;">
    <asp:Label runat="server" ID="similarset" Text= "<%$ Resources:LocalizedResource, SimilarCoursesSetting %>"></asp:Label>:</h5> 
<table style="color: #fff; font-size: 13px;">
    <tr>
        <td colspan="2" style="padding: 7px">
            <asp:CheckBox ID="ChkEnrolRequest" runat="server" />
            <asp:Label runat="server" ID="addwigs" Text= "<%$ Resources:LocalizedResource, AddWidget %>"></asp:Label>
        </td>
    </tr>
    
</table>
<table style="color: #fff; font-size: 13px;">
    <tr>
        <td style="padding: 7px">
            <asp:CheckBox ID="CheckBox1" runat="server" />
        </td>
        <td>
            <asp:Label runat="server" ID="undersamecat" Text= "<%$ Resources:LocalizedResource, CoursesUnderTheSameCategories %>"></asp:Label>
        </td>
    </tr>
    <tr>
        <td style="padding: 7px">
            <asp:CheckBox ID="CheckBox2" runat="server" />
        </td>
        <td>
            <asp:Label runat="server" ID="CoursesunderthesameSubcategories" Text= "<%$ Resources:LocalizedResource, CoursesunderthesameSubcategories %>"></asp:Label>
        </td>
    </tr>
    <tr>
        <td style="padding: 7px">
            <asp:CheckBox ID="CheckBox3" runat="server" />
        </td>
        <td>
            <asp:Label runat="server" ID="coursesame" Text= "<%$ Resources:LocalizedResource, CoursesUnderTheSameAuthor %>"></asp:Label>
        </td>
    </tr>
    <tr>
        <td style="padding: 7px">
            <asp:CheckBox ID="CheckBox4" runat="server" />
        </td>
        <td>
            <asp:Label runat="server" ID="samedept" Text= "<%$ Resources:LocalizedResource, CoursesUnderTheSameDepartment %>"></asp:Label>
        </td>
    </tr>
    <tr>
        <td style="padding: 7px">
            <asp:CheckBox ID="CheckBox5" runat="server" />
        </td>
        <td>
            <asp:Label runat="server" ID="BundlesCourses" Text= "<%$ Resources:LocalizedResource, BundlesCourses %>"></asp:Label>
        </td>
    </tr>
    <tr>
        <td style="padding: 7px">
            <asp:CheckBox ID="CheckBox6" runat="server" />
        </td>
        <td>
            <asp:Label runat="server" ID="sameunderprog" Text= "<%$ Resources:LocalizedResource, CoursesUnderTheSameProgram %>"></asp:Label>
        </td>
    </tr>
    <tr>
        <td style="padding: 7px">
            <asp:CheckBox ID="CheckBox7" runat="server" />
        </td>
        <td>
            <asp:Label runat="server" ID="samecatcateg" Text= "<%$ Resources:LocalizedResource, CoursesUnderTheSameCatalogueCategory %>"></asp:Label>
        </td>
    </tr>
    <tr>
        <td style="padding: 7px">
            <asp:CheckBox ID="CheckBox8" runat="server" />
        </td>
        <td>
            <asp:Label runat="server" ID="CoursesUnderTheSameCourseClassType" Text= "<%$ Resources:LocalizedResource, CoursesUnderTheSameCourseClassType %>"></asp:Label>
        </td>
    </tr>
    <tr>
        <td style="padding: 7px">
            <asp:CheckBox ID="CheckBox9" runat="server" />
        </td>
        <td>
           <asp:Label runat="server" ID="sameaverelview" Text= "<%$ Resources:LocalizedResource, SameAveragedRatingRealtimeView %>"></asp:Label> 
        </td>
    </tr>
    
</table>