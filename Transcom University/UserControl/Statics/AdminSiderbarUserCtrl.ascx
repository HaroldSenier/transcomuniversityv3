﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AdminSiderbarUserCtrl.ascx.cs"
    Inherits="WebUserControl1" %>
<rad:RadScriptBlock ID="RadScriptBlock" runat="server">
    <script type="text/javascript">
        function highlightSelectedSubmenu() {

            var tab = $(".breadcrumb li:last-child a span").html().replace(' ', '');
            var selectedTab = "#tab" + tab;
            $(selectedTab).addClass("active");
            if (!$(selectedTab).hasClass("submenu-title")) {
                if ($(selectedTab).parent().parent().find("a.submenu-title").length == 0)
                    $(selectedTab).parent().parent().parent().parent().parent().addClass("rtsSelected");
                else
                    $(selectedTab).parent().parent().find("a.submenu-title").addClass("active");
            }
        }

        function tabClick(el) {

            var caret = $(el).find("i.js-collapse-event-2");

            if ($(caret).hasClass("fa-sort-up")) {
                $(caret).removeClass("fa-sort-up").addClass("fa-sort-down");
            } else {
                $(caret).removeClass("fa-sort-down").addClass("fa-sort-up");
            }

        }
    </script>
</rad:RadScriptBlock>
<div class="sidebar-relative">
    <div class="side-menu-container">
        <div class="overlay">
        </div>
        <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
                <ul class="nav sidebar-nav">
                    <li class="sidebar-brand" style="z-index: 1000; color: #fff;">
                        <a href="#" class="close"><i class="fa fa-times" aria-hidden="true"></i></a> <asp:Label runat="server" Text="<%$ Resources: LocalizedResource, AdminTaskMenu %>" />
                    </li>
                    <rad:RadTabStrip ID="RadTabStrip1_Admin" runat="server" RenderMode="Lightweight" Skin="Black"><%--OnClientTabSelected="onClientTabSelected"--%>
                        <Tabs>
                            <rad:RadTab runat="server" Text="<%$ Resources: LocalizedResource, CourseManagement %>" Value="1" NavigateUrl="~/Admin.aspx?Tab=CourseMgmt" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                            <rad:RadTab runat="server" Text="<%$ Resources: LocalizedResource, CreateACourse %>" Value="2" NavigateUrl="~/CreateCourse.aspx?" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                            <rad:RadTab runat="server" Text="<%$ Resources: LocalizedResource, CourseCategories %>" Value="3" NavigateUrl="~/Admin.aspx?Tab=CourseCat" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                            <rad:RadTab runat="server" Text="<%$ Resources: LocalizedResource, ClassManagement %>" Value="4" NavigateUrl="~/ClassManagement.aspx" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                            <rad:RadTab runat="server" Text="<%$ Resources: LocalizedResource, CreateAClass %>" Value="5"  NavigateUrl="~/ClassBuilder.aspx" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                            <rad:RadTab runat="server" Text="<%$ Resources: LocalizedResource, PageEditor %>" Value="6"  Width="285px" SelectedCssClass="rtsSelected" CssClass="toggle-submenu"  PostBack="false">
                                 <TabTemplate>
                                  <ul class="nav custom-submenu panel-title" id="menuPageEditor">
		                            <li>
                                        <a class="tab-title" ><asp:Label ID="Label1" runat="server" Text="<%$ Resources: LocalizedResource, PageEditor %>" />
                                         <i class="fa fa-sort-down pull-right hover-pointer js-collapse-event-2" data-toggle="collapse" data-target="#pageEditorMenu" aria-expanded="false" onclick="tabClick(this);"></i>
                                        </a>
			                            <ul class="nav collapse submenu" id="pageEditorMenu" role="menu">
				                            <li id="tabCourseLogins">
                                                <a id="tabHomepageEditor" href="~/Admin.aspx?Tab=HomepageEditor" class="submenu-title"><asp:Label ID="Label2" runat="server" Text="<%$ Resources: LocalizedResource, HomepageLayout %>" />
                                                <i class="fa fa-sort-down pull-right hover-pointer js-collapse-event-2"  data-toggle="collapse" data-target="#homepageMenu" aria-expanded="false" onclick="tabClick(this);"></i>
                                                </a>
                                                 
                                                <ul class="nav collapse submenu" id="homepageMenu" role="menu" >
				                                    <li id="tabImageSlider"><a href="~/Admin.aspx?Tab=ImageSlider"><asp:Label ID="Label3" runat="server" Text="<%$ Resources: LocalizedResource, ImageSlider %>" /></a></li>
                                                    <li id="tabFeaturedCourses"><a href="~/Admin.aspx?Tab=FeaturedCourses"><asp:Label ID="Label4" runat="server" Text="<%$ Resources: LocalizedResource, FeaturedCourses %>" /></a></li>
                                                    <li id="tabLatestNews"><a href="~/Admin.aspx?Tab=LatestNews"><asp:Label ID="Label5" runat="server" Text="<%$ Resources: LocalizedResource, LatestNews %>" /></a></li>
                                                    <li id="tabTestimonials"><a href="~/Admin.aspx?Tab=Testimonials"><asp:Label ID="Label6" runat="server" Text="<%$ Resources: LocalizedResource, Testimonials %>" /></a></li>
			                                    </ul>
                                            </li>
				                             <li id="Li5">
                                                <a class="submenu-title" data-toggle="collapse" data-target="#courseCatalogMenu" aria-expanded="false" onclick="tabClick(this);"><asp:Label ID="Label7" runat="server" Text="<%$ Resources: LocalizedResource, CourseCatalog %>" />
                                                <i class="fa fa-sort-down pull-right hover-pointer js-collapse-event-2"></i>
                                                </a>
                                                <ul class="nav collapse submenu" id="courseCatalogMenu" role="menu" >
				                                    <li id="tabMandatoryCourses"><a href="~/Admin.aspx?Tab=MandatoryCourses"><asp:Label ID="Label8" runat="server" Text="<%$ Resources: LocalizedResource, MandatoryCourses %>" /></a></li>
                                                    <li id="tabTrendingCourses"><a href="~/Admin.aspx?Tab=TrendingCourses"><asp:Label ID="Label9" runat="server" Text="<%$ Resources: LocalizedResource, TrendingCourses %>" /></a></li>
                                                    <li id="tabRecommendedCourses"><a href="~/Admin.aspx?Tab=RecommendedCourses"><asp:Label ID="Label10" runat="server" Text="<%$ Resources: LocalizedResource, RecommendedCourses %>" /></a></li>
                                                    <li id="tabMustTakeCourses"><a href="~/Admin.aspx?Tab=MustTakeCourses"><asp:Label ID="Label11" runat="server" Text="<%$ Resources: LocalizedResource, MustTakeCourses %>" /></a></li>
                                                    <li id="tabRecentCourses"><a href="~/Admin.aspx?Tab=RecentCourses"><asp:Label ID="Label12" runat="server" Text="<%$ Resources: LocalizedResource, RecentlyAddedCourses %>" /></a></li>
			                                    </ul>
                                            </li>
			                            </ul>
		                            </li>
	                            </ul>
                                </TabTemplate>
                            </rad:RadTab>
                            <rad:RadTab runat="server" Text="<%$ Resources: LocalizedResource, UserManagement %>" Value="7" NavigateUrl="~/Admin.aspx?Tab=UserMgmt" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                            <rad:RadTab runat="server" Text="<%$ Resources: LocalizedResource, Forums %>" Value="8" NavigateUrl="~/Admin.aspx?Tab=AdminForum" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                        </Tabs>
                    </rad:RadTabStrip>
                </ul>
            </nav>
        <div class="hamburger-container text-center" style="font-size: 20px; color: #fff;
            margin-top: 10px">
            <a href="#" class="open-nav is-closed animated fadeInLeft"><i class="fa fa-bars"
                aria-hidden="true"></i></a>
        </div>
    </div>
</div>
