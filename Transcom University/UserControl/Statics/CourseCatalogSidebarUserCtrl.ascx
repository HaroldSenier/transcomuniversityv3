﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CourseCatalogSidebarUserCtrl.ascx.cs"
    Inherits="UserControl_Statics_CourseCatalogSidebarUserCtrl" %>
<div class="sidebar-relative">
    <div class="side-menu-container">
        <div class="overlay">
        </div>
        <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
            <ul class="nav sidebar-nav">
                <li class="sidebar-brand" style="z-index: 1000; color: #fff;">
                    <a href="#" class="close"><i class="fa fa-times" aria-hidden="true"></i></a> <asp:Label runat="server" Text="<%$ Resources: LocalizedResource, CourseCatalog %>" />
                </li>
                <rad:RadTabStrip ID="rtsCourseCatalog" runat="server" RenderMode="Lightweight" Skin="Black" AutoPostBack="true">
                    <Tabs>
                    </Tabs>
                </rad:RadTabStrip>
            </ul>
        </nav>
        <div class="hamburger-container text-center" style="font-size: 20px; color: #fff;
            margin-top: 10px">
            <a href="#" class="open-nav is-closed animated fadeInLeft hambuger-menubar"><i class="fa fa-bars"
                aria-hidden="true"></i></a>
        </div>
    </div>
</div>
