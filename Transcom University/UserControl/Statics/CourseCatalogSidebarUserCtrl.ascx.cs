﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using TranscomUniversityV3Model;

public partial class UserControl_Statics_CourseCatalogSidebarUserCtrl : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

        var db = new TranscomUniversityV3ModelContainer();

        var categories = db.tbl_TranscomUniversity_Lkp_Category.Where(c => c.HideFromList == false).ToList();
        var tab = Request.QueryString["Tab"];
        foreach (var category in categories)
        {
            RadTab categoryTab = new RadTab();
            string cat = category.Category;
            string encryptedId = Utils.Encrypt(category.CategoryID);
            categoryTab.NavigateUrl = "~/CourseCatalog.aspx?Tab=" + cat + "&Id=" + encryptedId;
            categoryTab.Text = cat;
            categoryTab.CssClass = tab == cat ? "rtsSelected" : "";

            rtsCourseCatalog.Tabs.Add(categoryTab);
        }


    }
}