﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LearnerSidebarUserCtrl.ascx.cs" Inherits="LearnerSidebarUserCtrl" %>

<div class="col-sm-1 side-menu-container">
    <div class="overlay">
    </div>
    <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
					<ul class="nav sidebar-nav">
						<li class="sidebar-brand" style="z-index: 1000; color: #fff;">
							<a href="#" class="close"><i class="fa fa-times" aria-hidden="true"></i></a>
							
							    My Profile
						</li>
						<li class="active">
							<a href="#"><asp:Label runat="server" Text="<%$ Resources: LocalizedResource, MyProfileSummary %>" /></a>
						</li>
						<li>
							<a href="#"><asp:Label ID="Label1" runat="server" Text="<%$ Resources: LocalizedResource, MyCertifications %>" /></a>
						</li>
                        <li>
							<a href="#"><asp:Label ID="Label2" runat="server" Text="<%$ Resources: LocalizedResource, MyCareerMap %>" /></a>
						</li>
						<!--<li class="dropdown">
						  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-fw fa-plus"></i> Dropdown <span class="caret"></span></a>
						  <ul class="dropdown-menu" role="menu">
							<li class="dropdown-header">Dropdown heading</li>
							<li><a href="#">Action</a></li>
							<li><a href="#">Another action</a></li>
						  </ul>
						</li>-->
					</ul>
				</nav>
    <!-- /#sidebar-wrapper -->
    <div class="hamburger-container text-center" style="font-size: 30px; color: #fff;">
        <a href="#" class="open-nav is-closed animated fadeInLeft"><i class="fa fa-bars"
            aria-hidden="true"></i></a>
    </div>
</div>
