﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TrainerSidebarUserCtrl.ascx.cs"
    Inherits="UserControl_Statics_TrainerSidebarUserCtrl" %>
<rad:RadScriptBlock ID="RadScriptBlock" runat="server">
    <script type="text/javascript">
        function highlightSelectedSubmenu() {

            var tab = $(".breadcrumb li:last-child a span").html().replace(' ', '');
            var selectedTab = "#tab" + tab;
            $(selectedTab).addClass("active");
            if (!$(selectedTab).hasClass("submenu-title")) {
                if ($(selectedTab).parent().parent().find("a.submenu-title").length == 0)
                    $(selectedTab).parent().parent().parent().parent().parent().addClass("rtsSelected");
                else
                    $(selectedTab).parent().parent().find("a.submenu-title").addClass("active");
            }
        }

        function tabClick(el) {

            var caret = $(el).find("i.js-collapse-event-2");

            if ($(caret).hasClass("fa-sort-up")) {
                $(caret).removeClass("fa-sort-up").addClass("fa-sort-down");
            } else {
                $(caret).removeClass("fa-sort-down").addClass("fa-sort-up");
            }

        }
    </script>
</rad:RadScriptBlock>
<div class="sidebar-relative">
    <div class="side-menu-container">
        <div class="overlay">
        </div>
        <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
                <ul class="nav sidebar-nav">
                    <li class="sidebar-brand" style="z-index: 1000; color: #fff;">
                        <a href="#" class="close"><i class="fa fa-times" aria-hidden="true"></i></a> <asp:Label runat="server" Text="<%$ Resources: LocalizedResource, TrainerDashboard %>" />
                    </li>
                    <rad:RadTabStrip ID="RadTabStrip1" runat="server" RenderMode="Lightweight" SelectedIndex="0" Skin="Black"><%--OnClientTabSelected="onClientTabSelected"--%>
                        <Tabs>
                            <rad:RadTab runat="server" Text="My Class" Value="1"  Width="285px" SelectedCssClass="rtsSelected" CssClass="toggle-submenu"  PostBack="false" Visible="false" >
                                 <TabTemplate>
                                  <ul class="nav custom-submenu panel-title" id="menuPageEditor">
		                            <li>
                                        <a class="tab-title" onclick="tabClick(this);" ><asp:Label ID="Label1" runat="server" Text="<%$ Resources: LocalizedResource, MyClass %>" />
                                         <i class="fa fa-sort-down pull-right hover-pointer js-collapse-event-2" data-toggle="collapse" data-target="#pageEditorMenu" aria-expanded="false"></i>
                                        </a>
			                            <ul class="nav collapse submenu" id="pageEditorMenu" role="menu">
				                            <li id="tabCourseLogins">
                                                <a id="tabHomepageEditor" href="Trainer.aspx?Tab=ClassMgmt" class="submenu-title"><asp:Label ID="Label2" runat="server" Text="<%$ Resources: LocalizedResource, ClassList %>" />
                                                </a>
                                            </li>
				                             <li id="Li5">
                                                <a class="submenu-title" data-toggle="collapse" data-target="#courseCatalogMenu" aria-expanded="false" onclick="tabClick(this);"><asp:Label ID="Label3" runat="server" Text="<%$ Resources: LocalizedResource, ClassCalendar %>" />
                                                </a>
                                            </li>
			                            </ul>
		                            </li>
	                            </ul>                                
                                </TabTemplate>
                            </rad:RadTab>
                            <%--<rad:RadTab runat="server" Text="Class Management" Value="2" NavigateUrl="Trainer.aspx?Tab=ClassMgmt" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>--%>
                            <rad:RadTab runat="server" Text="<%$ Resources: LocalizedResource, MyClass %>" Value="2" Width="285px" PostBack="false" CssClass="toggle-submenu" >
                                <TabTemplate>
                                  <ul class="nav custom-submenu panel-title" id="menuMyClass">
		                            <li>
                                        <a class="tab-title" data-toggle="collapse" data-target="#myClassMenu" aria-expanded="false" ><asp:Label ID="Label1" runat="server" Text="<%$ Resources: LocalizedResource, MyClass %>" />
                                         <i class="fa fa-sort-down pull-right hover-pointer js-collapse-event-2" onclick="tabClick(this);">
                                        </i>
                                        </a>
			                            <ul class="nav collapse submenu" id="myClassMenu" role="menu" aria-labelledby="customSubmenu">
				                            <li id="tabClassList"><a href="Trainer.aspx?Tab=ClassMgmt"><asp:Label ID="Label4" runat="server" Text="<%$ Resources: LocalizedResource, ClassList %>" /></a></li>
				                            <li id="tabClassCalendar"><a href="Trainer.aspx?Tab=ClassCalendar"><asp:Label ID="Label5" runat="server" Text="<%$ Resources: LocalizedResource, ClassCalendar %>" /></a></li>
			                            </ul>
		                            </li>
	                            </ul>
                                </TabTemplate>
                            </rad:RadTab>
                            <rad:RadTab runat="server" Text="<%$ Resources: LocalizedResource, ClassManagement %>" Value="6" NavigateUrl="~/ClassManagement.aspx?viewBy=Trainer" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                            <rad:RadTab runat="server" Text="<%$ Resources: LocalizedResource, CourseManagement %>" Value="3" NavigateUrl="~/Trainer.aspx?Tab=CourseMgmt" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                            
                            <rad:RadTab runat="server" Text="<%$ Resources: LocalizedResource, CourseCategories %>" Value="4" NavigateUrl="~/Trainer.aspx?Tab=CourseCat" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                            <rad:RadTab runat="server" Text="<%$ Resources: LocalizedResource, Forums %>" Value="5" NavigateUrl="~/Trainer.aspx?Tab=TrainerForum" Width="285px" SelectedCssClass="rtsSelected"></rad:RadTab>
                        </Tabs>
                    </rad:RadTabStrip>
                </ul>
                <div id="divCalendar" style="display: inline-block; position: relative; margin-top: 100%;" >
                    <rad:RadCalendar RenderMode="Lightweight" runat="server" ID="RadCalendar1" EnableMultiSelect="false"
                        DayNameFormat="FirstTwoLetters" EnableNavigation="true" Visible="false" 
                        EnableMonthYearFastNavigation="true" Skin="Metro" CssClass="Calendar1" >                            
                        <%--<ClientEvents OnDateSelected="OnCalendar1DateSelected" OnCalendarViewChanged="OnCalendar1ViewChanged" />--%>
                           <SpecialDays>
                                <rad:RadCalendarDay Repeatable="Today">
                                    <ItemStyle CssClass="rcToday" />
                                </rad:RadCalendarDay>
                            </SpecialDays>
                    </rad:RadCalendar>
                </div>
            </nav>
        <div class="hamburger-container text-center" style="font-size: 20px; color: #fff;
            margin-top: 10px">
            <a href="#" class="open-nav is-closed animated fadeInLeft"><i class="fa fa-bars"
                aria-hidden="true"></i></a>
        </div>
    </div>
</div>
