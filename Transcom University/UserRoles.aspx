﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TranscomUniversityV3.Master" AutoEventWireup="true" CodeFile="UserRoles.aspx.cs" Inherits="UserRoles" %>

<asp:Content ID="Content1" ContentPlaceHolderID="contentPlaceHolderLeftPanel" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <rad:RadWindowManager ID="radWindowManager1" runat="server" Modal="true" Width="800px" Height="600px" ReloadOnShow="false" DestroyOnClose="true" ShowContentDuringLoad="false" VisibleStatusbar="false" InitialBehaviors="Maximize">
    </rad:RadWindowManager>
    <rad:RadAjaxManager ID="ajaxManager1" runat="server" DefaultLoadingPanelID="LoadingPanel1">
        <AjaxSettings>
            <rad:AjaxSetting AjaxControlID="grdUsers">
                <UpdatedControls>
                    <rad:AjaxUpdatedControl ControlID="grdUsers" />
                </UpdatedControls>
            </rad:AjaxSetting>
        </AjaxSettings>
    </rad:RadAjaxManager>
    <rad:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server" Transparency="25" IsSticky="true" CssClass="Loading" />
    <rad:RadGrid ID="grdUsers" runat="server" OnNeedDataSource="grdUsers_NeedDataSource" AutoGenerateColumns="false" Skin="Bootstrap" AllowFilteringByColumn="true" Height="540px" OnItemDataBound="grdUsers_ItemDataBound" OnUpdateCommand="grdUsers_OnUpdateCommand">
        <PagerStyle Mode="NextPrevNumericAndAdvanced" AlwaysVisible="true" />
        <ClientSettings EnableAlternatingItems="true" AllowColumnsReorder="true">
            <Selecting AllowRowSelect="true"></Selecting>
            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
            <Resizing AllowColumnResize="true" ResizeGridOnColumnResize="true" />
        </ClientSettings>
        <GroupingSettings CaseSensitive="false" />
        <ExportSettings ExportOnlyData="true" IgnorePaging="true" FileName="Report">
        </ExportSettings>
        <ItemStyle Wrap="false"></ItemStyle>
        <MasterTableView DataKeyNames="UserId,UserName,RoleId" AllowSorting="true" EditMode="PopUp" AllowPaging="true" CommandItemDisplay="Top">
            <EditFormSettings>
                <PopUpSettings Modal="true" />
            </EditFormSettings>
            <CommandItemSettings ShowAddNewRecordButton="false" />
            <Columns>
                <rad:GridTemplateColumn UniqueName="IsAdmin" AllowFiltering="false" Exportable="false">
                    <%--<ItemTemplate>
                        <asp:LinkButton ID="btnShowEdit" runat="server" CommandName="Edit"><i class="fa fa-unlock" title="Modify Access" aria-hidden="true"></i></asp:LinkButton>
                    </ItemTemplate>--%>
                    <HeaderStyle Width="30px" />
                </rad:GridTemplateColumn>
                <rad:GridTemplateColumn UniqueName="ShowUserReports" AllowFiltering="false" Exportable="false" Visible="false">
                    <ItemTemplate>
                        <asp:LinkButton ID="btnShowUserReports" runat="server"><i class="fa fa-tasks" title="Show User Reports" aria-hidden="true"></i></asp:LinkButton>
                    </ItemTemplate>
                    <HeaderStyle Width="30px" />
                </rad:GridTemplateColumn>
                <rad:GridBoundColumn UniqueName="UserName" HeaderText="<%$ Resources: LocalizedResource, GoogleAccount %>" DataField="UserName" ReadOnly="true">
                </rad:GridBoundColumn>
                <rad:GridBoundColumn UniqueName="LastActivityDate" HeaderText="<%$ Resources: LocalizedResource, LastActivityDate %>" AllowFiltering="false" DataField="LastActivityDate" ReadOnly="true">
                </rad:GridBoundColumn>
                <rad:GridTemplateColumn HeaderText="<%$ Resources: LocalizedResource, Role %>" UniqueName="Role" AllowFiltering="false" Exportable="false">
                    <ItemTemplate>
                        <%# Eval("RoleName") %>
                    </ItemTemplate>
                    <%--<EditItemTemplate>
                        <rad:RadComboBox ID="cboRole" runat="server">
                            <Items>
                                <rad:RadComboBoxItem Text="" Value="" />
                                <rad:RadComboBoxItem Text="Admin" Value="63CC0CD2-E244-45C0-87ED-2475A796ACEE" />
                                <rad:RadComboBoxItem Text="Learner" Value="AC6C1230-5AA2-4D8E-8202-708CC4AB7D0F" />
                                <rad:RadComboBoxItem Text="Trainer" Value="BEA90A4B-0499-4DA9-8DAA-E68BC2E7C4D6" />
                            </Items>
                        </rad:RadComboBox>
                        <asp:RequiredFieldValidator ID="reqRole" runat="server" ControlToValidate="cboRole" ErrorMessage="*" Text="*">
                        </asp:RequiredFieldValidator>
                    </EditItemTemplate>--%>
                    <HeaderStyle Width="120px" />
                </rad:GridTemplateColumn>
                <%--<rad:GridTemplateColumn HeaderText="Inactive" UniqueName="Inactive" AllowFiltering="false" DataType="System.Boolean">
                    <ItemTemplate>
                        <%# Eval("HideFromList") != null ? (Convert.ToBoolean(Eval("HideFromList")) ? "Yes" : "No") : "No"  %>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:CheckBox ID="chkInactive" runat="server" />
                    </EditItemTemplate>
                </rad:GridTemplateColumn>--%>
            </Columns>
        </MasterTableView>
    </rad:RadGrid>
</asp:Content>