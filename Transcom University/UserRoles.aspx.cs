﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Web.Security;

public partial class UserRoles : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (!System.Web.Security.Roles.IsUserInRole(HttpContext.Current.User.Identity.Name.Split('|')[0], "Admin"))
                Response.Redirect("~/Unauthorized.aspx");

            grdUsers.Rebind();
        }
    }

    protected void grdUsers_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        if (e.RebindReason == Telerik.Web.UI.GridRebindReason.ExplicitRebind || e.RebindReason == Telerik.Web.UI.GridRebindReason.PostBackEvent)
        {
            var users = DataHelper.GetUsers();

            if (users != null)
            {
                grdUsers.DataSource = users;
            }
        }
    }

    protected void grdUsers_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            LinkButton btnShowUserReports = (LinkButton)e.Item.FindControl("btnShowUserReports");
            btnShowUserReports.Attributes["href"] = "javascript:void(0);";
            btnShowUserReports.Attributes["target"] = "_blank";
            btnShowUserReports.OnClientClick = String.Format("return ShowUserReports('{0}','{1}');", e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["UserName"], e.Item.ItemIndex);

            LinkButton btnShowEdit = (LinkButton)e.Item.FindControl("btnShowUserReports");
            btnShowEdit.Attributes["href"] = "javascript:void(0);";
            btnShowEdit.Attributes["target"] = "_blank";
            //btnShowEdit.OnClientClick = String.Format("return ShowUserReports('{0}','{1}');", e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["UserName"], e.Item.ItemIndex);
        }
    }

    protected void grdUsers_OnUpdateCommand(object sender, GridCommandEventArgs e)
    {
        //var editableItem = ((GridEditableItem)e.Item);
        //var userId = editableItem.GetDataKeyValue("UserId");
        //Hashtable newValues = new Hashtable();
        //e.Item.OwnerTableView.ExtractValuesFromItem(newValues, editableItem);

        //RadComboBox cboRole = e.Item.FindControl("cboRole") as RadComboBox;
        //CheckBox chkInactive = e.Item.FindControl("chkInactive") as CheckBox;

        //try
        //{
        //    using (TranscomUniversityV3ModelContainer db = new TranscomUniversityV3ModelContainer())
        //    {
        //        db.pr_TranscomUniversity_UpdateUsersInRoles(userId.ToString(), cboRole.SelectedValue.ToString());

        //        if (chkInactive.Checked)
        //            db.pr_TranscomUniversity_UpdateUsers(true, userId.ToString());
        //        else
        //            db.pr_TranscomUniversity_UpdateUsers(false, userId.ToString());
        //    }
        //}
        //catch
        //{
        //    grdUsers.Controls.Add(new LiteralControl(string.Format("<strong style='color: red'>Error Updating record: {0}</strong>", ex.Message)));
        //    e.Canceled = true;
        //}
    }
}