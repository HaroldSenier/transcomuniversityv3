﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TranscomUniversityV3.Master" AutoEventWireup="true"
    CodeFile="ViewCertificate.aspx.cs" Inherits="ViewCertificate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="contentPlaceHolderLeftPanel" runat="server">
    <rad:RadScriptBlock ID="Radscript" runat="server">
        <script type="text/javascript">

            function back() {
                window.history.back();
            }


            function openNonScormNewTab(url) {               
                    var popUp = window.open(url);
                    if (popUp == null || typeof (popUp) == 'undefined') {
                        alert($('#<%= hfPleasedisableyourpopupblockerorallowPopuptoloadthecourseresourceandclickthelinkagain.ClientID %>').val());
                        return;
                    }
                    else {
                        popUp.focus();
                    }
            }
        </script>
    </rad:RadScriptBlock>
    <rad:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true" RenderMode="Lightweight"
        Skin="Bootstrap">
    </rad:RadWindowManager>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolderMain" runat="server">
<asp:HiddenField ID="hfPleasedisableyourpopupblockerorallowPopuptoloadthecourseresourceandclickthelinkagain" runat="server" value="<%$ Resources:LocalizedResource, PleasedisableyourpopupblockerorallowPopuptoloadthecourseresourceandclickthelinkagain%>"/>
    <div class="col-lg-12">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12 thirty-px-padding">
                        <div class="row">
                            <div class="col-md-12 no-paddings">
                                <ul id="ulBreadcrumb" runat="server" class="breadcrumb">
                                    <li><a>
                                        <asp:Label ID="lblTab1" runat="server" Text="<%$ Resources: LocalizedResource, Learner %>" /></a></li>
                                    <li><a>
                                        <asp:Label ID="lblTab2" runat="server" Text="<%$ Resources: LocalizedResource, MyProfile %>" /></a></li>
                                    <li><a>
                                        <asp:Label ID="lblTab3" runat="server" Text="<%$ Resources: LocalizedResource, MyCertifications %>" /></a></li>
                                    <li>
                                        <asp:Label ID="lblCertType" runat="server" Text="<%$ Resources: LocalizedResource, ExtInt %>" /></li>
                                </ul>
                            </div>
                        </div>
                        <div class="row black-header ten-px-padding" style="margin-top: 10px;">
                            <div class="col-md-6 font-bold ">
                                <button onclick="back();" style="background: transparent;">
                                    <i class="fa fa-chevron-circle-left" aria-hidden="true" style="font-size: 30px;">
                                    </i>
                                </button>
                            </div>
                            <%-- <div class="col-md-6 font-bold">
                                <div class="row-fluid">
                                    <div class="col-xs-4 pull-right">
                                        <div id="progbar" runat="server" class="progress" style="width: 100%;">
                                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="40"
                                                aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                                40%
                                            </div>
                                        </div>
                                    </div>
                                    <div id="extra_text" runat="server" class="col-xs-8 text-right">
                                        Overall completion
                                    </div>
                                </div>
                            </div>--%>
                        </div>
                        <div class="row">
                            <div class="col-md-12 certificate-cover-photo" id="BgCert" runat="server">
                                <div class="layer">
                                </div>
                                <div class="certificate-title text-center">
                                    <asp:Label ID="CertificateName" runat="server" /></div>
                                <div class="row">
                                    <div class="col-md-6 col-md-offset-3 border-top-light-white">
                                    </div>
                                    <div class="col-md-10 col-md-offset-1 text-center color-white" style="margin-top: 15px;">
                                        <asp:Label ID="LblDesc" runat="server" />
                                    </div>
                                   <%-- <div class="col-md-3 col-sm-12 col-lg-12 text-center color-white" style="margin-top: 65px;">
                                        <asp:Label ID="LblRate" runat="server" Text="Rate this course" />
                                    </div>--%>
                                    <%--    <div class="col-md-4 col-lg-offset-4">
                                        <div class="row">
                                            <div class="col-md-4 col-lg-offset-4 border-top-light-yellow" style="margin-top: 5px;">
                                            </div>
                                        </div>
                                    </div>--%>
                                    <%--    <div class="col-sm-offset-5 col-sm-2" style="margin-top: 5px;">
                                        <div class="row">
                                            <div class="col-sm-offset-2 col-sm-4">
                                                <rad:RadRating RenderMode="Lightweight" ID="RadRating2" runat="server" ItemCount="5"
                                                    Value="0" SelectionMode="Continuous" Precision="Item" Orientation="Horizontal">
                                                </rad:RadRating>
                                            </div>
                                        </div>
                                    </div>--%>
                                    <div class="col-md-3 col-sm-12 col-lg-12 text-center color-white display-none" style="margin-top: 55px;">
                                        <asp:Label ID="Label1" runat="server" Text="<%$ Resources: LocalizedResource, BadgesEarned %>" />
                                    </div>
                                    <div class="col-md-4 col-lg-offset-4">
                                        <div class="row">
                                            <div class="col-md-4 col-lg-offset-4 border-top-light-yellow" style="margin-top: 5px;">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-offset-5 col-sm-2" style="margin-top: 5px;">
                                        <div class="row">
                                            <asp:DataList ID="repBadges" runat="server" RepeatLayout="Table" RepeatColumns="5"
                                                CellPadding="4" CellSpacing="4">
                                                <ItemTemplate>
                                                    <a href="javascript:;" onclick="popovertrigger(this); return false;" class="popovers"
                                                        data-placement="top" data-html="true" data-toggle="popover" data-trigger="focus"
                                                        title="Badge Information" data-container="body" data-content="<div class='row'><div class='col-md-6'><img src='/Media/Images/<%# Eval("BadgeImgBig") %>' width='75' class='img-responsive'></div><div class='col-md-6'><%#Eval("BadgeName") %><br /><%#Eval("Description") %></div></div>">
                                                        <asp:Image ID="Image1" ImageUrl='<%# "~/Media/Images/"+Eval("BadgeImgSmall") %>'
                                                            runat="server" />
                                                    </a>
                                                    <table>
                                                        <tr>
                                                            <td colspan="5">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:DataList>
                                        </div>
                                    </div>
                                    <% if (Request.QueryString["Type"] == "Int")
                                       { %>
                                    <div class="col-sm-offset-5 col-sm-2 text-center" style="margin-top: 100px;">
                                        <asp:Button  CssClass="btn btn-rounded-corner-gray" runat="server" ID="LinkCert" Text="<%$ Resources: LocalizedResource, ViewCertificate %>" OnClick="linkCert_OnClick"/>
                                      <%--  <a href="Certificate.aspx?CertID=<% Response.Write(Request.QueryString["CertID"]); %>"
                                            id="LinkCert" class="btn btn-rounded-corner-gray"><asp:Label runat="server" Text=/></a>--%>
                                    </div>


                                    <% } %>
                                </div>
                            </div>
                            <asp:Panel ID="CertDetails" runat="server" Visible="false">
                                <div class="col-md-12 text-center display-none">
                                    <asp:LinkButton ID="BtnTakeLater" runat="server" CssClass="btn btn-default">
                                       <i class="fa fa-heart"></i> <asp:Label ID="Label2" runat="server" Text="<%$ Resources: LocalizedResource, AddtotakeLaterCourses %>" />
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="BtnEnrolled" runat="server" CssClass="btn btn-default">
                                       <asp:Label ID="Label3" runat="server" Text="<%$ Resources: LocalizedResource, Enrolled %>" />
                                    </asp:LinkButton>
                                </div>
                                <div class="col-sm-offset-1 col-sm-10 border-top-light-dark-gray">
                                </div>
                            </asp:Panel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
