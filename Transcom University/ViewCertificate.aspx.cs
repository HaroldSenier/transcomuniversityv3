﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;


public partial class ViewCertificate : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Request.QueryString["Tab"] != null && Request.QueryString["CertID"] != null && Request.QueryString["Type"] == "Ext")
            {
                lblCertType.Text = Resources.LocalizedResource.ExternalCertifications.ToString();


                DataSet ds = DataHelper.GetCert(Convert.ToInt32(Utils.Decrypt(Request.QueryString["CertID"])));

                string GoogleID = DataHelper.GetGoogleID();

                CertificateName.Text = ds.Tables[0].Rows[0]["CertificateName"].ToString();
                string File = "Media/Uploads/Certs/" + GoogleID + "/" + ds.Tables[0].Rows[0]["ImgFile"].ToString(); ;


                BgCert.Attributes["style"] = "background-image: url(" + File + ")";
                //progbar.Attributes["style"] = "display: none;";
                //extra_text.Attributes["style"] = "display: none;";
                //LblRate.Visible = false;

                //for badges comment first 
                //badges not applicable
                //repBadges.DataSource = DataHelper.GetAllCertBadges(Convert.ToInt32(Request.QueryString["CertID"]));
                //repBadges.DataBind();


            }

            if (Request.QueryString["Tab"] != null && Request.QueryString["CertID"] != null && Request.QueryString["Type"] == "Int")
            {
                lblCertType.Text = Resources.LocalizedResource.InternalCertifications.ToString();


                DataSet ds = DataHelper.GetCert(Convert.ToInt32(Utils.Decrypt(Request.QueryString["CertID"])));

                string GoogleID = DataHelper.GetGoogleID();

                DataSet dsCourse = DataHelper.GetCourse(Convert.ToInt32(ds.Tables[0].Rows[0]["CourseID"].ToString()));

                //repBadges.DataSource = DataHelper.GetAllCertBadges(Convert.ToInt32(Utils.Decrypt(Request.QueryString["CertID"])));
                //repBadges.DataBind();

                CertificateName.Text = dsCourse.Tables[0].Rows[0]["Title"].ToString();
                string File = "Media/Uploads/Courses/" + dsCourse.Tables[0].Rows[0]["CourseImage"].ToString();
                LblDesc.Text = dsCourse.Tables[0].Rows[0]["Description"].ToString();

                //BgCert.Attributes["style"] = "background-image: url(" + File + ")";

                CertDetails.Visible = true;


            }
        }
        catch (Exception)
        {
            RadWindowManager1.RadAlert(Resources.LocalizedResource.SomethingWentWrongPleaseTryAgain.ToString() + ". Please refresh the page.", 330, 180, Resources.LocalizedResource.InvalidCertificate.ToString(), "");
        }
    }

    protected void linkCert_OnClick(object sender, EventArgs e)
    { 
       var path = "Certificate.aspx?CertID=" + Request.QueryString["CertID"] ;
       ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", Utils.callClientScript("openNonScormNewTab",path), true);
    }
}