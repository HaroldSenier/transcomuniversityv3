﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Welcome.aspx.cs" Inherits="Welcome" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Transcom University</title>
    <link rel="icon" type="image/png" href="Media/Images/favicon.png" />
    <link href="https://fonts.googleapis.com/css?family=Roboto|Cabin|Open+Sans" rel="stylesheet" />


    <style type="text/css">
        body, html
        {
            height: 100vh;
            width: 100vw;
            overflow: hidden;
            margin: 0;
            padding: 0;
            font-family: 'Roboto' , 'Open-sans' , Arial, Sans-Serif;
        }
        /**Variables**/
        :root
        {
            --teal: #09a8a4;
            --blue: #03088c;
        }
        /**Main Content**/
        #splashPage
        {
            display: block;
            position: absolute;
            height: 100%;
            width: 100%;
        }
        #splashPage .bg
        {
           background: white;

            height: 100%;
            width: 100%;
            text-align: center;
            opacity: 0.6;
            position: fixed;
            object-fit: cover;
        }
        #splashPage .banner.medium 
        {
            margin-top: 3%;
            height: 50%;
            width: 25%;
        }
        #splashPage .region.search
        {
            text-align: center;
        }
        
        #splashPage .region.search .area label
        {
            font-size: 20px;
        }
        
        #splashPage .region.search .area input.txtbox
        {
            width: 70vw;
            font-size: 14px;
            margin: 1% 0;
            padding: .5% .3%;
            border: 0;
            padding-left: 2%;
        }
        #splashPage .region.search .area .btn
        {
            height: 10%;
            width: 7%;
            border: 0;
            margin: 1%;
        }
        #splashPage .region.search .area .icon
        {
            width: 1.1%;
            position: relative;
            left: 24px;
            top: 3.5px;
            filter: invert(.5);
        }
        
        /**General Rules**/
        /**Positions**/
        .center-both
        {
            position: relative;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
        }
        .center-vertical
        {
            position: relative;
            left: 50%;
            transform: translate(-50%, 0);
        }
        /**Colors**/
        .btn.teal
        {
            background: var(--teal, teal);
            color: White;
            font-weight: bold;
            padding: .5%;
        }
        label.blue
        {
            color: var(--blue, blue);
        }
        /**Borders**/
        .soft
        {
            border-radius: 7px;
        }
        .soft1
        {
            border-radius: 5px;
        }
    </style>

    <link href="Media/mediaQuery.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div id="splashPage" class="container main-wrapper">
        <asp:Image ID="Image1" ImageUrl="Media/Images/bg_splash1.png" runat="server" CssClass="bg" />
        <asp:Image ID="Image2" ImageUrl="Media/Images/tuv3_icon2.png" runat="server" CssClass="banner medium center-vertical" />
        <div class="region search center-vertical">
            <div class="area">
                <label id="lbl1" class="blue">
                    Look for a course</label>
                <br />
                <asp:Image ImageUrl="Media/Images/search-icon.png" runat="server" class="icon" /><asp:TextBox ID="txtSearch"
                    runat="server" placeholder="Search for a course..." class="txtbox soft" onkeypress="return enterPressed(event)"></asp:TextBox>
                <br />
                <label id="Label1" class="blue">
                    or sign-in to your account</label>
                <br />
                <asp:Button ID="btnSignin" Text="Sign-in" runat="server" CssClass="btn teal soft1" OnClick="btnRequestLogin_Click" />
            </div>
        </div>
    </div>
    </form>

    <script type="text/javascript">
        function enterPressed(e) {
            if (e.keyCode == 13) {
                var q = document.getElementById("txtSearch").value;
                window.location.href = "SearchResult.aspx?q=" + q;
                return false;
            }
        }

//        function setCookie(name, value, days) {
//            var expires = "";
//            if (days) {
//                var date = new Date();
//                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
//                expires = "; expires=" + date.toUTCString();
//            }
//            document.cookie = name + "=" + (value || "") + expires + "; path=/";
//        }

//        function setFirstVisitCookie() {
//            setCookie("isFirstVisit", "", 99);
//            
//        }

        
    </script>
</body>
</html>
