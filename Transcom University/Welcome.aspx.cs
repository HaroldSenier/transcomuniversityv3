﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Membership.OpenAuth;

public partial class Welcome : BasePage
{
    public string ReturnUrl { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        ReturnUrl = Request.QueryString["ReturnUrl"];

        bool isAuth = (System.Web.HttpContext.Current.User != null) && System.Web.HttpContext.Current.User.Identity.IsAuthenticated;

        if (isAuth)
            Response.Redirect("Home.aspx");

    }

    protected void btnRequestLogin_Click(object sender, EventArgs e)
    {
        var redirectUrl = "~/ExternalLandingPage.aspx";

        if (!String.IsNullOrEmpty(ReturnUrl))
        {
            var resolvedReturnUrl = ResolveUrl(ReturnUrl);
            redirectUrl += "?ReturnUrl=" + HttpUtility.UrlEncode(resolvedReturnUrl);
        }

        OpenAuth.RequestAuthentication("google", redirectUrl);
    }
}