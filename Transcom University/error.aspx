<%@ Page Language="C#"  MasterPageFile="~/TranscomUniversityV3.Master"
    AutoEventWireup="true" CodeFile="error.aspx.cs" Inherits="error" Title="Transcom University Testing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="contentPlaceHolderLeftPanel" runat="Server">
<div class="container text-center">
    <asp:Panel ID="pnlMain" runat="server" HorizontalAlign="center">
        <table width="100%" style="height: 400px">
            <tr>
                <td>
                    <asp:Label ID="lblError" runat="server" />
                    <br />
                    <asp:Label ID="lblContact" runat="server" Text="<%$ Resources:LocalizedResource, Pleasecontactyourexamineradministrator %>" />
                    <br /><br />
                    <asp:Button ID="btnError" runat="server" Text="<%$ Resources:LocalizedResource, Return %>" OnClick="btnError_Click" CssClass="buttons btn-teal btn" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    </div>
</asp:Content>
