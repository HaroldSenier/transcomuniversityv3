using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class error : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.lblError.Text = SessionManager_Testing.SessionMainText;
    }
    protected void btnError_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/main.aspx?" + Request.QueryString.ToString());
    }
}
