using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;
using System.Collections.Generic;
//using TheLibrary.ErrorLogger;

public partial class gradeexam : BasePage
{
    private const string wrongImage = @"~/Media/images/icon-fail.gif";

    public string WrongImage
    {
        get { return wrongImage; }
    }

    private const string rightImage = @"~/Media/images/icon-pass.gif";

    public string RightImage
    {
        get { return rightImage; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            if (SessionManager_Testing.SessionTestTaken.UserID != null)
            {
                decimal decTotal = 0;
                try
                {
                    int totItems = 0, totRight = 0;
                    DataTable table = new DataTable("datatable");
                    table.Columns.Add("Question");
                    table.Columns.Add("ImageUrl");
                    DataRow row;
                    
                    TestCategory category = TestCategory.Select(SessionManager_Testing.SessionTestCategoryID, true);
                    TestTaken taken = TestTaken.Select(SessionManager_Testing.SessionTestTaken.TestTakenID);
                    if (category != null && taken != null)
                    {
                        Questionnaire[] questions = Questionnaire.SelectByCategory(category.TestCategoryID);
                        if (questions == null)
                        {
                            ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
                            service.WriteErrorWithUserID(4, "gradeexam.aspx", "Page_Load", "catch no questions - " + category.TestCategoryID.ToString(), SessionManager_Testing.SessionUsername); 
                            return;
                        }
                        foreach (Questionnaire question in questions)
                        {
                            row = table.NewRow();
                            row["Question"] = question.Question;
                            SaveTestResponse response = SaveTestResponse.Retrieve(SessionManager_Testing.SessionUsername, taken.TestTakenID, question.QuestionnaireID);

                            if (response == null)
                            {
                                ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
                                service.WriteErrorWithUserID(4, "gradeexam.aspx", "Page_Load", "no exam response found - " + SessionManager_Testing.SessionUsername + " " + taken.TestTakenID.ToString() + " " + question.QuestionnaireID.ToString(), SessionManager_Testing.SessionUsername);
                            }
                            if (question.TypeCode == "matching")
                            {
                                if (response.Response1.Trim() == question.Ans1.Split('|')[0].Trim() && response.Response2.Trim() == question.Ans2.Split('|')[0].Trim() &&
                                response.Response3.Trim() == question.Ans3.Split('|')[0].Trim() && response.Response4.Trim() == question.Ans4.Split('|')[0].Trim() &&
                                response.Response5.Trim() == question.Ans5.Split('|')[0].Trim() && response.Response6.Trim() == question.Ans6.Split('|')[0].Trim() &&
                                response.Response7.Trim() == question.Ans7.Split('|')[0].Trim() && response.Response8.Trim() == question.Ans8.Split('|')[0].Trim() &&
                                response.Response9.Trim() == question.Ans9.Split('|')[0].Trim() && response.Response10.Trim() == question.Ans10.Split('|')[0].Trim())
                                {
                                    row["ImageUrl"] = this.RightImage;
                                    totRight++;
                                }
                            }
                            //ADDED BY RAYMARK COSME <11/19/2015>
                            else if (question.TypeCode == "rate")
                            {
                                Session["rate"] = response.Response1.ToString();
                            }
                            //ADDED BY RAYMARK COSME <11/19/2015>
                            else if (question.TypeCode == "comments")
                            {
                                Session["comments"] = response.Response1.ToString();
                            }
                            //ADDED BY RAYMARK COSME <11/19/2015>
                            else if (question.TypeCode == "dropdown")
                            {
                                Session["dropdown"] = response.Response1.ToString();
                            }
                            //ADDED BY RAYMARK COSME <11/19/2015>
                            else if (question.TypeCode == "masurvey")
                            {
                                Session["masurvey1"] = response.Response1.ToString();
                                Session["masurvey2"] = response.Response2.ToString();
                                Session["masurvey3"] = response.Response3.ToString();
                                Session["masurvey4"] = response.Response4.ToString();
                                Session["masurvey5"] = response.Response5.ToString();
                                Session["masurvey6"] = response.Response6.ToString();
                                Session["masurvey7"] = response.Response7.ToString();
                                Session["masurvey8"] = response.Response8.ToString();
                                Session["masurvey9"] = response.Response9.ToString();
                                Session["masurvey10"] = response.Response10.ToString();
                            }
                            else if (question.TypeCode == "calendar")
                            {
                                Session["calendar"] = response.Response1.ToString();
                            }
                            else if (response.Response1.Trim() == question.Ans1.Trim() && response.Response2.Trim() == question.Ans2.Trim() &&
                                response.Response3.Trim() == question.Ans3.Trim() && response.Response4.Trim() == question.Ans4.Trim() &&
                                response.Response5.Trim() == question.Ans5.Trim() && response.Response6.Trim() == question.Ans6.Trim() &&
                                response.Response7.Trim() == question.Ans7.Trim() && response.Response8.Trim() == question.Ans8.Trim() &&
                                response.Response9.Trim() == question.Ans9.Trim() && response.Response10.Trim() == question.Ans10.Trim())
                            {
                                row["ImageUrl"] = this.RightImage;
                                totRight++;
                            }
                            else
                                row["ImageUrl"] = this.WrongImage;

                            totItems++;
                            table.Rows.Add(row);

                            //ADDED BY RAYMARK COSME <11/19/2015>
                            if (question.TypeCode == "rate" || question.TypeCode == "comments" || question.TypeCode == "dropdown" || question.TypeCode == "masurvey" || question.TypeCode == "calendar")
                            {
                                totItems--;
                                table.Rows.Remove(row);
                            }
                        }
                        taken.Score = totRight;

                        //ADDED BY RAYMARK COSME <11/19/2015>
                        decTotal = questions.Length;
                        foreach (Questionnaire question in questions)
                        {
                            if (question.TypeCode == "rate" || question.TypeCode == "comments" || question.TypeCode == "dropdown" || question.TypeCode == "masurvey" || question.TypeCode == "calendar")
                            {
                                decTotal--;
                            }
                        }

                        decimal decScore = (Convert.ToDecimal(totRight) / decTotal);
                        if (decScore >= (Convert.ToDecimal(category.PassingGrade) / Convert.ToDecimal(100)))
                        {
                            taken.SetPassed(true);
                            try
                            {
                                TestScoreSummary summary = new TestScoreSummary();
                                summary.AccountID = category.AccountID;
                                summary.CimNumber = SessionManager_Testing.SessionCim;
                                summary.Passed = true;
                                summary.Score = Convert.ToDouble(decScore);
                                summary.TestCategoryID = taken.TestCategoryID;
                                summary.TestName = category.TestName;
                                summary.TestTakenID = taken.TestTakenID;
                                summary.Insert();

                                //TestHarmonyQualification[] qualifications = TestHarmonyQualification.SelectByTestCategory(taken.TestCategoryID);
                                //foreach (TestHarmonyQualification qualification in qualifications)
                                //{
                                //    TestHarmonyQualification.InsertToVader2(Config.Vader2CimEnterpriseConnectionString(), qualification.HarmonyQualificationID, DateTime.Now, Convert.ToDateTime("3000/01/01"), 0, summary.Score.ToString(), Convert.ToInt32(SessionManager_Testing.SessionUsername), -99, 1, "", "");
                                //}
                            }
                            catch (Exception ex)
                            {
                                ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
                                service.WriteError(4, "gradeexam.aspx", "Page_Load", ex.Message);
                            }
                        }
                        else
                        {
                            taken.SetPassed(false);
                            try
                            {
                                TestScoreSummary summary = new TestScoreSummary();
                                summary.AccountID = category.AccountID;
                                summary.CimNumber = SessionManager_Testing.SessionCim;
                                summary.Passed = false;
                                summary.Score = Convert.ToDouble(decScore);
                                summary.TestCategoryID = taken.TestCategoryID;
                                summary.TestName = category.TestName;
                                summary.TestTakenID = taken.TestTakenID;
                                summary.Insert();
                            }
                            catch (Exception ex)
                            {

                            }
                        }

                        //TODO: GLA 01142014 Show Retry button if failed
                        bool bPassed = decScore * 100 >= category.PassingGrade;
                        btnRetry.Visible = bPassed == false ? true : false;

                        string passorfail = bPassed ? Resources.LocalizedResource.Passed.ToString() : Resources.LocalizedResource.Failed.ToString();

                        this.lblDateStartedVal.Text = taken.DateStartTaken.ToString();
                        this.lblDateFinishedVal.Text = taken.DateEndTaken.ToString();
                        this.lblCIMNumberVal.Text = SessionManager_Testing.SessionUsername;

                        this.lblResult.Text = category.TestName + " - " + passorfail;
                        this.lblCorrectValue.Text = totRight.ToString() + " (" + (decScore * 100) + "%)";
                        this.lblPassingValue.Text = category.PassingGrade.ToString() + "%";
                        this.lblTotalValue.Text = decTotal.ToString();
                        this.lblTriesValue.Text = (TestTaken.Count(SessionManager_Testing.SessionUsername, category.TestCategoryID)).ToString() + "/" + category.TestLimit.ToString();
                        DataSet set = new DataSet();
                        set.Tables.Clear();
                        set.Tables.Add(table);
                        this.gvResults.DataSource = set;
                        this.gvResults.DataBind();

                        //ADDED BY RAYMARK COSME <11/19/2015>

                        if (!string.IsNullOrEmpty(Session["rate"] as string))
                        {
                            this.lblCommentsValue.Text = Session["rate"].ToString();
                        }
                        if (!string.IsNullOrEmpty(Session["comments"] as string))
                        {
                            this.lblCommentsValue.Text = Session["comments"].ToString();
                        }
                        
                        foreach (Questionnaire question in questions)
                        {
                            SaveTestResponse response = SaveTestResponse.Retrieve(SessionManager_Testing.SessionUsername, taken.TestTakenID, question.QuestionnaireID);
                            if (response != null)
                            {
                                TestResponse newResponse = new TestResponse(response);
                                newResponse.Insert();
                                response.Delete();
                            }
                        }

                        // GLA 01142014
                        TestTakenID.Value = SessionManager_Testing.SessionTestTaken.TestTakenID.ToString();
                        TestCategoryID.Value = SessionManager_Testing.SessionTestTaken.TestCategoryID.ToString();

                        SessionManager_Testing.SessionTestTaken = null;
                    }
                    if (category.HideScores)
                    {
                            this.pnlMain.Visible = false;
                            this.pnlThanks.Visible = true;
                    }
                }
                catch (Exception ex)
                {
                    ErrorLoggerService.ErrorLoggerService service = new ErrorLoggerService.ErrorLoggerService();
                    service.WriteErrorWithUserID(4, "gradeexam.aspx", "Page_Load", ex.Message, SessionManager_Testing.SessionUsername);
                    SessionManager_Testing.SessionMainText = "An error was encountered while trying to compute for your exam's grade.";
                }

                ////ADDED BY RAYMARK COSME <11/19/2015>
                if (decTotal == 0)
                {
                    this.pnlThanksSurvey.Visible = true;
                    this.pnlMain.Visible = false;
                    this.pnlThanks.Visible = false;
                }
            }
            else
                Response.Redirect("~/testhub.aspx?" + Request.QueryString.ToString());
        }
    }

    protected void btnContinue_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/testhub.aspx?" + Request.QueryString.ToString());
    }
    //TODO: GLA 011420148
    protected void btnRetry_Click(object sender, EventArgs e)
    {
        int iTestTakenID = Convert.ToInt32(TestTakenID.Value);
        int iTestCategoryID = Convert.ToInt32(TestCategoryID.Value);

        TestCategory category = TestCategory.Select(iTestCategoryID, false);

        if (category != null)
        {
            int testCount = TestTaken.Count(SessionManager_Testing.SessionUsername, category.TestCategoryID);
            int testRetake = TestCategory.checkRetake(category.TestCategoryID);
            if (testCount < category.TestLimit || category.TestLimit == 0 || testRetake == 1)
            {
                SaveTestResponse[] responses = SaveTestResponse.CheckPendingEssays(SessionManager_Testing.SessionUsername, iTestTakenID);
                if (responses.Length > 0)
                {
                    this.lblMessage.Text = Resources.LocalizedResource.YouhavetakenthistestItisstillpendingcompletionchecking.ToString();
                    this.mpeMessage.Show();
                }
                else
                {
                    SessionManager_Testing.SessionTestCategoryID = category.TestCategoryID;
                    Questionnaire[] questionnaire = Questionnaire.SelectByCategory(category.TestCategoryID);
                    if (questionnaire.Length > 0)
                    {
                        bool hasPassed = false;
                        TestTaken[] taken = TestTaken.SelectByExam(category.TestCategoryID);
                        foreach (TestTaken test in taken)
                        {
                            if (test.Passed && test.UserID == SessionManager_Testing.SessionUsername)
                                hasPassed = true;
                        }
                        if (hasPassed == true && testRetake != 1)
                        {
                            iTestCategoryID = category.TestCategoryID;
                            this.mpeRetake.Show();
                        }
                        else
                        {
                            SessionManager_Testing.SessionQuestionnaire = new List<Questionnaire>();
                            foreach (Questionnaire question in questionnaire)
                                SessionManager_Testing.SessionQuestionnaire.Add(question);
                            SessionManager_Testing.SessionExamAction = "newexam";
                            SessionManager_Testing.SessionExamIsRetake = "retake"; //TODO: GLA 01092014
                            Response.Redirect("~/testdefault.aspx?" + Request.QueryString.ToString());
                        }
                    }
                    else
                    {
                        this.lblMessage.Text = Resources.LocalizedResource.Thistesthasnoitems.ToString();
                        this.mpeMessage.Show();
                    }
                }
            }
            else
            {
                this.lblMessage.Text = Resources.LocalizedResource.Youhavealreadyusedupalltriesavailableforthistest.ToString();
                this.mpeMessage.Show();
            }
        }
        else
        {
            lblMessage.Text = Resources.LocalizedResource.Thisexamhasbeenremovedfromthesystemorhasalreadyexpired.ToString();
            this.mpeMessage.Show();
        }
    }
}
