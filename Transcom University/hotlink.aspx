<%@ Page Language="C#" MasterPageFile="~/TranscomUniversityV3.Master"
    AutoEventWireup="true" CodeFile="hotlink.aspx.cs" Inherits="hotlink" %>

<asp:Content ID="Content1" ContentPlaceHolderID="contentPlaceHolderLeftPanel" runat="Server">
    <asp:Panel ID="pnlAll" runat="server" HorizontalAlign="center">
        <asp:UpdatePanel ID="udpSub" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlUpdate" runat="server" Height="150px">
                    <asp:UpdateProgress ID="udpProgress" runat="server">
                        <ProgressTemplate>
                            <asp:Panel ID="pnlBuffer" runat="server" Height="150px">
                                <table>
                                    <tr>
                                        <td align="center" valign="middle" style="height: 200px">
                                            <asp:Image ID="imgLoading" runat="server" ImageUrl="~/images/ajax-loader.gif" />
                                            <br />
                                            <asp:Label ID="lblPleaseWait" runat="server" Text="<%$ Resources: LocalizedResource, PleaseWait %>" CssClass="smallText" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </asp:Panel>
                <asp:Panel ID="pnlTaking" runat="server" HorizontalAlign="center">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="lblTaking" runat="server" Text="<%$ Resources: LocalizedResource, Youmustlogintotakethisexam %>" />
                            </td>
                        </tr>
                        <tr id="trExamName" runat="server">
                            <td>
                                <asp:Label ID="lblExamID" runat="server" Visible="false" Text="" />
                                <asp:Label ID="lblExamName" runat="server" Text="" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="pnlMain" runat="server" DefaultButton="lnkLogin" HorizontalAlign="center"
                    CssClass="panelStandard" BorderWidth="0px">
                    <table>
                        <tr>
                            <td colspan="2" align="center">
                                <asp:CustomValidator ID="cvLoginError" runat="server" ErrorMessage="<%$ Resources: LocalizedResource, InvalidLoginCredentials %>"
                                    Display="dynamic" CssClass="validatorStyle" />
                            </td>
                        </tr>
                        <tr class="display-none">
                            <td align="right">
                                <asp:Label ID="lblUsername" runat="server" Text="Username" />
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtUsername" runat="server" Width="120px" />
                            </td>
                        </tr>
                        <tr class="display-none">
                            <td align="right">
                                <asp:Label ID="lblPassword" runat="server" Text="Password" />
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtPassword" runat="server" Width="120px" TextMode="password" />
                            </td>
                        </tr>
                        <%--<tr>
                            <td align="right">
                                <asp:Label ID="lblSite" runat="server" Text="Site" />
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlSites" runat="server" DataTextField="CompanySite" DataValueField="CompanySiteShort"
                                    AppendDataBoundItems="true">
                                    <asp:ListItem Text="Testing" Value="Test" />
                                </asp:DropDownList>
                            </td>
                        </tr>--%>
                        <tr class="display-none">
                            <td align="center" colspan="2">
                                <asp:LinkButton ID="lnkLogin" runat="server" Text="<%$ Resources: LocalizedResource, Login %>" CssClass="linkbutton" OnClick="lnkLogin_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
