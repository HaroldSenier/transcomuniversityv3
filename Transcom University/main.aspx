<%@ Page Language="C#"  MasterPageFile="~/TranscomUniversityV3.Master"
    AutoEventWireup="true" CodeFile="main.aspx.cs" Inherits="main" Title="NuSkill Check v2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="contentPlaceHolderLeftPanel" runat="Server">
    <asp:Panel ID="pnlMain" runat="server" HorizontalAlign="center" Width="100%">
        <table width="100%">
            <tr>
                <td>
                    <asp:Label ID="lblWelcome" runat="server" Text="<%$ Resources: LocalizedResource, WelcometoTranscomUniversityTesting %>" CssClass="midHeader"/>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblTesthub" runat="server" Text="<%$ Resources: LocalizedResource, Click %> " CssClass="linkbutton" />
                    <asp:LinkButton ID="lnkTestHub" runat="server" Text="<%$ Resources: LocalizedResource, here %> " CssClass="linkbutton" OnClick="lnkTestHub_Click" />
                    <asp:Label ID="lblTestHub2" runat="server" Text="<%$ Resources: LocalizedResource, togotothetesthub %>" CssClass="linkbutton" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
