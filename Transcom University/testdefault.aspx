
<%@ Page Language="C#" MasterPageFile="~/TranscomUniversityV3.Master"
    AutoEventWireup="true" CodeFile="testdefault.aspx.cs" Inherits="testdefault"
    Title="Transcom University Testing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="contentPlaceHolderLeftPanel" runat="Server">
    <asp:Panel ID="pnlMain" runat="server"  CssClass="relative-hr-center" style="border: 1px solid black;margin: 5% 0;">
        <table cellpadding="10" cellspacing="0" width="100%">
            <tr>
                <td align="center">
                    <asp:Panel ID="pnlUpdate" runat="server">
                        <asp:UpdateProgress ID="udpProgress" runat="server">
                            <ProgressTemplate>
                                <asp:Panel ID="pnlBuffer" runat="server">
                                    <table>
                                        <tr>
                                            <td align="center" valign="middle">
                                                <asp:Image ID="imgLoading" runat="server" ImageUrl="~/images/ajax-loader.gif" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </asp:Panel>
                </td>
            </tr>
            <tr style="height: 80px; vertical-align: middle">
                <td align="center" style="vertical-align: middle; font-size: large; 
                    padding: 0px 10px 0px 10px">
                    <asp:Label ID="lblExamName" runat="server" CssClass="testQuestion" />
                </td>
            </tr>
            <tr>
                <td align="center" >
                    <asp:Label ID="lblInstructions" runat="server" Text="<%$ Resources:LocalizedResource, ClickTheBeginButtonToStartTheExamsurvey %>" Font-Bold="true" />
                </td>
            </tr>
            <tr>
                <td align="left"  id="tdInstructions" runat="server">
                    <asp:Label ID="lblInstructionValue" runat="server" Width="90%" />
                </td>
            </tr>
            <tr style="height: 30px">
                <td align="center" style="vertical-align: middle; font-size: large; ">
                    <table width="90%" cellpadding="5" style="vertical-align: middle; 
                        font-size: small;">
                        <tr>
                            <td align="center" style="padding: 10px 10px 10px 10px">
                                <asp:Label ID="lblAbout" runat="server" Text="<%$ Resources:LocalizedResource, Views %>" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="height: 20px">
                <td align="center">
                    <asp:Label ID="lblDuration" runat="server" Text="<%$ Resources:LocalizedResource, Duration %>:" />
                    <asp:Label ID="lblDurationVal" runat="server" />
                </td>
            </tr>
            <tr style="height: 40px">
                <td align="center" valign="middle" style="">
                    <asp:Button ID="btnContinue" runat="server" Text="<%$ Resources:LocalizedResource, Begin %>" OnClick="btnContinue_Click"
                        Width="80px" CssClass="buttons" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
