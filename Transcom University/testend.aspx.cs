using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NuSkill.Business;

public partial class testend : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(SessionManager_Testing.SessionUsername))
            Response.Redirect("~/LandingPage.aspx?" + Request.QueryString.ToString());
        TestCategory category = TestCategory.Select(SessionManager_Testing.SessionTestCategoryID, true);
        if (category != null)
        {
            if (category.IsEssay)
            {
                this.lblAbout.Text = "Your exam will be graded after an examiner checks the essay portion/s.";
                this.btnContinue.Text = "Return";
            }
        }
        else
        {
            this.lblAbout.Text = "System could not find this exam. Please contact your examiner/administrator.";
            SessionManager_Testing.SessionVarText = "404exam";
        }
            //Response.Redirect("~/testhub.aspx");
    }
    protected void btnContinue_Click(object sender, EventArgs e)
    {
        if (SessionManager_Testing.SessionVarText == "404exam")
        {
            SessionManager_Testing.SessionVarText = string.Empty;
            Response.Redirect("~/testhub.aspx?" + Request.QueryString.ToString());
        }
        if (SessionManager_Testing.SessionIsEssay)
            Response.Redirect("~/testhub.aspx?" + Request.QueryString.ToString());
        else
            Response.Redirect("~/gradeexam.aspx?" + Request.QueryString.ToString());
    }
}
